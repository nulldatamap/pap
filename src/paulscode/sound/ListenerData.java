package paulscode.sound;

public class ListenerData {
   public Vector3D position;
   public Vector3D lookAt;
   // $FF: renamed from: up paulscode.sound.Vector3D
   public Vector3D field_595;
   public float angle = 0.0F;

   public ListenerData() {
      this.position = new Vector3D(0.0F, 0.0F, 0.0F);
      this.lookAt = new Vector3D(0.0F, 0.0F, -1.0F);
      this.field_595 = new Vector3D(0.0F, 1.0F, 0.0F);
      this.angle = 0.0F;
   }

   public ListenerData(float var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8, float var9, float var10) {
      this.position = new Vector3D(var1, var2, var3);
      this.lookAt = new Vector3D(var4, var5, var6);
      this.field_595 = new Vector3D(var7, var8, var9);
      this.angle = var10;
   }

   public ListenerData(Vector3D var1, Vector3D var2, Vector3D var3, float var4) {
      this.position = var1.clone();
      this.lookAt = var2.clone();
      this.field_595 = var3.clone();
      this.angle = var4;
   }

   public void setData(float var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8, float var9, float var10) {
      this.position.field_694 = var1;
      this.position.field_695 = var2;
      this.position.field_696 = var3;
      this.lookAt.field_694 = var4;
      this.lookAt.field_695 = var5;
      this.lookAt.field_696 = var6;
      this.field_595.field_694 = var7;
      this.field_595.field_695 = var8;
      this.field_595.field_696 = var9;
      this.angle = var10;
   }

   public void setData(Vector3D var1, Vector3D var2, Vector3D var3, float var4) {
      this.position.field_694 = var1.field_694;
      this.position.field_695 = var1.field_695;
      this.position.field_696 = var1.field_696;
      this.lookAt.field_694 = var2.field_694;
      this.lookAt.field_695 = var2.field_695;
      this.lookAt.field_696 = var2.field_696;
      this.field_595.field_694 = var3.field_694;
      this.field_595.field_695 = var3.field_695;
      this.field_595.field_696 = var3.field_696;
      this.angle = var4;
   }

   public void setData(ListenerData var1) {
      this.position.field_694 = var1.position.field_694;
      this.position.field_695 = var1.position.field_695;
      this.position.field_696 = var1.position.field_696;
      this.lookAt.field_694 = var1.lookAt.field_694;
      this.lookAt.field_695 = var1.lookAt.field_695;
      this.lookAt.field_696 = var1.lookAt.field_696;
      this.field_595.field_694 = var1.field_595.field_694;
      this.field_595.field_695 = var1.field_595.field_695;
      this.field_595.field_696 = var1.field_595.field_696;
      this.angle = var1.angle;
   }

   public void setPosition(float var1, float var2, float var3) {
      this.position.field_694 = var1;
      this.position.field_695 = var2;
      this.position.field_696 = var3;
   }

   public void setPosition(Vector3D var1) {
      this.position.field_694 = var1.field_694;
      this.position.field_695 = var1.field_695;
      this.position.field_696 = var1.field_696;
   }

   public void setOrientation(float var1, float var2, float var3, float var4, float var5, float var6) {
      this.lookAt.field_694 = var1;
      this.lookAt.field_695 = var2;
      this.lookAt.field_696 = var3;
      this.field_595.field_694 = var4;
      this.field_595.field_695 = var5;
      this.field_595.field_696 = var6;
   }

   public void setOrientation(Vector3D var1, Vector3D var2) {
      this.lookAt.field_694 = var1.field_694;
      this.lookAt.field_695 = var1.field_695;
      this.lookAt.field_696 = var1.field_696;
      this.field_595.field_694 = var2.field_694;
      this.field_595.field_695 = var2.field_695;
      this.field_595.field_696 = var2.field_696;
   }

   public void setAngle(float var1) {
      this.angle = var1;
      this.lookAt.field_694 = -1.0F * (float)Math.sin((double)this.angle);
      this.lookAt.field_696 = -1.0F * (float)Math.cos((double)this.angle);
   }
}
