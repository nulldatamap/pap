package paulscode.sound.codecs;

import com.jcraft.jogg.Packet;
import com.jcraft.jogg.Page;
import com.jcraft.jogg.StreamState;
import com.jcraft.jogg.SyncState;
import com.jcraft.jorbis.Block;
import com.jcraft.jorbis.Comment;
import com.jcraft.jorbis.DspState;
import com.jcraft.jorbis.Info;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownServiceException;
import javax.sound.sampled.AudioFormat;
import paulscode.sound.ICodec;
import paulscode.sound.SoundBuffer;
import paulscode.sound.SoundSystemConfig;
import paulscode.sound.SoundSystemLogger;

public class CodecJOrbis implements ICodec {
   private static final boolean GET = false;
   private static final boolean SET = true;
   private static final boolean XXX = false;
   private URL url;
   private URLConnection urlConnection = null;
   private InputStream inputStream;
   private AudioFormat audioFormat;
   private boolean endOfStream = false;
   private boolean initialized = false;
   private byte[] buffer = null;
   private int bufferSize;
   private int count = 0;
   private int index = 0;
   private int convertedBufferSize;
   private float[][][] pcmInfo;
   private int[] pcmIndex;
   private Packet joggPacket = new Packet();
   private Page joggPage = new Page();
   private StreamState joggStreamState = new StreamState();
   private SyncState joggSyncState = new SyncState();
   private DspState jorbisDspState = new DspState();
   private Block jorbisBlock;
   private Comment jorbisComment;
   private Info jorbisInfo;
   private SoundSystemLogger logger;

   public CodecJOrbis() {
      this.jorbisBlock = new Block(this.jorbisDspState);
      this.jorbisComment = new Comment();
      this.jorbisInfo = new Info();
      this.logger = SoundSystemConfig.getLogger();
   }

   public void reverseByteOrder(boolean var1) {
   }

   public boolean initialize(URL var1) {
      this.initialized(true, false);
      if (this.joggStreamState != null) {
         this.joggStreamState.clear();
      }

      if (this.jorbisBlock != null) {
         this.jorbisBlock.clear();
      }

      if (this.jorbisDspState != null) {
         this.jorbisDspState.clear();
      }

      if (this.jorbisInfo != null) {
         this.jorbisInfo.clear();
      }

      if (this.joggSyncState != null) {
         this.joggSyncState.clear();
      }

      if (this.inputStream != null) {
         try {
            this.inputStream.close();
         } catch (IOException var6) {
            ;
         }
      }

      this.url = var1;
      this.bufferSize = SoundSystemConfig.getStreamingBufferSize() / 2;
      this.buffer = null;
      this.count = 0;
      this.index = 0;
      this.joggStreamState = new StreamState();
      this.jorbisBlock = new Block(this.jorbisDspState);
      this.jorbisDspState = new DspState();
      this.jorbisInfo = new Info();
      this.joggSyncState = new SyncState();

      try {
         this.urlConnection = var1.openConnection();
      } catch (UnknownServiceException var4) {
         this.errorMessage("Unable to create game UrlConnection in method 'initialize'.");
         this.printStackTrace(var4);
         this.cleanup();
         return false;
      } catch (IOException var5) {
         this.errorMessage("Unable to create game UrlConnection in method 'initialize'.");
         this.printStackTrace(var5);
         this.cleanup();
         return false;
      }

      if (this.urlConnection != null) {
         try {
            this.inputStream = this.urlConnection.getInputStream();
         } catch (IOException var3) {
            this.errorMessage("Unable to acquire inputstream in method 'initialize'.");
            this.printStackTrace(var3);
            this.cleanup();
            return false;
         }
      }

      this.endOfStream(true, false);
      this.joggSyncState.init();
      this.joggSyncState.buffer(this.bufferSize);
      this.buffer = this.joggSyncState.data;

      try {
         if (!this.readHeader()) {
            this.errorMessage("Error reading the header");
            return false;
         }
      } catch (IOException var7) {
         this.errorMessage("Error reading the header");
         return false;
      }

      this.convertedBufferSize = this.bufferSize << 1;
      this.jorbisDspState.synthesis_init(this.jorbisInfo);
      this.jorbisBlock.init(this.jorbisDspState);
      int var8 = this.jorbisInfo.channels;
      int var2 = this.jorbisInfo.rate;
      this.audioFormat = new AudioFormat((float)var2, 16, var8, true, false);
      this.pcmInfo = new float[1][][];
      this.pcmIndex = new int[this.jorbisInfo.channels];
      this.initialized(true, true);
      return true;
   }

   public boolean initialized() {
      return this.initialized(false, false);
   }

   public SoundBuffer read() {
      byte[] var1;
      return (var1 = this.readBytes()) == null ? null : new SoundBuffer(var1, this.audioFormat);
   }

   public SoundBuffer readAll() {
      byte[] var1 = this.readBytes();

      while(!this.endOfStream(false, false) && ((var1 = appendByteArrays(var1, this.readBytes())) == null || var1.length < SoundSystemConfig.getMaxFileSize())) {
         ;
      }

      return new SoundBuffer(var1, this.audioFormat);
   }

   public boolean endOfStream() {
      return this.endOfStream(false, false);
   }

   public void cleanup() {
      this.joggStreamState.clear();
      this.jorbisBlock.clear();
      this.jorbisDspState.clear();
      this.jorbisInfo.clear();
      this.joggSyncState.clear();
      if (this.inputStream != null) {
         try {
            this.inputStream.close();
         } catch (IOException var1) {
            ;
         }
      }

      this.joggStreamState = null;
      this.jorbisBlock = null;
      this.jorbisDspState = null;
      this.jorbisInfo = null;
      this.joggSyncState = null;
      this.inputStream = null;
   }

   public AudioFormat getAudioFormat() {
      return this.audioFormat;
   }

   private boolean readHeader() throws IOException {
      this.index = this.joggSyncState.buffer(this.bufferSize);
      int var1;
      if ((var1 = this.inputStream.read(this.joggSyncState.data, this.index, this.bufferSize)) < 0) {
         var1 = 0;
      }

      this.joggSyncState.wrote(var1);
      if (this.joggSyncState.pageout(this.joggPage) != 1) {
         if (var1 < this.bufferSize) {
            return true;
         } else {
            this.errorMessage("Ogg header not recognized in method 'readHeader'.");
            return false;
         }
      } else {
         this.joggStreamState.init(this.joggPage.serialno());
         this.jorbisInfo.init();
         this.jorbisComment.init();
         if (this.joggStreamState.pagein(this.joggPage) < 0) {
            this.errorMessage("Problem with first Ogg header page in method 'readHeader'.");
            return false;
         } else if (this.joggStreamState.packetout(this.joggPacket) != 1) {
            this.errorMessage("Problem with first Ogg header packet in method 'readHeader'.");
            return false;
         } else if (this.jorbisInfo.synthesis_headerin(this.jorbisComment, this.joggPacket) < 0) {
            this.errorMessage("File does not contain Vorbis header in method 'readHeader'.");
            return false;
         } else {
            int var2 = 0;

            while(var2 < 2) {
               label71:
               while(true) {
                  do {
                     if (var2 >= 2 || (var1 = this.joggSyncState.pageout(this.joggPage)) == 0) {
                        this.index = this.joggSyncState.buffer(this.bufferSize);
                        if ((var1 = this.inputStream.read(this.joggSyncState.data, this.index, this.bufferSize)) < 0) {
                           var1 = 0;
                        }

                        if (var1 == 0 && var2 < 2) {
                           this.errorMessage("End of file reached before finished readingOgg header in method 'readHeader'");
                           return false;
                        }

                        this.joggSyncState.wrote(var1);
                        continue label71;
                     }
                  } while(var1 != 1);

                  this.joggStreamState.pagein(this.joggPage);

                  while(var2 < 2 && (var1 = this.joggStreamState.packetout(this.joggPacket)) != 0) {
                     if (var1 == -1) {
                        this.errorMessage("Secondary Ogg header corrupt in method 'readHeader'.");
                        return false;
                     }

                     this.jorbisInfo.synthesis_headerin(this.jorbisComment, this.joggPacket);
                     ++var2;
                  }
               }
            }

            this.index = this.joggSyncState.buffer(this.bufferSize);
            this.buffer = this.joggSyncState.data;
            return true;
         }
      }
   }

   private byte[] readBytes() {
      if (!this.initialized(false, false)) {
         return null;
      } else if (this.endOfStream(false, false)) {
         return null;
      } else {
         byte[] var1 = null;
         switch(this.joggSyncState.pageout(this.joggPage)) {
         case -1:
         case 0:
            this.endOfStream(true, true);
            break;
         case 1:
            this.joggStreamState.pagein(this.joggPage);
            if (this.joggPage.granulepos() == 0L) {
               this.endOfStream(true, true);
            } else {
               label47:
               while(true) {
                  switch(this.joggStreamState.packetout(this.joggPacket)) {
                  case -1:
                  case 0:
                     if (this.joggPage.eos() != 0) {
                        this.endOfStream(true, true);
                     }
                     break label47;
                  case 1:
                     var1 = appendByteArrays(var1, this.decodeCurrentPacket());
                  }
               }
            }
         }

         if (!this.endOfStream(false, false)) {
            this.index = this.joggSyncState.buffer(this.bufferSize);
            if (this.index == -1) {
               this.endOfStream(true, true);
            } else {
               this.buffer = this.joggSyncState.data;

               try {
                  this.count = this.inputStream.read(this.buffer, this.index, this.bufferSize);
               } catch (Exception var3) {
                  this.printStackTrace(var3);
                  return var1;
               }

               this.joggSyncState.wrote(this.count);
               if (this.count == 0) {
                  this.endOfStream(true, true);
               }
            }
         }

         return var1;
      }
   }

   private byte[] decodeCurrentPacket() {
      byte[] var1 = new byte[this.convertedBufferSize];
      if (this.jorbisBlock.synthesis(this.joggPacket) == 0) {
         this.jorbisDspState.synthesis_blockin(this.jorbisBlock);
      }

      int var3 = this.convertedBufferSize / (this.jorbisInfo.channels << 1);
      int var4 = 0;

      int var2;
      while(var4 < this.convertedBufferSize && (var2 = this.jorbisDspState.synthesis_pcmout(this.pcmInfo, this.pcmIndex)) > 0) {
         if (var2 < var3) {
            var2 = var2;
         } else {
            var2 = var3;
         }

         for(int var5 = 0; var5 < this.jorbisInfo.channels; ++var5) {
            int var6 = var5 << 1;

            for(int var7 = 0; var7 < var2; ++var7) {
               int var8;
               if ((var8 = (int)(this.pcmInfo[0][var5][this.pcmIndex[var5] + var7] * 32767.0F)) > 32767) {
                  var8 = 32767;
               }

               if (var8 < -32768) {
                  var8 = -32768;
               }

               if (var8 < 0) {
                  var8 |= 32768;
               }

               var1[var4 + var6] = (byte)var8;
               var1[var4 + var6 + 1] = (byte)(var8 >>> 8);
               var6 += 2 * this.jorbisInfo.channels;
            }
         }

         var4 += var2 * this.jorbisInfo.channels << 1;
         this.jorbisDspState.synthesis_read(var2);
      }

      return trimArray(var1, var4);
   }

   private synchronized boolean initialized(boolean var1, boolean var2) {
      if (var1) {
         this.initialized = var2;
      }

      return this.initialized;
   }

   private synchronized boolean endOfStream(boolean var1, boolean var2) {
      if (var1) {
         this.endOfStream = var2;
      }

      return this.endOfStream;
   }

   private static byte[] trimArray(byte[] var0, int var1) {
      byte[] var2 = null;
      if (var0 != null && var0.length > var1) {
         var2 = new byte[var1];
         System.arraycopy(var0, 0, var2, 0, var1);
      }

      return var2;
   }

   private static byte[] appendByteArrays(byte[] var0, byte[] var1) {
      if (var0 == null && var1 == null) {
         return null;
      } else {
         byte[] var2;
         if (var0 == null) {
            var2 = new byte[var1.length];
            System.arraycopy(var1, 0, var2, 0, var1.length);
         } else if (var1 == null) {
            var2 = new byte[var0.length];
            System.arraycopy(var0, 0, var2, 0, var0.length);
         } else {
            var2 = new byte[var0.length + var1.length];
            System.arraycopy(var0, 0, var2, 0, var0.length);
            System.arraycopy(var1, 0, var2, var0.length, var1.length);
         }

         return var2;
      }
   }

   private void errorMessage(String var1) {
      this.logger.errorMessage("CodecJOrbis", var1, 0);
   }

   private void printStackTrace(Exception var1) {
      this.logger.printStackTrace(var1, 1);
   }
}
