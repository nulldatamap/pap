package paulscode.sound;

public class Vector3D {
   // $FF: renamed from: x float
   public float field_694;
   // $FF: renamed from: y float
   public float field_695;
   // $FF: renamed from: z float
   public float field_696;

   public Vector3D() {
      this.field_694 = 0.0F;
      this.field_695 = 0.0F;
      this.field_696 = 0.0F;
   }

   public Vector3D(float var1, float var2, float var3) {
      this.field_694 = var1;
      this.field_695 = var2;
      this.field_696 = var3;
   }

   public Vector3D clone() {
      return new Vector3D(this.field_694, this.field_695, this.field_696);
   }

   public Vector3D cross(Vector3D var1, Vector3D var2) {
      return new Vector3D(var1.field_695 * var2.field_696 - var2.field_695 * var1.field_696, var1.field_696 * var2.field_694 - var2.field_696 * var1.field_694, var1.field_694 * var2.field_695 - var2.field_694 * var1.field_695);
   }

   public Vector3D cross(Vector3D var1) {
      return new Vector3D(this.field_695 * var1.field_696 - var1.field_695 * this.field_696, this.field_696 * var1.field_694 - var1.field_696 * this.field_694, this.field_694 * var1.field_695 - var1.field_694 * this.field_695);
   }

   public float dot(Vector3D var1, Vector3D var2) {
      return var1.field_694 * var2.field_694 + var1.field_695 * var2.field_695 + var1.field_696 * var2.field_696;
   }

   public float dot(Vector3D var1) {
      return this.field_694 * var1.field_694 + this.field_695 * var1.field_695 + this.field_696 * var1.field_696;
   }

   public Vector3D add(Vector3D var1, Vector3D var2) {
      return new Vector3D(var1.field_694 + var2.field_694, var1.field_695 + var2.field_695, var1.field_696 + var2.field_696);
   }

   public Vector3D add(Vector3D var1) {
      return new Vector3D(this.field_694 + var1.field_694, this.field_695 + var1.field_695, this.field_696 + var1.field_696);
   }

   public Vector3D subtract(Vector3D var1, Vector3D var2) {
      return new Vector3D(var1.field_694 - var2.field_694, var1.field_695 - var2.field_695, var1.field_696 - var2.field_696);
   }

   public Vector3D subtract(Vector3D var1) {
      return new Vector3D(this.field_694 - var1.field_694, this.field_695 - var1.field_695, this.field_696 - var1.field_696);
   }

   public void normalize() {
      double var1 = Math.sqrt((double)(this.field_694 * this.field_694 + this.field_695 * this.field_695 + this.field_696 * this.field_696));
      this.field_694 = (float)((double)this.field_694 / var1);
      this.field_695 = (float)((double)this.field_695 / var1);
      this.field_696 = (float)((double)this.field_696 / var1);
   }
}
