package net.minecraft.game.entities.projectiles;

import a.MathHelper;
import com.data.nbt.CompoundTag;
import java.util.List;
import net.minecraft.game.world.World;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.items.Item;
import net.minecraft.game.entities.Mob;
import net.minecraft.game.entities.Entity;
import net.minecraft.game.entities.player.Player;
import net.minecraft.game.util.RaycastResult;
import net.minecraft.game.util.Vector;

// $FF: renamed from: net.minecraft.game.entities.projectiles.game
public class Arrow extends Entity {
   // $FF: renamed from: animals int
   private int field_51 = -1;
   // $FF: renamed from: O int
   private int field_52 = -1;
   // $FF: renamed from: P int
   private int field_53 = -1;
   // $FF: renamed from: Q int
   private int field_54 = 0;
   // $FF: renamed from: R boolean
   private boolean field_55 = false;
   // $FF: renamed from: game int
   public int field_56 = 0;
   // $FF: renamed from: S net.minecraft.game.entities.player
   private Mob field_57;
   // $FF: renamed from: T int
   private int field_58;
   // $FF: renamed from: U int
   private int field_59 = 0;

   public Arrow(World var1, Mob var2) {
      super(var1);
      this.field_57 = var2;
      this.set_size(0.5F, 0.5F);
      this.set_orientation(var2.pos_x, var2.pos_y, var2.pos_z, var2.yaw, var2.pitch);
      this.pos_x -= MathHelper.cos(this.yaw / 180.0F * 3.1415927F) * 0.16F;
      this.pos_y -= 0.1F;
      this.pos_z -= MathHelper.sin(this.yaw / 180.0F * 3.1415927F) * 0.16F;
      this.set_position(this.pos_x, this.pos_y, this.pos_z);
      this.camera_height = 0.0F;
      this.vel_x = -MathHelper.sin(this.yaw / 180.0F * 3.1415927F) * MathHelper.cos(this.pitch / 180.0F * 3.1415927F);
      this.vel_z = MathHelper.cos(this.yaw / 180.0F * 3.1415927F) * MathHelper.cos(this.pitch / 180.0F * 3.1415927F);
      this.vel_y = -MathHelper.sin(this.pitch / 180.0F * 3.1415927F);
      this.calculate_trajectory(this.vel_x, this.vel_y, this.vel_z, 1.5F, 1.0F);
   }

   // $FF: renamed from: game (float, float, float, float, float) void
   public final void calculate_trajectory(float var1, float var2, float var3, float var4, float var5) {
      float var6 = MathHelper.sqrt(var1 * var1 + var2 * var2 + var3 * var3);
      var1 /= var6;
      var2 /= var6;
      var3 /= var6;
      var1 = (float)((double)var1 + this.random.nextGaussian() * 0.007499999832361937D * (double)var5);
      var2 = (float)((double)var2 + this.random.nextGaussian() * 0.007499999832361937D * (double)var5);
      var3 = (float)((double)var3 + this.random.nextGaussian() * 0.007499999832361937D * (double)var5);
      var1 *= var4;
      var2 *= var4;
      var3 *= var4;
      this.vel_x = var1;
      this.vel_y = var2;
      this.vel_z = var3;
      var4 = MathHelper.sqrt(var1 * var1 + var3 * var3);
      this.prev_yaw = this.yaw = (float)(Math.atan2((double)var1, (double)var3) * 180.0D / 3.1415927410125732D);
      this.prev_pitch = this.pitch = (float)(Math.atan2((double)var2, (double)var4) * 180.0D / 3.1415927410125732D);
      this.field_58 = 0;
   }

   // $FF: renamed from: b_ () void
   public final void update_entity() {
      super.update_entity();
      if (this.field_56 > 0) {
         --this.field_56;
      }

      if (this.field_55) {
         if (this.world.block_at(this.field_51, this.field_52, this.field_53) == this.field_54) {
            ++this.field_58;
            if (this.field_58 == 1200) {
               this.destroy();
            }

            return;
         }

         this.field_55 = false;
         this.vel_x *= this.random.nextFloat() * 0.2F;
         this.vel_y *= this.random.nextFloat() * 0.2F;
         this.vel_z *= this.random.nextFloat() * 0.2F;
         this.field_58 = 0;
         this.field_59 = 0;
      } else {
         ++this.field_59;
      }

      Vector var1 = new Vector(this.pos_x, this.pos_y, this.pos_z);
      Vector var2 = new Vector(this.pos_x + this.vel_x, this.pos_y + this.vel_y, this.pos_z + this.vel_z);
      RaycastResult var3 = this.world.check_raycast(var1, var2);
      var1 = new Vector(this.pos_x, this.pos_y, this.pos_z);
      var2 = new Vector(this.pos_x + this.vel_x, this.pos_y + this.vel_y, this.pos_z + this.vel_z);
      if (var3 != null) {
         var2 = new Vector(var3.end_point.x, var3.end_point.y, var3.end_point.z);
      }

      Entity var4 = null;
      List var5 = this.world.hitbox_tree.colliding_entities(this, this.hitbox.extend_sides(this.vel_x, this.vel_y, this.vel_z).grow(1.0F, 1.0F, 1.0F));
      float var6 = 0.0F;

      for(int var7 = 0; var7 < var5.size(); ++var7) {
         Entity var8;
         RaycastResult var9;
         float var12;
         if ((var8 = (Entity)var5.get(var7)).maybe_is_visible() && (var8 != this.field_57 || this.field_59 >= 5) && (var9 = var8.hitbox.grow(0.3F, 0.3F, 0.3F).method_475(var1, var2)) != null && ((var12 = var1.distance(var9.end_point)) < var6 || var6 == 0.0F)) {
            var4 = var8;
            var6 = var12;
         }
      }

      if (var4 != null) {
         var3 = new RaycastResult(var4);
      }

      float var10;
      if (var3 != null) {
         if (var3.entity != null) {
            if (var3.entity.take_damage(this, 4)) {
               this.world.play_sound(this, "random.drr", 1.0F, 1.2F / (this.random.nextFloat() * 0.2F + 0.9F));
               this.destroy();
            } else {
               this.vel_x *= -0.1F;
               this.vel_y *= -0.1F;
               this.vel_z *= -0.1F;
               this.yaw += 180.0F;
               this.prev_yaw += 180.0F;
               this.field_59 = 0;
            }
         } else {
            this.field_51 = var3.x;
            this.field_52 = var3.y;
            this.field_53 = var3.z;
            this.field_54 = this.world.block_at(this.field_51, this.field_52, this.field_53);
            this.vel_x = var3.end_point.x - this.pos_x;
            this.vel_y = var3.end_point.y - this.pos_y;
            this.vel_z = var3.end_point.z - this.pos_z;
            var10 = MathHelper.sqrt(this.vel_x * this.vel_x + this.vel_y * this.vel_y + this.vel_z * this.vel_z);
            this.pos_x -= this.vel_x / var10 * 0.05F;
            this.pos_y -= this.vel_y / var10 * 0.05F;
            this.pos_z -= this.vel_z / var10 * 0.05F;
            this.world.play_sound(this, "random.drr", 1.0F, 1.2F / (this.random.nextFloat() * 0.2F + 0.9F));
            this.field_55 = true;
            this.field_56 = 7;
         }
      }

      this.pos_x += this.vel_x;
      this.pos_y += this.vel_y;
      this.pos_z += this.vel_z;
      var10 = MathHelper.sqrt(this.vel_x * this.vel_x + this.vel_z * this.vel_z);
      this.yaw = (float)(Math.atan2((double)this.vel_x, (double)this.vel_z) * 180.0D / 3.1415927410125732D);

      for(this.pitch = (float)(Math.atan2((double)this.vel_y, (double)var10) * 180.0D / 3.1415927410125732D); this.pitch - this.prev_pitch < -180.0F; this.prev_pitch -= 360.0F) {
         ;
      }

      while(this.pitch - this.prev_pitch >= 180.0F) {
         this.prev_pitch += 360.0F;
      }

      while(this.yaw - this.prev_yaw < -180.0F) {
         this.prev_yaw -= 360.0F;
      }

      while(this.yaw - this.prev_yaw >= 180.0F) {
         this.prev_yaw += 360.0F;
      }

      this.pitch = this.prev_pitch + (this.pitch - this.prev_pitch) * 0.2F;
      this.yaw = this.prev_yaw + (this.yaw - this.prev_yaw) * 0.2F;
      float var11 = 0.99F;
      if (this.collides_with_water()) {
         for(int var13 = 0; var13 < 4; ++var13) {
            this.world.spawn_effect("bubble", this.pos_x - this.vel_x * 0.25F, this.pos_y - this.vel_y * 0.25F, this.pos_z - this.vel_z * 0.25F, this.vel_x, this.vel_y, this.vel_z);
         }

         var11 = 0.8F;
      }

      this.vel_x *= var11;
      this.vel_y *= var11;
      this.vel_z *= var11;
      this.vel_y -= 0.03F;
      this.set_position(this.pos_x, this.pos_y, this.pos_z);
   }

   // $FF: renamed from: game (com.game.game.entities) void
   protected final void write_tag_extra(CompoundTag var1) {
      var1.write_short("xTile", (short)this.field_51);
      var1.write_short("yTile", (short)this.field_52);
      var1.write_short("zTile", (short)this.field_53);
      var1.write_byte("inTile", (byte)this.field_54);
      var1.write_byte("shake", (byte)this.field_56);
      var1.write_byte("inGround", (byte)(this.field_55 ? 1 : 0));
   }

   // $FF: renamed from: animals (com.game.game.entities) void
   protected final void read_tag_extra(CompoundTag var1) {
      this.field_51 = var1.read_short("xTile");
      this.field_52 = var1.read_short("yTile");
      this.field_53 = var1.read_short("zTile");
      this.field_54 = var1.read_byte("inTile") & 255;
      this.field_56 = var1.read_byte("shake") & 255;
      this.field_55 = var1.read_byte("inGround") == 1;
   }

   // $FF: renamed from: game () java.lang.String
   protected final String get_id() {
      return "Arrow";
   }

   // $FF: renamed from: game (net.minecraft.game.entities.player.game) void
   public final void contact_player(Player var1) {
      if (this.field_55 && this.field_57 == var1 && this.field_56 <= 0 && var1.inventory.add_item(new ItemStack(Item.bone.unique_id, 1))) {
         this.world.play_sound(this, "random.pop", 0.2F, ((this.random.nextFloat() - this.random.nextFloat()) * 0.7F + 1.0F) * 2.0F);
         var1.method_76(this);
         this.destroy();
      }

   }

   // $FF: renamed from: c_ () float
   public final float half_height() {
      return 0.0F;
   }
}
