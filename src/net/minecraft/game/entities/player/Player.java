package net.minecraft.game.entities.player;

import a.MathHelper;
import com.data.nbt.CompoundTag;
import java.util.List;
import net.minecraft.game.Storage;
import net.minecraft.game.world.World;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.world.block.a.FurnaceData;
import net.minecraft.game.world.material.Material;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.items.Item;
import net.minecraft.game.items.Armor;
import net.minecraft.game.entities.Mob;
import net.minecraft.game.entities.Entity;
import net.minecraft.game.entities.monsters.Monster;
import net.minecraft.game.entities.misc.ItemDrop;
import net.minecraft.game.entities.projectiles.Arrow;

// $FF: renamed from: net.minecraft.game.entities.player.game
public class Player extends Mob {
   // $FF: renamed from: animals net.minecraft.game.entities.player.animals
   public Inventory inventory = new Inventory(this);
   // $FF: renamed from: O byte
   public byte field_109 = 0;
   // $FF: renamed from: P int
   public int score = 0;
   // $FF: renamed from: Q float
   public float prev_head_bob_roll;
   // $FF: renamed from: R float
   public float head_bob_roll;
   // $FF: renamed from: game int
   private int leftover_partial_damage = 0;

   public Player(World w) {
      super(w);
      if (w != null) {
         w.player = this;
         w.somehow_initialize_the_player_in_the_wordl(this);
      }

      this.set_orientation((float)w.spawn_x, (float)w.spawn_y, (float)w.spawn_z, 0.0F, 0.0F);
      this.camera_height = 1.62F;
      this.health = 20;
      this.max_health = 20;
      this.skin_file = "/char.png";
   }

   // $FF: renamed from: j () void
   public final void spawn_entity() {
      this.camera_height = 1.62F;
      this.set_size(0.6F, 1.8F);
      super.spawn_entity();
      if (this.world != null) {
         this.world.player = this;
      }

      this.health = this.max_health;
      this.death_time = 0;
   }

   // $FF: renamed from: particles () void
   public void update_movement() {
      this.world.play_music(this.pos_x, this.pos_y, this.pos_z, "calm", 0.0F);
      if (this.world.difficulty == 0 && this.health < this.max_health && this.alive_tick % 20 << 2 == 0) {
         this.heal(1);
      }

      Inventory inv = this.inventory;

      for(int i = 0; i < inv.main_area.length; ++i) {
         if (inv.main_area[i] != null && inv.main_area[i].pickup_cooldown > 0) {
            --inv.main_area[i].pickup_cooldown;
         }
      }

      this.prev_head_bob_roll = this.head_bob_roll;
      super.update_movement();
      float speed = MathHelper.sqrt(this.vel_x * this.vel_x + this.vel_z * this.vel_z);
      float fall_skew = (float)Math.atan((double)(-this.vel_y * 0.2F)) * 15.0F;
      if (speed > 0.1F) {
         speed = 0.1F;
      }

      if (!this.maybe_grounded || this.health <= 0) {
         speed = 0.0F;
      }

      if (this.maybe_grounded || this.health <= 0) {
         fall_skew = 0.0F;
      }

      this.head_bob_roll += (speed - this.head_bob_roll) * 0.4F;
      this.head_bob_pitch += (fall_skew - this.head_bob_pitch) * 0.8F;
      List next_to = this.world.colliding_entities(this, this.hitbox.grow(1.0F, 0.0F, 1.0F));
      if (this.health > 0 && next_to != null) {
         for(int i = 0; i < next_to.size(); ++i) {
            Entity other = (Entity)next_to.get(i);
            other.contact_player(this);
         }
      }

   }

   // $FF: renamed from: projectiles (net.minecraft.game.entities.animals) void
   public final void on_death(Entity killer) {
      this.set_size(0.2F, 0.2F);
      this.set_position(this.pos_x, this.pos_y, this.pos_z);
      this.vel_y = 0.1F;
      if (killer != null) {
         this.vel_x = -MathHelper.cos((this.hurt_yaw + this.yaw) * 3.1415927F / 180.0F) * 0.1F;
         this.vel_z = -MathHelper.sin((this.hurt_yaw + this.yaw) * 3.1415927F / 180.0F) * 0.1F;
      } else {
         this.vel_x = this.vel_z = 0.0F;
      }

      this.camera_height = 0.1F;
   }

   // $FF: renamed from: k () void
   public final void destroy() {
   }

   // $FF: renamed from: game (net.minecraft.game.animals.vel_x) void
   public final void drop_items(ItemStack var1) {
      this.drop_items(var1, false);
   }

   // $FF: renamed from: game (net.minecraft.game.animals.vel_x, boolean) void
   public final void drop_items(ItemStack item, boolean var2) {
      if (item != null) {
         ItemDrop drop;
         (drop = new ItemDrop(this.world, this.pos_x, this.pos_y - 0.3F, this.pos_z, item)).pickup_cooldown = 40;
         drop.vel_x = -MathHelper.sin(this.yaw / 180.0F * 3.1415927F) * MathHelper.cos(this.pitch / 180.0F * 3.1415927F) * 0.3F;
         drop.vel_z = MathHelper.cos(this.yaw / 180.0F * 3.1415927F) * MathHelper.cos(this.pitch / 180.0F * 3.1415927F) * 0.3F;
         drop.vel_y = -MathHelper.sin(this.pitch / 180.0F * 3.1415927F) * 0.3F + 0.1F;
         float var3 = this.random.nextFloat() * 3.1415927F * 2.0F;
         float var5 = 0.02F * this.random.nextFloat();
         drop.vel_x = (float)((double)drop.vel_x + Math.cos((double)var3) * (double)var5);
         drop.vel_y += (this.random.nextFloat() - this.random.nextFloat()) * 0.1F;
         drop.vel_z = (float)((double)drop.vel_z + Math.sin((double)var3) * (double)var5);
         this.world.add_entity(drop);
      }
   }

   // $FF: renamed from: game (net.minecraft.game.game.animals.x) boolean
   public final boolean method_73(Block block) {
      Inventory inv = this.inventory;
      if (block.material != Material.stone_material && block.material != Material.field_612) {
         return true;
      } else {
         ItemStack var4;
         return (var4 = inv.get_slot(inv.selected_slot)) != null ? Item.items[var4.id].place_block(block) : false;
      }
   }

   // $FF: renamed from: animals (com.game.game.entities) void
   protected void read_tag_extra(CompoundTag var1) {
      super.read_tag_extra(var1);
   }

   // $FF: renamed from: game (com.game.game.entities) void
   protected void write_tag_extra(CompoundTag var1) {
      super.write_tag_extra(var1);
   }

   // $FF: renamed from: game () java.lang.String
   protected String get_id() {
      return null;
   }

   // $FF: renamed from: game (net.minecraft.game.animals) void
   public void method_74(Storage var1) {
   }

   // $FF: renamed from: g_ () void
   public void method_75() {
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals) void
   public void method_76(Entity var1) {
   }

   // $FF: renamed from: n () float
   protected final float head_offset() {
      return 0.12F;
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, int) boolean
   public final boolean take_damage(Entity other, int amount) {
      if (!this.world.can_take_damage) {
         return false;
      } else {
         this.despawn_tick = 0;
         if (this.health <= 0) {
            return false;
         } else if ((float)this.invincible_frames > (float)this.something_related_to_invincibility / 2.0F) {
            return false;
         } else {
            // Scale enemy damage (but not environmental damage) by difficulty
            if (other instanceof Monster || other instanceof Arrow) {
               if (this.world.difficulty == 0) {
                  amount = 0;
               }

               if (this.world.difficulty == 1) {
                  amount = amount / 3 + 1;
               }

               if (this.world.difficulty == 3) {
                  amount = amount * 3 / 2;
               }
            }

            // Calculate and distribute damage on armor items
            int inverse_armor = 25 - this.inventory.calculate_armor();
            int resisted_damage_factor = amount * inverse_armor + this.leftover_partial_damage;
            Inventory inv = this.inventory;

            for(int i = 0; i < inv.armor_area.length; ++i) {
               if (inv.armor_area[i] != null && inv.armor_area[i].item() instanceof Armor) {
                  inv.armor_area[i].take_damage(amount);
                  if (inv.armor_area[i].count == 0) {
                     inv.armor_area[i] = null;
                  }
               }
            }

            amount = resisted_damage_factor / 25;
            this.leftover_partial_damage = resisted_damage_factor % 25;
            if (amount == 0) {
               return false;
            } else {
               return super.take_damage(other, amount);
            }
         }
      }
   }

   // $FF: renamed from: game (net.minecraft.game.game.animals.game.animals) void
   public void method_77(FurnaceData var1) {
   }
}
