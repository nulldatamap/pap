package net.minecraft.game.entities.player;

import net.minecraft.game.Storage;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.items.Armor;

// $FF: renamed from: net.minecraft.game.entities.player.animals
public final class Inventory implements Storage {
   // $FF: renamed from: game net.minecraft.game.animals.k[]
   public ItemStack[] main_area = new ItemStack[36];
   // $FF: renamed from: animals net.minecraft.game.animals.k[]
   public ItemStack[] armor_area = new ItemStack[4];
   // $FF: renamed from: entities int
   public int selected_slot = 0;
   // $FF: renamed from: projectiles net.minecraft.game.entities.player.game
   private Player owner;

   public Inventory(Player var1) {
      this.owner = var1;
   }

   // $FF: renamed from: projectiles () net.minecraft.game.animals.k
   public final ItemStack selected_item() {
      return this.main_area[this.selected_slot];
   }

   // $FF: renamed from: projectiles (int) int
   private int index_of_item(int item_id) {
      for(int i = 0; i < this.main_area.length; ++i) {
         if (this.main_area[i] != null && this.main_area[i].id == item_id) {
            return i;
         }
      }

      return -1;
   }

   // $FF: renamed from: particles () int
   private int free_slot() {
      for(int i = 0; i < this.main_area.length; ++i) {
         if (this.main_area[i] == null) {
            return i;
         }
      }

      return -1;
   }

   // $FF: renamed from: animals (int) void
   public final void select_item(int item_id) {
      item_id = this.index_of_item(item_id);
      if (item_id >= 0 && item_id < 9) {
         this.selected_slot = item_id;
      }
   }

   // $FF: renamed from: entities (int) boolean
   public final boolean remove_one_of(int item_id) {
      int idx = this.index_of_item(item_id);
      if (idx < 0) {
         return false;
      } else {
         if (--this.main_area[idx].count <= 0) {
            this.main_area[idx] = null;
         }

         return true;
      }
   }

   // $FF: renamed from: game (net.minecraft.game.animals.k) boolean
   public final boolean add_item(ItemStack item) {
      if (item.damage == 0) {
         int count = item.count;
         int id = item.id;
         int var6 = id;
         Inventory inv = this;
         int var7 = 0;

         int var10001;
         while(true) {
            if (var7 >= inv.main_area.length) {
               var10001 = -1;
               break;
            }

            if (inv.main_area[var7] != null && inv.main_area[var7].id == var6 && inv.main_area[var7].count < inv.main_area[var7].item().get_stack_size() && inv.main_area[var7].count < 64) {
               var10001 = var7;
               break;
            }

            ++var7;
         }

         int var9 = var10001;
         if (var10001 < 0) {
            var9 = this.free_slot();
         }

         if (var9 < 0) {
            var10001 = count;
         } else {
            if (this.main_area[var9] == null) {
               this.main_area[var9] = new ItemStack(id, 0);
            }

            id = count;
            if (count > this.main_area[var9].item().get_stack_size() - this.main_area[var9].count) {
               id = this.main_area[var9].item().get_stack_size() - this.main_area[var9].count;
            }

            if (id > 64 - this.main_area[var9].count) {
               id = 64 - this.main_area[var9].count;
            }

            if (id == 0) {
               var10001 = count;
            } else {
               count -= id;
               this.main_area[var9].count += id;
               this.main_area[var9].pickup_cooldown = 5;
               var10001 = count;
            }
         }

         item.count = var10001;
         if (item.count == 0) {
            return true;
         }
      }

      int var2;
      if ((var2 = this.free_slot()) >= 0) {
         this.main_area[var2] = item;
         this.main_area[var2].pickup_cooldown = 5;
         return true;
      } else {
         return false;
      }
   }

   // $FF: renamed from: game (int, int) net.minecraft.game.animals.k
   public final ItemStack take_from(int index, int count) {
      ItemStack[] area = this.main_area;
      if (index >= this.main_area.length) {
         area = this.armor_area;
         index -= this.main_area.length;
      }

      if (area[index] != null) {
         ItemStack item;
         if (area[index].count <= count) {
            item = area[index];
            area[index] = null;
            return item;
         } else {
            item = area[index].split_stack(count);
            if (area[index].count == 0) {
               area[index] = null;
            }

            return item;
         }
      } else {
         return null;
      }
   }

   // $FF: renamed from: game (int, net.minecraft.game.animals.k) void
   public final void set_slot(int index, ItemStack item) {
      ItemStack[] area = this.main_area;
      if (index >= this.main_area.length) {
         area = this.armor_area;
         index -= this.main_area.length;
      }

      area[index] = item;
   }

   // $FF: renamed from: game () int
   public final int capacity() {
      return this.main_area.length + 4;
   }

   // $FF: renamed from: game (int) net.minecraft.game.animals.k
   public final ItemStack get_slot(int index) {
      ItemStack[] area = this.main_area;
      if (index >= this.main_area.length) {
         area = this.armor_area;
         index -= this.main_area.length;
      }

      return area[index];
   }

   // $FF: renamed from: animals () java.lang.String
   public final String nbtName() {
      return "Inventory";
   }

   // $FF: renamed from: entities () int
   public final int max_stack_size() {
      return 64;
   }

   // $FF: renamed from: player () int
   public final int calculate_armor() {
      int var1 = 0;
      int var2 = 0;
      int var3 = 0;

      for(int i = 0; i < this.armor_area.length; ++i) {
         if (this.armor_area[i] != null && this.armor_area[i].item() instanceof Armor) {
            int durability = this.armor_area[i].durability();
            int damage = this.armor_area[i].damage;
            damage = durability - damage;
            var2 += damage;
            var3 += durability;
            int d = ((Armor)this.armor_area[i].item()).field_216;
            var1 += d;
         }
      }

      if (var3 == 0) {
         return 0;
      } else {
         return (var1 - 1) * var2 / var3 + 1;
      }
   }
}
