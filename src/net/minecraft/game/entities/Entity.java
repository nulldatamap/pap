package net.minecraft.game.entities;

import a.MathHelper;
import com.data.nbt.ListTag;
import com.data.nbt.CompoundTag;
import com.data.nbt.FloatTag;
import java.util.ArrayList;
import java.util.Random;
import net.minecraft.game.world.World;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.world.block.SoundMaterial;
import net.minecraft.game.world.material.Material;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.entities.misc.ItemDrop;
import net.minecraft.game.entities.player.Player;
import net.minecraft.game.util.AABB;

// $FF: renamed from: net.minecraft.game.entities.animals
public abstract class Entity {
   // $FF: renamed from: entities boolean
   public boolean maybe_solid = false;
   // $FF: renamed from: projectiles net.minecraft.game.game.player
   protected World world;
   // $FF: renamed from: player float
   public float prev_x;
   // $FF: renamed from: particles float
   public float prev_y;
   // $FF: renamed from: player float
   public float prev_z;
   // $FF: renamed from: h float
   public float pos_x;
   // $FF: renamed from: i float
   public float pos_y;
   // $FF: renamed from: j float
   public float pos_z;
   // $FF: renamed from: k float
   public float vel_x;
   // $FF: renamed from: l float
   public float vel_y;
   // $FF: renamed from: m float
   public float vel_z;
   // $FF: renamed from: n float
   public float yaw;
   // $FF: renamed from: o float
   public float pitch;
   // $FF: renamed from: p float
   public float prev_yaw;
   // $FF: renamed from: q float
   public float prev_pitch;
   // $FF: renamed from: r net.minecraft.game.projectiles.animals
   public AABB hitbox;
   // $FF: renamed from: s boolean
   public boolean maybe_grounded = false;
   // $FF: renamed from: t boolean
   public boolean field_17 = false;
   // $FF: renamed from: game boolean
   private boolean field_18 = true;
   // $FF: renamed from: u boolean
   public boolean destroyed = false;
   // $FF: renamed from: v float
   public float camera_height = 0.0F;
   // $FF: renamed from: w float
   public float width = 0.6F;
   // $FF: renamed from: x float
   public float height = 1.8F;
   // $FF: renamed from: y float
   public float field_23 = 0.0F;
   // $FF: renamed from: z float
   public float field_24 = 0.0F;
   // $FF: renamed from: A boolean
   protected boolean field_25 = true;
   // $FF: renamed from: animals float
   private float fall_distance = 0.0F;
   // $FF: renamed from: O int
   private int field_27 = 1;
   // $FF: renamed from: B float
   public float chunk_x;
   // $FF: renamed from: C float
   public float chunk_y;
   // $FF: renamed from: D float
   public float chunk_z;
   // $FF: renamed from: P float
   private float field_31 = 0.0F;
   // $FF: renamed from: E float
   public float field_32 = 0.0F;
   // $FF: renamed from: F boolean
   public boolean field_33 = false;
   // $FF: renamed from: Q float
   private float field_34 = 0.0F;
   // $FF: renamed from: G java.util.Random
   protected Random random = new Random();
   // $FF: renamed from: H int
   public int alive_tick = 0;
   // $FF: renamed from: I int
   public int max_health = 1;
   // $FF: renamed from: J int
   public int burn_time = 0;
   // $FF: renamed from: K int
   protected int max_air = 300;
   // $FF: renamed from: R boolean
   private boolean in_water = false;
   // $FF: renamed from: L int
   public int invincible_frames = 0;
   // $FF: renamed from: M int
   public int air = 300;
   // $FF: renamed from: S boolean
   private boolean field_43 = true;
   // $FF: renamed from: N java.lang.String
   public String texture_url;

   public Entity(World var1) {
      this.world = var1;
      this.set_position(0.0F, 0.0F, 0.0F);
   }

   // $FF: renamed from: j () void
   protected void spawn_entity() {
      if (this.world != null) {
         // Place the player in the middle of the spawn block
         float x = (float)this.world.spawn_x + 0.5F;
         float y = (float)this.world.spawn_y;
         float z = (float)this.world.spawn_z + 0.5F;

         // And search upwards until they're not colliding with any blocks
         for(; y > 0.0F; ++y) {
            this.set_position(x, y, z);
            if (this.world.get_colliding_aabbs(this.hitbox).size() == 0) {
               break;
            }
         }

         this.vel_x = this.vel_y = this.vel_z = 0.0F;
         this.yaw = this.world.spawn_yaw;
         this.pitch = 0.0F;
      }
   }

   // $FF: renamed from: k () void
   public void destroy() {
      this.destroyed = true;
   }

   // $FF: renamed from: game (float, float) void
   protected void set_size(float var1, float var2) {
      this.width = var1;
      this.height = var2;
   }

   // $FF: renamed from: animals (float, float, float) void
   protected final void set_position(float var1, float var2, float var3) {
      this.pos_x = var1;
      this.pos_y = var2;
      this.pos_z = var3;
      float var4 = this.width / 2.0F;
      float var5 = this.height / 2.0F;
      this.hitbox = new AABB(var1 - var4, var2 - var5, var3 - var4, var1 + var4, var2 + var5, var3 + var4);
   }

   // $FF: renamed from: b_ () void
   public void update_entity() {
      ++this.alive_tick;
      this.field_23 = this.field_24;
      this.prev_x = this.pos_x;
      this.prev_y = this.pos_y;
      this.prev_z = this.pos_z;
      this.prev_pitch = this.pitch;
      this.prev_yaw = this.yaw;

      if (this.collides_with_water()) {
         // Water splashing logic:
         if (!this.in_water && !this.field_43) {
            float splash_loudness = MathHelper.sqrt(this.vel_x * this.vel_x * 0.2F
                                                   + this.vel_y * this.vel_y
                                                   + this.vel_z * this.vel_z * 0.2F) * 0.2F;
            if (splash_loudness > 1.0F) {
               splash_loudness = 1.0F;
            }

            this.world.play_sound(this, "random.splash", splash_loudness, 1.0F + (this.random.nextFloat() - this.random.nextFloat()) * 0.4F);
            splash_loudness = (float)((int)this.hitbox.min_y);

            int var2;
            float var3;
            float var4;
            for(var2 = 0; (float)var2 < 1.0F + this.width * 20.0F; ++var2) {
               var3 = (this.random.nextFloat() * 2.0F - 1.0F) * this.width;
               var4 = (this.random.nextFloat() * 2.0F - 1.0F) * this.width;
               this.world.spawn_effect("bubble", this.pos_x + var3, splash_loudness + 1.0F, this.pos_z + var4, this.vel_x, this.vel_y - this.random.nextFloat() * 0.2F, this.vel_z);
            }

            for(var2 = 0; (float)var2 < 1.0F + this.width * 20.0F; ++var2) {
               var3 = (this.random.nextFloat() * 2.0F - 1.0F) * this.width;
               var4 = (this.random.nextFloat() * 2.0F - 1.0F) * this.width;
               this.world.spawn_effect("splash", this.pos_x + var3, splash_loudness + 1.0F, this.pos_z + var4, this.vel_x, this.vel_y, this.vel_z);
            }
         }

         this.fall_distance = 0.0F;
         this.in_water = true;
         this.burn_time = 0;
      } else {
         this.in_water = false;
      }

      if (this.burn_time > 0) {
         if (this.burn_time % 20 == 0) {
            this.take_damage((Entity)null, 1);
         }

         --this.burn_time;
      }

      if (this.collides_with_lava()) {
         this.take_damage((Entity)null, 10);
         this.burn_time = 600;
      }

      // Limit the player's speed greatly once they're 8 blocks outside the world bounds

      float var5;
      if (this.pos_x < -8.0F) {
         var5 = -(this.pos_x + 8.0F);
         this.vel_x += var5 * 0.001F;
      }

      if (this.pos_z < -8.0F) {
         var5 = -(this.pos_z + 8.0F);
         this.vel_z += var5 * 0.001F;
      }

      if (this.pos_x > (float)this.world.width + 8.0F) {
         var5 = this.pos_x - (float)this.world.width + 8.0F;
         this.vel_x -= var5 * 0.001F;
      }

      if (this.pos_z > (float)this.world.depth + 8.0F) {
         var5 = this.pos_z - (float)this.world.depth + 8.0F;
         this.vel_z -= var5 * 0.001F;
      }

      this.field_43 = false;
   }

   // $FF: renamed from: entities (float, float, float) boolean
   public final boolean method_24(float var1, float var2, float var3) {
      float var4 = var3;
      var3 = var2;
      var2 = var1;
      AABB var5 = this.hitbox;
      var5 = new AABB(var5.min_x + var4, var5.min_y + var3, var5.min_z + var4, var5.max_x + var2, var5.max_y + var3, var5.max_z + var4);
      if (this.world.get_colliding_aabbs(var5).size() > 0) {
         return false;
      } else {
         return !this.world.method_183(var5);
      }
   }

   // $FF: renamed from: projectiles (float, float, float) void
   public final void handle_collision(float x, float y, float z) {
      if (this.field_33) {
         this.hitbox.offset(x, y, z);
         this.pos_x = (this.hitbox.min_x + this.hitbox.max_x) / 2.0F;
         this.pos_y = this.hitbox.min_y + this.camera_height - this.field_31;
         this.pos_z = (this.hitbox.min_z + this.hitbox.max_z) / 2.0F;
      } else {
         float px = this.pos_x;
         float pz = this.pos_z;
         float ax = x;
         float ay = y;
         float az = z;
         AABB hb = this.hitbox.copy();
         ArrayList collisions = this.world.get_colliding_aabbs(this.hitbox.extend_sides(x, y, z));

         for(int i = 0; i < collisions.size(); ++i) {
            y = ((AABB)collisions.get(i)).method_470(this.hitbox, y);
         }

         this.hitbox.offset(0.0F, y, 0.0F);
         if (!this.field_18 && ay != y) {
            z = 0.0F;
            y = 0.0F;
            x = 0.0F;
         }

         boolean var18 = this.maybe_grounded || ay != y && ay < 0.0F;

         int var12;
         for(var12 = 0; var12 < collisions.size(); ++var12) {
            x = ((AABB)collisions.get(var12)).method_469(this.hitbox, x);
         }

         this.hitbox.offset(x, 0.0F, 0.0F);
         if (!this.field_18 && ax != x) {
            z = 0.0F;
            y = 0.0F;
            x = 0.0F;
         }

         for(var12 = 0; var12 < collisions.size(); ++var12) {
            z = ((AABB)collisions.get(var12)).method_471(this.hitbox, z);
         }

         this.hitbox.offset(0.0F, 0.0F, z);
         if (!this.field_18 && az != z) {
            z = 0.0F;
            y = 0.0F;
            x = 0.0F;
         }

         int var17;
         float var19;
         float var20;
         if (this.field_32 > 0.0F && var18 && this.field_31 < 0.05F && (ax != x || az != z)) {
            var20 = x;
            var19 = y;
            float var13 = z;
            x = ax;
            y = this.field_32;
            z = az;
            AABB var14 = this.hitbox.copy();
            this.hitbox = hb.copy();
            collisions = this.world.get_colliding_aabbs(this.hitbox.extend_sides(ax, y, az));

            for(var17 = 0; var17 < collisions.size(); ++var17) {
               y = ((AABB)collisions.get(var17)).method_470(this.hitbox, y);
            }

            this.hitbox.offset(0.0F, y, 0.0F);
            if (!this.field_18 && ay != y) {
               z = 0.0F;
               y = 0.0F;
               x = 0.0F;
            }

            for(var17 = 0; var17 < collisions.size(); ++var17) {
               x = ((AABB)collisions.get(var17)).method_469(this.hitbox, x);
            }

            this.hitbox.offset(x, 0.0F, 0.0F);
            if (!this.field_18 && ax != x) {
               z = 0.0F;
               y = 0.0F;
               x = 0.0F;
            }

            for(var17 = 0; var17 < collisions.size(); ++var17) {
               z = ((AABB)collisions.get(var17)).method_471(this.hitbox, z);
            }

            this.hitbox.offset(0.0F, 0.0F, z);
            if (!this.field_18 && az != z) {
               z = 0.0F;
               y = 0.0F;
               x = 0.0F;
            }

            if (var20 * var20 + var13 * var13 >= x * x + z * z) {
               x = var20;
               y = var19;
               z = var13;
               this.hitbox = var14.copy();
            } else {
               this.field_31 = (float)((double)this.field_31 + 0.5D);
            }
         }

         this.pos_x = (this.hitbox.min_x + this.hitbox.max_x) / 2.0F;
         this.pos_y = this.hitbox.min_y + this.camera_height - this.field_31;
         this.pos_z = (this.hitbox.min_z + this.hitbox.max_z) / 2.0F;
         this.field_17 = ax != x || az != z;
         this.maybe_grounded = ay != y && ay < 0.0F;
         if (this.maybe_grounded) {
            if (this.fall_distance > 0.0F) {
               this.take_fall_damage(this.fall_distance);
               this.fall_distance = 0.0F;
            }
         } else if (y < 0.0F) {
            this.fall_distance -= y;
         }

         if (ax != x) {
            this.vel_x = 0.0F;
         }

         if (ay != y) {
            this.vel_y = 0.0F;
         }

         if (az != z) {
            this.vel_z = 0.0F;
         }

         var20 = this.pos_x - px;
         var19 = this.pos_z - pz;
         this.field_24 = (float)((double)this.field_24 + (double) MathHelper.sqrt(var20 * var20 + var19 * var19) * 0.6D);
         if (this.field_25) {
            int var21 = (int)this.pos_x;
            int var23 = (int)(this.pos_y - 0.2F - this.camera_height);
            var17 = (int)this.pos_z;
            int var16 = this.world.block_at(var21, var23, var17);
            if (this.field_24 > (float)this.field_27 && var16 > 0) {
               ++this.field_27;
               SoundMaterial var15 = Block.blocks[var16].sound_material;
               if (!Block.blocks[var16].material.method_343()) {
                  this.world.play_sound(this, var15.step_sound(), var15.field_958 * 0.15F, var15.field_959);
               }

               Block.blocks[var16].method_526(this.world, var21, var23, var17);
            }
         }

         this.field_31 *= 0.4F;
         boolean var22 = this.collides_with_water();
         if (this.world.method_184(this.hitbox)) {
            this.take_one_damage(1);
            if (!var22) {
               ++this.burn_time;
               if (this.burn_time == 0) {
                  this.burn_time = 300;
               }
            }
         } else if (this.burn_time <= 0) {
            this.burn_time = -this.max_health;
         }

         if (var22 && this.burn_time > 0) {
            this.world.play_sound(this, "random.fizz", 0.7F, 1.6F + (this.random.nextFloat() - this.random.nextFloat()) * 0.4F);
            this.burn_time = -this.max_health;
         }

      }
   }

   // $FF: renamed from: game (int) void
   protected void take_one_damage(int var1) {
      this.take_damage((Entity)null, 1);
   }

   // $FF: renamed from: animals (float) void
   protected void take_fall_damage(float var1) {
   }

   // $FF: renamed from: l () boolean
   public final boolean collides_with_water() {
      return this.world.collides_with_fluid(this.hitbox.grow(0.0F, -0.4F, 0.0F), Material.water_block);
   }

   // $FF: renamed from: m () boolean
   public final boolean on_water_block() {
      int block_id = this.world.block_at((int)this.pos_x, (int)(this.pos_y + this.head_offset()), (int)this.pos_z);
      if (block_id != 0) {
         return Block.blocks[block_id].material == Material.water_block;
      } else {
         return false;
      }
   }

   // $FF: renamed from: n () float
   protected float head_offset() {
      return 0.0F;
   }

   // $FF: renamed from: o () boolean
   public final boolean collides_with_lava() {
      return this.world.collides_with_fluid(this.hitbox.grow(0.0F, -0.4F, 0.0F), Material.lava_block);
   }

   // $FF: renamed from: player (float, float, float) void
   public final void move_forwards(float x, float z, float speed) {
      float vel = MathHelper.sqrt(x * x + z * z);
      if (vel >= 0.01F) {
         if (vel < 1.0F) {
            vel = 1.0F;
         }

         vel = speed / vel;
         x *= vel;
         z *= vel;
         speed = MathHelper.sin(this.yaw * 3.1415927F / 180.0F);
         vel = MathHelper.cos(this.yaw * 3.1415927F / 180.0F);
         this.vel_x += x * vel - z * speed;
         this.vel_z += z * vel + x * speed;
      }
   }

   // $FF: renamed from: game (float) float
   public float light_level(float var1) {
      int var4 = (int)this.pos_x;
      int var2 = (int)(this.pos_y + this.camera_height / 2.0F);
      int var3 = (int)this.pos_z;
      return this.world.light_at(var4, var2, var3);
   }

   // $FF: renamed from: game (net.minecraft.game.game.player) void
   public final void set_world(World var1) {
      this.world = var1;
   }

   // $FF: renamed from: animals (float, float, float, float, float) void
   public final void set_orientation(float var1, float var2, float var3, float var4, float var5) {
      this.prev_x = this.pos_x = var1;
      this.prev_y = this.pos_y = var2 + this.camera_height;
      this.prev_z = this.pos_z = var3;
      this.yaw = var4;
      this.pitch = var5;
      this.set_position(this.pos_x, this.pos_y, this.pos_z);
   }

   // $FF: renamed from: animals (net.minecraft.game.entities.animals) float
   public final float distance_squared(Entity var1) {
      float var2 = this.pos_x - var1.pos_x;
      float var3 = this.pos_y - var1.pos_y;
      float var4 = this.pos_z - var1.pos_z;
      return var2 * var2 + var3 * var3 + var4 * var4;
   }

   // $FF: renamed from: game (net.minecraft.game.entities.player.game) void
   public void contact_player(Player var1) {
   }

   // $FF: renamed from: entities (net.minecraft.game.entities.animals) void
   public final void eject_from_entity(Entity other) {
      float dx = other.pos_x - this.pos_x;
      float dz = other.pos_z - this.pos_z;
      float dist = dx * dx + dz * dz;
      if (dist >= 0.01F) {
         dist = MathHelper.sqrt(dist);
         dx /= dist;
         dz /= dist;
         dx /= dist;
         dz /= dist;
         dx *= 0.05F;
         dz *= 0.05F;
         this.accelerate(-dx, 0.0F, -dz);
         other.accelerate(dx, 0.0F, dz);
      }

   }

   // $FF: renamed from: game (float, float, float) void
   private void accelerate(float ax, float ay, float az) {
      this.vel_x += ax;
      this.vel_y = this.vel_y;
      this.vel_z += az;
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, int) boolean
   public boolean take_damage(Entity var1, int var2) {
      return false;
   }

   // $FF: renamed from: projectiles () boolean
   public boolean maybe_is_visible() {
      return false;
   }

   // $FF: renamed from: p () boolean
   public boolean is_ejecting() {
      return false;
   }

   // $FF: renamed from: q () java.lang.String
   public String maybe_texture_name() {
      return null;
   }

   // $FF: renamed from: entities (com.game.game.entities) void
   public final void write_to_tag(CompoundTag tag) {
      String id = this.get_id();
      if (!this.destroyed && id != null) {
         tag.write_string("id", id);
         tag.write_list("Pos", float_list_tag(this.pos_x, this.pos_y, this.pos_z));
         tag.write_list("Motion", float_list_tag(this.vel_x, this.vel_y, this.vel_z));
         tag.write_list("Rotation", float_list_tag(this.yaw, this.pitch));
         tag.write_float("FallDistance", this.fall_distance);
         tag.write_short("Fire", (short)this.burn_time);
         tag.write_short("Air", (short)this.air);
         this.write_tag_extra(tag);
      }
   }

   // $FF: renamed from: projectiles (com.game.game.entities) void
   public final void read_tag(CompoundTag var1) {
      ListTag pos = var1.read_list("Pos");
      ListTag vel = var1.read_list("Motion");
      ListTag rot = var1.read_list("Rotation");
      this.pos_x = ((FloatTag)pos.get(0)).value;
      this.pos_y = ((FloatTag)pos.get(1)).value;
      this.pos_z = ((FloatTag)pos.get(2)).value;
      this.vel_x = ((FloatTag)vel.get(0)).value;
      this.vel_y = ((FloatTag)vel.get(1)).value;
      this.vel_z = ((FloatTag)vel.get(2)).value;
      this.yaw = ((FloatTag)rot.get(0)).value;
      this.pitch = ((FloatTag)rot.get(1)).value;
      this.fall_distance = var1.method_454("FallDistance");
      this.burn_time = var1.read_short("Fire");
      this.air = var1.read_short("Air");
      this.set_orientation(this.pos_x, this.pos_y, this.pos_z, this.yaw, this.pitch);
      this.read_tag_extra(var1);
   }

   // $FF: renamed from: game () java.lang.String
   protected abstract String get_id();

   // $FF: renamed from: animals (com.game.game.entities) void
   protected abstract void read_tag_extra(CompoundTag var1);

   // $FF: renamed from: game (com.game.game.entities) void
   protected abstract void write_tag_extra(CompoundTag var1);

   // $FF: renamed from: game (float[]) com.game.game.particles
   private static ListTag float_list_tag(float... var0) {
      ListTag var1 = new ListTag();
      int var2 = (var0 = var0).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         float var4 = var0[var3];
         var1.method_436(new FloatTag(var4));
      }

      return var1;
   }

   // $FF: renamed from: c_ () float
   public float half_height() {
      return this.height / 2.0F;
   }

   // $FF: renamed from: game (int, int) net.minecraft.game.entities.entities.animals
   public final ItemDrop drop_item(int id, int _unused) {
      return this.drop_item(id, 1, 0.0F);
   }

   // $FF: renamed from: game (int, int, float) net.minecraft.game.entities.entities.animals
   public final ItemDrop drop_item(int id, int count, float speed) {
      ItemDrop drop = new ItemDrop(this.world, this.pos_x, this.pos_y + speed, this.pos_z, new ItemStack(id, count));
      drop.pickup_cooldown = 10;
      this.world.add_entity(drop);
      return drop;
   }

   // $FF: renamed from: r () boolean
   public boolean intact() {
      return !this.destroyed;
   }
}
