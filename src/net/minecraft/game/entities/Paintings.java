package net.minecraft.game.entities;

// $FF: renamed from: net.minecraft.game.entities.projectiles
public enum Paintings {
   // $FF: renamed from: game net.minecraft.game.entities.projectiles
   kebab("Kebab", 16, 16, 0, 0),
   // $FF: renamed from: player net.minecraft.game.entities.projectiles
   aztec("Aztec", 16, 16, 16, 0),
   // $FF: renamed from: h net.minecraft.game.entities.projectiles
   alban("Alban", 16, 16, 32, 0),
   // $FF: renamed from: i net.minecraft.game.entities.projectiles
   aztec2("Aztec2", 16, 16, 48, 0),
   // $FF: renamed from: j net.minecraft.game.entities.projectiles
   bomb("Bomb", 16, 16, 64, 0),
   // $FF: renamed from: k net.minecraft.game.entities.projectiles
   plant("Plant", 16, 16, 80, 0),
   // $FF: renamed from: l net.minecraft.game.entities.projectiles
   wasteland("Wasteland", 16, 16, 96, 0),
   // $FF: renamed from: m net.minecraft.game.entities.projectiles
   pool("Pool", 32, 16, 0, 32),
   // $FF: renamed from: n net.minecraft.game.entities.projectiles
   courbet("Courbet", 32, 16, 32, 32),
   // $FF: renamed from: o net.minecraft.game.entities.projectiles
   sea("Sea", 32, 16, 64, 32),
   // $FF: renamed from: p net.minecraft.game.entities.projectiles
   sunset("Sunset", 32, 16, 96, 32),
   // $FF: renamed from: q net.minecraft.game.entities.projectiles
   wanderer("Wanderer", 16, 32, 0, 64),
   // $FF: renamed from: r net.minecraft.game.entities.projectiles
   match("Match", 32, 32, 0, 128),
   // $FF: renamed from: s net.minecraft.game.entities.projectiles
   bust("Bust", 32, 32, 32, 128),
   // $FF: renamed from: t net.minecraft.game.entities.projectiles
   stage("Stage", 32, 32, 64, 128),
   // $FF: renamed from: u net.minecraft.game.entities.projectiles
   thevoid("Void", 32, 32, 96, 128),
   // $FF: renamed from: v net.minecraft.game.entities.projectiles
   skullandroses("SkullAndRoses", 32, 32, 128, 128),
   // $FF: renamed from: w net.minecraft.game.entities.projectiles
   fighters("Fighters", 64, 32, 0, 96),
   // $FF: renamed from: x net.minecraft.game.entities.projectiles
   pointer("Pointer", 64, 64, 0, 192);

   // $FF: renamed from: animals java.lang.String
   public final String name;
   // $FF: renamed from: entities int
   public final int width;
   // $FF: renamed from: projectiles int
   public final int height;
   // $FF: renamed from: player int
   public final int field_989;
   // $FF: renamed from: particles int
   public final int field_990;

   private Paintings(String var3, int var4, int var5, int var6, int var7) {
      this.name = var3;
      this.width = var4;
      this.height = var5;
      this.field_989 = var6;
      this.field_990 = var7;
   }
}
