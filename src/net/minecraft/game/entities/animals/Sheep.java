package net.minecraft.game.entities.animals;

import com.data.nbt.CompoundTag;
import net.minecraft.game.world.World;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.entities.Mob;
import net.minecraft.game.entities.Entity;
import net.minecraft.game.entities.misc.ItemDrop;

// $FF: renamed from: net.minecraft.game.entities.animals.animals
public class Sheep extends Animal {
   // $FF: renamed from: game boolean
   public boolean sheared = false;

   public Sheep(World var1) {
      super(var1);
      this.skin_file = "/mob/sheep.png";
      this.set_size(0.9F, 1.3F);
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, int) boolean
   public final boolean take_damage(Entity other, int amount) {
      if (!this.sheared && other instanceof Mob) {
         this.sheared = true;
         int var3 = 1 + this.random.nextInt(3);

         for(int i = 0; i < var3; ++i) {
            ItemDrop var5 = this.drop_item(Block.wool.block_id, 1, 1.0F);
            var5.vel_y += this.random.nextFloat() * 0.05F;
            var5.vel_x += (this.random.nextFloat() - this.random.nextFloat()) * 0.1F;
            var5.vel_z += (this.random.nextFloat() - this.random.nextFloat()) * 0.1F;
         }
      }

      return super.take_damage(other, amount);
   }

   // $FF: renamed from: game (com.game.game.entities) void
   protected final void write_tag_extra(CompoundTag var1) {
      super.write_tag_extra(var1);
      var1.method_448("Sheared", this.sheared);
   }

   // $FF: renamed from: animals (com.game.game.entities) void
   protected final void read_tag_extra(CompoundTag var1) {
      super.read_tag_extra(var1);
      this.sheared = var1.method_459("Sheared");
   }

   // $FF: renamed from: game () java.lang.String
   protected final String get_id() {
      return "Sheep";
   }

   // $FF: renamed from: player () java.lang.String
   protected final String idle_sound() {
      return "mob.sheep";
   }

   // $FF: renamed from: h () java.lang.String
   protected final String hurt_sound() {
      return "mob.sheep";
   }

   // $FF: renamed from: i () java.lang.String
   protected final String death_sound() {
      return "mob.sheep";
   }
}
