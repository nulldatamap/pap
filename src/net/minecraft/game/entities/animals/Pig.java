package net.minecraft.game.entities.animals;

import com.data.nbt.CompoundTag;
import net.minecraft.game.world.World;
import net.minecraft.game.items.Item;

// $FF: renamed from: net.minecraft.game.entities.animals.entities
public class Pig extends Animal {
   public Pig(World var1) {
      super(var1);
      this.skin_file = "/mob/pig.png";
      this.set_size(0.9F, 0.9F);
   }

   // $FF: renamed from: game (com.game.game.entities) void
   protected final void write_tag_extra(CompoundTag var1) {
      super.write_tag_extra(var1);
   }

   // $FF: renamed from: animals (com.game.game.entities) void
   protected final void read_tag_extra(CompoundTag var1) {
      super.read_tag_extra(var1);
   }

   // $FF: renamed from: game () java.lang.String
   protected final String get_id() {
      return "Pig";
   }

   // $FF: renamed from: player () java.lang.String
   protected final String idle_sound() {
      return "mob.pig";
   }

   // $FF: renamed from: h () java.lang.String
   protected final String hurt_sound() {
      return "mob.pig";
   }

   // $FF: renamed from: i () java.lang.String
   protected final String death_sound() {
      return "mob.pigdeath";
   }

   // $FF: renamed from: entities () int
   protected final int drop_item_id() {
      return Item.raw_pork.unique_id;
   }
}
