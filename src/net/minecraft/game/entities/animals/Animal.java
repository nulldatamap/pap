package net.minecraft.game.entities.animals;

import com.data.nbt.CompoundTag;
import net.minecraft.game.world.World;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.entities.LivingMob;

// $FF: renamed from: net.minecraft.game.entities.animals.game
public abstract class Animal extends LivingMob {
   public Animal(World var1) {
      super(var1);
   }

   // $FF: renamed from: game (int, int, int) float
   protected final float place_preference(int var1, int var2, int var3) {
      return this.world.block_at(var1, var2 - 1, var3) == Block.grass.block_id ? 10.0F : this.world.light_at(var1, var2, var3) - 0.5F;
   }

   // $FF: renamed from: game (com.game.game.entities) void
   protected void write_tag_extra(CompoundTag var1) {
      super.write_tag_extra(var1);
   }

   // $FF: renamed from: animals (com.game.game.entities) void
   protected void read_tag_extra(CompoundTag var1) {
      super.read_tag_extra(var1);
   }

   // $FF: renamed from: game (float, float, float) boolean
   public final boolean teleport_and_check(float var1, float var2, float var3) {
      return this.world.method_195((int)var1, (int)var2, (int)var3) > 8 && super.teleport_and_check(var1, var2, var3);
   }
}
