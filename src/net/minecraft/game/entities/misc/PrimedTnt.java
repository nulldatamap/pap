package net.minecraft.game.entities.misc;

import a.MathHelper;
import com.data.nbt.CompoundTag;
import net.minecraft.game.world.World;
import net.minecraft.game.entities.Entity;

// $FF: renamed from: net.minecraft.game.entities.entities.game
public class PrimedTnt extends Entity {
   // $FF: renamed from: game int
   public int fuse_time = 0;

   public PrimedTnt(World var1, float var2, float var3, float var4) {
      super(var1);
      this.maybe_solid = true;
      this.set_size(0.98F, 0.98F);
      this.camera_height = this.height / 2.0F;
      this.set_position(var2, var3, var4);
      float var5 = (float)(Math.random() * 3.1415927410125732D * 2.0D);
      this.vel_x = -MathHelper.sin(var5 * 3.1415927F / 180.0F) * 0.02F;
      this.vel_y = 0.2F;
      this.vel_z = -MathHelper.cos(var5 * 3.1415927F / 180.0F) * 0.02F;
      this.field_25 = false;
      this.fuse_time = 80;
      this.prev_x = var2;
      this.prev_y = var3;
      this.prev_z = var4;
   }

   // $FF: renamed from: projectiles () boolean
   public final boolean maybe_is_visible() {
      return !this.destroyed;
   }

   // $FF: renamed from: b_ () void
   public final void update_entity() {
      this.prev_x = this.pos_x;
      this.prev_y = this.pos_y;
      this.prev_z = this.pos_z;
      this.vel_y -= 0.04F;
      this.handle_collision(this.vel_x, this.vel_y, this.vel_z);
      this.vel_x *= 0.98F;
      this.vel_y *= 0.98F;
      this.vel_z *= 0.98F;
      if (this.maybe_grounded) {
         this.vel_x *= 0.7F;
         this.vel_y *= 0.7F;
         this.vel_z *= -0.5F;
      }

      if (this.fuse_time-- <= 0) {
         this.destroy();
         this.world.create_explosion((Entity)null, this.pos_x, this.pos_y, this.pos_z, 4.0F);
      } else {
         this.world.spawn_effect("smoke", this.pos_x, this.pos_y + 0.5F, this.pos_z, 0.0F, 0.0F, 0.0F);
      }
   }

   // $FF: renamed from: game (com.game.game.entities) void
   protected final void write_tag_extra(CompoundTag var1) {
      var1.write_byte("Fuse", (byte)this.fuse_time);
   }

   // $FF: renamed from: animals (com.game.game.entities) void
   protected final void read_tag_extra(CompoundTag var1) {
      this.fuse_time = var1.read_byte("Fuse");
   }

   // $FF: renamed from: game () java.lang.String
   protected final String get_id() {
      return "PrimedTnt";
   }

   // $FF: renamed from: c_ () float
   public final float half_height() {
      return 0.0F;
   }
}
