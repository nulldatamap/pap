package net.minecraft.game.entities.misc;

import com.data.nbt.CompoundTag;
import net.minecraft.game.entities.Entity;
import net.minecraft.game.world.World;

public class FallingBlock extends Entity {
    public int block_id;
    private int tick = 0;

    public FallingBlock(World w, int block_id, float x, float y, float z ) {
        super( w );

        this.maybe_solid = true;
        this.set_size(1F, 1F);
        this.camera_height = this.height / 2.0F;
        this.set_position(x, y, z);
        this.vel_x = 0F;
        this.vel_y = -0.4F;
        this.vel_z = 0F;
        this.field_25 = false;
        this.prev_x = x;
        this.prev_y = y;
        this.prev_z = z;

        this.block_id = block_id;
    }

    public final boolean maybe_is_visible() {
        return !this.destroyed;
    }

    // $FF: renamed from: b_ () void
    public final void update_entity() {
        //if( this.tick++ == 2 ) {
            world.update_blocks( (int)this.pos_x, (int)this.pos_y, (int)this.pos_z, 0 );
        //}

        this.prev_x = this.pos_x;
        this.prev_y = this.pos_y;
        this.prev_z = this.pos_z;
        this.vel_y -= 0.04F;
        this.handle_collision(this.vel_x, this.vel_y, this.vel_z);
        this.vel_x *= 0.98F;
        this.vel_y *= 0.98F;
        this.vel_z *= 0.98F;
        if (this.maybe_grounded) {
            this.destroy();
            world.set_block( (int) this.pos_x, (int) this.pos_y, (int) this.pos_z, block_id );
        }
    }

    // $FF: renamed from: game (com.game.game.entities) void
    protected final void write_tag_extra(CompoundTag var1) {
        var1.write_byte("BlockId", (byte)this.block_id);
    }

    // $FF: renamed from: animals (com.game.game.entities) void
    protected final void read_tag_extra(CompoundTag var1) {
        this.block_id = var1.read_byte("BlockId");
    }

    // $FF: renamed from: game () java.lang.String
    protected final String get_id() {
        return "FallingBlock";
    }

    // $FF: renamed from: c_ () float
    public final float half_height() {
        return 0.0F;
    }
}
