package net.minecraft.game.entities.misc;

import com.data.nbt.CompoundTag;
import net.minecraft.game.world.World;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.world.material.Material;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.entities.Entity;
import net.minecraft.game.entities.player.Player;

// $FF: renamed from: net.minecraft.game.entities.entities.animals
public class ItemDrop extends Entity {
   // $FF: renamed from: game net.minecraft.game.animals.k
   public ItemStack item_representing;
   // $FF: renamed from: Q int
   private int field_126;
   // $FF: renamed from: animals int
   public int age = 0;
   // $FF: renamed from: O int
   public int pickup_cooldown;
   // $FF: renamed from: R int
   private int health = 5;
   // $FF: renamed from: P float
   public float field_130 = (float)(Math.random() * 3.141592653589793D * 2.0D);

   public ItemDrop(World var1, float var2, float var3, float var4, ItemStack var5) {
      super(var1);
      this.set_size(0.25F, 0.25F);
      this.camera_height = this.height / 2.0F;
      this.set_position(var2, var3, var4);
      this.item_representing = var5;
      this.yaw = (float)(Math.random() * 360.0D);
      this.vel_x = (float)(Math.random() * 0.20000000298023224D - 0.10000000149011612D);
      this.vel_y = 0.2F;
      this.vel_z = (float)(Math.random() * 0.20000000298023224D - 0.10000000149011612D);
      this.field_25 = false;
   }

   public ItemDrop(World var1) {
      super(var1);
   }

   // $FF: renamed from: b_ () void
   public final void update_entity() {
      super.update_entity();
      if (this.pickup_cooldown > 0) {
         --this.pickup_cooldown;
      }

      this.prev_x = this.pos_x;
      this.prev_y = this.pos_y;
      this.prev_z = this.pos_z;
      this.vel_y -= 0.04F;
      // Lava burning
      if (this.world.method_198((int)this.pos_x, (int)this.pos_y, (int)this.pos_z) == Material.lava_block) {
         this.vel_y = 0.2F;
         this.vel_x = (this.random.nextFloat() - this.random.nextFloat()) * 0.2F;
         this.vel_z = (this.random.nextFloat() - this.random.nextFloat()) * 0.2F;
         this.world.play_sound(this, "random.fizz", 0.4F, 2.0F + this.random.nextFloat() * 0.4F);
      }

      float var4 = this.pos_z;
      float var3 = this.pos_y;
      float var2 = this.pos_x;
      int var5 = (int)var2;
      int var6 = (int)var3;
      int var7 = (int)var4;
      var2 -= (float)var5;
      var3 -= (float)var6;
      var4 -= (float)var7;
      if (Block.transparency[this.world.block_at(var5, var6, var7)]) {
         boolean var8 = !Block.transparency[this.world.block_at(var5 - 1, var6, var7)];
         boolean var9 = !Block.transparency[this.world.block_at(var5 + 1, var6, var7)];
         boolean var10 = !Block.transparency[this.world.block_at(var5, var6 - 1, var7)];
         boolean var11 = !Block.transparency[this.world.block_at(var5, var6 + 1, var7)];
         boolean var12 = !Block.transparency[this.world.block_at(var5, var6, var7 - 1)];
         boolean var13 = !Block.transparency[this.world.block_at(var5, var6, var7 + 1)];
         byte var14 = -1;
         float var15 = 9999.0F;
         if (var8 && var2 < 9999.0F) {
            var15 = var2;
            var14 = 0;
         }

         if (var9 && 1.0F - var2 < var15) {
            var15 = 1.0F - var2;
            var14 = 1;
         }

         if (var10 && var3 < var15) {
            var15 = var3;
            var14 = 2;
         }

         if (var11 && 1.0F - var3 < var15) {
            var15 = 1.0F - var3;
            var14 = 3;
         }

         if (var12 && var4 < var15) {
            var15 = var4;
            var14 = 4;
         }

         if (var13 && 1.0F - var4 < var15) {
            var14 = 5;
         }

         var2 = this.random.nextFloat() * 0.2F + 0.1F;
         if (var14 == 0) {
            this.vel_x = -var2;
         }

         if (var14 == 1) {
            this.vel_x = var2;
         }

         if (var14 == 2) {
            this.vel_y = -var2;
         }

         if (var14 == 3) {
            this.vel_y = var2;
         }

         if (var14 == 4) {
            this.vel_z = -var2;
         }

         if (var14 == 5) {
            this.vel_z = var2;
         }
      }

      boolean var10000 = false;
      this.handle_collision(this.vel_x, this.vel_y, this.vel_z);
      this.vel_x *= 0.98F;
      this.vel_y *= 0.98F;
      this.vel_z *= 0.98F;
      if (this.maybe_grounded) {
         this.vel_x *= 0.7F;
         this.vel_y *= 0.7F;
         this.vel_z *= -0.5F;
      }

      ++this.field_126;
      ++this.age;
      if (this.age >= 6000) {
         this.destroy();
      }

   }

   // $FF: renamed from: game (int) void
   protected final void take_one_damage(int var1) {
      this.take_damage((Entity)null, 1);
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, int) boolean
   public final boolean take_damage(Entity var1, int var2) {
      this.health -= var2;
      if (this.health <= 0) {
         this.destroy();
      }

      return false;
   }

   // $FF: renamed from: game (com.game.game.entities) void
   protected final void write_tag_extra(CompoundTag var1) {
      var1.write_short("Health", (byte)this.health);
      var1.write_short("Age", (short)this.age);
      var1.method_447("Item", this.item_representing.method_592(new CompoundTag()));
   }

   // $FF: renamed from: animals (com.game.game.entities) void
   protected final void read_tag_extra(CompoundTag var1) {
      this.health = var1.read_short("Health") & 255;
      this.age = var1.read_short("Age");
      var1 = var1.method_457("Item");
      this.item_representing = new ItemStack(var1);
   }

   // $FF: renamed from: game () java.lang.String
   protected final String get_id() {
      return "Item";
   }

   // $FF: renamed from: game (net.minecraft.game.entities.player.game) void
   public final void contact_player(Player var1) {
      if (this.pickup_cooldown == 0 && var1.inventory.add_item(this.item_representing)) {
         this.world.play_sound(this, "random.pop", 0.2F, ((this.random.nextFloat() - this.random.nextFloat()) * 0.7F + 1.0F) * 2.0F);
         var1.method_76(this);
         this.destroy();
      }

   }
}
