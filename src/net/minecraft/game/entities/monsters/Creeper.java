package net.minecraft.game.entities.monsters;

import com.data.nbt.CompoundTag;
import net.minecraft.game.world.World;
import net.minecraft.game.items.Item;
import net.minecraft.game.entities.Entity;

// $FF: renamed from: net.minecraft.game.entities.game.projectiles
public class Creeper extends Monster {
   // $FF: renamed from: P int
   private int fuse;
   // $FF: renamed from: Q int
   private int prev_fuse;
   // $FF: renamed from: R int
   private int max_fuse = 30;
   // $FF: renamed from: an int
   private int activation = -1;

   public Creeper(World var1) {
      super(var1);
      this.skin_file = "/mob/creeper.png";
   }

   // $FF: renamed from: game (com.game.game.entities) void
   protected final void write_tag_extra(CompoundTag var1) {
      super.write_tag_extra(var1);
   }

   // $FF: renamed from: animals (com.game.game.entities) void
   protected final void read_tag_extra(CompoundTag var1) {
      super.read_tag_extra(var1);
   }

   // $FF: renamed from: game () java.lang.String
   protected final String get_id() {
      return "Creeper";
   }

   // $FF: renamed from: player () void
   protected final void update_ai() {
      this.prev_fuse = this.fuse;
      if (this.fuse > 0 && this.activation < 0) {
         --this.fuse;
      }

      if (this.activation >= 0) {
         this.activation = 2;
      }

      super.update_ai();
      if (this.activation != 1) {
         this.activation = -1;
      }

   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, float) void
   protected final void when_can_see_entity(Entity other, float distance) {
      if (this.activation <= 0 && distance < 3.0F || this.activation > 0 && distance < 7.0F) {
         if (this.fuse == 0) {
            this.world.play_sound(this, "random.fuse", 1.0F, 0.5F);
         }

         this.activation = 1;
         ++this.fuse;
         if (this.fuse == this.max_fuse) {
            this.world.create_explosion(this, this.pos_x, this.pos_y, this.pos_z, 3.0F);
            this.destroy();
         }

         this.halt_pursuit = true;
      }

   }

   // $FF: renamed from: entities (float) float
   public final float fuse_procentage(float var1) {
      return ((float)this.prev_fuse + (float)(this.fuse - this.prev_fuse) * var1) / (float)(this.max_fuse - 2);
   }

   // $FF: renamed from: entities () int
   protected final int drop_item_id() {
      return Item.gun_powder.unique_id;
   }
}
