package net.minecraft.game.entities.monsters;

import com.data.nbt.CompoundTag;
import net.minecraft.game.world.World;
import net.minecraft.game.entities.LivingMob;
import net.minecraft.game.entities.Entity;

// $FF: renamed from: net.minecraft.game.entities.game.player
public class Monster extends LivingMob {
   // $FF: renamed from: game int
   protected int attack_damage = 2;

   public Monster(World var1) {
      super(var1);
      this.health = this.max_health;
   }

   // $FF: renamed from: particles () void
   public void update_movement() {
      if (this.light_level(1.0F) > 0.5F) {
         this.despawn_tick += 2;
      }

      super.update_movement();
   }

   // $FF: renamed from: b_ () void
   public final void update_entity() {
      super.update_entity();
      if (this.world.difficulty == 0) {
         this.destroy();
      }

   }

   // $FF: renamed from: animals () net.minecraft.game.entities.animals
   protected Entity find_target() {
      return this.world.player.distance_squared(this) < 256.0F ? this.world.player : null;
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, int) boolean
   public final boolean take_damage(Entity other, int amount) {
      if (super.take_damage(other, amount)) {
         if (other != this) {
            this.target = other;
         }

         return true;
      } else {
         return false;
      }
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, float) void
   protected void when_can_see_entity(Entity other, float distance) {
      if ((double)distance < 2.5D && other.hitbox.max_y > this.hitbox.min_y && other.hitbox.min_y < this.hitbox.max_y) {
         this.attack_time = 20;
         other.take_damage(this, this.attack_damage);
      }

   }

   // $FF: renamed from: game (int, int, int) float
   protected float place_preference(int var1, int var2, int var3) {
      return 0.5F - this.world.light_at(var1, var2, var3);
   }

   // $FF: renamed from: game (com.game.game.entities) void
   protected void write_tag_extra(CompoundTag var1) {
      super.write_tag_extra(var1);
   }

   // $FF: renamed from: animals (com.game.game.entities) void
   protected void read_tag_extra(CompoundTag var1) {
      super.read_tag_extra(var1);
   }

   // $FF: renamed from: game () java.lang.String
   protected String get_id() {
      return "Monster";
   }

   // $FF: renamed from: game (float, float, float) boolean
   public final boolean teleport_and_check(float var1, float var2, float var3) {
      return this.world.method_195((int)var1, (int)var2, (int)var3) <= this.random.nextInt(8) && super.teleport_and_check(var1, var2, var3);
   }
}
