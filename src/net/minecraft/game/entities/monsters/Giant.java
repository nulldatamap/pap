package net.minecraft.game.entities.monsters;

import net.minecraft.game.world.World;

// $FF: renamed from: net.minecraft.game.entities.game.game
public class Giant extends Monster {
   public Giant(World var1) {
      super(var1);
      this.skin_file = "/mob/zombie.png";
      this.field_107 = 0.5F;
      this.attack_damage = 50;
      this.health *= 10;
      this.camera_height *= 6.0F;
      this.set_size(this.width * 6.0F, this.height * 6.0F);
   }

   // $FF: renamed from: game (int, int, int) float
   protected final float place_preference(int var1, int var2, int var3) {
      return this.world.light_at(var1, var2, var3) - 0.5F;
   }

   // $FF: renamed from: game () java.lang.String
   protected final String get_id() {
      return "Giant";
   }
}
