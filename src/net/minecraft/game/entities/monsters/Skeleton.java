package net.minecraft.game.entities.monsters;

import a.MathHelper;
import com.data.nbt.CompoundTag;
import net.minecraft.game.world.World;
import net.minecraft.game.items.Item;
import net.minecraft.game.entities.Entity;
import net.minecraft.game.entities.projectiles.Arrow;

// $FF: renamed from: net.minecraft.game.entities.game.entities
public class Skeleton extends Monster {
   public Skeleton(World var1) {
      super(var1);
      this.skin_file = "/mob/skeleton.png";
   }

   // $FF: renamed from: particles () void
   public final void update_movement() {
      float light = this.light_level(1.0F);
      if (this.world.maybe_brightness > 7 && light > 0.5F && this.world.has_skylight((int)this.pos_x, (int)this.pos_y, (int)this.pos_z) && this.random.nextFloat() * 30.0F < (light - 0.4F) * 2.0F) {
         this.burn_time = 300;
      }

      super.update_movement();
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, float) void
   protected final void when_can_see_entity(Entity other, float distance) {
      if (distance < 10.0F) {
         float dx = other.pos_x - this.pos_x;
         float dz = other.pos_z - this.pos_z;
         if (this.attack_time == 0) {
            Arrow arrow;
            ++(arrow = new Arrow(this.world, this)).pos_y;
            float kinda_dy = other.pos_y - 0.2F - arrow.pos_y;
            float kinda_dist = MathHelper.sqrt(dx * dx + dz * dz) * 0.2F;
            this.world.play_sound(this, "random.bow", 1.0F, 1.0F / (this.random.nextFloat() * 0.4F + 0.8F));
            this.world.add_entity(arrow);
            arrow.calculate_trajectory(dx, kinda_dy + kinda_dist, dz, 0.6F, 12.0F);
            this.attack_time = 30;
         }

         this.yaw = (float)(Math.atan2((double)dz, (double) dx) * 180.0D / 3.1415927410125732D) - 90.0F;
         this.halt_pursuit = true;
      }

   }

   // $FF: renamed from: game (com.game.game.entities) void
   protected final void write_tag_extra(CompoundTag var1) {
      super.write_tag_extra(var1);
   }

   // $FF: renamed from: animals (com.game.game.entities) void
   protected final void read_tag_extra(CompoundTag var1) {
      super.read_tag_extra(var1);
   }

   // $FF: renamed from: game () java.lang.String
   protected final String get_id() {
      return "Skeleton";
   }

   // $FF: renamed from: entities () int
   protected final int drop_item_id() {
      return Item.bone.unique_id;
   }
}
