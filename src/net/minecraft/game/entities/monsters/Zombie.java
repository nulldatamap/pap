package net.minecraft.game.entities.monsters;

import net.minecraft.game.world.World;
import net.minecraft.game.items.Item;

// $FF: renamed from: net.minecraft.game.entities.game.particles
public class Zombie extends Monster {
   public Zombie(World var1) {
      super(var1);
      this.skin_file = "/mob/zombie.png";
      this.field_107 = 0.5F;
      this.attack_damage = 5;
   }

   // $FF: renamed from: particles () void
   public final void update_movement() {
      float var1;
      if (this.world.maybe_brightness > 7 && (var1 = this.light_level(1.0F)) > 0.5F && this.world.has_skylight((int)this.pos_x, (int)this.pos_y, (int)this.pos_z) && this.random.nextFloat() * 30.0F < (var1 - 0.4F) * 2.0F) {
         this.burn_time = 300;
      }

      super.update_movement();
   }

   // $FF: renamed from: game () java.lang.String
   protected final String get_id() {
      return "Zombie";
   }

   // $FF: renamed from: entities () int
   protected final int drop_item_id() {
      return Item.feather.unique_id;
   }
}
