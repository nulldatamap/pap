package net.minecraft.game.entities.monsters;

import a.MathHelper;
import com.data.nbt.CompoundTag;
import net.minecraft.game.world.World;
import net.minecraft.game.items.Item;
import net.minecraft.game.entities.Entity;

// $FF: renamed from: net.minecraft.game.entities.game.animals
public class Spider extends Monster {
   public Spider(World var1) {
      super(var1);
      this.skin_file = "/mob/spider.png";
      this.set_size(1.4F, 0.9F);
      this.field_107 = 0.8F;
   }

   // $FF: renamed from: animals () net.minecraft.game.entities.animals
   protected final Entity find_target() {
      return this.light_level(1.0F) < 0.5F && this.world.player.distance_squared(this) < 256.0F ? this.world.player : null;
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, float) void
   protected final void when_can_see_entity(Entity other, float distance) {
      if (this.light_level(1.0F) > 0.5F && this.random.nextInt(100) == 0) {
         this.target = null;
      } else {
         // Make it leap
         if (distance > 2.0F && distance < 6.0F && this.random.nextInt(10) == 0) {
            if (this.maybe_grounded) {
               distance = other.pos_x - this.pos_x;
               float var4 = other.pos_z - this.pos_z;
               float var3 = MathHelper.sqrt(distance * distance + var4 * var4);
               this.vel_x = distance / var3 * 0.5F * 0.8F + this.vel_x * 0.2F;
               this.vel_z = var4 / var3 * 0.5F * 0.8F + this.vel_z * 0.2F;
               this.vel_y = 0.4F;
               return;
            }
         } else {
            super.when_can_see_entity(other, distance);
         }

      }
   }

   // $FF: renamed from: game (com.game.game.entities) void
   protected final void write_tag_extra(CompoundTag var1) {
      super.write_tag_extra(var1);
   }

   // $FF: renamed from: animals (com.game.game.entities) void
   protected final void read_tag_extra(CompoundTag var1) {
      super.read_tag_extra(var1);
   }

   // $FF: renamed from: game () java.lang.String
   protected final String get_id() {
      return "Spider";
   }

   // $FF: renamed from: entities () int
   protected final int drop_item_id() {
      return Item.some_string.unique_id;
   }
}
