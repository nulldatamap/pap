package net.minecraft.game.entities;

import a.MathHelper;
import net.minecraft.game.world.World;
import net.minecraft.game.world.a.Path;
import net.minecraft.game.util.Vector;

// $FF: renamed from: net.minecraft.game.entities.entities
public class LivingMob extends Mob {
   // $FF: renamed from: game net.minecraft.game.game.game.animals
   private Path path;
   // $FF: renamed from: animals net.minecraft.game.entities.animals
   protected Entity target;
   // $FF: renamed from: O boolean
   protected boolean halt_pursuit = false;

   public LivingMob(World var1) {
      super(var1);
   }

   // $FF: renamed from: player () void
   protected void update_ai() {
      this.halt_pursuit = false;
      float var6;
      float var7;
      float var8;
      if (this.target == null) {
         this.target = this.find_target();
         if (this.target != null) {
            this.path = this.world.field_342.find_path(this, this.target, 16.0F);
         }
      } else if (!this.target.intact()) {
         this.target = null;
      } else {
         Entity var5 = this.target;
         var7 = this.target.pos_x - super.pos_x;
         var8 = var5.pos_y - super.pos_y;
         var6 = var5.pos_z - super.pos_z;
         float var1 = MathHelper.sqrt(var7 * var7 + var8 * var8 + var6 * var6);
         if (this.world.check_raycast(new Vector(this.pos_x, this.pos_y + this.head_offset(), this.pos_z), new Vector(this.target.pos_x, this.target.pos_y + this.target.head_offset(), this.target.pos_z)) == null) {
            this.when_can_see_entity(this.target, var1);
         }
      }

      if (this.halt_pursuit) {
         this.walk_dir_x = 0.0F;
         this.walk_dir_z = 0.0F;
         this.want_to_jump = false;
      } else {
         float var4;
         if (this.target != null && (this.path == null || this.random.nextInt(20) == 0)) {
            this.path = this.world.field_342.find_path(this, this.target, 16.0F);
         } else if (this.path == null || this.random.nextInt(100) == 0) {
            int var10 = -1;
            int var2 = -1;
            int var3 = -1;
            var4 = -99999.0F;

            for(int var14 = 0; var14 < 200; ++var14) {
               int var16 = (int)(this.pos_x + (float)this.random.nextInt(21) - 10.0F);
               int var17 = (int)(this.pos_y + (float)this.random.nextInt(9) - 4.0F);
               int var18 = (int)(this.pos_z + (float)this.random.nextInt(21) - 10.0F);
               float var9;
               if ((var9 = this.place_preference(var16, var17, var18)) > var4) {
                  var4 = var9;
                  var10 = var16;
                  var2 = var17;
                  var3 = var18;
               }
            }

            if (var10 > 0) {
               this.path = this.world.field_342.method_232(this, var10, var2, var3, 16.0F);
            }
         }

         boolean var11 = this.collides_with_water();
         boolean var12 = this.collides_with_lava();
         if (this.path != null && this.random.nextInt(100) != 0) {
            Vector var13 = this.path.current_waypoint(this);
            var4 = this.width * 2.0F;

            float var15;
            while(var13 != null) {
               var8 = this.pos_z;
               var7 = this.pos_y;
               var6 = this.pos_x;
               var6 -= var13.x;
               var7 -= var13.y;
               var15 = var8 - var13.z;
               if (var6 * var6 + var7 * var7 + var15 * var15 >= var4 * var4 || var13.y > this.pos_y) {
                  break;
               }

               this.path.next_waypoint();
               if (this.path.destination_reached()) {
                  var13 = null;
                  this.path = null;
               } else {
                  var13 = this.path.current_waypoint(this);
               }
            }

            this.want_to_jump = false;
            if (var13 != null) {
               var15 = var13.x - this.pos_x;
               var6 = var13.z - this.pos_z;
               var7 = var13.y - this.pos_y;
               this.yaw = (float)(Math.atan2((double)var6, (double)var15) * 180.0D / 3.1415927410125732D) - 90.0F;
               this.walk_dir_z = this.field_107;
               if (var7 > 0.0F) {
                  this.want_to_jump = true;
               }
            }

            if (this.random.nextFloat() < 0.8F && (var11 || var12)) {
               this.want_to_jump = true;
            }

         } else {
            super.update_ai();
            this.path = null;
         }
      }
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, float) void
   protected void when_can_see_entity(Entity var1, float var2) {
   }

   // $FF: renamed from: game (int, int, int) float
   protected float place_preference(int var1, int var2, int var3) {
      return 0.0F;
   }

   // $FF: renamed from: animals () net.minecraft.game.entities.animals
   protected Entity find_target() {
      return null;
   }

   // $FF: renamed from: game (float, float, float) boolean
   public boolean teleport_and_check(float var1, float var2, float var3) {
      return super.teleport_and_check(var1, var2, var3) && this.place_preference((int)var1, (int)var2, (int)var3) >= 0.0F;
   }
}
