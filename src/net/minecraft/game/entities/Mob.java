package net.minecraft.game.entities;

import a.MathHelper;
import com.data.nbt.CompoundTag;
import java.util.List;
import net.minecraft.game.world.World;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.world.block.SoundMaterial;

// $FF: renamed from: net.minecraft.game.entities.player
public class Mob extends Entity {
   // $FF: renamed from: S int
   public int something_related_to_invincibility = 20;
   // $FF: renamed from: T float
   public float field_82 = 0.0F;
   // $FF: renamed from: U float
   public float field_83 = 0.0F;
   // $FF: renamed from: game float
   private float field_84;
   // $FF: renamed from: animals float
   private float field_85;
   // $FF: renamed from: V java.lang.String
   protected String skin_file = "/char.png";
   // $FF: renamed from: O int
   private int field_87 = 0;
   // $FF: renamed from: W int
   public int health;
   // $FF: renamed from: X int
   public int prev_health;
   // $FF: renamed from: P int
   private int idle_sound_tick;
   // $FF: renamed from: Y int
   public int hurt_time;
   // $FF: renamed from: Z int
   public int something_related_to_hurt;
   // $FF: renamed from: aa float
   public float hurt_yaw = 0.0F;
   // $FF: renamed from: ab int
   public int death_time = 0;
   // $FF: renamed from: ac int
   public int attack_time = 0;
   // $FF: renamed from: ad float
   public float prev_head_bob_pitch;
   // $FF: renamed from: ae float
   public float head_bob_pitch;
   // $FF: renamed from: af float
   public float field_98;
   // $FF: renamed from: ag float
   public float field_99;
   // $FF: renamed from: ah float
   public float field_100;
   // $FF: renamed from: ai int
   protected int despawn_tick;
   // $FF: renamed from: aj float
   protected float walk_dir_x;
   // $FF: renamed from: ak float
   protected float walk_dir_z;
   // $FF: renamed from: Q float
   private float yaw_change;
   // $FF: renamed from: al boolean
   protected boolean want_to_jump;
   // $FF: renamed from: R float
   private float field_106;
   // $FF: renamed from: am float
   protected float field_107;

   public Mob(World var1) {
      super(var1);
      Math.random();
      this.despawn_tick = 0;
      this.want_to_jump = false;
      this.field_106 = 0.0F;
      this.field_107 = 0.7F;
      this.health = this.max_health = 10;
      this.maybe_solid = true;
      Math.random();
      this.set_position(this.pos_x, this.pos_y, this.pos_z);
      Math.random();
      this.yaw = (float)(Math.random() * 3.1415927410125732D * 2.0D);
      this.field_32 = 0.5F;
   }

   // $FF: renamed from: q () java.lang.String
   public final String maybe_texture_name() {
      return this.skin_file;
   }

   // $FF: renamed from: projectiles () boolean
   public final boolean maybe_is_visible() {
      return !this.destroyed;
   }

   // $FF: renamed from: p () boolean
   public final boolean is_ejecting() {
      return !this.destroyed;
   }

   // $FF: renamed from: n () float
   protected float head_offset() {
      return this.height * 0.85F;
   }

   // $FF: renamed from: b_ () void
   public void update_entity() {
      super.update_entity();

      // Randomly make sounds
      if (this.random.nextInt(1000) < this.idle_sound_tick++) {
         this.idle_sound_tick = -80;
         String sound = this.idle_sound();
         if (sound != null) {
            this.world.play_sound(this, sound, 1.0F, (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
         }
      }

      // Drowning
      if (this.on_water_block()) {
         --this.air;
         if (this.air == -20) {
            this.air = 0;

            float ox;
            float oy;
            float oz;
            for(int var7 = 0; var7 < 8; ++var7) {
               ox = this.random.nextFloat() - this.random.nextFloat();
               oy = this.random.nextFloat() - this.random.nextFloat();
               oz = this.random.nextFloat() - this.random.nextFloat();
               this.world.spawn_effect("bubble", this.pos_x + ox, this.pos_y + oy, this.pos_z + oz, this.vel_x, this.vel_y, this.vel_z);
            }

            this.take_damage((Entity)null, 2);
         }

         this.burn_time = 0;
      } else {
         this.air = this.max_air;
      }

      this.prev_head_bob_pitch = this.head_bob_pitch;
      if (this.attack_time > 0) {
         --this.attack_time;
      }

      if (this.hurt_time > 0) {
         --this.hurt_time;
      }

      if (this.invincible_frames > 0) {
         --this.invincible_frames;
      }

      if (this.health <= 0) {
         ++this.death_time;
         if (this.death_time > 20) {
            this.destroy();
         }
      }

      this.field_83 = this.field_82;
      this.prev_yaw = this.yaw;
      this.prev_pitch = this.pitch;
      this.update_movement();
      float dx = this.pos_x - this.prev_x;
      float dz = this.pos_z - this.prev_z;
      float speed = MathHelper.sqrt(dx * dx + dz * dz);

      float direction = this.field_82;
      float var5 = 0.0F;
      float var6 = 0.0F;
      if (speed > 0.05F) {
         var6 = 1.0F;
         var5 = speed * 3.0F;
         direction = (float)Math.atan2((double)dz, (double)dx) * 180.0F / 3.1415927F - 90.0F;
      }

      if (!this.maybe_grounded) {
         var6 = 0.0F;
      }

      this.field_84 += (var6 - this.field_84) * 0.3F;

      for(dx = direction - this.field_82; dx < -180.0F; dx += 360.0F) {
         ;
      }

      while(dx >= 180.0F) {
         dx -= 360.0F;
      }

      this.field_82 += dx * 0.1F;

      for(dx = this.yaw - this.field_82; dx < -180.0F; dx += 360.0F) {
         ;
      }

      while(dx >= 180.0F) {
         dx -= 360.0F;
      }

      boolean var9 = dx < -90.0F || dx >= 90.0F;
      if (dx < -75.0F) {
         dx = -75.0F;
      }

      if (dx >= 75.0F) {
         dx = 75.0F;
      }

      this.field_82 = this.yaw - dx;
      this.field_82 += dx * 0.1F;
      if (var9) {
         var5 = -var5;
      }

      while(this.yaw - this.prev_yaw < -180.0F) {
         this.prev_yaw -= 360.0F;
      }

      while(this.yaw - this.prev_yaw >= 180.0F) {
         this.prev_yaw += 360.0F;
      }

      while(this.field_82 - this.field_83 < -180.0F) {
         this.field_83 -= 360.0F;
      }

      while(this.field_82 - this.field_83 >= 180.0F) {
         this.field_83 += 360.0F;
      }

      while(this.pitch - this.prev_pitch < -180.0F) {
         this.prev_pitch -= 360.0F;
      }

      while(this.pitch - this.prev_pitch >= 180.0F) {
         this.prev_pitch += 360.0F;
      }

      this.field_85 += var5;
   }

   // $FF: renamed from: game (float, float) void
   protected final void set_size(float var1, float var2) {
      super.set_size(var1, var2);
   }

   // $FF: renamed from: animals (int) void
   public final void heal(int var1) {
      if (this.health > 0) {
         this.health += var1;
         if (this.health > this.max_health) {
            this.health = max_health;
         }

         this.invincible_frames = this.something_related_to_invincibility / 2;
      }
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, int) boolean
   public boolean take_damage(Entity other, int amount) {
      if (!this.world.can_take_damage) {
         return false;
      } else {
         this.despawn_tick = 0;
         if (this.health <= 0) {
            return false;
         } else {
            this.field_99 = 1.5F;
            if ((float)this.invincible_frames > (float)this.something_related_to_invincibility / 2.0F) {
               if (this.prev_health - amount >= this.health) {
                  return false;
               }

               this.health = this.prev_health - amount;
            } else {
               this.prev_health = this.health;
               this.invincible_frames = this.something_related_to_invincibility;
               this.health -= amount;
               this.hurt_time = this.something_related_to_hurt = 10;
            }

            this.hurt_yaw = 0.0F;
            if (other != null) {
               float var6 = other.pos_x - this.pos_x;
               float var3 = other.pos_z - this.pos_z;
               this.hurt_yaw = (float)(Math.atan2((double)var3, (double)var6) * 180.0D / 3.1415927410125732D) - this.yaw;
               float var5 = MathHelper.sqrt(var6 * var6 + var3 * var3);
               this.vel_x /= 2.0F;
               this.vel_y /= 2.0F;
               this.vel_z /= 2.0F;
               this.vel_x -= var6 / var5 * 0.4F;
               this.vel_y += 0.4F;
               this.vel_z -= var3 / var5 * 0.4F;
               if (this.vel_y > 0.4F) {
                  this.vel_y = 0.4F;
               }
            } else {
               this.hurt_yaw = (float)((int)(Math.random() * 2.0D) * 180);
            }

            if (this.health <= 0) {
               this.world.play_sound(this, this.death_sound(), 1.0F, (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
               this.on_death(other);
            } else {
               this.world.play_sound(this, this.hurt_sound(), 1.0F, (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
            }

            return true;
         }
      }
   }

   // $FF: renamed from: player () java.lang.String
   protected String idle_sound() {
      return null;
   }

   // $FF: renamed from: h () java.lang.String
   protected String hurt_sound() {
      return "random.hurt";
   }

   // $FF: renamed from: i () java.lang.String
   protected String death_sound() {
      return "random.hurt";
   }

   // $FF: renamed from: projectiles (net.minecraft.game.entities.animals) void
   public void on_death(Entity killer) {
      int id = this.drop_item_id();
      if (id > 0) {
         int amount = this.random.nextInt(3);

         for(int i = 0; i < amount; ++i) {
            this.drop_item(id, 1);
         }
      }

   }

   // $FF: renamed from: entities () int
   protected int drop_item_id() {
      return 0;
   }

   // $FF: renamed from: animals (float) void
   protected final void take_fall_damage(float fall_distance) {
      int damage = (int)Math.ceil((double)(fall_distance - 3.0F));
      if (damage > 0) {
         this.take_damage((Entity)null, damage);
         int block = this.world.block_at((int)this.pos_x, (int)(this.pos_y - 0.2F - this.camera_height), (int)this.pos_z);
         if (block > 0) {
            SoundMaterial mat = Block.blocks[block].sound_material;
            this.world.play_sound(this, mat.step_sound(), mat.field_958 * 0.5F, mat.field_959 * 0.75F);
         }
      }

   }

   // $FF: renamed from: game (com.game.game.entities) void
   protected void write_tag_extra(CompoundTag var1) {
      var1.write_short("Health", (short)this.health);
      var1.write_short("HurtTime", (short)this.hurt_time);
      var1.write_short("DeathTime", (short)this.death_time);
      var1.write_short("AttackTime", (short)this.attack_time);
   }

   // $FF: renamed from: animals (com.game.game.entities) void
   protected void read_tag_extra(CompoundTag var1) {
      this.health = var1.read_short("Health");
      if (!var1.method_449("Health")) {
         this.health = 10;
      }

      this.hurt_time = var1.read_short("HurtTime");
      this.death_time = var1.read_short("DeathTime");
      this.attack_time = var1.read_short("AttackTime");
   }

   // $FF: renamed from: game () java.lang.String
   protected String get_id() {
      return "Mob";
   }

   // $FF: renamed from: r () boolean
   public final boolean intact() {
      return !this.destroyed && this.health > 0;
   }

   // $FF: renamed from: particles () void
   public void update_movement() {
      ++this.despawn_tick;
      Entity player = this.world.get_player();
      if (this.despawn_tick > 600 && this.random.nextInt(800) == 0 && player != null) {
         float dx = player.pos_x - this.pos_x;
         float dy = player.pos_y - this.pos_y;
         float dz = player.pos_z - this.pos_z;
         if (dx * dx + dy * dy + dz * dz < 1024.0F) {
            this.despawn_tick = 0;
         } else {
            this.destroy();
         }
      }

      if (this.health <= 0) {
         this.want_to_jump = false;
         this.walk_dir_x = 0.0F;
         this.walk_dir_z = 0.0F;
         this.yaw_change = 0.0F;
      } else {
         this.update_ai();
      }

      if (this.want_to_jump) {
         if (this.collides_with_water()) {
            this.vel_y += 0.04F;
         } else if (this.collides_with_lava()) {
            this.vel_y += 0.04F;
         } else if (this.maybe_grounded) {
            this.vel_y = 0.42F;
         }
      }

      this.walk_dir_x *= 0.98F;
      this.walk_dir_z *= 0.98F;
      this.yaw_change *= 0.9F;
      float wz = this.walk_dir_z;
      float wx = this.walk_dir_x;
      float var4;
      if (this.collides_with_water()) {
         var4 = this.pos_y;
         this.move_forwards(wx, wz, 0.02F);
         this.handle_collision(this.vel_x, this.vel_y, this.vel_z);
         this.vel_x *= 0.8F;
         this.vel_y *= 0.8F;
         this.vel_z *= 0.8F;
         this.vel_y = (float)((double)this.vel_y - 0.02D);
         if (this.field_17 && this.method_24(this.vel_x, this.vel_y + 0.6F - this.pos_y + var4, this.vel_z)) {
            this.vel_y = 0.3F;
         }
      } else if (this.collides_with_lava()) {
         var4 = this.pos_y;
         this.move_forwards(wx, wz, 0.02F);
         this.handle_collision(this.vel_x, this.vel_y, this.vel_z);
         this.vel_x *= 0.5F;
         this.vel_y *= 0.5F;
         this.vel_z *= 0.5F;
         this.vel_y = (float)((double)this.vel_y - 0.02D);
         if (this.field_17 && this.method_24(this.vel_x, this.vel_y + 0.6F - this.pos_y + var4, this.vel_z)) {
            this.vel_y = 0.3F;
         }
      } else {
         this.move_forwards(wx, wz, this.maybe_grounded ? 0.1F : 0.02F);
         this.handle_collision(this.vel_x, this.vel_y, this.vel_z);
         this.vel_x *= 0.91F;
         this.vel_y *= 0.98F;
         this.vel_z *= 0.91F;
         this.vel_y = (float)((double)this.vel_y - 0.08D);
         if (this.maybe_grounded) {
            this.vel_x *= 0.6F;
            this.vel_z *= 0.6F;
         }
      }

      this.field_98 = this.field_99;
      var4 = this.pos_x - this.prev_x;
      wx = this.pos_z - this.prev_z;
      if ((wx = MathHelper.sqrt(var4 * var4 + wx * wx) * 4.0F) > 1.0F) {
         wx = 1.0F;
      }

      this.field_99 += (wx - this.field_99) * 0.4F;
      this.field_100 += this.field_99;
      List var9 = this.world.colliding_entities(this, this.hitbox.grow(0.2F, 0.0F, 0.2F));
      if (var9 != null && var9.size() > 0) {
         for(int var7 = 0; var7 < var9.size(); ++var7) {
            Entity var10 = (Entity)var9.get(var7);
            if (var10.is_ejecting()) {
               var10.eject_from_entity(this);
            }
         }
      }

   }

   // $FF: renamed from: player () void
   protected void update_ai() {
      if (this.random.nextFloat() < 0.07F) {
         this.walk_dir_x = (this.random.nextFloat() - 0.5F) * this.field_107;
         this.walk_dir_z = this.random.nextFloat() * this.field_107;
      }

      this.want_to_jump = this.random.nextFloat() < 0.01F;
      if (this.random.nextFloat() < 0.04F) {
         this.yaw_change = (this.random.nextFloat() - 0.5F) * 60.0F;
      }

      this.yaw += this.yaw_change;
      this.pitch = 0.0F;
      boolean var1 = this.collides_with_water();
      boolean var2 = this.collides_with_lava();
      if (var1 || var2) {
         this.want_to_jump = this.random.nextFloat() < 0.8F;
      }

   }

   // $FF: renamed from: game (float, float, float) boolean
   public boolean teleport_and_check(float var1, float var2, float var3) {
      this.set_position(var1, var2 + this.height / 2.0F, var3);
      return this.world.method_187(this.hitbox) && this.world.get_colliding_aabbs(this.hitbox).size() == 0 && !this.world.method_183(this.hitbox);
   }
}
