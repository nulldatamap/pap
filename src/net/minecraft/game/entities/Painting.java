package net.minecraft.game.entities;

import com.data.nbt.CompoundTag;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.game.world.World;
import net.minecraft.game.world.material.Material;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.items.Item;
import net.minecraft.game.entities.misc.ItemDrop;
import net.minecraft.game.util.AABB;

// $FF: renamed from: net.minecraft.game.entities.game
public class Painting extends Entity {
   // $FF: renamed from: O int
   private int field_45;
   // $FF: renamed from: game int
   public int field_46;
   // $FF: renamed from: P int
   private int field_47;
   // $FF: renamed from: Q int
   private int field_48;
   // $FF: renamed from: R int
   private int field_49;
   // $FF: renamed from: animals net.minecraft.game.entities.projectiles
   public Paintings field_50;

   public Painting(World var1) {
      super(var1);
      this.field_45 = 0;
      this.field_46 = 0;
      this.camera_height = 0.0F;
      this.set_size(0.5F, 0.5F);
   }

   public Painting(World var1, int var2, int var3, int var4, int var5) {
      this(var1);
      this.field_47 = var2;
      this.field_48 = var3;
      this.field_49 = var4;
      ArrayList var7 = new ArrayList();
      Paintings[] var8;
      var3 = (var8 = Paintings.values()).length;

      for(var4 = 0; var4 < var3; ++var4) {
         Paintings var6 = var8[var4];
         this.field_50 = var6;
         this.method_54(var5);
         if (this.method_56()) {
            var7.add(var6);
         }
      }

      if (var7.size() > 0) {
         this.field_50 = (Paintings)var7.get(this.random.nextInt(var7.size()));
      }

      this.method_54(var5);
   }

   // $FF: renamed from: animals (int) void
   private void method_54(int var1) {
      this.field_46 = var1;
      this.prev_yaw = this.yaw = (float)(var1 * 90);
      float var2 = (float)this.field_50.width;
      float var3 = (float)this.field_50.height;
      float var4 = (float)this.field_50.width;
      if (var1 != 0 && var1 != 2) {
         var2 = 0.5F;
      } else {
         var4 = 0.5F;
      }

      var2 /= 32.0F;
      var3 /= 32.0F;
      var4 /= 32.0F;
      float var5 = (float)this.field_47 + 0.5F;
      float var6 = (float)this.field_48 + 0.5F;
      float var7 = (float)this.field_49 + 0.5F;
      if (var1 == 0) {
         var7 -= 0.5625F;
      }

      if (var1 == 1) {
         var5 -= 0.5625F;
      }

      if (var1 == 2) {
         var7 += 0.5625F;
      }

      if (var1 == 3) {
         var5 += 0.5625F;
      }

      if (var1 == 0) {
         var5 -= method_55(this.field_50.width);
      }

      if (var1 == 1) {
         var7 += method_55(this.field_50.width);
      }

      if (var1 == 2) {
         var5 += method_55(this.field_50.width);
      }

      if (var1 == 3) {
         var7 -= method_55(this.field_50.width);
      }

      var6 += method_55(this.field_50.height);
      this.set_position(var5, var6, var7);
      this.hitbox = new AABB(var5 - var2, var6 - var3, var7 - var4, var5 + var2, var6 + var3, var7 + var4);
      float var8 = 0.00625F;
      var8 = 0.00625F;
      var8 = 0.00625F;
      AABB var9 = this.hitbox;
      var2 = this.hitbox.min_x;
      var3 = var9.min_y;
      var4 = var9.min_z;
      var5 = var9.max_x;
      var6 = var9.max_y;
      var8 = var9.max_z;
      var5 -= 0.00625F;
      var6 -= 0.00625F;
      var8 -= 0.00625F;
      this.hitbox = new AABB(var2, var3, var4, var5, var6, var8);
   }

   // $FF: renamed from: entities (int) float
   private static float method_55(int var0) {
      if (var0 == 32) {
         return 0.5F;
      } else {
         return var0 == 64 ? 0.5F : 0.0F;
      }
   }

   // $FF: renamed from: b_ () void
   public final void update_entity() {
      if (this.field_45++ == 100 && !this.method_56()) {
         this.field_45 = 0;
         this.destroy();
         this.world.add_entity(new ItemDrop(this.world, this.pos_x, this.pos_y, this.pos_z, new ItemStack(Item.field_198)));
      }

   }

   // $FF: renamed from: entities () boolean
   public final boolean method_56() {
      if (this.world.get_colliding_aabbs(this.hitbox).size() > 0) {
         return false;
      } else {
         int var1 = this.field_50.width / 16;
         int var2 = this.field_50.height / 16;
         int var3 = this.field_47;
         int var5 = this.field_49;
         if (this.field_46 == 0) {
            var3 = (int)(this.pos_x - (float)this.field_50.width / 32.0F);
         }

         if (this.field_46 == 1) {
            var5 = (int)(this.pos_z - (float)this.field_50.width / 32.0F);
         }

         if (this.field_46 == 2) {
            var3 = (int)(this.pos_x - (float)this.field_50.width / 32.0F);
         }

         if (this.field_46 == 3) {
            var5 = (int)(this.pos_z - (float)this.field_50.width / 32.0F);
         }

         int var4 = (int)(this.pos_y - (float)this.field_50.height / 32.0F);

         int var7;
         for(int var6 = 0; var6 < var1; ++var6) {
            for(var7 = 0; var7 < var2; ++var7) {
               Material var8;
               if (this.field_46 != 0 && this.field_46 != 2) {
                  var8 = this.world.method_198(this.field_47, var4 + var7, var5 + var6);
               } else {
                  var8 = this.world.method_198(var3 + var6, var4 + var7, this.field_49);
               }

               if (!var8.method_345()) {
                  return false;
               }
            }
         }

         List var9 = this.world.hitbox_tree.colliding_entities(this, this.hitbox);

         for(var7 = 0; var7 < var9.size(); ++var7) {
            if (var9.get(var7) instanceof Painting) {
               return false;
            }
         }

         return true;
      }
   }

   // $FF: renamed from: projectiles () boolean
   public final boolean maybe_is_visible() {
      return true;
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, int) boolean
   public final boolean take_damage(Entity var1, int var2) {
      this.destroy();
      this.world.add_entity(new ItemDrop(this.world, this.pos_x, this.pos_y, this.pos_z, new ItemStack(Item.field_198)));
      return true;
   }

   // $FF: renamed from: game (com.game.game.entities) void
   protected final void write_tag_extra(CompoundTag var1) {
      var1.write_byte("Dir", (byte)this.field_46);
      var1.write_string("Motive", this.field_50.name);
      var1.write_int("TileX", this.field_47);
      var1.write_int("TileY", this.field_48);
      var1.write_int("TileZ", this.field_49);
   }

   // $FF: renamed from: game () java.lang.String
   protected final String get_id() {
      return "Painting";
   }

   // $FF: renamed from: animals (com.game.game.entities) void
   protected final void read_tag_extra(CompoundTag var1) {
      this.field_46 = var1.read_byte("Dir");
      this.field_47 = var1.method_452("TileX");
      this.field_48 = var1.method_452("TileY");
      this.field_49 = var1.method_452("TileZ");
      String var6 = var1.method_455("Motive");
      Paintings[] var2;
      int var3 = (var2 = Paintings.values()).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         Paintings var5;
         if ((var5 = var2[var4]).name.equals(var6)) {
            this.field_50 = var5;
         }
      }

      if (this.field_50 == null) {
         this.field_50 = Paintings.kebab;
      }

      this.method_54(this.field_46);
   }
}
