package net.minecraft.game;

import net.minecraft.game.items.ItemStack;

// $FF: renamed from: net.minecraft.game.game
public final class class_220 implements Storage {
   // $FF: renamed from: game java.lang.String
   private String name;
   // $FF: renamed from: animals net.minecraft.game.animals
   private Storage field_993;
   // $FF: renamed from: entities net.minecraft.game.animals
   private Storage field_994;

   public class_220(String var1, Storage var2, Storage var3) {
      this.name = var1;
      this.field_993 = var2;
      this.field_994 = var3;
   }

   // $FF: renamed from: game () int
   public final int capacity() {
      return this.field_993.capacity() + this.field_994.capacity();
   }

   // $FF: renamed from: animals () java.lang.String
   public final String nbtName() {
      return this.name;
   }

   // $FF: renamed from: game (int) net.minecraft.game.animals.k
   public final ItemStack get_slot(int var1) {
      return var1 >= this.field_993.capacity() ? this.field_994.get_slot(var1 - this.field_993.capacity()) : this.field_993.get_slot(var1);
   }

   // $FF: renamed from: game (int, int) net.minecraft.game.animals.k
   public final ItemStack take_from(int var1, int var2) {
      return var1 >= this.field_993.capacity() ? this.field_994.take_from(var1 - this.field_993.capacity(), var2) : this.field_993.take_from(var1, var2);
   }

   // $FF: renamed from: game (int, net.minecraft.game.animals.k) void
   public final void set_slot(int var1, ItemStack var2) {
      if (var1 >= this.field_993.capacity()) {
         this.field_994.set_slot(var1 - this.field_993.capacity(), var2);
      } else {
         this.field_993.set_slot(var1, var2);
      }
   }

   // $FF: renamed from: entities () int
   public final int max_stack_size() {
      return this.field_993.max_stack_size();
   }
}
