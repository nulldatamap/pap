package net.minecraft.game.world;

import a.class_0;
import com.data.nbt.NbtTag;
import com.data.nbt.ListTag;
import com.data.nbt.ShortTag;
import com.data.nbt.CompoundTag;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.zip.GZIPOutputStream;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.world.block.a.DataBlock;
import net.minecraft.game.world.block.a.FurnaceData;
import net.minecraft.game.world.block.a.ChestData;
import net.minecraft.game.entities.Entity;
import net.minecraft.game.entities.Painting;
import net.minecraft.game.entities.monsters.Zombie;
import net.minecraft.game.entities.monsters.Creeper;
import net.minecraft.game.entities.monsters.Spider;
import net.minecraft.game.entities.monsters.Skeleton;
import net.minecraft.game.entities.monsters.Giant;
import net.minecraft.game.entities.animals.Pig;
import net.minecraft.game.entities.animals.Sheep;
import net.minecraft.game.entities.misc.ItemDrop;
import net.minecraft.client.class_126;

// $FF: renamed from: net.minecraft.game.game.game
public abstract class LevelSaverLoader {
   // $FF: renamed from: game game.animals
   private class_0 field_305;

   public LevelSaverLoader(class_0 var1) {
      this.field_305 = var1;
   }

   // $FF: renamed from: game (java.io.InputStream) net.minecraft.game.game.player
   public final World load(InputStream var1) throws IOException {
      if (this.field_305 != null) {
         this.field_305.method_0("Loading world");
      }

      if (this.field_305 != null) {
         this.field_305.method_1("Reading..");
      }

      CompoundTag var13;
      CompoundTag var2 = (var13 = class_126.method_334(var1)).method_457("About");
      CompoundTag var3 = var13.method_457("Map");
      CompoundTag var4 = var13.method_457("Environment");
      ListTag var5 = var13.read_list("Entities");
      short var6 = var3.read_short("Width");
      short var7 = var3.read_short("Length");
      short var8 = var3.read_short("Height");
      World var9 = new World();
      if (this.field_305 != null) {
         this.field_305.method_1("Preparing world..");
      }

      ListTag var10 = var3.read_list("Spawn");
      var9.spawn_x = ((ShortTag)var10.get(0)).field_750;
      var9.spawn_y = ((ShortTag)var10.get(1)).field_750;
      var9.spawn_z = ((ShortTag)var10.get(2)).field_750;
      var9.world_author = var2.method_455("Author");
      var9.world_name = var2.method_455("Name");
      var9.time_stamp = var2.method_453("CreatedOn");
      var9.cloud_color = var4.method_452("CloudColor");
      var9.sky_color = var4.method_452("SkyColor");
      var9.fog_color = var4.method_452("FogColor");
      var9.sky_brightness = var4.read_byte("SkyBrightness");
      if (var9.sky_brightness < 0) {
         var9.sky_brightness = 0;
      }

      if (var9.sky_brightness > 16) {
         var9.sky_brightness = var9.sky_brightness * 15 / 100;
      }

      var9.cloud_height = var4.read_short("CloudHeight");
      var9.surrounding_ground_height = var4.read_short("SurroundingGroundHeight");
      var9.surrounding_water_height = var4.read_short("SurroundingWaterHeight");
      var9.surrounding_water_type = var4.read_byte("SurroundingWaterType");
      var9.time_of_day = var4.read_short("TimeOfDay");
      var9.maybe_brightness = var9.method_178();
      var9.method_158(var6, var8, var7, var3.method_456("Blocks"), var3.method_456("Data"));
      if (this.field_305 != null) {
         this.field_305.method_1("Preparing entities..");
      }

      for(int var16 = 0; var16 < var5.method_438(); ++var16) {
         try {
            String var19 = (var3 = (CompoundTag)var5.get(var16)).method_455("id");
            Entity var22;
            if ((var22 = this.load_entity(var9, var19)) != null) {
               var22.read_tag(var3);
               var9.add_entity(var22);
            } else {
               System.out.println("Skipping unknown entity id \"" + var19 + "\"");
            }
         } catch (Exception var12) {
            System.out.println("Error reading entity");
            var12.printStackTrace();
         }
      }

      ListTag var17 = var13.read_list("TileEntities");

      for(int var18 = 0; var18 < var17.method_438(); ++var18) {
         try {
            int var23 = (var4 = (CompoundTag)var17.get(var18)).method_452("Pos");
            String var14;
            String var20;
            Object var21;
            if ((var21 = (var20 = var14 = var4.method_455("id")).equals("Chest") ? new ChestData() : (var20.equals("Furnace") ? new FurnaceData() : null)) != null) {
               int var15 = var23 % 1024;
               int var24 = (var23 >> 10) % 1024;
               var23 = (var23 >> 20) % 1024;
               ((DataBlock)var21).method_256(var4);
               var9.method_214(var15, var24, var23, (DataBlock)var21);
            } else {
               System.out.println("Skipping unknown tile entity id \"" + var14 + "\"");
            }
         } catch (Exception var11) {
            System.out.println("Error reading tileentity");
            var11.printStackTrace();
         }
      }

      return var9;
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, java.lang.String) net.minecraft.game.entities.animals
   protected Entity load_entity(World var1, String var2) {
      if (var2.equals("Pig")) {
         return new Pig(var1);
      } else if (var2.equals("Sheep")) {
         return new Sheep(var1);
      } else if (var2.equals("Creeper")) {
         return new Creeper(var1);
      } else if (var2.equals("Skeleton")) {
         return new Skeleton(var1);
      } else if (var2.equals("Spider")) {
         return new Spider(var1);
      } else if (var2.equals("Zombie")) {
         return new Zombie(var1);
      } else if (var2.equals("Giant")) {
         return new Giant(var1);
      } else if (var2.equals("Item")) {
         return new ItemDrop(var1);
      } else {
         return var2.equals("Painting") ? new Painting(var1) : null;
      }
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, java.io.OutputStream) void
   public final void save(World var1, OutputStream var2) throws IOException {
      if (this.field_305 != null) {
         this.field_305.method_0("Saving world");
      }

      if (this.field_305 != null) {
         this.field_305.method_1("Preparing world..");
      }

      CompoundTag var3;
      (var3 = new CompoundTag()).write_int("CloudColor", var1.cloud_color);
      var3.write_int("SkyColor", var1.sky_color);
      var3.write_int("FogColor", var1.fog_color);
      var3.write_byte("SkyBrightness", (byte)var1.sky_brightness);
      var3.write_short("CloudHeight", (short)var1.cloud_height);
      var3.write_short("SurroundingGroundHeight", (short)var1.surrounding_ground_height);
      var3.write_short("SurroundingWaterHeight", (short)var1.surrounding_water_height);
      var3.write_byte("SurroundingGroundType", (byte) Block.grass.block_id);
      var3.write_byte("SurroundingWaterType", (byte)var1.surrounding_water_type);
      var3.write_short("TimeOfDay", (short)var1.time_of_day);
      CompoundTag var4;
      (var4 = new CompoundTag()).write_short("Width", (short)var1.width);
      var4.write_short("Length", (short)var1.depth);
      var4.write_short("Height", (short)var1.height);
      var4.method_446("Blocks", var1.blocks);
      var4.method_446("Data", var1.data);
      ListTag var5;
      (var5 = new ListTag()).method_436(new ShortTag((short)var1.spawn_x));
      var5.method_436(new ShortTag((short)var1.spawn_y));
      var5.method_436(new ShortTag((short)var1.spawn_z));
      var4.write_list("Spawn", var5);
      CompoundTag var15;
      (var15 = new CompoundTag()).write_string("Author", var1.world_author);
      var15.write_string("Name", var1.world_name);
      var15.write_long("CreatedOn", var1.time_stamp);
      if (this.field_305 != null) {
         this.field_305.method_1("Preparing entities..");
      }

      ListTag var6 = new ListTag();
      Iterator var7 = var1.hitbox_tree.entities.iterator();

      while(var7.hasNext()) {
         Entity var8 = (Entity)var7.next();
         CompoundTag var9 = new CompoundTag();
         var8.write_to_tag(var9);
         if (!var9.method_460()) {
            var6.method_436(var9);
         }
      }

      ListTag var16 = new ListTag();
      Iterator var17 = var1.data_block_map.keySet().iterator();

      while(var17.hasNext()) {
         int var19 = ((Integer)var17.next()).intValue();
         CompoundTag var10;
         (var10 = new CompoundTag()).write_int("Pos", var19);
         ((DataBlock)var1.data_block_map.get(var19)).method_257(var10);
         var16.method_436(var10);
      }

      CompoundTag var18;
      (var18 = new CompoundTag()).setName("MinecraftLevel");
      var18.method_447("About", var15);
      var18.method_447("Map", var4);
      var18.method_447("Environment", var3);
      var18.write_list("Entities", var6);
      var18.write_list("TileEntities", var16);
      if (this.field_305 != null) {
         this.field_305.method_1("Writing..");
      }

      CompoundTag var13 = var18;
      DataOutputStream var14 = new DataOutputStream(new GZIPOutputStream(var2));

      try {
         NbtTag.method_434(var13, var14);
      } finally {
         var14.close();
      }

   }
}
