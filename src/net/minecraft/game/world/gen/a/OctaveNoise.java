package net.minecraft.game.world.gen.a;

import java.util.Random;

// $FF: renamed from: net.minecraft.game.game.entities.game.entities
public final class OctaveNoise extends Noise {
   // $FF: renamed from: game net.minecraft.game.game.entities.game.game[]
   private PerlinNoise[] field_263;
   // $FF: renamed from: animals int
   private int field_264;

   public OctaveNoise(Random var1, int var2) {
      this.field_264 = var2;
      this.field_263 = new PerlinNoise[var2];

      for(int var3 = 0; var3 < var2; ++var3) {
         this.field_263[var3] = new PerlinNoise(var1);
      }

   }

   // $FF: renamed from: game (double, double) double
   public final double sample_noise(double var1, double var3) {
      double var5 = 0.0D;
      double var7 = 1.0D;

      for(int var9 = 0; var9 < this.field_264; ++var9) {
         var5 += this.field_263[var9].sample_noise(var1 / var7, var3 / var7) * var7;
         var7 *= 2.0D;
      }

      return var5;
   }
}
