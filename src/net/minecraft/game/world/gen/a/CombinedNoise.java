package net.minecraft.game.world.gen.a;

// $FF: renamed from: net.minecraft.game.game.entities.game.projectiles
public final class CombinedNoise extends Noise {
   // $FF: renamed from: game net.minecraft.game.game.entities.game.animals
   private Noise a;
   // $FF: renamed from: animals net.minecraft.game.game.entities.game.animals
   private Noise b;

   public CombinedNoise(Noise var1, Noise var2) {
      this.a = var1;
      this.b = var2;
   }

   // $FF: renamed from: game (double, double) double
   public final double sample_noise(double x, double y) {
      return this.a.sample_noise(x + this.b.sample_noise(x, y), y);
   }
}
