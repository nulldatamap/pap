package net.minecraft.game.world.gen.a;

import a.MathHelper;
import java.util.Random;

// $FF: renamed from: net.minecraft.game.game.entities.game.game
public final class PerlinNoise extends Noise {
   // $FF: renamed from: game int[]
   private int[] field_262;

   public PerlinNoise() {
      this(new Random());
   }

   public PerlinNoise(Random var1) {
      this.field_262 = new int[512];

      int var2;
      for(var2 = 0; var2 < 256; this.field_262[var2] = var2++) {
         ;
      }

      for(var2 = 0; var2 < 256; ++var2) {
         int var3 = var1.nextInt(256 - var2) + var2;
         int var4 = this.field_262[var2];
         this.field_262[var2] = this.field_262[var3];
         this.field_262[var3] = var4;
         this.field_262[var2 + 256] = this.field_262[var2];
      }

   }

   // $FF: renamed from: game (double) double
   private static double method_130(double var0) {
      return var0 * var0 * var0 * (var0 * (var0 * 6.0D - 15.0D) + 10.0D);
   }

   // $FF: renamed from: game (double, double, double) double
   private static double method_131(double var0, double var2, double var4) {
      return var2 + var0 * (var4 - var2);
   }

   // $FF: renamed from: game (int, double, double, double) double
   private static double method_132(int var0, double var1, double var3, double var5) {
      double var8 = (var0 &= 15) < 8 ? var1 : var3;
      double var10 = var0 < 4 ? var3 : (var0 != 12 && var0 != 14 ? var5 : var1);
      return ((var0 & 1) == 0 ? var8 : -var8) + ((var0 & 2) == 0 ? var10 : -var10);
   }

   // $FF: renamed from: game (double, double) double
   public final double sample_noise(double var1, double var3) {
      double var10 = 0.0D;
      double var8 = var3;
      int var2 = MathHelper.floord(var1) & 255;
      int var21 = MathHelper.floord(var3) & 255;
      int var4 = MathHelper.floord(0.0D) & 255;
      double var6 = var1 - (double) MathHelper.floord(var1);
      var8 -= (double) MathHelper.floord(var8);
      var10 = 0.0D - (double) MathHelper.floord(0.0D);
      double var15 = method_130(var6);
      double var17 = method_130(var8);
      double var19 = method_130(var10);
      int var5 = this.field_262[var2] + var21;
      int var12 = this.field_262[var5] + var4;
      var5 = this.field_262[var5 + 1] + var4;
      var2 = this.field_262[var2 + 1] + var21;
      var21 = this.field_262[var2] + var4;
      var2 = this.field_262[var2 + 1] + var4;
      return method_131(var19, method_131(var17, method_131(var15, method_132(this.field_262[var12], var6, var8, var10), method_132(this.field_262[var21], var6 - 1.0D, var8, var10)), method_131(var15, method_132(this.field_262[var5], var6, var8 - 1.0D, var10), method_132(this.field_262[var2], var6 - 1.0D, var8 - 1.0D, var10))), method_131(var17, method_131(var15, method_132(this.field_262[var12 + 1], var6, var8, var10 - 1.0D), method_132(this.field_262[var21 + 1], var6 - 1.0D, var8, var10 - 1.0D)), method_131(var15, method_132(this.field_262[var5 + 1], var6, var8 - 1.0D, var10 - 1.0D), method_132(this.field_262[var2 + 1], var6 - 1.0D, var8 - 1.0D, var10 - 1.0D))));
   }
}
