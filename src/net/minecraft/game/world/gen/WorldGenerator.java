package net.minecraft.game.world.gen;

import a.class_0;
import a.MathHelper;
import java.util.ArrayList;
import java.util.Random;

import net.minecraft.game.world.World;
import net.minecraft.game.world.MobSpawner;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.world.block.class_208;
import net.minecraft.game.world.gen.a.OctaveNoise;
import net.minecraft.game.world.gen.a.CombinedNoise;

// $FF: renamed from: net.minecraft.game.game.entities.game
public final class WorldGenerator {
   // $FF: renamed from: player game.animals
   private class_0 status_label;
   // $FF: renamed from: particles int
   private int x_size;
   // $FF: renamed from: player int
   private int y_size;
   // $FF: renamed from: h int
   private int z_size;
   // $FF: renamed from: i java.util.Random
   private Random random = new Random();
   // $FF: renamed from: j byte[]
   private byte[] data;
   // $FF: renamed from: k int
   private int surrounding_water_height;
   // $FF: renamed from: l int
   private int surrounding_ground_height;
   // $FF: renamed from: game boolean
   public boolean is_island = false;
   // $FF: renamed from: animals boolean
   public boolean is_floating = false;
   // $FF: renamed from: entities boolean
   public boolean is_float = false;
   // $FF: renamed from: projectiles int
   public int world_theme;
   // $FF: renamed from: m int
   private int something_y;
   // $FF: renamed from: n int
   private int something_z;
   // $FF: renamed from: o float
   private float field_1029 = 0.0F;
   // $FF: renamed from: p int[]
   private int[] field_1030 = new int[1048576];

   public WorldGenerator(class_0 var1) {
      this.status_label = var1;
   }

   // $FF: renamed from: game (java.lang.String, int, int, int) net.minecraft.game.game.player
   public final World generate_world(String var1, int x_size, int y_size, int z_size) {
      int related_to_z_size = 1;
      if (this.is_floating) {
         related_to_z_size = (z_size - 64) / 48 + 1;
      }

      this.something_z = 13 + related_to_z_size * 4;
      this.status_label.method_0("Generating world");
      World world;
      (world = new World()).surrounding_water_height = this.surrounding_water_height;
      world.surrounding_ground_height = this.surrounding_ground_height;
      this.x_size = x_size;
      this.y_size = y_size;
      this.z_size = z_size;
      this.data = new byte[x_size * y_size * z_size];

      int var7;
      WorldGenerator gen;
      int var21;
      int var25;
      int var31;
      int i;
      int var51;
      int var52;
      int var53;
      int var56;
      for(var7 = 0; var7 < related_to_z_size; ++var7) {
         this.surrounding_water_height = z_size - 32 - var7 * 48;
         this.surrounding_ground_height = this.surrounding_water_height - 2;
         int[] var8;
         OctaveNoise var13;
         int var22;
         int[] var46;
         if (this.is_float) {
            var8 = new int[x_size * y_size];

            for(i = 0; i < var8.length; ++i) {
               var8[i] = 0;
            }

            this.update_loading_bar();
            this.update_loading_bar();
         } else {
            this.status_label.method_1("Raising..");
            this.update_loading_bar();
            gen = this;
            CombinedNoise var10 = new CombinedNoise(new OctaveNoise(this.random, 8), new OctaveNoise(this.random, 8));
            CombinedNoise var11 = new CombinedNoise(new OctaveNoise(this.random, 8), new OctaveNoise(this.random, 8));
            OctaveNoise var12 = new OctaveNoise(this.random, 6);
            var13 = new OctaveNoise(this.random, 2);
            int[] height_map = new int[this.x_size * this.y_size];
            var22 = 0;

            label349:
            while(true) {
               if (var22 >= gen.x_size) {
                  var8 = height_map;
                  this.status_label.method_1("Eroding..");
                  this.update_loading_bar();
                  var46 = height_map;
                  gen = this;
                  var11 = new CombinedNoise(new OctaveNoise(this.random, 8), new OctaveNoise(this.random, 8));
                  CombinedNoise var50 = new CombinedNoise(new OctaveNoise(this.random, 8), new OctaveNoise(this.random, 8));
                  var52 = 0;

                  while(true) {
                     if (var52 >= gen.x_size) {
                        break label349;
                     }

                     gen.method_581((float)var52 * 100.0F / (float)(gen.x_size - 1));

                     for(var53 = 0; var53 < gen.y_size; ++var53) {
                        double var20 = var11.sample_noise((double)(var52 << 1), (double)(var53 << 1)) / 8.0D;
                        var22 = var50.sample_noise((double)(var52 << 1), (double)(var53 << 1)) > 0.0D ? 1 : 0;
                        if (var20 > 2.0D) {
                           int var58 = ((var46[var52 + var53 * gen.x_size] - var22) / 2 << 1) + var22;
                           var46[var52 + var53 * gen.x_size] = var58;
                        }
                     }

                     ++var52;
                  }
               }

               double var23 = Math.abs(((double)var22 / ((double)gen.x_size - 1.0D) - 0.5D) * 2.0D);
               gen.method_581((float)var22 * 100.0F / (float)(gen.x_size - 1));

               for(var25 = 0; var25 < gen.y_size; ++var25) {
                  double var26 = Math.abs(((double)var25 / ((double)gen.y_size - 1.0D) - 0.5D) * 2.0D);
                  double var28 = var10.sample_noise((double)((float)var22 * 1.3F), (double)((float)var25 * 1.3F)) / 6.0D + -4.0D;
                  double var30 = var11.sample_noise((double)((float)var22 * 1.3F), (double)((float)var25 * 1.3F)) / 5.0D + 10.0D + -4.0D;
                  if (var12.sample_noise((double)var22, (double)var25) / 8.0D > 0.0D) {
                     var30 = var28;
                  }

                  double var34 = Math.max(var28, var30) / 2.0D;
                  if (gen.is_island) {
                     double var36 = Math.sqrt(var23 * var23 + var26 * var26) * 1.2000000476837158D;
                     double var39 = var13.sample_noise((double)((float)var22 * 0.05F), (double)((float)var25 * 0.05F)) / 4.0D + 1.0D;
                     if ((var36 = Math.max(Math.min(var36, var39), Math.max(var23, var26))) > 1.0D) {
                        var36 = 1.0D;
                     }

                     if (var36 < 0.0D) {
                        var36 = 0.0D;
                     }

                     var36 *= var36;
                     if ((var34 = var34 * (1.0D - var36) - var36 * 10.0D + 5.0D) < 0.0D) {
                        var34 -= var34 * var34 * 0.20000000298023224D;
                     }
                  } else if (var34 < 0.0D) {
                     var34 *= 0.8D;
                  }

                  height_map[var22 + var25 * gen.x_size] = (int)var34;
               }

               ++var22;
            }
         }

         this.status_label.method_1("Soiling..");
         this.update_loading_bar();
         var46 = var8;
         gen = this;
         int var49 = this.x_size;
         var51 = this.y_size;
         var52 = this.z_size;
         OctaveNoise var54 = new OctaveNoise(this.random, 8);
         OctaveNoise var55 = new OctaveNoise(this.random, 8);

         for(var21 = 0; var21 < var49; ++var21) {
            double var57 = Math.abs(((double)var21 / ((double)var49 - 1.0D) - 0.5D) * 2.0D);
            gen.method_581((float)var21 * 100.0F / (float)(var49 - 1));

            for(int var24 = 0; var24 < var51; ++var24) {
               double var64 = Math.abs(((double)var24 / ((double)var51 - 1.0D) - 0.5D) * 2.0D);
               double var27 = (var27 = Math.max(var57, var64)) * var27 * var27;
               int var29 = (int)(var54.sample_noise((double)var21, (double)var24) / 24.0D) - 4;
               int var72;
               var31 = (var72 = var46[var21 + var24 * var49] + gen.surrounding_water_height) + var29;
               var46[var21 + var24 * var49] = Math.max(var72, var31);
               if (var46[var21 + var24 * var49] > var52 - 2) {
                  var46[var21 + var24 * var49] = var52 - 2;
               }

               if (var46[var21 + var24 * var49] <= 0) {
                  var46[var21 + var24 * var49] = 1;
               }

               double var32;
               int var76;
               if ((var76 = (int)((double)((int)(Math.sqrt(Math.abs(var32 = var55.sample_noise((double)var21 * 2.3D, (double)var24 * 2.3D) / 24.0D)) * Math.signum(var32) * 20.0D) + gen.surrounding_water_height) * (1.0D - var27) + var27 * (double)gen.z_size)) > gen.surrounding_water_height) {
                  var76 = gen.z_size;
               }

               for(int var35 = 0; var35 < var52; ++var35) {
                  int var78 = (var35 * gen.y_size + var24) * gen.x_size + var21;
                  int var37 = 0;
                  if (var35 <= var72) {
                     var37 = Block.dirt.block_id;
                  }

                  if (var35 <= var31) {
                     var37 = Block.stone.block_id;
                  }

                  if (gen.is_floating && var35 < var76) {
                     var37 = 0;
                  }

                  if (gen.data[var78] == 0) {
                     gen.data[var78] = (byte)var37;
                  }
               }
            }
         }

         this.status_label.method_1("Growing..");
         this.update_loading_bar();
         var46 = var8;
         gen = this;
         var49 = this.x_size;
         var51 = this.y_size;
         var13 = new OctaveNoise(this.random, 8);
         var54 = new OctaveNoise(this.random, 8);
         var56 = this.surrounding_water_height - 1;
         if (this.world_theme == 2) {
            var56 += 2;
         }

         for(var21 = 0; var21 < var49; ++var21) {
            gen.method_581((float)var21 * 100.0F / (float)(var49 - 1));

            for(var22 = 0; var22 < var51; ++var22) {
               boolean var59 = var13.sample_noise((double)var21, (double)var22) > 8.0D;
               if (gen.is_island) {
                  var59 = var13.sample_noise((double)var21, (double)var22) > -8.0D;
               }

               if (gen.world_theme == 2) {
                  var59 = var13.sample_noise((double)var21, (double)var22) > -32.0D;
               }

               boolean var61 = var54.sample_noise((double)var21, (double)var22) > 12.0D;
               if (gen.world_theme == 1 || gen.world_theme == 3) {
                  var59 = var13.sample_noise((double)var21, (double)var22) > -8.0D;
               }

               int var65 = ((var25 = var46[var21 + var22 * var49]) * gen.y_size + var22) * gen.x_size + var21;
               int var67;
               if (((var67 = gen.data[((var25 + 1) * gen.y_size + var22) * gen.x_size + var21] & 255) == Block.water.block_id || var67 == Block.infinite_water.block_id || var67 == 0) && var25 <= gen.surrounding_water_height - 1 && var61) {
                  gen.data[var65] = (byte) Block.field_880.block_id;
               }

               if (var67 == 0) {
                  int var69 = -1;
                  if (var25 <= var56 && var59) {
                     var69 = Block.sand.block_id;
                     if (gen.world_theme == 1) {
                        var69 = Block.grass.block_id;
                     }
                  }

                  if (gen.data[var65] != 0 && var69 > 0) {
                     gen.data[var65] = (byte)var69;
                  }
               }
            }
         }
      }

      this.status_label.method_1("Carving..");
      this.update_loading_bar();
      gen = this;
      var51 = this.x_size;
      var52 = this.y_size;
      var53 = this.z_size;
      var56 = var51 * var52 * var53 / 256 / 64 << 1;

      for(var21 = 0; var21 < var56; ++var21) {
         gen.method_581((float)var21 * 100.0F / (float)(var56 - 1));
         float var60 = gen.random.nextFloat() * (float)var51;
         float var63 = gen.random.nextFloat() * (float)var53;
         float var62 = gen.random.nextFloat() * (float)var52;
         var25 = (int)((gen.random.nextFloat() + gen.random.nextFloat()) * 200.0F);
         float var66 = gen.random.nextFloat() * 3.1415927F * 2.0F;
         float var68 = 0.0F;
         float var71 = gen.random.nextFloat() * 3.1415927F * 2.0F;
         float var70 = 0.0F;
         float var73 = gen.random.nextFloat() * gen.random.nextFloat();

         for(var31 = 0; var31 < var25; ++var31) {
            var60 += MathHelper.sin(var66) * MathHelper.cos(var71);
            var62 += MathHelper.cos(var66) * MathHelper.cos(var71);
            var63 += MathHelper.sin(var71);
            var66 += var68 * 0.2F;
            var68 = (var68 *= 0.9F) + (gen.random.nextFloat() - gen.random.nextFloat());
            var71 = (var71 + var70 * 0.5F) * 0.5F;
            var70 = (var70 *= 0.75F) + (gen.random.nextFloat() - gen.random.nextFloat());
            if (gen.random.nextFloat() >= 0.25F) {
               float var74 = var60 + (gen.random.nextFloat() * 4.0F - 2.0F) * 0.2F;
               float var33 = var63 + (gen.random.nextFloat() * 4.0F - 2.0F) * 0.2F;
               float var77 = var62 + (gen.random.nextFloat() * 4.0F - 2.0F) * 0.2F;
               float var75 = ((float)gen.z_size - var33) / (float)gen.z_size;
               float var79 = 1.2F + (var75 * 3.5F + 1.0F) * var73;
               float var80 = MathHelper.sin((float)var31 * 3.1415927F / (float)var25) * var79;

               for(related_to_z_size = (int)(var74 - var80); related_to_z_size <= (int)(var74 + var80); ++related_to_z_size) {
                  for(int var81 = (int)(var33 - var80); var81 <= (int)(var33 + var80); ++var81) {
                     for(int var40 = (int)(var77 - var80); var40 <= (int)(var77 + var80); ++var40) {
                        float var41 = (float)related_to_z_size - var74;
                        float var42 = (float)var81 - var33;
                        float var48 = (float)var40 - var77;
                        if (var41 * var41 + var42 * var42 * 2.0F + var48 * var48 < var80 * var80 && related_to_z_size > 0 && var81 > 0 && var40 > 0 && related_to_z_size < gen.x_size - 1 && var81 < gen.z_size - 1 && var40 < gen.y_size - 1) {
                           var7 = (var81 * gen.y_size + var40) * gen.x_size + related_to_z_size;
                           if (gen.data[var7] == Block.stone.block_id) {
                              gen.data[var7] = 0;
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      var7 = this.method_578(Block.coal.block_id, 1000, 10, (z_size << 2) / 5);
      int var44 = this.method_578(Block.iron.block_id, 800, 8, z_size * 3 / 5);
      i = this.method_578(Block.gold.block_id, 500, 6, (z_size << 1) / 5);
      related_to_z_size = this.method_578(Block.diamond .block_id, 800, 2, z_size / 5);
      System.out.println("Coal: " + var7 + ", Iron: " + var44 + ", Gold: " + i + ", Diamond: " + related_to_z_size);
      this.status_label.method_1("Melting..");
      this.update_loading_bar();
      this.method_582();
      world.cloud_height = z_size + 2;
      if (this.is_floating) {
         this.surrounding_ground_height = -128;
         this.surrounding_water_height = this.surrounding_ground_height + 1;
         world.cloud_height = -16;
      } else if (!this.is_island) {
         this.surrounding_ground_height = this.surrounding_water_height + 1;
         this.surrounding_water_height = this.surrounding_ground_height - 16;
      } else {
         this.surrounding_ground_height = this.surrounding_water_height - 9;
      }

      this.status_label.method_1("Watering..");
      this.update_loading_bar();
      this.method_579();
      if (!this.is_floating) {
         related_to_z_size = Block.infinite_water.block_id;
         if (this.world_theme == 1) {
            related_to_z_size = Block.infinite_lava.block_id;
         }

         for(var7 = 0; var7 < x_size; ++var7) {
            this.method_583(var7, this.surrounding_water_height - 1, 0, 0, related_to_z_size);
            this.method_583(var7, this.surrounding_water_height - 1, y_size - 1, 0, related_to_z_size);
         }

         for(var7 = 0; var7 < y_size; ++var7) {
            this.method_583(x_size - 1, this.surrounding_water_height - 1, var7, 0, related_to_z_size);
            this.method_583(0, this.surrounding_water_height - 1, var7, 0, related_to_z_size);
         }
      }

      // Normal
      if (this.world_theme == 0) {
         world.sky_color = 10079487;
         world.fog_color = 16777215;
         world.cloud_color = 16777215;
      }

      // Hell
      if (this.world_theme == 1) {
         world.cloud_color = 2164736;
         world.fog_color = 1049600;
         world.sky_color = 1049600;
         world.maybe_brightness = world.sky_brightness = 7;
         world.surrounding_water_type = Block.lava.block_id;
         if (this.is_floating) {
            world.cloud_height = z_size + 2;
            this.surrounding_water_height = -16;
         }
      }

      // Paradise
      if (this.world_theme == 2) {
         world.sky_color = 13033215;
         world.fog_color = 13033215;
         world.cloud_color = 15658751;
         world.maybe_brightness = world.sky_brightness = 15;
         world.sky_brightness = 16;
         world.cloud_height = z_size + 64;
      }

      // Woods
      if (this.world_theme == 3) {
         world.sky_color = 7699847;
         world.fog_color = 5069403;
         world.cloud_color = 5069403;
         world.maybe_brightness = world.sky_brightness = 12;
      }

      world.surrounding_water_height = this.surrounding_water_height;
      world.surrounding_ground_height = this.surrounding_ground_height;
      this.status_label.method_1("Assembling..");
      this.update_loading_bar();
      this.method_581(0.0F);
      world.method_158(x_size, z_size, y_size, this.data, (byte[])null);
      this.status_label.method_1("Building..");
      this.update_loading_bar();
      this.method_581(0.0F);
      world.method_159();
      create_spawn_hut(world);
      this.status_label.method_1("Planting..");
      this.update_loading_bar();
      if (this.world_theme != 1) {
         this.method_575(world);
      }

      this.update_loading_bar();
      this.method_576(world);
      if (this.world_theme == 3) {
         for(related_to_z_size = 0; related_to_z_size < 50; ++related_to_z_size) {
            this.method_576(world);
         }
      }

      short var43 = 100;
      if (this.world_theme == 2) {
         var43 = 1000;
      }

      this.update_loading_bar();
      this.method_577(world, Block.field_904, var43);
      this.update_loading_bar();
      this.method_577(world, Block.field_905, var43);
      this.update_loading_bar();
      this.method_577(world, Block.field_906, 50);
      this.update_loading_bar();
      this.method_577(world, Block.field_907, 50);
      this.status_label.method_1("Lighting..");
      this.update_loading_bar();

      for(var7 = 0; var7 < 10000; ++var7) {
         this.method_581((float)(var7 * 100 / 10000));
         world.method_172();
      }

      this.status_label.method_1("Spawning..");
      this.update_loading_bar();
      MobSpawner var47 = new MobSpawner(world);

      for(x_size = 0; x_size < 1000; ++x_size) {
         this.method_581((float)x_size * 100.0F / 999.0F);
         var47.spawn_mobs();
      }

      world.time_stamp = System.currentTimeMillis();
      world.world_author = var1;
      world.world_name = "A Nice World";
      if (this.something_y != this.something_z) {
         throw new IllegalStateException("Wrong number of phases! Wanted " + this.something_z + ", got " + this.something_y);
      } else {
         return world;
      }
   }

   // $FF: renamed from: game (net.minecraft.game.game.player) void
   private static void create_spawn_hut(World var0) {
      int var1 = var0.spawn_x;
      int var2 = var0.spawn_y;
      int var3 = var0.spawn_z;

      for(int var4 = var1 - 3; var4 <= var1 + 3; ++var4) {
         for(int var5 = var2 - 2; var5 <= var2 + 2; ++var5) {
            for(int var6 = var3 - 3; var6 <= var3 + 3; ++var6) {
               int var7 = var5 < var2 - 1 ? Block.field_916.block_id : 0;
               if (var4 == var1 - 3 || var6 == var3 - 3 || var4 == var1 + 3 || var6 == var3 + 3 || var5 == var2 - 2 || var5 == var2 + 2) {
                  var7 = Block.stone.block_id;
                  if (var5 >= var2 - 1) {
                     var7 = Block.wood_planks.block_id;
                  }
               }

               if (var6 == var3 - 3 && var4 == var1 && var5 >= var2 - 1 && var5 <= var2) {
                  var7 = 0;
               }

               var0.method_165(var4, var5, var6, var7);
            }
         }
      }

      var0.method_165(var1 - 3 + 1, var2, var3, Block.field_917.block_id);
      var0.method_165(var1 + 3 - 1, var2, var3, Block.field_917.block_id);
   }

   // $FF: renamed from: animals (net.minecraft.game.game.player) void
   private void method_575(World var1) {
      for(int var2 = 0; var2 < this.x_size; ++var2) {
         this.method_581((float)var2 * 100.0F / (float)(this.x_size - 1));

         for(int var3 = 0; var3 < this.z_size; ++var3) {
            for(int var4 = 0; var4 < this.y_size; ++var4) {
               if (var1.block_at(var2, var3, var4) == Block.dirt.block_id && var1.method_195(var2, var3 + 1, var4) >= 4 && !var1.method_198(var2, var3 + 1, var4).method_346()) {
                  var1.method_164(var2, var3, var4, Block.grass.block_id);
               }
            }
         }
      }

   }

   // $FF: renamed from: entities (net.minecraft.game.game.player) void
   private void method_576(World var1) {
      int var2 = this.x_size * this.y_size * this.z_size / 80000;

      for(int var3 = 0; var3 < var2; ++var3) {
         if (var3 % 100 == 0) {
            this.method_581((float)var3 * 100.0F / (float)(var2 - 1));
         }

         int var4 = this.random.nextInt(this.x_size);
         int var5 = this.random.nextInt(this.z_size);
         int var6 = this.random.nextInt(this.y_size);

         for(int var7 = 0; var7 < 25; ++var7) {
            int var8 = var4;
            int var9 = var5;
            int var10 = var6;

            for(int var11 = 0; var11 < 20; ++var11) {
               var8 += this.random.nextInt(12) - this.random.nextInt(12);
               var9 += this.random.nextInt(3) - this.random.nextInt(6);
               var10 += this.random.nextInt(12) - this.random.nextInt(12);
               if (var8 >= 0 && var9 >= 0 && var10 >= 0 && var8 < this.x_size && var9 < this.z_size && var10 < this.y_size) {
                  var1.method_201(var8, var9, var10);
               }
            }
         }
      }

   }

   // $FF: renamed from: game (net.minecraft.game.game.player, net.minecraft.game.game.animals.ad, int) void
   private void method_577(World var1, class_208 var2, int var3) {
      var3 = (int)((long)this.x_size * (long)this.y_size * (long)this.z_size * (long)var3 / 1600000L);

      for(int var4 = 0; var4 < var3; ++var4) {
         if (var4 % 100 == 0) {
            this.method_581((float)var4 * 100.0F / (float)(var3 - 1));
         }

         int var5 = this.random.nextInt(this.x_size);
         int var6 = this.random.nextInt(this.z_size);
         int var7 = this.random.nextInt(this.y_size);

         for(int var8 = 0; var8 < 10; ++var8) {
            int var9 = var5;
            int var10 = var6;
            int var11 = var7;

            for(int var12 = 0; var12 < 10; ++var12) {
               var9 += this.random.nextInt(4) - this.random.nextInt(4);
               var10 += this.random.nextInt(2) - this.random.nextInt(2);
               var11 += this.random.nextInt(4) - this.random.nextInt(4);
               if (var9 >= 0 && var11 >= 0 && var10 > 0 && var9 < this.x_size && var11 < this.y_size && var10 < this.z_size && var1.block_at(var9, var10, var11) == 0 && var2.method_552(var1, var9, var10, var11)) {
                  var1.method_165(var9, var10, var11, var2.block_id);
               }
            }
         }
      }

   }

   // $FF: renamed from: game (int, int, int, int) int
   private int method_578(int var1, int var2, int var3, int var4) {
      int var5 = 0;
      byte var26 = (byte)var1;
      int var6 = this.x_size;
      int var7 = this.y_size;
      int var8 = this.z_size;
      var2 = var6 * var7 * var8 / 256 / 64 * var2 / 100;

      for(int var9 = 0; var9 < var2; ++var9) {
         this.method_581((float)var9 * 100.0F / (float)(var2 - 1));
         float var10 = this.random.nextFloat() * (float)var6;
         float var11 = this.random.nextFloat() * (float)var8;
         float var12 = this.random.nextFloat() * (float)var7;
         if (var11 <= (float)var4) {
            int var13 = (int)((this.random.nextFloat() + this.random.nextFloat()) * 75.0F * (float)var3 / 100.0F);
            float var14 = this.random.nextFloat() * 3.1415927F * 2.0F;
            float var15 = 0.0F;
            float var16 = this.random.nextFloat() * 3.1415927F * 2.0F;
            float var17 = 0.0F;

            for(int var18 = 0; var18 < var13; ++var18) {
               var10 += MathHelper.sin(var14) * MathHelper.cos(var16);
               var12 += MathHelper.cos(var14) * MathHelper.cos(var16);
               var11 += MathHelper.sin(var16);
               var14 += var15 * 0.2F;
               var15 = (var15 *= 0.9F) + (this.random.nextFloat() - this.random.nextFloat());
               var16 = (var16 + var17 * 0.5F) * 0.5F;
               var17 = (var17 *= 0.9F) + (this.random.nextFloat() - this.random.nextFloat());
               float var19 = MathHelper.sin((float)var18 * 3.1415927F / (float)var13) * (float)var3 / 100.0F + 1.0F;

               for(int var20 = (int)(var10 - var19); var20 <= (int)(var10 + var19); ++var20) {
                  for(int var21 = (int)(var11 - var19); var21 <= (int)(var11 + var19); ++var21) {
                     for(int var22 = (int)(var12 - var19); var22 <= (int)(var12 + var19); ++var22) {
                        float var23 = (float)var20 - var10;
                        float var24 = (float)var21 - var11;
                        float var25 = (float)var22 - var12;
                        if (var23 * var23 + var24 * var24 * 2.0F + var25 * var25 < var19 * var19 && var20 > 0 && var21 > 0 && var22 > 0 && var20 < this.x_size - 1 && var21 < this.z_size - 1 && var22 < this.y_size - 1) {
                           int var27 = (var21 * this.y_size + var22) * this.x_size + var20;
                           if (this.data[var27] == Block.stone.block_id) {
                              this.data[var27] = var26;
                              ++var5;
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      return var5;
   }

   // $FF: renamed from: game () void
   private void method_579() {
      int var1 = Block.infinite_water.block_id;
      if (this.world_theme == 1) {
         var1 = Block.infinite_lava.block_id;
      }

      int var2 = this.x_size * this.y_size * this.z_size / 1000;

      for(int var3 = 0; var3 < var2; ++var3) {
         if (var3 % 100 == 0) {
            this.method_581((float)var3 * 100.0F / (float)(var2 - 1));
         }

         int var4 = this.random.nextInt(this.x_size);
         int var5 = this.random.nextInt(this.z_size);
         int var6 = this.random.nextInt(this.y_size);
         if (this.data[(var5 * this.y_size + var6) * this.x_size + var4] == 0) {
            long var7;
            if ((var7 = this.method_583(var4, var5, var6, 0, 255)) > 0L && var7 < 640L) {
               this.method_583(var4, var5, var6, 255, var1);
            } else {
               this.method_583(var4, var5, var6, 255, 0);
            }
         }
      }

      this.method_581(100.0F);
   }

   // $FF: renamed from: animals () void
   private void update_loading_bar() {
      ++this.something_y;
      this.field_1029 = 0.0F;
      this.method_581(0.0F);
   }

   // $FF: renamed from: game (float) void
   private void method_581(float var1) {
      if (var1 < 0.0F) {
         throw new IllegalStateException("Failed to set next phase!");
      } else {
         int var2 = (int)(((float)(this.something_y - 1) + var1 / 100.0F) * 100.0F / (float)this.something_z);
         this.status_label.method_2(var2);
      }
   }

   // $FF: renamed from: entities () void
   private void method_582() {
      int var1 = this.x_size * this.y_size * this.z_size / 2000;
      int var2 = this.surrounding_ground_height;

      for(int var3 = 0; var3 < var1; ++var3) {
         if (var3 % 100 == 0) {
            this.method_581((float)var3 * 100.0F / (float)(var1 - 1));
         }

         int var4 = this.random.nextInt(this.x_size);
         int var5 = Math.min(Math.min(this.random.nextInt(var2), this.random.nextInt(var2)), Math.min(this.random.nextInt(var2), this.random.nextInt(var2)));
         int var6 = this.random.nextInt(this.y_size);
         if (this.data[(var5 * this.y_size + var6) * this.x_size + var4] == 0) {
            long var7;
            if ((var7 = this.method_583(var4, var5, var6, 0, 255)) > 0L && var7 < 640L) {
               this.method_583(var4, var5, var6, 255, Block.infinite_lava.block_id);
            } else {
               this.method_583(var4, var5, var6, 255, 0);
            }
         }
      }

      this.method_581(100.0F);
   }

   // $FF: renamed from: game (int, int, int, int, int) long
   private long method_583(int var1, int var2, int var3, int var4, int var5) {
      byte var6 = (byte)var5;
      byte var22 = (byte)var4;
      ArrayList var7 = new ArrayList();
      byte var8 = 0;
      int var9 = 1;

      int var10;
      for(var10 = 1; 1 << var9 < this.x_size; ++var9) {
         ;
      }

      while(1 << var10 < this.y_size) {
         ++var10;
      }

      int var11 = this.y_size - 1;
      int var12 = this.x_size - 1;
      int var23 = var8 + 1;
      this.field_1030[0] = ((var2 << var10) + var3 << var9) + var1;
      long var14 = 0L;
      var1 = this.x_size * this.y_size;

      while(var23 > 0) {
         --var23;
         var2 = this.field_1030[var23];
         if (var23 == 0 && var7.size() > 0) {
            this.field_1030 = (int[])var7.remove(var7.size() - 1);
            var23 = this.field_1030.length;
         }

         var3 = var2 >> var9 & var11;
         int var13 = var2 >> var9 + var10;

         int var16;
         int var17;
         for(var17 = var16 = var2 & var12; var16 > 0 && this.data[var2 - 1] == var22; --var2) {
            --var16;
         }

         while(var17 < this.x_size && this.data[var2 + var17 - var16] == var22) {
            ++var17;
         }

         int var18 = var2 >> var9 & var11;
         int var19 = var2 >> var9 + var10;
         if (var5 == 255 && (var16 == 0 || var17 == this.x_size - 1 || var13 == 0 || var13 == this.z_size - 1 || var3 == 0 || var3 == this.y_size - 1)) {
            return -1L;
         }

         if (var18 != var3 || var19 != var13) {
            System.out.println("Diagonal flood!?");
         }

         boolean var24 = false;
         boolean var25 = false;
         boolean var20 = false;
         var14 += (long)(var17 - var16);

         for(var16 = var16; var16 < var17; ++var16) {
            this.data[var2] = var6;
            boolean var21;
            if (var3 > 0) {
               if ((var21 = this.data[var2 - this.x_size] == var22) && !var24) {
                  if (var23 == this.field_1030.length) {
                     var7.add(this.field_1030);
                     this.field_1030 = new int[1048576];
                     var23 = 0;
                  }

                  this.field_1030[var23++] = var2 - this.x_size;
               }

               var24 = var21;
            }

            if (var3 < this.y_size - 1) {
               if ((var21 = this.data[var2 + this.x_size] == var22) && !var25) {
                  if (var23 == this.field_1030.length) {
                     var7.add(this.field_1030);
                     this.field_1030 = new int[1048576];
                     var23 = 0;
                  }

                  this.field_1030[var23++] = var2 + this.x_size;
               }

               var25 = var21;
            }

            if (var13 > 0) {
               byte var26 = this.data[var2 - var1];
               if ((var6 == Block.lava.block_id || var6 == Block.infinite_lava.block_id) && (var26 == Block.water.block_id || var26 == Block.infinite_water.block_id)) {
                  this.data[var2 - var1] = (byte) Block.stone.block_id;
               }

               if ((var21 = var26 == var22) && !var20) {
                  if (var23 == this.field_1030.length) {
                     var7.add(this.field_1030);
                     this.field_1030 = new int[1048576];
                     var23 = 0;
                  }

                  this.field_1030[var23++] = var2 - var1;
               }

               var20 = var21;
            }

            ++var2;
         }
      }

      return var14;
   }
}
