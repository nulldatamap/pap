package net.minecraft.game.world;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.game.entities.Entity;
import net.minecraft.game.util.AABB;

// $FF: renamed from: net.minecraft.game.game.i
public final class HitboxTree {
   // $FF: renamed from: game int
   public int width;
   // $FF: renamed from: animals int
   public int height;
   // $FF: renamed from: entities int
   public int depth;
   // $FF: renamed from: particles net.minecraft.game.game.particles
   private ChunkSegregator segregator_a = new ChunkSegregator(this);
   // $FF: renamed from: player net.minecraft.game.game.particles
   private ChunkSegregator segregator_b = new ChunkSegregator(this);
   // $FF: renamed from: projectiles java.util.List[]
   public List[] entity_chunks;
   // $FF: renamed from: player java.util.List
   public List entities = new ArrayList();
   // $FF: renamed from: h java.util.List
   private List collision_query_results = new ArrayList();

   public HitboxTree(int w, int h, int d) {
      this.width = w / 8;
      this.height = h / 8;
      this.depth = d / 8;
      if (this.width == 0) {
         this.width = 1;
      }

      if (this.height == 0) {
         this.height = 1;
      }

      if (this.depth == 0) {
         this.depth = 1;
      }

      this.entity_chunks = new ArrayList[this.width * this.height * this.depth];

      for(w = 0; w < this.width; ++w) {
         for(h = 0; h < this.height; ++h) {
            for(d = 0; d < this.depth; ++d) {
               this.entity_chunks[(d * this.height + h) * this.width + w] = new ArrayList();
            }
         }
      }

   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals) void
   public final void register_entity(Entity ent) {
      this.entities.add(ent);
      this.segregator_a.segregate(ent.pos_x, ent.pos_y, ent.pos_z).add_to_chunk(ent);
      ent.chunk_x = ent.pos_x;
      ent.chunk_y = ent.pos_y;
      ent.chunk_z = ent.pos_z;
   }

   // $FF: renamed from: animals (net.minecraft.game.entities.animals) void
   public final void unregister_entity(Entity var1) {
      this.segregator_a.segregate(var1.chunk_x, var1.chunk_y, var1.chunk_z).remove_from_chunk(var1);
      this.segregator_a.segregate(var1.pos_x, var1.pos_y, var1.pos_z).remove_from_chunk(var1);
      this.entities.remove(var1);
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, float, float, float, float, float, float) java.util.List
   public final List query_collisions_with(Entity ent, float min_x, float min_y, float min_z, float max_x, float max_y, float max_z) {
      this.collision_query_results.clear();
      return this.entities_colliding_with(ent, min_x, min_y, min_z, max_x, max_y, max_z, this.collision_query_results);
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, float, float, float, float, float, float, java.util.List) java.util.List
   private List entities_colliding_with(Entity ent, float min_x, float min_y, float min_z, float max_x, float max_y, float max_z, List collidees) {
      ChunkSegregator sega = this.segregator_a.segregate(min_x, min_y, min_z);
      ChunkSegregator segb = this.segregator_b.segregate(max_x, max_y, max_z);

      for(int x = ChunkSegregator.x_of(sega) - 1; x <= ChunkSegregator.x_of(segb) + 1; ++x) {
         for(int y = ChunkSegregator.y_of(sega) - 1; y <= ChunkSegregator.y_of(segb) + 1; ++y) {
            for(int z = ChunkSegregator.z_of(sega) - 1; z <= ChunkSegregator.z_of(segb) + 1; ++z) {

               if (x >= 0 && y >= 0 && z >= 0 && x < this.width && y < this.height && z < this.depth) {
                  List entities_in_same_chunk = this.entity_chunks[(z * this.height + y) * this.width + x];

                  for(int i = 0; i < entities_in_same_chunk.size(); ++i) {
                     Entity other = (Entity)entities_in_same_chunk.get(i);
                     if (other != ent) {
                        AABB hb = other.hitbox;
                        if (max_x > hb.min_x
                                && min_x < hb.max_x
                                && max_y > hb.min_y
                                && min_y < hb.max_y
                                && max_z > hb.min_z
                                && min_z < hb.max_z ) {
                           collidees.add(other);
                        }
                     }
                  }
               }
            }
         }
      }

      return collidees;
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, net.minecraft.game.projectiles.animals) java.util.List
   public final List colliding_entities(Entity ent, AABB hb) {
      this.collision_query_results.clear();
      return hb == null ? this.collision_query_results
              : this.entities_colliding_with(ent, hb.min_x, hb.min_y, hb.min_z, hb.max_x, hb.max_y, hb.max_z, this.collision_query_results);
   }

   // $FF: renamed from: game () void
   public final void update_entities_and_segregation() {
      for(int i = 0; i < this.entities.size(); ++i) {

         Entity ent = (Entity)this.entities.get(i);
         ent.chunk_x = ent.pos_x;
         ent.chunk_y = ent.pos_y;
         ent.chunk_z = ent.pos_z;

         ent.update_entity();
         if (ent.destroyed) {
            this.entities.remove(i--);
            this.segregator_a.segregate(ent.chunk_x, ent.chunk_y, ent.chunk_z).remove_from_chunk(ent);
         } else {
            int cx = (int)(ent.chunk_x / 8.0F);
            int cy = (int)(ent.chunk_y / 8.0F);
            int cz = (int)(ent.chunk_z / 8.0F);
            int px = (int)(ent.pos_x / 8.0F);
            int py = (int)(ent.pos_y / 8.0F);
            int pz = (int)(ent.pos_z / 8.0F);
            if (cx != px || cy != py || cz != pz) {
               ChunkSegregator sega = this.segregator_a.segregate(ent.chunk_x, ent.chunk_y, ent.chunk_z);
               ChunkSegregator segb = this.segregator_b.segregate(ent.pos_x, ent.pos_y, ent.pos_z);
               if (!sega.equals(segb)) {
                  sega.remove_from_chunk(ent);
                  segb.add_to_chunk(ent);
               }
            }
         }
      }

   }
}
