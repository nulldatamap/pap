package net.minecraft.game.world;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.game.world.block.Block;

// $FF: renamed from: net.minecraft.game.game.h
public final class MaybeChunk {
   // $FF: renamed from: game int
   private int field_280 = 0;
   // $FF: renamed from: animals java.util.List
   private List field_281 = new ArrayList();
   // $FF: renamed from: entities net.minecraft.game.game.player
   private World world;
   // $FF: renamed from: projectiles int
   private int width;
   // $FF: renamed from: player int
   private int depth;
   // $FF: renamed from: particles int
   private int height;
   // $FF: renamed from: player byte[]
   private byte[] blocks;
   // $FF: renamed from: h byte[]
   private byte[] data;
   // $FF: renamed from: i int[]
   private int[] height_map;
   // $FF: renamed from: j java.util.List
   private List field_289 = new ArrayList();
   // $FF: renamed from: k java.util.List
   private List field_290 = new ArrayList();
   // $FF: renamed from: l java.util.List
   private List field_291 = new ArrayList();
   // $FF: renamed from: m java.util.List
   private List field_292 = new ArrayList();
   // $FF: renamed from: n int[]
   private int[] field_293 = this.method_145();
   // $FF: renamed from: o byte[]
   private byte[] field_294;
   // $FF: renamed from: p net.minecraft.game.game.entities
   private class_70 field_295 = null;
   // $FF: renamed from: q int
   private int field_296 = 0;
   // $FF: renamed from: r int
   private int field_297;

   public MaybeChunk(World w) {
      this.world = w;
      this.width = w.width;
      this.depth = w.depth;
      this.height = w.height;
      this.field_294 = new byte[w.blocks.length / 8];
      this.blocks = w.blocks;
      this.data = w.data;
      this.height_map = w.height_map;
   }

   // $FF: renamed from: entities () int[]
   private int[] method_145() {
      return this.field_292.size() > 0 ? (int[])this.field_292.remove(this.field_292.size() - 1) : new int['耀'];
   }

   // $FF: renamed from: game (int, int, int, int) void
   public final void method_146(int var1, int var2, int var3, int var4) {
      this.field_291.add(new class_70(this, var1, var2, 0, var3, var4, 1));
   }

   // $FF: renamed from: game (int) void
   public final void method_147(int var1) {
      if (var1 > 15) {
         var1 = 15;
      }

      if (var1 < 0) {
         var1 = 0;
      }

      this.field_297 = var1 - this.world.maybe_brightness;
      if (this.field_297 != 0) {
         this.field_296 = this.world.maybe_brightness;
         this.world.maybe_brightness = var1;

         while(this.field_295 != null) {
            this.method_148(64);
         }

         this.field_295 = new class_70(this, 0, 0, 0, this.world.width, this.world.height, this.world.depth);
      }
   }

   // $FF: renamed from: animals (int) void
   private void method_148(int var1) {
      int var2 = this.field_295.field_298;
      int var3 = this.field_295.field_301;
      int var4 = this.field_295.field_300;
      int var5 = this.field_295.field_303;

      int var6;
      int var7;
      for(var2 = var2; var2 < var3; ++var2) {
         if (var1-- <= 0 && var2 != var3 - 1) {
            this.field_295.field_298 = var2;
            return;
         }

         for(var6 = var4; var6 < var5; ++var6) {
            for(var7 = this.height_map[var2 + var6 * this.width] - 1; var7 > 0 && Block.field_864[this.blocks[(var7 * this.depth + var6) * this.width + var2]] < 100; --var7) {
               ;
            }

            ++var7;

            for(; var7 < this.height; ++var7) {
               int var8 = (var7 * this.depth + var6) * this.width + var2;
               int var9;
               if (Block.field_867[this.blocks[var8]] == 0 && (var9 = this.data[var8] & 15) <= this.field_296) {
                  if (this.field_297 < 0 && var9 > 0) {
                     --this.data[var8];
                  } else if (this.field_297 > 0 && var9 < 15) {
                     ++this.data[var8];
                  }
               }
            }
         }
      }

      for(var6 = 0; var6 < this.width; var6 += 32) {
         for(var7 = 0; var7 < this.depth; var7 += 32) {
            this.field_290.add(new class_70(this, var6, 0, var7, var6 + 32, this.height, var7 + 32));
            this.field_289.add(new class_70(this, var6, 0, var7, var6 + 32, this.height, var7 + 32));
         }
      }

      for(var6 = 0; var6 < this.world.renderers.size(); ++var6) {
         ((IRenderer)this.world.renderers.get(var6)).method_11();
      }

      this.field_295 = null;
   }

   // $FF: renamed from: game (int, int, int, int, int, int) void
   public final void method_149(int var1, int var2, int var3, int var4, int var5, int var6) {
      this.field_290.add(new class_70(this, var1, var2, var3, var4, var5, var6));
   }

   // $FF: renamed from: animals (int, int, int, int, int, int) void
   private void method_150(int var1, int var2, int var3, int var4, int var5, int var6) {
      for(var2 = var2; var2 < var5; ++var2) {
         for(int var7 = var3; var7 < var6; ++var7) {
            for(int var8 = var1; var8 < var4; ++var8) {
               int var9 = var8 + var2 * this.width + var7 * this.width * this.height;
               if ((this.field_294[var9 >> 3] & 1 << (var9 & 7)) == 0) {
                  this.field_294[var9 >> 3] = (byte)(this.field_294[var9 >> 3] | 1 << (var9 & 7));
                  this.field_293[this.field_280++] = var9;
                  if ((this.field_294[var9 >> 3] & 1 << (var9 & 7)) == 0) {
                     System.out.println("OMG ERROR!");
                  }

                  if (this.field_280 > this.field_293.length - 32) {
                     var9 = this.field_293[--this.field_280];
                     this.field_293[this.field_293.length - 1] = this.field_280;
                     this.field_281.add(this.field_293);
                     this.field_293 = this.method_145();
                     this.field_280 = 1;
                     this.field_293[0] = var9;
                  }
               }
            }
         }
      }

   }

   // $FF: renamed from: game () void
   public final void method_151() {
      if (this.field_292.size() > 0) {
         this.field_292.remove(this.field_292.size() - 1);
      }

      int var1 = 5;

      while(this.field_289.size() > 0 && var1-- > 0) {
         class_70 var2 = (class_70)this.field_289.remove(0);

         for(int var3 = 0; var3 < this.world.renderers.size(); ++var3) {
            ((IRenderer)this.world.renderers.get(var3)).method_4(var2.field_298, var2.field_299, var2.field_300, var2.field_301, var2.field_302, var2.field_303);
         }
      }

      if (this.field_295 != null) {
         this.method_148(8);
      } else {
         for(int var19 = 0; var19 < 16; ++var19) {
            boolean var20 = false;
            class_70 var21;
            if (this.field_290.size() > 0) {
               var20 = true;
               var21 = (class_70)this.field_290.remove(0);
               this.method_150(var21.field_298, var21.field_299, var21.field_300, var21.field_301, var21.field_302, var21.field_303);
            }

            int var4;
            int var5;
            int var6;
            int var7;
            int var8;
            int var9;
            int var10;
            int var11;
            int var12;
            int var13;
            MaybeChunk var22;
            if (this.field_291.size() > 0) {
               var20 = true;
               var21 = (class_70)this.field_291.remove(0);
               var7 = var21.field_302;
               var6 = var21.field_301;
               var5 = var21.field_299;
               var4 = var21.field_298;
               var22 = this;

               for(var8 = var4; var8 < var4 + var6; ++var8) {
                  for(var9 = var5; var9 < var5 + var7; ++var9) {
                     var10 = var22.height_map[var8 + var9 * var22.width];

                     for(var11 = var22.height - 1; var11 > 0 && Block.field_864[var22.blocks[(var11 * var22.depth + var9) * var22.width + var8]] == 0; --var11) {
                        ;
                     }

                     var22.height_map[var8 + var9 * var22.width] = var11 + 1;
                     if (var10 != var11) {
                        var12 = var10 < var11 ? var10 : var11;
                        var13 = var10 > var11 ? var10 : var11;
                        var22.method_150(var8, var12, var9, var8 + 1, var13, var9 + 1);
                     }
                  }
               }
            }

            var22 = this;
            var4 = this.world.maybe_brightness;
            var5 = -999;
            var6 = -999;
            var7 = -999;
            var8 = -999;
            var9 = -999;
            var10 = -999;
            var11 = 1024;
            var12 = 0;

            while(var11-- > 0 && (var22.field_280 > 0 || var22.field_281.size() > 0)) {
               ++var12;
               if (var22.field_280 == 0) {
                  if (var22.field_293 != null) {
                     int[] var23 = var22.field_293;
                     var22.field_292.add(var23);
                  }

                  var22.field_293 = (int[])var22.field_281.remove(var22.field_281.size() - 1);
                  var22.field_280 = var22.field_293[var22.field_293.length - 1];
               }

               if (var22.field_280 > var22.field_293.length - 32) {
                  var13 = var22.field_293[--var22.field_280];
                  var22.field_293[var22.field_293.length - 1] = var22.field_280;
                  var22.field_281.add(var22.field_293);
                  var22.field_293 = var22.method_145();
                  var22.field_280 = 1;
                  var22.field_293[0] = var13;
               } else {
                  var1 = (var13 = var22.field_293[--var22.field_280]) % var22.width;
                  int var14 = var13 / var22.width % var22.height;
                  int var15 = var13 / var22.width / var22.height % var22.depth;
                  var22.field_294[var13 >> 3] = (byte)(var22.field_294[var13 >> 3] ^ 1 << (var13 & 7));
                  var13 = var22.height_map[var1 + var15 * var22.width];
                  int var16 = var14 >= var13 ? var4 : 0;
                  byte var24 = var22.blocks[(var14 * var22.depth + var15) * var22.width + var1];
                  int var17;
                  if ((var17 = Block.field_864[var24]) > 100) {
                     var16 = 0;
                  } else if (var16 < 15) {
                     if ((var17 = var17) == 0) {
                        var17 = 1;
                     }

                     int var18;
                     if (var1 > 0 && (var18 = (var22.data[(var14 * var22.depth + var15) * var22.width + (var1 - 1)] & 15) - var17) > var16) {
                        var16 = var18;
                     }

                     if (var1 < var22.width - 1 && (var18 = (var22.data[(var14 * var22.depth + var15) * var22.width + var1 + 1] & 15) - var17) > var16) {
                        var16 = var18;
                     }

                     if (var14 > 0 && (var18 = (var22.data[((var14 - 1) * var22.depth + var15) * var22.width + var1] & 15) - var17) > var16) {
                        var16 = var18;
                     }

                     if (var14 < var22.height - 1 && (var18 = (var22.data[((var14 + 1) * var22.depth + var15) * var22.width + var1] & 15) - var17) > var16) {
                        var16 = var18;
                     }

                     if (var15 > 0 && (var18 = (var22.data[(var14 * var22.depth + (var15 - 1)) * var22.width + var1] & 15) - var17) > var16) {
                        var16 = var18;
                     }

                     if (var15 < var22.depth - 1 && (var18 = (var22.data[(var14 * var22.depth + var15 + 1) * var22.width + var1] & 15) - var17) > var16) {
                        var16 = var18;
                     }
                  }

                  if (var16 < Block.field_867[var24]) {
                     var16 = Block.field_867[var24];
                  }

                  if ((var22.data[(var14 * var22.depth + var15) * var22.width + var1] & 15) != var16) {
                     var22.data[(var14 * var22.depth + var15) * var22.width + var1] = (byte)((var22.data[(var14 * var22.depth + var15) * var22.width + var1] & 240) + var16);
                     if (var1 > 0 && (var22.data[(var14 * var22.depth + var15) * var22.width + (var1 - 1)] & 15) != var16 - 1) {
                        var13 = var1 - 1 + var14 * var22.width + var15 * var22.width * var22.height;
                        if ((var22.field_294[var13 >> 3] & 1 << (var13 & 7)) == 0) {
                           var22.field_294[var13 >> 3] = (byte)(var22.field_294[var13 >> 3] | 1 << (var13 & 7));
                           var22.field_293[var22.field_280++] = var13;
                        }
                     }

                     if (var1 < var22.width - 1 && (var22.data[(var14 * var22.depth + var15) * var22.width + var1 + 1] & 15) != var16 - 1) {
                        var13 = var1 + 1 + var14 * var22.width + var15 * var22.width * var22.height;
                        if ((var22.field_294[var13 >> 3] & 1 << (var13 & 7)) == 0) {
                           var22.field_294[var13 >> 3] = (byte)(var22.field_294[var13 >> 3] | 1 << (var13 & 7));
                           var22.field_293[var22.field_280++] = var13;
                        }
                     }

                     if (var14 > 0 && (var22.data[((var14 - 1) * var22.depth + var15) * var22.width + var1] & 15) != var16 - 1) {
                        var13 = var1 + (var14 - 1) * var22.width + var15 * var22.width * var22.height;
                        if ((var22.field_294[var13 >> 3] & 1 << (var13 & 7)) == 0) {
                           var22.field_294[var13 >> 3] = (byte)(var22.field_294[var13 >> 3] | 1 << (var13 & 7));
                           var22.field_293[var22.field_280++] = var13;
                        }
                     }

                     if (var14 < var22.height - 1 && (var22.data[((var14 + 1) * var22.depth + var15) * var22.width + var1] & 15) != var16 - 1) {
                        var13 = var1 + (var14 + 1) * var22.width + var15 * var22.width * var22.height;
                        if ((var22.field_294[var13 >> 3] & 1 << (var13 & 7)) == 0) {
                           var22.field_294[var13 >> 3] = (byte)(var22.field_294[var13 >> 3] | 1 << (var13 & 7));
                           var22.field_293[var22.field_280++] = var13;
                        }
                     }

                     if (var15 > 0 && (var22.data[(var14 * var22.depth + (var15 - 1)) * var22.width + var1] & 15) != var16 - 1) {
                        var13 = var1 + var14 * var22.width + (var15 - 1) * var22.width * var22.height;
                        if ((var22.field_294[var13 >> 3] & 1 << (var13 & 7)) == 0) {
                           var22.field_294[var13 >> 3] = (byte)(var22.field_294[var13 >> 3] | 1 << (var13 & 7));
                           var22.field_293[var22.field_280++] = var13;
                        }
                     }

                     if (var15 < var22.depth - 1 && (var22.data[(var14 * var22.depth + var15 + 1) * var22.width + var1] & 15) != var16 - 1) {
                        var13 = var1 + var14 * var22.width + (var15 + 1) * var22.width * var22.height;
                        if ((var22.field_294[var13 >> 3] & 1 << (var13 & 7)) == 0) {
                           var22.field_294[var13 >> 3] = (byte)(var22.field_294[var13 >> 3] | 1 << (var13 & 7));
                           var22.field_293[var22.field_280++] = var13;
                        }
                     }

                     if (var5 == -999) {
                        var5 = var1;
                        var6 = var1;
                        var7 = var14;
                        var8 = var14;
                        var9 = var15;
                        var10 = var15;
                     }

                     if (var1 < var5) {
                        var5 = var1;
                     } else if (var1 > var6) {
                        var6 = var1;
                     }

                     if (var14 > var8) {
                        var8 = var14;
                     } else if (var14 < var7) {
                        var7 = var14;
                     }

                     if (var15 < var9) {
                        var9 = var15;
                     } else if (var15 > var10) {
                        var10 = var15;
                     }
                  }
               }
            }

            if (var5 > -999) {
               var22.field_289.add(new class_70(var22, var5, var7, var9, var6, var8, var10));
            }

            if (var12 > 0) {
               var20 = true;
            }
         }

      }
   }

   // $FF: renamed from: animals () java.lang.String
   public final String method_152() {
      return "" + (this.field_290.size() + this.field_289.size());
   }
}
