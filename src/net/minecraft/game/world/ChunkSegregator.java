package net.minecraft.game.world;

import net.minecraft.game.entities.Entity;

// $FF: renamed from: net.minecraft.game.game.particles
final class ChunkSegregator {
   // $FF: renamed from: game int
   private int x;
   // $FF: renamed from: animals int
   private int y;
   // $FF: renamed from: entities int
   private int z;
   // $FF: renamed from: projectiles net.minecraft.game.game.i
   // $FF: synthetic field
   private HitboxTree hittree;

   private ChunkSegregator(HitboxTree var1, byte var2) {
      super();
      this.hittree = var1;
   }

   // $FF: renamed from: game (float, float, float) net.minecraft.game.game.particles
   public final ChunkSegregator segregate(float x, float y, float z) {
      this.x = (int)(x / 8.0F);
      this.y = (int)(y / 8.0F);
      this.z = (int)(z / 8.0F);
      if (this.x < 0) {
         this.x = 0;
      }

      if (this.y < 0) {
         this.y = 0;
      }

      if (this.z < 0) {
         this.z = 0;
      }

      if (this.x >= this.hittree.width) {
         this.x = this.hittree.width - 1;
      }

      if (this.y >= this.hittree.height) {
         this.y = this.hittree.height - 1;
      }

      if (this.z >= this.hittree.depth) {
         this.z = this.hittree.depth - 1;
      }

      return this;
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals) void
   public final void add_to_chunk(Entity ent) {
      if (this.x >= 0 && this.y >= 0 && this.z >= 0) {
         this.hittree.entity_chunks[(this.z * this.hittree.height + this.y) * this.hittree.width + this.x].add(ent);
      }

   }

   // $FF: renamed from: animals (net.minecraft.game.entities.animals) void
   public final void remove_from_chunk(Entity ent) {
      if (this.x >= 0 && this.y >= 0 && this.z >= 0) {
         this.hittree.entity_chunks[(this.z * this.hittree.height + this.y) * this.hittree.width + this.x].remove(ent);
      }

   }

   // $FF: synthetic method
   ChunkSegregator(HitboxTree var1) {
      this(var1, (byte)0);
   }

   // $FF: renamed from: game (net.minecraft.game.game.particles) int
   // $FF: synthetic method
   static int x_of(ChunkSegregator var0) {
      return var0.x;
   }

   // $FF: renamed from: animals (net.minecraft.game.game.particles) int
   // $FF: synthetic method
   static int y_of(ChunkSegregator var0) {
      return var0.y;
   }

   // $FF: renamed from: entities (net.minecraft.game.game.particles) int
   // $FF: synthetic method
   static int z_of(ChunkSegregator var0) {
      return var0.z;
   }
}
