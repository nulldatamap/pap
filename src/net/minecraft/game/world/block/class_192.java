package net.minecraft.game.world.block;

import java.util.Random;
import net.minecraft.game.world.material.Material;
import net.minecraft.game.util.AABB;

// $FF: renamed from: net.minecraft.game.game.animals.k
public final class class_192 extends Block {
   protected class_192(int var1, int var2) {
      super(55, 62, Material.field_621);
   }

   // $FF: renamed from: game (int, int, int) net.minecraft.game.projectiles.animals
   public final AABB method_502(int var1, int var2, int var3) {
      return null;
   }

   // $FF: renamed from: animals () boolean
   public final boolean method_503() {
      return false;
   }

   // $FF: renamed from: entities () boolean
   public final boolean method_491() {
      return false;
   }

   // $FF: renamed from: game () int
   public final int method_492() {
      return 5;
   }

   // $FF: renamed from: game (java.util.Random) int
   public final int method_512(Random var1) {
      return 1;
   }

   // $FF: renamed from: projectiles () boolean
   public final boolean method_504() {
      return false;
   }
}
