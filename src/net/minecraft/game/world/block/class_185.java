package net.minecraft.game.world.block;

import net.minecraft.game.world.World;
import net.minecraft.game.world.block.a.DataBlock;
import net.minecraft.game.world.material.Material;

// $FF: renamed from: net.minecraft.game.game.animals.q
public abstract class class_185 extends Block {
   protected class_185(int var1, Material var2) {
      super(var1, var2);
   }

   // $FF: renamed from: projectiles (net.minecraft.game.game.player, int, int, int) void
   public void method_510(World var1, int var2, int var3, int var4) {
      super.method_510(var1, var2, var3, var4);
      var1.method_214(var2, var3, var4, this.method_528());
   }

   // $FF: renamed from: animals (net.minecraft.game.game.player, int, int, int) void
   public void method_511(World var1, int var2, int var3, int var4) {
      super.method_511(var1, var2, var3, var4);
      var1.method_215(var2, var3, var4);
   }

   // $FF: renamed from: a_ () net.minecraft.game.game.animals.game.game
   protected abstract DataBlock method_528();
}
