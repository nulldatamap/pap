package net.minecraft.game.world.block;

import java.util.Random;

import net.minecraft.game.entities.misc.FallingBlock;
import net.minecraft.game.world.World;
import net.minecraft.game.world.material.Material;

// $FF: renamed from: net.minecraft.game.game.animals.w
public class FallableBlock extends Block {
   public FallableBlock(int var1, int var2) {
      super(var1, var2, Material.field_620);
      new Random();
   }

   // $FF: renamed from: projectiles (net.minecraft.game.game.player, int, int, int) void
   public final void method_510(World var1, int var2, int var3, int var4) {
      this.method_541(var1, var2, var3, var4);
   }

   // $FF: renamed from: animals (net.minecraft.game.game.player, int, int, int, int) void
   public final void method_508(World var1, int var2, int var3, int var4, int var5) {
      this.method_541(var1, var2, var3, var4);
   }

   // $FF: renamed from: player (net.minecraft.game.game.player, int, int, int) void
   private void method_541(World world, int x, int y, int z) {
      world.make_block_fall( x, y, z );
      /*
      int ay = y;

      while(true) {
         int cy = ay - 1;
         int var6;
         Material var10;
         if (!((var6 = world.block_at(x, cy, z)) == 0 ? true: (var6 == Block.fire.block_id ? true : ((var10 = Block.blocks[var6].material) == Material.water_block ? true : var10 == Material.lava_block))) || ay < 0) {
            if (ay < 0) {
               world.set_block(x, y, z, 0);
            }

            if (ay != y) {
               if ((var6 = world.block_at(x, ay, z)) > 0 && Block.blocks[var6].material != Material.field_608) {
                  world.set_block(x, ay, z, 0);
               }

               world.method_163(x, y, z, x, ay, z);
            }

            return;
         }

         --ay;
         if (world.block_at(x, ay, z) == Block.fire.block_id) {
            world.method_164(x, ay, z, 0);
         }
      }
      */
   }
}
