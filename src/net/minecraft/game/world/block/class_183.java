package net.minecraft.game.world.block;

import java.util.Random;
import net.minecraft.game.world.material.Material;
import net.minecraft.game.items.Item;

// $FF: renamed from: net.minecraft.game.game.animals.entities
public final class class_183 extends Block {
   public class_183(int var1, int var2) {
      super(var1, var2, Material.stone_material);
   }

   // $FF: renamed from: game (int, java.util.Random) int
   public final int method_513(int var1, Random var2) {
      if (this.block_id == Block.coal.block_id) {
         return Item.field_140.unique_id;
      } else {
         return this.block_id == Block.diamond.block_id ? Item.diamond.unique_id : this.block_id;
      }
   }

   // $FF: renamed from: game (java.util.Random) int
   public final int method_512(Random var1) {
      return 1;
   }
}
