package net.minecraft.game.world.block;

import java.util.Random;
import net.minecraft.game.world.World;
import net.minecraft.game.world.material.Material;

// $FF: renamed from: net.minecraft.game.game.animals.animals
public final class InfiniteFluid extends class_188 {
   protected InfiniteFluid(int var1, Material var2) {
      super(var1, var2);
      this.field_946 = var1 - 1;
      this.field_945 = var1;
      this.maybeSetDrops(false);
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, java.util.Random) void
   public final void tick(World var1, int var2, int var3, int var4, Random var5) {
   }

   // $FF: renamed from: animals (net.minecraft.game.game.player, int, int, int, int) void
   public final void method_508(World var1, int var2, int var3, int var4, int var5) {
      boolean var6 = false;
      if (this.maybeCheckBlockNearby(var1, var2, var3 - 1, var4)) {
         var6 = true;
      }

      if (!var6 && this.maybeCheckBlockNearby(var1, var2 - 1, var3, var4)) {
         var6 = true;
      }

      if (!var6 && this.maybeCheckBlockNearby(var1, var2 + 1, var3, var4)) {
         var6 = true;
      }

      if (!var6 && this.maybeCheckBlockNearby(var1, var2, var3, var4 - 1)) {
         var6 = true;
      }

      if (!var6 && this.maybeCheckBlockNearby(var1, var2, var3, var4 + 1)) {
         var6 = true;
      }

      if (var5 != 0) {
         Material var7 = Block.blocks[var5].material;
         if (this.material == Material.water_block && var7 == Material.lava_block || var7 == Material.water_block && this.material == Material.lava_block) {
            var1.method_165(var2, var3, var4, Block.stone.block_id);
            return;
         }
      }

      if (Block.fire.method_547(var5)) {
         var6 = true;
      }

      if (var6) {
         var1.set_block(var2, var3, var4, this.field_946);
         var1.method_186(var2, var3, var4, this.field_946);
      }

   }
}
