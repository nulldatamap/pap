package net.minecraft.game.world.block;

import net.minecraft.game.world.World;
import net.minecraft.game.world.material.Material;
import net.minecraft.game.entities.player.Player;

// $FF: renamed from: net.minecraft.game.game.animals.t
public final class class_180 extends Block {
   protected class_180(int var1) {
      super(58, Material.wood_material);
      this.maybeDropCount = 59;
   }

   // $FF: renamed from: game (int) int
   public final int method_500(int var1) {
      if (var1 == 1) {
         return this.maybeDropCount - 16;
      } else if (var1 == 0) {
         return Block.wood_planks.method_500(0);
      } else {
         return var1 != 2 && var1 != 4 ? this.maybeDropCount : this.maybeDropCount + 1;
      }
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, net.minecraft.game.entities.player.game) boolean
   public final boolean method_525(World var1, int var2, int var3, int var4, Player var5) {
      var5.method_75();
      return true;
   }
}
