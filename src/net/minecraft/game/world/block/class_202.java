package net.minecraft.game.world.block;

import java.util.Random;
import net.minecraft.game.world.World;
import net.minecraft.game.world.material.Material;

// $FF: renamed from: net.minecraft.game.game.animals.ai
public final class class_202 extends Block {
   // $FF: renamed from: game int
   private int field_953;

   protected class_202(int var1, int var2) {
      super(var1, Block.blocks[var2].maybeDropCount, Material.water_block);
      this.field_953 = var2;
      this.maybeSetDrops(true);
   }

   // $FF: renamed from: projectiles (net.minecraft.game.game.player, int, int, int) void
   public final void method_510(World var1, int var2, int var3, int var4) {
      super.method_510(var1, var2, var3, var4);
      if (var1.block_at(var2 - 1, var3, var4) == 0) {
         var1.method_165(var2 - 1, var3, var4, this.field_953);
      }

      if (var1.block_at(var2 + 1, var3, var4) == 0) {
         var1.method_165(var2 + 1, var3, var4, this.field_953);
      }

      if (var1.block_at(var2, var3, var4 - 1) == 0) {
         var1.method_165(var2, var3, var4 - 1, this.field_953);
      }

      if (var1.block_at(var2, var3, var4 + 1) == 0) {
         var1.method_165(var2, var3, var4 + 1, this.field_953);
      }

   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, java.util.Random) void
   public final void tick(World var1, int var2, int var3, int var4, Random var5) {
      super.tick(var1, var2, var3, var4, var5);
      if (var1.block_at(var2 - 1, var3, var4) == 0) {
         var1.method_165(var2 - 1, var3, var4, this.field_953);
      }

      if (var1.block_at(var2 + 1, var3, var4) == 0) {
         var1.method_165(var2 + 1, var3, var4, this.field_953);
      }

      if (var1.block_at(var2, var3, var4 - 1) == 0) {
         var1.method_165(var2, var3, var4 - 1, this.field_953);
      }

      if (var1.block_at(var2, var3, var4 + 1) == 0) {
         var1.method_165(var2, var3, var4 + 1, this.field_953);
      }

   }
}
