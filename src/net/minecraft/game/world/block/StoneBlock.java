package net.minecraft.game.world.block;

import java.util.Random;
import net.minecraft.game.world.material.Material;

// $FF: renamed from: net.minecraft.game.game.animals.r
public final class StoneBlock extends Block {
   public StoneBlock(int var1, int var2) {
      super(var1, var2, Material.stone_material);
   }

   // $FF: renamed from: game (int, java.util.Random) int
   public final int method_513(int var1, Random var2) {
      return Block.cobble_stone.block_id;
   }
}
