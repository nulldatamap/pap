package net.minecraft.game.world.block;

import java.util.Random;
import net.minecraft.game.world.World;
import net.minecraft.game.world.material.Material;
import net.minecraft.game.util.RaycastResult;
import net.minecraft.game.util.AABB;
import net.minecraft.game.util.Vector;

// $FF: renamed from: net.minecraft.game.game.animals.ac
public final class class_191 extends Block {
   protected class_191(int var1, int var2) {
      super(50, 80, Material.field_621);
      this.maybeSetDrops(true);
   }

   // $FF: renamed from: game (int, int, int) net.minecraft.game.projectiles.animals
   public final AABB method_502(int var1, int var2, int var3) {
      return null;
   }

   // $FF: renamed from: animals () boolean
   public final boolean method_503() {
      return false;
   }

   // $FF: renamed from: entities () boolean
   public final boolean method_491() {
      return false;
   }

   // $FF: renamed from: game () int
   public final int method_492() {
      return 2;
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int) boolean
   public final boolean method_524(World var1, int var2, int var3, int var4) {
      if (var1.method_170(var2 - 1, var3, var4)) {
         return true;
      } else if (var1.method_170(var2 + 1, var3, var4)) {
         return true;
      } else if (var1.method_170(var2, var3, var4 - 1)) {
         return true;
      } else if (var1.method_170(var2, var3, var4 + 1)) {
         return true;
      } else {
         return var1.method_170(var2, var3 - 1, var4);
      }
   }

   // $FF: renamed from: player (net.minecraft.game.game.player, int, int, int, int) void
   public final void method_527(World var1, int var2, int var3, int var4, int var5) {
      byte var6 = var1.block_data_at(var2, var3, var4);
      if (var5 == 1 && var1.method_170(var2, var3 - 1, var4)) {
         var6 = 5;
      }

      if (var5 == 2 && var1.method_170(var2, var3, var4 + 1)) {
         var6 = 4;
      }

      if (var5 == 3 && var1.method_170(var2, var3, var4 - 1)) {
         var6 = 3;
      }

      if (var5 == 4 && var1.method_170(var2 + 1, var3, var4)) {
         var6 = 2;
      }

      if (var5 == 5 && var1.method_170(var2 - 1, var3, var4)) {
         var6 = 1;
      }

      var1.method_197(var2, var3, var4, var6);
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, java.util.Random) void
   public final void tick(World var1, int var2, int var3, int var4, Random var5) {
      super.tick(var1, var2, var3, var4, var5);
      if (var1.block_data_at(var2, var3, var4) == 0) {
         this.method_510(var1, var2, var3, var4);
      }

   }

   // $FF: renamed from: projectiles (net.minecraft.game.game.player, int, int, int) void
   public final void method_510(World var1, int var2, int var3, int var4) {
      if (var1.method_170(var2 - 1, var3, var4)) {
         var1.method_197(var2, var3, var4, 1);
      } else if (var1.method_170(var2 + 1, var3, var4)) {
         var1.method_197(var2, var3, var4, 2);
      } else if (var1.method_170(var2, var3, var4 - 1)) {
         var1.method_197(var2, var3, var4, 3);
      } else if (var1.method_170(var2, var3, var4 + 1)) {
         var1.method_197(var2, var3, var4, 4);
      } else if (var1.method_170(var2, var3 - 1, var4)) {
         var1.method_197(var2, var3, var4, 5);
      }

      this.method_540(var1, var2, var3, var4);
   }

   // $FF: renamed from: animals (net.minecraft.game.game.player, int, int, int, int) void
   public final void method_508(World var1, int var2, int var3, int var4, int var5) {
      if (this.method_540(var1, var2, var3, var4)) {
         byte var7 = var1.block_data_at(var2, var3, var4);
         boolean var6 = false;
         if (!var1.method_170(var2 - 1, var3, var4) && var7 == 1) {
            var6 = true;
         }

         if (!var1.method_170(var2 + 1, var3, var4) && var7 == 2) {
            var6 = true;
         }

         if (!var1.method_170(var2, var3, var4 - 1) && var7 == 3) {
            var6 = true;
         }

         if (!var1.method_170(var2, var3, var4 + 1) && var7 == 4) {
            var6 = true;
         }

         if (!var1.method_170(var2, var3 - 1, var4) && var7 == 5) {
            var6 = true;
         }

         if (var6) {
            this.method_515(var1, var2, var3, var4, var1.block_data_at(var2, var3, var4));
            var1.method_165(var2, var3, var4, 0);
         }
      }

   }

   // $FF: renamed from: player (net.minecraft.game.game.player, int, int, int) boolean
   private boolean method_540(World var1, int var2, int var3, int var4) {
      if (!this.method_524(var1, var2, var3, var4)) {
         this.method_515(var1, var2, var3, var4, var1.block_data_at(var2, var3, var4));
         var1.method_165(var2, var3, var4, 0);
         return false;
      } else {
         return true;
      }
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, net.minecraft.game.projectiles.game, net.minecraft.game.projectiles.game) net.minecraft.game.projectiles.entities
   public final RaycastResult method_518(World var1, int var2, int var3, int var4, Vector var5, Vector var6) {
      byte var7;
      if ((var7 = var1.block_data_at(var2, var3, var4)) == 1) {
         this.setStuff(0.0F, 0.2F, 0.35F, 0.3F, 0.8F, 0.65F);
      } else if (var7 == 2) {
         this.setStuff(0.7F, 0.2F, 0.35F, 1.0F, 0.8F, 0.65F);
      } else if (var7 == 3) {
         this.setStuff(0.35F, 0.2F, 0.0F, 0.65F, 0.8F, 0.3F);
      } else if (var7 == 4) {
         this.setStuff(0.35F, 0.2F, 0.7F, 0.65F, 0.8F, 1.0F);
      } else {
         this.setStuff(0.4F, 0.0F, 0.4F, 0.6F, 0.6F, 0.6F);
      }

      return super.method_518(var1, var2, var3, var4, var5, var6);
   }

   // $FF: renamed from: animals (net.minecraft.game.game.player, int, int, int, java.util.Random) void
   public final void method_506(World var1, int var2, int var3, int var4, Random var5) {
      byte var9 = var1.block_data_at(var2, var3, var4);
      float var6 = (float)var2 + 0.5F;
      float var7 = (float)var3 + 0.7F;
      float var8 = (float)var4 + 0.5F;
      if (var9 == 1) {
         var1.spawn_effect("smoke", var6 - 0.27F, var7 + 0.22F, var8, 0.0F, 0.0F, 0.0F);
         var1.spawn_effect("flame", var6 - 0.27F, var7 + 0.22F, var8, 0.0F, 0.0F, 0.0F);
      } else if (var9 == 2) {
         var1.spawn_effect("smoke", var6 + 0.27F, var7 + 0.22F, var8, 0.0F, 0.0F, 0.0F);
         var1.spawn_effect("flame", var6 + 0.27F, var7 + 0.22F, var8, 0.0F, 0.0F, 0.0F);
      } else if (var9 == 3) {
         var1.spawn_effect("smoke", var6, var7 + 0.22F, var8 - 0.27F, 0.0F, 0.0F, 0.0F);
         var1.spawn_effect("flame", var6, var7 + 0.22F, var8 - 0.27F, 0.0F, 0.0F, 0.0F);
      } else if (var9 == 4) {
         var1.spawn_effect("smoke", var6, var7 + 0.22F, var8 + 0.27F, 0.0F, 0.0F, 0.0F);
         var1.spawn_effect("flame", var6, var7 + 0.22F, var8 + 0.27F, 0.0F, 0.0F, 0.0F);
      } else {
         var1.spawn_effect("smoke", var6, var7, var8, 0.0F, 0.0F, 0.0F);
         var1.spawn_effect("flame", var6, var7, var8, 0.0F, 0.0F, 0.0F);
      }
   }
}
