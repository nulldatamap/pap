package net.minecraft.game.world.block;

import java.util.Random;
import net.minecraft.game.world.World;
import net.minecraft.game.world.material.Material;
import net.minecraft.game.util.AABB;

// $FF: renamed from: net.minecraft.game.game.animals.ad
public class class_208 extends Block {
   protected class_208(int var1, int var2) {
      super(var1, Material.field_616);
      this.maybeDropCount = var2;
      this.maybeSetDrops(true);
      this.setStuff(0.3F, 0.0F, 0.3F, 0.7F, 0.6F, 0.7F);
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int) boolean
   public final boolean method_524(World var1, int var2, int var3, int var4) {
      return this.method_550(var1.block_at(var2, var3 - 1, var4));
   }

   // $FF: renamed from: animals (int) boolean
   protected boolean method_550(int var1) {
      return var1 == Block.grass.block_id || var1 == Block.dirt.block_id || var1 == Block.field_927.block_id;
   }

   // $FF: renamed from: animals (net.minecraft.game.game.player, int, int, int, int) void
   public final void method_508(World var1, int var2, int var3, int var4, int var5) {
      super.method_508(var1, var2, var3, var4, var5);
      this.method_551(var1, var2, var3, var4);
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, java.util.Random) void
   public void tick(World var1, int var2, int var3, int var4, Random var5) {
      this.method_551(var1, var2, var3, var4);
   }

   // $FF: renamed from: h (net.minecraft.game.game.player, int, int, int) void
   private void method_551(World var1, int var2, int var3, int var4) {
      if (!this.method_552(var1, var2, var3, var4)) {
         this.method_515(var1, var2, var3, var4, var1.block_data_at(var2, var3, var4));
         var1.method_165(var2, var3, var4, 0);
      }

   }

   // $FF: renamed from: player (net.minecraft.game.game.player, int, int, int) boolean
   public boolean method_552(World var1, int var2, int var3, int var4) {
      return (var1.method_195(var2, var3, var4) >= 8 || var1.method_195(var2, var3, var4) >= 4 && var1.has_skylight(var2, var3, var4)) && this.method_550(var1.block_at(var2, var3 - 1, var4));
   }

   // $FF: renamed from: game (int, int, int) net.minecraft.game.projectiles.animals
   public final AABB method_502(int var1, int var2, int var3) {
      return null;
   }

   // $FF: renamed from: animals () boolean
   public final boolean method_503() {
      return false;
   }

   // $FF: renamed from: entities () boolean
   public final boolean method_491() {
      return false;
   }

   // $FF: renamed from: game () int
   public int method_492() {
      return 1;
   }
}
