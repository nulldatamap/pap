package net.minecraft.game.world.block;

import java.util.Random;
import net.minecraft.game.world.World;
import net.minecraft.game.world.material.Material;
import net.minecraft.game.util.AABB;

// $FF: renamed from: net.minecraft.game.game.animals.ab
public final class class_193 extends Block {
   protected class_193(int var1) {
      super(60, Material.field_609);
      this.maybeDropCount = 87;
      this.maybeSetDrops(true);
      this.setStuff(0.0F, 0.0F, 0.0F, 1.0F, 0.9375F, 1.0F);
      this.method_488(255);
   }

   // $FF: renamed from: game (int, int, int) net.minecraft.game.projectiles.animals
   public final AABB method_502(int var1, int var2, int var3) {
      return new AABB((float)var1, (float)var2, (float)var3, (float)(var1 + 1), (float)(var2 + 1), (float)(var3 + 1));
   }

   // $FF: renamed from: animals () boolean
   public final boolean method_503() {
      return false;
   }

   // $FF: renamed from: entities () boolean
   public final boolean method_491() {
      return false;
   }

   // $FF: renamed from: game (int, int) int
   public final int method_499(int var1, int var2) {
      if (var1 == 1 && var2 > 0) {
         return this.maybeDropCount - 1;
      } else {
         return var1 == 1 ? this.maybeDropCount : 2;
      }
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, java.util.Random) void
   public final void tick(World var1, int var2, int var3, int var4, Random var5) {
      if (var5.nextInt(5) == 0) {
         int var8 = var4;
         int var7 = var3;
         int var6 = var2;
         World var12 = var1;
         int var9 = var2 - 4;

         int var10;
         int var11;
         boolean var10000;
         label69:
         while(true) {
            if (var9 > var6 + 4) {
               var10000 = false;
               break;
            }

            for(var10 = var7; var10 <= var7 + 1; ++var10) {
               for(var11 = var8 - 4; var11 <= var8 + 4; ++var11) {
                  if (var12.method_198(var9, var10, var11) == Material.water_block) {
                     var10000 = true;
                     break label69;
                  }
               }
            }

            ++var9;
         }

         if (var10000) {
            var1.method_197(var2, var3, var4, 7);
            return;
         }

         byte var13;
         if ((var13 = var1.block_data_at(var2, var3, var4)) > 0) {
            var1.method_197(var2, var3, var4, var13 - 1);
            return;
         }

         var8 = var4;
         var7 = var3;
         var6 = var2;
         var12 = var1;
         var10 = var2;

         label49:
         while(true) {
            if (var10 > var6) {
               var10000 = false;
               break;
            }

            for(var11 = var8; var11 <= var8; ++var11) {
               if (var12.block_at(var10, var7 + 1, var11) == Block.field_926.block_id) {
                  var10000 = true;
                  break label49;
               }
            }

            ++var10;
         }

         if (!var10000) {
            var1.method_165(var2, var3, var4, Block.dirt.block_id);
         }
      }

   }

   // $FF: renamed from: player (net.minecraft.game.game.player, int, int, int) void
   public final void method_526(World var1, int var2, int var3, int var4) {
      if (var1.random_a.nextInt(4) == 0) {
         var1.method_165(var2, var3, var4, Block.dirt.block_id);
      }

   }

   // $FF: renamed from: animals (net.minecraft.game.game.player, int, int, int, int) void
   public final void method_508(World var1, int var2, int var3, int var4, int var5) {
      super.method_508(var1, var2, var3, var4, var5);
      if (var1.method_198(var2, var3 + 1, var4).method_345()) {
         var1.method_165(var2, var3, var4, Block.dirt.block_id);
      }

   }

   // $FF: renamed from: game (int, java.util.Random) int
   public final int method_513(int var1, Random var2) {
      return Block.dirt.method_513(0, var2);
   }
}
