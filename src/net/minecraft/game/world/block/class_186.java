package net.minecraft.game.world.block;

import java.util.Random;
import net.minecraft.game.world.World;
import net.minecraft.game.world.block.a.DataBlock;
import net.minecraft.game.world.block.a.FurnaceData;
import net.minecraft.game.world.material.Material;
import net.minecraft.game.entities.player.Player;

// $FF: renamed from: net.minecraft.game.game.animals.s
public final class class_186 extends class_185 {
   // $FF: renamed from: game boolean
   private final boolean field_943;

   protected class_186(int var1, boolean var2) {
      super(var1, Material.stone_material);
      this.field_943 = var2;
      this.maybeDropCount = 45;
   }

   // $FF: renamed from: projectiles (net.minecraft.game.game.player, int, int, int) void
   public final void method_510(World var1, int var2, int var3, int var4) {
      super.method_510(var1, var2, var3, var4);
      method_529(var1, var2, var3, var4);
   }

   // $FF: renamed from: player (net.minecraft.game.game.player, int, int, int) void
   private static void method_529(World var0, int var1, int var2, int var3) {
      int var4 = var0.block_at(var1, var2, var3 - 1);
      int var5 = var0.block_at(var1, var2, var3 + 1);
      int var6 = var0.block_at(var1 - 1, var2, var3);
      int var7 = var0.block_at(var1 + 1, var2, var3);
      byte var8 = 3;
      if (Block.transparency[var4] && !Block.transparency[var5]) {
         var8 = 3;
      }

      if (Block.transparency[var5] && !Block.transparency[var4]) {
         var8 = 2;
      }

      if (Block.transparency[var6] && !Block.transparency[var7]) {
         var8 = 5;
      }

      if (Block.transparency[var7] && !Block.transparency[var6]) {
         var8 = 4;
      }

      var0.method_197(var1, var2, var3, var8);
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, int) int
   public final int method_498(World var1, int var2, int var3, int var4, int var5) {
      if (var5 == 1) {
         return Block.stone.maybeDropCount;
      } else if (var5 == 0) {
         return Block.stone.maybeDropCount;
      } else {
         byte var6;
         if ((var6 = var1.block_data_at(var2, var3, var4)) == 0) {
            method_529(var1, var2, var3, var4);
            var6 = var1.block_data_at(var2, var3, var4);
         }

         if (var5 != var6) {
            return this.maybeDropCount;
         } else {
            return this.field_943 ? this.maybeDropCount + 16 : this.maybeDropCount - 1;
         }
      }
   }

   // $FF: renamed from: animals (net.minecraft.game.game.player, int, int, int, java.util.Random) void
   public final void method_506(World var1, int var2, int var3, int var4, Random var5) {
      if (this.field_943) {
         byte var6 = var1.block_data_at(var2, var3, var4);
         float var7 = (float)var2 + 0.5F;
         float var8 = (float)var3 + var5.nextFloat() * 6.0F / 16.0F;
         float var9 = (float)var4 + 0.5F;
         float var10 = var5.nextFloat() * 0.6F - 0.3F;
         if (var6 == 4) {
            var1.spawn_effect("smoke", var7 - 0.52F, var8, var9 + var10, 0.0F, 0.0F, 0.0F);
            var1.spawn_effect("flame", var7 - 0.52F, var8, var9 + var10, 0.0F, 0.0F, 0.0F);
         } else if (var6 == 5) {
            var1.spawn_effect("smoke", var7 + 0.52F, var8, var9 + var10, 0.0F, 0.0F, 0.0F);
            var1.spawn_effect("flame", var7 + 0.52F, var8, var9 + var10, 0.0F, 0.0F, 0.0F);
         } else if (var6 == 2) {
            var1.spawn_effect("smoke", var7 + var10, var8, var9 - 0.52F, 0.0F, 0.0F, 0.0F);
            var1.spawn_effect("flame", var7 + var10, var8, var9 - 0.52F, 0.0F, 0.0F, 0.0F);
         } else {
            if (var6 == 3) {
               var1.spawn_effect("smoke", var7 + var10, var8, var9 + 0.52F, 0.0F, 0.0F, 0.0F);
               var1.spawn_effect("flame", var7 + var10, var8, var9 + 0.52F, 0.0F, 0.0F, 0.0F);
            }

         }
      }
   }

   // $FF: renamed from: game (int) int
   public final int method_500(int var1) {
      if (var1 == 1) {
         return Block.stone.block_id;
      } else if (var1 == 0) {
         return Block.stone.block_id;
      } else {
         return var1 == 3 ? this.maybeDropCount - 1 : this.maybeDropCount;
      }
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, net.minecraft.game.entities.player.game) boolean
   public final boolean method_525(World var1, int var2, int var3, int var4, Player var5) {
      FurnaceData var6 = (FurnaceData)var1.method_216(var2, var3, var4);
      var5.method_77(var6);
      return true;
   }

   // $FF: renamed from: a_ () net.minecraft.game.game.animals.game.game
   protected final DataBlock method_528() {
      return new FurnaceData();
   }
}
