package net.minecraft.game.world.block;

import java.util.Random;
import net.minecraft.game.world.World;
import net.minecraft.game.world.material.Material;
import net.minecraft.game.entities.misc.PrimedTnt;

// $FF: renamed from: net.minecraft.game.game.animals.h
public final class TntBlock extends Block {
   public TntBlock(int var1, int var2) {
      super(46, 8, Material.field_623);
   }

   // $FF: renamed from: game (int) int
   public final int method_500(int var1) {
      if (var1 == 0) {
         return this.maybeDropCount + 2;
      } else {
         return var1 == 1 ? this.maybeDropCount + 1 : this.maybeDropCount;
      }
   }

   // $FF: renamed from: game (java.util.Random) int
   public final int method_512(Random var1) {
      return 0;
   }

   // $FF: renamed from: entities (net.minecraft.game.game.player, int, int, int) void
   public final void method_522(World var1, int var2, int var3, int var4) {
      PrimedTnt var5 = null;
      (var5 = new PrimedTnt(var1, (float)var2 + 0.5F, (float)var3 + 0.5F, (float)var4 + 0.5F)).fuse_time = var1.random_a.nextInt(var5.fuse_time / 4) + var5.fuse_time / 8;
      var1.add_entity(var5);
   }

   // $FF: renamed from: entities (net.minecraft.game.game.player, int, int, int, int) void
   public final void method_507(World var1, int var2, int var3, int var4, int var5) {
      PrimedTnt var6 = new PrimedTnt(var1, (float)var2 + 0.5F, (float)var3 + 0.5F, (float)var4 + 0.5F);
      var1.add_entity(var6);
      var1.play_sound(var6, "random.fuse", 1.0F, 1.0F);
   }
}
