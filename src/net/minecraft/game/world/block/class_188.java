package net.minecraft.game.world.block;

import java.util.Random;
import net.minecraft.game.world.World;
import net.minecraft.game.world.material.Material;
import net.minecraft.game.util.AABB;

// $FF: renamed from: net.minecraft.game.game.animals.p
public class class_188 extends Block {
   // $FF: renamed from: game int
   protected int field_945;
   // $FF: renamed from: animals int
   protected int field_946;

   protected class_188(int var1, Material var2) {
      super(var1, var2);
      this.maybeDropCount = 14;
      if (var2 == Material.lava_block) {
         this.maybeDropCount = 30;
      }

      Block.field_866[var1] = true;
      this.field_946 = var1;
      this.field_945 = var1 + 1;
      this.setStuff(0.01F, -0.09F, 0.01F, 1.01F, 0.90999997F, 1.01F);
      this.maybeSetDrops(true);
      this.method_490(2.0F);
   }

   // $FF: renamed from: game (int) int
   public final int method_500(int var1) {
      if (this.material == Material.lava_block) {
         return this.maybeDropCount;
      } else if (var1 == 1) {
         return this.maybeDropCount;
      } else {
         return var1 == 0 ? this.maybeDropCount : this.maybeDropCount + 32;
      }
   }

   // $FF: renamed from: entities () boolean
   public final boolean method_491() {
      return false;
   }

   // $FF: renamed from: projectiles (net.minecraft.game.game.player, int, int, int) void
   public void method_510(World var1, int var2, int var3, int var4) {
      var1.method_186(var2, var3, var4, this.field_946);
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, java.util.Random) void
   public void tick(World var1, int var2, int var3, int var4, Random var5) {
      this.method_531(var1, var2, var3, var4, 0);
   }

   // $FF: renamed from: player (net.minecraft.game.game.player, int, int, int, int) boolean
   public boolean method_531(World var1, int var2, int var3, int var4, int var5) {
      boolean var7 = false;

      boolean var6;
      do {
         --var3;
         if (!this.maybeCheckBlockNearby(var1, var2, var3, var4)) {
            break;
         }

         if (var6 = var1.method_165(var2, var3, var4, this.field_946)) {
            var7 = true;
         }
      } while(var6 && this.material != Material.lava_block);

      ++var3;
      if (this.material == Material.water_block || !var7) {
         var7 = var7 | this.method_534(var1, var2 - 1, var3, var4) | this.method_534(var1, var2 + 1, var3, var4) | this.method_534(var1, var2, var3, var4 - 1) | this.method_534(var1, var2, var3, var4 + 1);
      }

      if (this.material == Material.lava_block) {
         var7 = var7 | method_533(var1, var2 - 1, var3, var4) | method_533(var1, var2 + 1, var3, var4) | method_533(var1, var2, var3, var4 - 1) | method_533(var1, var2, var3, var4 + 1);
      }

      if (!var7) {
         var1.set_block(var2, var3, var4, this.field_945);
      } else {
         var1.method_186(var2, var3, var4, this.field_946);
      }

      return var7;
   }

   // $FF: renamed from: player (net.minecraft.game.game.player, int, int, int) boolean
   protected final boolean maybeCheckBlockNearby(World var1, int var2, int var3, int var4) {
      if (!var1.method_198(var2, var3, var4).method_344()) {
         return false;
      } else {
         if (this.material == Material.water_block) {
            for(int var5 = var2 - 2; var5 <= var2 + 2; ++var5) {
               for(int var6 = var3 - 2; var6 <= var3 + 2; ++var6) {
                  for(int var7 = var4 - 2; var7 <= var4 + 2; ++var7) {
                     if (var1.block_at(var5, var6, var7) == Block.field_886.block_id) {
                        return false;
                     }
                  }
               }
            }
         }

         return true;
      }
   }

   // $FF: renamed from: h (net.minecraft.game.game.player, int, int, int) boolean
   private static boolean method_533(World var0, int var1, int var2, int var3) {
      if (Block.fire.method_547(var0.block_at(var1, var2, var3))) {
         Block.fire.method_548(var0, var1, var2, var3);
         return true;
      } else {
         return false;
      }
   }

   // $FF: renamed from: i (net.minecraft.game.game.player, int, int, int) boolean
   private boolean method_534(World var1, int var2, int var3, int var4) {
      if (!this.maybeCheckBlockNearby(var1, var2, var3, var4)) {
         return false;
      } else {
         if (var1.method_165(var2, var3, var4, this.field_946)) {
            var1.method_186(var2, var3, var4, this.field_946);
         }

         return false;
      }
   }

   // $FF: renamed from: particles (net.minecraft.game.game.player, int, int, int) float
   public final float method_496(World var1, int var2, int var3, int var4) {
      return this.material == Material.lava_block ? 100.0F : var1.light_at(var2, var3, var4);
   }

   // $FF: renamed from: projectiles (net.minecraft.game.game.player, int, int, int, int) boolean
   public boolean method_497(World var1, int var2, int var3, int var4, int var5) {
      if (var2 >= 0 && var3 >= 0 && var4 >= 0 && var2 < var1.width && var4 < var1.depth) {
         int var6;
         if ((var6 = var1.block_at(var2, var3, var4)) != this.field_946 && var6 != this.field_945) {
            return var5 == 1 && (var1.block_at(var2 - 1, var3, var4) == 0 || var1.block_at(var2 + 1, var3, var4) == 0 || var1.block_at(var2, var3, var4 - 1) == 0 || var1.block_at(var2, var3, var4 + 1) == 0) ? true : super.method_497(var1, var2, var3, var4, var5);
         } else {
            return false;
         }
      } else {
         return false;
      }
   }

   // $FF: renamed from: projectiles () boolean
   public boolean method_504() {
      return false;
   }

   // $FF: renamed from: game (int, int, int) net.minecraft.game.projectiles.animals
   public AABB method_502(int var1, int var2, int var3) {
      return null;
   }

   // $FF: renamed from: animals () boolean
   public boolean method_503() {
      return false;
   }

   // $FF: renamed from: animals (net.minecraft.game.game.player, int, int, int, int) void
   public void method_508(World var1, int var2, int var3, int var4, int var5) {
      if (var5 != 0) {
         Material var6 = Block.blocks[var5].material;
         if (this.material == Material.water_block && var6 == Material.lava_block || var6 == Material.water_block && this.material == Material.lava_block) {
            var1.method_165(var2, var3, var4, Block.stone.block_id);
         }
      }

      var1.method_186(var2, var3, var4, this.block_id);
   }

   // $FF: renamed from: player () int
   public int method_509() {
      return this.material == Material.lava_block ? 25 : 5;
   }

   // $FF: renamed from: game (java.util.Random) int
   public int method_512(Random var1) {
      return 0;
   }

   // $FF: renamed from: particles () int
   public int method_523() {
      return this.material == Material.water_block ? 1 : 0;
   }

   // $FF: renamed from: animals (net.minecraft.game.game.player, int, int, int, java.util.Random) void
   public final void method_506(World var1, int var2, int var3, int var4, Random var5) {
      if (var5.nextInt(128) == -1 && var1.method_198(var2, var3 + 1, var4).method_347()) {
         if (this.material == Material.lava_block) {
            var1.method_212((float)var2 + 0.5F, (float)var3 + 0.5F, (float)var4 + 0.5F, "liquid.lava", var5.nextFloat() * 0.25F + 0.75F, var5.nextFloat() * 0.5F + 0.3F);
         }

         if (this.material == Material.water_block) {
            var1.method_212((float)var2 + 0.5F, (float)var3 + 0.5F, (float)var4 + 0.5F, "liquid.water", var5.nextFloat() * 0.25F + 0.75F, var5.nextFloat() + 0.5F);
         }
      }

      if (this.material == Material.lava_block && var1.method_198(var2, var3 + 1, var4) == Material.field_608 && !var1.method_170(var2, var3 + 1, var4) && var5.nextInt(100) == 0) {
         float var6 = (float)var2 + var5.nextFloat();
         float var7 = (float)var3 + this.field_938;
         float var8 = (float)var4 + var5.nextFloat();
         var1.spawn_effect("lava", var6, var7, var8, 0.0F, 0.0F, 0.0F);
      }

      if (this.material == Material.water_block) {
         int var9;
         if (method_535(var1, var2 + 1, var3, var4)) {
            for(var9 = 0; var9 < 4; ++var9) {
               var1.spawn_effect("splash", (float)(var2 + 1) + 0.125F, (float)var3, (float)var4 + var5.nextFloat(), 0.0F, 0.0F, 0.0F);
            }
         }

         if (method_535(var1, var2 - 1, var3, var4)) {
            for(var9 = 0; var9 < 4; ++var9) {
               var1.spawn_effect("splash", (float)var2 - 0.125F, (float)var3, (float)var4 + var5.nextFloat(), 0.0F, 0.0F, 0.0F);
            }
         }

         if (method_535(var1, var2, var3, var4 + 1)) {
            for(var9 = 0; var9 < 4; ++var9) {
               var1.spawn_effect("splash", (float)var2 + var5.nextFloat(), (float)var3, (float)(var4 + 1) + 0.125F, 0.0F, 0.0F, 0.0F);
            }
         }

         if (method_535(var1, var2, var3, var4 - 1)) {
            for(var9 = 0; var9 < 4; ++var9) {
               var1.spawn_effect("splash", (float)var2 + var5.nextFloat(), (float)var3, (float)var4 - 0.125F, 0.0F, 0.0F, 0.0F);
            }
         }
      }

   }

   // $FF: renamed from: j (net.minecraft.game.game.player, int, int, int) boolean
   private static boolean method_535(World var0, int var1, int var2, int var3) {
      Material var4 = var0.method_198(var1, var2, var3);
      Material var5 = var0.method_198(var1, var2 - 1, var3);
      if (!var4.method_347() && !var4.method_343()) {
         return var5.method_347() || var5.method_343();
      } else {
         return false;
      }
   }
}
