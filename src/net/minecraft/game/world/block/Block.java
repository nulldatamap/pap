package net.minecraft.game.world.block;

import java.util.Random;
import net.minecraft.game.world.World;
import net.minecraft.game.world.material.Material;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.items.Item;
import net.minecraft.game.items.class_34;
import net.minecraft.game.entities.misc.ItemDrop;
import net.minecraft.game.entities.player.Player;
import net.minecraft.game.entities.player.Inventory;
import net.minecraft.game.util.RaycastResult;
import net.minecraft.game.util.AABB;
import net.minecraft.game.util.Vector;

// $FF: renamed from: net.minecraft.game.game.animals.x
public class Block {
   // $FF: renamed from: game net.minecraft.game.game.animals.n
   private static SoundMaterial stone_sound = new SoundMaterial("stone", 1.0F, 1.0F);
   // $FF: renamed from: animals net.minecraft.game.game.animals.n
   private static SoundMaterial wood_sound = new SoundMaterial("wood", 1.0F, 1.0F);
   // $FF: renamed from: aD net.minecraft.game.game.animals.n
   private static SoundMaterial gravel_sound = new SoundMaterial("gravel", 1.0F, 1.0F);
   // $FF: renamed from: aE net.minecraft.game.game.animals.n
   private static SoundMaterial grass_sound = new SoundMaterial("grass", 1.0F, 1.0F);
   // $FF: renamed from: aF net.minecraft.game.game.animals.n
   private static SoundMaterial stone_sound_2 = new SoundMaterial("stone", 1.0F, 1.0F);
   // $FF: renamed from: aG net.minecraft.game.game.animals.n
   private static SoundMaterial stone_sound_3 = new SoundMaterial("stone", 1.0F, 1.5F);
   // $FF: renamed from: aH net.minecraft.game.game.animals.n
   private static SoundMaterial stone_sound_4 = new class_214("stone", 1.0F, 1.0F);
   // $FF: renamed from: aI net.minecraft.game.game.animals.n
   private static SoundMaterial cloth_sound = new SoundMaterial("cloth", 1.0F, 1.0F);
   // $FF: renamed from: aJ net.minecraft.game.game.animals.n
   private static SoundMaterial sand_sound = new class_213("sand", 1.0F, 1.0F);
   // $FF: renamed from: entities net.minecraft.game.game.animals.x[]
   public static final Block[] blocks = new Block[256];
   // $FF: renamed from: projectiles boolean[]
   public static final boolean[] field_862 = new boolean[256];
   // $FF: renamed from: player boolean[]
   public static final boolean[] transparency = new boolean[256];
   // $FF: renamed from: particles int[]
   public static final int[] field_864 = new int[256];
   // $FF: renamed from: aK boolean[]
   private static boolean[] field_865 = new boolean[256];
   // $FF: renamed from: player boolean[]
   public static final boolean[] field_866 = new boolean[256];
   // $FF: renamed from: h int[]
   public static final int[] field_867 = new int[256];
   // $FF: renamed from: i net.minecraft.game.game.animals.x
   public static final Block stone;
   // $FF: renamed from: j net.minecraft.game.game.animals.af
   public static final GrassBlock grass;
   // $FF: renamed from: k net.minecraft.game.game.animals.x
   public static final Block dirt;
   // $FF: renamed from: l net.minecraft.game.game.animals.x
   public static final Block cobble_stone;
   // $FF: renamed from: m net.minecraft.game.game.animals.x
   public static final Block wood_planks;
   // $FF: renamed from: n net.minecraft.game.game.animals.x
   public static final Block sapling;
   // $FF: renamed from: o net.minecraft.game.game.animals.x
   public static final Block bedrock;
   // $FF: renamed from: p net.minecraft.game.game.animals.x
   public static final Block water;
   // $FF: renamed from: q net.minecraft.game.game.animals.x
   public static final Block infinite_water;
   // $FF: renamed from: r net.minecraft.game.game.animals.x
   public static final Block lava;
   // $FF: renamed from: s net.minecraft.game.game.animals.x
   public static final Block infinite_lava;
   // $FF: renamed from: t net.minecraft.game.game.animals.x
   public static final Block sand;
   // $FF: renamed from: u net.minecraft.game.game.animals.x
   public static final Block field_880;
   // $FF: renamed from: v net.minecraft.game.game.animals.x
   public static final Block gold;
   // $FF: renamed from: w net.minecraft.game.game.animals.x
   public static final Block iron;
   // $FF: renamed from: x net.minecraft.game.game.animals.x
   public static final Block coal;
   // $FF: renamed from: y net.minecraft.game.game.animals.x
   public static final Block field_884;
   // $FF: renamed from: z net.minecraft.game.game.animals.x
   public static final Block field_885;
   // $FF: renamed from: A net.minecraft.game.game.animals.x
   public static final Block field_886;
   // $FF: renamed from: B net.minecraft.game.game.animals.x
   public static final Block glass;
   // $FF: renamed from: C net.minecraft.game.game.animals.x
   public static final Block field_888;
   // $FF: renamed from: D net.minecraft.game.game.animals.x
   public static final Block field_889;
   // $FF: renamed from: E net.minecraft.game.game.animals.x
   public static final Block field_890;
   // $FF: renamed from: F net.minecraft.game.game.animals.x
   public static final Block field_891;
   // $FF: renamed from: G net.minecraft.game.game.animals.x
   public static final Block field_892;
   // $FF: renamed from: H net.minecraft.game.game.animals.x
   public static final Block field_893;
   // $FF: renamed from: I net.minecraft.game.game.animals.x
   public static final Block field_894;
   // $FF: renamed from: J net.minecraft.game.game.animals.x
   public static final Block field_895;
   // $FF: renamed from: K net.minecraft.game.game.animals.x
   public static final Block field_896;
   // $FF: renamed from: L net.minecraft.game.game.animals.x
   public static final Block field_897;
   // $FF: renamed from: M net.minecraft.game.game.animals.x
   public static final Block field_898;
   // $FF: renamed from: N net.minecraft.game.game.animals.x
   public static final Block field_899;
   // $FF: renamed from: O net.minecraft.game.game.animals.x
   public static final Block field_900;
   // $FF: renamed from: P net.minecraft.game.game.animals.x
   public static final Block field_901;
   // $FF: renamed from: Q net.minecraft.game.game.animals.x
   public static final Block wool;
   // $FF: renamed from: R net.minecraft.game.game.animals.x
   public static final Block field_903;
   // $FF: renamed from: S net.minecraft.game.game.animals.ad
   public static final class_208 field_904;
   // $FF: renamed from: T net.minecraft.game.game.animals.ad
   public static final class_208 field_905;
   // $FF: renamed from: U net.minecraft.game.game.animals.ad
   public static final class_208 field_906;
   // $FF: renamed from: V net.minecraft.game.game.animals.ad
   public static final class_208 field_907;
   // $FF: renamed from: W net.minecraft.game.game.animals.x
   public static final Block field_908;
   // $FF: renamed from: X net.minecraft.game.game.animals.x
   public static final Block field_909;
   // $FF: renamed from: Y net.minecraft.game.game.animals.x
   public static final Block field_910;
   // $FF: renamed from: Z net.minecraft.game.game.animals.x
   public static final Block field_911;
   // $FF: renamed from: aa net.minecraft.game.game.animals.x
   public static final Block field_912;
   // $FF: renamed from: ab net.minecraft.game.game.animals.x
   public static final Block tnt;
   // $FF: renamed from: ac net.minecraft.game.game.animals.x
   public static final Block field_914;
   // $FF: renamed from: ad net.minecraft.game.game.animals.x
   public static final Block field_915;
   // $FF: renamed from: ae net.minecraft.game.game.animals.x
   public static final Block field_916;
   // $FF: renamed from: af net.minecraft.game.game.animals.x
   public static final Block field_917;
   // $FF: renamed from: ag net.minecraft.game.game.animals.ah
   public static final Fire fire;
   // $FF: renamed from: ah net.minecraft.game.game.animals.x
   public static final Block field_919;
   // $FF: renamed from: ai net.minecraft.game.game.animals.x
   public static final Block field_920;
   // $FF: renamed from: aj net.minecraft.game.game.animals.x
   public static final Block field_921;
   // $FF: renamed from: ak net.minecraft.game.game.animals.x
   public static final Block field_922;
   // $FF: renamed from: al net.minecraft.game.game.animals.x
   public static final Block diamond;
   // $FF: renamed from: am net.minecraft.game.game.animals.x
   public static final Block field_924;
   // $FF: renamed from: an net.minecraft.game.game.animals.x
   public static final Block field_925;
   // $FF: renamed from: ao net.minecraft.game.game.animals.x
   public static final Block field_926;
   // $FF: renamed from: ap net.minecraft.game.game.animals.x
   public static final Block field_927;
   // $FF: renamed from: aq net.minecraft.game.game.animals.x
   public static final Block field_928;
   // $FF: renamed from: ar net.minecraft.game.game.animals.x
   public static final Block field_929;
   // $FF: renamed from: as int
   public int maybeDropCount; // field_930
   // $FF: renamed from: at int
   public final int block_id; // field_931
   // $FF: renamed from: aL float
   private float maybeDurability; // field_932
   // $FF: renamed from: aM float
   private float field_933;
   // $FF: renamed from: au float
   public float field_934;
   // $FF: renamed from: av float
   public float field_935;
   // $FF: renamed from: aw float
   public float field_936;
   // $FF: renamed from: ax float
   public float field_937;
   // $FF: renamed from: ay float
   public float field_938;
   // $FF: renamed from: az float
   public float field_939;
   // $FF: renamed from: aA net.minecraft.game.game.animals.n
   public SoundMaterial sound_material;
   // $FF: renamed from: aB float
   public float field_941;
   // $FF: renamed from: aC net.minecraft.game.game.projectiles.entities
   public final Material material;

   protected Block(int id, Material var2) {
      this.sound_material = stone_sound;
      this.field_941 = 1.0F;
      if (blocks[id] != null) {
         throw new IllegalArgumentException("Slot " + id + " is already occupied by " + blocks[id] + " when adding " + this);
      } else {
         this.material = var2;
         blocks[id] = this;
         this.block_id = id;
         this.setStuff(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
         transparency[id] = this.method_503();
         field_864[id] = this.method_503() ? 255 : 0;
         field_865[id] = this.method_491();
         field_866[id] = false;
      }
   }

   protected Block(int var1, int var2, Material var3) {
      this(var1, var3);
      this.maybeDropCount = var2;
   }

   // $FF: renamed from: entities (int) net.minecraft.game.game.animals.x
   protected final Block method_488(int var1) {
      field_864[this.block_id] = var1;
      return this;
   }

   // $FF: renamed from: entities (float) net.minecraft.game.game.animals.x
   private Block method_489(float var1) {
      field_867[this.block_id] = (int)(15.0F * var1);
      return this;
   }

   // $FF: renamed from: game (float) net.minecraft.game.game.animals.x
   protected final Block method_490(float var1) {
      this.field_933 = var1 * 3.0F;
      return this;
   }

   // $FF: renamed from: entities () boolean
   public boolean method_491() {
      return true;
   }

   // $FF: renamed from: game () int
   public int method_492() {
      return 0;
   }

   // $FF: renamed from: animals (float) net.minecraft.game.game.animals.x
   protected final Block maybeSetDurability(float var1) {
      this.maybeDurability = var1;
      if (this.field_933 < var1 * 5.0F) {
         this.field_933 = var1 * 5.0F;
      }

      return this;
   }

   // $FF: renamed from: game (boolean) void
   protected final void maybeSetDrops(boolean var1) {
      field_862[this.block_id] = var1;
   }

   // $FF: renamed from: game (float, float, float, float, float, float) void
   protected final void setStuff(float var1, float var2, float var3, float var4, float var5, float var6) {
      this.field_934 = var1;
      this.field_935 = var2;
      this.field_936 = var3;
      this.field_937 = var4;
      this.field_938 = var5;
      this.field_939 = var6;
   }

   // $FF: renamed from: particles (net.minecraft.game.game.player, int, int, int) float
   public float method_496(World var1, int var2, int var3, int var4) {
      return var1.light_at(var2, var3, var4);
   }

   // $FF: renamed from: projectiles (net.minecraft.game.game.player, int, int, int, int) boolean
   public boolean method_497(World var1, int var2, int var3, int var4, int var5) {
      return !var1.method_170(var2, var3, var4);
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, int) int
   public int method_498(World var1, int var2, int var3, int var4, int var5) {
      return this.method_499(var5, var1.block_data_at(var2, var3, var4));
   }

   // $FF: renamed from: game (int, int) int
   public int method_499(int var1, int var2) {
      return this.method_500(var1);
   }

   // $FF: renamed from: game (int) int
   public int method_500(int var1) {
      return this.maybeDropCount;
   }

   // $FF: renamed from: animals (int, int, int) net.minecraft.game.projectiles.animals
   public final AABB method_501(int var1, int var2, int var3) {
      return new AABB((float)var1 + this.field_934, (float)var2 + this.field_935, (float)var3 + this.field_936, (float)var1 + this.field_937, (float)var2 + this.field_938, (float)var3 + this.field_939);
   }

   // $FF: renamed from: game (int, int, int) net.minecraft.game.projectiles.animals
   public AABB method_502(int var1, int var2, int var3) {
      return new AABB((float)var1 + this.field_934, (float)var2 + this.field_935, (float)var3 + this.field_936, (float)var1 + this.field_937, (float)var2 + this.field_938, (float)var3 + this.field_939);
   }

   // $FF: renamed from: animals () boolean
   public boolean method_503() {
      return true;
   }

   // $FF: renamed from: projectiles () boolean
   public boolean method_504() {
      return true;
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, java.util.Random) void
   public void tick(World var1, int var2, int var3, int var4, Random var5) {
   }

   // $FF: renamed from: animals (net.minecraft.game.game.player, int, int, int, java.util.Random) void
   public void method_506(World var1, int var2, int var3, int var4, Random var5) {
   }

   // $FF: renamed from: entities (net.minecraft.game.game.player, int, int, int, int) void
   public void method_507(World var1, int var2, int var3, int var4, int var5) {
   }

   // $FF: renamed from: animals (net.minecraft.game.game.player, int, int, int, int) void
   public void method_508(World var1, int var2, int var3, int var4, int var5) {
   }

   // $FF: renamed from: player () int
   public int method_509() {
      return 5;
   }

   // $FF: renamed from: projectiles (net.minecraft.game.game.player, int, int, int) void
   public void method_510(World var1, int var2, int var3, int var4) {
   }

   // $FF: renamed from: animals (net.minecraft.game.game.player, int, int, int) void
   public void method_511(World var1, int var2, int var3, int var4) {
   }

   // $FF: renamed from: game (java.util.Random) int
   public int method_512(Random var1) {
      return 1;
   }

   // $FF: renamed from: game (int, java.util.Random) int
   public int method_513(int var1, Random var2) {
      return this.block_id;
   }

   // $FF: renamed from: game (net.minecraft.game.entities.player.game) float
   public final float method_514(Player var1) {
      if (this.maybeDurability < 0.0F) {
         return 0.0F;
      } else if (!var1.method_73(this)) {
         return 1.0F / this.maybeDurability / 100.0F;
      } else {
         Inventory var2 = (var1 = var1).inventory;
         float var4 = 1.0F;
         if (var2.main_area[var2.selected_slot] != null) {
            var4 = 1.0F * var2.main_area[var2.selected_slot].item().method_86(this);
         }

         float var5 = var4;
         if (var1.on_water_block()) {
            var5 = var4 / 5.0F;
         }

         if (!var1.maybe_grounded) {
            var5 /= 5.0F;
         }

         return var5 / this.maybeDurability / 30.0F;
      }
   }

   // $FF: renamed from: particles (net.minecraft.game.game.player, int, int, int, int) void
   public final void method_515(World var1, int var2, int var3, int var4, int var5) {
      this.method_516(var1, var2, var3, var4, var5, 1.0F);
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, int, float) void
   public final void method_516(World var1, int var2, int var3, int var4, int var5, float var6) {
      int var7 = this.method_512(var1.random_a);

      for(int var8 = 0; var8 < var7; ++var8) {
         int var9;
         if (var1.random_a.nextFloat() <= var6 && (var9 = this.method_513(var5, var1.random_a)) > 0) {
            float var10 = var1.random_a.nextFloat() * 0.7F + 0.15F;
            float var11 = var1.random_a.nextFloat() * 0.7F + 0.15F;
            float var12 = var1.random_a.nextFloat() * 0.7F + 0.15F;
            ItemDrop var13;
            (var13 = new ItemDrop(var1, (float)var2 + var10, (float)var3 + var11, (float)var4 + var12, new ItemStack(var9))).pickup_cooldown = 10;
            var1.add_entity(var13);
         }
      }

   }

   // $FF: renamed from: player () float
   public final float method_517() {
      return this.field_933 / 5.0F;
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, net.minecraft.game.projectiles.game, net.minecraft.game.projectiles.game) net.minecraft.game.projectiles.entities
   public RaycastResult method_518(World var1, int var2, int var3, int var4, Vector var5, Vector var6) {
      var5 = var5.add((float)(-var2), (float)(-var3), (float)(-var4));
      var6 = var6.add((float)(-var2), (float)(-var3), (float)(-var4));
      Vector var12 = var5.weird_thing_x(var6, this.field_934);
      Vector var7 = var5.weird_thing_x(var6, this.field_937);
      Vector var8 = var5.weird_thing_y(var6, this.field_935);
      Vector var9 = var5.weird_thing_y(var6, this.field_938);
      Vector var10 = var5.weird_thing_z(var6, this.field_936);
      var6 = var5.weird_thing_z(var6, this.field_939);
      if (!this.method_519(var12)) {
         var12 = null;
      }

      if (!this.method_519(var7)) {
         var7 = null;
      }

      if (!this.method_520(var8)) {
         var8 = null;
      }

      if (!this.method_520(var9)) {
         var9 = null;
      }

      if (!this.method_521(var10)) {
         var10 = null;
      }

      if (!this.method_521(var6)) {
         var6 = null;
      }

      Vector var11 = null;
      if (var12 != null) {
         var11 = var12;
      }

      if (var7 != null && (var11 == null || var5.distance(var7) < var5.distance(var11))) {
         var11 = var7;
      }

      if (var8 != null && (var11 == null || var5.distance(var8) < var5.distance(var11))) {
         var11 = var8;
      }

      if (var9 != null && (var11 == null || var5.distance(var9) < var5.distance(var11))) {
         var11 = var9;
      }

      if (var10 != null && (var11 == null || var5.distance(var10) < var5.distance(var11))) {
         var11 = var10;
      }

      if (var6 != null && (var11 == null || var5.distance(var6) < var5.distance(var11))) {
         var11 = var6;
      }

      if (var11 == null) {
         return null;
      } else {
         byte var13 = -1;
         if (var11 == var12) {
            var13 = 4;
         }

         if (var11 == var7) {
            var13 = 5;
         }

         if (var11 == var8) {
            var13 = 0;
         }

         if (var11 == var9) {
            var13 = 1;
         }

         if (var11 == var10) {
            var13 = 2;
         }

         if (var11 == var6) {
            var13 = 3;
         }

         return new RaycastResult(var2, var3, var4, var13, var11.add((float)var2, (float)var3, (float)var4));
      }
   }

   // $FF: renamed from: game (net.minecraft.game.projectiles.game) boolean
   private boolean method_519(Vector var1) {
      if (var1 == null) {
         return false;
      } else {
         return var1.y >= this.field_935 && var1.y <= this.field_938 && var1.z >= this.field_936 && var1.z <= this.field_939;
      }
   }

   // $FF: renamed from: animals (net.minecraft.game.projectiles.game) boolean
   private boolean method_520(Vector var1) {
      if (var1 == null) {
         return false;
      } else {
         return var1.x >= this.field_934 && var1.x <= this.field_937 && var1.z >= this.field_936 && var1.z <= this.field_939;
      }
   }

   // $FF: renamed from: entities (net.minecraft.game.projectiles.game) boolean
   private boolean method_521(Vector var1) {
      if (var1 == null) {
         return false;
      } else {
         return var1.x >= this.field_934 && var1.x <= this.field_937 && var1.y >= this.field_935 && var1.y <= this.field_938;
      }
   }

   // $FF: renamed from: entities (net.minecraft.game.game.player, int, int, int) void
   public void method_522(World var1, int var2, int var3, int var4) {
   }

   // $FF: renamed from: particles () int
   public int method_523() {
      return 0;
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int) boolean
   public boolean method_524(World var1, int var2, int var3, int var4) {
      return true;
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, net.minecraft.game.entities.player.game) boolean
   public boolean method_525(World var1, int var2, int var3, int var4, Player var5) {
      return false;
   }

   // $FF: renamed from: player (net.minecraft.game.game.player, int, int, int) void
   public void method_526(World var1, int var2, int var3, int var4) {
   }

   // $FF: renamed from: player (net.minecraft.game.game.player, int, int, int, int) void
   public void method_527(World var1, int var2, int var3, int var4, int var5) {
   }

   static {
      Block block = (new StoneBlock(1, 1)).maybeSetDurability(1.5F).method_490(10.0F);
      SoundMaterial sm = stone_sound_2;
      Block also_block = block;
      block.sound_material = sm;
      stone = also_block;

      block = (new GrassBlock(2)).maybeSetDurability(0.6F);
      sm = grass_sound;
      also_block = block;
      block.sound_material = sm;
      grass = (GrassBlock)also_block;

      block = (new DirtBlock(3, 2)).maybeSetDurability(0.5F);
      sm = gravel_sound;
      also_block = block;
      block.sound_material = sm;
      dirt = also_block;

      block = (new Block(4, 16, Material.stone_material)).maybeSetDurability(2.0F).method_490(10.0F);
      sm = stone_sound_2;
      also_block = block;
      block.sound_material = sm;
      cobble_stone = also_block;

      block = (new Block(5, 4, Material.wood_material)).maybeSetDurability(2.0F).method_490(5.0F);
      sm = wood_sound;
      also_block = block;
      block.sound_material = sm;
      wood_planks = also_block;

      block = (new SaplingBlock(6, 15)).maybeSetDurability(0.0F);
      sm = grass_sound;
      also_block = block;
      block.sound_material = sm;
      sapling = also_block;

      block = (new Block(7, 17, Material.stone_material)).maybeSetDurability(-1.0F).method_490(6000000.0F);
      sm = stone_sound_2;
      also_block = block;
      block.sound_material = sm;
      bedrock = also_block;

      water = (new Fluid(8, Material.water_block)).maybeSetDurability(100.0F).method_488(3);

      infinite_water = (new InfiniteFluid(9, Material.water_block)).maybeSetDurability(100.0F).method_488(3);

      lava = (new Fluid(10, Material.lava_block)).maybeSetDurability(0.0F).method_489(1.0F).method_488(255);

      infinite_lava = (new InfiniteFluid(11, Material.lava_block)).maybeSetDurability(100.0F).method_489(1.0F).method_488(255);

      block = (new FallableBlock(12, 18)).maybeSetDurability(0.5F);
      sm = sand_sound;
      also_block = block;
      block.sound_material = sm;
      sand = also_block;

      block = (new class_200(13, 19)).maybeSetDurability(0.6F);
      sm = gravel_sound;
      also_block = block;
      block.sound_material = sm;
      field_880 = also_block;

      block = (new class_183(14, 32)).maybeSetDurability(3.0F).method_490(5.0F);
      sm = stone_sound_2;
      also_block = block;
      block.sound_material = sm;
      gold = also_block;

      block = (new class_183(15, 33)).maybeSetDurability(3.0F).method_490(5.0F);
      sm = stone_sound_2;
      also_block = block;
      block.sound_material = sm;
      iron = also_block;

      block = (new class_183(16, 34)).maybeSetDurability(3.0F).method_490(5.0F);
      sm = stone_sound_2;
      also_block = block;
      block.sound_material = sm;
      coal = also_block;

      block = (new class_195(17)).maybeSetDurability(2.0F);
      sm = wood_sound;
      also_block = block;
      block.sound_material = sm;
      field_884 = also_block;

      block = (new class_197(18, 52)).maybeSetDurability(0.2F).method_488(1);
      sm = grass_sound;
      also_block = block;
      block.sound_material = sm;
      field_885 = also_block;

      block = (new class_204(19)).maybeSetDurability(0.6F);
      sm = grass_sound;
      also_block = block;
      block.sound_material = sm;
      field_886 = also_block;

      block = (new class_207(20, 49, Material.field_622, false)).maybeSetDurability(0.3F);
      sm = stone_sound_4;
      also_block = block;
      block.sound_material = sm;
      glass = also_block;

      block = (new Block(21, 64, Material.field_618)).maybeSetDurability(0.8F);
      sm = cloth_sound;
      also_block = block;
      block.sound_material = sm;
      field_888 = also_block;

      block = (new Block(22, 65, Material.field_618)).maybeSetDurability(0.8F);
      sm = cloth_sound;
      also_block = block;
      block.sound_material = sm;
      field_889 = also_block;

      block = (new Block(23, 66, Material.field_618)).maybeSetDurability(0.8F);
      sm = cloth_sound;
      also_block = block;
      block.sound_material = sm;
      field_890 = also_block;

      block = (new Block(24, 67, Material.field_618)).maybeSetDurability(0.8F);
      sm = cloth_sound;
      also_block = block;
      block.sound_material = sm;
      field_891 = also_block;

      block = (new Block(25, 68, Material.field_618)).maybeSetDurability(0.8F);
      sm = cloth_sound;
      also_block = block;
      block.sound_material = sm;
      field_892 = also_block;

      block = (new Block(26, 69, Material.field_618)).maybeSetDurability(0.8F);
      sm = cloth_sound;
      also_block = block;
      block.sound_material = sm;
      field_893 = also_block;

      block = (new Block(27, 70, Material.field_618)).maybeSetDurability(0.8F);
      sm = cloth_sound;
      also_block = block;
      block.sound_material = sm;
      field_894 = also_block;

      block = (new Block(28, 71, Material.field_618)).maybeSetDurability(0.8F);
      sm = cloth_sound;
      also_block = block;
      block.sound_material = sm;
      field_895 = also_block;

      block = (new Block(29, 72, Material.field_618)).maybeSetDurability(0.8F);
      sm = cloth_sound;
      also_block = block;
      block.sound_material = sm;
      field_896 = also_block;

      block = (new Block(30, 73, Material.field_618)).maybeSetDurability(0.8F);
      sm = cloth_sound;
      also_block = block;
      block.sound_material = sm;
      field_897 = also_block;

      block = (new Block(31, 74, Material.field_618)).maybeSetDurability(0.8F);
      sm = cloth_sound;
      also_block = block;
      block.sound_material = sm;
      field_898 = also_block;

      block = (new Block(32, 75, Material.field_618)).maybeSetDurability(0.8F);
      sm = cloth_sound;
      also_block = block;
      block.sound_material = sm;
      field_899 = also_block;

      block = (new Block(33, 76, Material.field_618)).maybeSetDurability(0.8F);
      sm = cloth_sound;
      also_block = block;
      block.sound_material = sm;
      field_900 = also_block;

      block = (new Block(34, 77, Material.field_618)).maybeSetDurability(0.8F);
      sm = cloth_sound;
      also_block = block;
      block.sound_material = sm;
      field_901 = also_block;

      block = (new Block(35, 78, Material.field_618)).maybeSetDurability(0.8F);
      sm = cloth_sound;
      also_block = block;
      block.sound_material = sm;
      wool = also_block;

      block = (new Block(36, 79, Material.field_618)).maybeSetDurability(0.8F);
      sm = cloth_sound;
      also_block = block;
      block.sound_material = sm;
      field_903 = also_block;

      block = (new class_208(37, 13)).maybeSetDurability(0.0F);
      sm = grass_sound;
      also_block = block;
      block.sound_material = sm;
      field_904 = (class_208)also_block;

      block = (new class_208(38, 12)).maybeSetDurability(0.0F);
      sm = grass_sound;
      also_block = block;
      block.sound_material = sm;
      field_905 = (class_208)also_block;
      block = (new class_210(39, 29)).maybeSetDurability(0.0F);
      sm = grass_sound;
      also_block = block;
      block.sound_material = sm;
      field_906 = (class_208)also_block.method_489(0.125F);
      block = (new class_210(40, 28)).maybeSetDurability(0.0F);
      sm = grass_sound;
      also_block = block;
      block.sound_material = sm;
      field_907 = (class_208)also_block;
      block = (new class_184(41, 39)).maybeSetDurability(3.0F).method_490(10.0F);
      sm = stone_sound_3;
      also_block = block;
      block.sound_material = sm;
      field_908 = also_block;
      block = (new class_184(42, 38)).maybeSetDurability(5.0F).method_490(10.0F);
      sm = stone_sound_3;
      also_block = block;
      block.sound_material = sm;
      field_909 = also_block;
      block = (new class_201(43, true)).maybeSetDurability(2.0F).method_490(10.0F);
      sm = stone_sound_2;
      also_block = block;
      block.sound_material = sm;
      field_910 = also_block;
      block = (new class_201(44, false)).maybeSetDurability(2.0F).method_490(10.0F);
      sm = stone_sound_2;
      also_block = block;
      block.sound_material = sm;
      field_911 = also_block;
      block = (new Block(45, 7, Material.stone_material)).maybeSetDurability(2.0F).method_490(10.0F);
      sm = stone_sound_2;
      also_block = block;
      block.sound_material = sm;
      field_912 = also_block;
      block = (new TntBlock(46, 8)).maybeSetDurability(0.0F);
      sm = grass_sound;
      also_block = block;
      block.sound_material = sm;
      tnt = also_block;
      block = (new class_182(47, 35)).maybeSetDurability(1.5F);
      sm = wood_sound;
      also_block = block;
      block.sound_material = sm;
      field_914 = also_block;
      block = (new Block(48, 36, Material.stone_material)).maybeSetDurability(2.0F).method_490(10.0F);
      sm = stone_sound_2;
      also_block = block;
      block.sound_material = sm;
      field_915 = also_block;
      block = (new StoneBlock(49, 37)).maybeSetDurability(10.0F).method_490(10.0F);
      sm = stone_sound_2;
      also_block = block;
      block.sound_material = sm;
      field_916 = also_block;
      block = (new class_191(50, 80)).maybeSetDurability(0.0F).method_489(0.875F);
      sm = wood_sound;
      also_block = block;
      block.sound_material = sm;
      field_917 = also_block;
      block = (new Fire(51, 31)).maybeSetDurability(0.0F).method_489(1.0F);
      sm = wood_sound;
      also_block = block;
      block.sound_material = sm;
      fire = (Fire)also_block;
      block = (new class_202(52, water.block_id)).maybeSetDurability(0.0F);
      sm = wood_sound;
      also_block = block;
      block.sound_material = sm;
      field_919 = also_block;
      block = (new class_202(53, lava.block_id)).maybeSetDurability(0.0F);
      sm = wood_sound;
      also_block = block;
      block.sound_material = sm;
      field_920 = also_block;
      block = (new class_187(54)).maybeSetDurability(2.5F);
      sm = wood_sound;
      also_block = block;
      block.sound_material = sm;
      field_921 = also_block;
      block = (new class_192(55, 62)).maybeSetDurability(0.5F);
      sm = stone_sound_3;
      also_block = block;
      block.sound_material = sm;
      field_922 = also_block;
      block = (new class_183(56, 50)).maybeSetDurability(3.0F).method_490(5.0F);
      sm = stone_sound_2;
      also_block = block;
      block.sound_material = sm;
      diamond = also_block;
      block = (new class_184(57, 40)).maybeSetDurability(5.0F).method_490(10.0F);
      sm = stone_sound_3;
      also_block = block;
      block.sound_material = sm;
      field_924 = also_block;
      block = (new class_180(58)).maybeSetDurability(2.5F);
      sm = wood_sound;
      also_block = block;
      block.sound_material = sm;
      field_925 = also_block;
      block = (new class_211(59, 88)).maybeSetDurability(0.0F);
      sm = grass_sound;
      also_block = block;
      block.sound_material = sm;
      field_926 = also_block;
      block = (new class_193(60)).maybeSetDurability(0.6F);
      sm = gravel_sound;
      also_block = block;
      block.sound_material = sm;
      field_927 = also_block;
      block = (new class_186(61, false)).maybeSetDurability(3.5F);
      sm = stone_sound_2;
      also_block = block;
      block.sound_material = sm;
      field_928 = also_block;
      block = (new class_186(62, true)).maybeSetDurability(3.5F);
      sm = stone_sound_2;
      also_block = block;
      block.sound_material = sm;
      field_929 = also_block.method_489(0.875F);

      for(int var2 = 0; var2 < 256; ++var2) {
         if (blocks[var2] != null) {
            Item.items[var2] = new class_34(var2 - 256);
         }
      }

   }
}
