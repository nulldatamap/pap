package net.minecraft.game.world.block;

// $FF: renamed from: net.minecraft.game.game.animals.n
public class SoundMaterial {
   // $FF: renamed from: entities java.lang.String
   private String name;
   // $FF: renamed from: game float
   public final float field_958;
   // $FF: renamed from: animals float
   public final float field_959;

   public SoundMaterial(String var1, float var2, float var3) {
      this.name = var1;
      this.field_958 = var2;
      this.field_959 = var3;
   }

   // $FF: renamed from: game () java.lang.String
   public String break_sound() {
      return "step." + this.name;
   }

   // $FF: renamed from: animals () java.lang.String
   public final String step_sound() {
      return "step." + this.name;
   }
}
