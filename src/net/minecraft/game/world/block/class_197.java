package net.minecraft.game.world.block;

import java.util.Random;
import net.minecraft.game.world.World;
import net.minecraft.game.world.material.Material;

// $FF: renamed from: net.minecraft.game.game.animals.o
public final class class_197 extends class_196 {
   protected class_197(int var1, int var2) {
      super(18, 52, Material.field_615, true);
      this.maybeSetDrops(true);
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, java.util.Random) void
   public final void tick(World var1, int var2, int var3, int var4, Random var5) {
      if (!var1.method_198(var2, var3 - 1, var4).method_345()) {
         for(int var8 = var2 - 2; var8 <= var2 + 2; ++var8) {
            for(int var6 = var3 - 1; var6 <= var3; ++var6) {
               for(int var7 = var4 - 2; var7 <= var4 + 2; ++var7) {
                  if (var1.block_at(var8, var6, var7) == Block.field_884.block_id) {
                     return;
                  }
               }
            }
         }

         this.method_515(var1, var2, var3, var4, var1.block_data_at(var2, var3, var4));
         var1.method_165(var2, var3, var4, 0);
      }
   }

   // $FF: renamed from: game (java.util.Random) int
   public final int method_512(Random var1) {
      return var1.nextInt(10) == 0 ? 1 : 0;
   }

   // $FF: renamed from: game (int, java.util.Random) int
   public final int method_513(int var1, Random var2) {
      return Block.sapling.block_id;
   }
}
