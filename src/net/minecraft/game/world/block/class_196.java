package net.minecraft.game.world.block;

import net.minecraft.game.world.World;
import net.minecraft.game.world.material.Material;

// $FF: renamed from: net.minecraft.game.game.animals.player
public class class_196 extends Block {
   // $FF: renamed from: game boolean
   private boolean field_951 = true;

   protected class_196(int var1, int var2, Material var3, boolean var4) {
      super(var1, var2, var3);
   }

   // $FF: renamed from: animals () boolean
   public final boolean method_503() {
      return false;
   }

   // $FF: renamed from: projectiles (net.minecraft.game.game.player, int, int, int, int) boolean
   public final boolean method_497(World var1, int var2, int var3, int var4, int var5) {
      int var6 = var1.block_at(var2, var3, var4);
      return !this.field_951 && var6 == this.block_id ? false : super.method_497(var1, var2, var3, var4, var5);
   }
}
