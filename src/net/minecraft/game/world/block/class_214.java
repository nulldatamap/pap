package net.minecraft.game.world.block;

// $FF: renamed from: net.minecraft.game.game.animals.j
final class class_214 extends SoundMaterial {
   class_214(String var1, float var2, float var3) {
      super(var1, 1.0F, 1.0F);
   }

   // $FF: renamed from: game () java.lang.String
   public final String break_sound() {
      return "random.glass";
   }
}
