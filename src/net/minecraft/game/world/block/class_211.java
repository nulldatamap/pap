package net.minecraft.game.world.block;

import java.util.Random;
import net.minecraft.game.world.World;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.items.Item;
import net.minecraft.game.entities.misc.ItemDrop;

// $FF: renamed from: net.minecraft.game.game.animals.particles
public final class class_211 extends class_208 {
   protected class_211(int var1, int var2) {
      super(59, 88);
      this.maybeDropCount = 88;
      this.maybeSetDrops(true);
      this.setStuff(0.0F, 0.0F, 0.0F, 1.0F, 0.25F, 1.0F);
   }

   // $FF: renamed from: animals (int) boolean
   protected final boolean method_550(int var1) {
      return var1 == Block.field_927.block_id;
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, java.util.Random) void
   public final void tick(World var1, int var2, int var3, int var4, Random var5) {
      super.tick(var1, var2, var3, var4, var5);
      byte var6;
      if (var1.method_195(var2, var3 + 1, var4) >= 9 && (var6 = var1.block_data_at(var2, var3, var4)) < 7) {
         int var11 = var4;
         int var10 = var3;
         int var9 = var2;
         World var8 = var1;
         float var12 = 1.0F;
         int var13 = var1.block_at(var2, var3, var4 - 1);
         int var14 = var1.block_at(var2, var3, var4 + 1);
         int var15 = var1.block_at(var2 - 1, var3, var4);
         int var16 = var1.block_at(var2 + 1, var3, var4);
         int var17 = var1.block_at(var2 - 1, var3, var4 - 1);
         int var18 = var1.block_at(var2 + 1, var3, var4 - 1);
         int var19 = var1.block_at(var2 + 1, var3, var4 + 1);
         int var20 = var1.block_at(var2 - 1, var3, var4 + 1);
         boolean var23 = var15 == this.block_id || var16 == this.block_id;
         boolean var22 = var13 == this.block_id || var14 == this.block_id;
         boolean var7 = var17 == this.block_id || var18 == this.block_id || var19 == this.block_id || var20 == this.block_id;

         for(var14 = var2 - 1; var14 <= var9 + 1; ++var14) {
            for(var16 = var11 - 1; var16 <= var11 + 1; ++var16) {
               var17 = var8.block_at(var14, var10 - 1, var16);
               float var24 = 0.0F;
               if (var17 == Block.field_927.block_id) {
                  var24 = 1.0F;
                  if (var8.block_data_at(var14, var10 - 1, var16) > 0) {
                     var24 = 3.0F;
                  }
               }

               if (var14 != var9 || var16 != var11) {
                  var24 /= 4.0F;
               }

               var12 += var24;
            }
         }

         if (var7 || var23 && var22) {
            var12 /= 2.0F;
         }

         if (var5.nextInt((int)(100.0F / var12)) == 0) {
            int var21 = var6 + 1;
            var1.method_197(var2, var3, var4, var21);
         }
      }

   }

   // $FF: renamed from: game (int, int) int
   public final int method_499(int var1, int var2) {
      if (var2 < 0) {
         var2 = 7;
      }

      return this.maybeDropCount + var2;
   }

   // $FF: renamed from: game () int
   public final int method_492() {
      return 6;
   }

   // $FF: renamed from: entities (net.minecraft.game.game.player, int, int, int, int) void
   public final void method_507(World var1, int var2, int var3, int var4, int var5) {
      super.method_507(var1, var2, var3, var4, var5);

      for(int var6 = 0; var6 < 3; ++var6) {
         if (var1.random_a.nextInt(15) <= var5) {
            float var7 = var1.random_a.nextFloat() * 0.7F + 0.15F;
            float var8 = var1.random_a.nextFloat() * 0.7F + 0.15F;
            float var9 = var1.random_a.nextFloat() * 0.7F + 0.15F;
            ItemDrop var10;
            (var10 = new ItemDrop(var1, (float)var2 + var7, (float)var3 + var8, (float)var4 + var9, new ItemStack(Item.field_172))).pickup_cooldown = 10;
            var1.add_entity(var10);
         }
      }

   }

   // $FF: renamed from: game (int, java.util.Random) int
   public final int method_513(int var1, Random var2) {
      System.out.println("Get resource: " + var1);
      return var1 == 7 ? Item.field_173.unique_id : -1;
   }

   // $FF: renamed from: game (java.util.Random) int
   public final int method_512(Random var1) {
      return 1;
   }
}
