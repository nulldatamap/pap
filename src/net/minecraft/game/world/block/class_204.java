package net.minecraft.game.world.block;

import net.minecraft.game.world.World;
import net.minecraft.game.world.material.Material;

// $FF: renamed from: net.minecraft.game.game.animals.ag
public final class class_204 extends Block {
   protected class_204(int var1) {
      super(19, Material.field_617);
      this.maybeDropCount = 48;
   }

   // $FF: renamed from: projectiles (net.minecraft.game.game.player, int, int, int) void
   public final void method_510(World var1, int var2, int var3, int var4) {
      for(int var5 = var2 - 2; var5 <= var2 + 2; ++var5) {
         for(int var6 = var3 - 2; var6 <= var3 + 2; ++var6) {
            for(int var7 = var4 - 2; var7 <= var4 + 2; ++var7) {
               if (var1.method_199(var5, var6, var7)) {
                  var1.method_164(var5, var6, var7, 0);
               }
            }
         }
      }

   }

   // $FF: renamed from: animals (net.minecraft.game.game.player, int, int, int) void
   public final void method_511(World var1, int var2, int var3, int var4) {
      for(int var5 = var2 - 2; var5 <= var2 + 2; ++var5) {
         for(int var6 = var3 - 2; var6 <= var3 + 2; ++var6) {
            for(int var7 = var4 - 2; var7 <= var4 + 2; ++var7) {
               var1.update_blocks(var5, var6, var7, var1.block_at(var5, var6, var7));
            }
         }
      }

   }
}
