package net.minecraft.game.world.block;

import java.util.Random;
import net.minecraft.game.world.World;
import net.minecraft.game.world.material.Material;

// $FF: renamed from: net.minecraft.game.game.animals.aj
public final class class_201 extends Block {
   // $FF: renamed from: game boolean
   private boolean field_952;

   public class_201(int var1, boolean var2) {
      super(var1, 6, Material.stone_material);
      this.field_952 = var2;
      if (!var2) {
         this.setStuff(0.0F, 0.0F, 0.0F, 1.0F, 0.5F, 1.0F);
      }

      this.method_488(255);
   }

   // $FF: renamed from: game (int) int
   public final int method_500(int var1) {
      return var1 <= 1 ? 6 : 5;
   }

   // $FF: renamed from: animals () boolean
   public final boolean method_503() {
      return this.field_952;
   }

   // $FF: renamed from: animals (net.minecraft.game.game.player, int, int, int, int) void
   public final void method_508(World var1, int var2, int var3, int var4, int var5) {
      if (this == Block.field_911) {
         ;
      }
   }

   // $FF: renamed from: projectiles (net.minecraft.game.game.player, int, int, int) void
   public final void method_510(World var1, int var2, int var3, int var4) {
      if (this != Block.field_911) {
         super.method_510(var1, var2, var3, var4);
      }

      if (var1.block_at(var2, var3 - 1, var4) == field_911.block_id) {
         var1.method_165(var2, var3, var4, 0);
         var1.method_165(var2, var3 - 1, var4, Block.field_910.block_id);
      }

   }

   // $FF: renamed from: game (int, java.util.Random) int
   public final int method_513(int var1, Random var2) {
      return Block.field_911.block_id;
   }

   // $FF: renamed from: entities () boolean
   public final boolean method_491() {
      return this.field_952;
   }

   // $FF: renamed from: projectiles (net.minecraft.game.game.player, int, int, int, int) boolean
   public final boolean method_497(World var1, int var2, int var3, int var4, int var5) {
      if (var5 == 1) {
         return true;
      } else if (!super.method_497(var1, var2, var3, var4, var5)) {
         return false;
      } else if (var5 == 0) {
         return true;
      } else {
         return var1.block_at(var2, var3, var4) != this.block_id;
      }
   }
}
