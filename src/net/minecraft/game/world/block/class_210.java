package net.minecraft.game.world.block;

import net.minecraft.game.world.World;

// $FF: renamed from: net.minecraft.game.game.animals.v
public final class class_210 extends class_208 {
   protected class_210(int var1, int var2) {
      super(var1, var2);
      this.setStuff(0.3F, 0.0F, 0.3F, 0.7F, 0.4F, 0.7F);
   }

   // $FF: renamed from: animals (int) boolean
   protected final boolean method_550(int var1) {
      return Block.transparency[var1];
   }

   // $FF: renamed from: player (net.minecraft.game.game.player, int, int, int) boolean
   public final boolean method_552(World var1, int var2, int var3, int var4) {
      if (var1.method_195(var2, var3, var4) <= 13) {
         var2 = var1.block_at(var2, var3 - 1, var4);
         if (Block.transparency[var2]) {
            return true;
         }
      }

      return false;
   }
}
