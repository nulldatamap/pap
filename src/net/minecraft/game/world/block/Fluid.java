package net.minecraft.game.world.block;

import java.util.Random;
import net.minecraft.game.world.World;
import net.minecraft.game.world.material.Material;
import net.minecraft.game.util.AABB;

// $FF: renamed from: net.minecraft.game.game.animals.m
public final class Fluid extends class_188 {
   // $FF: renamed from: aD int
   private int field_947;
   // $FF: renamed from: aE int
   private int field_948;
   // $FF: renamed from: aF java.util.Random
   private Random field_949 = new Random();
   // $FF: renamed from: aG int[]
   private int[] field_950 = new int[]{0, 1, 2, 3};

   protected Fluid(int var1, Material var2) {
      super(var1, var2);
      this.maybeDropCount = 14;
      if (var2 == Material.lava_block) {
         this.maybeDropCount = 30;
      }

      Block.field_866[var1] = true;
      this.field_948 = var1;
      this.field_947 = var1 + 1;
      this.setStuff(0.01F, -0.09F, 0.01F, 1.01F, 0.90999997F, 1.01F);
      this.maybeSetDrops(true);
   }

   // $FF: renamed from: projectiles (net.minecraft.game.game.player, int, int, int) void
   public final void method_510(World var1, int var2, int var3, int var4) {
      var1.method_186(var2, var3, var4, this.field_948);
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, java.util.Random) void
   public final void tick(World var1, int var2, int var3, int var4, Random var5) {
      this.method_531(var1, var2, var3, var4, 0);
   }

   // $FF: renamed from: player (net.minecraft.game.game.player, int, int, int, int) boolean
   public final boolean method_531(World var1, int var2, int var3, int var4, int var5) {
      boolean var10 = false;
      boolean var6;
      int var7;
      int var8;
      int var9;
      if ((var6 = this.maybeCheckBlockNearby(var1, var2 - 1, var3, var4)
              || this.maybeCheckBlockNearby(var1, var2 + 1, var3, var4)
              || this.maybeCheckBlockNearby(var1, var2, var3, var4 - 1)
              || this.maybeCheckBlockNearby(var1, var2, var3, var4 + 1))
              && var1.method_198(var2, var3 - 1, var4) == this.material
              && var1.method_209(var2, var3 - 1, var4, this.field_948, this.field_947) == 1) {
         if ((var7 = var1.method_208(var2, var3, var4, this.field_948, this.field_947)) != -9999) {
            if (var7 < 0) {
               return false;
            } else {
               var8 = var7 % 1024;
               var9 = (var7 >>= 10) % 1024;
               var2 = (var7 >>= 10) % 1024;
               var1.method_165(var8, var2, var9, 0);
               return false;
            }
         } else {
            return false;
         }
      } else {
         var10 = this.method_537(var1, var2, var3, var4, var2, var3 - 1, var4);

         for(var7 = 0; var7 < 4; ++var7) {
            var8 = this.field_949.nextInt(4 - var7) + var7;
            var9 = this.field_950[var7];
            this.field_950[var7] = this.field_950[var8];
            this.field_950[var8] = var9;
            if (this.field_950[var7] == 0 && !var10) {
               var10 = this.method_537(var1, var2, var3, var4, var2 - 1, var3, var4);
            }

            if (this.field_950[var7] == 1 && !var10) {
               var10 = this.method_537(var1, var2, var3, var4, var2 + 1, var3, var4);
            }

            if (this.field_950[var7] == 2 && !var10) {
               var10 = this.method_537(var1, var2, var3, var4, var2, var3, var4 - 1);
            }

            if (this.field_950[var7] == 3 && !var10) {
               var10 = this.method_537(var1, var2, var3, var4, var2, var3, var4 + 1);
            }
         }

         if (!var10 && var6) {
            if (this.field_949.nextInt(3) == 0) {
               if (this.field_949.nextInt(3) == 0) {
                  var10 = false;

                  for(var7 = 0; var7 < 4; ++var7) {
                     var8 = this.field_949.nextInt(4 - var7) + var7;
                     var9 = this.field_950[var7];
                     this.field_950[var7] = this.field_950[var8];
                     this.field_950[var8] = var9;
                     if (this.field_950[var7] == 0 && !var10) {
                        var10 = this.method_536(var1, var2, var3, var4, var2 - 1, var3, var4);
                     }

                     if (this.field_950[var7] == 1 && !var10) {
                        var10 = this.method_536(var1, var2, var3, var4, var2 + 1, var3, var4);
                     }

                     if (this.field_950[var7] == 2 && !var10) {
                        var10 = this.method_536(var1, var2, var3, var4, var2, var3, var4 - 1);
                     }

                     if (this.field_950[var7] == 3 && !var10) {
                        var10 = this.method_536(var1, var2, var3, var4, var2, var3, var4 + 1);
                     }
                  }
               } else if (this.material == Material.lava_block) {
                  var1.method_165(var2, var3, var4, Block.stone.block_id);
               } else {
                  var1.method_165(var2, var3, var4, 0);
               }
            }

            return false;
         } else {
            if (this.material == Material.water_block) {
               var10 = var10 | method_538(var1, var2 - 1, var3, var4) | method_538(var1, var2 + 1, var3, var4) | method_538(var1, var2, var3, var4 - 1) | method_538(var1, var2, var3, var4 + 1);
            }

            if (this.material == Material.lava_block) {
               var10 = var10 | method_539(var1, var2 - 1, var3, var4) | method_539(var1, var2 + 1, var3, var4) | method_539(var1, var2, var3, var4 - 1) | method_539(var1, var2, var3, var4 + 1);
            }

            if (!var10) {
               var1.set_block(var2, var3, var4, this.field_947);
            } else {
               var1.method_186(var2, var3, var4, this.field_948);
            }

            return var10;
         }
      }
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, int, int, int) boolean
   private boolean method_536(World var1, int var2, int var3, int var4, int var5, int var6, int var7) {
      if (this.maybeCheckBlockNearby(var1, var5, var6, var7)) {
         var1.method_165(var5, var6, var7, this.block_id);
         var1.method_186(var5, var6, var7, this.block_id);
         return true;
      } else {
         return false;
      }
   }

   // $FF: renamed from: animals (net.minecraft.game.game.player, int, int, int, int, int, int) boolean
   private boolean method_537(World var1, int var2, int var3, int var4, int var5, int var6, int var7) {
      if (!this.maybeCheckBlockNearby(var1, var5, var6, var7)) {
         return false;
      } else {
         if ((var2 = var1.method_208(var2, var3, var4, this.field_948, this.field_947)) != -9999) {
            if (var2 < 0) {
               return false;
            }

            var3 = var2 % 1024;
            var4 = (var2 >>= 10) % 1024;
            if (((var2 = (var2 >>= 10) % 1024) > var6 || !this.maybeCheckBlockNearby(var1, var5, var6 - 1, var7)) && var2 <= var6 && var3 != 0 && var3 != var1.width - 1 && var4 != 0 && var4 != var1.depth - 1) {
               return false;
            }

            var1.method_165(var3, var2, var4, 0);
         }

         var1.method_165(var5, var6, var7, this.block_id);
         var1.method_186(var5, var6, var7, this.block_id);
         return true;
      }
   }

   // $FF: renamed from: projectiles (net.minecraft.game.game.player, int, int, int, int) boolean
   public final boolean method_497(World var1, int var2, int var3, int var4, int var5) {
      if (var2 >= 0 && var3 >= 0 && var4 >= 0 && var2 < var1.width && var4 < var1.depth) {
         int var6;
         if ((var6 = var1.block_at(var2, var3, var4)) != this.field_948 && var6 != this.field_947) {
            return var5 == 1 && (var1.block_at(var2 - 1, var3, var4) == 0 || var1.block_at(var2 + 1, var3, var4) == 0 || var1.block_at(var2, var3, var4 - 1) == 0 || var1.block_at(var2, var3, var4 + 1) == 0) ? true : super.method_497(var1, var2, var3, var4, var5);
         } else {
            return false;
         }
      } else {
         return false;
      }
   }

   // $FF: renamed from: projectiles () boolean
   public final boolean method_504() {
      return false;
   }

   // $FF: renamed from: game (int, int, int) net.minecraft.game.projectiles.animals
   public final AABB method_502(int var1, int var2, int var3) {
      return null;
   }

   // $FF: renamed from: animals () boolean
   public final boolean method_503() {
      return false;
   }

   // $FF: renamed from: animals (net.minecraft.game.game.player, int, int, int, int) void
   public final void method_508(World var1, int var2, int var3, int var4, int var5) {
   }

   // $FF: renamed from: player () int
   public final int method_509() {
      return this.material == Material.lava_block ? 25 : 5;
   }

   // $FF: renamed from: game (java.util.Random) int
   public final int method_512(Random var1) {
      return 0;
   }

   // $FF: renamed from: particles () int
   public final int method_523() {
      return this.material == Material.water_block ? 1 : 0;
   }

   // $FF: renamed from: h (net.minecraft.game.game.player, int, int, int) boolean
   private static boolean method_538(World var0, int var1, int var2, int var3) {
      if (var0.block_at(var1, var2, var3) == Block.fire.block_id) {
         var0.method_165(var1, var2, var3, 0);
         return true;
      } else if (var0.block_at(var1, var2, var3) != Block.lava.block_id && var0.block_at(var1, var2, var3) != Block.infinite_lava.block_id) {
         return false;
      } else {
         var0.method_165(var1, var2, var3, Block.stone.block_id);
         return true;
      }
   }

   // $FF: renamed from: i (net.minecraft.game.game.player, int, int, int) boolean
   private static boolean method_539(World var0, int var1, int var2, int var3) {
      if (Block.fire.method_547(var0.block_at(var1, var2, var3))) {
         Block.fire.method_548(var0, var1, var2, var3);
         return true;
      } else {
         return false;
      }
   }
}
