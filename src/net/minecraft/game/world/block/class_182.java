package net.minecraft.game.world.block;

import java.util.Random;
import net.minecraft.game.world.material.Material;

// $FF: renamed from: net.minecraft.game.game.animals.y
public final class class_182 extends Block {
   public class_182(int var1, int var2) {
      super(47, 35, Material.wood_material);
   }

   // $FF: renamed from: game (int) int
   public final int method_500(int var1) {
      return var1 <= 1 ? 4 : this.maybeDropCount;
   }

   // $FF: renamed from: game (java.util.Random) int
   public final int method_512(Random var1) {
      return 0;
   }
}
