package net.minecraft.game.world.block;

import java.util.Random;
import net.minecraft.game.world.World;
import net.minecraft.game.world.material.Material;

// $FF: renamed from: net.minecraft.game.game.animals.af
public final class GrassBlock extends Block {
   protected GrassBlock(int var1) {
      super(2, Material.field_609);
      this.maybeDropCount = 3;
      this.maybeSetDrops(true);
   }

   // $FF: renamed from: game (int) int
   public final int method_500(int var1) {
      if (var1 == 1) {
         return 0;
      } else {
         return var1 == 0 ? 2 : 3;
      }
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, java.util.Random) void
   public final void tick(World world, int x, int y, int z, Random rand) {
      if (world.method_195(x, y + 1, z) < 4 && world.method_198(x, y + 1, z).method_346()) {
         if (rand.nextInt(4) == 0) {
            world.method_165(x, y, z, Block.dirt.block_id);
         }
      } else {
         if (world.method_195(x, y + 1, z) >= 9) {
            x = x + rand.nextInt(3) - 1;
            y = y + rand.nextInt(5) - 3;
            z = z + rand.nextInt(3) - 1;
            if (world.block_at(x, y, z) == Block.dirt.block_id && world.method_195(x, y + 1, z) >= 4 && !world.method_198(x, y + 1, z).method_346()) {
               world.method_165(x, y, z, Block.grass.block_id);
            }
         }

      }
   }

   // $FF: renamed from: game (int, java.util.Random) int
   public final int method_513(int var1, Random var2) {
      return Block.dirt.method_513(0, var2);
   }
}
