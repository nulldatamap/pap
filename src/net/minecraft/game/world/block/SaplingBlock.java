package net.minecraft.game.world.block;

import java.util.Random;
import net.minecraft.game.world.World;

// $FF: renamed from: net.minecraft.game.game.animals.z
public final class SaplingBlock extends class_208 {
   protected SaplingBlock(int var1, int var2) {
      super(6, 15);
      this.setStuff(0.099999994F, 0.0F, 0.099999994F, 0.9F, 0.8F, 0.9F);
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, java.util.Random) void
   public final void tick(World var1, int var2, int var3, int var4, Random var5) {
      super.tick(var1, var2, var3, var4, var5);
      if (var1.method_195(var2, var3 + 1, var4) >= 9 && var5.nextInt(5) == 0) {
         byte var6;
         if ((var6 = var1.block_data_at(var2, var3, var4)) < 15) {
            var1.method_197(var2, var3, var4, var6 + 1);
            return;
         }

         var1.set_block(var2, var3, var4, 0);
         if (!var1.method_201(var2, var3, var4)) {
            var1.set_block(var2, var3, var4, this.block_id);
         }
      }

   }
}
