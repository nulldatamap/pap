package net.minecraft.game.world.block;

import java.util.Random;
import net.minecraft.game.world.material.Material;

// $FF: renamed from: net.minecraft.game.game.animals.aa
public final class class_195 extends Block {
   protected class_195(int var1) {
      super(17, Material.wood_material);
      this.maybeDropCount = 20;
   }

   // $FF: renamed from: game (java.util.Random) int
   public final int method_512(Random var1) {
      return 1;
   }

   // $FF: renamed from: game (int, java.util.Random) int
   public final int method_513(int var1, Random var2) {
      return Block.field_884.block_id;
   }

   // $FF: renamed from: game (int) int
   public final int method_500(int var1) {
      if (var1 == 1) {
         return 21;
      } else {
         return var1 == 0 ? 21 : 20;
      }
   }
}
