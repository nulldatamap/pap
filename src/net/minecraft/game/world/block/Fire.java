package net.minecraft.game.world.block;

import java.util.Random;
import net.minecraft.game.world.World;
import net.minecraft.game.world.material.Material;
import net.minecraft.game.util.AABB;

// $FF: renamed from: net.minecraft.game.game.animals.ah
public final class Fire extends Block {
   // $FF: renamed from: game int[]
   private int[] field_954 = new int[256];
   // $FF: renamed from: animals int[]
   private int[] field_955 = new int[256];

   protected Fire(int var1, int var2) {
      super(51, 31, Material.field_619);
      this.method_542(Block.wood_planks.block_id, 5, 20);
      this.method_542(Block.field_884.block_id, 5, 5);
      this.method_542(Block.field_885.block_id, 30, 60);
      this.method_542(Block.field_914.block_id, 30, 20);
      this.method_542(Block.tnt.block_id, 15, 100);

      for(var1 = 0; var1 < 16; ++var1) {
         this.method_542(Block.field_888.block_id + var1, 30, 60);
      }

      this.maybeSetDrops(true);
   }

   // $FF: renamed from: entities (int, int, int) void
   private void method_542(int var1, int var2, int var3) {
      this.field_954[var1] = var2;
      this.field_955[var1] = var3;
   }

   // $FF: renamed from: game (int, int, int) net.minecraft.game.projectiles.animals
   public final AABB method_502(int var1, int var2, int var3) {
      return null;
   }

   // $FF: renamed from: animals () boolean
   public final boolean method_503() {
      return false;
   }

   // $FF: renamed from: entities () boolean
   public final boolean method_491() {
      return false;
   }

   // $FF: renamed from: game () int
   public final int method_492() {
      return 3;
   }

   // $FF: renamed from: game (java.util.Random) int
   public final int method_512(Random var1) {
      return 0;
   }

   // $FF: renamed from: player () int
   public final int method_509() {
      return 20;
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, java.util.Random) void
   public final void tick(World var1, int var2, int var3, int var4, Random var5) {
      byte var6;
      if ((var6 = var1.block_data_at(var2, var3, var4)) < 15) {
         var1.method_197(var2, var3, var4, var6 + 1);
         var1.method_186(var2, var3, var4, this.block_id);
      }

      if (!this.method_544(var1, var2, var3, var4)) {
         if (!var1.method_170(var2, var3 - 1, var4) || var6 > 3) {
            var1.method_165(var2, var3, var4, 0);
         }

      } else if (!this.method_545(var1, var2, var3 - 1, var4) && var6 == 15 && var5.nextInt(4) == 0) {
         var1.method_165(var2, var3, var4, 0);
      } else {
         if (var6 % 5 == 0 && var6 > 5) {
            this.method_543(var1, var2 + 1, var3, var4, 300, var5);
            this.method_543(var1, var2 - 1, var3, var4, 300, var5);
            this.method_543(var1, var2, var3 - 1, var4, 100, var5);
            this.method_543(var1, var2, var3 + 1, var4, 200, var5);
            this.method_543(var1, var2, var3, var4 - 1, 300, var5);
            this.method_543(var1, var2, var3, var4 + 1, 300, var5);

            for(int var16 = var2 - 1; var16 <= var2 + 1; ++var16) {
               for(int var7 = var4 - 1; var7 <= var4 + 1; ++var7) {
                  for(int var8 = var3 - 1; var8 <= var3 + 4; ++var8) {
                     if (var16 != var2 || var8 != var3 || var7 != var4) {
                        int var9 = 100;
                        if (var8 > var3 + 1) {
                           var9 = 100 + (var8 - (var3 + 1)) * 100;
                        }

                        int var10000;
                        if (var1.block_at(var16, var8, var7) != 0) {
                           var10000 = 0;
                        } else {
                           int var15 = this.method_546(var1, var16 + 1, var8, var7, 0);
                           var15 = this.method_546(var1, var16 - 1, var8, var7, var15);
                           var15 = this.method_546(var1, var16, var8 - 1, var7, var15);
                           var15 = this.method_546(var1, var16, var8 + 1, var7, var15);
                           var15 = this.method_546(var1, var16, var8, var7 - 1, var15);
                           var10000 = this.method_546(var1, var16, var8, var7 + 1, var15);
                        }

                        int var10 = var10000;
                        if (var10000 > 0 && var5.nextInt(var9) <= var10) {
                           var1.method_165(var16, var8, var7, this.block_id);
                        }
                     }
                  }
               }
            }
         }

      }
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, int, java.util.Random) void
   private void method_543(World var1, int var2, int var3, int var4, int var5, Random var6) {
      int var7 = this.field_955[var1.block_at(var2, var3, var4)];
      if (var6.nextInt(var5) < var7) {
         boolean var8 = var1.block_at(var2, var3, var4) == Block.tnt.block_id;
         if (var6.nextInt(2) == 0) {
            var1.method_165(var2, var3, var4, this.block_id);
         } else {
            var1.method_165(var2, var3, var4, 0);
         }

         if (var8) {
            Block.tnt.method_507(var1, var2, var3, var4, 0);
         }
      }

   }

   // $FF: renamed from: i (net.minecraft.game.game.player, int, int, int) boolean
   private boolean method_544(World var1, int var2, int var3, int var4) {
      if (this.method_545(var1, var2 + 1, var3, var4)) {
         return true;
      } else if (this.method_545(var1, var2 - 1, var3, var4)) {
         return true;
      } else if (this.method_545(var1, var2, var3 - 1, var4)) {
         return true;
      } else if (this.method_545(var1, var2, var3 + 1, var4)) {
         return true;
      } else if (this.method_545(var1, var2, var3, var4 - 1)) {
         return true;
      } else {
         return this.method_545(var1, var2, var3, var4 + 1);
      }
   }

   // $FF: renamed from: projectiles () boolean
   public final boolean method_504() {
      return false;
   }

   // $FF: renamed from: player (net.minecraft.game.game.player, int, int, int) boolean
   public final boolean method_545(World var1, int var2, int var3, int var4) {
      return this.field_954[var1.block_at(var2, var3, var4)] > 0;
   }

   // $FF: renamed from: player (net.minecraft.game.game.player, int, int, int, int) int
   private int method_546(World var1, int var2, int var3, int var4, int var5) {
      int var6;
      return (var6 = this.field_954[var1.block_at(var2, var3, var4)]) > var5 ? var6 : var5;
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int) boolean
   public final boolean method_524(World var1, int var2, int var3, int var4) {
      return var1.method_170(var2, var3 - 1, var4) || this.method_544(var1, var2, var3, var4);
   }

   // $FF: renamed from: animals (net.minecraft.game.game.player, int, int, int, int) void
   public final void method_508(World var1, int var2, int var3, int var4, int var5) {
      if (!var1.method_170(var2, var3 - 1, var4) && !this.method_544(var1, var2, var3, var4)) {
         var1.method_165(var2, var3, var4, 0);
      }
   }

   // $FF: renamed from: projectiles (net.minecraft.game.game.player, int, int, int) void
   public final void method_510(World var1, int var2, int var3, int var4) {
      if (!var1.method_170(var2, var3 - 1, var4) && !this.method_544(var1, var2, var3, var4)) {
         var1.method_165(var2, var3, var4, 0);
      } else {
         var1.method_186(var2, var3, var4, this.block_id);
      }
   }

   // $FF: renamed from: animals (int) boolean
   public final boolean method_547(int var1) {
      return this.field_954[var1] > 0;
   }

   // $FF: renamed from: h (net.minecraft.game.game.player, int, int, int) void
   public final void method_548(World var1, int var2, int var3, int var4) {
      boolean var5 = false;
      if (!(var5 = method_549(var1, var2, var3 + 1, var4))) {
         var5 = method_549(var1, var2 - 1, var3, var4);
      }

      if (!var5) {
         var5 = method_549(var1, var2 + 1, var3, var4);
      }

      if (!var5) {
         var5 = method_549(var1, var2, var3, var4 - 1);
      }

      if (!var5) {
         var5 = method_549(var1, var2, var3, var4 + 1);
      }

      if (!var5) {
         var5 = method_549(var1, var2, var3 - 1, var4);
      }

      if (!var5) {
         var1.method_165(var2, var3, var4, Block.fire.block_id);
      }

   }

   // $FF: renamed from: animals (net.minecraft.game.game.player, int, int, int, java.util.Random) void
   public final void method_506(World var1, int var2, int var3, int var4, Random var5) {
      if (var5.nextInt(24) == 0) {
         var1.method_212((float)var2 + 0.5F, (float)var3 + 0.5F, (float)var4 + 0.5F, "fire.fire", 1.0F + var5.nextFloat(), var5.nextFloat() * 0.7F + 0.3F);
      }

      int var6;
      float var7;
      float var8;
      float var9;
      if (!var1.method_170(var2, var3 - 1, var4) && !Block.fire.method_545(var1, var2, var3 - 1, var4)) {
         if (Block.fire.method_545(var1, var2 - 1, var3, var4)) {
            for(var6 = 0; var6 < 2; ++var6) {
               var7 = (float)var2 + var5.nextFloat() * 0.1F;
               var8 = (float)var3 + var5.nextFloat();
               var9 = (float)var4 + var5.nextFloat();
               var1.spawn_effect("largesmoke", var7, var8, var9, 0.0F, 0.0F, 0.0F);
            }
         }

         if (Block.fire.method_545(var1, var2 + 1, var3, var4)) {
            for(var6 = 0; var6 < 2; ++var6) {
               var7 = (float)(var2 + 1) - var5.nextFloat() * 0.1F;
               var8 = (float)var3 + var5.nextFloat();
               var9 = (float)var4 + var5.nextFloat();
               var1.spawn_effect("largesmoke", var7, var8, var9, 0.0F, 0.0F, 0.0F);
            }
         }

         if (Block.fire.method_545(var1, var2, var3, var4 - 1)) {
            for(var6 = 0; var6 < 2; ++var6) {
               var7 = (float)var2 + var5.nextFloat();
               var8 = (float)var3 + var5.nextFloat();
               var9 = (float)var4 + var5.nextFloat() * 0.1F;
               var1.spawn_effect("largesmoke", var7, var8, var9, 0.0F, 0.0F, 0.0F);
            }
         }

         if (Block.fire.method_545(var1, var2, var3, var4 + 1)) {
            for(var6 = 0; var6 < 2; ++var6) {
               var7 = (float)var2 + var5.nextFloat();
               var8 = (float)var3 + var5.nextFloat();
               var9 = (float)(var4 + 1) - var5.nextFloat() * 0.1F;
               var1.spawn_effect("largesmoke", var7, var8, var9, 0.0F, 0.0F, 0.0F);
            }
         }

         if (Block.fire.method_545(var1, var2, var3 + 1, var4)) {
            for(var6 = 0; var6 < 2; ++var6) {
               var7 = (float)var2 + var5.nextFloat();
               var8 = (float)(var3 + 1) - var5.nextFloat() * 0.1F;
               var9 = (float)var4 + var5.nextFloat();
               var1.spawn_effect("largesmoke", var7, var8, var9, 0.0F, 0.0F, 0.0F);
            }
         }

      } else {
         for(var6 = 0; var6 < 3; ++var6) {
            var7 = (float)var2 + var5.nextFloat();
            var8 = (float)var3 + var5.nextFloat() * 0.5F + 0.5F;
            var9 = (float)var4 + var5.nextFloat();
            var1.spawn_effect("largesmoke", var7, var8, var9, 0.0F, 0.0F, 0.0F);
         }

      }
   }

   // $FF: renamed from: j (net.minecraft.game.game.player, int, int, int) boolean
   private static boolean method_549(World var0, int var1, int var2, int var3) {
      int var4;
      if ((var4 = var0.block_at(var1, var2, var3)) == Block.fire.block_id) {
         return true;
      } else if (var4 == 0) {
         var0.method_165(var1, var2, var3, Block.fire.block_id);
         return true;
      } else {
         return false;
      }
   }
}
