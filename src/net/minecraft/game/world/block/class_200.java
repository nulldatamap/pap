package net.minecraft.game.world.block;

import java.util.Random;
import net.minecraft.game.items.Item;

// $FF: renamed from: net.minecraft.game.game.animals.player
public final class class_200 extends FallableBlock {
   public class_200(int var1, int var2) {
      super(13, 19);
   }

   // $FF: renamed from: game (int, java.util.Random) int
   public final int method_513(int var1, Random var2) {
      return var2.nextInt(10) == 0 ? Item.field_195.unique_id : this.block_id;
   }
}
