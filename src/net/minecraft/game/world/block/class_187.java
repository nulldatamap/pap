package net.minecraft.game.world.block;

import java.util.Random;

import net.minecraft.game.world.World;
import net.minecraft.game.class_220;
import net.minecraft.game.Storage;
import net.minecraft.game.world.block.a.DataBlock;
import net.minecraft.game.world.block.a.ChestData;
import net.minecraft.game.world.material.Material;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.entities.misc.ItemDrop;
import net.minecraft.game.entities.player.Player;

// $FF: renamed from: net.minecraft.game.game.animals.game
public final class class_187 extends class_185 {
   // $FF: renamed from: game java.util.Random
   private Random field_944 = new Random();

   protected class_187(int var1) {
      super(54, Material.wood_material);
      this.maybeDropCount = 26;
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, int) int
   public final int method_498(World var1, int var2, int var3, int var4, int var5) {
      if (var5 == 1) {
         return this.maybeDropCount - 1;
      } else if (var5 == 0) {
         return this.maybeDropCount - 1;
      } else {
         int var6 = var1.block_at(var2, var3, var4 - 1);
         int var7 = var1.block_at(var2, var3, var4 + 1);
         int var8 = var1.block_at(var2 - 1, var3, var4);
         int var9 = var1.block_at(var2 + 1, var3, var4);
         int var10;
         int var11;
         int var12;
         byte var13;
         if (var6 != this.block_id && var7 != this.block_id) {
            if (var8 != this.block_id && var9 != this.block_id) {
               byte var14 = 3;
               if (Block.transparency[var6] && !Block.transparency[var7]) {
                  var14 = 3;
               }

               if (Block.transparency[var7] && !Block.transparency[var6]) {
                  var14 = 2;
               }

               if (Block.transparency[var8] && !Block.transparency[var9]) {
                  var14 = 5;
               }

               if (Block.transparency[var9] && !Block.transparency[var8]) {
                  var14 = 4;
               }

               return var5 == var14 ? this.maybeDropCount + 1 : this.maybeDropCount;
            } else if (var5 != 4 && var5 != 5) {
               var10 = 0;
               if (var8 == this.block_id) {
                  var10 = -1;
               }

               var11 = var1.block_at(var8 == this.block_id ? var2 - 1 : var2 + 1, var3, var4 - 1);
               var12 = var1.block_at(var8 == this.block_id ? var2 - 1 : var2 + 1, var3, var4 + 1);
               if (var5 == 3) {
                  var10 = -1 - var10;
               }

               var13 = 3;
               if ((Block.transparency[var6] || Block.transparency[var11]) && !Block.transparency[var7] && !Block.transparency[var12]) {
                  var13 = 3;
               }

               if ((Block.transparency[var7] || Block.transparency[var12]) && !Block.transparency[var6] && !Block.transparency[var11]) {
                  var13 = 2;
               }

               return (var5 == var13 ? this.maybeDropCount + 16 : this.maybeDropCount + 32) + var10;
            } else {
               return this.maybeDropCount;
            }
         } else if (var5 != 2 && var5 != 3) {
            var10 = 0;
            if (var6 == this.block_id) {
               var10 = -1;
            }

            var11 = var1.block_at(var2 - 1, var3, var6 == this.block_id ? var4 - 1 : var4 + 1);
            var12 = var1.block_at(var2 + 1, var3, var6 == this.block_id ? var4 - 1 : var4 + 1);
            if (var5 == 4) {
               var10 = -1 - var10;
            }

            var13 = 5;
            if ((Block.transparency[var8] || Block.transparency[var11]) && !Block.transparency[var9] && !Block.transparency[var12]) {
               var13 = 5;
            }

            if ((Block.transparency[var9] || Block.transparency[var12]) && !Block.transparency[var8] && !Block.transparency[var11]) {
               var13 = 4;
            }

            return (var5 == var13 ? this.maybeDropCount + 16 : this.maybeDropCount + 32) + var10;
         } else {
            return this.maybeDropCount;
         }
      }
   }

   // $FF: renamed from: game (int) int
   public final int method_500(int var1) {
      if (var1 == 1) {
         return this.maybeDropCount - 1;
      } else if (var1 == 0) {
         return this.maybeDropCount - 1;
      } else {
         return var1 == 3 ? this.maybeDropCount + 1 : this.maybeDropCount;
      }
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int) boolean
   public final boolean method_524(World var1, int var2, int var3, int var4) {
      int var5 = 0;
      if (var1.block_at(var2 - 1, var3, var4) == this.block_id) {
         ++var5;
      }

      if (var1.block_at(var2 + 1, var3, var4) == this.block_id) {
         ++var5;
      }

      if (var1.block_at(var2, var3, var4 - 1) == this.block_id) {
         ++var5;
      }

      if (var1.block_at(var2, var3, var4 + 1) == this.block_id) {
         ++var5;
      }

      if (var5 > 1) {
         return false;
      } else if (this.method_530(var1, var2 - 1, var3, var4)) {
         return false;
      } else if (this.method_530(var1, var2 + 1, var3, var4)) {
         return false;
      } else if (this.method_530(var1, var2, var3, var4 - 1)) {
         return false;
      } else {
         return !this.method_530(var1, var2, var3, var4 + 1);
      }
   }

   // $FF: renamed from: player (net.minecraft.game.game.player, int, int, int) boolean
   private boolean method_530(World var1, int var2, int var3, int var4) {
      if (var1.block_at(var2, var3, var4) != this.block_id) {
         return false;
      } else if (var1.block_at(var2 - 1, var3, var4) == this.block_id) {
         return true;
      } else if (var1.block_at(var2 + 1, var3, var4) == this.block_id) {
         return true;
      } else if (var1.block_at(var2, var3, var4 - 1) == this.block_id) {
         return true;
      } else {
         return var1.block_at(var2, var3, var4 + 1) == this.block_id;
      }
   }

   // $FF: renamed from: animals (net.minecraft.game.game.player, int, int, int) void
   public final void method_511(World var1, int var2, int var3, int var4) {
      ChestData var5 = (ChestData)var1.method_216(var2, var3, var4);

      for(int var6 = 0; var6 < var5.capacity(); ++var6) {
         ItemStack var7;
         if ((var7 = var5.get_slot(var6)) != null) {
            float var8 = this.field_944.nextFloat() * 0.8F + 0.1F;
            float var9 = this.field_944.nextFloat() * 0.8F + 0.1F;
            float var10 = this.field_944.nextFloat() * 0.8F + 0.1F;

            while(var7.count > 0) {
               int var11;
               if ((var11 = this.field_944.nextInt(21) + 10) > var7.count) {
                  var11 = var7.count;
               }

               var7.count -= var11;
               ItemDrop var12;
               (var12 = new ItemDrop(var1, (float)var2 + var8, (float)var3 + var9, (float)var4 + var10, new ItemStack(var7.id, var11, var7.damage))).vel_x = (float)this.field_944.nextGaussian() * 0.05F;
               var12.vel_y = (float)this.field_944.nextGaussian() * 0.05F + 0.2F;
               var12.vel_z = (float)this.field_944.nextGaussian() * 0.05F;
               var1.add_entity(var12);
            }
         }
      }

      super.method_511(var1, var2, var3, var4);
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, int, int, int, net.minecraft.game.entities.player.game) boolean
   public final boolean method_525(World var1, int var2, int var3, int var4, Player var5) {
      Object var6 = (ChestData)var1.method_216(var2, var3, var4);
      if (var1.method_170(var2, var3 + 1, var4)) {
         return true;
      } else if (var1.block_at(var2 - 1, var3, var4) == this.block_id && var1.method_170(var2 - 1, var3 + 1, var4)) {
         return true;
      } else if (var1.block_at(var2 + 1, var3, var4) == this.block_id && var1.method_170(var2 + 1, var3 + 1, var4)) {
         return true;
      } else if (var1.block_at(var2, var3, var4 - 1) == this.block_id && var1.method_170(var2, var3 + 1, var4 - 1)) {
         return true;
      } else if (var1.block_at(var2, var3, var4 + 1) == this.block_id && var1.method_170(var2, var3 + 1, var4 + 1)) {
         return true;
      } else {
         if (var1.block_at(var2 - 1, var3, var4) == this.block_id) {
            var6 = new class_220("Large chest", (ChestData)var1.method_216(var2 - 1, var3, var4), (Storage)var6);
         }

         if (var1.block_at(var2 + 1, var3, var4) == this.block_id) {
            var6 = new class_220("Large chest", (Storage)var6, (ChestData)var1.method_216(var2 + 1, var3, var4));
         }

         if (var1.block_at(var2, var3, var4 - 1) == this.block_id) {
            var6 = new class_220("Large chest", (ChestData)var1.method_216(var2, var3, var4 - 1), (Storage)var6);
         }

         if (var1.block_at(var2, var3, var4 + 1) == this.block_id) {
            var6 = new class_220("Large chest", (Storage)var6, (ChestData)var1.method_216(var2, var3, var4 + 1));
         }

         var5.method_74((Storage)var6);
         return true;
      }
   }

   // $FF: renamed from: a_ () net.minecraft.game.game.animals.game.game
   protected final DataBlock method_528() {
      return new ChestData();
   }
}
