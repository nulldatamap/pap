package net.minecraft.game.world.block.a;

import com.data.nbt.ListTag;
import com.data.nbt.CompoundTag;
import net.minecraft.game.Storage;
import net.minecraft.game.items.ItemStack;

// $FF: renamed from: net.minecraft.game.game.animals.game.entities
public final class ChestData extends DataBlock implements Storage {
   // $FF: renamed from: player net.minecraft.game.animals.k[]
   private ItemStack[] field_394 = new ItemStack[36];

   // $FF: renamed from: game () int
   public final int capacity() {
      return 27;
   }

   // $FF: renamed from: game (int) net.minecraft.game.animals.k
   public final ItemStack get_slot(int var1) {
      return this.field_394[var1];
   }

   // $FF: renamed from: game (int, int) net.minecraft.game.animals.k
   public final ItemStack take_from(int var1, int var2) {
      if (this.field_394[var1] != null) {
         ItemStack var3;
         if (this.field_394[var1].count <= var2) {
            var3 = this.field_394[var1];
            this.field_394[var1] = null;
            return var3;
         } else {
            var3 = this.field_394[var1].split_stack(var2);
            if (this.field_394[var1].count == 0) {
               this.field_394[var1] = null;
            }

            return var3;
         }
      } else {
         return null;
      }
   }

   // $FF: renamed from: game (int, net.minecraft.game.animals.k) void
   public final void set_slot(int var1, ItemStack var2) {
      this.field_394[var1] = var2;
      if (var2 != null && var2.count > 64) {
         var2.count = 64;
      }

   }

   // $FF: renamed from: animals () java.lang.String
   public final String nbtName() {
      return "Chest";
   }

   // $FF: renamed from: game (com.game.game.entities) void
   public final void method_256(CompoundTag var1) {
      ListTag var5 = var1.read_list("Items");
      this.field_394 = new ItemStack[27];

      for(int var2 = 0; var2 < var5.method_438(); ++var2) {
         CompoundTag var3;
         int var4;
         if ((var4 = (var3 = (CompoundTag)var5.get(var2)).read_byte("Slot") & 255) >= 0 && var4 < this.field_394.length) {
            this.field_394[var4] = new ItemStack(var3);
         }
      }

   }

   // $FF: renamed from: animals (com.game.game.entities) void
   public final void method_257(CompoundTag var1) {
      var1.write_string("id", "Chest");
      ListTag var2 = new ListTag();

      for(int var3 = 0; var3 < this.field_394.length; ++var3) {
         if (this.field_394[var3] != null) {
            CompoundTag var4;
            (var4 = new CompoundTag()).write_byte("Slot", (byte)var3);
            this.field_394[var3].method_592(var4);
            var2.method_436(var4);
         }
      }

      var1.write_list("Items", var2);
   }

   // $FF: renamed from: entities () int
   public final int max_stack_size() {
      return 64;
   }
}
