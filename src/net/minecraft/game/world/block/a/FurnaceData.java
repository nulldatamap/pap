package net.minecraft.game.world.block.a;

import com.data.nbt.ListTag;
import com.data.nbt.CompoundTag;
import net.minecraft.game.Storage;
import net.minecraft.game.world.World;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.world.material.Material;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.items.Item;

// $FF: renamed from: net.minecraft.game.game.animals.game.animals
public final class FurnaceData extends DataBlock implements Storage {
   // $FF: renamed from: player net.minecraft.game.animals.k[]
   private ItemStack[] contents = new ItemStack[3];
   // $FF: renamed from: particles int
   private int burn_time = 0;
   // $FF: renamed from: player int
   private int field_392 = 0;
   // $FF: renamed from: h int
   private int cook_time = 0;

   // $FF: renamed from: game () int
   public final int capacity() {
      return this.contents.length;
   }

   // $FF: renamed from: game (int) net.minecraft.game.animals.k
   public final ItemStack get_slot(int var1) {
      return this.contents[var1];
   }

   // $FF: renamed from: game (int, int) net.minecraft.game.animals.k
   public final ItemStack take_from(int var1, int var2) {
      if (this.contents[var1] != null) {
         ItemStack var3;
         if (this.contents[var1].count <= var2) {
            var3 = this.contents[var1];
            this.contents[var1] = null;
            return var3;
         } else {
            var3 = this.contents[var1].split_stack(var2);
            if (this.contents[var1].count == 0) {
               this.contents[var1] = null;
            }

            return var3;
         }
      } else {
         return null;
      }
   }

   // $FF: renamed from: game (int, net.minecraft.game.animals.k) void
   public final void set_slot(int var1, ItemStack var2) {
      this.contents[var1] = var2;
      if (var2 != null && var2.count > 64) {
         var2.count = 64;
      }

   }

   // $FF: renamed from: animals () java.lang.String
   public final String nbtName() {
      return "Chest";
   }

   // $FF: renamed from: game (com.game.game.entities) void
   public final void method_256(CompoundTag var1) {
      super.method_256(var1);
      ListTag var2 = var1.read_list("Items");
      this.contents = new ItemStack[this.contents.length];

      for(int var3 = 0; var3 < var2.method_438(); ++var3) {
         CompoundTag var4;
         byte var5;
         if ((var5 = (var4 = (CompoundTag)var2.get(var3)).read_byte("Slot")) >= 0 && var5 < this.contents.length) {
            this.contents[var5] = new ItemStack(var4);
         }
      }

      this.burn_time = var1.read_short("BurnTime");
      this.cook_time = var1.read_short("CookTime");
      this.field_392 = method_264(this.contents[1]);
      System.out.println("Lit: " + this.burn_time + "/" + this.field_392);
   }

   // $FF: renamed from: animals (com.game.game.entities) void
   public final void method_257(CompoundTag var1) {
      super.method_257(var1);
      var1.write_short("BurnTime", (short)this.burn_time);
      var1.write_short("CookTime", (short)this.cook_time);
      var1.write_string("id", "Furnace");
      ListTag var2 = new ListTag();

      for(int var3 = 0; var3 < this.contents.length; ++var3) {
         if (this.contents[var3] != null) {
            CompoundTag var4;
            (var4 = new CompoundTag()).write_byte("Slot", (byte)var3);
            this.contents[var3].method_592(var4);
            var2.method_436(var4);
         }
      }

      var1.write_list("Items", var2);
   }

   // $FF: renamed from: entities () int
   public final int max_stack_size() {
      return 64;
   }

   // $FF: renamed from: animals (int) int
   public final int method_259(int var1) {
      return this.cook_time * 24 / 200;
   }

   // $FF: renamed from: entities (int) int
   public final int method_260(int var1) {
      return this.burn_time * 12 / this.field_392;
   }

   // $FF: renamed from: player () boolean
   public final boolean method_261() {
      return this.burn_time > 0;
   }

   // $FF: renamed from: projectiles () void
   public final void method_258() {
      boolean var1 = this.burn_time > 0;
      if (this.burn_time > 0) {
         --this.burn_time;
      }

      if (this.burn_time == 0 && this.method_262()) {
         this.field_392 = this.burn_time = method_264(this.contents[1]);
         if (this.burn_time > 0 && this.contents[1] != null) {
            --this.contents[1].count;
            if (this.contents[1].count == 0) {
               this.contents[1] = null;
            }
         }
      }

      if (this.method_261() && this.method_262()) {
         ++this.cook_time;
         if (this.cook_time == 200) {
            this.cook_time = 0;
            if (this.method_262()) {
               int var3 = method_263(this.contents[0].item().unique_id);
               if (this.contents[2] == null) {
                  this.contents[2] = new ItemStack(var3, 1);
               } else if (this.contents[2].id == var3) {
                  ++this.contents[2].count;
               }

               --this.contents[0].count;
               if (this.contents[0].count <= 0) {
                  this.contents[0] = null;
               }
            }
         }
      } else {
         this.cook_time = 0;
      }

      if (var1 != this.burn_time > 0) {
         boolean var10000 = this.burn_time > 0;
         int var5 = this.z;
         int var4 = this.y;
         int var8 = this.x;
         World var9 = this.world;
         boolean var2 = var10000;
         byte var6 = var9.block_data_at(var8, var4, var5);
         DataBlock var7 = var9.method_216(var8, var4, var5);
         if (var2) {
            var9.method_165(var8, var4, var5, Block.field_929.block_id);
         } else {
            var9.method_165(var8, var4, var5, Block.field_928.block_id);
         }

         var9.method_197(var8, var4, var5, var6);
         var9.method_214(var8, var4, var5, var7);
      }

   }

   // $FF: renamed from: particles () boolean
   private boolean method_262() {
      if (this.contents[0] == null) {
         return false;
      } else {
         int var1;
         if ((var1 = method_263(this.contents[0].item().unique_id)) < 0) {
            return false;
         } else if (this.contents[2] == null) {
            return true;
         } else if (this.contents[2].id != var1) {
            return false;
         } else if (this.contents[2].count < 64) {
            return true;
         } else {
            return this.contents[2].count < Item.items[var1].get_stack_size();
         }
      }
   }

   // $FF: renamed from: projectiles (int) int
   private static int method_263(int var0) {
      if (var0 == Block.iron.block_id) {
         return Item.iron_ingot.unique_id;
      } else if (var0 == Block.gold.block_id) {
         return Item.gold_ingot.unique_id;
      } else if (var0 == Block.diamond.block_id) {
         return Item.diamond.unique_id;
      } else if (var0 == Block.sand.block_id) {
         return Block.glass.block_id;
      } else if (var0 == Item.raw_pork.unique_id) {
         return Item.cooked_pork.unique_id;
      } else {
         return var0 == Block.cobble_stone.block_id ? Block.stone.block_id : -1;
      }
   }

   // $FF: renamed from: game (net.minecraft.game.animals.k) int
   private static int method_264(ItemStack var0) {
      if (var0 == null) {
         return 0;
      } else {
         int var1;
         if ((var1 = var0.item().unique_id) < 256 && Block.blocks[var1].material == Material.wood_material) {
            return 300;
         } else if (var1 == Item.stick.unique_id) {
            return 100;
         } else {
            return var1 == Item.field_140.unique_id ? 1600 : 0;
         }
      }
   }
}
