package net.minecraft.game.world.block.a;

import com.data.nbt.CompoundTag;
import net.minecraft.game.world.World;

// $FF: renamed from: net.minecraft.game.game.animals.game.game
public class DataBlock {
   // $FF: renamed from: game net.minecraft.game.game.player
   public World world;
   // $FF: renamed from: animals int
   public int x;
   // $FF: renamed from: entities int
   public int y;
   // $FF: renamed from: projectiles int
   public int z;

   // $FF: renamed from: game (com.game.game.entities) void
   public void method_256(CompoundTag var1) {
   }

   // $FF: renamed from: animals (com.game.game.entities) void
   public void method_257(CompoundTag var1) {
   }

   // $FF: renamed from: projectiles () void
   public void method_258() {
   }
}
