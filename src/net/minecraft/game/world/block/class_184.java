package net.minecraft.game.world.block;

import net.minecraft.game.world.material.Material;

// $FF: renamed from: net.minecraft.game.game.animals.projectiles
public final class class_184 extends Block {
   public class_184(int var1, int var2) {
      super(var1, Material.field_612);
      this.maybeDropCount = var2;
   }

   // $FF: renamed from: game (int) int
   public final int method_500(int var1) {
      if (var1 == 1) {
         return this.maybeDropCount - 16;
      } else {
         return var1 == 0 ? this.maybeDropCount + 16 : this.maybeDropCount;
      }
   }
}
