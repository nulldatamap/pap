package net.minecraft.game.world.material;

// $FF: renamed from: net.minecraft.game.game.projectiles.entities
public class Material {
   // $FF: renamed from: game net.minecraft.game.game.projectiles.entities
   public static final Material field_608 = new class_133();
   // $FF: renamed from: animals net.minecraft.game.game.projectiles.entities
   public static final Material field_609 = new Material();
   // $FF: renamed from: entities net.minecraft.game.game.projectiles.entities
   public static final Material wood_material = new Material();
   // $FF: renamed from: projectiles net.minecraft.game.game.projectiles.entities
   public static final Material stone_material = new Material();
   // $FF: renamed from: player net.minecraft.game.game.projectiles.entities
   public static final Material field_612 = new Material();
   // $FF: renamed from: particles net.minecraft.game.game.projectiles.entities
   public static final Material water_block = new class_132();
   // $FF: renamed from: player net.minecraft.game.game.projectiles.entities
   public static final Material lava_block = new class_132();
   // $FF: renamed from: h net.minecraft.game.game.projectiles.entities
   public static final Material field_615 = new Material();
   // $FF: renamed from: i net.minecraft.game.game.projectiles.entities
   public static final Material field_616 = new class_131();
   // $FF: renamed from: j net.minecraft.game.game.projectiles.entities
   public static final Material field_617 = new Material();
   // $FF: renamed from: k net.minecraft.game.game.projectiles.entities
   public static final Material field_618 = new Material();
   // $FF: renamed from: l net.minecraft.game.game.projectiles.entities
   public static final Material field_619 = new class_133();
   // $FF: renamed from: m net.minecraft.game.game.projectiles.entities
   public static final Material field_620 = new Material();
   // $FF: renamed from: n net.minecraft.game.game.projectiles.entities
   public static final Material field_621 = new class_131();
   // $FF: renamed from: o net.minecraft.game.game.projectiles.entities
   public static final Material field_622 = new Material();
   // $FF: renamed from: p net.minecraft.game.game.projectiles.entities
   public static final Material field_623 = new Material();

   // $FF: renamed from: projectiles () boolean
   public boolean method_343() {
      return false;
   }

   // $FF: renamed from: player () boolean
   public final boolean method_344() {
      return !this.method_343() && !this.method_345();
   }

   // $FF: renamed from: game () boolean
   public boolean method_345() {
      return true;
   }

   // $FF: renamed from: animals () boolean
   public boolean method_346() {
      return true;
   }

   // $FF: renamed from: entities () boolean
   public boolean method_347() {
      return true;
   }
}
