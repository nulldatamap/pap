package net.minecraft.game.world.a;

import net.minecraft.game.entities.Entity;
import net.minecraft.game.util.Vector;

// $FF: renamed from: net.minecraft.game.game.game.animals
public final class Path {
   // $FF: renamed from: game net.minecraft.game.game.game.game[]
   private final Coordinate[] waypoints;
   // $FF: renamed from: animals int
   private int index;

   public Path(Coordinate[] var1) {
      this.waypoints = var1;
   }

   // $FF: renamed from: game () void
   public final void next_waypoint() {
      ++this.index;
   }

   // $FF: renamed from: animals () boolean
   public final boolean destination_reached() {
      return this.index >= this.waypoints.length;
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals) net.minecraft.game.projectiles.game
   public final Vector current_waypoint(Entity var1) {
      float var2 = (float)this.waypoints[this.index].x + (float)((int)(var1.width + 1.0F)) * 0.5F;
      float var3 = (float)this.waypoints[this.index].y;
      float var4 = (float)this.waypoints[this.index].z + (float)((int)(var1.width + 1.0F)) * 0.5F;
      return new Vector(var2, var3, var4);
   }
}
