package net.minecraft.game.world.a;

// $FF: renamed from: net.minecraft.game.game.game.projectiles
public final class class_81 {
   // $FF: renamed from: game net.minecraft.game.game.game.game[]
   private Coordinate[] field_381 = new Coordinate[1024];
   // $FF: renamed from: animals int
   private int field_382 = 0;

   // $FF: renamed from: game (net.minecraft.game.game.game.game) net.minecraft.game.game.game.game
   public final Coordinate method_249(Coordinate var1) {
      if (var1.field_365 >= 0) {
         throw new IllegalStateException("OW KNOWS!");
      } else {
         if (this.field_382 == this.field_381.length) {
            Coordinate[] var2 = new Coordinate[this.field_382 << 1];
            System.arraycopy(this.field_381, 0, var2, 0, this.field_382);
            this.field_381 = var2;
         }

         this.field_381[this.field_382] = var1;
         var1.field_365 = this.field_382;
         this.method_253(this.field_382++);
         return var1;
      }
   }

   // $FF: renamed from: game () void
   public final void method_250() {
      this.field_382 = 0;
   }

   // $FF: renamed from: animals () net.minecraft.game.game.game.game
   public final Coordinate method_251() {
      Coordinate var1 = this.field_381[0];
      this.field_381[0] = this.field_381[--this.field_382];
      this.field_381[this.field_382] = null;
      if (this.field_382 > 0) {
         this.method_254(0);
      }

      var1.field_365 = -1;
      return var1;
   }

   // $FF: renamed from: game (net.minecraft.game.game.game.game, float) void
   public final void method_252(Coordinate var1, float var2) {
      float var3 = var1.field_368;
      var1.field_368 = var2;
      if (var2 < var3) {
         this.method_253(var1.field_365);
      } else {
         this.method_254(var1.field_365);
      }
   }

   // $FF: renamed from: game (int) void
   private void method_253(int var1) {
      Coordinate var2;
      int var4;
      for(float var3 = (var2 = this.field_381[var1]).field_368; var1 > 0; var1 = var4) {
         var4 = var1 - 1 >> 1;
         Coordinate var5 = this.field_381[var4];
         if (var3 >= var5.field_368) {
            break;
         }

         this.field_381[var1] = var5;
         var5.field_365 = var1;
      }

      this.field_381[var1] = var2;
      var2.field_365 = var1;
   }

   // $FF: renamed from: animals (int) void
   private void method_254(int var1) {
      Coordinate var2;
      float var3 = (var2 = this.field_381[var1]).field_368;

      while(true) {
         int var4;
         int var5 = (var4 = 1 + (var1 << 1)) + 1;
         if (var4 >= this.field_382) {
            break;
         }

         Coordinate var6;
         float var7 = (var6 = this.field_381[var4]).field_368;
         Coordinate var8;
         float var9;
         if (var5 >= this.field_382) {
            var8 = null;
            var9 = Float.POSITIVE_INFINITY;
         } else {
            var9 = (var8 = this.field_381[var5]).field_368;
         }

         if (var7 < var9) {
            if (var7 >= var3) {
               break;
            }

            this.field_381[var1] = var6;
            var6.field_365 = var1;
            var1 = var4;
         } else {
            if (var9 >= var3) {
               break;
            }

            this.field_381[var1] = var8;
            var8.field_365 = var1;
            var1 = var5;
         }
      }

      this.field_381[var1] = var2;
      var2.field_365 = var1;
   }

   // $FF: renamed from: entities () boolean
   public final boolean method_255() {
      return this.field_382 == 0;
   }
}
