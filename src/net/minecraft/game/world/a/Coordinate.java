package net.minecraft.game.world.a;

import a.MathHelper;

// $FF: renamed from: net.minecraft.game.game.game.game
public final class Coordinate {
   // $FF: renamed from: game int
   public final int x;
   // $FF: renamed from: animals int
   public final int y;
   // $FF: renamed from: entities int
   public final int z;
   // $FF: renamed from: projectiles int
   public final int index;
   // $FF: renamed from: player int
   int field_365 = -1;
   // $FF: renamed from: particles float
   float field_366;
   // $FF: renamed from: player float
   float field_367;
   // $FF: renamed from: h float
   float field_368;
   // $FF: renamed from: i net.minecraft.game.game.game.game
   Coordinate field_369;
   // $FF: renamed from: j boolean
   public boolean field_370 = false;

   public Coordinate(int var1, int var2, int var3) {
      this.x = var1;
      this.y = var2;
      this.z = var3;
      this.index = var1 | var2 << 10 | var3 << 20;
   }

   // $FF: renamed from: game (net.minecraft.game.game.game.game) float
   public final float distance(Coordinate var1) {
      float var2 = (float)(var1.x - this.x);
      float var3 = (float)(var1.y - this.y);
      float var4 = (float)(var1.z - this.z);
      return MathHelper.sqrt(var2 * var2 + var3 * var3 + var4 * var4);
   }

   public final boolean equals(Object var1) {
      return ((Coordinate)var1).index == this.index;
   }

   public final int hashCode() {
      return this.index;
   }

   // $FF: renamed from: game () boolean
   public final boolean method_230() {
      return this.field_365 >= 0;
   }

   public final String toString() {
      return this.x + ", " + this.y + ", " + this.z;
   }
}
