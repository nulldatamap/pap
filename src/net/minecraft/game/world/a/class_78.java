package net.minecraft.game.world.a;

import java.util.HashMap;
import java.util.Map;
import net.minecraft.game.world.World;
import net.minecraft.game.world.material.Material;
import net.minecraft.game.entities.Entity;

// $FF: renamed from: net.minecraft.game.game.game.entities
public final class class_78 {
   // $FF: renamed from: game net.minecraft.game.game.player
   private World world;
   // $FF: renamed from: animals net.minecraft.game.game.game.projectiles
   private class_81 field_372 = new class_81();
   // $FF: renamed from: entities java.util.Map
   private Map field_373 = new HashMap();
   // $FF: renamed from: projectiles net.minecraft.game.game.game.game[]
   private Coordinate[] field_374 = new Coordinate[32];

   public class_78(World var1) {
      this.world = var1;
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, net.minecraft.game.entities.animals, float) net.minecraft.game.game.game.animals
   public final Path find_path(Entity var1, Entity var2, float var3) {
      return this.method_233(var1, var2.pos_x, var2.hitbox.min_y, var2.pos_z, 16.0F);
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, int, int, int, float) net.minecraft.game.game.game.animals
   public final Path method_232(Entity var1, int var2, int var3, int var4, float var5) {
      return this.method_233(var1, (float)var2 + 0.5F, (float)var3 + 0.5F, (float)var4 + 0.5F, 16.0F);
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, float, float, float, float) net.minecraft.game.game.game.animals
   private Path method_233(Entity var1, float var2, float var3, float var4, float var5) {
      this.field_372.method_250();
      this.field_373.clear();
      Coordinate var6 = this.method_235((int)var1.hitbox.min_x, (int)var1.hitbox.min_y, (int)var1.hitbox.min_z);
      Coordinate var21 = this.method_235((int)(var2 - var1.width / 2.0F), (int)var3, (int)(var4 - var1.width / 2.0F));
      Coordinate var23 = new Coordinate((int)(var1.width + 1.0F), (int)(var1.height + 1.0F), (int)(var1.width + 1.0F));
      float var26 = var5;
      Coordinate var25 = var23;
      Coordinate var24 = var21;
      Entity var22 = var1;
      class_78 var20 = this;
      var6.field_366 = 0.0F;
      var6.field_367 = var6.distance(var21);
      var6.field_368 = var6.field_367;
      this.field_372.method_250();
      this.field_372.method_249(var6);
      Coordinate var7 = var6;

      Path var10000;
      while(true) {
         if (var20.field_372.method_255()) {
            var10000 = var7 == var6 ? null : method_237(var7);
            break;
         }

         Coordinate var8;
         if ((var8 = var20.field_372.method_251()).index == var24.index) {
            var10000 = method_237(var24);
            break;
         }

         if (var8.distance(var24) < var7.distance(var24)) {
            var7 = var8;
         }

         var8.field_370 = true;
         int var15 = 0;
         byte var16 = 0;
         if (var20.method_236(var8.x, var8.y + 1, var8.z, var25) > 0) {
            var16 = 1;
         }

         Coordinate var17 = var20.method_234(var22, var8.x, var8.y, var8.z + 1, var25, var16);
         Coordinate var18 = var20.method_234(var22, var8.x - 1, var8.y, var8.z, var25, var16);
         Coordinate var19 = var20.method_234(var22, var8.x + 1, var8.y, var8.z, var25, var16);
         Coordinate var10 = var20.method_234(var22, var8.x, var8.y, var8.z - 1, var25, var16);
         if (var17 != null && !var17.field_370 && var17.distance(var24) < var26) {
            ++var15;
            var20.field_374[0] = var17;
         }

         if (var18 != null && !var18.field_370 && var18.distance(var24) < var26) {
            var20.field_374[var15++] = var18;
         }

         if (var19 != null && !var19.field_370 && var19.distance(var24) < var26) {
            var20.field_374[var15++] = var19;
         }

         if (var10 != null && !var10.field_370 && var10.distance(var24) < var26) {
            var20.field_374[var15++] = var10;
         }

         int var9 = var15;

         for(int var27 = 0; var27 < var9; ++var27) {
            Coordinate var11 = var20.field_374[var27];
            float var12 = var8.field_366 + var8.distance(var11);
            if (!var11.method_230() || var12 < var11.field_366) {
               var11.field_369 = var8;
               var11.field_366 = var12;
               var11.field_367 = var11.distance(var24);
               if (var11.method_230()) {
                  var20.field_372.method_252(var11, var11.field_366 + var11.field_367);
               } else {
                  var11.field_368 = var11.field_366 + var11.field_367;
                  var20.field_372.method_249(var11);
               }
            }
         }
      }

      return var10000;
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, int, int, int, net.minecraft.game.game.game.game, int) net.minecraft.game.game.game.game
   private Coordinate method_234(Entity var1, int var2, int var3, int var4, Coordinate var5, int var6) {
      Coordinate var8 = null;
      if (this.method_236(var2, var3, var4, var5) > 0) {
         var8 = this.method_235(var2, var3, var4);
      }

      if (var8 == null && this.method_236(var2, var3 + var6, var4, var5) > 0) {
         var8 = this.method_235(var2, var3 + var6, var4);
      }

      if (var8 != null) {
         var6 = 0;

         while(true) {
            int var7;
            if (var3 <= 0 || (var7 = this.method_236(var2, var3 - 1, var4, var5)) <= 0) {
               Material var9;
               if ((var9 = this.world.method_198(var2, var3 - 1, var4)) == Material.water_block || var9 == Material.lava_block) {
                  return null;
               }
               break;
            }

            if (var7 < 0) {
               return null;
            }

            ++var6;
            if (var6 >= 4) {
               return null;
            }

            --var3;
            var8 = this.method_235(var2, var3, var4);
         }
      }

      return var8;
   }

   // $FF: renamed from: game (int, int, int) net.minecraft.game.game.game.game
   private final Coordinate method_235(int var1, int var2, int var3) {
      int var4 = var1 | var2 << 10 | var3 << 20;
      Coordinate var5;
      if ((var5 = (Coordinate)this.field_373.get(var4)) == null) {
         var5 = new Coordinate(var1, var2, var3);
         this.field_373.put(var4, var5);
      }

      return var5;
   }

   // $FF: renamed from: game (int, int, int, net.minecraft.game.game.game.game) int
   private int method_236(int var1, int var2, int var3, Coordinate var4) {
      for(int var5 = var1; var5 < var1 + var4.x; ++var5) {
         if (var5 < 0 || var5 >= this.world.width) {
            return 0;
         }

         for(int var6 = var2; var6 < var2 + var4.y; ++var6) {
            if (var6 < 0 || var6 >= this.world.height) {
               return 0;
            }

            int var7 = var3;

            while(var7 < var3 + var4.z) {
               if (var7 >= 0 && var7 < this.world.depth) {
                  Material var8;
                  if ((var8 = this.world.method_198(var1, var2, var3)).method_347()) {
                     return 0;
                  }

                  if (var8 != Material.water_block && var8 != Material.lava_block) {
                     ++var7;
                     continue;
                  }

                  return -1;
               }

               return 0;
            }
         }
      }

      return 1;
   }

   // $FF: renamed from: game (net.minecraft.game.game.game.game) net.minecraft.game.game.game.animals
   private static Path method_237(Coordinate var0) {
      int var1 = 1;

      Coordinate var2;
      for(var2 = var0; var2.field_369 != null; var2 = var2.field_369) {
         ++var1;
      }

      Coordinate[] var3 = new Coordinate[var1];
      var2 = var0;
      --var1;

      for(var3[var1] = var0; var2.field_369 != null; var3[var1] = var2) {
         var2 = var2.field_369;
         --var1;
      }

      return new Path(var3);
   }
}
