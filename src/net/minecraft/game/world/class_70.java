package net.minecraft.game.world;

// $FF: renamed from: net.minecraft.game.game.entities
final class class_70 {
   // $FF: renamed from: game int
   public int field_298;
   // $FF: renamed from: animals int
   public int field_299;
   // $FF: renamed from: entities int
   public int field_300;
   // $FF: renamed from: projectiles int
   public int field_301;
   // $FF: renamed from: player int
   public int field_302;
   // $FF: renamed from: particles int
   public int field_303;

   public class_70(MaybeChunk var1, int var2, int var3, int var4, int var5, int var6, int var7) {
      this.field_298 = var2;
      this.field_299 = var3;
      this.field_300 = var4;
      this.field_301 = var5;
      this.field_302 = var6;
      this.field_303 = var7;
   }
}
