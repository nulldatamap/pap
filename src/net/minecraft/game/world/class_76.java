package net.minecraft.game.world;

// $FF: renamed from: net.minecraft.game.game.player
public final class class_76 {
   // $FF: renamed from: game int
   public int field_354;
   // $FF: renamed from: animals int
   public int field_355;
   // $FF: renamed from: entities int
   public int field_356;
   // $FF: renamed from: projectiles int
   public int field_357;
   // $FF: renamed from: player int
   public int field_358;

   public class_76(int var1, int var2, int var3, int var4) {
      this.field_354 = var1;
      this.field_355 = var2;
      this.field_356 = var3;
      this.field_357 = var4;
   }
}
