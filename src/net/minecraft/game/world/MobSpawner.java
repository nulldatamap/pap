package net.minecraft.game.world;

import net.minecraft.game.entities.Mob;
import net.minecraft.game.entities.Entity;
import net.minecraft.game.entities.monsters.Monster;
import net.minecraft.game.entities.monsters.Zombie;
import net.minecraft.game.entities.monsters.Creeper;
import net.minecraft.game.entities.monsters.Spider;
import net.minecraft.game.entities.monsters.Giant;
import net.minecraft.game.entities.monsters.Skeleton;
import net.minecraft.game.entities.animals.Pig;
import net.minecraft.game.entities.animals.Sheep;

// $FF: renamed from: net.minecraft.game.game.animals
public class MobSpawner {
   // $FF: renamed from: game net.minecraft.game.game.player
   private World world;

   public MobSpawner(World var1) {
      this.world = var1;
   }

   // $FF: renamed from: game () void
   public final void spawn_mobs() {
      int var1 = this.world.width * this.world.depth * this.world.height * 20 / 64 / 64 / 64 / 2;
      if (this.world.difficulty == 0) {
         var1 = 0 / 4;
      }

      if (this.world.difficulty == 1) {
         var1 = var1 * 3 / 4;
      }

      if (this.world.difficulty == 2) {
         var1 = (var1 << 2) / 4;
      }

      if (this.world.difficulty == 3) {
         var1 = var1 * 6 / 4;
      }

      int var2 = this.world.width * this.world.depth / 4000;
      int var3 = this.world.method_180(Monster.class); // <<player.class>>

      int var4;
      Mob var5;
      int var6;
      int var7;
      int var8;
      int var9;
      int var10;
      int var11;
      int var12;
      int var13;
      int var14;
      int var15;
      float var16;
      float var17;
      float var18;
      float var19;
      float var20;
      float var21;
      MobSpawner var22;
      Object var23;
      for(var4 = 0; var4 < 4; ++var4) {
         if (var3 < var1) {
            var5 = this.world.player;
            var22 = this;
            var6 = 0;
            var7 = this.world.random_a.nextInt(5);
            var8 = this.world.random_a.nextInt(this.world.width);
            var9 = (int)(Math.min(this.world.random_a.nextFloat(), this.world.random_a.nextFloat()) * (float)this.world.height);
            var10 = this.world.random_a.nextInt(this.world.depth);

            for(var11 = 0; var11 < 2; ++var11) {
               var12 = var8;
               var13 = var9;
               var14 = var10;

               for(var15 = 0; var15 < 3; ++var15) {
                  var12 += var22.world.random_a.nextInt(6) - var22.world.random_a.nextInt(6);
                  var13 += var22.world.random_a.nextInt(1) - var22.world.random_a.nextInt(1);
                  var14 += var22.world.random_a.nextInt(6) - var22.world.random_a.nextInt(6);
                  if (var12 >= 0 && var14 > 0 && var13 >= 0 && var13 < var22.world.height - 2 && var12 < var22.world.width && var14 < var22.world.depth) {
                     var16 = (float)var12 + 0.5F;
                     var17 = (float)var13 + 0.5F;
                     var18 = (float)var14 + 0.5F;
                     if (var5 != null) {
                        var19 = var16 - var5.pos_x;
                        var20 = var17 - var5.pos_y;
                        var21 = var18 - var5.pos_z;
                        if (var19 * var19 + var20 * var20 + var21 * var21 < 1024.0F) {
                           continue;
                        }
                     } else {
                        var19 = var16 - (float)var22.world.spawn_x;
                        var20 = var17 - (float)var22.world.spawn_y;
                        var21 = var18 - (float)var22.world.spawn_z;
                        if (var19 * var19 + var20 * var20 + var21 * var21 < 1024.0F) {
                           continue;
                        }
                     }

                     var23 = null;
                     if (var7 == 0) {
                        var23 = new Skeleton(var22.world);
                     }

                     if (var7 == 1) {
                        var23 = new Creeper(var22.world);
                     }

                     if (var7 == 2) {
                        var23 = new Spider(var22.world);
                     }

                     if (var7 == 3) {
                        var23 = new Zombie(var22.world);
                     }

                     if (var23 instanceof Monster && var22.world.difficulty == 0) {
                        var23 = null;
                     }

                     if (var23 != null && !var22.world.method_170(var12, var13, var14) && var22.world.method_170(var12, var13 - 1, var14) && ((Mob)var23).teleport_and_check(var16, var17, var18)) {
                        var21 = var22.world.random_a.nextFloat() * 360.0F;
                        ((Mob)var23).set_orientation(var16, var17, var18, var21, 0.0F);
                        ++var6;
                        var22.world.add_entity((Entity)var23);
                     }
                  }
               }
            }

            var3 += var6;
         }
      }

      var4 = this.world.method_180(Giant.class); // <<game.class>>

      for(var1 = 0; var1 < 4; ++var1) {
         if (var4 < var2) {
            var5 = this.world.player;
            var22 = this;
            var6 = 0;
            var7 = this.world.random_a.nextInt(2);
            var8 = this.world.random_a.nextInt(this.world.width);
            var9 = this.world.random_a.nextInt(this.world.height);
            var10 = this.world.random_a.nextInt(this.world.depth);

            for(var11 = 0; var11 < 2; ++var11) {
               var12 = var8;
               var13 = var9;
               var14 = var10;

               for(var15 = 0; var15 < 3; ++var15) {
                  var12 += var22.world.random_a.nextInt(6) - var22.world.random_a.nextInt(6);
                  var13 += var22.world.random_a.nextInt(1) - var22.world.random_a.nextInt(1);
                  var14 += var22.world.random_a.nextInt(6) - var22.world.random_a.nextInt(6);
                  if (var12 >= 0 && var14 > 0 && var13 >= 0 && var13 < var22.world.height - 2 && var12 < var22.world.width && var14 < var22.world.depth) {
                     var16 = (float)var12 + 0.5F;
                     var17 = (float)var13 + 0.5F;
                     var18 = (float)var14 + 0.5F;
                     if (var5 != null) {
                        var19 = var16 - var5.pos_x;
                        var20 = var17 - var5.pos_y;
                        var21 = var18 - var5.pos_z;
                        if (var19 * var19 + var20 * var20 + var21 * var21 < 1024.0F) {
                           continue;
                        }
                     } else {
                        var19 = var16 - (float)var22.world.spawn_x;
                        var20 = var17 - (float)var22.world.spawn_y;
                        var21 = var18 - (float)var22.world.spawn_z;
                        if (var19 * var19 + var20 * var20 + var21 * var21 < 1024.0F) {
                           continue;
                        }
                     }

                     var23 = null;
                     if (var7 == 0) {
                        var23 = new Pig(var22.world);
                     }

                     if (var7 == 1) {
                        var23 = new Sheep(var22.world);
                     }

                     if (var23 != null && !var22.world.method_170(var12, var13, var14) && var22.world.method_170(var12, var13 - 1, var14) && ((Mob)var23).teleport_and_check(var16, var17, var18)) {
                        var21 = var22.world.random_a.nextFloat() * 360.0F;
                        ((Mob)var23).set_orientation(var16, var17, var18, var21, 0.0F);
                        ++var6;
                        var22.world.add_entity((Entity)var23);
                     }
                  }
               }
            }

            var4 += var6;
         }
      }

   }
}
