package net.minecraft.game.world;

import a.MathHelper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeSet;

import net.minecraft.game.entities.misc.FallingBlock;
import net.minecraft.game.world.a.class_78;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.world.block.class_185;
import net.minecraft.game.world.block.a.DataBlock;
import net.minecraft.game.world.material.Material;
import net.minecraft.game.entities.Mob;
import net.minecraft.game.entities.Entity;
import net.minecraft.game.util.RaycastResult;
import net.minecraft.game.util.AABB;
import net.minecraft.game.util.Vector;

// $FF: renamed from: net.minecraft.game.game.player
public final class World {
   // $FF: renamed from: F float[]
   private static float[] maybe_light_levels = new float[16];
   // $FF: renamed from: game int
   public int width;
   // $FF: renamed from: animals int
   public int depth;
   // $FF: renamed from: entities int
   public int height;
   // $FF: renamed from: projectiles byte[]
   public byte[] blocks;
   // $FF: renamed from: player byte[]
   public byte[] data;
   // $FF: renamed from: particles java.lang.String
   public String world_name;
   // $FF: renamed from: player java.lang.String
   public String world_author;
   // $FF: renamed from: h long
   public long time_stamp;
   // $FF: renamed from: i int
   public int spawn_x;
   // $FF: renamed from: j int
   public int spawn_y;
   // $FF: renamed from: k int
   public int spawn_z;
   // $FF: renamed from: l float
   public float spawn_yaw;
   // $FF: renamed from: m int
   public int surrounding_water_type;
   // $FF: renamed from: n java.util.List
   List renderers;
   // $FF: renamed from: G java.util.List
   private List field_322;
   // $FF: renamed from: o java.util.Map
   public Map data_block_map;
   // $FF: renamed from: H java.util.List
   private List data_blocks;
   // $FF: renamed from: p int[]
   int[] height_map;
   // $FF: renamed from: q java.util.Random
   public Random random_a;
   // $FF: renamed from: I java.util.Random
   private Random random_b;
   // $FF: renamed from: J int
   private int field_328;
   // $FF: renamed from: r net.minecraft.game.game.i
   public HitboxTree hitbox_tree;
   // $FF: renamed from: s int
   public int surrounding_water_height;
   // $FF: renamed from: t int
   public int surrounding_ground_height;
   // $FF: renamed from: u int
   public int cloud_height;
   // $FF: renamed from: v int
   public int sky_color;
   // $FF: renamed from: w int
   public int fog_color;
   // $FF: renamed from: x int
   public int cloud_color;
   // $FF: renamed from: K int
   private int block_count;
   // $FF: renamed from: L int
   private int maybe_tick;
   // $FF: renamed from: y net.minecraft.game.entities.player
   public Mob player;
   // $FF: renamed from: z boolean
   public boolean can_take_damage;
   // $FF: renamed from: A int
   public int sky_brightness;
   // $FF: renamed from: B int
   public int maybe_brightness;
   // $FF: renamed from: C net.minecraft.game.game.game.entities
   public class_78 field_342;
   // $FF: renamed from: M net.minecraft.game.game.h
   private MaybeChunk maybe_chunk;
   // $FF: renamed from: D int
   public int time_of_day;
   // $FF: renamed from: N short
   private static short field_345;
   // $FF: renamed from: O short[]
   private short[] field_346;
   // $FF: renamed from: P int[]
   private int[] field_347;
   // $FF: renamed from: Q int[]
   private int[] field_348;
   // $FF: renamed from: E int
   public int difficulty;

   public World() {
      this.surrounding_water_type = Block.water.block_id;
      this.renderers = new ArrayList();
      this.field_322 = new LinkedList();
      this.data_block_map = new HashMap();
      this.data_blocks = new ArrayList();
      this.random_a = new Random();
      this.random_b = new Random();
      this.field_328 = this.random_a.nextInt();
      this.block_count = 0;
      this.maybe_tick = 0;
      this.can_take_damage = true;
      this.sky_brightness = 15;
      this.maybe_brightness = 15;
      this.field_342 = new class_78(this);
      this.time_of_day = 0;
      this.field_346 = new short[0x100000];
      this.field_347 = new int[0x100000];
      this.field_348 = new int[0x100000];
      this.difficulty = 2;
      this.sky_color = 0x99CCFF;
      this.fog_color = 0xFFFFFF;
      this.cloud_color = 0xFFFFFF;
   }

   // $FF: renamed from: game () void
   public final void method_157() {
      if (this.blocks == null) {
         throw new RuntimeException("The world is corrupt!");
      } else {
         this.renderers = new ArrayList();
         this.height_map = new int[this.width * this.depth];
         Arrays.fill(this.height_map, this.height);
         this.maybe_chunk.method_146(0, 0, this.width, this.depth);
         this.random_a = new Random();
         this.field_328 = this.random_a.nextInt();
         this.field_322 = new ArrayList();
         if (this.hitbox_tree == null) {
            this.hitbox_tree = new HitboxTree(this.width, this.height, this.depth);
         }

      }
   }

   // $FF: renamed from: game (int, int, int, byte[], byte[]) void
   public final void method_158(int var1, int var2, int var3, byte[] var4, byte[] var5) {
      if (var5 != null && var5.length == 0) {
         var5 = null;
      }

      this.width = var1;
      this.depth = var3;
      this.height = var2;
      this.blocks = var4;

      int var6;
      int var7;
      for(var2 = 0; var2 < this.width; ++var2) {
         for(var6 = 0; var6 < this.depth; ++var6) {
            for(var7 = 0; var7 < this.height; ++var7) {
               int var8 = 0;
               if (var7 <= 1 && var7 < this.surrounding_ground_height - 1 && var4[((var7 + 1) * this.depth + var6) * this.width + var2] == 0) {
                  var8 = Block.infinite_lava.block_id;
               } else if (var7 < this.surrounding_ground_height - 1) {
                  var8 = Block.bedrock.block_id;
               } else if (var7 < this.surrounding_ground_height) {
                  if (this.surrounding_ground_height > this.surrounding_water_height && this.surrounding_water_type == Block.water.block_id) {
                     var8 = Block.grass.block_id;
                  } else {
                     var8 = Block.dirt.block_id;
                  }
               } else if (var7 < this.surrounding_water_height) {
                  var8 = this.surrounding_water_type;
               }

               var4[(var7 * this.depth + var6) * this.width + var2] = (byte)var8;
               if (var7 == 1 && var2 != 0 && var6 != 0 && var2 != this.width - 1 && var6 != this.depth - 1) {
                  var7 = this.height - 2;
               }
            }
         }
      }

      this.height_map = new int[var1 * var3];
      Arrays.fill(this.height_map, this.height);
      if (var5 == null) {
         this.data = new byte[var4.length];
         this.maybe_chunk = new MaybeChunk(this);
         boolean var10 = true;
         World var11 = this;
         var2 = this.maybe_brightness;

         for(var3 = 0; var3 < var11.width; ++var3) {
            for(int var12 = 0; var12 < var11.depth; ++var12) {
               int var13;
               for(var13 = var11.height - 1; var13 > 0 && Block.field_864[var11.block_at(var3, var13, var12)] == 0; --var13) {
                  ;
               }

               var11.height_map[var3 + var12 * var11.width] = var13 + 1;

               for(var13 = 0; var13 < var11.height; ++var13) {
                  var6 = (var13 * var11.depth + var12) * var11.width + var3;
                  var7 = var11.height_map[var3 + var12 * var11.width];
                  var7 = var13 >= var7 ? var2 : 0;
                  byte var14 = var11.blocks[var6];
                  if (var7 < Block.field_867[var14]) {
                     var7 = Block.field_867[var14];
                  }

                  var11.data[var6] = (byte)((var11.data[var6] & 240) + var7);
               }
            }
         }

         var11.maybe_chunk.method_149(0, 0, 0, var11.width, var11.height, var11.depth);
      } else {
         this.data = var5;
         this.maybe_chunk = new MaybeChunk(this);
      }

      for(var2 = 0; var2 < this.renderers.size(); ++var2) {
         ((IRenderer)this.renderers.get(var2)).update_render_chunks();
      }

      this.field_322.clear();
      this.method_159();
      this.method_157();
      System.gc();
   }

   // $FF: renamed from: animals () void
   public final void method_159() {
      Random var1 = new Random();
      int var2 = 0;

      while(true) {
         label58:
         while(true) {
            int var3;
            int var4;
            int var5;
            do {
               do {
                  ++var2;
                  var3 = var1.nextInt(this.width / 2) + this.width / 4;
                  var4 = var1.nextInt(this.depth / 2) + this.depth / 4;
                  var5 = this.method_192(var3, var4) + 1;
                  if (var2 == 1000000) {
                     this.spawn_x = var3;
                     this.spawn_y = this.height + 100;
                     this.spawn_z = var4;
                     this.spawn_yaw = 180.0F;
                     return;
                  }
               } while(var5 < 4);
            } while(var5 <= this.surrounding_water_height);

            int var6;
            int var7;
            int var8;
            for(var6 = var3 - 3; var6 <= var3 + 3; ++var6) {
               for(var7 = var5 - 1; var7 <= var5 + 2; ++var7) {
                  for(var8 = var4 - 3 - 2; var8 <= var4 + 3; ++var8) {
                     if (this.method_198(var6, var7, var8).method_345()) {
                        continue label58;
                     }
                  }
               }
            }

            var6 = var5 - 2;

            for(var7 = var3 - 3; var7 <= var3 + 3; ++var7) {
               for(var8 = var4 - 3 - 2; var8 <= var4 + 3; ++var8) {
                  if (!Block.transparency[this.block_at(var7, var6, var8)]) {
                     continue label58;
                  }
               }
            }

            this.spawn_x = var3;
            this.spawn_y = var5;
            this.spawn_z = var4;
            this.spawn_yaw = 180.0F;
            return;
         }
      }
   }

   // $FF: renamed from: game (net.minecraft.game.game.projectiles) void
   public final void add_renderer(IRenderer var1) {
      for(int var2 = 0; var2 < this.hitbox_tree.entities.size(); ++var2) {
         var1.method_9((Entity)this.hitbox_tree.entities.get(var2));
      }

      this.renderers.add(var1);
   }

   public final void finalize() {
   }

   // $FF: renamed from: animals (net.minecraft.game.game.projectiles) void
   public final void remove_renderer(IRenderer var1) {
      this.renderers.remove(var1);
   }

   // $FF: renamed from: game (net.minecraft.game.projectiles.animals) java.util.ArrayList
   public final ArrayList get_colliding_aabbs(AABB var1) {
      ArrayList var2 = new ArrayList();
      int var3 = (int)var1.min_x;
      int var4 = (int)var1.max_x + 1;
      int var5 = (int)var1.min_y;
      int var6 = (int)var1.max_y + 1;
      int var7 = (int)var1.min_z;
      int var8 = (int)var1.max_z + 1;
      if (var1.min_x < 0.0F) {
         --var3;
      }

      if (var1.min_y < 0.0F) {
         --var5;
      }

      if (var1.min_z < 0.0F) {
         --var7;
      }

      for(var3 = var3; var3 < var4; ++var3) {
         for(int var9 = var5; var9 < var6; ++var9) {
            for(int var10 = var7; var10 < var8; ++var10) {
               Block var11;
               AABB var12;
               if ((var11 = Block.blocks[this.block_at(var3, var9, var10)]) != null) {
                  if ((var12 = var11.method_502(var3, var9, var10)) != null && var1.method_472(var12)) {
                     var2.add(var12);
                  }
               } else if (this.surrounding_ground_height < 0 && (var9 < this.surrounding_ground_height || var9 < this.surrounding_water_height) && (var12 = Block.bedrock.method_502(var3, var9, var10)) != null && var1.method_472(var12)) {
                  var2.add(var12);
               }
            }
         }
      }

      return var2;
   }

   // $FF: renamed from: game (int, int, int, int, int, int) void
   public final void method_163(int var1, int var2, int var3, int var4, int var5, int var6) {
      int var7 = this.block_at(var1, var2, var3);
      int var8 = this.block_at(var4, var5, var6);
      this.method_164(var1, var2, var3, var8);
      this.method_164(var4, var5, var6, var7);
      this.update_blocks(var1, var2, var3, var8);
      this.update_blocks(var4, var5, var6, var7);
   }

   // $FF: renamed from: game (int, int, int, int) boolean
   public final boolean method_164(int var1, int var2, int var3, int var4) {
      if (var1 > 0 && var2 > 0 && var3 > 0 && var1 < this.width - 1 && var2 < this.height - 1 && var3 < this.depth - 1) {
         if (var4 == this.blocks[(var2 * this.depth + var3) * this.width + var1]) {
            return false;
         } else {
            if (var4 == 0 && (var1 == 0 || var3 == 0 || var1 == this.width - 1 || var3 == this.depth - 1) && var2 >= this.surrounding_ground_height && var2 < this.surrounding_water_height) {
               var4 = Block.water.block_id;
            }

            byte var5 = this.blocks[(var2 * this.depth + var3) * this.width + var1];
            this.blocks[(var2 * this.depth + var3) * this.width + var1] = (byte)var4;
            this.method_197(var1, var2, var3, 0);
            if (var5 != 0) {
               Block.blocks[var5].method_511(this, var1, var2, var3);
            }

            if (var4 != 0) {
               Block.blocks[var4].method_510(this, var1, var2, var3);
            }

            if (Block.field_864[var5] != Block.field_864[var4] || Block.field_867[var5] != 0 || Block.field_867[var4] != 0) {
               this.maybe_chunk.method_146(var1, var3, 1, 1);
               this.maybe_chunk.method_149(var1, var2, var3, var1 + 1, var2 + 1, var3 + 1);
            }

            for(var4 = 0; var4 < this.renderers.size(); ++var4) {
               ((IRenderer)this.renderers.get(var4)).method_3(var1, var2, var3);
            }

            return true;
         }
      } else {
         return false;
      }
   }

   // $FF: renamed from: animals (int, int, int, int) boolean
   public final boolean method_165(int var1, int var2, int var3, int var4) {
      if (this.method_164(var1, var2, var3, var4)) {
         this.update_blocks(var1, var2, var3, var4);
         return true;
      } else {
         return false;
      }
   }

   // $FF: renamed from: entities (int, int, int, int) void
   public final void update_blocks(int var1, int var2, int var3, int var4) {
      this.method_168(var1 - 1, var2, var3, var4);
      this.method_168(var1 + 1, var2, var3, var4);
      this.method_168(var1, var2 - 1, var3, var4);
      this.method_168(var1, var2 + 1, var3, var4);
      this.method_168(var1, var2, var3 - 1, var4);
      this.method_168(var1, var2, var3 + 1, var4);
   }

   // $FF: renamed from: projectiles (int, int, int, int) boolean
   public final boolean set_block(int var1, int var2, int var3, int var4) {
      if (var1 >= 0 && var2 >= 0 && var3 >= 0 && var1 < this.width && var2 < this.height && var3 < this.depth) {
         if (var4 == this.blocks[(var2 * this.depth + var3) * this.width + var1]) {
            return false;
         } else {
            this.blocks[(var2 * this.depth + var3) * this.width + var1] = (byte)var4;
            this.maybe_chunk.method_149(var1, var2, var3, var1 + 1, var2 + 1, var3 + 1);
            return true;
         }
      } else {
         return false;
      }
   }

   // $FF: renamed from: h (int, int, int, int) void
   private void method_168(int var1, int var2, int var3, int var4) {
      if (var1 >= 0 && var2 >= 0 && var3 >= 0 && var1 < this.width && var2 < this.height && var3 < this.depth) {
         Block var5;
         if ((var5 = Block.blocks[this.blocks[(var2 * this.depth + var3) * this.width + var1]]) != null) {
            var5.method_508(this, var1, var2, var3, var4);
         }

      }
   }

   // $FF: renamed from: game (int, int, int) int
   public final int block_at(int x, int y, int z) {
      if (x < 0) {
         x = 0;
      } else if (x >= this.width) {
         x = this.width - 1;
      }

      if (y < 0) {
         y = 0;
      } else if (y >= this.height) {
         y = this.height - 1;
      }

      if (z < 0) {
         z = 0;
      } else if (z >= this.depth) {
         z = this.depth - 1;
      }

      return this.blocks[(y * this.depth + z) * this.width + x] & 255;
   }

   // $FF: renamed from: animals (int, int, int) boolean
   public final boolean method_170(int var1, int var2, int var3) {
      Block var4;
      return (var4 = Block.blocks[this.block_at(var1, var2, var3)]) == null ? false : var4.method_503();
   }

   // $FF: renamed from: entities () void
   public final void update() {
      this.hitbox_tree.update_entities_and_segregation();

      for(int var1 = 0; var1 < this.data_blocks.size(); ++var1) {
         ((DataBlock)this.data_blocks.get(var1)).method_258();
      }

   }

   // $FF: renamed from: projectiles () void
   public final void method_172() {
      this.maybe_chunk.method_151();
   }

   // $FF: renamed from: game (float) float
   public final float method_173(float var1) {
      var1 = this.method_175(var1);
      if ((var1 = 1.0F - (MathHelper.cos(var1 * 3.1415927F * 2.0F) * 2.0F + 0.75F)) < 0.0F) {
         var1 = 0.0F;
      }

      if (var1 > 1.0F) {
         var1 = 1.0F;
      }

      return var1 * var1 * 0.5F;
   }

   // $FF: renamed from: animals (float) net.minecraft.game.projectiles.game
   public final Vector sky_color(float var1) {
      if ((var1 = MathHelper.cos(this.method_175(var1) * 3.1415927F * 2.0F) * 2.0F + 0.5F) < 0.0F) {
         var1 = 0.0F;
      }

      if (var1 > 1.0F) {
         var1 = 1.0F;
      }

      float var2 = (float)(this.sky_color >> 16 & 255) / 255.0F;
      float var3 = (float)(this.sky_color >> 8 & 255) / 255.0F;
      float var4 = (float)(this.sky_color & 255) / 255.0F;
      var2 *= var1;
      var3 *= var1;
      var4 *= var1;
      return new Vector(var2, var3, var4);
   }

   // $FF: renamed from: entities (float) float
   public final float method_175(float var1) {
      return this.sky_brightness > 15 ? 0.0F : ((float)this.time_of_day + var1) / 24000.0F - 0.15F;
   }

   // $FF: renamed from: projectiles (float) net.minecraft.game.projectiles.game
   public final Vector fog_color(float dt) {
      if ((dt = MathHelper.cos(this.method_175(dt) * 3.1415927F * 2.0F) * 2.0F + 0.5F) < 0.0F) {
         dt = 0.0F;
      }

      if (dt > 1.0F) {
         dt = 1.0F;
      }

      float var2 = (float)(this.fog_color >> 16 & 255) / 255.0F;
      float var3 = (float)(this.fog_color >> 8 & 255) / 255.0F;
      float var4 = (float)(this.fog_color & 255) / 255.0F;
      var2 *= dt * 0.94F + 0.06F;
      var3 *= dt * 0.94F + 0.06F;
      var4 *= dt * 0.91F + 0.09F;
      return new Vector(var2, var3, var4);
   }

   // $FF: renamed from: player (float) net.minecraft.game.projectiles.game
   public final Vector cloud_color(float var1) {
      if ((var1 = MathHelper.cos(this.method_175(var1) * 3.1415927F * 2.0F) * 2.0F + 0.5F) < 0.0F) {
         var1 = 0.0F;
      }

      if (var1 > 1.0F) {
         var1 = 1.0F;
      }

      float var2 = (float)(this.cloud_color >> 16 & 255) / 255.0F;
      float var3 = (float)(this.cloud_color >> 8 & 255) / 255.0F;
      float var4 = (float)(this.cloud_color & 255) / 255.0F;
      var2 *= var1 * 0.9F + 0.1F;
      var3 *= var1 * 0.9F + 0.1F;
      var4 *= var1 * 0.85F + 0.15F;
      return new Vector(var2, var3, var4);
   }

   // $FF: renamed from: player () int
   public final int method_178() {
      float var1;
      if ((var1 = MathHelper.cos(this.method_175(1.0F) * 3.1415927F * 2.0F) * 1.5F + 0.5F) < 0.0F) {
         var1 = 0.0F;
      }

      if (var1 > 1.0F) {
         var1 = 1.0F;
      }

      int var2;
      if ((var2 = (int)(var1 * ((float)(15 * this.sky_brightness) / 15.0F - 4.0F) + 4.0F)) > 15) {
         var2 = 15;
      }

      if (var2 < 4) {
         var2 = 4;
      }

      return var2;
   }

   // $FF: renamed from: particles () void
   public final void method_179() {
      ++this.time_of_day;
      if (this.time_of_day == 24000) {
         this.time_of_day = 0;
      }

      int var1 = this.method_178();
      if (this.maybe_brightness > var1) {
         this.method_221(this.maybe_brightness - 1);
      }

      if (this.maybe_brightness < var1) {
         this.method_221(this.maybe_brightness + 1);
      }

      ++this.maybe_tick;
      var1 = 1;

      int var2;
      for(var2 = 1; 1 << var1 < this.width; ++var1) {
         ;
      }

      while(1 << var2 < this.depth) {
         ++var2;
      }

      int var3 = this.depth - 1;
      int var4 = this.width - 1;
      int var5 = this.height - 1;
      int var6;
      if ((var6 = this.field_322.size()) > 200) {
         var6 = 200;
      }

      int var7;
      int var10;
      for(var7 = 0; var7 < var6; ++var7) {
         class_76 var8;
         if ((var8 = (class_76)this.field_322.remove(0)).field_358 > 0) {
            --var8.field_358;
            this.field_322.add(var8);
         } else {
            int var12 = var8.field_356;
            int var11 = var8.field_355;
            var10 = var8.field_354;
            byte var9;
            if (var10 >= 0 && var11 >= 0 && var12 >= 0 && var10 < this.width && var11 < this.height && var12 < this.depth && (var9 = this.blocks[(var8.field_355 * this.depth + var8.field_356) * this.width + var8.field_354]) == var8.field_357 && var9 > 0) {
               Block.blocks[var9].tick(this, var8.field_354, var8.field_355, var8.field_356, this.random_a);
            }
         }
      }

      this.block_count += this.width * this.depth * this.height;
      var6 = this.block_count / 200;
      this.block_count -= var6 * 200;

      for(var7 = 0; var7 < var6; ++var7) {
         this.field_328 = this.field_328 * 3 + 1013904223;
         int var13;
         int var14 = (var13 = this.field_328 >> 2) & var4;
         var10 = var13 >> var1 & var3;
         var13 = var13 >> var1 + var2 & var5;
         byte var15 = this.blocks[(var13 * this.depth + var10) * this.width + var14];
         if (Block.field_862[var15]) {
            Block.blocks[var15].tick(this, var14, var13, var10, this.random_a);
         }
      }

   }

   // $FF: renamed from: game (java.lang.Class) int
   public final int method_180(Class var1) {
      int var2 = 0;

      for(int var3 = 0; var3 < this.hitbox_tree.entities.size(); ++var3) {
         Entity var4 = (Entity)this.hitbox_tree.entities.get(var3);
         if (var1.isAssignableFrom(var4.getClass())) {
            ++var2;
         }
      }

      return var2;
   }

   // $FF: renamed from: player () int
   public final int method_181() {
      return this.surrounding_ground_height;
   }

   // $FF: renamed from: h () int
   public final int water_height() {
      return this.surrounding_water_height;
   }

   // $FF: renamed from: animals (net.minecraft.game.projectiles.animals) boolean
   public final boolean method_183(AABB var1) {
      int var2 = (int)var1.min_x;
      int var3 = (int)var1.max_x + 1;
      int var4 = (int)var1.min_y;
      int var5 = (int)var1.max_y + 1;
      int var6 = (int)var1.min_z;
      int var7 = (int)var1.max_z + 1;
      if (var1.min_x < 0.0F) {
         --var2;
      }

      if (var1.min_y < 0.0F) {
         --var4;
      }

      if (var1.min_z < 0.0F) {
         --var6;
      }

      if (var2 < 0) {
         var2 = 0;
      }

      if (var4 < 0) {
         var4 = 0;
      }

      if (var6 < 0) {
         var6 = 0;
      }

      if (var3 > this.width) {
         var3 = this.width;
      }

      if (var5 > this.height) {
         var5 = this.height;
      }

      if (var7 > this.depth) {
         var7 = this.depth;
      }

      for(int var10 = var2; var10 < var3; ++var10) {
         for(var2 = var4; var2 < var5; ++var2) {
            for(int var8 = var6; var8 < var7; ++var8) {
               Block var9;
               if ((var9 = Block.blocks[this.block_at(var10, var2, var8)]) != null && var9.material.method_343()) {
                  return true;
               }
            }
         }
      }

      return false;
   }

   // $FF: renamed from: entities (net.minecraft.game.projectiles.animals) boolean
   public final boolean method_184(AABB var1) {
      int var2 = (int)var1.min_x;
      int var3 = (int)var1.max_x + 1;
      int var4 = (int)var1.min_y;
      int var5 = (int)var1.max_y + 1;
      int var6 = (int)var1.min_z;
      int var10 = (int)var1.max_z + 1;

      for(var2 = var2; var2 < var3; ++var2) {
         for(int var7 = var4; var7 < var5; ++var7) {
            for(int var8 = var6; var8 < var10; ++var8) {
               int var9;
               if ((var9 = this.block_at(var2, var7, var8)) == Block.fire.block_id || var9 == Block.lava.block_id || var9 == Block.infinite_lava.block_id) {
                  return true;
               }
            }
         }
      }

      return false;
   }

   // $FF: renamed from: game (net.minecraft.game.projectiles.animals, net.minecraft.game.game.projectiles.entities) boolean
   public final boolean collides_with_fluid(AABB var1, Material var2) {
      int var3 = (int)var1.min_x;
      int var4 = (int)var1.max_x + 1;
      int var5 = (int)var1.min_y;
      int var6 = (int)var1.max_y + 1;
      int var7 = (int)var1.min_z;
      int var11 = (int)var1.max_z + 1;

      for(var3 = var3; var3 < var4; ++var3) {
         for(int var8 = var5; var8 < var6; ++var8) {
            for(int var9 = var7; var9 < var11; ++var9) {
               Block var10;
               if ((var10 = Block.blocks[this.block_at(var3, var8, var9)]) != null && var10.material == var2) {
                  return true;
               }
            }
         }
      }

      return false;
   }

   // $FF: renamed from: player (int, int, int, int) void
   public final void method_186(int var1, int var2, int var3, int var4) {
      class_76 var5 = new class_76(var1, var2, var3, var4);
      if (var4 > 0) {
         var3 = Block.blocks[var4].method_509();
         var5.field_358 = var3;
      }

      this.field_322.add(var5);
   }

   // $FF: renamed from: projectiles (net.minecraft.game.projectiles.animals) boolean
   public final boolean method_187(AABB var1) {
      return this.hitbox_tree.colliding_entities((Entity)null, var1).size() == 0;
   }

   // $FF: renamed from: player (net.minecraft.game.projectiles.animals) boolean
   public final boolean method_188(AABB var1) {
      List var4 = this.hitbox_tree.colliding_entities((Entity)null, var1);

      for(int var2 = 0; var2 < var4.size(); ++var2) {
         if (((Entity)var4.get(var2)).maybe_solid) {
            return false;
         }
      }

      return true;
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, net.minecraft.game.projectiles.animals) java.util.List
   public final List colliding_entities(Entity var1, AABB var2) {
      return this.hitbox_tree.colliding_entities(var1, var2);
   }

   // $FF: renamed from: game (float, float, float, float) boolean
   public final boolean method_190(float x, float y, float z, float var4) {
      if (this.method_191(x - 0.1F, y - 0.1F, z - 0.1F)) {
         return true;
      } else if (this.method_191(x - 0.1F, y - 0.1F, z + 0.1F)) {
         return true;
      } else if (this.method_191(x - 0.1F, y + 0.1F, z - 0.1F)) {
         return true;
      } else if (this.method_191(x - 0.1F, y + 0.1F, z + 0.1F)) {
         return true;
      } else if (this.method_191(x + 0.1F, y - 0.1F, z - 0.1F)) {
         return true;
      } else if (this.method_191(x + 0.1F, y - 0.1F, z + 0.1F)) {
         return true;
      } else if (this.method_191(x + 0.1F, y + 0.1F, z - 0.1F)) {
         return true;
      } else {
         return this.method_191(x + 0.1F, y + 0.1F, z + 0.1F);
      }
   }

   // $FF: renamed from: game (float, float, float) boolean
   private boolean method_191(float x, float y, float z) {
      int id = this.block_at((int)x, (int)y, (int)z);
      return id > 0 && Block.blocks[id].method_503();
   }

   // $FF: renamed from: game (int, int) int
   private int method_192(int var1, int var2) {
      int var3;
      for(var3 = this.height; (this.block_at(var1, var3 - 1, var2) == 0 || Block.blocks[this.block_at(var1, var3 - 1, var2)].material == Material.field_608) && var3 > 0; --var3) {
         ;
      }

      return var3;
   }

   // $FF: renamed from: game (int, int, int, float) void
   public final void maybe_set_player_spawn(int var1, int var2, int var3, float var4) {
      this.spawn_x = var1;
      this.spawn_y = var2;
      this.spawn_z = var3;
      this.spawn_yaw = var4;
   }

   // $FF: renamed from: entities (int, int, int) float
   public final float light_at(int var1, int var2, int var3) {
      return maybe_light_levels[this.method_195(var1, var2, var3)];
   }

   // $FF: renamed from: projectiles (int, int, int) byte
   public final byte method_195(int var1, int var2, int var3) {
      if (var1 < 0) {
         var1 = 0;
      } else if (var1 >= this.width) {
         var1 = this.width - 1;
      }

      if (var2 < 0) {
         var2 = 0;
      } else if (var2 >= this.height) {
         var2 = this.height - 1;
      }

      if (var3 < 0) {
         var3 = 0;
      } else if (var3 >= this.depth) {
         var3 = this.depth - 1;
      }

      if (this.blocks[(var2 * this.depth + var3) * this.width + var1] == Block.field_911.block_id) {
         return var2 < this.height - 1 ? (byte)(this.data[((var2 + 1) * this.depth + var3) * this.width + var1] & 15) : 15;
      } else {
         return (byte)(this.data[(var2 * this.depth + var3) * this.width + var1] & 15);
      }
   }

   // $FF: renamed from: player (int, int, int) byte
   public final byte block_data_at(int var1, int var2, int var3) {
      if (var1 < 0) {
         var1 = 0;
      } else if (var1 >= this.width) {
         var1 = this.width - 1;
      }

      if (var2 < 0) {
         var2 = 0;
      } else if (var2 >= this.height) {
         var2 = this.height - 1;
      }

      if (var3 < 0) {
         var3 = 0;
      } else if (var3 >= this.depth) {
         var3 = this.depth - 1;
      }

      return (byte)(this.data[(var2 * this.depth + var3) * this.width + var1] >>> 4 & 15);
   }

   // $FF: renamed from: particles (int, int, int, int) void
   public final void method_197(int var1, int var2, int var3, int var4) {
      if (var1 < 0) {
         var1 = 0;
      } else if (var1 >= this.width) {
         var1 = this.width - 1;
      }

      if (var2 < 0) {
         var2 = 0;
      } else if (var2 >= this.height) {
         var2 = this.height - 1;
      }

      if (var3 < 0) {
         var3 = 0;
      } else if (var3 >= this.depth) {
         var3 = this.depth - 1;
      }

      this.data[(var2 * this.depth + var3) * this.width + var1] = (byte)((this.data[(var2 * this.depth + var3) * this.width + var1] & 15) + (var4 << 4));

      for(var4 = 0; var4 < this.renderers.size(); ++var4) {
         ((IRenderer)this.renderers.get(var4)).method_3(var1, var2, var3);
      }

   }

   // $FF: renamed from: particles (int, int, int) net.minecraft.game.game.projectiles.entities
   public final Material method_198(int var1, int var2, int var3) {
      return (var1 = this.block_at(var1, var2, var3)) == 0 ? Material.field_608 : Block.blocks[var1].material;
   }

   // $FF: renamed from: player (int, int, int) boolean
   public final boolean method_199(int var1, int var2, int var3) {
      return (var1 = this.block_at(var1, var2, var3)) > 0 && Block.blocks[var1].material == Material.water_block;
   }

   // $FF: renamed from: game (net.minecraft.game.projectiles.game, net.minecraft.game.projectiles.game) net.minecraft.game.projectiles.entities
   public final RaycastResult check_raycast(Vector a, Vector b) {
      if (!Float.isNaN(a.x) && !Float.isNaN(a.y) && !Float.isNaN(a.z)) {
         if (!Float.isNaN(b.x) && !Float.isNaN(b.y) && !Float.isNaN(b.z)) {
            int var3 = MathHelper.floorf(b.x);
            int var4 = MathHelper.floorf(b.y);
            int var5 = MathHelper.floorf(b.z);
            int var6 = MathHelper.floorf(a.x);
            int var7 = MathHelper.floorf(a.y);
            int var8 = MathHelper.floorf(a.z);
            int var9 = 20;

            int var21;
            RaycastResult var22;
            Block var23;
            do {
               if (var9-- < 0) {
                  return null;
               }

               if (Float.isNaN(a.x) || Float.isNaN(a.y) || Float.isNaN(a.z)) {
                  return null;
               }

               if (var6 == var3 && var7 == var4 && var8 == var5) {
                  return null;
               }

               float var10 = 999.0F;
               float var11 = 999.0F;
               float var12 = 999.0F;
               if (var3 > var6) {
                  var10 = (float)var6 + 1.0F;
               }

               if (var3 < var6) {
                  var10 = (float)var6;
               }

               if (var4 > var7) {
                  var11 = (float)var7 + 1.0F;
               }

               if (var4 < var7) {
                  var11 = (float)var7;
               }

               if (var5 > var8) {
                  var12 = (float)var8 + 1.0F;
               }

               if (var5 < var8) {
                  var12 = (float)var8;
               }

               float var13 = 999.0F;
               float var14 = 999.0F;
               float var15 = 999.0F;
               float var16 = b.x - a.x;
               float var17 = b.y - a.y;
               float var18 = b.z - a.z;
               if (var10 != 999.0F) {
                  var13 = (var10 - a.x) / var16;
               }

               if (var11 != 999.0F) {
                  var14 = (var11 - a.y) / var17;
               }

               if (var12 != 999.0F) {
                  var15 = (var12 - a.z) / var18;
               }

               byte var19;
               if (var13 < var14 && var13 < var15) {
                  if (var3 > var6) {
                     var19 = 4;
                  } else {
                     var19 = 5;
                  }

                  a.x = var10;
                  a.y += var17 * var13;
                  a.z += var18 * var13;
               } else if (var14 < var15) {
                  if (var4 > var7) {
                     var19 = 0;
                  } else {
                     var19 = 1;
                  }

                  a.x += var16 * var14;
                  a.y = var11;
                  a.z += var18 * var14;
               } else {
                  if (var5 > var8) {
                     var19 = 2;
                  } else {
                     var19 = 3;
                  }

                  a.x += var16 * var15;
                  a.y += var17 * var15;
                  a.z = var12;
               }

               Vector var20;
               var6 = (int)((var20 = new Vector(a.x, a.y, a.z)).x = (float) MathHelper.floorf(a.x));
               if (var19 == 5) {
                  --var6;
                  ++var20.x;
               }

               var7 = (int)(var20.y = (float) MathHelper.floorf(a.y));
               if (var19 == 1) {
                  --var7;
                  ++var20.y;
               }

               var8 = (int)(var20.z = (float) MathHelper.floorf(a.z));
               if (var19 == 3) {
                  --var8;
                  ++var20.z;
               }

               var21 = this.block_at(var6, var7, var8);
               var23 = Block.blocks[var21];
            } while(var21 <= 0 || !var23.method_504() || (var22 = var23.method_518(this, var6, var7, var8, a, b)) == null);

            return var22;
         } else {
            return null;
         }
      } else {
         return null;
      }
   }

   // $FF: renamed from: h (int, int, int) boolean
   public final boolean method_201(int var1, int var2, int var3) {
      int var4 = this.random_a.nextInt(3) + 4;
      boolean var5 = true;
      if (var2 > 0 && var2 + var4 + 1 <= this.height) {
         int var6;
         int var8;
         int var9;
         for(var6 = var2; var6 <= var2 + 1 + var4; ++var6) {
            byte var7 = 1;
            if (var6 == var2) {
               var7 = 0;
            }

            if (var6 >= var2 + 1 + var4 - 2) {
               var7 = 2;
            }

            for(var8 = var1 - var7; var8 <= var1 + var7 && var5; ++var8) {
               for(var9 = var3 - var7; var9 <= var3 + var7 && var5; ++var9) {
                  if (var8 >= 0 && var6 >= 0 && var9 >= 0 && var8 < this.width && var6 < this.height && var9 < this.depth) {
                     if ((this.blocks[(var6 * this.depth + var9) * this.width + var8] & 255) != 0) {
                        var5 = false;
                     }
                  } else {
                     var5 = false;
                  }
               }
            }
         }

         if (!var5) {
            return false;
         } else if (((var6 = this.blocks[((var2 - 1) * this.depth + var3) * this.width + var1] & 255) == Block.grass.block_id || var6 == Block.dirt.block_id) && var2 < this.height - var4 - 1) {
            this.method_165(var1, var2 - 1, var3, Block.dirt.block_id);

            int var13;
            for(var13 = var2 - 3 + var4; var13 <= var2 + var4; ++var13) {
               var8 = var13 - (var2 + var4);
               var9 = 1 - var8 / 2;

               for(int var10 = var1 - var9; var10 <= var1 + var9; ++var10) {
                  int var12 = var10 - var1;

                  for(var6 = var3 - var9; var6 <= var3 + var9; ++var6) {
                     int var11 = var6 - var3;
                     if ((Math.abs(var12) != var9 || Math.abs(var11) != var9 || this.random_a.nextInt(2) != 0 && var8 != 0) && !Block.transparency[this.block_at(var10, var13, var6)]) {
                        this.method_165(var10, var13, var6, Block.field_885.block_id);
                     }
                  }
               }
            }

            for(var13 = 0; var13 < var4; ++var13) {
               if (!Block.transparency[this.block_at(var1, var2 + var13, var3)]) {
                  this.method_165(var1, var2 + var13, var3, Block.field_884.block_id);
               }
            }

            return true;
         } else {
            return false;
         }
      } else {
         return false;
      }
   }

   // $FF: renamed from: i () net.minecraft.game.entities.animals
   public final Entity get_player() {
      return this.player;
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals) void
   public final void add_entity(Entity var1) {
      this.hitbox_tree.register_entity(var1);
      var1.set_world(this);

      for(int var2 = 0; var2 < this.renderers.size(); ++var2) {
         ((IRenderer)this.renderers.get(var2)).method_9(var1);
      }

   }

   // $FF: renamed from: animals (net.minecraft.game.entities.animals) void
   public final void somehow_initialize_the_player_in_the_wordl(Entity var1) {
      this.hitbox_tree.unregister_entity(var1);

      for(int var2 = 0; var2 < this.renderers.size(); ++var2) {
         ((IRenderer)this.renderers.get(var2)).method_10(var1);
      }

   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, float, float, float, float) void
   public final void create_explosion(Entity var1, float var2, float var3, float var4, float var5) { // method_205
      this.method_212(var2, var3, var4, "random.explode", 4.0F, (1.0F + (this.random_a.nextFloat() - this.random_a.nextFloat()) * 0.2F) * 0.7F);
      TreeSet var6 = new TreeSet();
      float var7 = var5;

      int var8;
      int var9;
      int var10;
      float var11;
      float var18;
      int var19;
      int var20;
      int var21;
      int var22;
      for(var8 = 0; var8 < 16; ++var8) {
         for(var9 = 0; var9 < 16; ++var9) {
            for(var10 = 0; var10 < 16; ++var10) {
               if (var8 == 0 || var8 == 15 || var9 == 0 || var9 == 15 || var10 == 0 || var10 == 15) {
                  var11 = (float)var8 / 15.0F * 2.0F - 1.0F;
                  float var12 = (float)var9 / 15.0F * 2.0F - 1.0F;
                  float var13 = (float)var10 / 15.0F * 2.0F - 1.0F;
                  float var14 = (float)Math.sqrt((double)(var11 * var11 + var12 * var12 + var13 * var13));
                  var11 /= var14;
                  var12 /= var14;
                  var13 /= var14;
                  float var15 = var5 * (0.7F + this.random_a.nextFloat() * 0.6F);
                  float var16 = var2;
                  float var17 = var3;

                  for(var18 = var4; var15 > 0.0F; var15 -= 0.22500001F) {
                     var19 = (int)var16;
                     var20 = (int)var17;
                     var21 = (int)var18;
                     if ((var22 = this.block_at(var19, var20, var21)) > 0) {
                        var15 -= (Block.blocks[var22].method_517() + 0.3F) * 0.3F;
                     }

                     if (var15 > 0.0F) {
                        int var23 = var19 + (var20 << 10) + (var21 << 10 << 10);
                        var6.add(var23);
                     }

                     var16 += var11 * 0.3F;
                     var17 += var12 * 0.3F;
                     var18 += var13 * 0.3F;
                  }
               }
            }
         }
      }

      var5 *= 2.0F;
      var8 = (int)(var2 - var5 - 1.0F);
      var9 = (int)(var2 + var5 + 1.0F);
      var10 = (int)(var3 - var5 - 1.0F);
      int var30 = (int)(var3 + var5 + 1.0F);
      int var31 = (int)(var4 - var5 - 1.0F);
      int var32 = (int)(var4 + var5 + 1.0F);
      List var33 = this.hitbox_tree.query_collisions_with(var1, (float)var8, (float)var10, (float)var31, (float)var9, (float)var30, (float)var32);
      Vector var34 = new Vector(var2, var3, var4);

      float var28;
      float var29;
      float var44;
      for(int var35 = 0; var35 < var33.size(); ++var35) {
         Entity var26;
         Entity var37;
         var28 = (var26 = var37 = (Entity)var33.get(var35)).pos_x - var2;
         var29 = var26.pos_y - var3;
         float var27 = var26.pos_z - var4;
         if ((var18 = MathHelper.sqrt(var28 * var28 + var29 * var29 + var27 * var27) / var5) <= 1.0F) {
            var27 = var37.pos_x - var2;
            float var40 = var37.pos_y - var3;
            float var41 = var37.pos_z - var4;
            float var42 = MathHelper.sqrt(var27 * var27 + var40 * var40 + var41 * var41);
            var27 /= var42;
            var40 /= var42;
            var41 /= var42;
            float var43 = this.method_206(var34, var37.hitbox);
            var44 = (1.0F - var18) * var43;
            var37.take_damage(var1, (int)((var44 * var44 + var44) / 2.0F * 8.0F * var5 + 1.0F));
            var37.vel_x += var27 * var44;
            var37.vel_y += var40 * var44;
            var37.vel_z += var41 * var44;
         }
      }

      var5 = var7;
      ArrayList var36;
      (var36 = new ArrayList()).addAll(var6);

      for(int var38 = var36.size() - 1; var38 >= 0; --var38) {
         int var39;
         var8 = (var39 = ((Integer)var36.get(var38)).intValue()) & 1023;
         var19 = var39 >> 10 & 1023;
         var20 = var39 >> 20 & 1023;
         if (var8 >= 0 && var19 >= 0 && var20 >= 0 && var8 < this.width && var19 < this.height && var20 < this.depth) {
            var21 = this.block_at(var8, var19, var20);

            for(var22 = 0; var22 <= 0; ++var22) {
               var44 = (float)var8 + this.random_a.nextFloat();
               var28 = (float)var19 + this.random_a.nextFloat();
               float var24 = (float)var20 + this.random_a.nextFloat();
               float var25 = var44 - var2;
               var7 = var28 - var3;
               var29 = var24 - var4;
               var11 = MathHelper.sqrt(var25 * var25 + var7 * var7 + var29 * var29);
               var25 /= var11;
               var7 /= var11;
               var29 /= var11;
               var11 = (var11 = 0.5F / (var11 / var5 + 0.1F)) * (this.random_a.nextFloat() * this.random_a.nextFloat() + 0.3F);
               var25 *= var11;
               var7 *= var11;
               var29 *= var11;
               this.spawn_effect("explode", (var44 + var2) / 2.0F, (var28 + var3) / 2.0F, (var24 + var4) / 2.0F, var25, var7, var29);
               this.spawn_effect("smoke", var44, var28, var24, var25, var7, var29);
            }

            if (var21 > 0) {
               Block.blocks[var21].method_516(this, var8, var19, var20, this.block_data_at(var8, var19, var20), 0.3F);
               this.method_165(var8, var19, var20, 0);
               Block.blocks[var21].method_522(this, var8, var19, var20);
            }
         }
      }

   }

   // $FF: renamed from: game (net.minecraft.game.projectiles.game, net.minecraft.game.projectiles.animals) float
   private float method_206(Vector var1, AABB var2) {
      float var3 = 1.0F / ((var2.max_x - var2.min_x) * 2.0F + 1.0F);
      float var4 = 1.0F / ((var2.max_y - var2.min_y) * 2.0F + 1.0F);
      float var5 = 1.0F / ((var2.max_z - var2.min_z) * 2.0F + 1.0F);
      int var6 = 0;
      int var7 = 0;

      for(float var8 = 0.0F; var8 <= 1.0F; var8 += var3) {
         for(float var9 = 0.0F; var9 <= 1.0F; var9 += var4) {
            for(float var10 = 0.0F; var10 <= 1.0F; var10 += var5) {
               float var11 = var2.min_x + (var2.max_x - var2.min_x) * var8;
               float var12 = var2.min_y + (var2.max_y - var2.min_y) * var9;
               float var13 = var2.min_z + (var2.max_z - var2.min_z) * var10;
               if (this.check_raycast(new Vector(var11, var12, var13), var1) == null) {
                  ++var6;
               }

               ++var7;
            }
         }
      }

      return (float)var6 / (float)var7;
   }

   // $FF: renamed from: animals (java.lang.Class) net.minecraft.game.entities.animals
   public final Entity find_entity_of_type(Class var1) {
      for(int var2 = 0; var2 < this.hitbox_tree.entities.size(); ++var2) {
         Entity var3 = (Entity)this.hitbox_tree.entities.get(var2);
         if (var1.isAssignableFrom(var3.getClass())) {
            return var3;
         }
      }

      return null;
   }

   // $FF: renamed from: game (int, int, int, int, int) int
   public final int method_208(int var1, int var2, int var3, int var4, int var5) {
      if (var1 >= 0 && var2 >= 0 && var3 >= 0 && var1 < this.width && var2 < this.height && var3 < this.depth) {
         int var6 = var1;
         int var7 = var3;
         int var8 = ((var2 << 10) + var3 << 10) + var1;
         byte var9 = 0;
         int var20 = var9 + 1;
         this.field_347[0] = var1 + (var3 << 10);
         int var11 = -9999;
         if (var4 == Block.infinite_water.block_id || var4 == Block.water.block_id) {
            var11 = Block.field_919.block_id;
         }

         if (var4 == Block.infinite_lava.block_id || var4 == Block.lava.block_id) {
            var11 = Block.field_920.block_id;
         }

         int var10;
         boolean var12;
         label170:
         do {
            var12 = false;
            int var13 = -1;
            var10 = 0;
            if (++field_345 == 30000) {
               Arrays.fill(this.field_346, (short)0);
               field_345 = 1;
            }

            while(true) {
               int var14;
               do {
                  if (var20 <= 0) {
                     ++var2;
                     int[] var21 = this.field_348;
                     this.field_348 = this.field_347;
                     this.field_347 = var21;
                     var20 = var10;
                     continue label170;
                  }

                  --var20;
                  var14 = this.field_347[var20];
               } while(this.field_346[var14] == field_345);

               var1 = var14 % 1024;

               int var15;
               for(var15 = (var15 = (var3 = var14 / 1024) - var7) * var15; var1 > 0 && this.field_346[var14 - 1] != field_345 && (this.blocks[(var2 * this.depth + var3) * this.width + var1 - 1] == var4 || this.blocks[(var2 * this.depth + var3) * this.width + var1 - 1] == var5); --var14) {
                  --var1;
               }

               if (var1 > 0 && this.blocks[(var2 * this.depth + var3) * this.width + var1 - 1] == var11) {
                  var12 = true;
               }

               boolean var16 = false;
               boolean var17 = false;

               for(boolean var18 = false; var1 < this.width && this.field_346[var14] != field_345 && (this.blocks[(var2 * this.depth + var3) * this.width + var1] == var4 || this.blocks[(var2 * this.depth + var3) * this.width + var1] == var5); ++var1) {
                  byte var19;
                  boolean var22;
                  if (var3 > 0) {
                     if ((var19 = this.blocks[(var2 * this.depth + var3 - 1) * this.width + var1]) == var11) {
                        var12 = true;
                     }

                     if ((var22 = this.field_346[var14 - 1024] != field_345 && (var19 == var4 || var19 == var5)) && !var16) {
                        this.field_347[var20++] = var14 - 1024;
                     }

                     var16 = var22;
                  }

                  if (var3 < this.depth - 1) {
                     if ((var19 = this.blocks[(var2 * this.depth + var3 + 1) * this.width + var1]) == var11) {
                        var12 = true;
                     }

                     if ((var22 = this.field_346[var14 + 1024] != field_345 && (var19 == var4 || var19 == var5)) && !var17) {
                        this.field_347[var20++] = var14 + 1024;
                     }

                     var17 = var22;
                  }

                  if (var2 < this.height - 1) {
                     if ((var22 = (var19 = this.blocks[((var2 + 1) * this.depth + var3) * this.width + var1]) == var4 || var19 == var5) && !var18) {
                        this.field_348[var10++] = var14;
                     }

                     var18 = var22;
                  }

                  int var23;
                  if ((var23 = (var23 = var1 - var6) * var23 + var15) > var13) {
                     var13 = var23;
                     var8 = ((var2 << 10) + var3 << 10) + var1;
                  }

                  this.field_346[var14++] = field_345;
               }

               if (var1 < this.width && this.blocks[(var2 * this.depth + var3) * this.width + var1] == var11) {
                  var12 = true;
               }
            }
         } while(var10 > 0);

         if (var12) {
            return -9999;
         } else {
            return var8;
         }
      } else {
         return -1;
      }
   }

   // $FF: renamed from: animals (int, int, int, int, int) int
   public final int method_209(int var1, int var2, int var3, int var4, int var5) {
      if (var1 >= 0 && var2 >= 0 && var3 >= 0 && var1 < this.width && var2 < this.height && var3 < this.depth) {
         if (++field_345 == 30000) {
            Arrays.fill(this.field_346, (short)0);
            field_345 = 1;
         }

         byte var6 = 0;
         int var11 = var6 + 1;
         this.field_347[0] = var1 + (var3 << 10);

         do {
            int var7;
            do {
               if (var11 <= 0) {
                  return 1;
               }

               --var11;
               var7 = this.field_347[var11];
            } while(this.field_346[var7] == field_345);

            var1 = var7 % 1024;
            var3 = var7 / 1024;
            if (var1 == 0 || var1 == this.width - 1 || var2 == 0 || var2 == this.height - 1 || var3 == 0 || var3 == this.depth - 1) {
               return 2;
            }

            while(var1 > 0 && this.field_346[var7 - 1] != field_345 && (this.blocks[(var2 * this.depth + var3) * this.width + var1 - 1] == var4 || this.blocks[(var2 * this.depth + var3) * this.width + var1 - 1] == var5)) {
               --var1;
               --var7;
            }

            if (var1 > 0 && this.blocks[(var2 * this.depth + var3) * this.width + var1 - 1] == 0) {
               return 0;
            }

            boolean var8 = false;

            for(boolean var9 = false; var1 < this.width && this.field_346[var7] != field_345 && (this.blocks[(var2 * this.depth + var3) * this.width + var1] == var4 || this.blocks[(var2 * this.depth + var3) * this.width + var1] == var5); ++var1) {
               if (var1 == 0 || var1 == this.width - 1) {
                  return 2;
               }

               byte var10;
               boolean var12;
               if (var3 > 0) {
                  if ((var10 = this.blocks[(var2 * this.depth + var3 - 1) * this.width + var1]) == 0) {
                     return 0;
                  }

                  if ((var12 = this.field_346[var7 - 1024] != field_345 && (var10 == var4 || var10 == var5)) && !var8) {
                     this.field_347[var11++] = var7 - 1024;
                  }

                  var8 = var12;
               }

               if (var3 < this.depth - 1) {
                  if ((var10 = this.blocks[(var2 * this.depth + var3 + 1) * this.width + var1]) == 0) {
                     return 0;
                  }

                  if ((var12 = this.field_346[var7 + 1024] != field_345 && (var10 == var4 || var10 == var5)) && !var9) {
                     this.field_347[var11++] = var7 + 1024;
                  }

                  var9 = var12;
               }

               this.field_346[var7] = field_345;
               ++var7;
            }
         } while(var1 >= this.width || this.blocks[(var2 * this.depth + var3) * this.width + var1] != 0);

         return 0;
      } else {
         return 0;
      }
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, java.lang.String, float, float) void
   public final void play_sound(Entity var1, String var2, float var3, float var4) {
      for(int var5 = 0; var5 < this.renderers.size(); ++var5) {
         float var6 = 16.0F;
         if (var3 > 1.0F) {
            var6 = 16.0F * var3;
         }

         if (this.player.distance_squared(var1) < var6 * var6) {
            ((IRenderer)this.renderers.get(var5)).method_6(var2, var1.pos_x, var1.pos_y - var1.camera_height, var1.pos_z, var3, var4);
         }
      }

   }

   // $FF: renamed from: game (float, float, float, java.lang.String, float) void
   public final void play_music(float var1, float var2, float var3, String var4, float var5) {
      try {
         for(int var7 = 0; var7 < this.renderers.size(); ++var7) {
            ((IRenderer)this.renderers.get(var7)).method_8(var4, var1, var2, var3, 0.0F);
         }

      } catch (Exception var6) {
         var6.printStackTrace();
      }
   }

   // $FF: renamed from: game (float, float, float, java.lang.String, float, float) void
   public final void method_212(float var1, float var2, float var3, String var4, float var5, float var6) {
      try {
         for(int var7 = 0; var7 < this.renderers.size(); ++var7) {
            float var8 = 16.0F;
            if (var5 > 1.0F) {
               var8 = 16.0F * var5;
            }

            float var9 = var1 - this.player.pos_x;
            float var10 = var2 - this.player.pos_y;
            float var11 = var3 - this.player.pos_z;
            if (var9 * var9 + var10 * var10 + var11 * var11 < var8 * var8) {
               ((IRenderer)this.renderers.get(var7)).method_6(var4, var1, var2, var3, var5, var6);
            }
         }

      } catch (Exception var12) {
         var12.printStackTrace();
      }
   }

   // $FF: renamed from: player (int, int, int, int) void
   public final void method_213(int var1, int var2, int var3, int var4) {
      if (var4 == 0) {
         --var2;
      }

      if (var4 == 1) {
         ++var2;
      }

      if (var4 == 2) {
         --var3;
      }

      if (var4 == 3) {
         ++var3;
      }

      if (var4 == 4) {
         --var1;
      }

      if (var4 == 5) {
         ++var1;
      }

      if (this.block_at(var1, var2, var3) == Block.fire.block_id) {
         this.method_212((float)var1 + 0.5F, (float)var2 + 0.5F, (float)var3 + 0.5F, "random.fizz", 0.5F, 2.6F + (this.random_a.nextFloat() - this.random_a.nextFloat()) * 0.8F);
         this.method_165(var1, var2, var3, 0);
      }

   }

   // $FF: renamed from: game (int, int, int, net.minecraft.game.game.animals.game.game) void
   public final void method_214(int x, int y, int z, DataBlock dblock) {
      dblock.world = this;
      dblock.x = x;
      dblock.y = y;
      dblock.z = z;
      this.data_block_map.put(x + (y << 10) + (z << 10 << 10), dblock);
      this.data_blocks.add(dblock);
   }

   // $FF: renamed from: i (int, int, int) void
   public final void method_215(int var1, int var2, int var3) {
      this.data_blocks.remove(this.data_block_map.remove(var1 + (var2 << 10) + (var3 << 10 << 10)));
   }

   // $FF: renamed from: j (int, int, int) net.minecraft.game.game.animals.game.game
   public final DataBlock method_216(int var1, int var2, int var3) {
      int var4 = var1 + (var2 << 10) + (var3 << 10 << 10);
      DataBlock var5;
      if ((var5 = (DataBlock)this.data_block_map.get(var4)) == null) {
         int var6 = this.block_at(var1, var2, var3);
         ((class_185) Block.blocks[var6]).method_510(this, var1, var2, var3);
         var5 = (DataBlock)this.data_block_map.get(var4);
      }

      return var5;
   }

   // $FF: renamed from: game (java.lang.String, float, float, float, float, float, float) void
   public final void spawn_effect(String var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      for(int var8 = 0; var8 < this.renderers.size(); ++var8) {
         ((IRenderer)this.renderers.get(var8)).method_7(var1, var2, var3, var4, var5, var6, var7);
      }

   }

   // $FF: renamed from: k (int, int, int) void
   public final void method_218(int var1, int var2, int var3) {
      for(int var4 = 0; var4 < 1000; ++var4) {
         int var5 = var1 + this.random_a.nextInt(16) - this.random_a.nextInt(16);
         int var6 = var2 + this.random_a.nextInt(16) - this.random_a.nextInt(16);
         int var7 = var3 + this.random_a.nextInt(16) - this.random_a.nextInt(16);
         int var8;
         if ((var8 = this.block_at(var5, var6, var7)) > 0) {
            Block.blocks[var8].method_506(this, var5, var6, var7, this.random_b);
         }
      }

   }

   public void make_block_fall( int x, int y, int z ) {
      int by = y - 1;
      int below = this.block_at( x, by, z );
      if( by < 0
              || below == 0
              || below == Block.fire.block_id
              || Block.blocks[below].material == Material.water_block
              || Block.blocks[below].material == Material.lava_block ) {
         this.add_entity( new FallingBlock( this, this.block_at( x, y, z ), (float)x + 0.5f, (float)y + 0.5f, (float)z + 0.5f ));
         this.set_block(x, y, z, 0);
      }
   }

   // $FF: renamed from: j () java.lang.String
   public final String method_219() {
      return "" + this.field_322.size() + ". L: " + this.maybe_chunk.method_152();
   }

   // $FF: renamed from: k () void
   public final void setup_stuff() {
      for(int var1 = 0; var1 < this.renderers.size(); ++var1) {
         IRenderer var2 = (IRenderer)this.renderers.get(var1);

         for(int var3 = 0; var3 < this.hitbox_tree.entities.size(); ++var3) {
            var2.method_10((Entity)this.hitbox_tree.entities.get(var1));
         }
      }

   }

   // $FF: renamed from: game (int) void
   private void method_221(int var1) {
      this.maybe_chunk.method_147(var1);
   }

   // $FF: renamed from: l (int, int, int) boolean
   public final boolean has_skylight(int x, int y, int z) {
      if (this.height_map[x + z * this.width] <= y) {
         return true;
      } else {
         while(y < this.height) {
            if (Block.transparency[this.block_at(x, y, z)]) {
               return false;
            }

            ++y;
         }

         return true;
      }
   }

   static {
      for(int var0 = 0; var0 <= 15; ++var0) {
         float var1 = 1.0F - (float)var0 / 15.0F;
         maybe_light_levels[var0] = (1.0F - var1) / (var1 * 3.0F + 1.0F) * 0.95F + 0.05F;
      }

      field_345 = 0;
   }
}
