package net.minecraft.game.world;

import net.minecraft.game.entities.Entity;

// $FF: renamed from: net.minecraft.game.game.projectiles
public interface IRenderer {
   // $FF: renamed from: game (int, int, int) void
   void method_3(int var1, int var2, int var3);

   // $FF: renamed from: game (int, int, int, int, int, int) void
   void method_4(int var1, int var2, int var3, int var4, int var5, int var6);

   // $FF: renamed from: game () void
   void update_render_chunks();

   // $FF: renamed from: game (java.lang.String, float, float, float, float, float) void
   void method_6(String var1, float var2, float var3, float var4, float var5, float var6);

   // $FF: renamed from: game (java.lang.String, float, float, float, float, float, float) void
   void method_7(String var1, float var2, float var3, float var4, float var5, float var6, float var7);

   // $FF: renamed from: game (java.lang.String, float, float, float, float) void
   void method_8(String var1, float var2, float var3, float var4, float var5);

   // $FF: renamed from: game (net.minecraft.game.entities.animals) void
   void method_9(Entity var1);

   // $FF: renamed from: animals (net.minecraft.game.entities.animals) void
   void method_10(Entity var1);

   // $FF: renamed from: h () void
   void method_11();
}
