package net.minecraft.game.util;

import a.MathHelper;

// $FF: renamed from: net.minecraft.game.projectiles.game
public final class Vector {
   // $FF: renamed from: game float
   public float x;
   // $FF: renamed from: animals float
   public float y;
   // $FF: renamed from: entities float
   public float z;

   public Vector(float var1, float var2, float var3) {
      this.x = var1;
      this.y = var2;
      this.z = var3;
   }

   // $FF: renamed from: game (net.minecraft.game.projectiles.game) net.minecraft.game.projectiles.game
   public final Vector sub(Vector var1) {
      return new Vector(this.x - var1.x, this.y - var1.y, this.z - var1.z);
   }

   // $FF: renamed from: game () net.minecraft.game.projectiles.game
   public final Vector norm() {
      float var1 = MathHelper.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
      return new Vector(this.x / var1, this.y / var1, this.z / var1);
   }

   // $FF: renamed from: game (float, float, float) net.minecraft.game.projectiles.game
   public final Vector add(float var1, float var2, float var3) {
      return new Vector(this.x + var1, this.y + var2, this.z + var3);
   }

   // $FF: renamed from: animals (net.minecraft.game.projectiles.game) float
   public final float distance(Vector var1) {
      float var2 = var1.x - this.x;
      float var3 = var1.y - this.y;
      float var4 = var1.z - this.z;
      return MathHelper.sqrt(var2 * var2 + var3 * var3 + var4 * var4);
   }

   // $FF: renamed from: entities (net.minecraft.game.projectiles.game) float
   public final float dot(Vector var1) {
      float var2 = var1.x - this.x;
      float var3 = var1.y - this.y;
      float var4 = var1.z - this.z;
      return var2 * var2 + var3 * var3 + var4 * var4;
   }

   // $FF: renamed from: game (net.minecraft.game.projectiles.game, float) net.minecraft.game.projectiles.game
   public final Vector weird_thing_x(Vector b, float k) {
      float dx = b.x - this.x;
      float dy = b.y - this.y;
      float dz = b.z - this.z;
      if (dx * dx < 1.0E-7F) {
         return null;
      } else {
         k = (k - this.x) / dx;
         if( k >= 0.0F && k <= 1.0F ) {
            return new Vector(this.x + dx * k, this.y + dy * k, this.z + dz * k);
         }
         return null;
      }
   }

   // $FF: renamed from: animals (net.minecraft.game.projectiles.game, float) net.minecraft.game.projectiles.game
   public final Vector weird_thing_y(Vector var1, float var2) {
      float var3 = var1.x - this.x;
      float var4 = var1.y - this.y;
      float var5 = var1.z - this.z;
      if (var4 * var4 < 1.0E-7F) {
         return null;
      } else {
         return (var2 = (var2 - this.y) / var4) >= 0.0F && var2 <= 1.0F ? new Vector(this.x + var3 * var2, this.y + var4 * var2, this.z + var5 * var2) : null;
      }
   }

   // $FF: renamed from: entities (net.minecraft.game.projectiles.game, float) net.minecraft.game.projectiles.game
   public final Vector weird_thing_z(Vector var1, float var2) {
      float var3 = var1.x - this.x;
      float var4 = var1.y - this.y;
      float var5;
      if ((var5 = var1.z - this.z) * var5 < 1.0E-7F) {
         return null;
      } else {
         return (var2 = (var2 - this.z) / var5) >= 0.0F && var2 <= 1.0F ? new Vector(this.x + var3 * var2, this.y + var4 * var2, this.z + var5 * var2) : null;
      }
   }

   public final String toString() {
      return "(" + this.x + ", " + this.y + ", " + this.z + ")";
   }
}
