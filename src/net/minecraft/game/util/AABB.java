package net.minecraft.game.util;

// $FF: renamed from: net.minecraft.game.projectiles.animals
public final class AABB {
   // $FF: renamed from: player float
   private float field_818 = 0.0F;
   // $FF: renamed from: game float
   public float min_x;
   // $FF: renamed from: animals float
   public float min_y;
   // $FF: renamed from: entities float
   public float min_z;
   // $FF: renamed from: projectiles float
   public float max_x;
   // $FF: renamed from: player float
   public float max_y;
   // $FF: renamed from: particles float
   public float max_z;

   public AABB(float var1, float var2, float var3, float var4, float var5, float var6) {
      this.min_x = var1;
      this.min_y = var2;
      this.min_z = var3;
      this.max_x = var4;
      this.max_y = var5;
      this.max_z = var6;
   }

   // $FF: renamed from: game (float, float, float) net.minecraft.game.projectiles.animals
   public final AABB extend_sides(float w, float h, float z) {
      float mx = this.min_x;
      float my = this.min_y;
      float mz = this.min_z;
      float xx = this.max_x;
      float xy = this.max_y;
      float xz = this.max_z;
      if (w < 0.0F) {
         mx += w;
      }

      if (w > 0.0F) {
         xx += w;
      }

      if (h < 0.0F) {
         my += h;
      }

      if (h > 0.0F) {
         xy += h;
      }

      if (z < 0.0F) {
         mz += z;
      }

      if (z > 0.0F) {
         xz += z;
      }

      return new AABB(mx, my, mz, xx, xy, xz);
   }

   // $FF: renamed from: animals (float, float, float) net.minecraft.game.projectiles.animals
   public final AABB grow(float x, float y, float z) {
      if (this.min_y > this.max_y) {
         throw new IllegalArgumentException("NOOOOOO!");
      } else {
         float var4 = this.min_x - x;
         float var5 = this.min_y - y;
         float var6 = this.min_z - z;
         x += this.max_x;
         y += this.max_y;
         z += this.max_z;
         return new AABB(var4, var5, var6, x, y, z);
      }
   }

   // $FF: renamed from: game (net.minecraft.game.projectiles.animals, float) float
   public final float method_469(AABB var1, float var2) {
      if (var1.max_y > this.min_y && var1.min_y < this.max_y) {
         if (var1.max_z > this.min_z && var1.min_z < this.max_z) {
            float var3;
            if (var2 > 0.0F && var1.max_x <= this.min_x && (var3 = this.min_x - var1.max_x) < var2) {
               var2 = var3;
            }

            if (var2 < 0.0F && var1.min_x >= this.max_x && (var3 = this.max_x - var1.min_x) > var2) {
               var2 = var3;
            }

            return var2;
         } else {
            return var2;
         }
      } else {
         return var2;
      }
   }

   // $FF: renamed from: animals (net.minecraft.game.projectiles.animals, float) float
   public final float method_470(AABB other, float var2) {
      if (other.max_x > this.min_x && other.min_x < this.max_x) {
         if (other.max_z > this.min_z && other.min_z < this.max_z) {
            float var3;
            if (var2 > 0.0F && other.max_y <= this.min_y && (var3 = this.min_y - other.max_y) < var2) {
               var2 = var3;
            }

            if (var2 < 0.0F && other.min_y >= this.max_y && (var3 = this.max_y - other.min_y) > var2) {
               var2 = var3;
            }

            return var2;
         } else {
            return var2;
         }
      } else {
         return var2;
      }
   }

   // $FF: renamed from: entities (net.minecraft.game.projectiles.animals, float) float
   public final float method_471(AABB var1, float var2) {
      if (var1.max_x > this.min_x && var1.min_x < this.max_x) {
         if (var1.max_y > this.min_y && var1.min_y < this.max_y) {
            float var3;
            if (var2 > 0.0F && var1.max_z <= this.min_z && (var3 = this.min_z - var1.max_z) < var2) {
               var2 = var3;
            }

            if (var2 < 0.0F && var1.min_z >= this.max_z && (var3 = this.max_z - var1.min_z) > var2) {
               var2 = var3;
            }

            return var2;
         } else {
            return var2;
         }
      } else {
         return var2;
      }
   }

   // $FF: renamed from: game (net.minecraft.game.projectiles.animals) boolean
   public final boolean method_472(AABB var1) {
      if (var1.max_x >= this.min_x && var1.min_x <= this.max_x) {
         if (var1.max_y >= this.min_y && var1.min_y <= this.max_y) {
            return var1.max_z >= this.min_z && var1.min_z <= this.max_z;
         } else {
            return false;
         }
      } else {
         return false;
      }
   }

   // $FF: renamed from: entities (float, float, float) void
   public final void offset(float w, float h, float d) {
      this.min_x += w;
      this.min_y += h;
      this.min_z += d;
      this.max_x += w;
      this.max_y += h;
      this.max_z += d;
   }

   // $FF: renamed from: game () net.minecraft.game.projectiles.animals
   public final AABB copy() {
      return new AABB(this.min_x, this.min_y, this.min_z, this.max_x, this.max_y, this.max_z);
   }

   // $FF: renamed from: game (net.minecraft.game.projectiles.game, net.minecraft.game.projectiles.game) net.minecraft.game.projectiles.entities
   public final RaycastResult method_475(Vector var1, Vector var2) {
      Vector var3 = var1.weird_thing_x(var2, this.min_x);
      Vector var4 = var1.weird_thing_x(var2, this.max_x);
      Vector var5 = var1.weird_thing_y(var2, this.min_y);
      Vector var6 = var1.weird_thing_y(var2, this.max_y);
      Vector var7 = var1.weird_thing_z(var2, this.min_z);
      var2 = var1.weird_thing_z(var2, this.max_z);
      if (!this.method_476(var3)) {
         var3 = null;
      }

      if (!this.method_476(var4)) {
         var4 = null;
      }

      if (!this.method_477(var5)) {
         var5 = null;
      }

      if (!this.method_477(var6)) {
         var6 = null;
      }

      if (!this.method_478(var7)) {
         var7 = null;
      }

      if (!this.method_478(var2)) {
         var2 = null;
      }

      Vector var8 = null;
      if (var3 != null) {
         var8 = var3;
      }

      if (var4 != null && (var8 == null || var1.dot(var4) < var1.dot(var8))) {
         var8 = var4;
      }

      if (var5 != null && (var8 == null || var1.dot(var5) < var1.dot(var8))) {
         var8 = var5;
      }

      if (var6 != null && (var8 == null || var1.dot(var6) < var1.dot(var8))) {
         var8 = var6;
      }

      if (var7 != null && (var8 == null || var1.dot(var7) < var1.dot(var8))) {
         var8 = var7;
      }

      if (var2 != null && (var8 == null || var1.dot(var2) < var1.dot(var8))) {
         var8 = var2;
      }

      if (var8 == null) {
         return null;
      } else {
         byte var9 = -1;
         if (var8 == var3) {
            var9 = 4;
         }

         if (var8 == var4) {
            var9 = 5;
         }

         if (var8 == var5) {
            var9 = 0;
         }

         if (var8 == var6) {
            var9 = 1;
         }

         if (var8 == var7) {
            var9 = 2;
         }

         if (var8 == var2) {
            var9 = 3;
         }

         return new RaycastResult(0, 0, 0, var9, var8);
      }
   }

   // $FF: renamed from: game (net.minecraft.game.projectiles.game) boolean
   private boolean method_476(Vector var1) {
      if (var1 == null) {
         return false;
      } else {
         return var1.y >= this.min_y && var1.y <= this.max_y && var1.z >= this.min_z && var1.z <= this.max_z;
      }
   }

   // $FF: renamed from: animals (net.minecraft.game.projectiles.game) boolean
   private boolean method_477(Vector var1) {
      if (var1 == null) {
         return false;
      } else {
         return var1.x >= this.min_x && var1.x <= this.max_x && var1.z >= this.min_z && var1.z <= this.max_z;
      }
   }

   // $FF: renamed from: entities (net.minecraft.game.projectiles.game) boolean
   private boolean method_478(Vector var1) {
      if (var1 == null) {
         return false;
      } else {
         return var1.x >= this.min_x && var1.x <= this.max_x && var1.y >= this.min_y && var1.y <= this.max_y;
      }
   }
}
