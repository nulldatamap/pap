package net.minecraft.game.util;

import net.minecraft.game.entities.Entity;

// $FF: renamed from: net.minecraft.game.projectiles.entities
public final class RaycastResult {
   // $FF: renamed from: game int
   public int field_811;
   // $FF: renamed from: animals int
   public int x;
   // $FF: renamed from: entities int
   public int y;
   // $FF: renamed from: projectiles int
   public int z;
   // $FF: renamed from: player int
   public int face;
   // $FF: renamed from: particles net.minecraft.game.projectiles.game
   public Vector end_point;
   // $FF: renamed from: player net.minecraft.game.entities.animals
   public Entity entity;

   public RaycastResult(int var1, int var2, int var3, int var4, Vector var5) {
      this.field_811 = 0;
      this.x = var1;
      this.y = var2;
      this.z = var3;
      this.face = var4;
      this.end_point = new Vector(var5.x, var5.y, var5.z);
   }

   public RaycastResult(Entity ent) {
      this.field_811 = 1;
      this.entity = ent;
      this.end_point = new Vector(ent.pos_x, ent.pos_y, ent.pos_z);
   }
}
