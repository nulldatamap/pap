package net.minecraft.game;

import net.minecraft.game.items.ItemStack;

// $FF: renamed from: net.minecraft.game.animals
public interface Storage {
   // $FF: renamed from: game () int
   int capacity();

   // $FF: renamed from: game (int) net.minecraft.game.animals.k
   ItemStack get_slot(int var1);

   // $FF: renamed from: game (int, int) net.minecraft.game.animals.k
   ItemStack take_from(int var1, int var2);

   // $FF: renamed from: game (int, net.minecraft.game.animals.k) void
   void set_slot(int var1, ItemStack var2);

   // $FF: renamed from: animals () java.lang.String
   String nbtName();

   // $FF: renamed from: entities () int
   int max_stack_size();
}
