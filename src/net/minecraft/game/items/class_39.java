package net.minecraft.game.items;

import net.minecraft.game.world.block.Block;

// $FF: renamed from: net.minecraft.game.animals.player
public class class_39 extends Item {
   // $FF: renamed from: as net.minecraft.game.game.animals.x[]
   private Block[] field_206;
   // $FF: renamed from: at float
   private float field_207 = 4.0F;
   // $FF: renamed from: au int
   private int field_208;

   public class_39(int var1, int var2, int var3, Block[] var4) {
      super(var1);
      this.field_206 = var4;
      this.stack_size = 1;
      this.durability = 32 << var3;
      this.field_207 = (float)(var3 + 1 << 1);
      this.field_208 = var2 + var3;
   }

   // $FF: renamed from: animals (net.minecraft.game.game.animals.x) float
   public final float method_86(Block var1) {
      for(int var2 = 0; var2 < this.field_206.length; ++var2) {
         if (this.field_206[var2] == var1) {
            return this.field_207;
         }
      }

      return 1.0F;
   }

   // $FF: renamed from: game (net.minecraft.game.animals.k) void
   public final void method_90(ItemStack var1) {
      var1.take_damage(2);
   }

   // $FF: renamed from: animals (net.minecraft.game.animals.k) void
   public final void method_91(ItemStack var1) {
      var1.take_damage(1);
   }

   // $FF: renamed from: game () int
   public final int method_92() {
      return this.field_208;
   }
}
