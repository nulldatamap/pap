package net.minecraft.game.items;

import net.minecraft.game.world.World;
import net.minecraft.game.entities.player.Player;

// $FF: renamed from: net.minecraft.game.animals.p
public class class_45 extends Item {
   // $FF: renamed from: as int
   private int heal_amount;

   public class_45(int var1, int var2) {
      super(var1);
      this.heal_amount = var2;
      this.stack_size = 1;
   }

   // $FF: renamed from: game (net.minecraft.game.animals.k, net.minecraft.game.game.player, net.minecraft.game.entities.player.game) net.minecraft.game.animals.k
   public ItemStack on_use(ItemStack items, World var2, Player var3) {
      --items.count;
      var3.heal(this.heal_amount);
      return items;
   }
}
