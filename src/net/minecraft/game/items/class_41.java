package net.minecraft.game.items;

import net.minecraft.game.world.block.Block;

// $FF: renamed from: net.minecraft.game.animals.l
public final class class_41 extends class_39 {
   // $FF: renamed from: as net.minecraft.game.game.animals.x[]
   private static Block[] field_211;

   public class_41(int var1, int var2) {
      super(var1, 1, var2, field_211);
   }

   static {
      field_211 = new Block[]{Block.grass, Block.dirt, Block.sand, Block.field_880};
   }
}
