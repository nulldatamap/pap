package net.minecraft.game.items;

import net.minecraft.game.world.block.Block;

// $FF: renamed from: net.minecraft.game.animals.h
public final class class_42 extends class_39 {
   // $FF: renamed from: as net.minecraft.game.game.animals.x[]
   private static Block[] field_212;

   public class_42(int var1, int var2) {
      super(var1, 3, var2, field_212);
   }

   static {
      field_212 = new Block[]{Block.wood_planks, Block.field_914, Block.field_884, Block.field_921};
   }
}
