package net.minecraft.game.items;

import net.minecraft.game.world.World;
import net.minecraft.game.entities.projectiles.Arrow;
import net.minecraft.game.entities.player.Player;

// $FF: renamed from: net.minecraft.game.animals.player
public final class Bow extends Item {
   public Bow(int var1) {
      super(5);
      this.stack_size = 1;
   }

   // $FF: renamed from: game (net.minecraft.game.animals.k, net.minecraft.game.game.player, net.minecraft.game.entities.player.game) net.minecraft.game.animals.k
   public final ItemStack on_use(ItemStack var1, World var2, Player var3) {
      if (var3.inventory.remove_one_of(Item.bone.unique_id)) {
         var2.play_sound(var3, "random.bow", 1.0F, 1.0F / (field_132.nextFloat() * 0.4F + 0.8F));
         var2.add_entity(new Arrow(var2, var3));
      }

      return var1;
   }
}
