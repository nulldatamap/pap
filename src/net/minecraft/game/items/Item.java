package net.minecraft.game.items;

import java.util.Random;
import net.minecraft.game.world.World;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.entities.player.Player;

// $FF: renamed from: net.minecraft.game.animals.i
public class Item {
   // $FF: renamed from: game java.util.Random
   protected static Random field_132 = new Random();
   // $FF: renamed from: animals net.minecraft.game.animals.i[]
   public static Item[] items = new Item[1024];
   // $FF: renamed from: entities net.minecraft.game.animals.i
   public static Item field_134;
   // $FF: renamed from: projectiles net.minecraft.game.animals.i
   public static Item field_135;
   // $FF: renamed from: player net.minecraft.game.animals.i
   public static Item field_136;
   // $FF: renamed from: particles net.minecraft.game.animals.i
   public static Item field_137;
   // $FF: renamed from: player net.minecraft.game.animals.i
   public static Item field_138;
   // $FF: renamed from: h net.minecraft.game.animals.i
   public static Item bone;
   // $FF: renamed from: i net.minecraft.game.animals.i
   public static Item field_140;
   // $FF: renamed from: j net.minecraft.game.animals.i
   public static Item diamond;
   // $FF: renamed from: k net.minecraft.game.animals.i
   public static Item iron_ingot;
   // $FF: renamed from: l net.minecraft.game.animals.i
   public static Item gold_ingot;
   // $FF: renamed from: m net.minecraft.game.animals.i
   public static Item field_144;
   // $FF: renamed from: n net.minecraft.game.animals.i
   public static Item field_145;
   // $FF: renamed from: o net.minecraft.game.animals.i
   public static Item field_146;
   // $FF: renamed from: p net.minecraft.game.animals.i
   public static Item field_147;
   // $FF: renamed from: q net.minecraft.game.animals.i
   public static Item field_148;
   // $FF: renamed from: r net.minecraft.game.animals.i
   public static Item field_149;
   // $FF: renamed from: s net.minecraft.game.animals.i
   public static Item field_150;
   // $FF: renamed from: t net.minecraft.game.animals.i
   public static Item field_151;
   // $FF: renamed from: u net.minecraft.game.animals.i
   public static Item field_152;
   // $FF: renamed from: v net.minecraft.game.animals.i
   public static Item field_153;
   // $FF: renamed from: w net.minecraft.game.animals.i
   public static Item field_154;
   // $FF: renamed from: x net.minecraft.game.animals.i
   public static Item field_155;
   // $FF: renamed from: y net.minecraft.game.animals.i
   public static Item field_156;
   // $FF: renamed from: z net.minecraft.game.animals.i
   public static Item stick;
   // $FF: renamed from: A net.minecraft.game.animals.i
   public static Item bowl;
   // $FF: renamed from: B net.minecraft.game.animals.i
   public static Item field_159;
   // $FF: renamed from: C net.minecraft.game.animals.i
   public static Item field_160;
   // $FF: renamed from: D net.minecraft.game.animals.i
   public static Item field_161;
   // $FF: renamed from: E net.minecraft.game.animals.i
   public static Item field_162;
   // $FF: renamed from: F net.minecraft.game.animals.i
   public static Item field_163;
   // $FF: renamed from: G net.minecraft.game.animals.i
   public static Item some_string;
   // $FF: renamed from: H net.minecraft.game.animals.i
   public static Item feather;
   // $FF: renamed from: I net.minecraft.game.animals.i
   public static Item gun_powder;
   // $FF: renamed from: J net.minecraft.game.animals.i
   public static Item field_167;
   // $FF: renamed from: K net.minecraft.game.animals.i
   public static Item field_168;
   // $FF: renamed from: L net.minecraft.game.animals.i
   public static Item field_169;
   // $FF: renamed from: M net.minecraft.game.animals.i
   public static Item field_170;
   // $FF: renamed from: N net.minecraft.game.animals.i
   public static Item field_171;
   // $FF: renamed from: O net.minecraft.game.animals.i
   public static Item field_172;
   // $FF: renamed from: P net.minecraft.game.animals.i
   public static Item field_173;
   // $FF: renamed from: Q net.minecraft.game.animals.i
   public static Item field_174;
   // $FF: renamed from: R net.minecraft.game.animals.i
   public static Item field_175;
   // $FF: renamed from: S net.minecraft.game.animals.i
   public static Item field_176;
   // $FF: renamed from: T net.minecraft.game.animals.i
   public static Item field_177;
   // $FF: renamed from: U net.minecraft.game.animals.i
   public static Item field_178;
   // $FF: renamed from: V net.minecraft.game.animals.i
   public static Item field_179;
   // $FF: renamed from: W net.minecraft.game.animals.i
   public static Item field_180;
   // $FF: renamed from: X net.minecraft.game.animals.i
   public static Item field_181;
   // $FF: renamed from: Y net.minecraft.game.animals.i
   public static Item field_182;
   // $FF: renamed from: Z net.minecraft.game.animals.i
   public static Item field_183;
   // $FF: renamed from: aa net.minecraft.game.animals.i
   public static Item field_184;
   // $FF: renamed from: ab net.minecraft.game.animals.i
   public static Item field_185;
   // $FF: renamed from: ac net.minecraft.game.animals.i
   public static Item field_186;
   // $FF: renamed from: ad net.minecraft.game.animals.i
   public static Item field_187;
   // $FF: renamed from: ae net.minecraft.game.animals.i
   public static Item field_188;
   // $FF: renamed from: af net.minecraft.game.animals.i
   public static Item field_189;
   // $FF: renamed from: ag net.minecraft.game.animals.i
   public static Item field_190;
   // $FF: renamed from: ah net.minecraft.game.animals.i
   public static Item field_191;
   // $FF: renamed from: ai net.minecraft.game.animals.i
   public static Item field_192;
   // $FF: renamed from: aj net.minecraft.game.animals.i
   public static Item field_193;
   // $FF: renamed from: ak net.minecraft.game.animals.i
   public static Item field_194;
   // $FF: renamed from: al net.minecraft.game.animals.i
   public static Item field_195;
   // $FF: renamed from: am net.minecraft.game.animals.i
   public static Item raw_pork;
   // $FF: renamed from: an net.minecraft.game.animals.i
   public static Item cooked_pork;
   // $FF: renamed from: ao net.minecraft.game.animals.i
   public static Item field_198;
   public static Item stone_rod;
   // $FF: renamed from: ap int
   public final int unique_id;
   // $FF: renamed from: aq int
   protected int stack_size = 64;
   // $FF: renamed from: ar int
   protected int durability = 32;
   // $FF: renamed from: as int
   protected int item_id;

   protected Item(int var1) {
      this.unique_id = var1 + 256;
      if (items[var1 + 256] != null) {
         System.out.println("CONFLICT @ " + var1);
      }

      items[var1 + 256] = this;
   }

   // $FF: renamed from: game (int) net.minecraft.game.animals.i
   public final Item method_83(int var1) {
      this.item_id = var1;
      return this;
   }

   // $FF: renamed from: animals () int
   public final int get_item_id() {
      return this.item_id;
   }

   // $FF: renamed from: game (net.minecraft.game.animals.k, net.minecraft.game.game.player, int, int, int, int) boolean
   public boolean activate_on(ItemStack var1, World var2, int var3, int var4, int var5, int var6) {
      return false;
   }

   // $FF: renamed from: animals (net.minecraft.game.game.animals.x) float
   public float method_86(Block var1) {
      return 1.0F;
   }

   // $FF: renamed from: game (net.minecraft.game.animals.k, net.minecraft.game.game.player, net.minecraft.game.entities.player.game) net.minecraft.game.animals.k
   public ItemStack on_use(ItemStack var1, World var2, Player var3) {
      return var1;
   }

   // $FF: renamed from: entities () int
   public final int get_stack_size() {
      return this.stack_size;
   }

   // $FF: renamed from: projectiles () int
   public final int get_durability() {
      return this.durability;
   }

   // $FF: renamed from: game (net.minecraft.game.animals.k) void
   public void method_90(ItemStack var1) {
   }

   // $FF: renamed from: animals (net.minecraft.game.animals.k) void
   public void method_91(ItemStack var1) {
   }

   // $FF: renamed from: game () int
   public int method_92() {
      return 1;
   }

   // $FF: renamed from: game (net.minecraft.game.game.animals.x) boolean
   public boolean place_block(Block var1) {
      return false;
   }

   static {
      class_41 var10000 = new class_41(0, 2);
      byte var1 = 82;
      class_41 var0 = var10000;
      var10000.item_id = var1;
      field_134 = var0;

      class_40 var15 = new class_40(1, 2);
      var1 = 98;
      class_40 var2 = var15;
      var15.item_id = var1;
      field_135 = var2;


      class_42 var16 = new class_42(2, 2);
      var1 = 114;
      class_42 var3 = var16;
      var16.item_id = var1;
      field_136 = var3;

      FlintAndSteel var17 = new FlintAndSteel(3);
      var1 = 5;
      FlintAndSteel var4 = var17;
      var17.item_id = var1;
      field_137 = var4;

      class_45 var18 = new class_45(4, 4);
      var1 = 4;
      var18.item_id = var1;
      Bow var19 = new Bow(5);
      var1 = 21;
      Bow var5 = var19;
      var19.item_id = var1;
      field_138 = var5;

      Item var20 = new Item(6);
      var1 = 37;
      Item var6 = var20;
      var20.item_id = var1;
      bone = var6;
      var20 = new Item(7);
      var1 = 7;
      var6 = var20;
      var20.item_id = var1;
      field_140 = var6;

      var20 = new Item(8);
      var1 = 55;
      var6 = var20;
      var20.item_id = var1;
      diamond = var6;

      var20 = new Item(9);
      var1 = 23;
      var6 = var20;
      var20.item_id = var1;
      iron_ingot = var6;

      var20 = new Item(10);
      var1 = 39;
      var6 = var20;
      var20.item_id = var1;
      gold_ingot = var6;

      class_33 var21 = new class_33(11, 2);
      var1 = 66;
      class_33 var7 = var21;
      var21.item_id = var1;
      field_144 = var7;

      var21 = new class_33(12, 0);
      var1 = 64;
      var7 = var21;
      var21.item_id = var1;
      field_145 = var7;

      var10000 = new class_41(13, 0);
      var1 = 80;
      var0 = var10000;
      var10000.item_id = var1;
      field_146 = var0;

      var15 = new class_40(14, 0);
      var1 = 96;
      var2 = var15;
      var15.item_id = var1;
      field_147 = var2;

      var16 = new class_42(15, 0);
      var1 = 112;
      var3 = var16;
      var16.item_id = var1;
      field_148 = var3;

      var21 = new class_33(16, 1);
      var1 = 65;
      var7 = var21;
      var21.item_id = var1;
      field_149 = var7;

      var10000 = new class_41(17, 1);
      var1 = 81;
      var0 = var10000;
      var10000.item_id = var1;
      field_150 = var0;

      var15 = new class_40(18, 1);
      var1 = 97;
      var2 = var15;
      var15.item_id = var1;
      field_151 = var2;

      var16 = new class_42(19, 1);
      var1 = 113;
      var3 = var16;
      var16.item_id = var1;
      field_152 = var3;

      var21 = new class_33(20, 3);
      var1 = 67;
      var7 = var21;
      var21.item_id = var1;
      field_153 = var7;

      var10000 = new class_41(21, 3);
      var1 = 83;
      var0 = var10000;
      var10000.item_id = var1;
      field_154 = var0;

      var15 = new class_40(22, 3);
      var1 = 99;
      var2 = var15;
      var15.item_id = var1;
      field_155 = var2;

      var16 = new class_42(23, 3);
      var1 = 115;
      var3 = var16;
      var16.item_id = var1;
      field_156 = var3;

      var20 = new Item(24);
      var1 = 53;
      var6 = var20;
      var20.item_id = var1;
      stick = var6;

      var20 = new Item(25);
      var1 = 71;
      var6 = var20;
      var20.item_id = var1;
      bowl = var6;

      bowl_of_soup var22 = new bowl_of_soup(26, 10);
      var1 = 72;
      bowl_of_soup var8 = var22;
      var22.item_id = var1;
      field_159 = var8;

      var21 = new class_33(27, 0);
      var1 = 68;
      var7 = var21;
      var21.item_id = var1;
      field_160 = var7;

      var10000 = new class_41(28, 0);
      var1 = 84;
      var0 = var10000;
      var10000.item_id = var1;
      field_161 = var0;

      var15 = new class_40(29, 0);
      var1 = 100;
      var2 = var15;
      var15.item_id = var1;
      field_162 = var2;

      var16 = new class_42(30, 0);
      var1 = 116;
      var3 = var16;
      var16.item_id = var1;
      field_163 = var3;

      var20 = new Item(31);
      var1 = 8;
      var6 = var20;
      var20.item_id = var1;
      some_string = var6;

      var20 = new Item(32);
      var1 = 24;
      var6 = var20;
      var20.item_id = var1;
      feather = var6;

      var20 = new Item(33);
      var1 = 40;
      var6 = var20;
      var20.item_id = var1;
      gun_powder = var6;

      class_38 var23 = new class_38(34, 0);
      short var10 = 128;
      class_38 var9 = var23;
      var23.item_id = var10;
      field_167 = var9;

      var23 = new class_38(35, 1);
      var10 = 129;
      var9 = var23;
      var23.item_id = var10;
      field_168 = var9;

      var23 = new class_38(36, 2);
      var10 = 130;
      var9 = var23;
      var23.item_id = var10;
      field_169 = var9;

      var23 = new class_38(37, 3);
      var10 = 131;
      var9 = var23;
      var23.item_id = var10;
      field_170 = var9;

      var23 = new class_38(38, 4);
      var10 = 132;
      var9 = var23;
      var23.item_id = var10;
      field_171 = var9;

      class_36 var24 = new class_36(39, Block.field_926.block_id);
      var1 = 9;
      class_36 var11 = var24;
      var24.item_id = var1;
      field_172 = var11;

      var20 = new Item(40);
      var1 = 25;
      var6 = var20;
      var20.item_id = var1;
      field_173 = var6;

      var18 = new class_45(41, 5);
      var1 = 41;
      class_45 var12 = var18;
      var18.item_id = var1;
      field_174 = var12;

      Armor var25 = new Armor(42, 0, 0, 0);
      var1 = 0;
      Armor var13 = var25;
      var25.item_id = var1;
      field_175 = var13;

      var25 = new Armor(43, 0, 0, 1);
      var1 = 16;
      var13 = var25;
      var25.item_id = var1;
      field_176 = var13;

      var25 = new Armor(44, 0, 0, 2);
      var1 = 32;
      var13 = var25;
      var25.item_id = var1;
      field_177 = var13;

      var25 = new Armor(45, 0, 0, 3);
      var1 = 48;
      var13 = var25;
      var25.item_id = var1;
      field_178 = var13;

      var25 = new Armor(46, 1, 1, 0);
      var1 = 1;
      var13 = var25;
      var25.item_id = var1;
      field_179 = var13;

      var25 = new Armor(47, 1, 1, 1);
      var1 = 17;
      var13 = var25;
      var25.item_id = var1;
      field_180 = var13;

      var25 = new Armor(48, 1, 1, 2);
      var1 = 33;
      var13 = var25;
      var25.item_id = var1;
      field_181 = var13;

      var25 = new Armor(49, 1, 1, 3);
      var1 = 49;
      var13 = var25;
      var25.item_id = var1;
      field_182 = var13;

      var25 = new Armor(50, 2, 2, 0);
      var1 = 2;
      var13 = var25;
      var25.item_id = var1;
      field_183 = var13;

      var25 = new Armor(51, 2, 2, 1);
      var1 = 18;
      var13 = var25;
      var25.item_id = var1;
      field_184 = var13;

      var25 = new Armor(52, 2, 2, 2);
      var1 = 34;
      var13 = var25;
      var25.item_id = var1;
      field_185 = var13;

      var25 = new Armor(53, 2, 2, 3);
      var1 = 50;
      var13 = var25;
      var25.item_id = var1;
      field_186 = var13;

      var25 = new Armor(54, 3, 3, 0);
      var1 = 3;
      var13 = var25;
      var25.item_id = var1;
      field_187 = var13;

      var25 = new Armor(55, 3, 3, 1);
      var1 = 19;
      var13 = var25;
      var25.item_id = var1;
      field_188 = var13;

      var25 = new Armor(56, 3, 3, 2);
      var1 = 35;
      var13 = var25;
      var25.item_id = var1;
      field_189 = var13;

      var25 = new Armor(57, 3, 3, 3);
      var1 = 51;
      var13 = var25;
      var25.item_id = var1;
      field_190 = var13;

      var25 = new Armor(58, 1, 4, 0);
      var1 = 4;
      var13 = var25;
      var25.item_id = var1;
      field_191 = var13;

      var25 = new Armor(59, 1, 4, 1);
      var1 = 20;
      var13 = var25;
      var25.item_id = var1;
      field_192 = var13;

      var25 = new Armor(60, 1, 4, 2);
      var1 = 36;
      var13 = var25;
      var25.item_id = var1;
      field_193 = var13;

      var25 = new Armor(61, 1, 4, 3);
      var1 = 52;
      var13 = var25;
      var25.item_id = var1;
      field_194 = var13;

      var20 = new Item(62);
      var1 = 6;
      var6 = var20;
      var20.item_id = var1;
      field_195 = var6;

      var18 = new class_45(63, 3);
      var1 = 87;
      var12 = var18;
      var18.item_id = var1;
      raw_pork = var12;
      var18 = new class_45(64, 8);
      var1 = 88;
      var12 = var18;
      var18.item_id = var1;
      cooked_pork = var12;

      class_44 var26 = new class_44(65);
      var1 = 26;
      class_44 var14 = var26;
      var26.item_id = var1;
      field_198 = var14;

      StoneRod srod = new StoneRod( 66 );
      srod.item_id = 53;
      stone_rod = srod;
   }
}
