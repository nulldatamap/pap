package net.minecraft.game.items.a;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.items.Item;

// $FF: renamed from: net.minecraft.game.animals.game.particles
public final class Recipes {
   // $FF: renamed from: game net.minecraft.game.animals.game.particles
   private static final Recipes field_1045 = new Recipes();
   // $FF: renamed from: animals java.util.List
   private List field_1046 = new ArrayList();

   // $FF: renamed from: game () net.minecraft.game.animals.game.particles
   public static final Recipes method_596() {
      return field_1045;
   }

   private Recipes() {
      (new class_238()).method_599(this);
      (new class_235()).method_595(this);
      (new class_229()).method_584(this);
      new class_231();
      this.add_recipes(new ItemStack(Item.field_159), "Y", "X", "#", 'X', Block.field_906, 'Y', Block.field_907, '#', Item.bowl);
      this.add_recipes(new ItemStack(Item.field_159), "Y", "X", "#", 'X', Block.field_907, 'Y', Block.field_906, '#', Item.bowl);
      new class_236();
      this.add_recipes(new ItemStack(Block.field_921), "###", "# #", "###", '#', Block.wood_planks);
      this.add_recipes(new ItemStack(Block.field_928), "###", "# #", "###", '#', Block.cobble_stone);
      this.add_recipes(new ItemStack(Block.field_925), "##", "##", '#', Block.wood_planks);
      (new class_232()).method_589(this);
      this.add_recipes(new ItemStack(Block.wool, 1), "###", "###", "###", '#', Item.some_string);
      this.add_recipes(new ItemStack(Block.tnt, 1), "X#X", "#X#", "X#X", 'X', Item.gun_powder, '#', Block.sand);
      this.add_recipes(new ItemStack(Block.field_911, 3), "###", '#', Block.cobble_stone);
      this.add_recipes(new ItemStack(Block.wood_planks, 4), "#", '#', Block.field_884);
      this.add_recipes(new ItemStack(Item.stick, 4), "#", "#", '#', Block.wood_planks);
      this.add_recipes(new ItemStack(Block.field_917, 4), "X", "#", 'X', Item.field_140, '#', Item.stick);
      this.add_recipes(new ItemStack(Item.bowl, 4), "# #", " # ", '#', Block.wood_planks);
      this.add_recipes(new ItemStack(Item.field_137, 1), "A ", " B", 'A', Item.iron_ingot, 'B', Item.field_195);
      this.add_recipes(new ItemStack(Item.field_174, 1), "###", '#', Item.field_173);
      this.add_recipes(new ItemStack(Item.field_198, 1), "###", "#X#", "###", '#', Block.wood_planks, 'X', Block.wool);
      this.add_recipes(new ItemStack(Item.stone_rod, 1), "#", "#", '#', Block.cobble_stone);
      Collections.sort(this.field_1046, new class_234(this));
      System.out.println(this.field_1046.size() + " recipes");
   }

   // $FF: renamed from: game (net.minecraft.game.animals.k, java.lang.Object[]) void
   final void add_recipes(ItemStack var1, Object... var2) {
      String var3 = "";
      int var4 = 0;
      int var5 = 0;
      int var6 = 0;
      if (var2[0] instanceof String[]) {
         ++var4;
         String[] var11 = (String[])var2[0];

         for(int var8 = 0; var8 < var11.length; ++var8) {
            String var9 = var11[var8];
            ++var6;
            var5 = var9.length();
            var3 = var3 + var9;
         }
      } else {
         while(var2[var4] instanceof String) {
            String var7 = (String)var2[var4++];
            ++var6;
            var5 = var7.length();
            var3 = var3 + var7;
         }
      }

      HashMap var12;
      int var15;
      for(var12 = new HashMap(); var4 < var2.length; var4 += 2) {
         Character var13 = (Character)var2[var4];
         var15 = 0;
         if (var2[var4 + 1] instanceof Item) {
            var15 = ((Item)var2[var4 + 1]).unique_id;
         } else if (var2[var4 + 1] instanceof Block) {
            var15 = ((Block)var2[var4 + 1]).block_id;
         }

         var12.put(var13, var15);
      }

      int[] var14 = new int[var5 * var6];

      for(var15 = 0; var15 < var5 * var6; ++var15) {
         char var10 = var3.charAt(var15);
         if (var12.containsKey(var10)) {
            var14[var15] = ((Integer)var12.get(var10)).intValue();
         } else {
            var14[var15] = -1;
         }
      }

      this.field_1046.add(new class_230(var5, var6, var14, var1));
   }

   // $FF: renamed from: game (int[]) net.minecraft.game.animals.k
   public final ItemStack method_598(int[] var1) {
      for(int var2 = 0; var2 < this.field_1046.size(); ++var2) {
         class_230 var3;
         if ((var3 = (class_230)this.field_1046.get(var2)).method_585(var1)) {
            return var3.method_587();
         }
      }

      return null;
   }
}
