package net.minecraft.game.items.a;

import net.minecraft.game.world.block.Block;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.items.Item;

// $FF: renamed from: net.minecraft.game.animals.game.projectiles
public final class class_229 {
   // $FF: renamed from: game java.lang.Object[][]
   private Object[][] field_1032;

   public class_229() {
      this.field_1032 = new Object[][]{{Block.field_908, Item.gold_ingot}, {Block.field_909, Item.iron_ingot}, {Block.field_924, Item.diamond}};
   }

   // $FF: renamed from: game (net.minecraft.game.animals.game.particles) void
   public final void method_584(Recipes var1) {
      for(int var2 = 0; var2 < this.field_1032.length; ++var2) {
         Block var3 = (Block)this.field_1032[var2][0];
         Item var4 = (Item)this.field_1032[var2][1];
         var1.add_recipes(new ItemStack(var3), "###", "###", "###", '#', var4);
         var1.add_recipes(new ItemStack(var4, 9), "#", '#', var3);
      }

   }
}
