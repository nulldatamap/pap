package net.minecraft.game.items.a;

import java.util.Comparator;

// $FF: renamed from: net.minecraft.game.animals.game.i
final class class_234 implements Comparator {
   class_234(Recipes var1) {
   }

   public final int compare(Object obj, Object obj1) {
      if(((class_230) (obj1)).method_588() < ((class_230) (obj)).method_588()) {
         return -1;
      }
      return ((class_230) (obj1)).method_588() <= ((class_230) (obj)).method_588() ? 0 : 1;
   }
}
