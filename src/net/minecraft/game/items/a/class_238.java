package net.minecraft.game.items.a;

import net.minecraft.game.world.block.Block;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.items.Item;

// $FF: renamed from: net.minecraft.game.animals.game.player
public final class class_238 {
   // $FF: renamed from: game java.lang.String[][]
   private String[][] field_1047 = new String[][]{{"XXX", " # ", " # "}, {"X", "#", "#"}, {"XX", "X#", " #"}, {"XX", " #", " #"}};
   // $FF: renamed from: animals java.lang.Object[][]
   private Object[][] field_1048;

   public class_238() {
      this.field_1048 = new Object[][]{{Block.wood_planks, Block.cobble_stone, Item.iron_ingot, Item.diamond, Item.gold_ingot}, {Item.field_147, Item.field_151, Item.field_135, Item.field_155, Item.field_162}, {Item.field_146, Item.field_150, Item.field_134, Item.field_154, Item.field_161}, {Item.field_148, Item.field_152, Item.field_136, Item.field_156, Item.field_163}, {Item.field_167, Item.field_168, Item.field_169, Item.field_170, Item.field_171}};
   }

   // $FF: renamed from: game (net.minecraft.game.animals.game.particles) void
   public final void method_599(Recipes var1) {
      for(int var2 = 0; var2 < this.field_1048[0].length; ++var2) {
         Object var3 = this.field_1048[0][var2];

         for(int var4 = 0; var4 < this.field_1048.length - 1; ++var4) {
            Item var5 = (Item)this.field_1048[var4 + 1][var2];
            var1.add_recipes(new ItemStack(var5), this.field_1047[var4], '#', Item.stick, 'X', var3);
         }
      }

   }
}
