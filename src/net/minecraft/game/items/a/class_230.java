package net.minecraft.game.items.a;

import net.minecraft.game.items.ItemStack;

// $FF: renamed from: net.minecraft.game.animals.game.entities
public final class class_230 {
   // $FF: renamed from: game int
   private int field_1033;
   // $FF: renamed from: animals int
   private int field_1034;
   // $FF: renamed from: entities int[]
   private int[] field_1035;
   // $FF: renamed from: projectiles net.minecraft.game.animals.k
   private ItemStack field_1036;

   public class_230(int var1, int var2, int[] var3, ItemStack var4) {
      this.field_1033 = var1;
      this.field_1034 = var2;
      this.field_1035 = var3;
      this.field_1036 = var4;
   }

   // $FF: renamed from: game (int[]) boolean
   public final boolean method_585(int[] var1) {
      for(int var2 = 0; var2 <= 3 - this.field_1033; ++var2) {
         for(int var3 = 0; var3 <= 3 - this.field_1034; ++var3) {
            if (this.method_586(var1, var2, var3, true)) {
               return true;
            }

            if (this.method_586(var1, var2, var3, false)) {
               return true;
            }
         }
      }

      return false;
   }

   // $FF: renamed from: game (int[], int, int, boolean) boolean
   private boolean method_586(int[] var1, int var2, int var3, boolean var4) {
      for(int var5 = 0; var5 < 3; ++var5) {
         for(int var6 = 0; var6 < 3; ++var6) {
            int var7 = var5 - var2;
            int var8 = var6 - var3;
            int var9 = -1;
            if (var7 >= 0 && var8 >= 0 && var7 < this.field_1033 && var8 < this.field_1034) {
               if (var4) {
                  var9 = this.field_1035[this.field_1033 - var7 - 1 + var8 * this.field_1033];
               } else {
                  var9 = this.field_1035[var7 + var8 * this.field_1033];
               }
            }

            if (var1[var5 + var6 * 3] != var9) {
               return false;
            }
         }
      }

      return true;
   }

   // $FF: renamed from: game () net.minecraft.game.animals.k
   public final ItemStack method_587() {
      return new ItemStack(this.field_1036.id, this.field_1036.count);
   }

   // $FF: renamed from: animals () int
   public final int method_588() {
      return this.field_1033 * this.field_1034;
   }
}
