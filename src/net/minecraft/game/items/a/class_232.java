package net.minecraft.game.items.a;

import net.minecraft.game.world.block.Block;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.items.Item;

// $FF: renamed from: net.minecraft.game.animals.game.game
public final class class_232 {
   // $FF: renamed from: game java.lang.String[][]
   private String[][] field_1037 = new String[][]{{"XXX", "X X"}, {"X X", "XXX", "XXX"}, {"XXX", "X X", "X X"}, {"X X", "X X"}};
   // $FF: renamed from: animals java.lang.Object[][]
   private Object[][] field_1038;

   public class_232() {
      this.field_1038 = new Object[][]{{Block.wool, Block.fire, Item.iron_ingot, Item.diamond, Item.gold_ingot}, {Item.field_175, Item.field_179, Item.field_183, Item.field_187, Item.field_191}, {Item.field_176, Item.field_180, Item.field_184, Item.field_188, Item.field_192}, {Item.field_177, Item.field_181, Item.field_185, Item.field_189, Item.field_193}, {Item.field_178, Item.field_182, Item.field_186, Item.field_190, Item.field_194}};
   }

   // $FF: renamed from: game (net.minecraft.game.animals.game.particles) void
   public final void method_589(Recipes var1) {
      for(int var2 = 0; var2 < this.field_1038[0].length; ++var2) {
         Object var3 = this.field_1038[0][var2];

         for(int var4 = 0; var4 < this.field_1038.length - 1; ++var4) {
            Item var5 = (Item)this.field_1038[var4 + 1][var2];
            var1.add_recipes(new ItemStack(var5), this.field_1037[var4], 'X', var3);
         }
      }

   }
}
