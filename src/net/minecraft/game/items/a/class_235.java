package net.minecraft.game.items.a;

import net.minecraft.game.world.block.Block;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.items.Item;

// $FF: renamed from: net.minecraft.game.animals.game.h
public final class class_235 {
   // $FF: renamed from: game java.lang.String[][]
   private String[][] field_1043 = new String[][]{{"X", "X", "#"}};
   // $FF: renamed from: animals java.lang.Object[][]
   private Object[][] field_1044;

   public class_235() {
      this.field_1044 = new Object[][]{{Block.wood_planks, Block.cobble_stone, Item.iron_ingot, Item.diamond, Item.gold_ingot}, {Item.field_145, Item.field_149, Item.field_144, Item.field_153, Item.field_160}};
   }

   // $FF: renamed from: game (net.minecraft.game.animals.game.particles) void
   public final void method_595(Recipes var1) {
      for(int var2 = 0; var2 < this.field_1044[0].length; ++var2) {
         Object var3 = this.field_1044[0][var2];

         for(int var4 = 0; var4 < this.field_1044.length - 1; ++var4) {
            Item var5 = (Item)this.field_1044[var4 + 1][var2];
            var1.add_recipes(new ItemStack(var5), this.field_1043[var4], '#', Item.stick, 'X', var3);
         }
      }

      var1.add_recipes(new ItemStack(Item.field_138, 1), " #X", "# X", " #X", 'X', Item.some_string, '#', Item.stick);
      var1.add_recipes(new ItemStack(Item.bone, 4), "X", "#", "Y", 'Y', Item.feather, 'X', Item.iron_ingot, '#', Item.stick);
   }
}
