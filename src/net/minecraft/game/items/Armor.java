package net.minecraft.game.items;

// $FF: renamed from: net.minecraft.game.animals.j
public final class Armor extends Item {
   // $FF: renamed from: av int[]
   private static final int[] field_213 = new int[]{3, 8, 6, 3};
   // $FF: renamed from: aw int[]
   private static final int[] material_durabilities = new int[]{11, 16, 15, 13};
   // $FF: renamed from: as int
   public final int field_215;
   // $FF: renamed from: at int
   public final int field_216;
   // $FF: renamed from: au int
   public final int field_217;

   public Armor(int var1, int var2, int var3, int material_id) {
      super(var1);
      this.field_215 = material_id;
      this.field_217 = var3;
      this.field_216 = field_213[material_id];
      this.durability = material_durabilities[material_id] * 3 << var2;
      this.stack_size = 1;
   }
}
