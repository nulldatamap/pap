package net.minecraft.game.items;

import com.data.nbt.CompoundTag;
import net.minecraft.game.world.block.Block;

// $FF: renamed from: net.minecraft.game.animals.k
public final class ItemStack {
   // $FF: renamed from: game int
   public int count;
   // $FF: renamed from: animals int
   public int pickup_cooldown;
   // $FF: renamed from: entities int
   public int id;
   // $FF: renamed from: projectiles int
   public int damage;

   public ItemStack(Block var1) {
      this((Block)var1, 1);
   }

   public ItemStack(Block var1, int var2) {
      this(var1.block_id, var2);
   }

   public ItemStack(Item var1) {
      this((Item)var1, 1);
   }

   public ItemStack(Item var1, int var2) {
      this(var1.unique_id, var2);
   }

   public ItemStack(int var1) {
      this(var1, 1);
   }

   public ItemStack(int id, int count) {
      this.count = 0;
      this.id = id;
      this.count = count;
   }

   public ItemStack(int var1, int var2, int var3) {
      this.count = 0;
      this.id = var1;
      this.count = var2;
      this.damage = var3;
   }

   public ItemStack(CompoundTag tag) {
      this.count = 0;
      this.id = tag.read_short("id");
      this.count = tag.read_byte("Count");
      this.damage = tag.read_short("Damage");
   }

   // $FF: renamed from: game (int) net.minecraft.game.animals.k
   public final ItemStack split_stack(int amount) {
      this.count -= amount;
      return new ItemStack(this.id, amount, this.damage);
   }

   // $FF: renamed from: game () net.minecraft.game.animals.i
   public final Item item() {
      return Item.items[this.id];
   }

   // $FF: renamed from: game (com.game.game.entities) com.game.game.entities
   public final CompoundTag method_592(CompoundTag tag) {
      tag.write_short("id", (short)this.id);
      tag.write_byte("Count", (byte)this.count);
      tag.write_short("Damage", (short)this.damage);
      return tag;
   }

   // $FF: renamed from: animals () int
   public final int durability() {
      return Item.items[this.id].get_durability();
   }

   // $FF: renamed from: animals (int) void
   public final void take_damage(int dmg) {
      this.damage += dmg;
      if (this.damage > this.durability()) {
         --this.count;
         if (this.count < 0) {
            this.count = 0;
         }

         this.damage = 0;
      }

   }
}
