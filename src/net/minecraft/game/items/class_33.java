package net.minecraft.game.items;

import net.minecraft.game.world.block.Block;

// $FF: renamed from: net.minecraft.game.animals.entities
public final class class_33 extends Item {
   // $FF: renamed from: as int
   private int field_203;

   public class_33(int var1, int var2) {
      super(var1);
      this.stack_size = 1;
      this.durability = 32 << var2;
      this.field_203 = 4 + (var2 << 1);
   }

   // $FF: renamed from: animals (net.minecraft.game.game.animals.x) float
   public final float method_86(Block var1) {
      return 1.5F;
   }

   // $FF: renamed from: game (net.minecraft.game.animals.k) void
   public final void method_90(ItemStack var1) {
      var1.take_damage(1);
   }

   // $FF: renamed from: animals (net.minecraft.game.animals.k) void
   public final void method_91(ItemStack var1) {
      var1.take_damage(2);
   }

   // $FF: renamed from: game () int
   public final int method_92() {
      return this.field_203;
   }
}
