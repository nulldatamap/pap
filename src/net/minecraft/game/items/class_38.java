package net.minecraft.game.items;

import net.minecraft.game.world.World;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.world.block.SoundMaterial;
import net.minecraft.game.entities.misc.ItemDrop;

// $FF: renamed from: net.minecraft.game.animals.n
public final class class_38 extends Item {
   public class_38(int var1, int var2) {
      super(var1);
      this.stack_size = 1;
      this.durability = 32 << var2;
   }

   // $FF: renamed from: game (net.minecraft.game.animals.k, net.minecraft.game.game.player, int, int, int, int) boolean
   public final boolean activate_on(ItemStack var1, World var2, int var3, int var4, int var5, int var6) {
      if (var3 > 0 && var4 > 0 && var5 > 0 && var3 < var2.width - 1 && var4 < var2.height - 1 && var5 < var2.depth - 1) {
         var6 = var2.block_at(var3, var4, var5);
         if ((var2.method_198(var3, var4 + 1, var5).method_345() || var6 != Block.grass.block_id) && var6 != Block.dirt.block_id) {
            return false;
         } else {
            Block var7 = Block.field_927;
            float var10001 = (float)var3 + 0.5F;
            float var10002 = (float)var4 + 0.5F;
            float var10003 = (float)var5 + 0.5F;
            String var10004 = var7.sound_material.step_sound();
            SoundMaterial var8 = var7.sound_material;
            float var10005 = (var7.sound_material.field_958 + 1.0F) / 2.0F;
            var8 = var7.sound_material;
            var2.method_212(var10001, var10002, var10003, var10004, var10005, var7.sound_material.field_959 * 0.8F);
            var2.method_165(var3, var4, var5, var7.block_id);
            var1.take_damage(1);
            if (var2.random_a.nextInt(8) == 0 && var6 == Block.grass.block_id) {
               for(int var9 = 0; var9 <= 0; ++var9) {
                  float var10 = var2.random_a.nextFloat() * 0.7F + 0.15F;
                  float var12 = var2.random_a.nextFloat() * 0.7F + 0.15F;
                  ItemDrop var11;
                  (var11 = new ItemDrop(var2, (float)var3 + var10, (float)var4 + 1.2F, (float)var5 + var12, new ItemStack(Item.field_172))).pickup_cooldown = 10;
                  var2.add_entity(var11);
               }
            }

            return true;
         }
      } else {
         return false;
      }
   }
}
