package net.minecraft.game.items;

import net.minecraft.game.world.World;

public class StoneRod extends Item {
    public StoneRod( int id ) {
        super( id );
        this.stack_size = 64;
    }

    public boolean activate_on(ItemStack stack, World world, int x, int y, int z, int dir) {
        if( world.block_at( x, y, z ) != 0 ) {
            world.make_block_fall( x, y, z );
            return true;
        }

        return false;
    }
}
