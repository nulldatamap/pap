package net.minecraft.game.items;

import net.minecraft.game.world.World;
import net.minecraft.game.entities.Painting;

// $FF: renamed from: net.minecraft.game.animals.o
public final class class_44 extends Item {
   public class_44(int var1) {
      super(65);
      this.durability = 64;
   }

   // $FF: renamed from: game (net.minecraft.game.animals.k, net.minecraft.game.game.player, int, int, int, int) boolean
   public final boolean activate_on(ItemStack var1, World var2, int var3, int var4, int var5, int var6) {
      if (var6 == 0) {
         return false;
      } else if (var6 == 1) {
         return false;
      } else if (var3 > 0 && var4 > 0 && var5 > 0 && var3 < var2.width - 1 && var4 < var2.height - 1 && var5 < var2.depth - 1) {
         byte var7 = 0;
         if (var6 == 4) {
            var7 = 1;
         }

         if (var6 == 3) {
            var7 = 2;
         }

         if (var6 == 5) {
            var7 = 3;
         }

         Painting var8;
         if ((var8 = new Painting(var2, var3, var4, var5, var7)).method_56()) {
            var2.add_entity(var8);
            --var1.count;
         }

         return true;
      } else {
         return false;
      }
   }
}
