package net.minecraft.game.items;

import net.minecraft.game.world.World;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.world.block.SoundMaterial;
import net.minecraft.game.util.AABB;

// $FF: renamed from: net.minecraft.game.animals.projectiles
public final class class_34 extends Item {
   // $FF: renamed from: as int
   private int field_204;

   public class_34(int var1) {
      super(var1);
      this.field_204 = var1 + 256;
      this.method_83(Block.blocks[var1 + 256].method_500(2));
   }

   // $FF: renamed from: game (net.minecraft.game.animals.k, net.minecraft.game.game.player, int, int, int, int) boolean
   public final boolean activate_on(ItemStack var1, World var2, int var3, int var4, int var5, int var6) {
      if (var6 == 0) {
         --var4;
      }

      if (var6 == 1) {
         ++var4;
      }

      if (var6 == 2) {
         --var5;
      }

      if (var6 == 3) {
         ++var5;
      }

      if (var6 == 4) {
         --var3;
      }

      if (var6 == 5) {
         ++var3;
      }

      if (var1.count == 0) {
         return false;
      } else if (var3 > 0 && var4 > 0 && var5 > 0 && var3 < var2.width - 1 && var4 < var2.height - 1 && var5 < var2.depth - 1) {
         int var7 = var2.block_at(var3, var4, var5);
         Block var10 = Block.blocks[var7];
         if (this.field_204 > 0 && var10 == null || var10 == Block.water || var10 == Block.infinite_water || var10 == Block.lava || var10 == Block.infinite_lava || var10 == Block.fire) {
            AABB var8 = (var10 = Block.blocks[this.field_204]).method_502(var3, var4, var5);
            if (var2.method_188(var8) && var10.method_524(var2, var3, var4, var5) && var2.method_165(var3, var4, var5, this.field_204)) {
               Block.blocks[this.field_204].method_527(var2, var3, var4, var5, var6);
               float var10001 = (float)var3 + 0.5F;
               float var10002 = (float)var4 + 0.5F;
               float var10003 = (float)var5 + 0.5F;
               String var10004 = var10.sound_material.step_sound();
               SoundMaterial var9 = var10.sound_material;
               float var10005 = (var10.sound_material.field_958 + 1.0F) / 2.0F;
               var9 = var10.sound_material;
               var2.method_212(var10001, var10002, var10003, var10004, var10005, var10.sound_material.field_959 * 0.8F);
               --var1.count;
            }
         }

         return true;
      } else {
         return false;
      }
   }
}
