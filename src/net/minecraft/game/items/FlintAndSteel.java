package net.minecraft.game.items;

import net.minecraft.game.world.World;
import net.minecraft.game.world.block.Block;

// $FF: renamed from: net.minecraft.game.animals.m
public final class FlintAndSteel extends Item {
   public FlintAndSteel(int var1) {
      super(3);
      this.stack_size = 1;
      this.durability = 64;
   }

   // $FF: renamed from: game (net.minecraft.game.animals.k, net.minecraft.game.game.player, int, int, int, int) boolean
   public final boolean activate_on(ItemStack var1, World world, int x, int y, int z, int direction) {
      if (direction == 0) {
         --y;
      }

      if (direction == 1) {
         ++y;
      }

      if (direction == 2) {
         --z;
      }

      if (direction == 3) {
         ++z;
      }

      if (direction == 4) {
         --x;
      }

      if (direction == 5) {
         ++x;
      }

      if (x > 0 && y > 0 && z > 0 && x < world.width - 1 && y < world.height - 1 && z < world.depth - 1) {
         if (world.block_at(x, y, z) == 0) {
            world.method_212((float)x + 0.5F, (float)y + 0.5F, (float)z + 0.5F, "fire.ignite", 1.0F, field_132.nextFloat() * 0.4F + 0.8F);
            world.method_165(x, y, z, Block.fire.block_id);
         }

         var1.take_damage(1);
         return true;
      } else {
         return false;
      }
   }
}
