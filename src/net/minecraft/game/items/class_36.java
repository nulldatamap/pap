package net.minecraft.game.items;

import net.minecraft.game.world.World;
import net.minecraft.game.world.block.Block;

// $FF: renamed from: net.minecraft.game.animals.particles
public final class class_36 extends Item {
   // $FF: renamed from: as int
   private int field_205;

   public class_36(int var1, int var2) {
      super(39);
      this.field_205 = var2;
   }

   // $FF: renamed from: game (net.minecraft.game.animals.k, net.minecraft.game.game.player, int, int, int, int) boolean
   public final boolean activate_on(ItemStack var1, World var2, int var3, int var4, int var5, int var6) {
      if (var6 != 1) {
         return false;
      } else if (var3 > 0 && var4 > 0 && var5 > 0 && var3 < var2.width - 1 && var4 < var2.height - 1 && var5 < var2.depth - 1) {
         if (var2.block_at(var3, var4, var5) == Block.field_927.block_id) {
            var2.method_165(var3, var4 + 1, var5, this.field_205);
            --var1.count;
            return true;
         } else {
            return false;
         }
      } else {
         return false;
      }
   }
}
