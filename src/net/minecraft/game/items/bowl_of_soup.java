package net.minecraft.game.items;

import net.minecraft.game.world.World;
import net.minecraft.game.entities.player.Player;

// $FF: renamed from: net.minecraft.game.animals.animals
public final class bowl_of_soup extends class_45 {
   public bowl_of_soup(int var1, int var2) {
      super(26, 10);
   }

   // $FF: renamed from: game (net.minecraft.game.animals.k, net.minecraft.game.game.player, net.minecraft.game.entities.player.game) net.minecraft.game.animals.k
   public final ItemStack on_use(ItemStack var1, World var2, Player var3) {
      super.on_use(var1, var2, var3);
      return new ItemStack(Item.bowl);
   }
}
