package net.minecraft.game.items;

import net.minecraft.game.world.block.Block;
import net.minecraft.game.world.material.Material;

// $FF: renamed from: net.minecraft.game.animals.game
public final class class_40 extends class_39 {
   // $FF: renamed from: as net.minecraft.game.game.animals.x[]
   private static Block[] field_209;
   // $FF: renamed from: at int
   private int field_210;

   public class_40(int var1, int var2) {
      super(var1, 2, var2, field_209);
      this.field_210 = var2;
   }

   // $FF: renamed from: game (net.minecraft.game.game.animals.x) boolean
   public final boolean place_block(Block var1) {
      if (var1 == Block.field_916) {
         return this.field_210 == 3;
      } else if (var1 != Block.field_924 && var1 != Block.diamond) {
         if (var1 != Block.field_908 && var1 != Block.gold) {
            if (var1 != Block.field_909 && var1 != Block.iron) {
               if (var1.material == Material.stone_material) {
                  return true;
               } else {
                  return var1.material == Material.field_612;
               }
            } else {
               return this.field_210 > 0;
            }
         } else {
            return this.field_210 >= 2;
         }
      } else {
         return this.field_210 >= 2;
      }
   }

   static {
      field_209 = new Block[]{Block.cobble_stone, Block.field_910, Block.field_911, Block.stone, Block.field_915, Block.iron, Block.field_909, Block.coal, Block.field_908, Block.gold, Block.diamond, Block.field_924};
   }
}
