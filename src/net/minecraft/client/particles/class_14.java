package net.minecraft.client.particles;

import net.minecraft.game.world.World;
import net.minecraft.game.entities.Mob;
import net.minecraft.game.entities.Entity;
import net.minecraft.client.rendering.GeometryDrawer;
import net.minecraft.client.rendering.models.ModelManager;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.particles.player
public final class class_14 extends Particle {
   // $FF: renamed from: Z net.minecraft.game.entities.animals
   private Entity field_76;
   // $FF: renamed from: aa net.minecraft.game.entities.player
   private Mob field_77;
   // $FF: renamed from: ab int
   private int field_78 = 0;
   // $FF: renamed from: ac int
   private int field_79 = 0;
   // $FF: renamed from: ad float
   private float field_80;

   public class_14(World var1, Entity var2, Mob var3, float var4) {
      super(var1, var2.pos_x, var2.pos_y, var2.pos_z, var2.vel_x, var2.vel_y, var2.vel_z);
      this.field_76 = var2;
      this.field_77 = var3;
      this.field_79 = 3;
      this.field_80 = -0.5F;
   }

   // $FF: renamed from: game (net.minecraft.client.game.projectiles, float, float, float, float, float, float) void
   public final void render_particle(GeometryDrawer drawer, float dt, float var3, float var4, float var5, float var6, float var7) {
      float var9 = (var9 = ((float)this.field_78 + dt) / (float)this.field_79) * var9;
      var3 = this.field_76.pos_x;
      var4 = this.field_76.pos_y;
      var5 = this.field_76.pos_z;
      var6 = this.field_77.chunk_x + (this.field_77.pos_x - this.field_77.chunk_x) * dt;
      var7 = this.field_77.chunk_y + (this.field_77.pos_y - this.field_77.chunk_y) * dt + this.field_80;
      float var8 = this.field_77.chunk_z + (this.field_77.pos_z - this.field_77.chunk_z) * dt;
      var3 += (var6 - var3) * var9;
      var4 += (var7 - var4) * var9;
      var9 = var5 + (var8 - var5) * var9;
      GL11.glColor4f(var5 = this.world.light_at((int)var3, (int)var4, (int)var9), var5, var5, 1.0F);
      ModelManager.singleton.fully_render_entity(this.field_76, var3, var4, var9, this.field_76.yaw, dt);
   }

   // $FF: renamed from: b_ () void
   public final void update_entity() {
      ++this.field_78;
      if (this.field_78 == this.field_79) {
         this.destroy();
      }

   }

   // $FF: renamed from: entities () int
   public final int method_61() {
      return 2;
   }
}
