package net.minecraft.client.particles;

import net.minecraft.game.world.World;
import net.minecraft.client.rendering.GeometryDrawer;

// $FF: renamed from: net.minecraft.client.particles.entities
public final class class_9 extends Particle {
   // $FF: renamed from: Z float
   private float field_74;

   public class_9(World var1, float var2, float var3, float var4) {
      super(var1, var2, var3, var4, 0.0F, 0.0F, 0.0F);
      this.field_60 *= 0.01F;
      this.field_61 *= 0.01F;
      this.field_62 *= 0.01F;
      this.random.nextFloat();
      this.random.nextFloat();
      this.random.nextFloat();
      this.random.nextFloat();
      this.random.nextFloat();
      this.random.nextFloat();
      this.field_74 = this.field_68;
      this.field_70 = this.field_71 = this.field_72 = 1.0F;
      this.field_67 = (int)(8.0D / (Math.random() * 0.8D + 0.2D)) + 4;
      this.field_33 = true;
      this.field_63 = 48;
   }

   // $FF: renamed from: game (net.minecraft.client.game.projectiles, float, float, float, float, float, float) void
   public final void render_particle(GeometryDrawer drawer, float dt, float var3, float var4, float var5, float var6, float var7) {
      float var8 = ((float)this.field_66 + dt) / (float)this.field_67;
      this.field_68 = this.field_74 * (1.0F - var8 * var8 * 0.5F);
      super.render_particle(drawer, dt, var3, var4, var5, var6, var7);
   }

   // $FF: renamed from: game (float) float
   public final float light_level(float var1) {
      float var2;
      if ((var2 = ((float)this.field_66 + var1) / (float)this.field_67) < 0.0F) {
         var2 = 0.0F;
      }

      if (var2 > 1.0F) {
         var2 = 1.0F;
      }

      return super.light_level(var1) * var2 + (1.0F - var2);
   }

   // $FF: renamed from: b_ () void
   public final void update_entity() {
      this.prev_x = this.pos_x;
      this.prev_y = this.pos_y;
      this.prev_z = this.pos_z;
      if (this.field_66++ >= this.field_67) {
         this.destroy();
      }

      this.handle_collision(this.field_60, this.field_61, this.field_62);
      this.field_60 *= 0.96F;
      this.field_61 *= 0.96F;
      this.field_62 *= 0.96F;
      if (this.maybe_grounded) {
         this.field_60 *= 0.7F;
         this.field_62 *= 0.7F;
      }

   }
}
