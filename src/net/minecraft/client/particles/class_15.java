package net.minecraft.client.particles;

import net.minecraft.game.world.World;
import net.minecraft.game.world.material.Material;
import net.minecraft.client.rendering.GeometryDrawer;

// $FF: renamed from: net.minecraft.client.particles.i
public class class_15 extends Particle {
   public class_15(World var1, float var2, float var3, float var4) {
      super(var1, var2, var3, var4, 0.0F, 0.0F, 0.0F);
      this.field_60 *= 0.3F;
      this.field_61 = (float)Math.random() * 0.2F + 0.1F;
      this.field_62 *= 0.3F;
      this.field_70 = 1.0F;
      this.field_71 = 1.0F;
      this.field_72 = 1.0F;
      this.field_63 = 16;
      this.set_size(0.01F, 0.01F);
      this.field_69 = 0.06F;
      this.field_67 = (int)(8.0D / (Math.random() * 0.8D + 0.2D));
   }

   // $FF: renamed from: game (net.minecraft.client.game.projectiles, float, float, float, float, float, float) void
   public final void render_particle(GeometryDrawer drawer, float dt, float var3, float var4, float var5, float var6, float var7) {
      super.render_particle(drawer, dt, var3, var4, var5, var6, var7);
   }

   // $FF: renamed from: b_ () void
   public final void update_entity() {
      this.prev_x = this.pos_x;
      this.prev_y = this.pos_y;
      this.prev_z = this.pos_z;
      this.field_61 -= this.field_69;
      this.handle_collision(this.field_60, this.field_61, this.field_62);
      this.field_60 *= 0.98F;
      this.field_61 *= 0.98F;
      this.field_62 *= 0.98F;
      if (this.field_67-- <= 0) {
         this.destroy();
      }

      if (this.maybe_grounded) {
         if (Math.random() < 0.5D) {
            this.destroy();
         }

         this.field_60 *= 0.7F;
         this.field_62 *= 0.7F;
      }

      Material var1;
      if ((var1 = this.world.method_198((int)this.pos_x, (int)this.pos_y, (int)this.pos_z)).method_343() || var1.method_345()) {
         this.destroy();
      }

   }
}
