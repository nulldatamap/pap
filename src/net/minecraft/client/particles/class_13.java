package net.minecraft.client.particles;

import net.minecraft.game.world.World;
import net.minecraft.client.rendering.GeometryDrawer;

// $FF: renamed from: net.minecraft.client.particles.h
public final class class_13 extends Particle {
   public class_13(World var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super(var1, var2, var3, var4, var5, var6, var7);
      this.field_60 = var5 + (float)(Math.random() * 2.0D - 1.0D) * 0.05F;
      this.field_61 = var6 + (float)(Math.random() * 2.0D - 1.0D) * 0.05F;
      this.field_62 = var7 + (float)(Math.random() * 2.0D - 1.0D) * 0.05F;
      this.field_70 = this.field_71 = this.field_72 = this.random.nextFloat() * 0.3F + 0.7F;
      this.field_68 = this.random.nextFloat() * this.random.nextFloat() * 6.0F + 1.0F;
      this.field_67 = (int)(16.0D / ((double)this.random.nextFloat() * 0.8D + 0.2D)) + 2;
   }

   // $FF: renamed from: game (net.minecraft.client.game.projectiles, float, float, float, float, float, float) void
   public final void render_particle(GeometryDrawer drawer, float dt, float var3, float var4, float var5, float var6, float var7) {
      super.render_particle(drawer, dt, var3, var4, var5, var6, var7);
   }

   // $FF: renamed from: b_ () void
   public final void update_entity() {
      this.prev_x = this.pos_x;
      this.prev_y = this.pos_y;
      this.prev_z = this.pos_z;
      if (this.field_66++ >= this.field_67) {
         this.destroy();
      }

      this.field_63 = 7 - (this.field_66 << 3) / this.field_67;
      this.field_61 = (float)((double)this.field_61 + 0.004D);
      this.handle_collision(this.field_60, this.field_61, this.field_62);
      this.field_60 *= 0.9F;
      this.field_61 *= 0.9F;
      this.field_62 *= 0.9F;
      if (this.maybe_grounded) {
         this.field_60 *= 0.7F;
         this.field_62 *= 0.7F;
      }

   }
}
