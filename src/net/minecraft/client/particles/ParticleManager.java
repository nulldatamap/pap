package net.minecraft.client.particles;

import a.MathHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import net.minecraft.game.world.World;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.entities.Entity;
import net.minecraft.client.rendering.GeometryDrawer;
import net.minecraft.client.rendering.ResourceManager;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.particles.projectiles
public final class ParticleManager {
   // $FF: renamed from: game net.minecraft.game.game.player
   private World world;
   // $FF: renamed from: animals java.util.List[]
   private List[] particles = new List[3];
   // $FF: renamed from: entities net.minecraft.client.game.k
   private ResourceManager resource_manager;
   // $FF: renamed from: projectiles java.util.Random
   private Random random = new Random();

   public ParticleManager(World var1, ResourceManager var2) {
      if (var1 != null) {
         this.world = var1;
      }

      this.resource_manager = var2;

      for(int i = 0; i < 3; ++i) {
         this.particles[i] = new ArrayList();
      }

   }

   // $FF: renamed from: game (net.minecraft.client.particles.k) void
   public final void spawn_particle(Particle p) {
      int var2 = p.method_61();
      this.particles[var2].add(p);
   }

   // $FF: renamed from: game () void
   public final void method_242() {
      for(int var1 = 0; var1 < 3; ++var1) {
         for(int var2 = 0; var2 < this.particles[var1].size(); ++var2) {
            Particle var3;
            (var3 = (Particle)this.particles[var1].get(var2)).update_entity();
            if (var3.destroyed) {
               this.particles[var1].remove(var2--);
            }
         }
      }

   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, float) void
   public final void render_particles(Entity var1, float var2) {
      float var3 = MathHelper.cos(var1.yaw * 3.1415927F / 180.0F);
      float var4;
      float var5 = -(var4 = MathHelper.sin(var1.yaw * 3.1415927F / 180.0F)) * MathHelper.sin(var1.pitch * 3.1415927F / 180.0F);
      float var6 = var3 * MathHelper.sin(var1.pitch * 3.1415927F / 180.0F);
      float var11 = MathHelper.cos(var1.pitch * 3.1415927F / 180.0F);

      for(int var7 = 0; var7 < 2; ++var7) {
         if (this.particles[var7].size() != 0) {
            int var8 = 0;
            if (var7 == 0) {
               var8 = this.resource_manager.load_texture("/particles.png");
            }

            if (var7 == 1) {
               var8 = this.resource_manager.load_texture("/terrain.png");
            }

            GL11.glBindTexture(3553, var8);
            GeometryDrawer var12 = GeometryDrawer.singleton;
            GeometryDrawer.singleton.quads_mode();

            for(int var9 = 0; var9 < this.particles[var7].size(); ++var9) {
               ((Particle)this.particles[var7].get(var9)).render_particle(var12, var2, var3, var11, var4, var5, var6);
            }

            var12.draw();
         }
      }

   }

   // $FF: renamed from: game (float) void
   public final void pre_render_particles(float var1) {
      if (this.particles[2].size() != 0) {
         GeometryDrawer var2 = GeometryDrawer.singleton;

         for(int var3 = 0; var3 < this.particles[2].size(); ++var3) {
            ((Particle)this.particles[2].get(var3)).render_particle(var2, var1, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F);
         }

      }
   }

   // $FF: renamed from: game (net.minecraft.game.game.player) void
   public final void method_245(World var1) {
      this.world = var1;

      for(int var2 = 0; var2 < 3; ++var2) {
         this.particles[var2].clear();
      }

   }

   // $FF: renamed from: game (int, int, int) void
   public final void spawn_block_particles(int x, int y, int z) {
      int id = this.world.block_at(x, y, z);
      if (id != 0) {
         Block block = Block.blocks[id];

         for(int i = 0; i < 4; ++i) {
            for(int j = 0; j < 4; ++j) {
               for(int k = 0; k < 4; ++k) {
                  float cx = (float)x + ((float)i + 0.5F) / 4.0F;
                  float cy = (float)y + ((float)j + 0.5F) / 4.0F;
                  float cz = (float)z + ((float)k + 0.5F) / 4.0F;
                  this.spawn_particle(new BlockParticle(this.world, cx, cy, cz, cx - (float)x - 0.5F, cy - (float)y - 0.5F, cz - (float)z - 0.5F, block));
               }
            }
         }

      }
   }

   // $FF: renamed from: game (int, int, int, int) void
   public final void method_247(int var1, int var2, int var3, int var4) {
      int var5;
      if ((var5 = this.world.block_at(var1, var2, var3)) != 0) {
         Block var9 = Block.blocks[var5];
         float var6 = (float)var1 + this.random.nextFloat() * (var9.field_937 - var9.field_934 - 0.2F) + 0.1F + var9.field_934;
         float var7 = (float)var2 + this.random.nextFloat() * (var9.field_938 - var9.field_935 - 0.2F) + 0.1F + var9.field_935;
         float var8 = (float)var3 + this.random.nextFloat() * (var9.field_939 - var9.field_936 - 0.2F) + 0.1F + var9.field_936;
         if (var4 == 0) {
            var7 = (float)var2 + var9.field_935 - 0.1F;
         }

         if (var4 == 1) {
            var7 = (float)var2 + var9.field_938 + 0.1F;
         }

         if (var4 == 2) {
            var8 = (float)var3 + var9.field_936 - 0.1F;
         }

         if (var4 == 3) {
            var8 = (float)var3 + var9.field_939 + 0.1F;
         }

         if (var4 == 4) {
            var6 = (float)var1 + var9.field_934 - 0.1F;
         }

         if (var4 == 5) {
            var6 = (float)var1 + var9.field_937 + 0.1F;
         }

         this.spawn_particle((new BlockParticle(this.world, var6, var7, var8, 0.0F, 0.0F, 0.0F, var9)).method_58(0.2F).method_59(0.6F));
      }
   }

   // $FF: renamed from: animals () java.lang.String
   public final String method_248() {
      return "" + (this.particles[0].size() + this.particles[1].size() + this.particles[2].size());
   }
}
