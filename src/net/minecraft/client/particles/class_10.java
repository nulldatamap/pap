package net.minecraft.client.particles;

import net.minecraft.game.world.World;
import net.minecraft.game.world.material.Material;

// $FF: renamed from: net.minecraft.client.particles.game
public final class class_10 extends Particle {
   public class_10(World var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super(var1, var2, var3, var4, var5, var6, var7);
      this.field_70 = 1.0F;
      this.field_71 = 1.0F;
      this.field_72 = 1.0F;
      this.field_63 = 32;
      this.set_size(0.02F, 0.02F);
      this.field_68 *= this.random.nextFloat() * 0.6F + 0.2F;
      this.field_60 = var5 * 0.2F + (float)(Math.random() * 2.0D - 1.0D) * 0.02F;
      this.field_61 = var6 * 0.2F + (float)(Math.random() * 2.0D - 1.0D) * 0.02F;
      this.field_62 = var7 * 0.2F + (float)(Math.random() * 2.0D - 1.0D) * 0.02F;
      this.field_67 = (int)(8.0D / (Math.random() * 0.8D + 0.2D));
   }

   // $FF: renamed from: b_ () void
   public final void update_entity() {
      this.prev_x = this.pos_x;
      this.prev_y = this.pos_y;
      this.prev_z = this.pos_z;
      this.field_61 = (float)((double)this.field_61 + 0.002D);
      this.handle_collision(this.field_60, this.field_61, this.field_62);
      this.field_60 *= 0.85F;
      this.field_61 *= 0.85F;
      this.field_62 *= 0.85F;
      if (this.world.method_198((int)this.pos_x, (int)this.pos_y, (int)this.pos_z) != Material.water_block) {
         this.destroy();
      }

      if (this.field_67-- <= 0) {
         this.destroy();
      }

   }
}
