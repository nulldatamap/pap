package net.minecraft.client.particles;

import net.minecraft.game.world.World;
import net.minecraft.client.rendering.GeometryDrawer;

// $FF: renamed from: net.minecraft.client.particles.j
public final class class_8 extends Particle {
   // $FF: renamed from: Z float
   private float field_73;

   public class_8(World var1, float var2, float var3, float var4) {
      this(var1, var2, var3, var4, 1.0F);
   }

   public class_8(World var1, float var2, float var3, float var4, float var5) {
      super(var1, var2, var3, var4, 0.0F, 0.0F, 0.0F);
      this.field_60 *= 0.1F;
      this.field_61 *= 0.1F;
      this.field_62 *= 0.1F;
      this.field_70 = this.field_71 = this.field_72 = (float)(Math.random() * 0.30000001192092896D);
      this.field_68 *= 0.75F;
      this.field_68 *= var5;
      this.field_73 = this.field_68;
      this.field_67 = (int)(8.0D / (Math.random() * 0.8D + 0.2D));
      this.field_67 = (int)((float)this.field_67 * var5);
      this.field_33 = false;
   }

   // $FF: renamed from: game (net.minecraft.client.game.projectiles, float, float, float, float, float, float) void
   public final void render_particle(GeometryDrawer drawer, float dt, float var3, float var4, float var5, float var6, float var7) {
      float var8;
      if ((var8 = ((float)this.field_66 + dt) / (float)this.field_67 * 32.0F) < 0.0F) {
         var8 = 0.0F;
      }

      if (var8 > 1.0F) {
         var8 = 1.0F;
      }

      this.field_68 = this.field_73 * var8;
      super.render_particle(drawer, dt, var3, var4, var5, var6, var7);
   }

   // $FF: renamed from: b_ () void
   public final void update_entity() {
      this.prev_x = this.pos_x;
      this.prev_y = this.pos_y;
      this.prev_z = this.pos_z;
      if (this.field_66++ >= this.field_67) {
         this.destroy();
      }

      this.field_63 = 7 - (this.field_66 << 3) / this.field_67;
      this.field_61 = (float)((double)this.field_61 + 0.004D);
      this.handle_collision(this.field_60, this.field_61, this.field_62);
      if (this.pos_y == this.prev_y) {
         this.field_60 = (float)((double)this.field_60 * 1.1D);
         this.field_62 = (float)((double)this.field_62 * 1.1D);
      }

      this.field_60 *= 0.96F;
      this.field_61 *= 0.96F;
      this.field_62 *= 0.96F;
      if (this.maybe_grounded) {
         this.field_60 *= 0.7F;
         this.field_62 *= 0.7F;
      }

   }
}
