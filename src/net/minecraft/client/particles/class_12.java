package net.minecraft.client.particles;

import net.minecraft.game.world.World;
import net.minecraft.client.rendering.GeometryDrawer;

// $FF: renamed from: net.minecraft.client.particles.player
public final class class_12 extends Particle {
   // $FF: renamed from: Z float
   private float field_75;

   public class_12(World var1, float var2, float var3, float var4) {
      super(var1, var2, var3, var4, 0.0F, 0.0F, 0.0F);
      this.field_60 *= 0.8F;
      this.field_61 *= 0.8F;
      this.field_62 *= 0.8F;
      this.field_61 = this.random.nextFloat() * 0.4F + 0.05F;
      this.field_70 = this.field_71 = this.field_72 = 1.0F;
      this.field_68 *= this.random.nextFloat() * 2.0F + 0.2F;
      this.field_75 = this.field_68;
      this.field_67 = (int)(16.0D / (Math.random() * 0.8D + 0.2D));
      this.field_33 = false;
      this.field_63 = 49;
   }

   // $FF: renamed from: game (float) float
   public final float light_level(float var1) {
      return 1.0F;
   }

   // $FF: renamed from: game (net.minecraft.client.game.projectiles, float, float, float, float, float, float) void
   public final void render_particle(GeometryDrawer drawer, float dt, float var3, float var4, float var5, float var6, float var7) {
      float var8 = ((float)this.field_66 + dt) / (float)this.field_67;
      this.field_68 = this.field_75 * (1.0F - var8 * var8);
      super.render_particle(drawer, dt, var3, var4, var5, var6, var7);
   }

   // $FF: renamed from: b_ () void
   public final void update_entity() {
      this.prev_x = this.pos_x;
      this.prev_y = this.pos_y;
      this.prev_z = this.pos_z;
      if (this.field_66++ >= this.field_67) {
         this.destroy();
      }

      float var1 = (float)this.field_66 / (float)this.field_67;
      if (this.random.nextFloat() > var1) {
         this.world.spawn_effect("smoke", this.pos_x, this.pos_y, this.pos_z, this.field_60, this.field_61, this.field_62);
      }

      this.field_61 = (float)((double)this.field_61 - 0.03D);
      this.handle_collision(this.field_60, this.field_61, this.field_62);
      this.field_60 *= 0.999F;
      this.field_61 *= 0.999F;
      this.field_62 *= 0.999F;
      if (this.maybe_grounded) {
         this.field_60 *= 0.7F;
         this.field_62 *= 0.7F;
      }

   }
}
