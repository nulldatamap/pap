package net.minecraft.client.particles;

import a.MathHelper;
import com.data.nbt.CompoundTag;
import net.minecraft.game.world.World;
import net.minecraft.game.entities.Entity;
import net.minecraft.client.rendering.GeometryDrawer;

// $FF: renamed from: net.minecraft.client.particles.k
public class Particle extends Entity {
   // $FF: renamed from: game float
   protected float field_60;
   // $FF: renamed from: animals float
   protected float field_61;
   // $FF: renamed from: O float
   protected float field_62;
   // $FF: renamed from: P int
   protected int field_63;
   // $FF: renamed from: Q float
   protected float field_64;
   // $FF: renamed from: R float
   protected float field_65;
   // $FF: renamed from: S int
   protected int field_66 = 0;
   // $FF: renamed from: T int
   protected int field_67 = 0;
   // $FF: renamed from: U float
   protected float field_68;
   // $FF: renamed from: V float
   protected float field_69;
   // $FF: renamed from: W float
   protected float field_70;
   // $FF: renamed from: X float
   protected float field_71;
   // $FF: renamed from: Y float
   protected float field_72;

   public Particle(World w, float var2, float var3, float var4, float var5, float var6, float var7) {
      super(w);
      this.set_size(0.2F, 0.2F);
      this.camera_height = this.height / 2.0F;
      this.set_position(var2, var3, var4);
      this.field_70 = this.field_71 = this.field_72 = 1.0F;
      this.field_60 = var5 + (float)(Math.random() * 2.0D - 1.0D) * 0.4F;
      this.field_61 = var6 + (float)(Math.random() * 2.0D - 1.0D) * 0.4F;
      this.field_62 = var7 + (float)(Math.random() * 2.0D - 1.0D) * 0.4F;
      float var8 = (float)(Math.random() + Math.random() + 1.0D) * 0.15F;
      var2 = MathHelper.sqrt(this.field_60 * this.field_60 + this.field_61 * this.field_61 + this.field_62 * this.field_62);
      this.field_60 = this.field_60 / var2 * var8 * 0.4F;
      this.field_61 = this.field_61 / var2 * var8 * 0.4F + 0.1F;
      this.field_62 = this.field_62 / var2 * var8 * 0.4F;
      this.field_64 = this.random.nextFloat() * 3.0F;
      this.field_65 = this.random.nextFloat() * 3.0F;
      this.field_68 = (this.random.nextFloat() * 0.5F + 0.5F) * 2.0F;
      this.field_67 = (int)(4.0F / (this.random.nextFloat() * 0.9F + 0.1F));
      this.field_66 = 0;
      this.field_25 = false;
   }

   // $FF: renamed from: entities (float) net.minecraft.client.particles.k
   public final Particle method_58(float var1) {
      this.field_60 *= 0.2F;
      this.field_61 = (this.field_61 - 0.1F) * 0.2F + 0.1F;
      this.field_62 *= 0.2F;
      return this;
   }

   // $FF: renamed from: projectiles (float) net.minecraft.client.particles.k
   public final Particle method_59(float var1) {
      this.set_size(0.120000005F, 0.120000005F);
      this.field_68 *= 0.6F;
      return this;
   }

   // $FF: renamed from: b_ () void
   public void update_entity() {
      this.prev_x = this.pos_x;
      this.prev_y = this.pos_y;
      this.prev_z = this.pos_z;
      if (this.field_66++ >= this.field_67) {
         this.destroy();
      }

      this.field_61 = (float)((double)this.field_61 - 0.04D * (double)this.field_69);
      this.handle_collision(this.field_60, this.field_61, this.field_62);
      this.field_60 *= 0.98F;
      this.field_61 *= 0.98F;
      this.field_62 *= 0.98F;
      if (this.maybe_grounded) {
         this.field_60 *= 0.7F;
         this.field_62 *= 0.7F;
      }

   }

   // $FF: renamed from: game (net.minecraft.client.game.projectiles, float, float, float, float, float, float) void
   public void render_particle(GeometryDrawer drawer, float dt, float var3, float var4, float var5, float var6, float var7) {
      float var8;
      float var9 = (var8 = (float)(this.field_63 % 16) / 16.0F) + 0.0624375F;
      float var10;
      float var11 = (var10 = (float)(this.field_63 / 16) / 16.0F) + 0.0624375F;
      float var12 = 0.1F * this.field_68;
      float var13 = this.prev_x + (this.pos_x - this.prev_x) * dt;
      float var14 = this.prev_y + (this.pos_y - this.prev_y) * dt;
      float var15 = this.prev_z + (this.pos_z - this.prev_z) * dt;
      float light = this.light_level(dt);
      drawer.set_color(this.field_70 * light, this.field_71 * light, this.field_72 * light);
      drawer.add_vertex(var13 - var3 * var12 - var6 * var12, var14 - var4 * var12, var15 - var5 * var12 - var7 * var12, var8, var11);
      drawer.add_vertex(var13 - var3 * var12 + var6 * var12, var14 + var4 * var12, var15 - var5 * var12 + var7 * var12, var8, var10);
      drawer.add_vertex(var13 + var3 * var12 + var6 * var12, var14 + var4 * var12, var15 + var5 * var12 + var7 * var12, var9, var10);
      drawer.add_vertex(var13 + var3 * var12 - var6 * var12, var14 - var4 * var12, var15 + var5 * var12 - var7 * var12, var9, var11);
   }

   // $FF: renamed from: entities () int
   public int method_61() {
      return 0;
   }

   // $FF: renamed from: game (com.game.game.entities) void
   protected final void write_tag_extra(CompoundTag var1) {
   }

   // $FF: renamed from: game () java.lang.String
   protected final String get_id() {
      return null;
   }

   // $FF: renamed from: animals (com.game.game.entities) void
   protected final void read_tag_extra(CompoundTag var1) {
   }
}
