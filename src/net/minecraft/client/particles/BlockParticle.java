package net.minecraft.client.particles;

import net.minecraft.game.world.World;
import net.minecraft.game.world.block.Block;
import net.minecraft.client.rendering.GeometryDrawer;

// $FF: renamed from: net.minecraft.client.particles.animals
public final class BlockParticle extends Particle {
   public BlockParticle(World var1, float var2, float var3, float var4, float var5, float var6, float var7, Block var8) {
      super(var1, var2, var3, var4, var5, var6, var7);
      this.field_63 = var8.maybeDropCount;
      this.field_69 = var8.field_941;
      this.field_70 = this.field_71 = this.field_72 = 0.6F;
      this.field_68 /= 2.0F;
   }

   // $FF: renamed from: entities () int
   public final int method_61() {
      return 1;
   }

   // $FF: renamed from: game (net.minecraft.client.game.projectiles, float, float, float, float, float, float) void
   public final void render_particle(GeometryDrawer drawer, float dt, float var3, float var4, float var5, float var6, float var7) {
      float var8;
      float var9 = (var8 = ((float)(this.field_63 % 16) + this.field_64 / 4.0F) / 16.0F) + 0.015609375F;
      float var10;
      float var11 = (var10 = ((float)(this.field_63 / 16) + this.field_65 / 4.0F) / 16.0F) + 0.015609375F;
      float var12 = 0.1F * this.field_68;
      float var13 = this.prev_x + (this.pos_x - this.prev_x) * dt;
      float var14 = this.prev_y + (this.pos_y - this.prev_y) * dt;
      float var15 = this.prev_z + (this.pos_z - this.prev_z) * dt;
      dt = this.light_level(dt);
      drawer.set_color(dt * this.field_70, dt * this.field_71, dt * this.field_72);
      drawer.add_vertex(var13 - var3 * var12 - var6 * var12, var14 - var4 * var12, var15 - var5 * var12 - var7 * var12, var8, var11);
      drawer.add_vertex(var13 - var3 * var12 + var6 * var12, var14 + var4 * var12, var15 - var5 * var12 + var7 * var12, var8, var10);
      drawer.add_vertex(var13 + var3 * var12 + var6 * var12, var14 + var4 * var12, var15 + var5 * var12 + var7 * var12, var9, var10);
      drawer.add_vertex(var13 + var3 * var12 - var6 * var12, var14 - var4 * var12, var15 + var5 * var12 - var7 * var12, var9, var11);
   }
}
