package net.minecraft.client;

import java.awt.Canvas;

// $FF: renamed from: net.minecraft.client.animals
final class MinecraftCanvas extends Canvas {
   // $FF: renamed from: game net.minecraft.client.MinecraftApplet
   // $FF: synthetic field
   private MinecraftApplet applet;

   MinecraftCanvas(MinecraftApplet var1) {
      super();
      this.applet = var1;
   }

   public final synchronized void addNotify() {
      super.addNotify();
      this.applet.method_338();
   }

   public final synchronized void removeNotify() {
      this.applet.method_339();
      super.removeNotify();
   }
}
