package net.minecraft.client.models;

import a.MathHelper;

// $FF: renamed from: net.minecraft.client.animals.h
public class MobModel extends Model {
   // $FF: renamed from: game net.minecraft.client.animals.entities
   public ModelBox field_757;
   // $FF: renamed from: animals net.minecraft.client.animals.entities
   public ModelBox field_758;
   // $FF: renamed from: entities net.minecraft.client.animals.entities
   public ModelBox field_759;
   // $FF: renamed from: projectiles net.minecraft.client.animals.entities
   public ModelBox field_760;
   // $FF: renamed from: player net.minecraft.client.animals.entities
   public ModelBox field_761;
   // $FF: renamed from: particles net.minecraft.client.animals.entities
   public ModelBox field_762;
   // $FF: renamed from: player net.minecraft.client.animals.entities
   public ModelBox field_763;

   public MobModel() {
      this(0.0F);
   }

   public MobModel(float var1) {
      this(var1, 0.0F);
   }

   private MobModel(float var1, float var2) {
      this.field_757 = new ModelBox(0, 0);
      this.field_757.set_dimensions(-4.0F, -8.0F, -4.0F, 8, 8, 8, var1);
      this.field_757.set_position(0.0F, 0.0F, 0.0F);
      this.field_758 = new ModelBox(32, 0);
      this.field_758.set_dimensions(-4.0F, -8.0F, -4.0F, 8, 8, 8, var1 + 0.5F);
      this.field_758.set_position(0.0F, 0.0F, 0.0F);
      this.field_759 = new ModelBox(16, 16);
      this.field_759.set_dimensions(-4.0F, 0.0F, -2.0F, 8, 12, 4, var1);
      this.field_759.set_position(0.0F, 0.0F, 0.0F);
      this.field_760 = new ModelBox(40, 16);
      this.field_760.set_dimensions(-3.0F, -2.0F, -2.0F, 4, 12, 4, var1);
      this.field_760.set_position(-5.0F, 2.0F, 0.0F);
      this.field_761 = new ModelBox(40, 16);
      this.field_761.field_800 = true;
      this.field_761.set_dimensions(-1.0F, -2.0F, -2.0F, 4, 12, 4, var1);
      this.field_761.set_position(5.0F, 2.0F, 0.0F);
      this.field_762 = new ModelBox(0, 16);
      this.field_762.set_dimensions(-2.0F, 0.0F, -2.0F, 4, 12, 4, var1);
      this.field_762.set_position(-2.0F, 12.0F, 0.0F);
      this.field_763 = new ModelBox(0, 16);
      this.field_763.field_800 = true;
      this.field_763.set_dimensions(-2.0F, 0.0F, -2.0F, 4, 12, 4, var1);
      this.field_763.set_position(2.0F, 12.0F, 0.0F);
   }

   // $FF: renamed from: game (float, float, float, float, float, float) void
   public final void animate_and_render(float var1, float var2, float var3, float var4, float var5, float var6) {
      this.animate(var1, var2, var3, var4, var5, 1.0F);
      this.field_757.render(1.0F);
      this.field_759.render(1.0F);
      this.field_760.render(1.0F);
      this.field_761.render(1.0F);
      this.field_762.render(1.0F);
      this.field_763.render(1.0F);
      this.field_758.render(1.0F);
   }

   // $FF: renamed from: animals (float, float, float, float, float, float) void
   public void animate(float var1, float var2, float var3, float var4, float var5, float var6) {
      this.field_757.field_796 = var4 / 57.295776F;
      this.field_757.field_795 = var5 / 57.295776F;
      this.field_758.field_796 = this.field_757.field_796;
      this.field_758.field_795 = this.field_757.field_795;
      this.field_760.field_795 = MathHelper.cos(var1 * 0.6662F + 3.1415927F) * 2.0F * var2;
      this.field_760.field_797 = (MathHelper.cos(var1 * 0.2312F) + 1.0F) * var2;
      this.field_761.field_795 = MathHelper.cos(var1 * 0.6662F) * 2.0F * var2;
      this.field_761.field_797 = (MathHelper.cos(var1 * 0.2812F) - 1.0F) * var2;
      this.field_762.field_795 = MathHelper.cos(var1 * 0.6662F) * 1.4F * var2;
      this.field_763.field_795 = MathHelper.cos(var1 * 0.6662F + 3.1415927F) * 1.4F * var2;
      this.field_760.field_797 += MathHelper.cos(var3 * 0.09F) * 0.05F + 0.05F;
      this.field_761.field_797 -= MathHelper.cos(var3 * 0.09F) * 0.05F + 0.05F;
      this.field_760.field_795 += MathHelper.sin(var3 * 0.067F) * 0.05F;
      this.field_761.field_795 -= MathHelper.sin(var3 * 0.067F) * 0.05F;
   }
}
