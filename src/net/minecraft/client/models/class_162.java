package net.minecraft.client.models;

import a.MathHelper;

// $FF: renamed from: net.minecraft.client.animals.k
public final class class_162 extends Model {
   // $FF: renamed from: game net.minecraft.client.animals.entities
   private ModelBox field_775 = new ModelBox(0, 0);
   // $FF: renamed from: animals net.minecraft.client.animals.entities
   private ModelBox field_776;
   // $FF: renamed from: entities net.minecraft.client.animals.entities
   private ModelBox field_777;
   // $FF: renamed from: projectiles net.minecraft.client.animals.entities
   private ModelBox field_778;
   // $FF: renamed from: player net.minecraft.client.animals.entities
   private ModelBox field_779;
   // $FF: renamed from: particles net.minecraft.client.animals.entities
   private ModelBox field_780;
   // $FF: renamed from: player net.minecraft.client.animals.entities
   private ModelBox field_781;

   public class_162() {
      this.field_775.set_dimensions(-4.0F, -8.0F, -4.0F, 8, 8, 8, 0.0F);
      this.field_775.set_position(0.0F, 4.0F, 0.0F);
      this.field_776 = new ModelBox(32, 0);
      this.field_776.set_dimensions(-4.0F, -8.0F, -4.0F, 8, 8, 8, 0.5F);
      this.field_776.set_position(0.0F, 4.0F, 0.0F);
      this.field_777 = new ModelBox(16, 16);
      this.field_777.set_dimensions(-4.0F, 0.0F, -2.0F, 8, 12, 4, 0.0F);
      this.field_777.set_position(0.0F, 4.0F, 0.0F);
      this.field_778 = new ModelBox(0, 16);
      this.field_778.set_dimensions(-2.0F, 0.0F, -2.0F, 4, 6, 4, 0.0F);
      this.field_778.set_position(-2.0F, 16.0F, 4.0F);
      this.field_779 = new ModelBox(0, 16);
      this.field_779.set_dimensions(-2.0F, 0.0F, -2.0F, 4, 6, 4, 0.0F);
      this.field_779.set_position(2.0F, 16.0F, 4.0F);
      this.field_780 = new ModelBox(0, 16);
      this.field_780.set_dimensions(-2.0F, 0.0F, -2.0F, 4, 6, 4, 0.0F);
      this.field_780.set_position(-2.0F, 16.0F, -4.0F);
      this.field_781 = new ModelBox(0, 16);
      this.field_781.set_dimensions(-2.0F, 0.0F, -2.0F, 4, 6, 4, 0.0F);
      this.field_781.set_position(2.0F, 16.0F, -4.0F);
   }

   // $FF: renamed from: game (float, float, float, float, float, float) void
   public final void animate_and_render(float var1, float var2, float var3, float var4, float var5, float var6) {
      this.animate(var1, var2, var3, var4, var5, 1.0F);
      this.field_775.render(1.0F);
      this.field_777.render(1.0F);
      this.field_778.render(1.0F);
      this.field_779.render(1.0F);
      this.field_780.render(1.0F);
      this.field_781.render(1.0F);
   }

   // $FF: renamed from: animals (float, float, float, float, float, float) void
   public final void animate(float var1, float var2, float var3, float var4, float var5, float var6) {
      this.field_775.field_796 = var4 / 57.295776F;
      this.field_775.field_795 = var5 / 57.295776F;
      this.field_778.field_795 = MathHelper.cos(var1 * 0.6662F) * 1.4F * var2;
      this.field_779.field_795 = MathHelper.cos(var1 * 0.6662F + 3.1415927F) * 1.4F * var2;
      this.field_780.field_795 = MathHelper.cos(var1 * 0.6662F + 3.1415927F) * 1.4F * var2;
      this.field_781.field_795 = MathHelper.cos(var1 * 0.6662F) * 1.4F * var2;
   }
}
