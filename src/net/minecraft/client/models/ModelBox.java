package net.minecraft.client.models;

import net.minecraft.game.util.Vector;
import net.minecraft.client.rendering.GeometryDrawer;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.animals.entities
public final class ModelBox {
   // $FF: renamed from: particles net.minecraft.client.animals.j[]
   private class_168[] field_788;
   // $FF: renamed from: player net.minecraft.client.animals.i[]
   private class_169[] field_789;
   // $FF: renamed from: h int
   private int field_790;
   // $FF: renamed from: i int
   private int field_791;
   // $FF: renamed from: j float
   private float field_792;
   // $FF: renamed from: k float
   private float field_793;
   // $FF: renamed from: l float
   private float field_794;
   // $FF: renamed from: game float
   public float field_795;
   // $FF: renamed from: animals float
   public float field_796;
   // $FF: renamed from: entities float
   public float field_797;
   // $FF: renamed from: m boolean
   private boolean field_798 = false;
   // $FF: renamed from: n int
   private int field_799 = 0;
   // $FF: renamed from: projectiles boolean
   public boolean field_800 = false;
   // $FF: renamed from: player boolean
   public boolean field_801 = true;
   // $FF: renamed from: o boolean
   private boolean field_802 = false;

   public ModelBox(int var1, int var2) {
      this.field_790 = var1;
      this.field_791 = var2;
   }

   // $FF: renamed from: game (float, float, float, int, int, int, float) void
   public final void set_dimensions(float var1, float var2, float var3, int var4, int var5, int var6, float var7) {
      this.field_788 = new class_168[8];
      this.field_789 = new class_169[6];
      float var8 = var1 + (float)var4;
      float var9 = var2 + (float)var5;
      float var10 = var3 + (float)var6;
      var1 -= var7;
      var2 -= var7;
      var3 -= var7;
      var8 += var7;
      var9 += var7;
      var10 += var7;
      if (this.field_800) {
         var7 = var8;
         var8 = var1;
         var1 = var7;
      }

      class_168 var20 = new class_168(var1, var2, var3, 0.0F, 0.0F);
      class_168 var11 = new class_168(var8, var2, var3, 0.0F, 8.0F);
      class_168 var12 = new class_168(var8, var9, var3, 8.0F, 8.0F);
      class_168 var18 = new class_168(var1, var9, var3, 8.0F, 0.0F);
      class_168 var13 = new class_168(var1, var2, var10, 0.0F, 0.0F);
      class_168 var15 = new class_168(var8, var2, var10, 0.0F, 8.0F);
      class_168 var21 = new class_168(var8, var9, var10, 8.0F, 8.0F);
      class_168 var14 = new class_168(var1, var9, var10, 8.0F, 0.0F);
      this.field_788[0] = var20;
      this.field_788[1] = var11;
      this.field_788[2] = var12;
      this.field_788[3] = var18;
      this.field_788[4] = var13;
      this.field_788[5] = var15;
      this.field_788[6] = var21;
      this.field_788[7] = var14;
      this.field_789[0] = new class_169(new class_168[]{var15, var11, var12, var21}, this.field_790 + var6 + var4, this.field_791 + var6, this.field_790 + var6 + var4 + var6, this.field_791 + var6 + var5);
      this.field_789[1] = new class_169(new class_168[]{var20, var13, var14, var18}, this.field_790, this.field_791 + var6, this.field_790 + var6, this.field_791 + var6 + var5);
      this.field_789[2] = new class_169(new class_168[]{var15, var13, var20, var11}, this.field_790 + var6, this.field_791, this.field_790 + var6 + var4, this.field_791 + var6);
      this.field_789[3] = new class_169(new class_168[]{var12, var18, var14, var21}, this.field_790 + var6 + var4, this.field_791, this.field_790 + var6 + var4 + var4, this.field_791 + var6);
      this.field_789[4] = new class_169(new class_168[]{var11, var20, var18, var12}, this.field_790 + var6, this.field_791 + var6, this.field_790 + var6 + var4, this.field_791 + var6 + var5);
      this.field_789[5] = new class_169(new class_168[]{var13, var15, var21, var14}, this.field_790 + var6 + var4 + var6, this.field_791 + var6, this.field_790 + var6 + var4 + var6 + var4, this.field_791 + var6 + var5);
      if (this.field_800) {
         for(int var16 = 0; var16 < this.field_789.length; ++var16) {
            class_169 var17;
            class_168[] var19 = new class_168[(var17 = this.field_789[var16]).field_806.length];

            for(var4 = 0; var4 < var17.field_806.length; ++var4) {
               var19[var4] = var17.field_806[var17.field_806.length - var4 - 1];
            }

            var17.field_806 = var19;
         }
      }

   }

   // $FF: renamed from: game (float, float, float) void
   public final void set_position(float var1, float var2, float var3) {
      this.field_792 = var1;
      this.field_793 = var2;
      this.field_794 = var3;
   }

   // $FF: renamed from: game (float) void
   public final void render(float var1) {
      if (this.field_801) {
         if (!this.field_798) {
            float var3 = var1;
            ModelBox var2 = this;
            this.field_799 = GL11.glGenLists(1);
            GL11.glNewList(this.field_799, 4864);
            GeometryDrawer var4 = GeometryDrawer.singleton;

            for(int var5 = 0; var5 < var2.field_789.length; ++var5) {
               var4.quads_mode();
               class_169 var10000 = var2.field_789[var5];
               float var8 = var3;
               GeometryDrawer var7 = var4;
               class_169 var6 = var10000;
               Vector var9 = var10000.field_806[1].field_803.sub(var6.field_806[0].field_803).norm();
               Vector var10 = var6.field_806[1].field_803.sub(var6.field_806[2].field_803).norm();
               GeometryDrawer.set_normal(-(var9 = (new Vector(var9.y * var10.z - var9.z * var10.y, var9.z * var10.x - var9.x * var10.z, var9.x * var10.y - var9.y * var10.x)).norm()).x, -var9.y, -var9.z);

               for(int var11 = 0; var11 < 4; ++var11) {
                  class_168 var12 = var6.field_806[var11];
                  var7.add_vertex(var12.field_803.x * var8, var12.field_803.y * var8, var12.field_803.z * var8, var12.field_804, var12.field_805);
               }

               var4.draw();
            }

            GL11.glEndList();
            var2.field_798 = true;
         }

         if (this.field_795 == 0.0F && this.field_796 == 0.0F && this.field_797 == 0.0F) {
            if (this.field_792 == 0.0F && this.field_793 == 0.0F && this.field_794 == 0.0F) {
               GL11.glCallList(this.field_799);
            } else {
               GL11.glTranslatef(this.field_792 * var1, this.field_793 * var1, this.field_794 * var1);
               GL11.glCallList(this.field_799);
               GL11.glTranslatef(-this.field_792 * var1, -this.field_793 * var1, -this.field_794 * var1);
            }
         } else {
            GL11.glPushMatrix();
            GL11.glTranslatef(this.field_792 * var1, this.field_793 * var1, this.field_794 * var1);
            if (this.field_797 != 0.0F) {
               GL11.glRotatef(this.field_797 * 57.295776F, 0.0F, 0.0F, 1.0F);
            }

            if (this.field_796 != 0.0F) {
               GL11.glRotatef(this.field_796 * 57.295776F, 0.0F, 1.0F, 0.0F);
            }

            if (this.field_795 != 0.0F) {
               GL11.glRotatef(this.field_795 * 57.295776F, 1.0F, 0.0F, 0.0F);
            }

            GL11.glCallList(this.field_799);
            GL11.glPopMatrix();
         }
      }
   }
}
