package net.minecraft.client.models;

// $FF: renamed from: net.minecraft.client.animals.m
public final class SkeletonModel extends ZombieModel {
   public SkeletonModel() {
      this.field_760 = new ModelBox(40, 16);
      this.field_760.set_dimensions(-1.0F, -2.0F, -1.0F, 2, 12, 2, 0.0F);
      this.field_760.set_position(-5.0F, 2.0F, 0.0F);
      this.field_761 = new ModelBox(40, 16);
      this.field_761.field_800 = true;
      this.field_761.set_dimensions(-1.0F, -2.0F, -1.0F, 2, 12, 2, 0.0F);
      this.field_761.set_position(5.0F, 2.0F, 0.0F);
      this.field_762 = new ModelBox(0, 16);
      this.field_762.set_dimensions(-1.0F, 0.0F, -1.0F, 2, 12, 2, 0.0F);
      this.field_762.set_position(-2.0F, 12.0F, 0.0F);
      this.field_763 = new ModelBox(0, 16);
      this.field_763.field_800 = true;
      this.field_763.set_dimensions(-1.0F, 0.0F, -1.0F, 2, 12, 2, 0.0F);
      this.field_763.set_position(2.0F, 12.0F, 0.0F);
   }
}
