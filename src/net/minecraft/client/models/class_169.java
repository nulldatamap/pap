package net.minecraft.client.models;

// $FF: renamed from: net.minecraft.client.animals.i
public final class class_169 {
   // $FF: renamed from: game net.minecraft.client.animals.j[]
   public class_168[] field_806;

   private class_169(class_168[] var1) {
      this.field_806 = var1;
   }

   public class_169(class_168[] var1, int var2, int var3, int var4, int var5) {
      this(var1);
      var1[0] = var1[0].method_466((float)var4 / 64.0F - 0.0015625F, (float)var3 / 32.0F + 0.003125F);
      var1[1] = var1[1].method_466((float)var2 / 64.0F + 0.0015625F, (float)var3 / 32.0F + 0.003125F);
      var1[2] = var1[2].method_466((float)var2 / 64.0F + 0.0015625F, (float)var5 / 32.0F - 0.003125F);
      var1[3] = var1[3].method_466((float)var4 / 64.0F - 0.0015625F, (float)var5 / 32.0F - 0.003125F);
   }
}
