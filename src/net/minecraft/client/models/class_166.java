package net.minecraft.client.models;

// $FF: renamed from: net.minecraft.client.animals.game
public final class class_166 extends QuadpedalModel {
   public class_166() {
      super(12, 0.0F);
      this.head = new ModelBox(0, 0);
      this.head.set_dimensions(-3.0F, -4.0F, -6.0F, 6, 6, 8, 0.0F);
      this.head.set_position(0.0F, 6.0F, -8.0F);
      this.torso = new ModelBox(28, 8);
      this.torso.set_dimensions(-4.0F, -10.0F, -7.0F, 8, 16, 6, 0.0F);
      this.torso.set_position(0.0F, 5.0F, 2.0F);
   }
}
