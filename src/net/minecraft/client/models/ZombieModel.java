package net.minecraft.client.models;

import a.MathHelper;

// $FF: renamed from: net.minecraft.client.animals.player
public class ZombieModel extends MobModel {
   // $FF: renamed from: animals (float, float, float, float, float, float) void
   public final void animate(float var1, float var2, float var3, float var4, float var5, float var6) {
      super.animate(var1, var2, var3, var4, var5, var6);
      var1 = MathHelper.sin(0.0F);
      var2 = MathHelper.sin(0.0F);
      this.field_760.field_797 = 0.0F;
      this.field_761.field_797 = 0.0F;
      this.field_760.field_796 = -(0.1F - var1 * 0.6F);
      this.field_761.field_796 = 0.1F - var1 * 0.6F;
      this.field_760.field_795 = -1.5707964F;
      this.field_761.field_795 = -1.5707964F;
      this.field_760.field_795 -= var1 * 1.2F - var2 * 0.4F;
      this.field_761.field_795 -= var1 * 1.2F - var2 * 0.4F;
      this.field_760.field_797 += MathHelper.cos(var3 * 0.09F) * 0.05F + 0.05F;
      this.field_761.field_797 -= MathHelper.cos(var3 * 0.09F) * 0.05F + 0.05F;
      this.field_760.field_795 += MathHelper.sin(var3 * 0.067F) * 0.05F;
      this.field_761.field_795 -= MathHelper.sin(var3 * 0.067F) * 0.05F;
   }
}
