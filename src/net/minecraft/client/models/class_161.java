package net.minecraft.client.models;

import a.MathHelper;

// $FF: renamed from: net.minecraft.client.animals.player
public final class class_161 extends Model {
   // $FF: renamed from: game net.minecraft.client.animals.entities
   private ModelBox field_764 = new ModelBox(32, 4);
   // $FF: renamed from: animals net.minecraft.client.animals.entities
   private ModelBox field_765;
   // $FF: renamed from: entities net.minecraft.client.animals.entities
   private ModelBox field_766;
   // $FF: renamed from: projectiles net.minecraft.client.animals.entities
   private ModelBox field_767;
   // $FF: renamed from: player net.minecraft.client.animals.entities
   private ModelBox field_768;
   // $FF: renamed from: particles net.minecraft.client.animals.entities
   private ModelBox field_769;
   // $FF: renamed from: player net.minecraft.client.animals.entities
   private ModelBox field_770;
   // $FF: renamed from: h net.minecraft.client.animals.entities
   private ModelBox field_771;
   // $FF: renamed from: i net.minecraft.client.animals.entities
   private ModelBox field_772;
   // $FF: renamed from: j net.minecraft.client.animals.entities
   private ModelBox field_773;
   // $FF: renamed from: k net.minecraft.client.animals.entities
   private ModelBox field_774;

   public class_161() {
      this.field_764.set_dimensions(-4.0F, -4.0F, -8.0F, 8, 8, 8, 0.0F);
      this.field_764.set_position(0.0F, 15.0F, -3.0F);
      this.field_765 = new ModelBox(0, 0);
      this.field_765.set_dimensions(-3.0F, -3.0F, -3.0F, 6, 6, 6, 0.0F);
      this.field_765.set_position(0.0F, 15.0F, 0.0F);
      this.field_766 = new ModelBox(0, 12);
      this.field_766.set_dimensions(-5.0F, -4.0F, -6.0F, 10, 8, 12, 0.0F);
      this.field_766.set_position(0.0F, 15.0F, 9.0F);
      this.field_767 = new ModelBox(18, 0);
      this.field_767.set_dimensions(-15.0F, -1.0F, -1.0F, 16, 2, 2, 0.0F);
      this.field_767.set_position(-4.0F, 15.0F, 2.0F);
      this.field_768 = new ModelBox(18, 0);
      this.field_768.set_dimensions(-1.0F, -1.0F, -1.0F, 16, 2, 2, 0.0F);
      this.field_768.set_position(4.0F, 15.0F, 2.0F);
      this.field_769 = new ModelBox(18, 0);
      this.field_769.set_dimensions(-15.0F, -1.0F, -1.0F, 16, 2, 2, 0.0F);
      this.field_769.set_position(-4.0F, 15.0F, 1.0F);
      this.field_770 = new ModelBox(18, 0);
      this.field_770.set_dimensions(-1.0F, -1.0F, -1.0F, 16, 2, 2, 0.0F);
      this.field_770.set_position(4.0F, 15.0F, 1.0F);
      this.field_771 = new ModelBox(18, 0);
      this.field_771.set_dimensions(-15.0F, -1.0F, -1.0F, 16, 2, 2, 0.0F);
      this.field_771.set_position(-4.0F, 15.0F, 0.0F);
      this.field_772 = new ModelBox(18, 0);
      this.field_772.set_dimensions(-1.0F, -1.0F, -1.0F, 16, 2, 2, 0.0F);
      this.field_772.set_position(4.0F, 15.0F, 0.0F);
      this.field_773 = new ModelBox(18, 0);
      this.field_773.set_dimensions(-15.0F, -1.0F, -1.0F, 16, 2, 2, 0.0F);
      this.field_773.set_position(-4.0F, 15.0F, -1.0F);
      this.field_774 = new ModelBox(18, 0);
      this.field_774.set_dimensions(-1.0F, -1.0F, -1.0F, 16, 2, 2, 0.0F);
      this.field_774.set_position(4.0F, 15.0F, -1.0F);
   }

   // $FF: renamed from: game (float, float, float, float, float, float) void
   public final void animate_and_render(float var1, float var2, float var3, float var4, float var5, float var6) {
      this.animate(var1, var2, var3, var4, var5, 1.0F);
      this.field_764.render(1.0F);
      this.field_765.render(1.0F);
      this.field_766.render(1.0F);
      this.field_767.render(1.0F);
      this.field_768.render(1.0F);
      this.field_769.render(1.0F);
      this.field_770.render(1.0F);
      this.field_771.render(1.0F);
      this.field_772.render(1.0F);
      this.field_773.render(1.0F);
      this.field_774.render(1.0F);
   }

   // $FF: renamed from: animals (float, float, float, float, float, float) void
   public final void animate(float var1, float var2, float var3, float var4, float var5, float var6) {
      this.field_764.field_796 = var4 / 57.295776F;
      this.field_764.field_795 = var5 / 57.295776F;
      this.field_767.field_797 = -0.7853982F;
      this.field_768.field_797 = 0.7853982F;
      this.field_769.field_797 = -0.58119464F;
      this.field_770.field_797 = 0.58119464F;
      this.field_771.field_797 = -0.58119464F;
      this.field_772.field_797 = 0.58119464F;
      this.field_773.field_797 = -0.7853982F;
      this.field_774.field_797 = 0.7853982F;
      this.field_767.field_796 = 0.7853982F;
      this.field_768.field_796 = -0.7853982F;
      this.field_769.field_796 = 0.3926991F;
      this.field_770.field_796 = -0.3926991F;
      this.field_771.field_796 = -0.3926991F;
      this.field_772.field_796 = 0.3926991F;
      this.field_773.field_796 = -0.7853982F;
      this.field_774.field_796 = 0.7853982F;
      var3 = -(MathHelper.cos(var1 * 0.6662F * 2.0F) * 0.4F) * var2;
      var4 = -(MathHelper.cos(var1 * 0.6662F * 2.0F + 3.1415927F) * 0.4F) * var2;
      var5 = -(MathHelper.cos(var1 * 0.6662F * 2.0F + 1.5707964F) * 0.4F) * var2;
      var6 = -(MathHelper.cos(var1 * 0.6662F * 2.0F + 4.712389F) * 0.4F) * var2;
      float var7 = Math.abs(MathHelper.sin(var1 * 0.6662F) * 0.4F) * var2;
      float var8 = Math.abs(MathHelper.sin(var1 * 0.6662F + 3.1415927F) * 0.4F) * var2;
      float var9 = Math.abs(MathHelper.sin(var1 * 0.6662F + 1.5707964F) * 0.4F) * var2;
      var1 = Math.abs(MathHelper.sin(var1 * 0.6662F + 4.712389F) * 0.4F) * var2;
      this.field_767.field_796 += var3;
      this.field_768.field_796 -= var3;
      this.field_769.field_796 += var4;
      this.field_770.field_796 -= var4;
      this.field_771.field_796 += var5;
      this.field_772.field_796 -= var5;
      this.field_773.field_796 += var6;
      this.field_774.field_796 -= var6;
      this.field_767.field_797 += var7;
      this.field_768.field_797 -= var7;
      this.field_769.field_797 += var8;
      this.field_770.field_797 -= var8;
      this.field_771.field_797 += var9;
      this.field_772.field_797 -= var9;
      this.field_773.field_797 += var1;
      this.field_774.field_797 -= var1;
   }
}
