package net.minecraft.client.models;

// $FF: renamed from: net.minecraft.client.animals.projectiles
public final class class_165 extends QuadpedalModel {
   public class_165() {
      super(12, 0.0F);
      this.head = new ModelBox(0, 0);
      this.head.set_dimensions(-3.0F, -4.0F, -4.0F, 6, 6, 6, 0.6F);
      this.head.set_position(0.0F, 6.0F, -8.0F);
      this.torso = new ModelBox(28, 8);
      this.torso.set_dimensions(-4.0F, -10.0F, -7.0F, 8, 16, 6, 1.75F);
      this.torso.set_position(0.0F, 5.0F, 2.0F);
      this.left_front_leg = new ModelBox(0, 16);
      this.left_front_leg.set_dimensions(-2.0F, 0.0F, -2.0F, 4, 6, 4, 0.5F);
      this.left_front_leg.set_position(-3.0F, 12.0F, 7.0F);
      this.right_front_leg = new ModelBox(0, 16);
      this.right_front_leg.set_dimensions(-2.0F, 0.0F, -2.0F, 4, 6, 4, 0.5F);
      this.right_front_leg.set_position(3.0F, 12.0F, 7.0F);
      this.left_hind_leg = new ModelBox(0, 16);
      this.left_hind_leg.set_dimensions(-2.0F, 0.0F, -2.0F, 4, 6, 4, 0.5F);
      this.left_hind_leg.set_position(-3.0F, 12.0F, -5.0F);
      this.right_hind_leg = new ModelBox(0, 16);
      this.right_hind_leg.set_dimensions(-2.0F, 0.0F, -2.0F, 4, 6, 4, 0.5F);
      this.right_hind_leg.set_position(3.0F, 12.0F, -5.0F);
   }
}
