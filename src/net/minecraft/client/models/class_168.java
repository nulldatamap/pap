package net.minecraft.client.models;

import net.minecraft.game.util.Vector;

// $FF: renamed from: net.minecraft.client.animals.j
public final class class_168 {
   // $FF: renamed from: game net.minecraft.game.projectiles.game
   public Vector field_803;
   // $FF: renamed from: animals float
   public float field_804;
   // $FF: renamed from: entities float
   public float field_805;

   public class_168(float var1, float var2, float var3, float var4, float var5) {
      this(new Vector(var1, var2, var3), var4, var5);
   }

   // $FF: renamed from: game (float, float) net.minecraft.client.animals.j
   public final class_168 method_466(float var1, float var2) {
      return new class_168(this, var1, var2);
   }

   private class_168(class_168 var1, float var2, float var3) {
      this.field_803 = var1.field_803;
      this.field_804 = var2;
      this.field_805 = var3;
   }

   private class_168(Vector var1, float var2, float var3) {
      this.field_803 = var1;
      this.field_804 = var2;
      this.field_805 = var3;
   }
}
