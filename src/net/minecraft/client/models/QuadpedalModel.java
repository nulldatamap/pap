package net.minecraft.client.models;

import a.MathHelper;

// $FF: renamed from: net.minecraft.client.animals.animals
public class QuadpedalModel extends Model {
   // $FF: renamed from: game net.minecraft.client.animals.entities
   public ModelBox head = new ModelBox(0, 0);
   // $FF: renamed from: animals net.minecraft.client.animals.entities
   public ModelBox torso;
   // $FF: renamed from: entities net.minecraft.client.animals.entities
   public ModelBox left_front_leg;
   // $FF: renamed from: projectiles net.minecraft.client.animals.entities
   public ModelBox right_front_leg;
   // $FF: renamed from: player net.minecraft.client.animals.entities
   public ModelBox left_hind_leg;
   // $FF: renamed from: particles net.minecraft.client.animals.entities
   public ModelBox right_hind_leg;

   public QuadpedalModel(int var1, float var2) {
      this.head.set_dimensions(-4.0F, -4.0F, -8.0F, 8, 8, 8, 0.0F);
      this.head.set_position(0.0F, (float)(18 - var1), -6.0F);
      this.torso = new ModelBox(28, 8);
      this.torso.set_dimensions(-5.0F, -10.0F, -7.0F, 10, 16, 8, 0.0F);
      this.torso.set_position(0.0F, (float)(17 - var1), 2.0F);
      this.left_front_leg = new ModelBox(0, 16);
      this.left_front_leg.set_dimensions(-2.0F, 0.0F, -2.0F, 4, var1, 4, 0.0F);
      this.left_front_leg.set_position(-3.0F, (float)(24 - var1), 7.0F);
      this.right_front_leg = new ModelBox(0, 16);
      this.right_front_leg.set_dimensions(-2.0F, 0.0F, -2.0F, 4, var1, 4, 0.0F);
      this.right_front_leg.set_position(3.0F, (float)(24 - var1), 7.0F);
      this.left_hind_leg = new ModelBox(0, 16);
      this.left_hind_leg.set_dimensions(-2.0F, 0.0F, -2.0F, 4, var1, 4, 0.0F);
      this.left_hind_leg.set_position(-3.0F, (float)(24 - var1), -5.0F);
      this.right_hind_leg = new ModelBox(0, 16);
      this.right_hind_leg.set_dimensions(-2.0F, 0.0F, -2.0F, 4, var1, 4, 0.0F);
      this.right_hind_leg.set_position(3.0F, (float)(24 - var1), -5.0F);
   }

   // $FF: renamed from: game (float, float, float, float, float, float) void
   public final void animate_and_render(float var1, float var2, float var3, float var4, float var5, float var6) {
      this.animate(var1, var2, var3, var4, var5, 1.0F);
      this.head.render(1.0F);
      this.torso.render(1.0F);
      this.left_front_leg.render(1.0F);
      this.right_front_leg.render(1.0F);
      this.left_hind_leg.render(1.0F);
      this.right_hind_leg.render(1.0F);
   }

   // $FF: renamed from: animals (float, float, float, float, float, float) void
   public final void animate(float var1, float var2, float var3, float var4, float var5, float var6) {
      this.head.field_796 = var4 / 57.295776F;
      this.head.field_795 = var5 / 57.295776F;
      this.torso.field_795 = 1.5707964F;
      this.left_front_leg.field_795 = MathHelper.cos(var1 * 0.6662F) * 1.4F * var2;
      this.right_front_leg.field_795 = MathHelper.cos(var1 * 0.6662F + 3.1415927F) * 1.4F * var2;
      this.left_hind_leg.field_795 = MathHelper.cos(var1 * 0.6662F + 3.1415927F) * 1.4F * var2;
      this.right_hind_leg.field_795 = MathHelper.cos(var1 * 0.6662F) * 1.4F * var2;
   }
}
