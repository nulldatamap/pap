package net.minecraft.client.gamemode;

import net.minecraft.game.world.World;
import net.minecraft.game.world.MobSpawner;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.entities.player.Player;
import net.minecraft.client.UsernameAndBlocks;
import net.minecraft.client.Game;

// $FF: renamed from: net.minecraft.client.projectiles.entities
public final class CreativeMode extends GameMode {
   // $FF: renamed from: entities net.minecraft.game.game.animals
   private MobSpawner mob_spawner;

   public CreativeMode(Game var1) {
      super( var1 );
   }

   // $FF: renamed from: game (net.minecraft.game.entities.player.game) void
   public final void initialize_player(Player player) {
      for(int var2 = 0; var2 < 9; ++var2) {
         if (player.inventory.main_area[var2] == null) {
            this.game.player.inventory.main_area[var2] = new ItemStack(((Block) UsernameAndBlocks.blocks.get(var2)).block_id);
         } else {
            this.game.player.inventory.main_area[var2].count = 1;
         }
      }

   }

   // $FF: renamed from: projectiles () boolean
   public final boolean ui_enabled() {
      return false;
   }

   // $FF: renamed from: game (net.minecraft.game.game.player) void
   public final void configure_world(World world) {
      super.configure_world(world);
      world.can_take_damage = false;
      this.mob_spawner = new MobSpawner(world);
   }

   // $FF: renamed from: entities () void
   public final void method_570() {
      this.mob_spawner.spawn_mobs();
   }
}
