package net.minecraft.client.gamemode;

import net.minecraft.game.world.MobSpawner;
import net.minecraft.game.world.World;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.world.block.SoundMaterial;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.items.Item;
import net.minecraft.client.Game;
import net.minecraft.client.sound.SoundManager;
import net.minecraft.client.player.LocalPlayer;

// $FF: renamed from: net.minecraft.client.projectiles.animals
public final class SurvivalMode extends GameMode {
   // $FF: renamed from: entities int
   private int field_1007 = -1;
   // $FF: renamed from: projectiles int
   private int field_1008 = -1;
   // $FF: renamed from: player int
   private int field_1009 = -1;
   // $FF: renamed from: particles float
   private float field_1010 = 0.0F;
   // $FF: renamed from: player float
   private float field_1011 = 0.0F;
   // $FF: renamed from: h float
   private float field_1012 = 0.0F;
   // $FF: renamed from: i int
   private int field_1013 = 0;
   // $FF: renamed from: j net.minecraft.game.game.animals
   private MobSpawner field_1014;

   public SurvivalMode(Game var1) {
      super(var1);
   }

   // $FF: renamed from: animals (int, int, int) boolean
   public final boolean hit_block(int x, int y, int z) {
      int var4 = this.game.world.block_at(x, y, z);
      byte var5 = this.game.world.block_data_at(x, y, z);
      boolean var6 = super.hit_block(x, y, z);
      LocalPlayer var7 = this.game.player;
      ItemStack var9;
      if ((var9 = this.game.player.inventory.selected_item()) != null) {
         Item.items[var9.id].method_91(var9);
         if (var9.count == 0) {
            this.game.player.method_78();
         }
      }

      if (var6 && this.game.player.method_73(Block.blocks[var4])) {
         Block.blocks[var4].method_515(this.game.world, x, y, z, var5);
      }

      return var6;
   }

   // $FF: renamed from: game (int, int, int) void
   public final void hit_block_also(int var1, int var2, int var3) {
      int var4;
      if ((var4 = this.game.world.block_at(var1, var2, var3)) > 0 && Block.blocks[var4].method_514(this.game.player) >= 1.0F) {
         this.hit_block(var1, var2, var3);
      }

   }

   // $FF: renamed from: game () void
   public final void method_567() {
      this.field_1010 = 0.0F;
      this.field_1013 = 0;
   }

   // $FF: renamed from: game (int, int, int, int) void
   public final void method_566(int var1, int var2, int var3, int var4) {
      if (this.field_1013 > 0) {
         --this.field_1013;
      } else {
         super.method_566(var1, var2, var3, var4);
         if (var1 == this.field_1007 && var2 == this.field_1008 && var3 == this.field_1009) {
            if ((var4 = this.game.world.block_at(var1, var2, var3)) != 0) {
               Block var6 = Block.blocks[var4];
               this.field_1010 += var6.method_514(this.game.player);
               if (this.field_1012 % 4.0F == 0.0F && var6 != null) {
                  SoundManager var10000 = this.game.sound_manager;
                  String var10001 = var6.sound_material.step_sound();
                  float var10002 = (float)var1 + 0.5F;
                  float var10003 = (float)var2 + 0.5F;
                  float var10004 = (float)var3 + 0.5F;
                  SoundMaterial var5 = var6.sound_material;
                  float var10005 = (var6.sound_material.field_958 + 1.0F) / 8.0F;
                  var5 = var6.sound_material;
                  var10000.play_sound(var10001, var10002, var10003, var10004, var10005, var6.sound_material.field_959 * 0.5F);
               }

               ++this.field_1012;
               if (this.field_1010 >= 1.0F) {
                  this.hit_block(var1, var2, var3);
                  this.field_1010 = 0.0F;
                  this.field_1011 = 0.0F;
                  this.field_1012 = 0.0F;
                  this.field_1013 = 5;
               }

            }
         } else {
            this.field_1010 = 0.0F;
            this.field_1011 = 0.0F;
            this.field_1012 = 0.0F;
            this.field_1007 = var1;
            this.field_1008 = var2;
            this.field_1009 = var3;
         }
      }
   }

   // $FF: renamed from: game (float) void
   public final void method_568(float var1) {
      if (this.field_1010 <= 0.0F) {
         this.game.game_renderer.field_653 = 0.0F;
      } else {
         var1 = this.field_1011 + (this.field_1010 - this.field_1011) * var1;
         this.game.game_renderer.field_653 = var1;
      }
   }

   // $FF: renamed from: animals () float
   public final float reach_distance() {
      return 4.0F;
   }

   // $FF: renamed from: game (net.minecraft.game.game.player) void
   public final void configure_world(World world) {
      super.configure_world(world);
      this.field_1014 = new MobSpawner(world);
   }

   // $FF: renamed from: entities () void
   public final void method_570() {
      this.field_1011 = this.field_1010;
      this.field_1014.spawn_mobs();
   }
}
