package net.minecraft.client.gamemode;

import net.minecraft.game.items.ItemStack;
import net.minecraft.game.world.World;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.world.block.SoundMaterial;
import net.minecraft.game.entities.player.Player;
import net.minecraft.client.Game;
import net.minecraft.client.sound.SoundManager;

// $FF: renamed from: net.minecraft.client.projectiles.game
public class GameMode {
   // $FF: renamed from: game net.minecraft.client.projectiles
   protected final Game game;
   // $FF: renamed from: animals boolean
   public boolean field_1005 = false;

   public GameMode(Game g) {
      this.game = g;
   }

   // $FF: renamed from: game (net.minecraft.game.game.player) void
   public void configure_world(World world) {
   }

   // $FF: renamed from: game (int, int, int) void
   public void hit_block_also(int var1, int var2, int var3) {
      this.hit_block(var1, var2, var3);
   }

   // $FF: renamed from: animals (int, int, int) boolean
   public boolean hit_block(int x, int y, int z) {
      this.game.particle_manager.spawn_block_particles(x, y, z);
      World world = this.game.world;
      Block block = Block.blocks[world.block_at(x, y, z)];
      byte data = world.block_data_at(x, y, z);
      boolean var7 = world.method_165(x, y, z, 0);
      if (block != null && var7) {
         SoundManager sm = this.game.sound_manager;
         String sound = block.sound_material.break_sound();
         float cx = (float)x + 0.5F;
         float cy = (float)y + 0.5F;
         float cz = (float)z + 0.5F;
         SoundMaterial soundmat = block.sound_material;
         float var10005 = (block.sound_material.field_958 + 1.0F) / 2.0F;
         soundmat = block.sound_material;
         sm.play_sound(sound, cx, cy, cz, var10005, block.sound_material.field_959 * 0.8F);
         block.method_507(world, x, y, z, data);
      }

      return var7;
   }

   // $FF: renamed from: game (int, int, int, int) void
   public void method_566(int var1, int var2, int var3, int var4) {
   }

   // $FF: renamed from: game () void
   public void method_567() {
   }

   // $FF: renamed from: game (float) void
   public void method_568(float var1) {
   }

   // $FF: renamed from: animals () float
   public float reach_distance() {
      return 5.0F;
   }

   // $FF: renamed from: entities () void
   public void method_570() {
   }

   // $FF: renamed from: projectiles () boolean
   public boolean ui_enabled() {
      return true;
   }

   // $FF: renamed from: game (net.minecraft.game.entities.player.game) void
   public void initialize_player(Player var1) {
      var1.inventory.add_item( new ItemStack(Block.grass, 64) );
   }
}
