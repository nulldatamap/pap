package net.minecraft.client;

import a.class_0;
import com.data.nbt.NbtTag;
import com.data.nbt.CompoundTag;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;
import net.minecraft.client.rendering.GeometryDrawer;
import net.minecraft.client.ui.WidthHeight;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.game
public class class_126 implements class_0 {
   // $FF: renamed from: game java.lang.String
   private String field_596;
   // $FF: renamed from: animals net.minecraft.client.projectiles
   private Game field_597;
   // $FF: renamed from: entities java.lang.String
   private String field_598;
   // $FF: renamed from: projectiles long
   private long field_599;

   public class_126(Game var1) {
      this.field_596 = "";
      this.field_598 = "";
      this.field_599 = System.currentTimeMillis();
      this.field_597 = var1;
   }

   // $FF: renamed from: game (java.lang.String) void
   public final void method_0(String var1) {
      if (!this.field_597.field_578) {
         throw new class_89();
      } else {
         this.field_598 = var1;
         WidthHeight var3;
         int var2 = (var3 = new WidthHeight(this.field_597.width, this.field_597.height)).get_width();
         int var4 = var3.get_height();
         GL11.glClear(256);
         GL11.glMatrixMode(5889);
         GL11.glLoadIdentity();
         GL11.glOrtho(0.0D, (double)var2, (double)var4, 0.0D, 100.0D, 300.0D);
         GL11.glMatrixMode(5888);
         GL11.glLoadIdentity();
         GL11.glTranslatef(0.0F, 0.0F, -200.0F);
      }
   }

   // $FF: renamed from: animals (java.lang.String) void
   public final void method_1(String var1) {
      if (!this.field_597.field_578) {
         throw new class_89();
      } else {
         this.field_599 = 0L;
         this.field_596 = var1;
         this.method_2(-1);
         this.field_599 = 0L;
      }
   }

   // $FF: renamed from: game (int) void
   public final void method_2(int var1) {
      if (!this.field_597.field_578) {
         throw new class_89();
      } else {
         long var2;
         if ((var2 = System.currentTimeMillis()) - this.field_599 >= 20L) {
            this.field_599 = var2;
            WidthHeight var8;
            int var3 = (var8 = new WidthHeight(this.field_597.width, this.field_597.height)).get_width();
            int var9 = var8.get_height();
            GL11.glClear(256);
            GL11.glMatrixMode(5889);
            GL11.glLoadIdentity();
            GL11.glOrtho(0.0D, (double)var3, (double)var9, 0.0D, 100.0D, 300.0D);
            GL11.glMatrixMode(5888);
            GL11.glLoadIdentity();
            GL11.glTranslatef(0.0F, 0.0F, -200.0F);
            GL11.glClear(16640);
            GeometryDrawer var4 = GeometryDrawer.singleton;
            int var5 = this.field_597.resource_manager.load_texture("/dirt.png");
            GL11.glBindTexture(3553, var5);
            var4.quads_mode();
            var4.set_color(4210752);
            var4.add_vertex(0.0F, (float)var9, 0.0F, 0.0F, (float)var9 / 32.0F);
            var4.add_vertex((float)var3, (float)var9, 0.0F, (float)var3 / 32.0F, (float)var9 / 32.0F);
            var4.add_vertex((float)var3, 0.0F, 0.0F, (float)var3 / 32.0F, 0.0F);
            var4.add_vertex(0.0F, 0.0F, 0.0F, 0.0F, 0.0F);
            var4.draw();
            if (var1 >= 0) {
               var5 = var3 / 2 - 50;
               int var6 = var9 / 2 + 16;
               GL11.glDisable(3553);
               var4.quads_mode();
               var4.set_color(8421504);
               var4.add_vertex((float)var5, (float)var6, 0.0F);
               var4.add_vertex((float)var5, (float)(var6 + 2), 0.0F);
               var4.add_vertex((float)(var5 + 100), (float)(var6 + 2), 0.0F);
               var4.add_vertex((float)(var5 + 100), (float)var6, 0.0F);
               var4.set_color(8454016);
               var4.add_vertex((float)var5, (float)var6, 0.0F);
               var4.add_vertex((float)var5, (float)(var6 + 2), 0.0F);
               var4.add_vertex((float)(var5 + var1), (float)(var6 + 2), 0.0F);
               var4.add_vertex((float)(var5 + var1), (float)var6, 0.0F);
               var4.draw();
               GL11.glEnable(3553);
            }

            this.field_597.text_drawer.draw_text(this.field_598, (var3 - this.field_597.text_drawer.text_width(this.field_598)) / 2, var9 / 2 - 4 - 16, 16777215);
            this.field_597.text_drawer.draw_text(this.field_596, (var3 - this.field_597.text_drawer.text_width(this.field_596)) / 2, var9 / 2 - 4 + 8, 16777215);
            Display.update();

            try {
               Thread.yield();
            } catch (Exception var7) {
               ;
            }
         }
      }
   }

   public class_126() {
   }

   // $FF: renamed from: game (java.io.InputStream) com.game.game.entities
   public static CompoundTag method_334(InputStream var0) throws IOException {
      DataInputStream var4 = new DataInputStream(new GZIPInputStream(var0));

      CompoundTag var5;
      try {
         NbtTag var1;
         if (!((var1 = NbtTag.method_433(var4)) instanceof CompoundTag)) {
            throw new IOException("Root tag must be game named compound tag");
         }

         var5 = (CompoundTag)var1;
      } finally {
         var4.close();
      }

      return var5;
   }
}
