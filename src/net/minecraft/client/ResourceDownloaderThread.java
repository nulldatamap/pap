package net.minecraft.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

// $FF: renamed from: net.minecraft.client.j
public final class ResourceDownloaderThread extends Thread {
   // $FF: renamed from: game java.io.File
   private File resource_directory;
   // $FF: renamed from: animals net.minecraft.client.projectiles
   private Game game;
   // $FF: renamed from: entities boolean
   private boolean field_533 = false;

   public ResourceDownloaderThread(File var1, Game var2) {
      this.game = var2;
      this.setName("Resource download thread");
      this.setDaemon(true);
      this.resource_directory = new File(var1, "resources/");
      if (!this.resource_directory.exists() && !this.resource_directory.mkdirs()) {
         throw new RuntimeException("The working directory could not be created: " + this.resource_directory);
      }
   }

   public void run() {
      try {
         URL url = new URL("http://s3.amazonaws.com/MinecraftResources/");
         DocumentBuilderFactory documentbuilderfactory = DocumentBuilderFactory.newInstance();
         DocumentBuilder documentbuilder = documentbuilderfactory.newDocumentBuilder();
         Document document = documentbuilder.parse(url.openStream());
         NodeList nodelist = document.getElementsByTagName("Contents");

         for(int i = 0; i < 2; ++i) {
            for(int j = 0; j < nodelist.getLength(); ++j) {
               Node node = nodelist.item(j);
               if (node.getNodeType() == 1) {
                  Element element = (Element)node;
                  String s = ((Element)element.getElementsByTagName("Key").item(0)).getChildNodes().item(0).getNodeValue();
                  long l = Long.parseLong(((Element)element.getElementsByTagName("Size").item(0)).getChildNodes().item(0).getNodeValue());
                  if (l > 0L) {
                     this.downloadAndInstallResource(url, s, l, i);
                     if (this.field_533) {
                        return;
                     }
                  }
               }
            }
         }
      } catch (Exception var13) {
         this.loadResource(this.resource_directory, "");
         var13.printStackTrace();
      }

   }

   public void reloadResources() {
      this.loadResource(this.resource_directory, "");
   }

   private void loadResource(File file, String s) {
      File[] afile = file.listFiles();

      for(int i = 0; i < afile.length; ++i) {
         if (afile[i].isDirectory()) {
            this.loadResource(afile[i], s + afile[i].getName() + "/");
         } else {
            try {
               this.installResource(s + afile[i].getName(), afile[i]);
            } catch (Exception var6) {
               System.out.println("Failed to add " + s + afile[i].getName());
            }
         }
      }

   }

   public void installResource(String s, File file) {
      int i = s.indexOf("/");
      String s1 = s.substring(0, i);
      s = s.substring(i + 1);
      if (s1.equalsIgnoreCase("sound")) {
         this.game.sound_manager.load_sound(s, file);
      } else if (s1.equalsIgnoreCase("newsound")) {
         this.game.sound_manager.load_sound(s, file);
      } else if (s1.equalsIgnoreCase("music")) {
         this.game.sound_manager.load_music(s, file);
      }

   }

   private void downloadAndInstallResource(URL url, String s, long l, int i) {
      try {
         int j = s.indexOf("/");
         String s1 = s.substring(0, j);
         if (!s1.equals("sound") && !s1.equals("newsound")) {
            if (i != 1) {
               return;
            }
         } else if (i != 0) {
            return;
         }

         File file = new File(this.resource_directory, s);
         if (!file.exists() || file.length() != l) {
            file.getParentFile().mkdirs();
            String s2 = s.replaceAll(" ", "%20");
            this.downloadResource(new URL(url, s2), file, l);
            if (this.field_533) {
               return;
            }
         }

         this.installResource(s, file);
      } catch (Exception var10) {
         var10.printStackTrace();
      }

   }

   private void downloadResource(URL url, File file, long l) throws IOException {
      byte[] abyte0 = new byte[4096];
      DataInputStream datainputstream = new DataInputStream(url.openStream());
      DataOutputStream dataoutputstream = new DataOutputStream(new FileOutputStream(file));
      boolean var8 = false;

      int i;
      while((i = datainputstream.read(abyte0)) >= 0) {
         dataoutputstream.write(abyte0, 0, i);
         if (this.field_533) {
            return;
         }
      }

      datainputstream.close();
      dataoutputstream.close();
   }

   // $FF: renamed from: game (java.net.URL, java.io.File) void
   private void method_319(URL var1, File var2) throws IOException {
      byte[] var3 = new byte[4096];
      DataInputStream var5 = new DataInputStream(var1.openStream());
      DataOutputStream var6 = new DataOutputStream(new FileOutputStream(var2));

      int var4;
      while((var4 = var5.read(var3)) >= 0) {
         var6.write(var3, 0, var4);
         if (this.field_533) {
            return;
         }
      }

      var5.close();
      var6.close();
   }

   // $FF: renamed from: game () void
   public final void method_320() {
      this.field_533 = true;
   }
}
