package net.minecraft.client;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.game.world.block.Block;

// $FF: renamed from: net.minecraft.client.l
public final class UsernameAndBlocks {
   // $FF: renamed from: game java.util.List
   public static List blocks;
   // $FF: renamed from: animals java.lang.String
   public String username;
   // $FF: renamed from: entities java.lang.String
   public String field_518;

   public UsernameAndBlocks(String var1, String var2) {
      this.username = var1;
      this.field_518 = var2;
   }

   static {
      (blocks = new ArrayList()).add(Block.stone);
      blocks.add(Block.cobble_stone);
      blocks.add(Block.field_912);
      blocks.add(Block.dirt);
      blocks.add(Block.wood_planks);
      blocks.add(Block.field_884);
      blocks.add(Block.field_885);
      blocks.add(Block.field_917);
      blocks.add(Block.field_911);
      blocks.add(Block.glass);
      blocks.add(Block.field_915);
      blocks.add(Block.sapling);
      blocks.add(Block.field_904);
      blocks.add(Block.field_905);
      blocks.add(Block.field_906);
      blocks.add(Block.field_907);
      blocks.add(Block.sand);
      blocks.add(Block.field_880);
      blocks.add(Block.field_886);
      blocks.add(Block.field_888);
      blocks.add(Block.field_889);
      blocks.add(Block.field_890);
      blocks.add(Block.field_891);
      blocks.add(Block.field_892);
      blocks.add(Block.field_893);
      blocks.add(Block.field_894);
      blocks.add(Block.field_895);
      blocks.add(Block.field_896);
      blocks.add(Block.field_897);
      blocks.add(Block.field_898);
      blocks.add(Block.field_899);
      blocks.add(Block.field_900);
      blocks.add(Block.field_901);
      blocks.add(Block.wool);
      blocks.add(Block.field_903);
      blocks.add(Block.coal);
      blocks.add(Block.iron);
      blocks.add(Block.gold);
      blocks.add(Block.field_909);
      blocks.add(Block.field_908);
      blocks.add(Block.field_914);
      blocks.add(Block.tnt);
      blocks.add(Block.field_916);
      System.out.println(blocks.size());
   }
}
