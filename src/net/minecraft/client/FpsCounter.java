package net.minecraft.client;

// $FF: renamed from: net.minecraft.client.player
public final class FpsCounter {
   // $FF: renamed from: game float
   float field_583 = 20.0F;
   // $FF: renamed from: projectiles double
   private double field_584;
   // $FF: renamed from: animals int
   public int field_585;
   // $FF: renamed from: entities float
   public float field_586;
   // $FF: renamed from: player float
   private float field_587 = 1.0F;
   // $FF: renamed from: particles float
   private float field_588 = 0.0F;
   // $FF: renamed from: player long
   private long field_589 = System.currentTimeMillis();
   // $FF: renamed from: h long
   private long field_590 = System.nanoTime() / 1000000L;
   // $FF: renamed from: i double
   private double field_591 = 1.0D;

   public FpsCounter(float var1) {
   }

   // $FF: renamed from: game () void
   public final void method_333() {
      long var1;
      long var3 = (var1 = System.currentTimeMillis()) - this.field_589;
      long var5 = System.nanoTime() / 1000000L;
      double var9;
      if (var3 > 1000L) {
         long var7 = var5 - this.field_590;
         var9 = (double)var3 / (double)var7;
         this.field_591 += (var9 - this.field_591) * 0.20000000298023224D;
         this.field_589 = var1;
         this.field_590 = var5;
      }

      if (var3 < 0L) {
         this.field_589 = var1;
         this.field_590 = var5;
      }

      double var11;
      var9 = ((var11 = (double)var5 / 1000.0D) - this.field_584) * this.field_591;
      this.field_584 = var11;
      if (var9 < 0.0D) {
         var9 = 0.0D;
      }

      if (var9 > 1.0D) {
         var9 = 1.0D;
      }

      this.field_588 = (float)((double)this.field_588 + var9 * (double)this.field_587 * (double)this.field_583);
      this.field_585 = (int)this.field_588;
      this.field_588 -= (float)this.field_585;
      if (this.field_585 > 10) {
         this.field_585 = 10;
      }

      this.field_586 = this.field_588;
   }
}
