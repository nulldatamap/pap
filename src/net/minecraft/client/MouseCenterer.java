package net.minecraft.client;

import java.awt.AWTException;
import java.awt.Component;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Robot;
import java.nio.IntBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Cursor;
import org.lwjgl.input.Mouse;

// $FF: renamed from: net.minecraft.client.m
public final class MouseCenterer {
   // $FF: renamed from: entities java.awt.Component
   private Component component;
   // $FF: renamed from: projectiles java.awt.Robot
   private Robot robot;
   // $FF: renamed from: player int
   private int field_503;
   // $FF: renamed from: particles int
   private int field_504;
   // $FF: renamed from: player org.lwjgl.input.Cursor
   private Cursor cursor;
   // $FF: renamed from: game int
   public int field_506;
   // $FF: renamed from: animals int
   public int field_507;
   // $FF: renamed from: h int
   private int field_508 = 10;

   public MouseCenterer(Component cmp) {
      this.component = cmp;

      try {
         this.robot = new Robot();
      } catch (AWTException var4) {
         var4.printStackTrace();
      }

      IntBuffer buf;
      (buf = BufferUtils.createIntBuffer(1)).put(0);
      buf.flip();
      IntBuffer var2 = BufferUtils.createIntBuffer(1024);

      try {
         this.cursor = new Cursor(32, 32, 16, 16, 1, var2, buf);
      } catch (LWJGLException var3) {
         var3.printStackTrace();
      }
   }

   // $FF: renamed from: game () void
   public final void method_312() {
      try {
         Mouse.setNativeCursor(this.cursor);
      } catch (LWJGLException var2) {
         var2.printStackTrace();
      }

      this.method_313();
      this.field_506 = 0;
      this.field_507 = 0;
   }

   // $FF: renamed from: animals () void
   public final void method_313() {
      Point var1 = MouseInfo.getPointerInfo().getLocation();
      Point var2 = this.component.getLocationOnScreen();
      this.robot.mouseMove(this.field_503, this.field_504);
      this.field_503 = var2.x + this.component.getWidth() / 2;
      this.field_504 = var2.y + this.component.getHeight() / 2;
      if (this.field_508 == 0) {
         this.field_506 = var1.x - this.field_503;
         this.field_507 = var1.y - this.field_504;
      } else {
         this.field_506 = this.field_507 = 0;
         --this.field_508;
      }
   }
}
