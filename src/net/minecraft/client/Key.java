package net.minecraft.client;

// $FF: renamed from: net.minecraft.client.h
public final class Key {
   // $FF: renamed from: game java.lang.String
   public String key_name;
   // $FF: renamed from: animals int
   public int key_code;

   public Key(String key_name, int key_code) {
      this.key_name = key_name;
      this.key_code = key_code;
   }
}
