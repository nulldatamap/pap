package net.minecraft.client.player;

import com.data.nbt.ListTag;
import com.data.nbt.CompoundTag;
import net.minecraft.game.Storage;
import net.minecraft.game.world.World;
import net.minecraft.game.world.block.a.FurnaceData;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.entities.Entity;
import net.minecraft.game.entities.player.Player;
import net.minecraft.game.entities.player.Inventory;
import net.minecraft.client.UsernameAndBlocks;
import net.minecraft.client.Game;
import net.minecraft.client.ui.storage.class_105;
import net.minecraft.client.ui.storage.class_106;
import net.minecraft.client.ui.storage.class_107;
import net.minecraft.client.particles.class_14;

// $FF: renamed from: net.minecraft.client.player.game
public class LocalPlayer extends Player {
   // $FF: renamed from: game net.minecraft.client.player.animals
   public MovementControls controls;
   // $FF: renamed from: an net.minecraft.client.projectiles
   private Game game;

   public LocalPlayer(Game g, World world, UsernameAndBlocks uab) {
      super(world);
      this.game = g;
      if (uab != null) {
         this.texture_url = "http://www.minecraft.net/skin/" + uab.username + ".png";
      }

   }

   // $FF: renamed from: player () void
   public final void update_ai() {
      this.walk_dir_x = this.controls.accel_x;
      this.walk_dir_z = this.controls.accel_y;
      this.want_to_jump = this.controls.jump;
   }

   // $FF: renamed from: particles () void
   public final void update_movement() {
      this.controls.update_controls();
      super.update_movement();
   }

   // $FF: renamed from: game (com.game.game.entities) void
   protected final void write_tag_extra(CompoundTag tag) {
      super.write_tag_extra(tag);
      tag.write_int("Score", this.score);
      Inventory inv = this.inventory;
      ListTag inv_tag = new ListTag();
      Inventory var5 = inv;

      int var3;
      CompoundTag var4;
      for(var3 = 0; var3 < var5.main_area.length; ++var3) {
         if (var5.main_area[var3] != null) {
            (var4 = new CompoundTag()).write_byte("Slot", (byte)var3);
            var5.main_area[var3].method_592(var4);
            inv_tag.method_436(var4);
         }
      }

      for(var3 = 0; var3 < var5.armor_area.length; ++var3) {
         if (var5.armor_area[var3] != null) {
            (var4 = new CompoundTag()).write_byte("Slot", (byte)(var3 + 100));
            var5.armor_area[var3].method_592(var4);
            inv_tag.method_436(var4);
         }
      }

      tag.write_list("Inventory", inv_tag);
   }

   // $FF: renamed from: animals (com.game.game.entities) void
   protected final void read_tag_extra(CompoundTag var1) {
      super.read_tag_extra(var1);
      this.score = var1.method_452("Score");
      ListTag var6 = var1.read_list("Inventory");
      ListTag var2 = var6;
      Inventory var7 = this.inventory;
      this.inventory.main_area = new ItemStack[36];
      var7.armor_area = new ItemStack[4];

      for(int var3 = 0; var3 < var2.method_438(); ++var3) {
         CompoundTag var4;
         int var5;
         if ((var5 = (var4 = (CompoundTag)var2.get(var3)).read_byte("Slot") & 255) >= 0 && var5 < var7.main_area.length) {
            var7.main_area[var5] = new ItemStack(var4);
         }

         if (var5 >= 100 && var5 < var7.armor_area.length + 100) {
            var7.armor_area[var5 - 100] = new ItemStack(var4);
         }
      }

   }

   // $FF: renamed from: game () java.lang.String
   protected final String get_id() {
      return "LocalPlayer";
   }

   // $FF: renamed from: game (net.minecraft.game.animals) void
   public final void method_74(Storage var1) {
      this.game.method_322(new class_107(this.inventory, var1));
   }

   // $FF: renamed from: g_ () void
   public final void method_75() {
      this.game.method_322(new class_105(this.inventory));
   }

   // $FF: renamed from: game (net.minecraft.game.game.animals.game.animals) void
   public final void method_77(FurnaceData var1) {
      this.game.method_322(new class_106(this.inventory, var1));
   }

   // $FF: renamed from: h_ () void
   public final void method_78() {
      this.inventory.set_slot(this.inventory.selected_slot, (ItemStack)null);
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals) void
   public final void method_76(Entity var1) {
      this.game.particle_manager.spawn_particle(new class_14(this.game.world, var1, this, -0.5F));
   }
}
