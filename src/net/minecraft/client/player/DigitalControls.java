package net.minecraft.client.player;

import net.minecraft.client.GameOptions;

// $FF: renamed from: net.minecraft.client.player.entities
public final class DigitalControls extends MovementControls {
   // $FF: renamed from: projectiles boolean[]
   private boolean[] keys = new boolean[10];
   // $FF: renamed from: player net.minecraft.client.q
   private GameOptions options;

   public DigitalControls(GameOptions var1) {
      this.options = var1;
   }

   // $FF: renamed from: game (int, boolean) void
   public final void on_key(int key, boolean pressed) {
      byte idx = -1;
      if (key == this.options.forward_key.key_code) {
         idx = 0;
      }

      if (key == this.options.back_key.key_code) {
         idx = 1;
      }

      if (key == this.options.left_key.key_code) {
         idx = 2;
      }

      if (key == this.options.right_key.key_code) {
         idx = 3;
      }

      if (key == this.options.jump_key.key_code) {
         idx = 4;
      }

      if (idx >= 0) {
         this.keys[idx] = pressed;
      }

   }

   // $FF: renamed from: animals () void
   public final void reset_controls() {
      for(int var1 = 0; var1 < 10; ++var1) {
         this.keys[var1] = false;
      }

   }

   // $FF: renamed from: game () void
   public final void update_controls() {
      this.accel_x = 0.0F;
      this.accel_y = 0.0F;
      if (this.keys[0]) {
         ++this.accel_y;
      }

      if (this.keys[1]) {
         --this.accel_y;
      }

      if (this.keys[2]) {
         ++this.accel_x;
      }

      if (this.keys[3]) {
         --this.accel_x;
      }

      this.jump = this.keys[4];
   }
}
