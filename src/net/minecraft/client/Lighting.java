package net.minecraft.client;

import java.nio.FloatBuffer;
import net.minecraft.game.util.Vector;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.entities
public final class Lighting {
   // $FF: renamed from: game java.nio.FloatBuffer
   private static FloatBuffer float_buffer = BufferUtils.createFloatBuffer(16);

   // $FF: renamed from: game () void
   public static void disable_lighting() {
      GL11.glDisable(GL11.GL_LIGHTING);
      GL11.glDisable(GL11.GL_LIGHT0);
      GL11.glDisable(GL11.GL_LIGHT1);
      GL11.glDisable(GL11.GL_COLOR_MATERIAL);
   }

   // $FF: renamed from: animals () void
   public static void enable_lighting() {
      GL11.glEnable(GL11.GL_LIGHTING);
      GL11.glEnable(GL11.GL_LIGHT0);
      GL11.glEnable(GL11.GL_LIGHT1);
      GL11.glEnable(GL11.GL_COLOR_MATERIAL);
      GL11.glColorMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_AMBIENT_AND_DIFFUSE);
      Vector pos = new Vector(0.3F, 1.0F, -0.7F);
      GL11.glLight(GL11.GL_LIGHT0, GL11.GL_POSITION, to_buf(pos.x, pos.y, pos.z, 0.0F));
      GL11.glLight(GL11.GL_LIGHT0, GL11.GL_DIFFUSE, to_buf(0.5F, 0.5F, 0.5F, 1.0F));
      GL11.glLight(GL11.GL_LIGHT0, GL11.GL_AMBIENT, to_buf(0.0F, 0.0F, 0.0F, 1.0F));
      GL11.glLight(GL11.GL_LIGHT0, GL11.GL_SPECULAR, to_buf(0.0F, 0.0F, 0.0F, 1.0F));
      pos = new Vector(-0.7F, 1.0F, 0.2F);
      GL11.glLight(GL11.GL_LIGHT1, GL11.GL_POSITION, to_buf(pos.x, pos.y, pos.z, 0.0F));
      GL11.glLight(GL11.GL_LIGHT1, GL11.GL_DIFFUSE, to_buf(0.5F, 0.5F, 0.5F, 1.0F));
      GL11.glLight(GL11.GL_LIGHT1, GL11.GL_AMBIENT, to_buf(0.0F, 0.0F, 0.0F, 1.0F));
      GL11.glLight(GL11.GL_LIGHT1, GL11.GL_SPECULAR, to_buf(0.0F, 0.0F, 0.0F, 1.0F));
      GL11.glShadeModel(GL11.GL_SMOOTH);
      GL11.glLightModel(GL11.GL_LIGHT_MODEL_AMBIENT, to_buf(0.5F, 0.5F, 0.5F, 1.0F));
   }

   // $FF: renamed from: game (float, float, float, float) java.nio.FloatBuffer
   private static FloatBuffer to_buf(float x, float y, float z, float w) {
      float_buffer.clear();
      float_buffer.put(x).put(y).put(z).put(w);
      float_buffer.flip();
      return float_buffer;
   }
}
