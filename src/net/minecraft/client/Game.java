package net.minecraft.client;

import java.awt.Canvas;
import java.awt.Component;
import java.io.File;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import javax.swing.JOptionPane;
import net.minecraft.game.world.World;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.world.gen.WorldGenerator;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.items.Item;
import net.minecraft.game.entities.Mob;
import net.minecraft.game.entities.player.Inventory;
import net.minecraft.game.util.RaycastResult;
import net.minecraft.client.rendering.WorldRenderer;
import net.minecraft.client.rendering.GameRenderer;
import net.minecraft.client.rendering.ResourceManager;
import net.minecraft.client.rendering.RenderChunk;
import net.minecraft.client.rendering.c.class_174;
import net.minecraft.client.rendering.c.class_175;
import net.minecraft.client.rendering.c.class_176;
import net.minecraft.client.rendering.c.class_177;
import net.minecraft.client.rendering.c.class_178;
import net.minecraft.client.models.MobModel;
import net.minecraft.client.ui.EscMenu;
import net.minecraft.client.ui.class_103;
import net.minecraft.client.ui.WidthHeight;
import net.minecraft.client.ui.TextDrawer;
import net.minecraft.client.ui.HudUI;
import net.minecraft.client.ui.ClickableElement;
import net.minecraft.client.ui.DeathScreen;
import net.minecraft.client.ui.storage.class_108;
import net.minecraft.client.gamemode.GameMode;
import net.minecraft.client.gamemode.CreativeMode;
import net.minecraft.client.gamemode.SurvivalMode;
import net.minecraft.client.sound.SoundManager;
import net.minecraft.client.particles.ParticleManager;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.client.player.DigitalControls;
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Controllers;
import org.lwjgl.input.Cursor;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.ContextCapabilities;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;
import net.minecraft.game.entities.Entity;

// $FF: renamed from: net.minecraft.client.projectiles
public final class Game implements Runnable {
   // $FF: renamed from: game net.minecraft.client.projectiles.game
   public GameMode game_mode = new SurvivalMode(this);
   // $FF: renamed from: E boolean
   private boolean field_541 = false;
   // $FF: renamed from: animals int
   public int width;
   // $FF: renamed from: entities int
   public int height;
   // $FF: renamed from: F net.minecraft.client.i
   private class_118 field_544;
   // $FF: renamed from: G net.minecraft.client.player
   private FpsCounter field_545 = new FpsCounter(20.0F);
   // $FF: renamed from: projectiles net.minecraft.game.game.player
   public World world;
   // $FF: renamed from: player net.minecraft.client.game.animals
   public WorldRenderer game_renderer;
   // $FF: renamed from: particles net.minecraft.client.player.game
   public LocalPlayer player;
   // $FF: renamed from: player net.minecraft.client.particles.projectiles
   public ParticleManager particle_manager;
   // $FF: renamed from: h net.minecraft.client.l
   public UsernameAndBlocks account = null;
   // $FF: renamed from: i java.lang.String
   public String field_551;
   // $FF: renamed from: j java.awt.Canvas
   public Canvas canvas;
   // $FF: renamed from: k boolean
   public boolean field_553 = true;
   // $FF: renamed from: l boolean
   public volatile boolean is_running = false;
   // $FF: renamed from: m net.minecraft.client.game.k
   public ResourceManager resource_manager;
   // $FF: renamed from: n net.minecraft.client.entities.j
   public TextDrawer text_drawer;
   // $FF: renamed from: o net.minecraft.client.entities.i
   public ClickableElement field_557 = null;
   // $FF: renamed from: p net.minecraft.client.game
   public class_126 field_558 = new class_126(this);
   // $FF: renamed from: q net.minecraft.client.game.player
   public GameRenderer renderer = new GameRenderer(this);
   // $FF: renamed from: H net.minecraft.client.j
   private ResourceDownloaderThread download_thread;
   // $FF: renamed from: I int
   private int field_561 = 0;
   // $FF: renamed from: J int
   private int field_562 = 0;
   // $FF: renamed from: K int
   private int field_563;
   // $FF: renamed from: L int
   private int field_564;
   // $FF: renamed from: r java.lang.String
   public String field_565 = null;
   // $FF: renamed from: s int
   public int field_566 = 0;
   // $FF: renamed from: t net.minecraft.client.entities.l
   public HudUI hud;
   // $FF: renamed from: u boolean
   public boolean field_568 = false;
   // $FF: renamed from: v net.minecraft.game.projectiles.entities
   public RaycastResult looking_at;
   // $FF: renamed from: w net.minecraft.client.q
   public GameOptions options;
   // $FF: renamed from: M net.minecraft.client.MinecraftApplet
   private MinecraftApplet applet;
   // $FF: renamed from: x net.minecraft.client.player.entities
   public SoundManager sound_manager;
   // $FF: renamed from: y net.minecraft.client.m
   public MouseCenterer field_573;
   // $FF: renamed from: z java.io.File
   public File field_574;
   // $FF: renamed from: N java.lang.String
   private String field_575;
   // $FF: renamed from: O net.minecraft.client.game.entities.particles
   private class_174 field_576;
   // $FF: renamed from: P net.minecraft.client.game.entities.projectiles
   private class_175 field_577;
   // $FF: renamed from: A boolean
   volatile boolean field_578;
   // $FF: renamed from: B java.lang.String
   public String field_579;
   // $FF: renamed from: C boolean
   public boolean field_580;
   // $FF: renamed from: Q int
   private int field_581;
   // $FF: renamed from: D boolean
   public boolean field_582;

   public Game(Canvas var1, MinecraftApplet var2, int var3, int var4, boolean var5) {
      new MobModel(0.0F);
      this.looking_at = null;
      this.sound_manager = new SoundManager();
      this.field_575 = null;
      this.field_576 = new class_174();
      this.field_577 = new class_175();
      this.field_578 = false;
      this.field_579 = "";
      this.field_580 = false;
      this.field_581 = 0;
      this.field_582 = false;
      this.field_563 = var3;
      this.field_564 = var4;
      this.field_541 = var5;
      this.applet = var2;
      new SleepThread(this, "Timer hack thread");
      this.canvas = var1;
      this.width = var3;
      this.height = var4;
      this.field_541 = var5;
   }

   // $FF: renamed from: game (java.lang.String, int) void
   public final void method_321(String var1, int var2) {
      this.field_575 = var1;
   }

   // $FF: renamed from: game (net.minecraft.client.entities.i) void
   public final void method_322(ClickableElement var1) {
      if (!(this.field_557 instanceof class_103)) {
         if (this.field_557 != null) {
            this.field_557.method_299();
         }

         if (var1 == null && this.world == null) {
            var1 = new MainScreenElement();
         } else if (var1 == null && this.player.health <= 0) {
            var1 = new DeathScreen();
         }

         this.field_557 = (ClickableElement)var1;
         if (var1 != null) {
            this.method_325();
            WidthHeight var2;
            int var3 = (var2 = new WidthHeight(this.width, this.height)).get_width();
            int var4 = var2.get_height();
            ((ClickableElement)var1).method_294(this, var3, var4);
            this.field_568 = false;
         } else {
            this.method_324();
         }
      }

   }

   // $FF: renamed from: game () void
   public final void method_323() {
      this.shutdown();
   }

   public final void shutdown() {
      try {
         if (this.download_thread != null) {
            this.download_thread.method_320();
         }
      } catch (Exception var5) {
         ;
      }

      boolean var4 = false;

      try {
         var4 = true;
         this.sound_manager.cleanup();
         Mouse.destroy();
         Keyboard.destroy();
         var4 = false;
      } finally {
         if (var4) {
            Display.destroy();
         }

      }

      Display.destroy();
   }

   public final void run() {
      this.field_578 = true;

      try {
         Game var1 = this;
         if (this.canvas != null) {
            Display.setParent(this.canvas);
         } else if (this.field_541) {
            Display.setFullscreen(true);
            this.width = Display.getDisplayMode().getWidth();
            this.height = Display.getDisplayMode().getHeight();
         } else {
            Display.setDisplayMode(new DisplayMode(this.width, this.height));
         }

         Display.setTitle("Minecraft Minecraft Indev");

         try {
            Display.create();
            System.out.println("LWJGL version: " + Sys.getVersion());
            System.out.println("GL RENDERER: " + GL11.glGetString(7937));
            System.out.println("GL VENDOR: " + GL11.glGetString(7936));
            System.out.println("GL VERSION: " + GL11.glGetString(7938));
            ContextCapabilities var2 = GLContext.getCapabilities();
            System.out.println("OpenGL 3.0: " + var2.OpenGL30);
            System.out.println("OpenGL 3.1: " + var2.OpenGL31);
            System.out.println("OpenGL 3.2: " + var2.OpenGL32);
            System.out.println("ARB_compatibility: " + var2.GL_ARB_compatibility);
            if (var2.OpenGL32) {
               IntBuffer var25 = ByteBuffer.allocateDirect(64).order(ByteOrder.nativeOrder()).asIntBuffer();
               GL11.glGetInteger(37158, var25);
               int var26 = var25.get(0);
               System.out.println("PROFILE MASK: " + Integer.toBinaryString(var26));
               System.out.println("CORE PROFILE: " + ((var26 & 1) != 0));
               System.out.println("COMPATIBILITY PROFILE: " + ((var26 & 2) != 0));
            }
         } catch (LWJGLException var19) {
            var19.printStackTrace();

            try {
               Thread.sleep(1000L);
            } catch (InterruptedException var18) {
               ;
            }

            Display.create();
         }

         Keyboard.create();
         Mouse.create();
         this.field_573 = new MouseCenterer(this.canvas);

         try {
            Controllers.create();
         } catch (Exception var17) {
            var17.printStackTrace();
         }

         GL11.glEnable(3553);
         GL11.glShadeModel(7425);
         GL11.glClearDepth(1.0D);
         GL11.glEnable(2929);
         GL11.glDepthFunc(515);
         GL11.glEnable(3008);
         GL11.glAlphaFunc(516, 0.1F);
         GL11.glCullFace(1029);
         GL11.glMatrixMode(5889);
         GL11.glLoadIdentity();
         GL11.glMatrixMode(5888);
         this.field_544 = new class_118();
         String var3 = "decompiled";
         String var27 = System.getProperty("user.home", ".");
         File var29;
         String var4;
         switch(OsData.field_519[((var4 = System.getProperty("os.name").toLowerCase()).contains("win") ? OsEnum.field_536 : (var4.contains("mac") ? OsEnum.field_537 : (var4.contains("solaris") ? OsEnum.field_535 : (var4.contains("sunos") ? OsEnum.field_535 : (var4.contains("linux") ? OsEnum.field_534 : (var4.contains("unix") ? OsEnum.field_534 : OsEnum.field_538)))))).ordinal()]) {
         case 1:
         case 2:
            var29 = new File(var27, '.' + var3 + '/');
            break;
         case 3:
            if ((var4 = System.getenv("APPDATA")) != null) {
               var29 = new File(var4, "." + var3 + '/');
            } else {
               var29 = new File(var27, '.' + var3 + '/');
            }
            break;
         case 4:
            var29 = new File(var27, "Library/Application Support/" + var3);
            break;
         default:
            var29 = new File(var27, var3 + '/');
         }

         if (!var29.exists() && !var29.mkdirs()) {
            throw new RuntimeException("The working directory could not be created: " + var29);
         }

         this.field_574 = var29;
         this.options = new GameOptions(this, this.field_574);
         this.sound_manager.set_options(this.options);
         this.resource_manager = new ResourceManager(this.options);
         this.resource_manager.method_397(this.field_577);
         this.resource_manager.method_397(this.field_576);
         this.resource_manager.method_397(new class_178());
         this.resource_manager.method_397(new class_176(0));
         this.resource_manager.method_397(new class_176(1));
         this.resource_manager.method_397(new class_177(0));
         this.resource_manager.method_397(new class_177(1));
         this.text_drawer = new TextDrawer(this.options, "/default.png", this.resource_manager);
         BufferUtils.createIntBuffer(256).clear().limit(256);
         this.game_renderer = new WorldRenderer(this, this.resource_manager);

         GL11.glViewport(0, 0, this.width, this.height);
         if (this.field_575 != null && this.account != null) {
            World var31;
            (var31 = new World()).method_158(8, 8, 8, new byte[512], new byte[512]);
            this.set_world(var31);
         } else if (this.world == null) {
            this.method_322(new MainScreenElement());
         }

         this.particle_manager = new ParticleManager(this.world, this.resource_manager);

         try {
            var1.download_thread = new ResourceDownloaderThread(var1.field_574, var1);
            var1.download_thread.start();
         } catch (Exception var16) {
            ;
         }

         this.hud = new HudUI(this);
      } catch (Exception var24) {
         var24.printStackTrace();
         JOptionPane.showMessageDialog((Component)null, var24.toString(), "Failed to start Minecraft", 0);
         return;
      }

      long var24 = System.currentTimeMillis();
      int var28 = 0;

      label444:
      while(true) {
         boolean var14 = false;

         label441: {
            try {
               if (this.field_578) {
                  if (this.world != null) {
                     this.world.method_172();
                  }

                  if (this.canvas == null && Display.isCloseRequested()) {
                     this.field_578 = false;
                  }

                  try {
                     if (this.is_running) {
                        float var30 = this.field_545.field_586;
                        this.field_545.method_333();
                        this.field_545.field_586 = var30;
                     } else {
                        this.field_545.method_333();
                     }

                     for(int var32 = 0; var32 < this.field_545.field_585; ++var32) {
                        ++this.field_561;
                        this.method_330();
                     }

                     this.sound_manager.update_listener(this.player, this.field_545.field_586);
                     GL11.glEnable(3553);
                     this.game_mode.method_568(this.field_545.field_586);
                     this.renderer.render_all(this.field_545.field_586);
                     if (!Display.isActive()) {
                        if (this.field_541) {
                           this.method_328();
                        }

                        Thread.sleep(10L);
                     }

                     if (this.canvas != null && !this.field_541 && (this.canvas.getWidth() != this.width || this.canvas.getHeight() != this.height)) {
                        this.width = this.canvas.getWidth();
                        this.height = this.canvas.getHeight();
                        this.method_329(this.width, this.height);
                     }

                     if (this.options.limit_framerate) {
                        Thread.sleep(5L);
                     }

                     ++var28;
                     this.is_running = this.field_557 != null && this.field_557.method_301();
                  } catch (Exception var20) {
                     this.method_322(new class_103("Client error", "The game broke! [" + var20 + "]"));
                     var20.printStackTrace();
                     var14 = false;
                     break;
                  }

                  while(true) {
                     if (System.currentTimeMillis() < var24 + 1000L) {
                        continue label444;
                     }

                     this.field_579 = var28 + " fps, " + RenderChunk.field_727 + " chunk updates";
                     RenderChunk.field_727 = 0;
                     var24 += 1000L;
                     var28 = 0;
                  }
               }

               var14 = false;
               break;
            } catch (class_89 var21) {
               var14 = false;
               break label441;
            } catch (Exception var22) {
               var22.printStackTrace();
               var14 = false;
            } finally {
               if (var14) {
                  this.shutdown();
               }

            }

            this.shutdown();
            return;
         }

         this.shutdown();
         return;
      }

      this.shutdown();
   }

   // $FF: renamed from: animals () void
   public final void method_324() {
      if (Display.isActive() && !this.field_580) {
         this.field_580 = true;
         this.field_573.method_312();
         this.method_322((ClickableElement)null);
         this.field_581 = this.field_561 + 10000;
      }

   }

   // $FF: renamed from: player () void
   private void method_325() {
      if (this.field_580) {
         if (this.player != null) {
            this.player.controls.reset_controls();
         }

         this.field_580 = false;

         try {
            Mouse.setNativeCursor((Cursor)null);
         } catch (LWJGLException var2) {
            var2.printStackTrace();
         }
      }

   }

   // $FF: renamed from: entities () void
   public final void method_326() {
      if (this.field_557 == null) {
         this.method_322(new EscMenu());
      }

   }

   // $FF: renamed from: game (int) void
   private void method_327(int var1) {
      if (var1 != 0 || this.field_562 <= 0) {
         if (var1 == 0) {
            this.renderer.view_model_renderer.method_405();
         }

         ItemStack var2;
         int var3;
         World var5;
         if (var1 == 1 && (var2 = this.player.inventory.selected_item()) != null) {
            var3 = var2.count;
            LocalPlayer var7 = this.player;
            var5 = this.world;
            ItemStack var4;
            if ((var4 = var2.item().on_use(var2, var5, var7)) != var2 || var4 != null && var4.count != var3) {
               this.player.inventory.main_area[this.player.inventory.selected_slot] = var4;
               this.renderer.view_model_renderer.method_406();
               if (var4.count == 0) {
                  this.player.inventory.main_area[this.player.inventory.selected_slot] = null;
               }
            }
         }

         if (this.looking_at == null) {
            if (var1 == 0 && !(this.game_mode instanceof CreativeMode)) {
               this.field_562 = 10;
            }
         } else {
            ItemStack var9;
            if (this.looking_at.field_811 == 1) {
               if (var1 == 0) {
                  Entity var14 = this.looking_at.entity;
                  LocalPlayer var12 = this.player;
                  Inventory var11 = this.player.inventory;
                  int var10000 = (var9 = this.player.inventory.get_slot(var11.selected_slot)) != null ? Item.items[var9.id].method_92() : 1;
                  if (var10000 > 0) {
                     var14.take_damage(var12, var10000);
                     if ((var2 = var12.inventory.selected_item()) != null && var14 instanceof Mob) {
                        Mob var8 = (Mob)var14;
                        Item.items[var2.id].method_90(var2);
                        if (var2.count <= 0) {
                           var12.method_78();
                        }
                     }
                  }

                  return;
               }
            } else if (this.looking_at.field_811 == 0) {
               int var10 = this.looking_at.x;
               var3 = this.looking_at.y;
               int var13 = this.looking_at.z;
               int var15 = this.looking_at.face;
               Block var6 = Block.blocks[this.world.block_at(var10, var3, var13)];
               if (var1 == 0) {
                  this.world.method_213(var10, var3, var13, this.looking_at.face);
                  if (var6 != Block.bedrock) {
                     this.game_mode.hit_block_also(var10, var3, var13);
                     return;
                  }
               } else {
                  var9 = this.player.inventory.selected_item();
                  int var16;
                  if ((var16 = this.world.block_at(var10, var3, var13)) > 0 && Block.blocks[var16].method_525(this.world, var10, var3, var13, this.player)) {
                     return;
                  }

                  if (var9 == null) {
                     return;
                  }

                  var16 = var9.count;
                  var5 = this.world;
                  if (var9.item().activate_on(var9, var5, var10, var3, var13, var15)) {
                     this.renderer.view_model_renderer.method_405();
                  }

                  if (var9.count == 0) {
                     this.player.inventory.main_area[this.player.inventory.selected_slot] = null;
                     return;
                  }

                  if (var9.count != var16) {
                     this.renderer.view_model_renderer.method_404();
                  }
               }
            }
         }
      }

   }

   // $FF: renamed from: projectiles () void
   public final void method_328() {
      try {
         this.field_541 = !this.field_541;
         System.out.println("Toggle fullscreen!");
         if (this.field_541) {
            Display.setDisplayMode(Display.getDesktopDisplayMode());
            this.width = Display.getDisplayMode().getWidth();
            this.height = Display.getDisplayMode().getHeight();
         } else {
            if (this.canvas != null) {
               this.width = this.canvas.getWidth();
               this.height = this.canvas.getHeight();
            } else {
               this.width = this.field_563;
               this.height = this.field_564;
            }

            Display.setDisplayMode(new DisplayMode(this.field_563, this.field_564));
         }

         this.method_325();
         Display.setFullscreen(this.field_541);
         Display.update();
         Thread.sleep(1000L);
         if (this.field_541) {
            this.method_324();
         }

         if (this.field_557 != null) {
            this.method_325();
            this.method_329(this.width, this.height);
         }

         System.out.println("Size: " + this.width + ", " + this.height);
      } catch (Exception var2) {
         var2.printStackTrace();
      }

   }

   // $FF: renamed from: game (int, int) void
   private void method_329(int var1, int var2) {
      this.width = var1;
      this.height = var2;
      if (this.field_557 != null) {
         WidthHeight var3;
         var2 = (var3 = new WidthHeight(var1, var2)).get_width();
         var1 = var3.get_height();
         this.field_557.method_294(this, var2, var1);
      }

   }

   // $FF: renamed from: particles () void
   private void method_330() {
      this.hud.method_287();
      if (!this.is_running && this.world != null) {
         this.game_mode.method_570();
      }

      try {
         GL11.glBindTexture(3553, this.resource_manager.load_texture("/terrain.png"));
      } catch (NullPointerException var7) {
         var7.printStackTrace();
      }

      if (!this.is_running) {
         this.resource_manager.method_398();
      }

      if (this.field_557 == null && this.player != null && this.player.health <= 0) {
         this.method_322((ClickableElement)null);
      }

      if (this.field_557 == null || this.field_557.field_450) {
         label292:
         while(true) {
            while(true) {
               int var1;
               int var2;
               while(Mouse.next()) {
                  if ((var1 = Mouse.getEventDWheel()) != 0) {
                     var2 = var1;
                     Inventory var5 = this.player.inventory;
                     if (var1 > 0) {
                        var2 = 1;
                     }

                     if (var2 < 0) {
                        var2 = -1;
                     }

                     for(var5.selected_slot -= var2; var5.selected_slot < 0; var5.selected_slot += 9) {
                        ;
                     }

                     while(var5.selected_slot >= 9) {
                        var5.selected_slot -= 9;
                     }
                  }

                  if (this.field_557 == null) {
                     if (!this.field_580 && Mouse.getEventButtonState()) {
                        this.method_324();
                     } else {
                        if (Mouse.getEventButton() == 0 && Mouse.getEventButtonState()) {
                           this.method_327(0);
                           this.field_581 = this.field_561;
                        }

                        if (Mouse.getEventButton() == 1 && Mouse.getEventButtonState()) {
                           this.method_327(1);
                           this.field_581 = this.field_561;
                        }

                        if (Mouse.getEventButton() == 2 && Mouse.getEventButtonState() && this.looking_at != null) {
                           if ((var2 = this.world.block_at(this.looking_at.x, this.looking_at.y, this.looking_at.z)) == Block.grass.block_id) {
                              var2 = Block.dirt.block_id;
                           }

                           if (var2 == Block.field_910.block_id) {
                              var2 = Block.field_911.block_id;
                           }

                           if (var2 == Block.bedrock.block_id) {
                              var2 = Block.stone.block_id;
                           }

                           this.player.inventory.select_item(var2);
                        }
                     }
                  } else if (this.field_557 != null) {
                     this.field_557.method_296();
                  }
               }

               if (this.field_562 > 0) {
                  --this.field_562;
               }

               while(true) {
                  while(true) {
                     do {
                        int var7;
                        boolean var3;
                        if (!Keyboard.next()) {
                           if (this.field_557 == null) {
                              if (Mouse.isButtonDown(0) && (float)(this.field_561 - this.field_581) >= this.field_545.field_583 / 4.0F && this.field_580) {
                                 this.method_327(0);
                                 this.field_581 = this.field_561;
                              }

                              if (Mouse.isButtonDown(1) && (float)(this.field_561 - this.field_581) >= this.field_545.field_583 / 4.0F && this.field_580) {
                                 this.method_327(1);
                                 this.field_581 = this.field_561;
                              }
                           }

                           var3 = this.field_557 == null && Mouse.isButtonDown(0) && this.field_580;
                           boolean var8 = false;
                           if (!this.game_mode.field_1005 && this.field_562 <= 0) {
                              if (var3 && this.looking_at != null && this.looking_at.field_811 == 0) {
                                 var2 = this.looking_at.x;
                                 var7 = this.looking_at.y;
                                 int var4 = this.looking_at.z;
                                 this.game_mode.method_566(var2, var7, var4, this.looking_at.face);
                                 this.particle_manager.method_247(var2, var7, var4, this.looking_at.face);
                              } else {
                                 this.game_mode.method_567();
                              }
                           }
                           break label292;
                        }

                        LocalPlayer var10000 = this.player;
                        var7 = Keyboard.getEventKey();
                        var3 = Keyboard.getEventKeyState();
                        var10000.controls.on_key(var7, var3);
                     } while(!Keyboard.getEventKeyState());

                     if (Keyboard.getEventKey() == 87) {
                        this.method_328();
                     } else {
                        if (this.field_557 != null) {
                           this.field_557.method_297();
                        } else {
                           if (Keyboard.getEventKey() == 1) {
                              this.method_326();
                           }

                           if (Keyboard.getEventKey() == 65) {
                              this.renderer.take_screenshot();
                           }

                           if (this.game_mode instanceof CreativeMode) {
                              if (Keyboard.getEventKey() == this.options.load_loc_key.key_code) {
                                 this.player.spawn_entity();
                              }

                              if (Keyboard.getEventKey() == this.options.save_loc_key.key_code) {
                                 this.world.maybe_set_player_spawn((int)this.player.pos_x, (int)this.player.pos_y, (int)this.player.pos_z, this.player.yaw);
                                 this.player.spawn_entity();
                              }
                           }

                           if (Keyboard.getEventKey() == 63) {
                              this.options.third_person_mode = !this.options.third_person_mode;
                           }

                           if (Keyboard.getEventKey() == this.options.inventory_key.key_code) {
                              this.method_322(new class_108(this.player.inventory));
                           }

                           if (Keyboard.getEventKey() == this.options.drop_key.key_code) {
                              this.player.drop_items(this.player.inventory.take_from(this.player.inventory.selected_slot, 1), false);
                           }
                        }

                        for(var1 = 0; var1 < 9; ++var1) {
                           if (Keyboard.getEventKey() == var1 + 2) {
                              this.player.inventory.selected_slot = var1;
                           }
                        }

                        if (Keyboard.getEventKey() == this.options.toggle_fog_key.key_code) {
                           this.options.method_277(4, !Keyboard.isKeyDown(42) && !Keyboard.isKeyDown(54) ? 1 : -1);
                        }
                     }
                  }
               }
            }
         }
      }

      if (this.field_557 != null) {
         this.field_581 = this.field_561 + 10000;
      }

      if (this.field_557 != null) {
         ClickableElement var6 = this.field_557;

         while(Mouse.next()) {
            var6.method_296();
         }

         while(Keyboard.next()) {
            var6.method_297();
         }

         if (this.field_557 != null) {
            this.field_557.method_298();
         }
      }

      if (this.world != null) {
         this.world.difficulty = this.options.difficulty;
         if (!this.is_running) {
            this.renderer.update();
         }

         if (!this.is_running) {
            this.game_renderer.method_356();
         }

         if (!this.is_running) {
            this.world.update();
         }

         if (!this.is_running) {
            this.world.method_179();
         }

         if (!this.is_running) {
            this.world.method_218((int)this.player.pos_x, (int)this.player.pos_y, (int)this.player.pos_z);
         }

         if (!this.is_running) {
            this.particle_manager.method_242();
         }
      }

   }

   // $FF: renamed from: game (int, int, int, int) void
   public final void generate_and_enter_world(int size, int shape, int type, int theme) {
      this.set_world((World)null);
      System.gc();
      String var5 = this.account != null ? this.account.username : "anonymous";
      WorldGenerator gen;
      (gen = new WorldGenerator(this.field_558)).is_island = type == 1;
      gen.is_floating = type == 2;
      gen.is_float = type == 3;
      gen.world_theme = theme;
      type = size = 128 << size;
      short var8 = 64;
      if (shape == 1) {
         size /= 2;
         type <<= 1;
      } else if (shape == 2) {
         type = size /= 2;
         var8 = 256;
      }

      World var7 = gen.generate_world(var5, size, type, var8);
      this.set_world(var7);
   }

   // $FF: renamed from: game (net.minecraft.game.game.player) void
   public final void set_world(World w) {
      if (this.world != null) {
         this.world.setup_stuff();
      }

      this.world = w;
      if (w != null) {
         w.method_157();
         this.game_mode.configure_world(w);
         this.player = (LocalPlayer)w.find_entity_of_type(LocalPlayer.class);
         w.player = this.player;
         if (this.player == null) {
            this.player = new LocalPlayer(this, w, this.account);
            this.player.spawn_entity();
            if (w != null) {
               w.add_entity(this.player);
               w.player = this.player;
            }
         }

         if (this.player != null) {
            this.player.controls = new DigitalControls(this.options);
            this.game_mode.initialize_player(this.player);
         }

         if (this.game_renderer != null) {
            this.game_renderer.set_world(w);
         }

         if (this.particle_manager != null) {
            this.particle_manager.method_245(w);
         }

         this.field_576.field_831 = 0;
         this.field_577.field_831 = 0;
         int var4 = this.resource_manager.load_texture("/water.png");
         if (w.surrounding_water_type == Block.water.block_id) {
            this.field_576.field_831 = var4;
         } else {
            this.field_577.field_831 = var4;
         }
      }

      System.gc();
   }
}
