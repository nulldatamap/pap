package net.minecraft.client;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Canvas;

public class MinecraftApplet extends Applet {
   // $FF: renamed from: game java.awt.Canvas
   private Canvas field_602;
   // $FF: renamed from: animals net.minecraft.client.projectiles
   private Game game;
   // $FF: renamed from: entities java.lang.Thread
   private Thread field_604 = null;

   public void init() {
      this.field_602 = new MinecraftCanvas(this);
      boolean var1 = false;
      if (this.getParameter("fullscreen") != null) {
         var1 = this.getParameter("fullscreen").equalsIgnoreCase("true");
      }

      this.game = new Game(this.field_602, this, this.getWidth(), this.getHeight(), var1);
      this.game.field_551 = this.getDocumentBase().getHost();
      if (this.getDocumentBase().getPort() > 0) {
         this.game.field_551 = this.game.field_551 + ":" + this.getDocumentBase().getPort();
      }
      this.game.account = new UsernameAndBlocks("nulldata", "always");

      /*
      if (this.getParameter("username") != null && this.getParameter("sessionid") != null) {
         this.game.account = new UsernameAndBlocks(this.getParameter("username"), this.getParameter("sessionid"));
         if (this.getParameter("mppass") != null) {
            this.getParameter("mppass");
         }
      }
      */

      if (this.getParameter("loadmap_user") != null && this.getParameter("loadmap_id") != null) {
         this.game.field_565 = this.getParameter("loadmap_user");
         this.game.field_566 = Integer.parseInt(this.getParameter("loadmap_id"));
      } else if (this.getParameter("server") != null && this.getParameter("port") != null) {
         this.game.method_321(this.getParameter("server"), Integer.parseInt(this.getParameter("port")));
      }

      this.game.field_553 = true;
      this.setLayout(new BorderLayout());
      this.add(this.field_602, "Center");
      this.field_602.setFocusable(true);
      this.validate();
   }

   // $FF: renamed from: game () void
   public final void method_338() {
      if (this.field_604 == null) {
         this.field_604 = new Thread(this.game, "Minecraft main thread");
         this.field_604.start();
      }
   }

   public void start() {
      this.game.is_running = false;
   }

   public void stop() {
      this.game.is_running = true;
   }

   public void destroy() {
      this.method_339();
   }

   // $FF: renamed from: animals () void
   public final void method_339() {
      if (this.field_604 != null) {
         Game var1 = this.game;
         this.game.field_578 = false;

         try {
            this.field_604.join(1000L);
         } catch (InterruptedException var3) {
            try {
               this.game.method_323();
            } catch (Exception var2) {
               var2.printStackTrace();
            }
         }

         this.field_604 = null;
      }
   }
}
