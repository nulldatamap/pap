package net.minecraft.client.ui;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.IntBuffer;
import javax.imageio.ImageIO;
import net.minecraft.client.GameOptions;
import net.minecraft.client.rendering.GeometryDrawer;
import net.minecraft.client.rendering.ResourceManager;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.entities.j
public final class TextDrawer {
   // $FF: renamed from: game int[]
   private int[] field_962 = new int[256];
   // $FF: renamed from: animals int
   private int font_texture = 0;
   // $FF: renamed from: entities int
   private int gl_list;
   // $FF: renamed from: projectiles java.nio.IntBuffer
   private IntBuffer field_965 = BufferUtils.createIntBuffer(1024);

   public TextDrawer(GameOptions options, String font, ResourceManager resource_manager) {
      BufferedImage font_image;
      try {
         font_image = ImageIO.read(ResourceManager.class.getResourceAsStream(font));
      } catch (IOException var15) {
         throw new RuntimeException(var15);
      }

      int width = font_image.getWidth();
      int height = font_image.getHeight();
      int[] buf = new int[width * height];
      font_image.getRGB(0, 0, width, height, buf, 0, width);

      int yi;
      int var9;
      int var11;
      int var13;
      int xi;
      for(int i = 0; i < 128; ++i) {
         xi = i % 16;
         yi = i / 16;
         var9 = 0;

         for(boolean var10 = false; var9 < 8 && !var10; ++var9) {
            var11 = (xi << 3) + var9;
            var10 = true;

            for(int var12 = 0; var12 < 8 && var10; ++var12) {
               var13 = ((yi << 3) + var12) * width;
               if ((buf[var11 + var13] & 255) > 128) {
                  var10 = false;
               }
            }
         }

         if (i == 32) {
            var9 = 4;
         }

         this.field_962[i] = var9;
      }

      this.font_texture = resource_manager.load_texture(font);

      this.gl_list = GL11.glGenLists(288);
      GeometryDrawer drawer = GeometryDrawer.singleton;


      for(int i = 0; i < 256; ++i) {

         GL11.glNewList(this.gl_list + i, GL11.GL_COMPILE);
         drawer.quads_mode();
         yi = i % 16 << 3;
         var9 = i / 16 << 3;
         drawer.add_vertex(0.0F, 7.99F, 0.0F, (float)yi / 128.0F, ((float)var9 + 7.99F) / 128.0F);
         drawer.add_vertex(7.99F, 7.99F, 0.0F, ((float)yi + 7.99F) / 128.0F, ((float)var9 + 7.99F) / 128.0F);
         drawer.add_vertex(7.99F, 0.0F, 0.0F, ((float)yi + 7.99F) / 128.0F, (float)var9 / 128.0F);
         drawer.add_vertex(0.0F, 0.0F, 0.0F, (float)yi / 128.0F, (float)var9 / 128.0F);
         drawer.draw();

         GL11.glTranslatef((float)this.field_962[i], 0.0F, 0.0F);

         GL11.glEndList();
      }

      for(int i = 0; i < 32; ++i) {
         yi = (i & 8) << 3;
         var9 = (i & 1) * 191 + yi;
         int var19 = ((i & 2) >> 1) * 191 + yi;
         var11 = ((i & 4) >> 2) * 191 + yi;
         boolean var20 = i >= 16;
         if (options.anaglyph_3d) {
            var13 = (var11 * 30 + var19 * 59 + var9 * 11) / 100;
            int var14 = (var11 * 30 + var19 * 70) / 100;
            int var16 = (var11 * 30 + var9 * 70) / 100;
            var11 = var13;
            var19 = var14;
            var9 = var16;
         }

         i += 2;
         if (var20) {
            var11 /= 4;
            var19 /= 4;
            var9 /= 4;
         }

         GL11.glColor4f((float)var11 / 255.0F, (float)var19 / 255.0F, (float)var9 / 255.0F, 1.0F);
      }

   }

   // $FF: renamed from: game (java.lang.String, int, int, int) void
   public final void draw_text(String text, int x, int y, int color) {
      this.draw_text(text, x + 1, y + 1, color, true);
      this.also_draw_text(text, x, y, color);
   }

   // $FF: renamed from: animals (java.lang.String, int, int, int) void
   public final void also_draw_text(String text, int x, int y, int color) {
      this.draw_text(text, x, y, color, false);
   }

   // $FF: renamed from: game (java.lang.String, int, int, int, boolean) void
   private void draw_text(String text, int x, int y, int color, boolean is_shadow) {
      if (text != null) {
         char[] var8 = text.toCharArray();
         if (is_shadow) {
            color = (color & 16579836) >> 2;
         }


         GL11.glBindTexture(3553, this.font_texture);
         float var6 = (float)(color >> 16 & 255) / 255.0F;
         float var7 = (float)(color >> 8 & 255) / 255.0F;
         float var9 = (float)(color & 255) / 255.0F;

         GL11.glColor4f(var6, var7, var9, 1.0F);
         this.field_965.clear();
         GL11.glPushMatrix();

         GL11.glTranslatef((float)x, (float)y, 0.0F);

         for(int var10 = 0; var10 < var8.length; ++var10) {
            for(; var8[var10] == '&' && var8.length > var10 + 1; var10 += 2) {
               int var11;
               if ((var11 = "0123456789abcdef".indexOf(var8[var10 + 1])) < 0 || var11 > 15) {
                  var11 = 15;
               }

               this.field_965.put(this.gl_list + 256 + var11 + (is_shadow ? 16 : 0));
               if (this.field_965.remaining() == 0) {
                  this.field_965.flip();
                  GL11.glCallLists(this.field_965);
                  this.field_965.clear();
               }
            }

            this.field_965.put(this.gl_list + var8[var10]);
            if (this.field_965.remaining() == 0) {
               this.field_965.flip();
               GL11.glCallLists(this.field_965);
               this.field_965.clear();
            }
         }

         this.field_965.flip();
         GL11.glCallLists(this.field_965);
         GL11.glPopMatrix();
      }
   }

   // $FF: renamed from: game (java.lang.String) int
   public final int text_width(String var1) {
      if (var1 == null) {
         return 0;
      } else {
         char[] var4 = var1.toCharArray();
         int var2 = 0;

         for(int var3 = 0; var3 < var4.length; ++var3) {
            if (var4[var3] == '&') {
               ++var3;
            } else {
               var2 += this.field_962[var4[var3]];
            }
         }

         return var2;
      }
   }
}
