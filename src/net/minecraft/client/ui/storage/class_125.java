package net.minecraft.client.ui.storage;

import net.minecraft.game.Storage;
import net.minecraft.game.items.ItemStack;

// $FF: renamed from: net.minecraft.client.entities.game.game
public final class class_125 implements Storage {
   // $FF: renamed from: game net.minecraft.game.animals.k[]
   private ItemStack[] field_592;
   // $FF: renamed from: animals int
   private int field_593;
   // $FF: renamed from: entities net.minecraft.client.entities.game.j
   private StorageElement field_594;

   public class_125(StorageElement var1, int var2, int var3) {
      this.field_593 = var2 * var3;
      this.field_592 = new ItemStack[this.field_593];
      this.field_594 = var1;
   }

   // $FF: renamed from: game () int
   public final int capacity() {
      return this.field_593;
   }

   // $FF: renamed from: game (int) net.minecraft.game.animals.k
   public final ItemStack get_slot(int var1) {
      return this.field_592[var1];
   }

   // $FF: renamed from: animals () java.lang.String
   public final String nbtName() {
      return "Crafting";
   }

   // $FF: renamed from: game (int, int) net.minecraft.game.animals.k
   public final ItemStack take_from(int var1, int var2) {
      if (this.field_592[var1] != null) {
         ItemStack var3;
         if (this.field_592[var1].count <= var2) {
            var3 = this.field_592[var1];
            this.field_592[var1] = null;
            this.field_594.method_311();
            return var3;
         } else {
            var3 = this.field_592[var1].split_stack(var2);
            if (this.field_592[var1].count == 0) {
               this.field_592[var1] = null;
            }

            this.field_594.method_311();
            return var3;
         }
      } else {
         return null;
      }
   }

   // $FF: renamed from: game (int, net.minecraft.game.animals.k) void
   public final void set_slot(int var1, ItemStack var2) {
      this.field_592[var1] = var2;
      this.field_594.method_311();
   }

   // $FF: renamed from: entities () int
   public final int max_stack_size() {
      return 64;
   }
}
