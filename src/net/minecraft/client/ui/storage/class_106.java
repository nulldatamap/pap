package net.minecraft.client.ui.storage;

import net.minecraft.game.world.block.a.FurnaceData;
import net.minecraft.game.entities.player.Inventory;
import net.minecraft.client.rendering.ResourceManager;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.entities.game.entities
public final class class_106 extends StorageElement {
   // $FF: renamed from: k net.minecraft.game.game.animals.game.animals
   private FurnaceData field_490;

   public class_106(Inventory var1, FurnaceData var2) {
      new class_117();
      this.field_490 = var2;
      this.field_487.add(new class_111(this, var2, 0, 56, 17));
      this.field_487.add(new class_111(this, var2, 1, 56, 53));
      this.field_487.add(new class_111(this, var2, 2, 116, 35));

      int var4;
      for(var4 = 0; var4 < 3; ++var4) {
         for(int var3 = 0; var3 < 9; ++var3) {
            this.field_487.add(new class_111(this, var1, var3 + (var4 + 1) * 9, 8 + var3 * 18, 84 + var4 * 18));
         }
      }

      for(var4 = 0; var4 < 9; ++var4) {
         this.field_487.add(new class_111(this, var1, var4, 8 + var4 * 18, 142));
      }

   }

   // $FF: renamed from: e_ () void
   protected final void method_309() {
      this.field_451.also_draw_text("Furnace", 60, 6, 4210752);
      this.field_451.also_draw_text("Inventory", 8, this.field_486 - 96 + 2, 4210752);
   }

   // $FF: renamed from: projectiles () void
   protected final void method_310() {
      int var1 = this.game.resource_manager.load_texture("/gui/furnace.png");
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      ResourceManager.bind_texture(var1);
      var1 = (this.field_447 - this.field_485) / 2;
      int var2 = (this.field_448 - this.field_486) / 2;
      this.draw_pane(var1, var2, 0, 0, this.field_485, this.field_486);
      int var3;
      if (this.field_490.method_261()) {
         var3 = this.field_490.method_260(12);
         this.draw_pane(var1 + 56, var2 + 36 + 12 - var3, 176, 12 - var3, 14, var3 + 2);
      }

      var3 = this.field_490.method_259(24);
      this.draw_pane(var1 + 79, var2 + 34, 176, 14, var3 + 1, 16);
   }
}
