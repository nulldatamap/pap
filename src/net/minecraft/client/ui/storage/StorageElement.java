package net.minecraft.client.ui.storage;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.game.Storage;
import net.minecraft.game.items.ItemStack;
import net.minecraft.client.Lighting;
import net.minecraft.client.rendering.ResourceManager;
import net.minecraft.client.rendering.models.ItemDropModelRenderer;
import net.minecraft.client.ui.ClickableElement;
import net.minecraft.client.player.LocalPlayer;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.entities.game.j
public abstract class StorageElement extends ClickableElement {
   // $FF: renamed from: k net.minecraft.client.game.game.player
   private static ItemDropModelRenderer field_483 = new ItemDropModelRenderer();
   // $FF: renamed from: l net.minecraft.game.animals.k
   private ItemStack field_484 = null;
   // $FF: renamed from: game int
   protected int field_485 = 176;
   // $FF: renamed from: i int
   protected int field_486 = 166;
   // $FF: renamed from: j java.util.List
   protected List field_487 = new ArrayList();

   // $FF: renamed from: game (int, int, float) void
   public void method_290(int var1, int var2, float var3) {
      this.method_300();
      int var13 = (this.field_447 - this.field_485) / 2;
      int var4 = (this.field_448 - this.field_486) / 2;
      this.method_310();
      GL11.glPushMatrix();
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      Lighting.enable_lighting();
      GL11.glPopMatrix();
      GL11.glPushMatrix();
      GL11.glTranslatef((float)var13, (float)var4, 0.0F);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GL11.glEnable(2977);

      for(int var5 = 0; var5 < this.field_487.size(); ++var5) {
         class_111 var6 = (class_111)this.field_487.get(var5);
         Storage var9 = var6.field_512;
         int var10 = var6.field_509;
         int var11 = var6.field_510;
         int var12 = var6.field_511;
         int var8;
         ItemStack var15;
         if ((var15 = var9.get_slot(var10)) == null && (var8 = var6.method_318()) >= 0) {
            GL11.glDisable(2896);
            ResourceManager.bind_texture(this.game.resource_manager.load_texture("/gui/items.png"));
            this.draw_pane(var11, var12, var8 % 16 << 4, var8 / 16 << 4, 16, 16);

            GL11.glEnable(2896);
         } else {
            field_483.render_in_inventory(this.game.resource_manager, var15, var11, var12);
            field_483.render_info_in_inventory(this.field_451, var15, var11, var12);
         }

         if (var6.method_314(var1, var2)) {
            GL11.glDisable(2896);
            GL11.glDisable(2929);
            int var7 = var6.field_510;
            int var14 = var6.field_511;
            method_282(var7, var14, var7 + 16, var14 + 16, -2130706433, -2130706433);
            GL11.glEnable(2896);
            GL11.glEnable(2929);
         }
      }

      if (this.field_484 != null) {
         GL11.glTranslatef(0.0F, 0.0F, 32.0F);
         field_483.render_in_inventory(this.game.resource_manager, this.field_484, var1 - var13 - 8, var2 - var4 - 8);
         field_483.render_info_in_inventory(this.field_451, this.field_484, var1 - var13 - 8, var2 - var4 - 8);
      }

      GL11.glDisable(2977);
      Lighting.disable_lighting();
      GL11.glDisable(2896);
      GL11.glDisable(2929);
      this.method_309();
      GL11.glEnable(2896);
      GL11.glEnable(2929);
      GL11.glPopMatrix();
   }

   // $FF: renamed from: e_ () void
   protected void method_309() {
   }

   // $FF: renamed from: projectiles () void
   protected abstract void method_310();

   // $FF: renamed from: game (int, int, int) void
   protected final void method_292(int var1, int var2, int var3) {
      if (var3 == 0 || var3 == 1) {
         int var6 = var2;
         int var4 = var1;
         StorageElement var5 = this;
         int var7 = 0;

         class_111 var10000;
         while(true) {
            if (var7 >= var5.field_487.size()) {
               var10000 = null;
               break;
            }

            class_111 var8;
            if ((var8 = (class_111)var5.field_487.get(var7)).method_314(var4, var6)) {
               var10000 = var8;
               break;
            }

            ++var7;
         }

         class_111 var11 = var10000;
         if (var10000 == null) {
            if (this.field_484 != null) {
               int var13 = (this.field_447 - this.field_485) / 2;
               var6 = (this.field_448 - this.field_486) / 2;
               if (var1 < var13 || var2 < var6 || var1 >= var13 + this.field_485 || var2 >= var6 + this.field_485) {
                  LocalPlayer var10 = this.game.player;
                  if (var3 == 0) {
                     var10.drop_items(this.field_484);
                     this.field_484 = null;
                  }

                  if (var3 == 1) {
                     var10.drop_items(this.field_484.split_stack(1));
                     if (this.field_484.count == 0) {
                        this.field_484 = null;
                     }
                  }
               }
            }
         } else {
            ItemStack var12;
            if ((var12 = var11.field_512.get_slot(var11.field_509)) != null || this.field_484 != null) {
               if (var12 != null && this.field_484 == null) {
                  var6 = var3 == 0 ? var12.count : (var12.count + 1) / 2;
                  this.field_484 = var11.field_512.take_from(var11.field_509, var6);
                  if (var12.count == 0) {
                     var11.method_317((ItemStack)null);
                  }

                  var11.method_315();
                  return;
               }

               if (var12 == null && this.field_484 != null && var11.method_316(this.field_484)) {
                  if ((var6 = var3 == 0 ? this.field_484.count : 1) > var11.field_512.max_stack_size()) {
                     var6 = var11.field_512.max_stack_size();
                  }

                  var11.method_317(this.field_484.split_stack(var6));
                  if (this.field_484.count == 0) {
                     this.field_484 = null;
                  }

                  return;
               }

               if (var12 != null && this.field_484 != null) {
                  ItemStack var9;
                  if (var11.method_316(this.field_484)) {
                     if (var12.id != this.field_484.id) {
                        if (this.field_484.count <= var11.field_512.max_stack_size()) {
                           var11.method_317(this.field_484);
                           this.field_484 = var12;
                           return;
                        }
                     } else if (var12.id == this.field_484.id) {
                        if (var3 == 0) {
                           if ((var6 = this.field_484.count) > var11.field_512.max_stack_size() - var12.count) {
                              var6 = var11.field_512.max_stack_size() - var12.count;
                           }

                           var9 = this.field_484;
                           if (var6 > this.field_484.item().get_stack_size() - var12.count) {
                              var9 = this.field_484;
                              var6 = this.field_484.item().get_stack_size() - var12.count;
                           }

                           this.field_484.split_stack(var6);
                           if (this.field_484.count == 0) {
                              this.field_484 = null;
                           }

                           var12.count += var6;
                           return;
                        }

                        if (var3 == 1) {
                           var6 = 1;
                           if (1 > var11.field_512.max_stack_size() - var12.count) {
                              var6 = var11.field_512.max_stack_size() - var12.count;
                           }

                           var9 = this.field_484;
                           if (var6 > this.field_484.item().get_stack_size() - var12.count) {
                              var9 = this.field_484;
                              var6 = this.field_484.item().get_stack_size() - var12.count;
                           }

                           this.field_484.split_stack(var6);
                           if (this.field_484.count == 0) {
                              this.field_484 = null;
                           }

                           var12.count += var6;
                           return;
                        }
                     }
                  } else if (var12.id == this.field_484.id) {
                     var9 = this.field_484;
                     if (this.field_484.item().get_stack_size() > 1) {
                        var6 = var12.count;
                        if (var12.count > 0) {
                           int var14 = var6 + this.field_484.count;
                           var9 = this.field_484;
                           if (var14 <= this.field_484.item().get_stack_size()) {
                              this.field_484.count += var6;
                              var12.split_stack(var6);
                              if (var12.count == 0) {
                                 var11.method_317((ItemStack)null);
                              }

                              var11.method_315();
                           }
                        }
                     }
                  }
               }
            }

            return;
         }
      }

   }

   // $FF: renamed from: game (char, int) void
   protected final void method_291(char var1, int var2) {
      if (var2 == 1 || var2 == this.game.options.inventory_key.key_code) {
         this.game.method_322((ClickableElement)null);
      }

   }

   // $FF: renamed from: game () void
   public void method_299() {
      if (this.field_484 != null) {
         this.game.player.drop_items(this.field_484);
      }

   }

   // $FF: renamed from: d_ () void
   public void method_311() {
   }

   // $FF: renamed from: entities () boolean
   public final boolean method_301() {
      return false;
   }
}
