package net.minecraft.client.ui.storage;

import net.minecraft.game.Storage;
import net.minecraft.game.items.ItemStack;

// $FF: renamed from: net.minecraft.client.entities.game.h
public final class class_117 implements Storage {
   // $FF: renamed from: game net.minecraft.game.animals.k[]
   private ItemStack[] field_520 = new ItemStack[1];

   // $FF: renamed from: game () int
   public final int capacity() {
      return 1;
   }

   // $FF: renamed from: game (int) net.minecraft.game.animals.k
   public final ItemStack get_slot(int var1) {
      return this.field_520[var1];
   }

   // $FF: renamed from: animals () java.lang.String
   public final String nbtName() {
      return "Result";
   }

   // $FF: renamed from: game (int, int) net.minecraft.game.animals.k
   public final ItemStack take_from(int var1, int var2) {
      if (this.field_520[var1] != null) {
         ItemStack var3 = this.field_520[var1];
         this.field_520[var1] = null;
         return var3;
      } else {
         return null;
      }
   }

   // $FF: renamed from: game (int, net.minecraft.game.animals.k) void
   public final void set_slot(int var1, ItemStack var2) {
      this.field_520[var1] = var2;
   }

   // $FF: renamed from: entities () int
   public final int max_stack_size() {
      return 64;
   }
}
