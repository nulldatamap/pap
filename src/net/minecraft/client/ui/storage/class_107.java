package net.minecraft.client.ui.storage;

import net.minecraft.game.Storage;
import net.minecraft.client.rendering.ResourceManager;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.entities.game.i
public final class class_107 extends StorageElement {
   // $FF: renamed from: k net.minecraft.game.animals
   private Storage field_491;
   // $FF: renamed from: l net.minecraft.game.animals
   private Storage field_492;
   // $FF: renamed from: m int
   private int field_493 = 0;

   public class_107(Storage var1, Storage var2) {
      this.field_491 = var1;
      this.field_492 = var2;
      this.field_450 = false;
      this.field_493 = var2.capacity() / 9;
      this.field_486 = 114 + this.field_493 * 18;
      int var3 = (this.field_493 - 4) * 18;

      int var4;
      int var5;
      for(var4 = 0; var4 < this.field_493; ++var4) {
         for(var5 = 0; var5 < 9; ++var5) {
            this.field_487.add(new class_111(this, var2, var5 + var4 * 9, 8 + var5 * 18, 18 + var4 * 18));
         }
      }

      for(var4 = 0; var4 < 3; ++var4) {
         for(var5 = 0; var5 < 9; ++var5) {
            this.field_487.add(new class_111(this, var1, var5 + (var4 + 1) * 9, 8 + var5 * 18, 103 + var4 * 18 + var3));
         }
      }

      for(var4 = 0; var4 < 9; ++var4) {
         this.field_487.add(new class_111(this, var1, var4, 8 + var4 * 18, var3 + 161));
      }

   }

   // $FF: renamed from: e_ () void
   protected final void method_309() {
      this.field_451.also_draw_text(this.field_492.nbtName(), 8, 6, 4210752);
      this.field_451.also_draw_text(this.field_491.nbtName(), 8, this.field_486 - 96 + 2, 4210752);
   }

   // $FF: renamed from: projectiles () void
   protected final void method_310() {
      int var1 = this.game.resource_manager.load_texture("/gui/container.png");
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      ResourceManager.bind_texture(var1);
      var1 = (this.field_447 - this.field_485) / 2;
      int var2 = (this.field_448 - this.field_486) / 2;
      this.draw_pane(var1, var2, 0, 0, this.field_485, this.field_493 * 18 + 17);
      this.draw_pane(var1, var2 + this.field_493 * 18 + 17, 0, 126, this.field_485, 96);
   }
}
