package net.minecraft.client.ui.storage;

import net.minecraft.game.Storage;
import net.minecraft.game.items.ItemStack;

// $FF: renamed from: net.minecraft.client.entities.game.projectiles
final class class_112 extends class_111 {
   // $FF: renamed from: player net.minecraft.game.animals
   private final Storage field_514;

   public class_112(StorageElement var1, Storage var2, Storage var3, int var4, int var5, int var6) {
      super(var1, var3, 0, var5, var6);
      this.field_514 = var2;
   }

   // $FF: renamed from: game (net.minecraft.game.animals.k) boolean
   public final boolean method_316(ItemStack var1) {
      return false;
   }

   // $FF: renamed from: game () void
   public final void method_315() {
      for(int var1 = 0; var1 < this.field_514.capacity(); ++var1) {
         if (this.field_514.get_slot(var1) != null) {
            this.field_514.take_from(var1, 1);
         }
      }

   }
}
