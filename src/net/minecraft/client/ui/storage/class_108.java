package net.minecraft.client.ui.storage;

import net.minecraft.game.Storage;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.items.a.Recipes;
import net.minecraft.client.Lighting;
import net.minecraft.client.rendering.ResourceManager;
import net.minecraft.client.rendering.models.ModelManager;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.entities.game.particles
public final class class_108 extends StorageElement {
   // $FF: renamed from: k net.minecraft.client.entities.game.game
   private class_125 field_494 = new class_125(this, 2, 2);
   // $FF: renamed from: l net.minecraft.game.animals
   private Storage field_495 = new class_117();
   // $FF: renamed from: m float
   private float field_496;
   // $FF: renamed from: n float
   private float field_497;

   public class_108(Storage var1) {
      this.field_450 = true;
      this.field_487.add(new class_112(this, this.field_494, this.field_495, 0, 144, 36));

      int var2;
      int var3;
      for(var2 = 0; var2 < 2; ++var2) {
         for(var3 = 0; var3 < 2; ++var3) {
            this.field_487.add(new class_111(this, this.field_494, var3 + (var2 << 1), 88 + var3 * 18, 26 + var2 * 18));
         }
      }

      for(var2 = 0; var2 < 4; ++var2) {
         this.field_487.add(new class_113(this, this, var1, var1.capacity() - 1 - var2, 8, 8 + var2 * 18, var2));
      }

      for(var2 = 0; var2 < 3; ++var2) {
         for(var3 = 0; var3 < 9; ++var3) {
            this.field_487.add(new class_111(this, var1, var3 + (var2 + 1) * 9, 8 + var3 * 18, 84 + var2 * 18));
         }
      }

      for(var2 = 0; var2 < 9; ++var2) {
         this.field_487.add(new class_111(this, var1, var2, 8 + var2 * 18, 142));
      }

   }

   // $FF: renamed from: game () void
   public final void method_299() {
      super.method_299();

      for(int var1 = 0; var1 < this.field_494.capacity(); ++var1) {
         ItemStack var2;
         if ((var2 = this.field_494.get_slot(var1)) != null) {
            this.game.player.drop_items(var2);
         }
      }

   }

   // $FF: renamed from: d_ () void
   public final void method_311() {
      int[] var1 = new int[9];

      for(int var2 = 0; var2 < 3; ++var2) {
         for(int var3 = 0; var3 < 3; ++var3) {
            int var4 = -1;
            ItemStack var5;
            if (var2 < 2 && var3 < 2 && (var5 = this.field_494.get_slot(var2 + (var3 << 1))) != null) {
               var4 = var5.id;
            }

            var1[var2 + var3 * 3] = var4;
         }
      }

      this.field_495.set_slot(0, Recipes.method_596().method_598(var1));
   }

   // $FF: renamed from: e_ () void
   protected final void method_309() {
      this.field_451.also_draw_text("Crafting", 86, 16, 4210752);
   }

   // $FF: renamed from: game (int, int, float) void
   public final void method_290(int var1, int var2, float var3) {
      super.method_290(var1, var2, var3);
      this.field_496 = (float)var1;
      this.field_497 = (float)var2;
   }

   // $FF: renamed from: projectiles () void
   protected final void method_310() {
      int var1 = this.game.resource_manager.load_texture("/gui/inventory.png");
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      ResourceManager.bind_texture(var1);
      var1 = (this.field_447 - this.field_485) / 2;
      int var2 = (this.field_448- this.field_486) / 2;
      this.draw_pane(var1, var2, 0, 0, this.field_485, this.field_486);
      GL11.glEnable(2977);
      GL11.glEnable(2903);
      GL11.glPushMatrix();
      GL11.glTranslatef((float)(var1 + 51), (float)(var2 + 75), 50.0F);
      GL11.glScalef(-30.0F, 30.0F, 30.0F);
      GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
      float var3 = this.game.player.field_82;
      float var4 = this.game.player.yaw;
      float var5 = this.game.player.pitch;
      float var6 = (float)(var1 + 51) - this.field_496;
      float var7 = (float)(var2 + 75 - 50) - this.field_497;
      GL11.glRotatef(135.0F, 0.0F, 1.0F, 0.0F);
      Lighting.enable_lighting();
      GL11.glRotatef(-135.0F, 0.0F, 1.0F, 0.0F);
      GL11.glRotatef(-((float)Math.atan((double)(var7 / 40.0F))) * 20.0F, 1.0F, 0.0F, 0.0F);
      this.game.player.field_82 = (float)Math.atan((double)(var6 / 40.0F)) * 20.0F;
      this.game.player.yaw = (float)Math.atan((double)(var6 / 40.0F)) * 40.0F;
      this.game.player.pitch = -((float)Math.atan((double)(var7 / 40.0F))) * 20.0F;
      GL11.glTranslatef(0.0F, this.game.player.camera_height, 0.0F);
      ModelManager.singleton.fully_render_entity(this.game.player, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F);
      this.game.player.field_82 = var3;
      this.game.player.yaw = var4;
      this.game.player.pitch = var5;
      GL11.glPopMatrix();
      Lighting.disable_lighting();
      GL11.glDisable(2977);
   }
}
