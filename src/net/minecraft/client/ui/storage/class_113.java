package net.minecraft.client.ui.storage;

import net.minecraft.game.Storage;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.items.Armor;

// $FF: renamed from: net.minecraft.client.entities.game.player
final class class_113 extends class_111 {
   // $FF: renamed from: player int
   // $FF: synthetic field
   private int field_515;

   class_113(class_108 var1, StorageElement var2, Storage var3, int var4, int var5, int var6, int var7) {
      super(var2, var3, var4, 8, var6);
      this.field_515 = var7;
   }

   // $FF: renamed from: game (net.minecraft.game.animals.k) boolean
   public final boolean method_316(ItemStack var1) {
      if (var1.item() instanceof Armor) {
         return ((Armor)var1.item()).field_215 == this.field_515;
      } else {
         return false;
      }
   }

   // $FF: renamed from: animals () int
   public final int method_318() {
      return 15 + (this.field_515 << 4);
   }
}
