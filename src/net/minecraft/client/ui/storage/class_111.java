package net.minecraft.client.ui.storage;

import net.minecraft.game.Storage;
import net.minecraft.game.items.ItemStack;

// $FF: renamed from: net.minecraft.client.entities.game.player
public class class_111 {
   // $FF: renamed from: game int
   public final int field_509;
   // $FF: renamed from: animals int
   public final int field_510;
   // $FF: renamed from: entities int
   public final int field_511;
   // $FF: renamed from: projectiles net.minecraft.game.animals
   public final Storage field_512;
   // $FF: renamed from: player net.minecraft.client.entities.game.j
   private final StorageElement field_513;

   public class_111(StorageElement var1, Storage var2, int var3, int var4, int var5) {
      this.field_513 = var1;
      this.field_512 = var2;
      this.field_509 = var3;
      this.field_510 = var4;
      this.field_511 = var5;
   }

   // $FF: renamed from: game (int, int) boolean
   public final boolean method_314(int var1, int var2) {
      int var3 = (this.field_513.field_447 - this.field_513.field_485) / 2;
      int var4 = (this.field_513.field_448 - this.field_513.field_486) / 2;
      var1 -= var3;
      var2 -= var4;
      return var1 >= this.field_510 - 1 && var1 < this.field_510 + 16 + 1 && var2 >= this.field_511 - 1 && var2 < this.field_511 + 16 + 1;
   }

   // $FF: renamed from: game () void
   public void method_315() {
   }

   // $FF: renamed from: game (net.minecraft.game.animals.k) boolean
   public boolean method_316(ItemStack var1) {
      return true;
   }

   // $FF: renamed from: animals (net.minecraft.game.animals.k) void
   public final void method_317(ItemStack var1) {
      this.field_512.set_slot(this.field_509, var1);
   }

   // $FF: renamed from: animals () int
   public int method_318() {
      return -1;
   }
}
