package net.minecraft.client.ui.storage;

import net.minecraft.game.Storage;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.items.a.Recipes;
import net.minecraft.game.entities.player.Inventory;
import net.minecraft.client.rendering.ResourceManager;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.entities.game.animals
public final class class_105 extends StorageElement {
   // $FF: renamed from: k net.minecraft.client.entities.game.game
   private class_125 field_488 = new class_125(this, 3, 3);
   // $FF: renamed from: l net.minecraft.game.animals
   private Storage field_489 = new class_117();

   public class_105(Inventory var1) {
      this.field_487.add(new class_112(this, this.field_488, this.field_489, 0, 124, 35));

      int var2;
      int var3;
      for(var2 = 0; var2 < 3; ++var2) {
         for(var3 = 0; var3 < 3; ++var3) {
            this.field_487.add(new class_111(this, this.field_488, var3 + var2 * 3, 30 + var3 * 18, 17 + var2 * 18));
         }
      }

      for(var2 = 0; var2 < 3; ++var2) {
         for(var3 = 0; var3 < 9; ++var3) {
            this.field_487.add(new class_111(this, var1, var3 + (var2 + 1) * 9, 8 + var3 * 18, 84 + var2 * 18));
         }
      }

      for(var2 = 0; var2 < 9; ++var2) {
         this.field_487.add(new class_111(this, var1, var2, 8 + var2 * 18, 142));
      }

   }

   // $FF: renamed from: game () void
   public final void method_299() {
      super.method_299();

      for(int var1 = 0; var1 < 9; ++var1) {
         ItemStack var2;
         if ((var2 = this.field_488.get_slot(var1)) != null) {
            this.game.player.drop_items(var2);
         }
      }

   }

   // $FF: renamed from: d_ () void
   public final void method_311() {
      int[] var1 = new int[9];

      for(int var2 = 0; var2 < 3; ++var2) {
         for(int var3 = 0; var3 < 3; ++var3) {
            int var4 = var2 + var3 * 3;
            ItemStack var5;
            if ((var5 = this.field_488.get_slot(var4)) == null) {
               var1[var4] = -1;
            } else {
               var1[var4] = var5.id;
            }
         }
      }

      this.field_489.set_slot(0, Recipes.method_596().method_598(var1));
   }

   // $FF: renamed from: e_ () void
   protected final void method_309() {
      this.field_451.also_draw_text("Crafting", 28, 6, 4210752);
      this.field_451.also_draw_text("Inventory", 8, this.field_486 - 96 + 2, 4210752);
   }

   // $FF: renamed from: projectiles () void
   protected final void method_310() {
      int var1 = this.game.resource_manager.load_texture("/gui/crafting.png");
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      ResourceManager.bind_texture(var1);
      var1 = (this.field_447 - this.field_485) / 2;
      int var2 = (this.field_448 - this.field_486) / 2;
      this.draw_pane(var1, var2, 0, 0, this.field_485, this.field_486);
   }
}
