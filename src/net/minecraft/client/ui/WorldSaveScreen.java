package net.minecraft.client.ui;

import java.awt.Dialog;
import java.awt.FileDialog;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import net.minecraft.client.SinglePlayerLevelLoader;

// $FF: renamed from: net.minecraft.client.entities.o
public final class WorldSaveScreen extends WorldLoader {
   public WorldSaveScreen(ClickableElement var1) {
      super(var1);
      this.field_478 = "Save world";
   }

   // $FF: renamed from: player () java.awt.FileDialog
   protected final FileDialog method_304() {
      return new FileDialog((Dialog)null, "Save world", 1);
   }

   // $FF: renamed from: animals () void
   public final void method_295() {
      super.method_295();
      ((class_92)this.field_449.get(5)).field_442 = "Save file...";
   }

   // $FF: renamed from: game (java.lang.String[]) void
   protected final void method_303(String[] var1) {
      for(int var2 = 0; var2 < 5; ++var2) {
         ((class_92)this.field_449.get(var2)).field_442 = var1[var2];
         ((class_92)this.field_449.get(var2)).field_445 = true;
      }

      ((class_92)this.field_449.get(5)).field_445 = true;
   }

   // $FF: renamed from: game (java.io.File) void
   protected final void load(File var1) {
      try {
         FileOutputStream var3 = new FileOutputStream(var1);
         (new SinglePlayerLevelLoader(this.game, this.game.field_558)).save(this.game.world, var3);
         var3.close();
      } catch (IOException var3) {
         var3.printStackTrace();
      }

   }

   // $FF: renamed from: game (int) void
   protected final void method_305(int var1) {
      this.game.method_322(new class_97(this, ((class_92)this.field_449.get(var1)).field_442, var1));
   }
}
