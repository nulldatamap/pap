package net.minecraft.client.ui;

import net.minecraft.client.rendering.GeometryDrawer;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.entities.k
public class UIElement {
   // $FF: renamed from: h float
   protected float z = 0.0F;

   // $FF: renamed from: game (int, int, int, int, int) void
   protected static void method_281(int var0, int var1, int var2, int var3, int var4) {
      float var5 = (float)(var4 >>> 24) / 255.0F;
      float var6 = (float)(var4 >> 16 & 255) / 255.0F;
      float var7 = (float)(var4 >> 8 & 255) / 255.0F;
      float var9 = (float)(var4 & 255) / 255.0F;
      GeometryDrawer var8 = GeometryDrawer.singleton;
      GL11.glEnable(3042);
      GL11.glDisable(3553);
      GL11.glBlendFunc(770, 771);
      GL11.glColor4f(var6, var7, var9, var5);
      var8.quads_mode();
      var8.add_vertex((float)var0, (float)var3, 0.0F);
      var8.add_vertex((float)var2, (float)var3, 0.0F);
      var8.add_vertex((float)var2, (float)var1, 0.0F);
      var8.add_vertex((float)var0, (float)var1, 0.0F);
      var8.draw();
      GL11.glEnable(3553);
      GL11.glDisable(3042);
   }

   // $FF: renamed from: game (int, int, int, int, int, int) void
   protected static void method_282(int var0, int var1, int var2, int var3, int var4, int var5) {
      float var6 = (float)(var4 >>> 24) / 255.0F;
      float var7 = (float)(var4 >> 16 & 255) / 255.0F;
      float var8 = (float)(var4 >> 8 & 255) / 255.0F;
      float var13 = (float)(var4 & 255) / 255.0F;
      float var9 = (float)(var5 >>> 24) / 255.0F;
      float var10 = (float)(var5 >> 16 & 255) / 255.0F;
      float var11 = (float)(var5 >> 8 & 255) / 255.0F;
      float var14 = (float)(var5 & 255) / 255.0F;
      GL11.glDisable(3553);
      GL11.glEnable(3042);
      GL11.glDisable(3008);
      GL11.glBlendFunc(770, 771);
      GeometryDrawer var12 = GeometryDrawer.singleton;
      GeometryDrawer.singleton.quads_mode();
      var12.set_color(var7, var8, var13, var6);
      var12.add_vertex((float)var2, (float)var1, 0.0F);
      var12.add_vertex((float)var0, (float)var1, 0.0F);
      var12.set_color(var10, var11, var14, var9);
      var12.add_vertex((float)var0, (float)var3, 0.0F);
      var12.add_vertex((float)var2, (float)var3, 0.0F);
      var12.draw();
      GL11.glDisable(3042);
      GL11.glEnable(3008);
      GL11.glEnable(3553);
   }

   // $FF: renamed from: game (net.minecraft.client.entities.j, java.lang.String, int, int, int) void
   public static void method_283(TextDrawer var0, String var1, int var2, int var3, int var4) {
      var0.draw_text(var1, var2 - var0.text_width(var1) / 2, var3, var4);
   }

   // $FF: renamed from: animals (net.minecraft.client.entities.j, java.lang.String, int, int, int) void
   public static void method_284(TextDrawer var0, String var1, int var2, int var3, int var4) {
      var0.draw_text(var1, var2, var3, var4);
   }

   // $FF: renamed from: animals (int, int, int, int, int, int) void
   public final void draw_pane(int x, int y, int z, int idx, int w, int h) {
      GeometryDrawer drawer = GeometryDrawer.singleton;
      GeometryDrawer.singleton.quads_mode();
      drawer.add_vertex((float)x, (float)(y + h), this.z, (float)z * 0.00390625F, (float)(idx + h) * 0.00390625F);
      drawer.add_vertex((float)(x + w), (float)(y + h), this.z, (float)(z + w) * 0.00390625F, (float)(idx + h) * 0.00390625F);
      drawer.add_vertex((float)(x + w), (float)y, this.z, (float)(z + w) * 0.00390625F, (float)idx * 0.00390625F);
      drawer.add_vertex((float)x, (float)y, this.z, (float)z * 0.00390625F, (float)idx * 0.00390625F);
      drawer.draw();
   }
}
