package net.minecraft.client.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.entities.player.Inventory;
import net.minecraft.client.class_120;
import net.minecraft.client.Game;
import net.minecraft.client.Lighting;
import net.minecraft.client.rendering.models.ItemDropModelRenderer;
import net.minecraft.client.player.LocalPlayer;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.entities.l
public final class HudUI extends UIElement {
   // $FF: renamed from: game net.minecraft.client.game.game.player
   private static ItemDropModelRenderer item = new ItemDropModelRenderer();
   // $FF: renamed from: animals java.util.List
   private List field_434 = new ArrayList();
   // $FF: renamed from: entities java.util.Random
   private Random random = new Random();
   // $FF: renamed from: projectiles net.minecraft.client.projectiles
   private Game game;
   // $FF: renamed from: player int
   private int tick = 0;

   public HudUI(Game game) {
      this.game = game;
   }

   // $FF: renamed from: game (float) void
   public final void render_gui(float dt) {
      WidthHeight wh;
      int width = (wh = new WidthHeight(this.game.width, this.game.height)).get_width();
      int height = wh.get_height();
      TextDrawer text_drawer = this.game.text_drawer;
      this.game.renderer.setup_ui_view();

      GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.game.resource_manager.load_texture("/gui/gui.png"));

      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

      GL11.glEnable(GL11.GL_BLEND);
      Inventory inventory = this.game.player.inventory;
      this.z = -90.0F;
      this.draw_pane(width / 2 - 91, height - 22, 0, 0, 182, 22);
      this.draw_pane(width / 2 - 91 - 1 + inventory.selected_slot * 20, height - 22 - 1, 0, 22, 24, 22);

      GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.game.resource_manager.load_texture("/gui/icons.png"));

      GL11.glEnable(GL11.GL_BLEND);

      GL11.glBlendFunc(GL11.GL_ONE_MINUS_DST_COLOR, GL11.GL_ONE_MINUS_SRC_COLOR);
      this.draw_pane(width / 2 - 7, height / 2 - 7, 0, 0, 16, 16);

      GL11.glDisable(GL11.GL_BLEND);
      boolean not_really_invincible = this.game.player.invincible_frames / 3 % 2 == 1;
      if (this.game.player.invincible_frames < 10) {
         not_really_invincible = false;
      }

      int playerHealth = this.game.player.health;
      int prev_health = this.game.player.prev_health;
      this.random.setSeed((long)(this.tick * 312871));
      int armor;
      int yi;
      if (this.game.game_mode.ui_enabled()) {
         LocalPlayer player = this.game.player;
         armor = this.game.player.inventory.calculate_armor();

         int i;
         int xi;
         for(i = 0; i < 10; ++i) {
            yi = height - 32;
            if (armor > 0) {
               xi = width / 2 + 91 - (i << 3) - 9;
               if ((i << 1) + 1 < armor) {
                  this.draw_pane(xi, yi, 34, 9, 9, 9);
               }

               if ((i << 1) + 1 == armor) {
                  this.draw_pane(xi, yi, 25, 9, 9, 9);
               }

               if ((i << 1) + 1 > armor) {
                  this.draw_pane(xi, yi, 16, 9, 9, 9);
               }
            }

            byte var26 = 0;
            if (not_really_invincible) {
               var26 = 1;
            }

            int var14 = width / 2 - 91 + (i << 3);
            if (playerHealth <= 4) {
               yi += this.random.nextInt(2);
            }

            this.draw_pane(var14, yi, 16 + var26 * 9, 0, 9, 9);
            if (not_really_invincible) {
               if ((i << 1) + 1 < prev_health) {
                  this.draw_pane(var14, yi, 70, 0, 9, 9);
               }

               if ((i << 1) + 1 == prev_health) {
                  this.draw_pane(var14, yi, 79, 0, 9, 9);
               }
            }

            if ((i << 1) + 1 < playerHealth) {
               this.draw_pane(var14, yi, 52, 0, 9, 9);
            }

            if ((i << 1) + 1 == playerHealth) {
               this.draw_pane(var14, yi, 61, 0, 9, 9);
            }
         }

         if (this.game.player.on_water_block()) {
            i = (int)Math.ceil((double)(this.game.player.air - 2) * 10.0D / 300.0D);
            yi = (int)Math.ceil((double)this.game.player.air * 10.0D / 300.0D) - i;

            for(xi = 0; xi < i + yi; ++xi) {
               if (xi < i) {
                  this.draw_pane(width / 2 - 91 + (xi << 3), height - 32 - 9, 16, 18, 9, 9);
               } else {
                  this.draw_pane(width / 2 - 91 + (xi << 3), height - 32 - 9, 25, 18, 9, 9);
               }
            }
         }
      }


      GL11.glDisable(GL11.GL_BLEND);

      GL11.glEnable(GL11.GL_NORMALIZE);

      GL11.glPushMatrix();

      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      Lighting.enable_lighting();

      GL11.glPopMatrix();

      for(armor = 0; armor < 9; ++armor) {
         float var25 = (float)(width / 2 - 90 + armor * 20 + 2);
         yi = height - 16 - 3;
         float var21 = var25;
         ItemStack var22;
         if ((var22 = this.game.player.inventory.main_area[armor]) != null) {
            float var9;
            if ((var9 = (float)var22.pickup_cooldown - dt) > 0.0F) {

               GL11.glPushMatrix();
               var25 = 1.0F + var9 / 5.0F;

               GL11.glTranslatef((float)(var21 + 8), (float)(yi + 12), 0.0F);

               GL11.glScalef(1.0F / var25, (var25 + 1.0F) / 2.0F, 1.0F);

               GL11.glTranslatef((float)(-(var21 + 8)), (float)(-(yi + 12)), 0.0F);
            }

            item.render_in_inventory(this.game.resource_manager, var22, (int)var25, yi);
            if (var9 > 0.0F) {

               GL11.glPopMatrix();
            }

            item.render_info_in_inventory(this.game.text_drawer, var22, (int)var25, yi);
         }
      }

      Lighting.disable_lighting();

      GL11.glDisable(GL11.GL_NORMALIZE);
      if (this.game.options.show_fps) {
         text_drawer.draw_text("Minecraft Indev (" + this.game.field_579 + ")", 2, 2, 16777215);
         Game var23 = this.game;
         text_drawer.draw_text(this.game.game_renderer.chunk_rendering_debug_info(), 2, 12, 16777215);
         var23 = this.game;
         text_drawer.draw_text(this.game.game_renderer.entity_rendering_debug_info(), 2, 22, 16777215);
         var23 = this.game;
         text_drawer.draw_text("P: " + var23.particle_manager.method_248() + ". T: " + var23.world.method_219(), 2, 32, 16777215);
         long var24 = Runtime.getRuntime().maxMemory();
         long var27 = Runtime.getRuntime().totalMemory();
         long var28 = Runtime.getRuntime().freeMemory();
         long var16 = var24 - var28;
         String var18 = "Free memory: " + var16 * 100L / var24 + "% of " + var24 / 1024L / 1024L + "MB";
         method_284(text_drawer, var18, width - text_drawer.text_width(var18) - 2, 2, 14737632);
         var18 = "Allocated memory: " + var27 * 100L / var24 + "% (" + var27 / 1024L / 1024L + "MB)";
         method_284(text_drawer, var18, width - text_drawer.text_width(var18) - 2, 12, 14737632);
      } else {
         text_drawer.draw_text("Minecraft Indev", 2, 2, 16777215);
      }

      for(yi = 0; yi < this.field_434.size() && yi < 10; ++yi) {
         if (((class_120)this.field_434.get(yi)).field_523 < 200) {
            this.field_434.get(yi);
            text_drawer.draw_text((String)null, 2, height - 8 - yi * 9 - 20, 16777215);
         }
      }

   }

   // $FF: renamed from: game () void
   public final void method_287() {
      ++this.tick;

      for(int i = 0; i < this.field_434.size(); ++i) {
         ++((class_120)this.field_434.get(i)).field_523;
      }

   }
}
