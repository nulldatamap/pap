package net.minecraft.client.ui;

import org.lwjgl.input.Keyboard;

// $FF: renamed from: net.minecraft.client.entities.p
public final class class_97 extends ClickableElement {
   // $FF: renamed from: game net.minecraft.client.entities.i
   private ClickableElement field_465;
   // $FF: renamed from: i java.lang.String
   private String field_466 = "Enter world name:";
   // $FF: renamed from: j int
   private int field_467;
   // $FF: renamed from: k java.lang.String
   private String field_468;
   // $FF: renamed from: l int
   private int field_469 = 0;

   public class_97(ClickableElement var1, String var2, int var3) {
      this.field_465 = var1;
      this.field_467 = var3;
      this.field_468 = var2;
      if (this.field_468.equals("-")) {
         this.field_468 = "";
      }

   }

   // $FF: renamed from: animals () void
   public final void method_295() {
      this.field_449.clear();
      Keyboard.enableRepeatEvents(true);
      this.field_449.add(new class_92(0, this.field_447 / 2 - 100, this.field_448 / 4 + 120, "Save"));
      this.field_449.add(new class_92(1, this.field_447 / 2 - 100, this.field_448 / 4 + 144, "Cancel"));
      ((class_92)this.field_449.get(0)).field_444 = this.field_468.trim().length() > 1;
   }

   // $FF: renamed from: game () void
   public final void method_299() {
      Keyboard.enableRepeatEvents(false);
   }

   // $FF: renamed from: f_ () void
   public final void method_298() {
      ++this.field_469;
   }

   // $FF: renamed from: game (net.minecraft.client.entities.r) void
   protected final void method_293(class_92 var1) {
      if (var1.field_444) {
         if (var1.field_443 == 0 && this.field_468.trim().length() > 1) {
            this.field_468.trim();
            this.game.method_322((ClickableElement)null);
            this.game.method_324();
         }

         if (var1.field_443 == 1) {
            this.game.method_322(this.field_465);
         }

      }
   }

   // $FF: renamed from: game (char, int) void
   protected final void method_291(char var1, int var2) {
      if (var2 == 14 && this.field_468.length() > 0) {
         this.field_468 = this.field_468.substring(0, this.field_468.length() - 1);
      }

      if ("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ,.:-_'*!\"#%/()=+?[]{}<>".indexOf(var1) >= 0 && this.field_468.length() < 64) {
         this.field_468 = this.field_468 + var1;
      }

      ((class_92)this.field_449.get(0)).field_444 = this.field_468.trim().length() > 1;
   }

   // $FF: renamed from: game (int, int, float) void
   public final void method_290(int var1, int var2, float var3) {
      this.method_300();
      method_283(this.field_451, this.field_466, this.field_447 / 2, 40, 16777215);
      int var4 = this.field_447 / 2 - 100;
      int var5 = this.field_448 / 2 - 10;
      method_281(var4 - 1, var5 - 1, var4 + 200 + 1, var5 + 20 + 1, -6250336);
      method_281(var4, var5, var4 + 200, var5 + 20, -16777216);
      method_284(this.field_451, this.field_468 + (this.field_469 / 6 % 2 == 0 ? "_" : ""), var4 + 4, var5 + 6, 14737632);
      super.method_290(var1, var2, var3);
   }
}
