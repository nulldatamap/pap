package net.minecraft.client.ui;

// $FF: renamed from: net.minecraft.client.entities.h
public final class WidthHeight {
   // $FF: renamed from: game int
   private int width;
   // $FF: renamed from: animals int
   private int height;

   public WidthHeight(int width, int height) {
      this.width = width;
      this.height = height;

      int i;
      for(i = 1; this.width / (i + 1) >= 320 && this.height / (i + 1) >= 240; ++i) {
         ;
      }

      this.width /= i;
      this.height /= i;
   }

   // $FF: renamed from: game () int
   public final int get_width() {
      return this.width;
   }

   // $FF: renamed from: animals () int
   public final int get_height() {
      return this.height;
   }
}
