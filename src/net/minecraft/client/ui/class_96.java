package net.minecraft.client.ui;

import net.minecraft.client.GameOptions;

// $FF: renamed from: net.minecraft.client.entities.m
public final class class_96 extends ClickableElement {
   // $FF: renamed from: game net.minecraft.client.entities.i
   private ClickableElement field_461;
   // $FF: renamed from: i java.lang.String
   private String field_462 = "Controls";
   // $FF: renamed from: j net.minecraft.client.q
   private GameOptions options;
   // $FF: renamed from: k int
   private int field_464 = -1;

   public class_96(ClickableElement var1, GameOptions var2) {
      this.field_461 = var1;
      this.options = var2;
   }

   // $FF: renamed from: animals () void
   public final void method_295() {
      for(int var1 = 0; var1 < this.options.keys.length; ++var1) {
         this.field_449.add(new class_93(var1, this.field_447 / 2 - 155 + var1 % 2 * 160, this.field_448 / 6 + 24 * (var1 >> 1), this.options.GetKeyString(var1)));
      }

      this.field_449.add(new class_92(200, this.field_447 / 2 - 100, this.field_448 / 6 + 168, "Done"));
   }

   // $FF: renamed from: game (net.minecraft.client.entities.r) void
   protected final void method_293(class_92 var1) {
      for(int var2 = 0; var2 < this.options.keys.length; ++var2) {
         ((class_92)this.field_449.get(var2)).field_442 = this.options.GetKeyString(var2);
      }

      if (var1.field_443 == 200) {
         this.game.method_322(this.field_461);
      } else {
         this.field_464 = var1.field_443;
         var1.field_442 = "> " + this.options.GetKeyString(var1.field_443) + " <";
      }
   }

   // $FF: renamed from: game (char, int) void
   protected final void method_291(char var1, int var2) {
      if (this.field_464 >= 0) {
         this.options.method_276(this.field_464, var2);
         ((class_92)this.field_449.get(this.field_464)).field_442 = this.options.GetKeyString(this.field_464);
         this.field_464 = -1;
      } else {
         super.method_291(var1, var2);
      }
   }

   // $FF: renamed from: game (int, int, float) void
   public final void method_290(int var1, int var2, float var3) {
      this.method_300();
      method_283(this.field_451, this.field_462, this.field_447 / 2, 20, 16777215);
      super.method_290(var1, var2, var3);
   }
}
