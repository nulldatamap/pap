package net.minecraft.client.ui;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.client.Game;
import net.minecraft.client.rendering.GeometryDrawer;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.entities.i
public class ClickableElement extends UIElement {
   // $FF: renamed from: animals net.minecraft.client.projectiles
   protected Game game;
   // $FF: renamed from: entities int
   public int field_447;
   // $FF: renamed from: projectiles int
   public int field_448;
   // $FF: renamed from: player java.util.List
   protected List field_449 = new ArrayList();
   // $FF: renamed from: particles boolean
   public boolean field_450 = false;
   // $FF: renamed from: player net.minecraft.client.entities.j
   protected TextDrawer field_451;

   // $FF: renamed from: game (int, int, float) void
   public void method_290(int var1, int var2, float var3) {
      for(int var5 = 0; var5 < this.field_449.size(); ++var5) {
         ((class_92)this.field_449.get(var5)).method_288(this.game, var1, var2);
      }

   }

   // $FF: renamed from: game (char, int) void
   protected void method_291(char var1, int var2) {
      if (var2 == 1) {
         this.game.method_322((ClickableElement)null);
         this.game.method_324();
      }

   }

   // $FF: renamed from: game (int, int, int) void
   protected void method_292(int var1, int var2, int var3) {
      if (var3 == 0) {
         for(var3 = 0; var3 < this.field_449.size(); ++var3) {
            class_92 var4;
            if ((var4 = (class_92)this.field_449.get(var3)).method_289(var1, var2)) {
               // TODO: add this back
               // this.game.sound_manager.play_sound("random.click", 1.0F, 1.0F);
               this.method_293(var4);
            }
         }
      }

   }

   // $FF: renamed from: game (net.minecraft.client.entities.r) void
   protected void method_293(class_92 var1) {
   }

   // $FF: renamed from: game (net.minecraft.client.projectiles, int, int) void
   public final void method_294(Game var1, int var2, int var3) {
      this.game = var1;
      this.field_451 = var1.text_drawer;
      this.field_447 = var2;
      this.field_448 = var3;
      this.method_295();
   }

   // $FF: renamed from: animals () void
   public void method_295() {
   }

   // $FF: renamed from: particles () void
   public final void method_296() {
      if (Mouse.getEventButtonState()) {
         int var1 = Mouse.getEventX() * this.field_447 / this.game.width;
         int var2 = this.field_448 - Mouse.getEventY() * this.field_448 / this.game.height - 1;
         this.method_292(var1, var2, Mouse.getEventButton());
      } else {
         Mouse.getEventX();
         Mouse.getEventY();
         Mouse.getEventButton();
      }
   }

   // $FF: renamed from: player () void
   public final void method_297() {
      if (Keyboard.getEventKeyState()) {
         if (Keyboard.getEventKey() == 87) {
            this.game.method_328();
            return;
         }

         this.method_291(Keyboard.getEventCharacter(), Keyboard.getEventKey());
      }

   }

   // $FF: renamed from: f_ () void
   public void method_298() {
   }

   // $FF: renamed from: game () void
   public void method_299() {
   }

   // $FF: renamed from: h () void
   public final void method_300() {
      boolean var1 = false;
      if (this.game.world != null) {
         draw_pane(0, 0, this.field_447, this.field_448, 1610941696, -1607454624);
      } else {
         GL11.glDisable(2896);
         GL11.glDisable(2912);
         GeometryDrawer var2 = GeometryDrawer.singleton;
         GL11.glBindTexture(3553, this.game.resource_manager.load_texture("/dirt.png"));
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         var2.quads_mode();
         var2.set_color(4210752);
         var2.add_vertex(0.0F, (float)this.field_448, 0.0F, 0.0F, (float)this.field_448 / 32.0F);
         var2.add_vertex((float)this.field_447, (float)this.field_448, 0.0F, (float)this.field_447 / 32.0F, (float)this.field_448 / 32.0F);
         var2.add_vertex((float)this.field_447, 0.0F, 0.0F, (float)this.field_447 / 32.0F, 0.0F);
         var2.add_vertex(0.0F, 0.0F, 0.0F, 0.0F, 0.0F);
         var2.draw();
      }
   }

   // $FF: renamed from: entities () boolean
   public boolean method_301() {
      return true;
   }
}
