package net.minecraft.client.ui;

// $FF: renamed from: net.minecraft.client.entities.entities
public final class class_103 extends ClickableElement {
   // $FF: renamed from: game java.lang.String
   private String field_481;
   // $FF: renamed from: i java.lang.String
   private String field_482;

   public class_103(String var1, String var2) {
      this.field_481 = var1;
      this.field_482 = var2;
   }

   // $FF: renamed from: animals () void
   public final void method_295() {
   }

   // $FF: renamed from: game (int, int, float) void
   public final void method_290(int var1, int var2, float var3) {
      method_282(0, 0, this.field_447, this.field_448, -12574688, -11530224);
      method_283(this.field_451, this.field_481, this.field_447 / 2, 90, 16777215);
      method_283(this.field_451, this.field_482, this.field_447 / 2, 110, 16777215);
      super.method_290(var1, var2, var3);
   }

   // $FF: renamed from: game (char, int) void
   protected final void method_291(char var1, int var2) {
   }
}
