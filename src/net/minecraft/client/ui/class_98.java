package net.minecraft.client.ui;

import net.minecraft.client.GameOptions;

// $FF: renamed from: net.minecraft.client.entities.player
public final class class_98 extends ClickableElement {
   // $FF: renamed from: game net.minecraft.client.entities.i
   private ClickableElement field_470;
   // $FF: renamed from: i java.lang.String
   private String field_471 = "Options";
   // $FF: renamed from: j net.minecraft.client.q
   private GameOptions options;

   public class_98(ClickableElement var1, GameOptions options) {
      this.field_470 = var1;
      this.options = options;
   }

   // $FF: renamed from: animals () void
   public final void method_295() {
      for(int i = 0; i < this.options.option_count; ++i) {
         this.field_449.add(new class_93(i, this.field_447 / 2 - 155 + i % 2 * 160, this.field_448 / 6 + 24 * (i >> 1), this.options.method_278(i)));
      }

      this.field_449.add(new class_92(100, this.field_447 / 2 - 100, this.field_448 / 6 + 120 + 12, "Controls..."));
      this.field_449.add(new class_92(200, this.field_447 / 2 - 100, this.field_448 / 6 + 168, "Done"));
   }

   // $FF: renamed from: game (net.minecraft.client.entities.r) void
   protected final void method_293(class_92 var1) {
      if (var1.field_444) {
         if (var1.field_443 < 100) {
            this.options.method_277(var1.field_443, 1);
            var1.field_442 = this.options.method_278(var1.field_443);
         }

         if (var1.field_443 == 100) {
            this.game.method_322(new class_96(this, this.options));
         }

         if (var1.field_443 == 200) {
            this.game.method_322(this.field_470);
         }

      }
   }

   // $FF: renamed from: game (int, int, float) void
   public final void method_290(int var1, int var2, float var3) {
      this.method_300();
      method_283(this.field_451, this.field_471, this.field_447 / 2, 20, 16777215);
      super.method_290(var1, var2, var3);
   }
}
