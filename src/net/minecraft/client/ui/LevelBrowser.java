package net.minecraft.client.ui;

import java.awt.FileDialog;
import java.io.File;

// $FF: renamed from: net.minecraft.client.entities.particles
final class LevelBrowser extends Thread {
   // $FF: renamed from: game net.minecraft.client.entities.player
   // $FF: synthetic field
   private WorldLoader field_966;

   LevelBrowser(WorldLoader var1) {
      super();
      this.field_966 = var1;
   }

   public final void run() {
      try {
         FileDialog df = this.field_966.method_304();
         File f;
         (f = new File(this.field_966.game.field_574, "saves")).mkdir();
         String var5;
         if (!(var5 = f.toString()).endsWith(File.separator)) {
            var5 = var5 + File.separator;
         }

         df.setDirectory(var5);
         LevelNameFilter var6 = new LevelNameFilter(this);
         df.setFilenameFilter(var6);
         df.setLocationRelativeTo(this.field_966.game.canvas);
         df.setVisible(true);
         if (df.getFile() != null) {
            if (!(var5 = df.getDirectory()).endsWith(File.separator)) {
               var5 = var5 + File.separator;
            }

            WorldLoader.method_307(this.field_966, new File(var5 + df.getFile()));
         }
      } finally {
         WorldLoader.method_308(this.field_966, false);
      }

   }
}
