package net.minecraft.client.ui;

// $FF: renamed from: net.minecraft.client.entities.n
public final class WorldCreationElement extends ClickableElement {
   // $FF: renamed from: game net.minecraft.client.entities.i
   private ClickableElement field_452;
   // $FF: renamed from: i java.lang.String[]
   private String[] type_option = new String[]{"Inland", "Island", "Floating", "Flat"};
   // $FF: renamed from: j java.lang.String[]
   private String[] shape_option = new String[]{"Square", "Long", "Deep"};
   // $FF: renamed from: k java.lang.String[]
   private String[] size_option = new String[]{"Small", "Normal", "Huge"};
   // $FF: renamed from: l java.lang.String[]
   private String[] theme_option = new String[]{"Normal", "Hell", "Paradise", "Woods"};
   // $FF: renamed from: m int
   private int type = 1;
   // $FF: renamed from: n int
   private int shape = 0;
   // $FF: renamed from: o int
   private int size = 1;
   // $FF: renamed from: p int
   private int theme = 0;

   public WorldCreationElement(ClickableElement var1) {
      this.field_452 = var1;
   }

   // $FF: renamed from: animals () void
   public final void method_295() {
      this.field_449.clear();
      this.field_449.add(new class_92(0, this.field_447 / 2 - 100, this.field_448 / 4, "Type: "));
      this.field_449.add(new class_92(1, this.field_447 / 2 - 100, this.field_448 / 4 + 24, "Shape:"));
      this.field_449.add(new class_92(2, this.field_447 / 2 - 100, this.field_448 / 4 + 48, "Size: "));
      this.field_449.add(new class_92(3, this.field_447 / 2 - 100, this.field_448 / 4 + 72, "Theme: "));
      this.field_449.add(new class_92(4, this.field_447 / 2 - 100, this.field_448 / 4 + 96 + 12, "Create"));
      this.field_449.add(new class_92(5, this.field_447 / 2 - 100, this.field_448 / 4 + 120 + 12, "Cancel"));
      this.method_302();
   }

   // $FF: renamed from: player () void
   private void method_302() {
      ((class_92)this.field_449.get(0)).field_442 = "Type: " + this.type_option[this.type];
      ((class_92)this.field_449.get(1)).field_442 = "Shape: " + this.shape_option[this.shape];
      ((class_92)this.field_449.get(2)).field_442 = "Size: " + this.size_option[this.size];
      ((class_92)this.field_449.get(3)).field_442 = "Theme: " + this.theme_option[this.theme];
   }

   // $FF: renamed from: game (net.minecraft.client.entities.r) void
   protected final void method_293(class_92 var1) {
      if (var1.field_443 == 5) {
         this.game.method_322(this.field_452);
      } else if (var1.field_443 == 4) {
         this.game.generate_and_enter_world(this.size, this.shape, this.type, this.theme);
         this.game.method_322((ClickableElement)null);
      } else if (var1.field_443 == 0) {
         this.type = (this.type + 1) % this.type_option.length;
      } else if (var1.field_443 == 1) {
         this.shape = (this.shape + 1) % this.shape_option.length;
      } else if (var1.field_443 == 2) {
         this.size = (this.size + 1) % this.size_option.length;
      } else if (var1.field_443 == 3) {
         this.theme = (this.theme + 1) % this.theme_option.length;
      }

      this.method_302();
   }

   // $FF: renamed from: game (int, int, float) void
   public final void method_290(int var1, int var2, float var3) {
      this.method_300();
      method_284(this.field_451, "Generate new world", this.field_447 / 2, 40, 16777215);
      super.method_290(var1, var2, var3);
   }
}
