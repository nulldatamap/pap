package net.minecraft.client.ui;

import java.awt.Dialog;
import java.awt.FileDialog;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import net.minecraft.game.world.World;
import net.minecraft.client.SinglePlayerLevelLoader;

// $FF: renamed from: net.minecraft.client.entities.player
public class WorldLoader extends ClickableElement implements Runnable {
   // $FF: renamed from: i net.minecraft.client.entities.i
   private ClickableElement field_473;
   // $FF: renamed from: j boolean
   private boolean field_474 = false;
   // $FF: renamed from: k boolean
   private boolean field_475 = false;
   // $FF: renamed from: l java.lang.String[]
   private String[] field_476 = null;
   // $FF: renamed from: m java.lang.String
   private String field_477 = "";
   // $FF: renamed from: game java.lang.String
   protected String field_478 = "Load world";
   // $FF: renamed from: n boolean
   private boolean field_479 = false;
   // $FF: renamed from: o java.io.File
   private File file;

   public WorldLoader(ClickableElement var1) {
      this.field_473 = var1;
   }

   // $FF: renamed from: f_ () void
   public final void method_298() {
      if (this.file != null) {
         if (!this.file.getName().endsWith(".mclevel")) {
            this.file = new File(this.file.getAbsolutePath() + ".mclevel");
         }

         this.load(this.file);
         this.file = null;
         this.game.method_322((ClickableElement)null);
      }

   }

   public void run() {
      try {
         this.field_477 = "Getting world list..";
         String var3 = "-;-;-;-;-;";
         this.field_476 = var3.split(";");
         if (this.field_476.length >= 5) {
            this.method_303(this.field_476);
            this.field_475 = true;
            return;
         }

         this.field_477 = this.field_476[0];
         this.field_474 = true;
      } catch (Exception var2) {
         var2.printStackTrace();
         this.field_477 = "Failed to load levels";
         this.field_474 = true;
      }

   }

   // $FF: renamed from: game (java.lang.String[]) void
   protected void method_303(String[] var1) {
      for(int var2 = 0; var2 < 5; ++var2) {
         ((class_92)this.field_449.get(var2)).field_444 = !var1[var2].equals("-");
         ((class_92)this.field_449.get(var2)).field_442 = var1[var2];
         ((class_92)this.field_449.get(var2)).field_445 = true;
      }

      ((class_92)this.field_449.get(5)).field_445 = true;
   }

   // $FF: renamed from: animals () void
   public void method_295() {
      (new Thread(this)).start();

      for(int var1 = 0; var1 < 5; ++var1) {
         this.field_449.add(new class_92(var1, this.field_447 / 2 - 100, this.field_448 / 6 + var1 * 24, "---"));
         ((class_92)this.field_449.get(var1)).field_445 = false;
      }

      this.field_449.add(new class_92(5, this.field_447 / 2 - 100, this.field_448 / 6 + 120 + 12, "Load file..."));
      this.field_449.add(new class_92(6, this.field_447 / 2 - 100, this.field_448 / 6 + 168, "Cancel"));
      ((class_92)this.field_449.get(5)).field_445 = false;
   }

   // $FF: renamed from: game (net.minecraft.client.entities.r) void
   protected final void method_293(class_92 var1) {
      if (!this.field_479 && var1.field_444) {
         if (this.field_475 && var1.field_443 < 5) {
            this.method_305(var1.field_443);
         }

         if (this.field_474 || this.field_475 && var1.field_443 == 5) {
            this.field_479 = true;
            LevelBrowser var2;
            (var2 = new LevelBrowser(this)).setDaemon(true);
            var2.start();
         }

         if (this.field_474 || this.field_475 && var1.field_443 == 6) {
            this.game.method_322(this.field_473);
         }
      }

   }

   // $FF: renamed from: player () java.awt.FileDialog
   protected FileDialog method_304() {
      return new FileDialog((Dialog)null, "Load world", 0);
   }

   // $FF: renamed from: game (int) void
   protected void method_305(int var1) {
      this.game.method_322((ClickableElement)null);
      this.game.method_324();
   }

   // $FF: renamed from: game (int, int, float) void
   public final void method_290(int var1, int var2, float var3) {
      this.method_300();
      method_283(this.field_451, this.field_478, this.field_447 / 2, 20, 16777215);
      if (!this.field_475) {
         method_283(this.field_451, this.field_477, this.field_447 / 2, this.field_448 / 2 - 4, 16777215);
      }

      super.method_290(var1, var2, var3);
   }

   // $FF: renamed from: game (java.io.File) void
   protected void load(File var1) {
      try {
         FileInputStream var4 = new FileInputStream(var1);
         World var2 = (new SinglePlayerLevelLoader(this.game, this.game.field_558)).load(var4);
         var4.close();
         this.game.set_world(var2);
      } catch (IOException var4) {
         var4.printStackTrace();
      }

   }

   // $FF: renamed from: game (net.minecraft.client.entities.player, java.io.File) java.io.File
   static File method_307(WorldLoader var0, File var1) {
      return var0.file = var1;
   }

   // $FF: renamed from: game (net.minecraft.client.entities.player, boolean) boolean
   static boolean method_308(WorldLoader var0, boolean var1) {
      return var0.field_479 = false;
   }
}
