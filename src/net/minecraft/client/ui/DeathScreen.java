package net.minecraft.client.ui;

import net.minecraft.client.player.LocalPlayer;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.entities.projectiles
public final class DeathScreen extends ClickableElement {
   // $FF: renamed from: animals () void
   public final void method_295() {
      this.field_449.clear();
      this.field_449.add(new class_92(1, this.field_447 / 2 - 100, this.field_448 / 4 + 72, "Generate new world..."));
      this.field_449.add(new class_92(2, this.field_447 / 2 - 100, this.field_448 / 4 + 96, "Load world.."));
      if (this.game.account == null) {
         ((class_92)this.field_449.get(1)).field_444 = false;
      }

   }

   // $FF: renamed from: game (char, int) void
   protected final void method_291(char var1, int var2) {
   }

   // $FF: renamed from: game (net.minecraft.client.entities.r) void
   protected final void method_293(class_92 var1) {
      if (var1.field_443 == 0) {
         this.game.method_322(new class_98(this, this.game.options));
      }

      if (var1.field_443 == 1) {
         this.game.method_322(new WorldCreationElement(this));
      }

      if (this.game.account != null && var1.field_443 == 2) {
         this.game.method_322(new WorldLoader(this));
      }

   }

   // $FF: renamed from: game (int, int, float) void
   public final void method_290(int var1, int var2, float var3) {
      method_282(0, 0, this.field_447, this.field_448, 1615855616, -1602211792);
      GL11.glPushMatrix();
      GL11.glScalef(2.0F, 2.0F, 2.0F);
      method_283(this.field_451, "Game over!", this.field_447 / 2 / 2, 30, 16777215);
      GL11.glPopMatrix();
      TextDrawer var10000 = this.field_451;
      StringBuilder var10001 = (new StringBuilder()).append("Score: &player");
      LocalPlayer var4 = this.game.player;
      method_283(var10000, var10001.append(this.game.player.score).toString(), this.field_447 / 2, 100, 16777215);
      super.method_290(var1, var2, var3);
   }

   // $FF: renamed from: entities () boolean
   public final boolean method_301() {
      return false;
   }
}
