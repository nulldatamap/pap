package net.minecraft.client.ui;

// $FF: renamed from: net.minecraft.client.entities.animals
public final class EscMenu extends ClickableElement {
   // $FF: renamed from: animals () void
   public final void method_295() {
      this.field_449.clear();
      this.field_449.add(new class_92(0, this.field_447 / 2 - 100, this.field_448 / 4, "Options..."));
      this.field_449.add(new class_92(1, this.field_447 / 2 - 100, this.field_448 / 4 + 24, "Generate new world..."));
      this.field_449.add(new class_92(2, this.field_447 / 2 - 100, this.field_448 / 4 + 48, "Save world.."));
      this.field_449.add(new class_92(3, this.field_447 / 2 - 100, this.field_448 / 4 + 72, "Load world.."));
      this.field_449.add(new class_92(4, this.field_447 / 2 - 100, this.field_448 / 4 + 120, "Back to game"));
      if (this.game.account == null) {
         ((class_92)this.field_449.get(2)).field_444 = false;
         ((class_92)this.field_449.get(3)).field_444 = false;
      }

   }

   // $FF: renamed from: game (net.minecraft.client.entities.r) void
   protected final void method_293(class_92 var1) {
      if (var1.field_443 == 0) {
         this.game.method_322(new class_98(this, this.game.options));
      }

      if (var1.field_443 == 1) {
         this.game.method_322(new WorldCreationElement(this));
      }

      if (this.game.account != null) {
         if (var1.field_443 == 2) {
            this.game.method_322(new WorldSaveScreen(this));
         }

         if (var1.field_443 == 3) {
            this.game.method_322(new WorldLoader(this));
         }
      }

      if (var1.field_443 == 4) {
         this.game.method_322((ClickableElement)null);
         this.game.method_324();
      }

   }

   // $FF: renamed from: game (int, int, float) void
   public final void method_290(int var1, int var2, float var3) {
      this.method_300();
      method_283(this.field_451, "Game menu", this.field_447 / 2, 40, 16777215);
      super.method_290(var1, var2, var3);
   }
}
