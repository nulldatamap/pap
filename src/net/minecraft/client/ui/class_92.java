package net.minecraft.client.ui;

import net.minecraft.client.Game;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.entities.r
public class class_92 extends UIElement {
   // $FF: renamed from: player int
   private int field_438;
   // $FF: renamed from: particles int
   private int field_439;
   // $FF: renamed from: player int
   private int field_440;
   // $FF: renamed from: i int
   private int field_441;
   // $FF: renamed from: game java.lang.String
   public String field_442;
   // $FF: renamed from: animals int
   public int field_443;
   // $FF: renamed from: entities boolean
   public boolean field_444;
   // $FF: renamed from: projectiles boolean
   public boolean field_445;

   public class_92(int var1, int var2, int var3, String var4) {
      this(var1, var2, var3, 200, 20, var4);
   }

   protected class_92(int var1, int var2, int var3, int var4, int var5, String var6) {
      this.field_438 = 200;
      this.field_439 = 20;
      this.field_444 = true;
      this.field_445 = true;
      this.field_443 = var1;
      this.field_440 = var2;
      this.field_441 = var3;
      this.field_438 = var4;
      this.field_439 = 20;
      this.field_442 = var6;
   }

   // $FF: renamed from: game (net.minecraft.client.projectiles, int, int) void
   public final void method_288(Game var1, int var2, int var3) {
      if (this.field_445) {
         TextDrawer var4 = var1.text_drawer;
         GL11.glBindTexture(3553, var1.resource_manager.load_texture("/gui/gui.png"));
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         byte var5 = 1;
         boolean var6 = var2 >= this.field_440 && var3 >= this.field_441 && var2 < this.field_440 + this.field_438 && var3 < this.field_441 + this.field_439;
         if (!this.field_444) {
            var5 = 0;
         } else if (var6) {
            var5 = 2;
         }

         this.draw_pane(this.field_440, this.field_441, 0, 46 + var5 * 20, this.field_438 / 2, this.field_439);
         this.draw_pane(this.field_440 + this.field_438 / 2, this.field_441, 200 - this.field_438 / 2, 46 + var5 * 20, this.field_438 / 2, this.field_439);
         if (!this.field_444) {
            method_283(var4, this.field_442, this.field_440 + this.field_438 / 2, this.field_441 + (this.field_439 - 8) / 2, -6250336);
         } else if (var6) {
            method_283(var4, this.field_442, this.field_440 + this.field_438 / 2, this.field_441 + (this.field_439 - 8) / 2, 16777120);
         } else {
            method_283(var4, this.field_442, this.field_440 + this.field_438 / 2, this.field_441 + (this.field_439 - 8) / 2, 14737632);
         }
      }
   }

   // $FF: renamed from: game (int, int) boolean
   public final boolean method_289(int var1, int var2) {
      return this.field_444 && var1 >= this.field_440 && var2 >= this.field_441 && var1 < this.field_440 + this.field_438 && var2 < this.field_441 + this.field_439;
   }
}
