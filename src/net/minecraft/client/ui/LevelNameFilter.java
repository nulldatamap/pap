package net.minecraft.client.ui;

import java.io.File;
import java.io.FilenameFilter;

// $FF: renamed from: net.minecraft.client.entities.q
final class LevelNameFilter implements FilenameFilter {
   LevelNameFilter(LevelBrowser var1) {
   }

   public final boolean accept(File var1, String var2) {
      return var2.toLowerCase().endsWith(".mclevel");
   }
}
