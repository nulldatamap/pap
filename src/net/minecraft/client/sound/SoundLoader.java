package net.minecraft.client.sound;

import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

// $FF: renamed from: net.minecraft.client.player.animals
public final class SoundLoader {
   // $FF: renamed from: game java.util.Random
   private Random random = new Random();
   // $FF: renamed from: animals java.util.Map
   private Map field_260 = new HashMap();
   // $FF: renamed from: entities int
   private int field_261 = 0;

   // $FF: renamed from: game (java.lang.String, java.io.File) net.minecraft.client.player.game
   public final Sound load_sound(String var1, File var2) {
      try {
         String var3 = var1;

         for(var1 = var1.substring(0, var1.indexOf(".")); Character.isDigit(var1.charAt(var1.length() - 1)); var1 = var1.substring(0, var1.length() - 1)) {
            ;
         }

         var1 = var1.replaceAll("/", ".");
         if (!this.field_260.containsKey(var1)) {
            this.field_260.put(var1, new ArrayList());
         }

         Sound var5 = new Sound(var3, var2.toURI().toURL());
         ((List)this.field_260.get(var1)).add(var5);
         ++this.field_261;
         return var5;
      } catch (MalformedURLException var4) {
         var4.printStackTrace();
         throw new RuntimeException(var4);
      }
   }

   // $FF: renamed from: game (java.lang.String) net.minecraft.client.player.game
   public final Sound get_sound(String var1) {
      List var2;
      return (var2 = (List)this.field_260.get(var1)) == null ? null : (Sound)var2.get(this.random.nextInt(var2.size()));
   }
}
