package net.minecraft.client.sound;

import a.MathHelper;
import java.io.File;
import net.minecraft.game.entities.Mob;
import net.minecraft.client.GameOptions;
import paulscode.sound.SoundSystem;
import paulscode.sound.SoundSystemConfig;
import paulscode.sound.codecs.CodecJOrbis;
import paulscode.sound.codecs.CodecWav;
import paulscode.sound.libraries.LibraryLWJGLOpenAL;

// $FF: renamed from: net.minecraft.client.player.entities
public final class SoundManager {
   // $FF: renamed from: game paulscode.sound.SoundSystem
   private SoundSystem sound_system;
   // $FF: renamed from: animals net.minecraft.client.player.animals
   private SoundLoader effect_loader = new SoundLoader();
   // $FF: renamed from: entities net.minecraft.client.player.animals
   private SoundLoader music_loader = new SoundLoader();
   // $FF: renamed from: projectiles int
   private int source_id = 0;
   // $FF: renamed from: player net.minecraft.client.q
   private GameOptions options;
   // $FF: renamed from: particles boolean
   private boolean sound_working = false;

   // $FF: renamed from: game (net.minecraft.client.q) void
   public final void set_options(GameOptions opt) {
      this.options = opt;
      if (!this.sound_working && (opt.sound_enabled || opt.music_enabled)) {
         this.link_soundsystem();
      }

   }

   // $FF: renamed from: entities () void
   private void link_soundsystem() {
      try {
         boolean var1 = this.options.sound_enabled;
         boolean var2 = this.options.music_enabled;
         this.options.sound_enabled = false;
         this.options.music_enabled = false;
         this.options.save_options();
         SoundSystemConfig.addLibrary(LibraryLWJGLOpenAL.class);
         SoundSystemConfig.setCodec("ogg", CodecJOrbis.class);
         SoundSystemConfig.setCodec("wav", CodecWav.class);
         this.sound_system = new SoundSystem();
         this.options.sound_enabled = var1;
         this.options.music_enabled = var2;
         this.options.save_options();
      } catch (Throwable var3) {
         System.err.println("error linking with the LibraryJavaSound plug-in");
      }

      this.sound_working = true;
   }

   // $FF: renamed from: game () void
   public final void stop_music() {
      if (!this.sound_working && (this.options.sound_enabled || this.options.music_enabled)) {
         this.link_soundsystem();
      }

      if (!this.options.music_enabled) {
         this.sound_system.stop("BgMusic");
      }

   }

   // $FF: renamed from: animals () void
   public final void cleanup() {
      if (this.sound_working) {
         this.sound_system.cleanup();
      }

   }

   // $FF: renamed from: game (java.lang.String, java.io.File) void
   public final void load_sound(String var1, File var2) {
      this.effect_loader.load_sound(var1, var2);
   }

   // $FF: renamed from: animals (java.lang.String, java.io.File) void
   public final void load_music(String var1, File var2) {
      this.music_loader.load_sound(var1, var2);
   }

   // $FF: renamed from: game (float, float, float) void
   public final void play_music(float var1, float var2, float var3) {
      if (this.sound_working && this.options.music_enabled) {
         if (!this.sound_system.playing("BgMusic")) {
            Sound var4 = this.music_loader.get_sound("calm");
            this.sound_system.newStreamingSource(true, "BgMusic", var4.url, var4.name, false, var1, var2, var3, 2, 32.0F);
            this.sound_system.play("BgMusic");
         }

      }
   }

   // $FF: renamed from: game (net.minecraft.game.entities.player, float) void
   public final void update_listener(Mob listener, float dt) {
      if (this.sound_working && this.options.sound_enabled) {
         if (listener != null) {
            float var3 = listener.prev_pitch + (listener.pitch - listener.prev_pitch) * dt;
            float var4 = listener.prev_yaw + (listener.yaw - listener.prev_yaw) * dt;
            float var5 = listener.prev_x + (listener.pos_x - listener.prev_x) * dt;
            float var6 = listener.prev_y + (listener.pos_y - listener.prev_y) * dt;
            float var10 = listener.prev_z + (listener.pos_z - listener.prev_z) * dt;
            dt = MathHelper.cos(-var4 * 0.017453292F - 3.1415927F);
            var4 = MathHelper.sin(-var4 * 0.017453292F - 3.1415927F);
            float var7 = MathHelper.cos(-var3 * 0.017453292F);
            var3 = MathHelper.sin(-var3 * 0.017453292F);
            float var8 = -var4 * var7;
            float var9 = -dt * var7;
            var4 = -var4 * var3;
            dt = -dt * var3;
            this.sound_system.setListenerPosition(var5, var6, var10);
            this.sound_system.setListenerOrientation(var8, var3, var9, var4, var7, dt);
         }
      }
   }

   // $FF: renamed from: game (java.lang.String, float, float, float, float, float) void
   public final void play_sound(String var1, float var2, float var3, float var4, float var5, float var6) {
      if (this.sound_working && this.options.sound_enabled) {
         Sound var9;
         if ((var9 = this.effect_loader.get_sound(var1)) != null && var5 > 0.0F) {
            this.source_id = (this.source_id + 1) % 256;
            String var7 = "sound_" + this.source_id;
            float var8 = 16.0F;
            if (var5 > 1.0F) {
               var8 = 16.0F * var5;
            }

            this.sound_system.newSource(var5 > 1.0F, var7, var9.url, var9.name, false, var2, var3, var4, 2, var8);
            this.sound_system.setPitch(var7, var6);
            if (var5 > 1.0F) {
               var5 = 1.0F;
            }

            this.sound_system.setVolume(var7, var5);
            this.sound_system.play(var7);
         }

      }
   }

   // $FF: renamed from: game (java.lang.String, float, float) void
   public final void play_sound(String var1, float var2, float var3) {
      if (this.sound_working && this.options.sound_enabled) {
         Sound var4;
         if ((var4 = this.effect_loader.get_sound(var1)) != null) {
            this.source_id = (this.source_id + 1) % 256;
            String var5 = "sound_" + this.source_id;
            this.sound_system.newSource(false, var5, var4.url, var4.name, false, 0.0F, 0.0F, 0.0F, 0, 0.0F);
            this.sound_system.setPitch(var5, 1.0F);
            this.sound_system.setVolume(var5, 0.25F);
            this.sound_system.play(var5);
         }

      }
   }
}
