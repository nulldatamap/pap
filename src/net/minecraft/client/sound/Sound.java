package net.minecraft.client.sound;

import java.net.URL;

// $FF: renamed from: net.minecraft.client.player.game
public final class Sound {
   // $FF: renamed from: game java.lang.String
   public String name;
   // $FF: renamed from: animals java.net.URL
   public URL url;

   public Sound(String var1, URL var2) {
      this.name = var1;
      this.url = var2;
   }
}
