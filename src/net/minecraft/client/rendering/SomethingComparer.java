package net.minecraft.client.rendering;

import java.util.Comparator;
import net.minecraft.game.entities.player.Player;
import net.minecraft.game.entities.Mob;

// $FF: renamed from: net.minecraft.client.game.game
public final class SomethingComparer implements Comparator {
   // $FF: renamed from: game net.minecraft.game.entities.player.game
   private Player player;

   public SomethingComparer(Player var1) {
      this.player = var1;
   }

   public final int compare(Object obj, Object obj1) {
      RenderChunk h1 = (RenderChunk)obj1;
      obj1 = ((Object) ((RenderChunk)obj));
      obj = ((Object) (this));
      RenderChunk chunk = (RenderChunk)obj1;
      boolean flag = chunk.maybe_is_visible;
      boolean flag1 = h1.maybe_is_visible;
      if(flag && !flag1) {
         return 1;
      }
      return flag1 && !flag || chunk.distance(((Mob) this.player)) >= h1.distance(((Mob) (this.player))) ? -1 : 1;
   }
}
