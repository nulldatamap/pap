package net.minecraft.client.rendering;

import net.minecraft.game.world.World;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.world.material.Material;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.game.particles
public final class BlockRenderer {
   // $FF: renamed from: game net.minecraft.game.game.player
   private World world;
   // $FF: renamed from: animals int
   private int field_718 = -1;
   // $FF: renamed from: entities boolean
   private boolean field_719 = false;

   public BlockRenderer(World var1) {
      this.world = var1;
   }

   public BlockRenderer() {
   }

   // $FF: renamed from: game (net.minecraft.game.game.animals.x, int, int, int, int) void
   public final void method_407(Block block, int x, int z, int y, int var5) {
      this.field_718 = var5;
      this.actually_render_block(block, x, z, y);
      this.field_718 = -1;
   }

   // $FF: renamed from: game (net.minecraft.game.game.animals.x, int, int, int) void
   public final void render_block(Block block, int x, int y, int z) {
      this.field_719 = true;
      this.actually_render_block(block, x, y, z);
      this.field_719 = false;
   }

   // $FF: renamed from: animals (net.minecraft.game.game.animals.x, int, int, int) boolean
   public final boolean actually_render_block(Block block, int x, int y, int z) {
      int var5;
      GeometryDrawer var6;
      float var10;
      boolean var26;
      if ((var5 = block.method_492()) == 0) {
         var6 = GeometryDrawer.singleton;
         var26 = false;
         if (this.field_719 || block.method_497(this.world, x, y - 1, z, 0)) {
            var10 = block.method_496(this.world, x, y - 1, z);
            if (Block.field_867[block.block_id] > 0) {
               var10 = 1.0F;
            }

            var6.set_color(0.5F * var10, 0.5F * var10, 0.5F * var10);
            this.method_414(block, (float)x, (float)y, (float)z, block.method_498(this.world, x, y, z, 0));
            var26 = true;
         }

         if (this.field_719 || block.method_497(this.world, x, y + 1, z, 1)) {
            var10 = block.method_496(this.world, x, y + 1, z);
            if (Block.field_867[block.block_id] > 0) {
               var10 = 1.0F;
            }

            var6.set_color(var10 * 1.0F, var10 * 1.0F, var10 * 1.0F);
            this.method_415(block, (float)x, (float)y, (float)z, block.method_498(this.world, x, y, z, 1));
            var26 = true;
         }

         if (this.field_719 || block.method_497(this.world, x, y, z - 1, 2)) {
            var10 = block.method_496(this.world, x, y, z - 1);
            if (Block.field_867[block.block_id] > 0) {
               var10 = 1.0F;
            }

            var6.set_color(0.8F * var10, 0.8F * var10, 0.8F * var10);
            this.method_416(block, x, y, z, block.method_498(this.world, x, y, z, 2));
            var26 = true;
         }

         if (this.field_719 || block.method_497(this.world, x, y, z + 1, 3)) {
            var10 = block.method_496(this.world, x, y, z + 1);
            if (Block.field_867[block.block_id] > 0) {
               var10 = 1.0F;
            }

            var6.set_color(0.8F * var10, 0.8F * var10, 0.8F * var10);
            this.method_417(block, x, y, z, block.method_498(this.world, x, y, z, 3));
            var26 = true;
         }

         if (this.field_719 || block.method_497(this.world, x - 1, y, z, 4)) {
            var10 = block.method_496(this.world, x - 1, y, z);
            if (Block.field_867[block.block_id] > 0) {
               var10 = 1.0F;
            }

            var6.set_color(0.6F * var10, 0.6F * var10, 0.6F * var10);
            this.method_418(block, x, y, z, block.method_498(this.world, x, y, z, 4));
            var26 = true;
         }

         if (this.field_719 || block.method_497(this.world, x + 1, y, z, 5)) {
            var10 = block.method_496(this.world, x + 1, y, z);
            if (Block.field_867[block.block_id] > 0) {
               var10 = 1.0F;
            }

            var6.set_color(0.6F * var10, 0.6F * var10, 0.6F * var10);
            this.method_419(block, x, y, z, block.method_498(this.world, x, y, z, 5));
            var26 = true;
         }

         return var26;
      } else {
         float var11;
         float var22;
         if (var5 == 4) {
            var6 = GeometryDrawer.singleton;
            var26 = false;
            var10 = block.field_935;
            var11 = block.field_938;
            block.field_938 = var11 - this.method_413(x, y, z);
            if (this.field_719 || block.method_497(this.world, x, y - 1, z, 0)) {
               var22 = block.method_496(this.world, x, y - 1, z);
               var6.set_color(0.5F * var22, 0.5F * var22, 0.5F * var22);
               this.method_414(block, (float)x, (float)y, (float)z, block.method_500(0));
               var26 = true;
            }

            if (this.field_719 || block.method_497(this.world, x, y + 1, z, 1)) {
               var22 = block.method_496(this.world, x, y + 1, z);
               var6.set_color(var22 * 1.0F, var22 * 1.0F, var22 * 1.0F);
               this.method_415(block, (float)x, (float)y, (float)z, block.method_500(1));
               var26 = true;
            }

            block.field_935 = var11 - this.method_413(x, y, z - 1);
            if (this.field_719 || block.field_938 > block.field_935 || block.method_497(this.world, x, y, z - 1, 2)) {
               var22 = block.method_496(this.world, x, y, z - 1);
               var6.set_color(0.8F * var22, 0.8F * var22, 0.8F * var22);
               this.method_416(block, x, y, z, block.method_500(2));
               var26 = true;
            }

            block.field_935 = var11 - this.method_413(x, y, z + 1);
            if (this.field_719 || block.field_938 > block.field_935 || block.method_497(this.world, x, y, z + 1, 3)) {
               var22 = block.method_496(this.world, x, y, z + 1);
               var6.set_color(0.8F * var22, 0.8F * var22, 0.8F * var22);
               this.method_417(block, x, y, z, block.method_500(3));
               var26 = true;
            }

            block.field_935 = var11 - this.method_413(x - 1, y, z);
            if (this.field_719 || block.field_938 > block.field_935 || block.method_497(this.world, x - 1, y, z, 4)) {
               var22 = block.method_496(this.world, x - 1, y, z);
               var6.set_color(0.6F * var22, 0.6F * var22, 0.6F * var22);
               this.method_418(block, x, y, z, block.method_500(4));
               var26 = true;
            }

            block.field_935 = var11 - this.method_413(x + 1, y, z);
            if (this.field_719 || block.field_938 > block.field_935 || block.method_497(this.world, x + 1, y, z, 5)) {
               var22 = block.method_496(this.world, x + 1, y, z);
               var6.set_color(0.6F * var22, 0.6F * var22, 0.6F * var22);
               this.method_419(block, x, y, z, block.method_500(5));
               var26 = true;
            }

            block.field_935 = var10;
            block.field_938 = var11;
            return var26;
         } else if (var5 == 1) {
            var6 = GeometryDrawer.singleton;
            var22 = block.method_496(this.world, x, y, z);
            var6.set_color(var22, var22, var22);
            this.method_411(block, this.world.block_data_at(x, y, z), (float)x, (float)y, (float)z);
            return true;
         } else if (var5 == 6) {
            var6 = GeometryDrawer.singleton;
            var22 = block.method_496(this.world, x, y, z);
            var6.set_color(var22, var22, var22);
            this.method_412(block, this.world.block_data_at(x, y, z), (float)x, (float)y - 0.0625F, (float)z);
            return true;
         } else {
            float var8;
            if (var5 == 2) {
               byte var21 = this.world.block_data_at(x, y, z);
               GeometryDrawer var25 = GeometryDrawer.singleton;
               var8 = block.method_496(this.world, x, y, z);
               if (Block.field_867[block.block_id] > 0) {
                  var8 = 1.0F;
               }

               var25.set_color(var8, var8, var8);
               if (var21 == 1) {
                  this.method_410(block, (float)x - 0.099999994F, (float)y + 0.2F, (float)z, -0.4F, 0.0F);
               } else if (var21 == 2) {
                  this.method_410(block, (float)x + 0.099999994F, (float)y + 0.2F, (float)z, 0.4F, 0.0F);
               } else if (var21 == 3) {
                  this.method_410(block, (float)x, (float)y + 0.2F, (float)z - 0.099999994F, 0.0F, -0.4F);
               } else if (var21 == 4) {
                  this.method_410(block, (float)x, (float)y + 0.2F, (float)z + 0.099999994F, 0.0F, 0.4F);
               } else {
                  this.method_410(block, (float)x, (float)y, (float)z, 0.0F, 0.0F);
               }

               return true;
            } else {
               int var7;
               float var12;
               float var13;
               float var14;
               float var15;
               int var23;
               if (var5 == 3) {
                  var5 = z;
                  z = y;
                  y = x;
                  var6 = GeometryDrawer.singleton;
                  var7 = block.method_500(0);
                  if (this.field_718 >= 0) {
                     var7 = this.field_718;
                  }

                  var8 = block.method_496(this.world, x, z, var5);
                  var6.set_color(var8, var8, var8);
                  x = (var7 & 15) << 4;
                  var23 = var7 & 240;
                  float var24 = (float)x / 256.0F;
                  var10 = ((float)x + 15.99F) / 256.0F;
                  var11 = (float)var23 / 256.0F;
                  var22 = ((float)var23 + 15.99F) / 256.0F;
                  float var16;
                  float var19;
                  if (!this.world.method_170(y, z - 1, var5) && !Block.fire.method_545(this.world, y, z - 1, var5)) {
                     if ((y + z + var5 & 1) == 1) {
                        var24 = (float)x / 256.0F;
                        var10 = ((float)x + 15.99F) / 256.0F;
                        var11 = (float)(var23 + 16) / 256.0F;
                        var22 = ((float)var23 + 15.99F + 16.0F) / 256.0F;
                     }

                     if ((y / 2 + z / 2 + var5 / 2 & 1) == 1) {
                        var14 = var10;
                        var10 = var24;
                        var24 = var14;
                     }

                     if (Block.fire.method_545(this.world, y - 1, z, var5)) {
                        var6.add_vertex((float)y + 0.2F, (float)z + 1.4F + 0.0625F, (float)(var5 + 1), var10, var11);
                        var6.add_vertex((float)y, (float)z + 0.0625F, (float)(var5 + 1), var10, var22);
                        var6.add_vertex((float)y, (float)z + 0.0625F, (float)var5, var24, var22);
                        var6.add_vertex((float)y + 0.2F, (float)z + 1.4F + 0.0625F, (float)var5, var24, var11);
                        var6.add_vertex((float)y + 0.2F, (float)z + 1.4F + 0.0625F, (float)var5, var24, var11);
                        var6.add_vertex((float)y, (float)z + 0.0625F, (float)var5, var24, var22);
                        var6.add_vertex((float)y, (float)z + 0.0625F, (float)(var5 + 1), var10, var22);
                        var6.add_vertex((float)y + 0.2F, (float)z + 1.4F + 0.0625F, (float)(var5 + 1), var10, var11);
                     }

                     if (Block.fire.method_545(this.world, y + 1, z, var5)) {
                        var6.add_vertex((float)(y + 1) - 0.2F, (float)z + 1.4F + 0.0625F, (float)var5, var24, var11);
                        var6.add_vertex((float)(y + 1), (float)z + 0.0625F, (float)var5, var24, var22);
                        var6.add_vertex((float)(y + 1), (float)z + 0.0625F, (float)(var5 + 1), var10, var22);
                        var6.add_vertex((float)(y + 1) - 0.2F, (float)z + 1.4F + 0.0625F, (float)(var5 + 1), var10, var11);
                        var6.add_vertex((float)(y + 1) - 0.2F, (float)z + 1.4F + 0.0625F, (float)(var5 + 1), var10, var11);
                        var6.add_vertex((float)(y + 1), (float)z + 0.0625F, (float)(var5 + 1), var10, var22);
                        var6.add_vertex((float)(y + 1), (float)z + 0.0625F, (float)var5, var24, var22);
                        var6.add_vertex((float)(y + 1) - 0.2F, (float)z + 1.4F + 0.0625F, (float)var5, var24, var11);
                     }

                     if (Block.fire.method_545(this.world, y, z, var5 - 1)) {
                        var6.add_vertex((float)y, (float)z + 1.4F + 0.0625F, (float)var5 + 0.2F, var10, var11);
                        var6.add_vertex((float)y, (float)z + 0.0625F, (float)var5, var10, var22);
                        var6.add_vertex((float)(y + 1), (float)z + 0.0625F, (float)var5, var24, var22);
                        var6.add_vertex((float)(y + 1), (float)z + 1.4F + 0.0625F, (float)var5 + 0.2F, var24, var11);
                        var6.add_vertex((float)(y + 1), (float)z + 1.4F + 0.0625F, (float)var5 + 0.2F, var24, var11);
                        var6.add_vertex((float)(y + 1), (float)z + 0.0625F, (float)var5, var24, var22);
                        var6.add_vertex((float)y, (float)z + 0.0625F, (float)var5, var10, var22);
                        var6.add_vertex((float)y, (float)z + 1.4F + 0.0625F, (float)var5 + 0.2F, var10, var11);
                     }

                     if (Block.fire.method_545(this.world, y, z, var5 + 1)) {
                        var6.add_vertex((float)(y + 1), (float)z + 1.4F + 0.0625F, (float)(var5 + 1) - 0.2F, var24, var11);
                        var6.add_vertex((float)(y + 1), (float)z + 0.0625F, (float)(var5 + 1), var24, var22);
                        var6.add_vertex((float)y, (float)z + 0.0625F, (float)(var5 + 1), var10, var22);
                        var6.add_vertex((float)y, (float)z + 1.4F + 0.0625F, (float)(var5 + 1) - 0.2F, var10, var11);
                        var6.add_vertex((float)y, (float)z + 1.4F + 0.0625F, (float)(var5 + 1) - 0.2F, var10, var11);
                        var6.add_vertex((float)y, (float)z + 0.0625F, (float)(var5 + 1), var10, var22);
                        var6.add_vertex((float)(y + 1), (float)z + 0.0625F, (float)(var5 + 1), var24, var22);
                        var6.add_vertex((float)(y + 1), (float)z + 1.4F + 0.0625F, (float)(var5 + 1) - 0.2F, var24, var11);
                     }

                     if (Block.fire.method_545(this.world, y, z + 1, var5)) {
                        var14 = (float)y + 0.5F + 0.5F;
                        var15 = (float)y + 0.5F - 0.5F;
                        var19 = (float)var5 + 0.5F + 0.5F;
                        var16 = (float)var5 + 0.5F - 0.5F;
                        var24 = (float)x / 256.0F;
                        var10 = ((float)x + 15.99F) / 256.0F;
                        var11 = (float)var23 / 256.0F;
                        var22 = ((float)var23 + 15.99F) / 256.0F;
                        ++z;
                        if ((y + z + var5 & 1) == 0) {
                           var6.add_vertex(var15, (float)z + -0.2F, (float)var5, var10, var11);
                           var6.add_vertex(var14, (float)z, (float)var5, var10, var22);
                           var6.add_vertex(var14, (float)z, (float)(var5 + 1), var24, var22);
                           var6.add_vertex(var15, (float)z + -0.2F, (float)(var5 + 1), var24, var11);
                           var24 = (float)x / 256.0F;
                           var10 = ((float)x + 15.99F) / 256.0F;
                           var11 = (float)(var23 + 16) / 256.0F;
                           var22 = ((float)var23 + 15.99F + 16.0F) / 256.0F;
                           var6.add_vertex(var14, (float)z + -0.2F, (float)(var5 + 1), var10, var11);
                           var6.add_vertex(var15, (float)z, (float)(var5 + 1), var10, var22);
                           var6.add_vertex(var15, (float)z, (float)var5, var24, var22);
                           var6.add_vertex(var14, (float)z + -0.2F, (float)var5, var24, var11);
                        } else {
                           var6.add_vertex((float)y, (float)z + -0.2F, var19, var10, var11);
                           var6.add_vertex((float)y, (float)z, var16, var10, var22);
                           var6.add_vertex((float)(y + 1), (float)z, var16, var24, var22);
                           var6.add_vertex((float)(y + 1), (float)z + -0.2F, var19, var24, var11);
                           var24 = (float)x / 256.0F;
                           var10 = ((float)x + 15.99F) / 256.0F;
                           var11 = (float)(var23 + 16) / 256.0F;
                           var22 = ((float)var23 + 15.99F + 16.0F) / 256.0F;
                           var6.add_vertex((float)(y + 1), (float)z + -0.2F, var16, var10, var11);
                           var6.add_vertex((float)(y + 1), (float)z, var19, var10, var22);
                           var6.add_vertex((float)y, (float)z, var19, var24, var22);
                           var6.add_vertex((float)y, (float)z + -0.2F, var16, var24, var11);
                        }
                     }
                  } else {
                     var12 = (float)y + 0.5F + 0.2F;
                     var13 = (float)y + 0.5F - 0.2F;
                     var14 = (float)var5 + 0.5F + 0.2F;
                     var15 = (float)var5 + 0.5F - 0.2F;
                     var19 = (float)y + 0.5F - 0.3F;
                     var16 = (float)y + 0.5F + 0.3F;
                     float var17 = (float)var5 + 0.5F - 0.3F;
                     float var18 = (float)var5 + 0.5F + 0.3F;
                     var6.add_vertex(var19, (float)z + 1.4F, (float)(var5 + 1), var10, var11);
                     var6.add_vertex(var12, (float)z, (float)(var5 + 1), var10, var22);
                     var6.add_vertex(var12, (float)z, (float)var5, var24, var22);
                     var6.add_vertex(var19, (float)z + 1.4F, (float)var5, var24, var11);
                     var6.add_vertex(var16, (float)z + 1.4F, (float)var5, var10, var11);
                     var6.add_vertex(var13, (float)z, (float)var5, var10, var22);
                     var6.add_vertex(var13, (float)z, (float)(var5 + 1), var24, var22);
                     var6.add_vertex(var16, (float)z + 1.4F, (float)(var5 + 1), var24, var11);
                     var24 = (float)x / 256.0F;
                     var10 = ((float)x + 15.99F) / 256.0F;
                     var11 = (float)(var23 + 16) / 256.0F;
                     var22 = ((float)var23 + 15.99F + 16.0F) / 256.0F;
                     var6.add_vertex((float)(y + 1), (float)z + 1.4F, var18, var10, var11);
                     var6.add_vertex((float)(y + 1), (float)z, var15, var10, var22);
                     var6.add_vertex((float)y, (float)z, var15, var24, var22);
                     var6.add_vertex((float)y, (float)z + 1.4F, var18, var24, var11);
                     var6.add_vertex((float)y, (float)z + 1.4F, var17, var10, var11);
                     var6.add_vertex((float)y, (float)z, var14, var10, var22);
                     var6.add_vertex((float)(y + 1), (float)z, var14, var24, var22);
                     var6.add_vertex((float)(y + 1), (float)z + 1.4F, var17, var24, var11);
                     var12 = (float)y + 0.5F - 0.5F;
                     var13 = (float)y + 0.5F + 0.5F;
                     var14 = (float)var5 + 0.5F - 0.5F;
                     var15 = (float)var5 + 0.5F + 0.5F;
                     var19 = (float)y + 0.5F - 0.4F;
                     var16 = (float)y + 0.5F + 0.4F;
                     var17 = (float)var5 + 0.5F - 0.4F;
                     var18 = (float)var5 + 0.5F + 0.4F;
                     var6.add_vertex(var19, (float)z + 1.4F, (float)var5, var24, var11);
                     var6.add_vertex(var12, (float)z, (float)var5, var24, var22);
                     var6.add_vertex(var12, (float)z, (float)(var5 + 1), var10, var22);
                     var6.add_vertex(var19, (float)z + 1.4F, (float)(var5 + 1), var10, var11);
                     var6.add_vertex(var16, (float)z + 1.4F, (float)(var5 + 1), var24, var11);
                     var6.add_vertex(var13, (float)z, (float)(var5 + 1), var24, var22);
                     var6.add_vertex(var13, (float)z, (float)var5, var10, var22);
                     var6.add_vertex(var16, (float)z + 1.4F, (float)var5, var10, var11);
                     var24 = (float)x / 256.0F;
                     var10 = ((float)x + 15.99F) / 256.0F;
                     var11 = (float)var23 / 256.0F;
                     var22 = ((float)var23 + 15.99F) / 256.0F;
                     var6.add_vertex((float)y, (float)z + 1.4F, var18, var24, var11);
                     var6.add_vertex((float)y, (float)z, var15, var24, var22);
                     var6.add_vertex((float)(y + 1), (float)z, var15, var10, var22);
                     var6.add_vertex((float)(y + 1), (float)z + 1.4F, var18, var10, var11);
                     var6.add_vertex((float)(y + 1), (float)z + 1.4F, var17, var24, var11);
                     var6.add_vertex((float)(y + 1), (float)z, var14, var24, var22);
                     var6.add_vertex((float)y, (float)z, var14, var10, var22);
                     var6.add_vertex((float)y, (float)z + 1.4F, var17, var10, var11);
                  }

                  return true;
               } else if (var5 == 5) {
                  var5 = z;
                  z = y;
                  y = x;
                  var6 = GeometryDrawer.singleton;
                  var7 = block.method_500(0);
                  if (this.field_718 >= 0) {
                     var7 = this.field_718;
                  }

                  var8 = block.method_496(this.world, x, z, var5);
                  var6.set_color(var8, var8, var8);
                  x = ((var7 & 15) << 4) + 16;
                  var23 = (var7 & 15) << 4;
                  int var9 = var7 & 240;
                  if ((y + z + var5 & 1) == 1) {
                     x = (var7 & 15) << 4;
                     var23 = ((var7 & 15) << 4) + 16;
                  }

                  var10 = (float)x / 256.0F;
                  var11 = ((float)x + 15.99F) / 256.0F;
                  var22 = (float)var9 / 256.0F;
                  float var20 = ((float)var9 + 15.99F) / 256.0F;
                  var12 = (float)var23 / 256.0F;
                  var13 = ((float)var23 + 15.99F) / 256.0F;
                  var14 = (float)var9 / 256.0F;
                  var15 = ((float)var9 + 15.99F) / 256.0F;
                  if (this.world.method_170(y - 1, z, var5)) {
                     var6.add_vertex((float)y + 0.05F, (float)(z + 1) + 0.125F, (float)(var5 + 1) + 0.125F, var10, var22);
                     var6.add_vertex((float)y + 0.05F, (float)z - 0.125F, (float)(var5 + 1) + 0.125F, var10, var20);
                     var6.add_vertex((float)y + 0.05F, (float)z - 0.125F, (float)var5 - 0.125F, var11, var20);
                     var6.add_vertex((float)y + 0.05F, (float)(z + 1) + 0.125F, (float)var5 - 0.125F, var11, var22);
                  }

                  if (this.world.method_170(y + 1, z, var5)) {
                     var6.add_vertex((float)(y + 1) - 0.05F, (float)z - 0.125F, (float)(var5 + 1) + 0.125F, var11, var20);
                     var6.add_vertex((float)(y + 1) - 0.05F, (float)(z + 1) + 0.125F, (float)(var5 + 1) + 0.125F, var11, var22);
                     var6.add_vertex((float)(y + 1) - 0.05F, (float)(z + 1) + 0.125F, (float)var5 - 0.125F, var10, var22);
                     var6.add_vertex((float)(y + 1) - 0.05F, (float)z - 0.125F, (float)var5 - 0.125F, var10, var20);
                  }

                  if (this.world.method_170(y, z, var5 - 1)) {
                     var6.add_vertex((float)(y + 1) + 0.125F, (float)z - 0.125F, (float)var5 + 0.05F, var13, var15);
                     var6.add_vertex((float)(y + 1) + 0.125F, (float)(z + 1) + 0.125F, (float)var5 + 0.05F, var13, var14);
                     var6.add_vertex((float)y - 0.125F, (float)(z + 1) + 0.125F, (float)var5 + 0.05F, var12, var14);
                     var6.add_vertex((float)y - 0.125F, (float)z - 0.125F, (float)var5 + 0.05F, var12, var15);
                  }

                  if (this.world.method_170(y, z, var5 + 1)) {
                     var6.add_vertex((float)(y + 1) + 0.125F, (float)(z + 1) + 0.125F, (float)(var5 + 1) - 0.05F, var12, var14);
                     var6.add_vertex((float)(y + 1) + 0.125F, (float)z - 0.125F, (float)(var5 + 1) - 0.05F, var12, var15);
                     var6.add_vertex((float)y - 0.125F, (float)z - 0.125F, (float)(var5 + 1) - 0.05F, var13, var15);
                     var6.add_vertex((float)y - 0.125F, (float)(z + 1) + 0.125F, (float)(var5 + 1) - 0.05F, var13, var14);
                  }

                  return true;
               } else {
                  return false;
               }
            }
         }
      }
   }

   // $FF: renamed from: game (net.minecraft.game.game.animals.x, float, float, float, float, float) void
   private void method_410(Block var1, float var2, float var3, float var4, float var5, float var6) {
      GeometryDrawer var7 = GeometryDrawer.singleton;
      int var19 = var1.method_500(0);
      if (this.field_718 >= 0) {
         var19 = this.field_718;
      }

      int var8 = (var19 & 15) << 4;
      var19 &= 240;
      float var9 = (float)var8 / 256.0F;
      float var21 = ((float)var8 + 15.99F) / 256.0F;
      float var10 = (float)var19 / 256.0F;
      float var20 = ((float)var19 + 15.99F) / 256.0F;
      float var11 = var9 + 0.02734375F;
      float var12 = var10 + 0.0234375F;
      float var13 = var9 + 0.03515625F;
      float var14 = var10 + 0.03125F;
      var2 += 0.5F;
      var4 += 0.5F;
      float var15 = var2 - 0.5F;
      float var16 = var2 + 0.5F;
      float var17 = var4 - 0.5F;
      float var18 = var4 + 0.5F;
      var7.add_vertex(var2 + var5 * 0.375F - 0.0625F, var3 + 0.625F, var4 + var6 * 0.375F - 0.0625F, var11, var12);
      var7.add_vertex(var2 + var5 * 0.375F - 0.0625F, var3 + 0.625F, var4 + var6 * 0.375F + 0.0625F, var11, var14);
      var7.add_vertex(var2 + var5 * 0.375F + 0.0625F, var3 + 0.625F, var4 + var6 * 0.375F + 0.0625F, var13, var14);
      var7.add_vertex(var2 + var5 * 0.375F + 0.0625F, var3 + 0.625F, var4 + var6 * 0.375F - 0.0625F, var13, var12);
      var7.add_vertex(var2 - 0.0625F, var3 + 1.0F, var17, var9, var10);
      var7.add_vertex(var2 - 0.0625F + var5, var3, var17 + var6, var9, var20);
      var7.add_vertex(var2 - 0.0625F + var5, var3, var18 + var6, var21, var20);
      var7.add_vertex(var2 - 0.0625F, var3 + 1.0F, var18, var21, var10);
      var7.add_vertex(var2 + 0.0625F, var3 + 1.0F, var18, var9, var10);
      var7.add_vertex(var2 + var5 + 0.0625F, var3, var18 + var6, var9, var20);
      var7.add_vertex(var2 + var5 + 0.0625F, var3, var17 + var6, var21, var20);
      var7.add_vertex(var2 + 0.0625F, var3 + 1.0F, var17, var21, var10);
      var7.add_vertex(var15, var3 + 1.0F, var4 + 0.0625F, var9, var10);
      var7.add_vertex(var15 + var5, var3, var4 + 0.0625F + var6, var9, var20);
      var7.add_vertex(var16 + var5, var3, var4 + 0.0625F + var6, var21, var20);
      var7.add_vertex(var16, var3 + 1.0F, var4 + 0.0625F, var21, var10);
      var7.add_vertex(var16, var3 + 1.0F, var4 - 0.0625F, var9, var10);
      var7.add_vertex(var16 + var5, var3, var4 - 0.0625F + var6, var9, var20);
      var7.add_vertex(var15 + var5, var3, var4 - 0.0625F + var6, var21, var20);
      var7.add_vertex(var15, var3 + 1.0F, var4 - 0.0625F, var21, var10);
   }

   // $FF: renamed from: game (net.minecraft.game.game.animals.x, int, float, float, float) void
   private void method_411(Block var1, int var2, float var3, float var4, float var5) {
      GeometryDrawer var6 = GeometryDrawer.singleton;
      int var11 = var1.method_499(0, var2);
      if (this.field_718 >= 0) {
         var11 = this.field_718;
      }

      var2 = (var11 & 15) << 4;
      var11 &= 240;
      float var7 = (float)var2 / 256.0F;
      float var12 = ((float)var2 + 15.99F) / 256.0F;
      float var8 = (float)var11 / 256.0F;
      float var13 = ((float)var11 + 15.99F) / 256.0F;
      float var9 = var3 + 0.5F - 0.45F;
      var3 = var3 + 0.5F + 0.45F;
      float var10 = var5 + 0.5F - 0.45F;
      var5 = var5 + 0.5F + 0.45F;
      var6.add_vertex(var9, var4 + 1.0F, var10, var7, var8);
      var6.add_vertex(var9, var4, var10, var7, var13);
      var6.add_vertex(var3, var4, var5, var12, var13);
      var6.add_vertex(var3, var4 + 1.0F, var5, var12, var8);
      var6.add_vertex(var3, var4 + 1.0F, var5, var7, var8);
      var6.add_vertex(var3, var4, var5, var7, var13);
      var6.add_vertex(var9, var4, var10, var12, var13);
      var6.add_vertex(var9, var4 + 1.0F, var10, var12, var8);
      var6.add_vertex(var9, var4 + 1.0F, var5, var7, var8);
      var6.add_vertex(var9, var4, var5, var7, var13);
      var6.add_vertex(var3, var4, var10, var12, var13);
      var6.add_vertex(var3, var4 + 1.0F, var10, var12, var8);
      var6.add_vertex(var3, var4 + 1.0F, var10, var7, var8);
      var6.add_vertex(var3, var4, var10, var7, var13);
      var6.add_vertex(var9, var4, var5, var12, var13);
      var6.add_vertex(var9, var4 + 1.0F, var5, var12, var8);
   }

   // $FF: renamed from: animals (net.minecraft.game.game.animals.x, int, float, float, float) void
   private void method_412(Block var1, int var2, float var3, float var4, float var5) {
      GeometryDrawer var6 = GeometryDrawer.singleton;
      int var13 = var1.method_499(0, var2);
      if (this.field_718 >= 0) {
         var13 = this.field_718;
      }

      var2 = (var13 & 15) << 4;
      var13 &= 240;
      float var7 = (float)var2 / 256.0F;
      float var14 = ((float)var2 + 15.99F) / 256.0F;
      float var8 = (float)var13 / 256.0F;
      float var15 = ((float)var13 + 15.99F) / 256.0F;
      float var9 = var3 + 0.5F - 0.25F;
      float var10 = var3 + 0.5F + 0.25F;
      float var11 = var5 + 0.5F - 0.5F;
      float var12 = var5 + 0.5F + 0.5F;
      var6.add_vertex(var9, var4 + 1.0F, var11, var7, var8);
      var6.add_vertex(var9, var4, var11, var7, var15);
      var6.add_vertex(var9, var4, var12, var14, var15);
      var6.add_vertex(var9, var4 + 1.0F, var12, var14, var8);
      var6.add_vertex(var9, var4 + 1.0F, var12, var7, var8);
      var6.add_vertex(var9, var4, var12, var7, var15);
      var6.add_vertex(var9, var4, var11, var14, var15);
      var6.add_vertex(var9, var4 + 1.0F, var11, var14, var8);
      var6.add_vertex(var10, var4 + 1.0F, var12, var7, var8);
      var6.add_vertex(var10, var4, var12, var7, var15);
      var6.add_vertex(var10, var4, var11, var14, var15);
      var6.add_vertex(var10, var4 + 1.0F, var11, var14, var8);
      var6.add_vertex(var10, var4 + 1.0F, var11, var7, var8);
      var6.add_vertex(var10, var4, var11, var7, var15);
      var6.add_vertex(var10, var4, var12, var14, var15);
      var6.add_vertex(var10, var4 + 1.0F, var12, var14, var8);
      var9 = var3 + 0.5F - 0.5F;
      var10 = var3 + 0.5F + 0.5F;
      var11 = var5 + 0.5F - 0.25F;
      var12 = var5 + 0.5F + 0.25F;
      var6.add_vertex(var9, var4 + 1.0F, var11, var7, var8);
      var6.add_vertex(var9, var4, var11, var7, var15);
      var6.add_vertex(var10, var4, var11, var14, var15);
      var6.add_vertex(var10, var4 + 1.0F, var11, var14, var8);
      var6.add_vertex(var10, var4 + 1.0F, var11, var7, var8);
      var6.add_vertex(var10, var4, var11, var7, var15);
      var6.add_vertex(var9, var4, var11, var14, var15);
      var6.add_vertex(var9, var4 + 1.0F, var11, var14, var8);
      var6.add_vertex(var10, var4 + 1.0F, var12, var7, var8);
      var6.add_vertex(var10, var4, var12, var7, var15);
      var6.add_vertex(var9, var4, var12, var14, var15);
      var6.add_vertex(var9, var4 + 1.0F, var12, var14, var8);
      var6.add_vertex(var9, var4 + 1.0F, var12, var7, var8);
      var6.add_vertex(var9, var4, var12, var7, var15);
      var6.add_vertex(var10, var4, var12, var14, var15);
      var6.add_vertex(var10, var4 + 1.0F, var12, var14, var8);
   }

   // $FF: renamed from: game (int, int, int) float
   private float method_413(int var1, int var2, int var3) {
      return this.world.method_198(var1, var2, var3) != Material.water_block ? 1.0F : (float)this.world.block_data_at(var1, var2, var3) / 9.0F;
   }

   // $FF: renamed from: game (net.minecraft.game.game.animals.x, float, float, float, int) void
   private void method_414(Block var1, float var2, float var3, float var4, int var5) {
      GeometryDrawer var6 = GeometryDrawer.singleton;
      if (this.field_718 >= 0) {
         var5 = this.field_718;
      }

      int var7 = (var5 & 15) << 4;
      var5 &= 240;
      float var8 = (float)var7 / 256.0F;
      float var14 = ((float)var7 + 15.99F) / 256.0F;
      float var9 = (float)var5 / 256.0F;
      float var13 = ((float)var5 + 15.99F) / 256.0F;
      float var10 = var2 + var1.field_934;
      var2 += var1.field_937;
      var3 += var1.field_935;
      float var11 = var4 + var1.field_936;
      float var12 = var4 + var1.field_939;
      var6.add_vertex(var10, var3, var12, var8, var13);
      var6.add_vertex(var10, var3, var11, var8, var9);
      var6.add_vertex(var2, var3, var11, var14, var9);
      var6.add_vertex(var2, var3, var12, var14, var13);
   }

   // $FF: renamed from: animals (net.minecraft.game.game.animals.x, float, float, float, int) void
   private void method_415(Block var1, float var2, float var3, float var4, int var5) {
      GeometryDrawer var6 = GeometryDrawer.singleton;
      if (this.field_718 >= 0) {
         var5 = this.field_718;
      }

      int var7 = (var5 & 15) << 4;
      var5 &= 240;
      float var8 = (float)var7 / 256.0F;
      float var14 = ((float)var7 + 15.99F) / 256.0F;
      float var9 = (float)var5 / 256.0F;
      float var13 = ((float)var5 + 15.99F) / 256.0F;
      float var10 = var2 + var1.field_934;
      var2 += var1.field_937;
      var3 += var1.field_938;
      float var11 = var4 + var1.field_936;
      float var12 = var4 + var1.field_939;
      var6.add_vertex(var2, var3, var12, var14, var13);
      var6.add_vertex(var2, var3, var11, var14, var9);
      var6.add_vertex(var10, var3, var11, var8, var9);
      var6.add_vertex(var10, var3, var12, var8, var13);
   }

   // $FF: renamed from: animals (net.minecraft.game.game.animals.x, int, int, int, int) void
   private void method_416(Block var1, int var2, int var3, int var4, int var5) {
      GeometryDrawer var6 = GeometryDrawer.singleton;
      if (this.field_718 >= 0) {
         var5 = this.field_718;
      }

      int var7 = (var5 & 15) << 4;
      var5 &= 240;
      float var8 = (float)var7 / 256.0F;
      float var16 = ((float)var7 + 15.99F) / 256.0F;
      float var9;
      float var15;
      if (var1.field_935 >= 0.0F && var1.field_938 <= 1.0F) {
         var9 = ((float)var5 + var1.field_935 * 15.99F) / 256.0F;
         var15 = ((float)var5 + var1.field_938 * 15.99F) / 256.0F;
      } else {
         var9 = (float)var5 / 256.0F;
         var15 = ((float)var5 + 15.99F) / 256.0F;
      }

      float var10 = (float)var2 + var1.field_934;
      float var13 = (float)var2 + var1.field_937;
      float var11 = (float)var3 + var1.field_935;
      float var14 = (float)var3 + var1.field_938;
      float var12 = (float)var4 + var1.field_936;
      var6.add_vertex(var10, var14, var12, var16, var9);
      var6.add_vertex(var13, var14, var12, var8, var9);
      var6.add_vertex(var13, var11, var12, var8, var15);
      var6.add_vertex(var10, var11, var12, var16, var15);
   }

   // $FF: renamed from: entities (net.minecraft.game.game.animals.x, int, int, int, int) void
   private void method_417(Block var1, int var2, int var3, int var4, int var5) {
      GeometryDrawer var6 = GeometryDrawer.singleton;
      if (this.field_718 >= 0) {
         var5 = this.field_718;
      }

      int var7 = (var5 & 15) << 4;
      var5 &= 240;
      float var8 = (float)var7 / 256.0F;
      float var16 = ((float)var7 + 15.99F) / 256.0F;
      float var9;
      float var15;
      if (var1.field_935 >= 0.0F && var1.field_938 <= 1.0F) {
         var9 = ((float)var5 + var1.field_935 * 15.99F) / 256.0F;
         var15 = ((float)var5 + var1.field_938 * 15.99F) / 256.0F;
      } else {
         var9 = (float)var5 / 256.0F;
         var15 = ((float)var5 + 15.99F) / 256.0F;
      }

      float var10 = (float)var2 + var1.field_934;
      float var13 = (float)var2 + var1.field_937;
      float var11 = (float)var3 + var1.field_935;
      float var14 = (float)var3 + var1.field_938;
      float var12 = (float)var4 + var1.field_939;
      var6.add_vertex(var10, var14, var12, var8, var9);
      var6.add_vertex(var10, var11, var12, var8, var15);
      var6.add_vertex(var13, var11, var12, var16, var15);
      var6.add_vertex(var13, var14, var12, var16, var9);
   }

   // $FF: renamed from: projectiles (net.minecraft.game.game.animals.x, int, int, int, int) void
   private void method_418(Block var1, int var2, int var3, int var4, int var5) {
      GeometryDrawer var6 = GeometryDrawer.singleton;
      if (this.field_718 >= 0) {
         var5 = this.field_718;
      }

      int var7 = (var5 & 15) << 4;
      var5 &= 240;
      float var8 = (float)var7 / 256.0F;
      float var16 = ((float)var7 + 15.99F) / 256.0F;
      float var9;
      float var15;
      if (var1.field_935 >= 0.0F && var1.field_938 <= 1.0F) {
         var9 = ((float)var5 + var1.field_935 * 15.99F) / 256.0F;
         var15 = ((float)var5 + var1.field_938 * 15.99F) / 256.0F;
      } else {
         var9 = (float)var5 / 256.0F;
         var15 = ((float)var5 + 15.99F) / 256.0F;
      }

      float var13 = (float)var2 + var1.field_934;
      float var10 = (float)var3 + var1.field_935;
      float var14 = (float)var3 + var1.field_938;
      float var11 = (float)var4 + var1.field_936;
      float var12 = (float)var4 + var1.field_939;
      var6.add_vertex(var13, var14, var12, var16, var9);
      var6.add_vertex(var13, var14, var11, var8, var9);
      var6.add_vertex(var13, var10, var11, var8, var15);
      var6.add_vertex(var13, var10, var12, var16, var15);
   }

   // $FF: renamed from: player (net.minecraft.game.game.animals.x, int, int, int, int) void
   private void method_419(Block var1, int var2, int var3, int var4, int var5) {
      GeometryDrawer var6 = GeometryDrawer.singleton;
      if (this.field_718 >= 0) {
         var5 = this.field_718;
      }

      int var7 = (var5 & 15) << 4;
      var5 &= 240;
      float var8 = (float)var7 / 256.0F;
      float var16 = ((float)var7 + 15.99F) / 256.0F;
      float var9;
      float var15;
      if (var1.field_935 >= 0.0F && var1.field_938 <= 1.0F) {
         var9 = ((float)var5 + var1.field_935 * 15.99F) / 256.0F;
         var15 = ((float)var5 + var1.field_938 * 15.99F) / 256.0F;
      } else {
         var9 = (float)var5 / 256.0F;
         var15 = ((float)var5 + 15.99F) / 256.0F;
      }

      float var13 = (float)var2 + var1.field_937;
      float var10 = (float)var3 + var1.field_935;
      float var14 = (float)var3 + var1.field_938;
      float var11 = (float)var4 + var1.field_936;
      float var12 = (float)var4 + var1.field_939;
      var6.add_vertex(var13, var10, var12, var8, var15);
      var6.add_vertex(var13, var10, var11, var16, var15);
      var6.add_vertex(var13, var14, var11, var16, var9);
      var6.add_vertex(var13, var14, var12, var8, var9);
   }

   // $FF: renamed from: game (net.minecraft.game.game.animals.x) void
   public final void fully_render(Block var1) {
      GeometryDrawer var2 = GeometryDrawer.singleton;
      int var3;
      if ((var3 = var1.method_492()) == 0) {
         GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
         var2.quads_mode();
         GeometryDrawer.set_normal(0.0F, -1.0F, 0.0F);
         this.method_414(var1, 0.0F, 0.0F, 0.0F, var1.method_500(0));
         var2.draw();
         var2.quads_mode();
         GeometryDrawer.set_normal(0.0F, 1.0F, 0.0F);
         this.method_415(var1, 0.0F, 0.0F, 0.0F, var1.method_500(1));
         var2.draw();
         var2.quads_mode();
         GeometryDrawer.set_normal(0.0F, 0.0F, -1.0F);
         this.method_416(var1, 0, 0, 0, var1.method_500(2));
         var2.draw();
         var2.quads_mode();
         GeometryDrawer.set_normal(0.0F, 0.0F, 1.0F);
         this.method_417(var1, 0, 0, 0, var1.method_500(3));
         var2.draw();
         var2.quads_mode();
         GeometryDrawer.set_normal(-1.0F, 0.0F, 0.0F);
         this.method_418(var1, 0, 0, 0, var1.method_500(4));
         var2.draw();
         var2.quads_mode();
         GeometryDrawer.set_normal(1.0F, 0.0F, 0.0F);
         this.method_419(var1, 0, 0, 0, var1.method_500(5));
         var2.draw();
         GL11.glTranslatef(0.5F, 0.5F, 0.5F);
      } else if (var3 == 1) {
         var2.quads_mode();
         GeometryDrawer.set_normal(0.0F, -1.0F, 0.0F);
         this.method_411(var1, -1, -0.5F, -0.5F, -0.5F);
         var2.draw();
      } else if (var3 == 6) {
         var2.quads_mode();
         GeometryDrawer.set_normal(0.0F, -1.0F, 0.0F);
         this.method_412(var1, -1, -0.5F, -0.5F, -0.5F);
         var2.draw();
      } else {
         if (var3 == 2) {
            var2.quads_mode();
            GeometryDrawer.set_normal(0.0F, -1.0F, 0.0F);
            this.method_410(var1, -0.5F, -0.5F, -0.5F, 0.0F, 0.0F);
            var2.draw();
         }

      }
   }
}
