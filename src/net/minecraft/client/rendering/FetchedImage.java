package net.minecraft.client.rendering;

import java.awt.image.BufferedImage;

// $FF: renamed from: net.minecraft.client.game.i
public final class FetchedImage {
   // $FF: renamed from: game java.awt.image.BufferedImage
   public BufferedImage field_720;
   // $FF: renamed from: animals int
   public int field_721 = 1;
   // $FF: renamed from: entities int
   public int field_722 = -1;
   // $FF: renamed from: projectiles boolean
   public boolean field_723 = false;

   public FetchedImage(String var1, KindofImage var2) {
      (new ImageFetcher(this, var1, var2)).start();
   }
}
