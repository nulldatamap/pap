package net.minecraft.client.rendering;

import a.MathHelper;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Random;
import javax.imageio.ImageIO;

import net.minecraft.game.world.World;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.world.material.Material;
import net.minecraft.game.entities.Entity;
import net.minecraft.game.util.RaycastResult;
import net.minecraft.game.util.Vector;
import net.minecraft.client.Game;
import net.minecraft.client.Lighting;
import net.minecraft.client.rendering.b.SomeKindOfCamera;
import net.minecraft.client.rendering.b.class_223;
import net.minecraft.client.rendering.b.FustrumCuller;
import net.minecraft.client.ui.WidthHeight;
import net.minecraft.client.gamemode.CreativeMode;
import net.minecraft.client.particles.ParticleManager;
import net.minecraft.client.player.LocalPlayer;
import org.lwjgl.BufferUtils;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;

// $FF: renamed from: net.minecraft.client.game.player
public final class GameRenderer {
   // $FF: renamed from: animals net.minecraft.client.projectiles
   private Game game;
   // $FF: renamed from: entities boolean
   private boolean field_655 = false;
   // $FF: renamed from: projectiles float
   private float rendering_block_distance = 0.0F;
   // $FF: renamed from: game net.minecraft.client.game.player
   public ViewModelRenderer view_model_renderer;
   // $FF: renamed from: player int
   private int tick;
   // $FF: renamed from: particles net.minecraft.game.entities.animals
   private Entity hit_entity = null;
   // $FF: renamed from: player int
   private int field_660;
   // $FF: renamed from: h int
   private int field_661;
   // $FF: renamed from: i java.text.DecimalFormat
   private DecimalFormat decimal_format = new DecimalFormat("0000");
   // $FF: renamed from: j java.nio.ByteBuffer
   private ByteBuffer byte_buffer;
   // $FF: renamed from: k java.nio.FloatBuffer
   private FloatBuffer float_buffer = BufferUtils.createFloatBuffer(16);
   // $FF: renamed from: l java.util.Random
   private Random random = new Random();
   // $FF: renamed from: m int
   private volatile int field_666 = 0;
   // $FF: renamed from: n int
   private volatile int field_667 = 0;
   // $FF: renamed from: o java.nio.FloatBuffer
   private FloatBuffer ohter_float_buffer = BufferUtils.createFloatBuffer(16);
   // $FF: renamed from: p float
   private float fog_b;
   // $FF: renamed from: q float
   private float fog_g;
   // $FF: renamed from: r float
   private float fog_r;
   // $FF: renamed from: s float
   private float prev_light;
   // $FF: renamed from: t float
   private float light;

   public GameRenderer(Game var1) {
      this.game = var1;
      this.view_model_renderer = new ViewModelRenderer(var1);
   }

   // $FF: renamed from: game () void
   public final void update() {
      this.prev_light = this.light;
      float light = this.game.world.light_at((int)this.game.player.pos_x, (int)this.game.player.pos_y, (int)this.game.player.pos_z);
      float distance = (float)(3 - this.game.options.render_distance) / 3.0F;
      light = light * (1.0F - distance) + distance;
      this.light += (light - this.light) * 0.1F;
      ++this.tick;
      this.view_model_renderer.update();
   }

   // $FF: renamed from: animals (float) net.minecraft.game.projectiles.game
   private Vector player_vec(float dt) {
      LocalPlayer player = this.game.player;
      float x = player.prev_x + (player.pos_x - player.prev_x) * dt;
      float y = player.prev_y + (player.pos_y - player.prev_y) * dt;
      float z = player.prev_z + (player.pos_z - player.prev_z) * dt;
      return new Vector(x, y, z);
   }

   // $FF: renamed from: entities (float) void
   private void hurt_camera(float dt) {
      LocalPlayer player = this.game.player;
      float hurt_left = (float)this.game.player.hurt_time - dt;
      // NOTE: Probably causes the rendering glitch
      if (player.hurt_time <= 0) {
         dt += (float)player.death_time;
         GL11.glRotatef(40.0F - 8000.0F / (dt + 200.0F), 0.0F, 0.0F, 1.0F);
      }


      if (hurt_left >= 0.0F) {
         hurt_left = MathHelper.sin((hurt_left /= (float)player.something_related_to_hurt) * hurt_left * hurt_left * hurt_left * 3.1415927F);
         dt = player.hurt_yaw;
         GL11.glRotatef(-player.hurt_yaw, 0.0F, 1.0F, 0.0F);
         GL11.glRotatef(-hurt_left * 14.0F, 0.0F, 0.0F, 1.0F);
         GL11.glRotatef(dt, 0.0F, 1.0F, 0.0F);
      }

   }

   // $FF: renamed from: projectiles (float) void
   private void head_bob(float var1) {
      if (!this.game.options.third_person_mode) {
         LocalPlayer player = this.game.player;
         float var3 = this.game.player.field_24 - player.field_23;
         var3 = player.field_24 + var3 * var1;
         float var4 = player.prev_head_bob_roll + (player.head_bob_roll - player.prev_head_bob_roll) * var1;
         var1 = player.prev_head_bob_pitch + (player.head_bob_pitch - player.prev_head_bob_pitch) * var1;
         GL11.glTranslatef(MathHelper.sin(var3 * 3.1415927F) * var4 * 0.5F, -Math.abs(MathHelper.cos(var3 * 3.1415927F) * var4), 0.0F);
         GL11.glRotatef(MathHelper.sin(var3 * 3.1415927F) * var4 * 3.0F, 0.0F, 0.0F, 1.0F);
         GL11.glRotatef(Math.abs(MathHelper.cos(var3 * 3.1415927F + 0.2F) * var4) * 5.0F, 1.0F, 0.0F, 0.0F);
         GL11.glRotatef(var1, 1.0F, 0.0F, 0.0F);
      }
   }

   // $FF: renamed from: game (float) void
   public final void render_all(float var1) {
      if (this.field_655 && !Display.isActive()) {
         this.game.method_326();
      }

      this.field_655 = Display.isActive();
      int var5;
      int var6;
      if (this.game.field_580) {
         Mouse.getDX();
         byte var2 = 0;
         Mouse.getDY();
         byte var3 = 0;
         this.game.field_573.method_313();
         byte var4 = 1;
         if (this.game.options.mouse_inverted) {
            var4 = -1;
         }

         var5 = var2 + this.game.field_573.field_506;
         var6 = var3 - this.game.field_573.field_507;
         if (var2 != 0 || this.field_660 != 0) {
            System.out.println("xxo: " + var2 + ", " + this.field_660 + ": " + this.field_660 + ", xo: " + var5);
         }

         if (this.field_660 != 0) {
            this.field_660 = 0;
         }

         if (this.field_661 != 0) {
            this.field_661 = 0;
         }

         if (var2 != 0) {
            this.field_660 = var2;
         }

         if (var3 != 0) {
            this.field_661 = var3;
         }

         float var10001 = (float)var5;
         float var11 = (float)(var6 * var4);
         float var9 = var10001;
         LocalPlayer var7 = this.game.player;
         float var13 = this.game.player.pitch;
         float var14 = var7.yaw;
         var7.yaw = (float)((double)var7.yaw + (double)var9 * 0.15D);
         var7.pitch = (float)((double)var7.pitch - (double)var11 * 0.15D);
         if (var7.pitch < -90.0F) {
            var7.pitch = -90.0F;
         }

         if (var7.pitch > 90.0F) {
            var7.pitch = 90.0F;
         }

         var7.prev_pitch += var7.pitch - var13;
         var7.prev_yaw += var7.yaw - var14;
      }

      WidthHeight var8;
      int var10 = (var8 = new WidthHeight(this.game.width, this.game.height)).get_width();
      int var12 = var8.get_height();
      var5 = Mouse.getX() * var10 / this.game.width;
      var6 = var12 - Mouse.getY() * var12 / this.game.height - 1;
      if (this.game.world != null) {
         this.render_player_perspective(var1);
         this.game.hud.render_gui(var1);
      } else {
         GL11.glViewport(0, 0, this.game.width, this.game.height);
         GL11.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
         GL11.glClear(16640);
         GL11.glMatrixMode(5889);
         GL11.glLoadIdentity();
         GL11.glMatrixMode(5888);
         GL11.glLoadIdentity();
         this.setup_ui_view();
      }

      if (this.game.field_557 != null) {
         GL11.glClear(256);
         this.game.field_557.method_290(var5, var6, var1);
      }

      Thread.yield();
      Display.update();
   }

   // $FF: renamed from: animals () void
   public final void take_screenshot() {
      this.game.field_558.method_0("Grabbing large screenshot");
      File var1 = new File(System.getProperty("user.home", "."));

      int var2;
      File var3;
      for(var2 = 0; (var3 = new File(var1, "mc_map_" + this.decimal_format.format((long)var2) + ".png")).exists(); ++var2) {
         ;
      }

      var3 = var3.getAbsoluteFile();
      this.game.field_558.method_1("Rendering");
      this.game.field_558.method_2(0);

      try {
         int var19 = (this.game.world.width << 4) + (this.game.world.depth << 4);
         var2 = (this.game.world.height << 4) + var19 / 2;
         BufferedImage var4;
         Graphics var5 = (var4 = new BufferedImage(var19, var2, 1)).getGraphics();
         int var6 = this.game.width;
         int var7 = this.game.height;
         int var8 = (var19 / var6 + 1) * (var2 / var7 + 1);
         int var9 = 0;

         for(int var10 = 0; var10 < var19; var10 += var6) {
            for(int var11 = 0; var11 < var2; var11 += var7) {
               ++var9;
               this.game.field_558.method_2(var9 * 100 / var8);
               int var10001 = var10 - var19 / 2;
               int var10002 = var11 - var2 / 2;
               float var12 = 0.0F;
               int var14 = var10002;
               int var13 = var10001;
               if (this.byte_buffer == null) {
                  this.byte_buffer = BufferUtils.createByteBuffer(this.game.width * this.game.height << 2);
               }

               LocalPlayer var15 = this.game.player;
               World var16 = this.game.world;
               WorldRenderer var17 = this.game.game_renderer;
               GL11.glViewport(0, 0, this.game.width, this.game.height);
               this.configure_fog_and_render_sky(0.0F);
               GL11.glClear(16640);
               GL11.glEnable(2884);
               this.rendering_block_distance = (float)(512 >> (this.game.options.render_distance << 1));
               GL11.glMatrixMode(5889);
               GL11.glLoadIdentity();
               GL11.glOrtho(0.0D, (double)this.game.width, 0.0D, (double)this.game.height, 10.0D, 10000.0D);
               GL11.glMatrixMode(5888);
               GL11.glLoadIdentity();
               GL11.glTranslatef((float)(-var13), (float)(-var14), -5000.0F);
               GL11.glScalef(16.0F, -16.0F, -16.0F);
               float var22 = 1.0F;
               var22 = 0.0F;
               var22 = 0.0F;
               var22 = 0.0F;
               var22 = 0.0F;
               var22 = 0.0F;
               var22 = 0.5F;
               var22 = 1.0F;
               var22 = 0.0F;
               var22 = -1.0F;
               var22 = 1.0F;
               var22 = 0.0F;
               var22 = 0.0F;
               var22 = 0.0F;
               var22 = -0.5F;
               var22 = 1.0F;
               this.float_buffer.clear();
               this.float_buffer.put(1.0F).put(-0.5F).put(0.0F).put(0.0F);
               this.float_buffer.put(0.0F).put(1.0F).put(-1.0F).put(0.0F);
               this.float_buffer.put(1.0F).put(0.5F).put(0.0F).put(0.0F);
               this.float_buffer.put(0.0F).put(0.0F).put(0.0F).put(1.0F);
               this.float_buffer.flip();
               GL11.glMultMatrix(this.float_buffer);
               GL11.glRotatef(0.0F, 0.0F, 1.0F, 0.0F);
               GL11.glTranslatef((float)(-var16.width) / 2.0F, (float)(-var16.height) / 2.0F, (float)(-var16.depth) / 2.0F);
               class_223 var24 = new class_223();
               this.game.game_renderer.method_366(var24);
               this.game.game_renderer.method_362(var15);
               this.render_fog();
               GL11.glEnable(2912);
               GL11.glFogi(2917, 9729);
               float var23 = (float)var16.height * 8.0F;
               GL11.glFogf(2915, 5000.0F - var23);
               GL11.glFogf(2916, 5000.0F + var23 * 8.0F);
               Lighting.enable_lighting();
               var17.render_entities(this.player_vec(0.0F), var24, 0.0F);
               Lighting.disable_lighting();
               GL11.glBindTexture(3553, this.game.resource_manager.load_texture("/terrain.png"));
               var17.do_occlusion_queries(var15, 0);
               var17.render_grass();
               if (var16.cloud_height < var16.height) {
                  var17.method_357(0.0F);
               }

               GL11.glEnable(3042);
               GL11.glBlendFunc(770, 771);
               GL11.glColorMask(false, false, false, false);
               var13 = var17.do_occlusion_queries(var15, 1);
               GL11.glColorMask(true, true, true, true);
               if (var13 > 0) {
                  var17.maybe_render_ground();
               }

               if (var16.method_181() >= 0) {
                  var17.render_surrounding_water();
               }

               GL11.glDepthMask(true);
               GL11.glDisable(3042);
               GL11.glDisable(2912);
               this.byte_buffer.clear();
               GL11.glPixelStorei(3333, 1);
               GL11.glReadPixels(0, 0, this.game.width, this.game.height, 6407, 5121, this.byte_buffer);
               BufferedImage var21 = create_image_buffer(this.byte_buffer, var6, var7);
               var5.drawImage(var21, var10, var11, (ImageObserver)null);
            }
         }

         var5.dispose();
         this.game.field_558.method_1("Saving as " + var3.toString());
         this.game.field_558.method_2(100);
         FileOutputStream var20 = new FileOutputStream(var3);
         ImageIO.write(var4, "png", var20);
         var20.close();
      } catch (Throwable var18) {
         var18.printStackTrace();
      }
   }

   // $FF: renamed from: game (java.nio.ByteBuffer, int, int) java.awt.image.BufferedImage
   private static BufferedImage create_image_buffer(ByteBuffer buf, int w, int h) {
      buf.position(0).limit(w * h << 2);
      BufferedImage var3;
      int[] var4 = ((DataBufferInt)(var3 = new BufferedImage(w, h, 1)).getRaster().getDataBuffer()).getData();

      for(int var5 = 0; var5 < w * h; ++var5) {
         int var6 = buf.get(var5 * 3) & 255;
         int var7 = buf.get(var5 * 3 + 1) & 255;
         int var8 = buf.get(var5 * 3 + 2) & 255;
         var4[var5] = var6 << 16 | var7 << 8 | var8;
      }

      return var3;
   }

   // $FF: renamed from: player (float) void
   private void render_player_perspective(float dt) {
      GameRenderer renderer = this;
      LocalPlayer player = this.game.player;
      float pitch = player.prev_pitch + (player.pitch - player.prev_pitch) * dt;
      float yaw = player.prev_yaw + (player.yaw - player.prev_yaw) * dt;
      Vector pv = this.player_vec(dt);
      float cy = MathHelper.cos(-yaw * 0.017453292F - 3.1415927F);
      float sy = MathHelper.sin(-yaw * 0.017453292F - 3.1415927F);
      float cp = -MathHelper.cos(-pitch * 0.017453292F);
      float sp = MathHelper.sin(-pitch * 0.017453292F);
      float syp = sy * cp;
      float cyp = cy * cp;
      float reach = this.game.game_mode.reach_distance();
      Vector max_reach = pv.add(syp * reach, sp * reach, cyp * reach);
      this.game.looking_at = this.game.world.check_raycast(pv, max_reach);
      float looking_distance = reach;
      pv = this.player_vec(dt);
      if (this.game.looking_at != null) {
         looking_distance = this.game.looking_at.end_point.distance(pv);
      }

      if (this.game.game_mode instanceof CreativeMode) {
         reach = 32.0F;
      } else {
         if (looking_distance > 3.0F) {
            looking_distance = 3.0F;
         }

         reach = looking_distance;
      }

      max_reach = pv.add(syp * reach, sp * reach, cyp * reach);
      this.hit_entity = null;
      List in_hit_range = this.game.world.hitbox_tree.colliding_entities(player, player.hitbox.extend_sides(syp * reach, sp * reach, cyp * reach));
      float closest_distance = 0.0F;

      for(int i = 0; i < in_hit_range.size(); ++i) {
         Entity current = (Entity)in_hit_range.get(i);
         RaycastResult hit = current.hitbox.grow(0.1F, 0.1F, 0.1F).method_475(pv, max_reach);
         float dist;
         if (current.maybe_is_visible() && hit != null
                 && ((dist = pv.distance(hit.end_point)) < closest_distance || closest_distance == 0.0F)) {
            renderer.hit_entity = current;
            closest_distance = dist;
         }
      }

      if (renderer.hit_entity != null && !(renderer.game.game_mode instanceof CreativeMode)) {
         renderer.game.looking_at = new RaycastResult(renderer.hit_entity);
      }

      for(int render_pass = 0; render_pass < 2; ++render_pass) {
         if (this.game.options.anaglyph_3d) {
            if (render_pass == 0) {
               GL11.glColorMask(false, true, true, false);
            } else {
               GL11.glColorMask(true, false, false, false);
            }
         }

         player = this.game.player;
         World world = this.game.world;
         WorldRenderer game_renderer = this.game.game_renderer;
         ParticleManager particle_manager = this.game.particle_manager;

         GL11.glViewport(0, 0, this.game.width, this.game.height);
         this.configure_fog_and_render_sky(dt);
         GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
         GL11.glEnable(GL11.GL_CULL_FACE);
         float _dt = dt;
         this.rendering_block_distance = (float)(512 >> (this.game.options.render_distance << 1));
         GL11.glMatrixMode(GL11.GL_PROJECTION);
         GL11.glLoadIdentity();
         if (this.game.options.anaglyph_3d) {
            GL11.glTranslatef((float)(-((render_pass << 1) - 1)) * 0.07F, 0.0F, 0.0F);
         }

         player = this.game.player;
         float fov = 70.0F;
         if (player.on_water_block()) {
            fov = 60.0F;
         }

         if (player.health <= 0) {
            cp = (float)player.death_time + dt;
            fov /= (1.0F - 500.0F / (cp + 500.0F)) * 2.0F + 1.0F;
         }

         GLU.gluPerspective(fov, (float)this.game.width / (float)this.game.height, 0.05F, this.rendering_block_distance);
         GL11.glMatrixMode(GL11.GL_MODELVIEW);
         GL11.glLoadIdentity();
         if (this.game.options.anaglyph_3d) {
            GL11.glTranslatef((float)((render_pass << 1) - 1) * 0.1F, 0.0F, 0.0F);
         }

         this.hurt_camera(dt);
         if (this.game.options.view_bobbing) {
            this.head_bob(dt);
         }

         renderer = this;
         player = this.game.player;
         float px = player.prev_x + (player.pos_x - player.prev_x) * dt;
         float py = player.prev_y + (player.pos_y - player.prev_y) * dt;
         float pz = player.prev_z + (player.pos_z - player.prev_z) * dt;
         if (!this.game.options.third_person_mode) {
            GL11.glTranslatef(0.0F, 0.0F, -0.1F);
         } else {
            // Place the 3d person camera as far back as allowed (with a distance of max 4 blocks)
            float furthetst_distance = 4.0F;
            float lookx = -MathHelper.sin(player.yaw / 180.0F * 3.1415927F) * MathHelper.cos(player.pitch / 180.0F * 3.1415927F) * 4.0F;
            float looky = MathHelper.cos(player.yaw / 180.0F * 3.1415927F) * MathHelper.cos(player.pitch / 180.0F * 3.1415927F) * 4.0F;
            float lookz = -MathHelper.sin(player.pitch / 180.0F * 3.1415927F) * 4.0F;

            for(int i = 0; i < 8; ++i) {
               float wx = (float)(((i & 1) << 1) - 1);
               float wy = (float)(((i >> 1 & 1) << 1) - 1);
               float wz = (float)(((i >> 2 & 1) << 1) - 1);
               wx *= 0.1F;
               wy *= 0.1F;
               wz *= 0.1F;
               float dist;
               RaycastResult hit = renderer.game.world.check_raycast(
                       new Vector(px + wx, py + wy, pz + wz),
                       new Vector(px - lookx + wx + wz, py - lookz + wy, pz - looky + wz));
               if (hit != null && (dist = hit.end_point.distance(new Vector(px, py, pz))) < furthetst_distance) {
                  furthetst_distance = dist;
               }
            }

            GL11.glTranslatef(0.0F, 0.0F, -furthetst_distance);
         }
         // Rotate camera
         GL11.glRotatef(player.prev_pitch + (player.pitch - player.prev_pitch) * _dt, 1.0F, 0.0F, 0.0F);
         GL11.glRotatef(player.prev_yaw + (player.yaw - player.prev_yaw) * _dt + 180.0F, 0.0F, 1.0F, 0.0F);
         // Place the player
         GL11.glTranslatef(-px, -py, -pz);

         SomeKindOfCamera.update_camera();
         this.render_fog();
         GL11.glEnable(2912);
         game_renderer.method_357(dt);
         this.render_fog();
         FustrumCuller var25 = new FustrumCuller();
         this.game.game_renderer.method_366(var25);
         this.game.game_renderer.method_362(player);
         this.render_fog();
         GL11.glEnable(GL11.GL_FOG);
         GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.game.resource_manager.load_texture("/terrain.png"));
         Lighting.disable_lighting();
         game_renderer.do_occlusion_queries(player, 0);
         int var28;
         if (world.method_190(player.pos_x, player.pos_y, player.pos_z, 0.1F)) {
            var28 = (int)player.pos_x;
            int var30 = (int)player.pos_y;
            int var31 = (int)player.pos_z;
            BlockRenderer var32 = new BlockRenderer(world);

            for(int x = var28 - 1; x <= var28 + 1; ++x) {
               for(int y = var30 - 1; y <= var30 + 1; ++y) {
                  for(int z = var31 - 1; z <= var31 + 1; ++z) {
                     int block_id;
                     if ((block_id = world.block_at(x, y, z)) > 0) {
                        var32.render_block(Block.blocks[block_id], x, y, z);
                     }
                  }
               }
            }
         }

         Lighting.enable_lighting();
         game_renderer.render_entities(this.player_vec(dt), var25, dt);
         particle_manager.pre_render_particles(dt);
         Lighting.disable_lighting();
         this.render_fog();
         particle_manager.render_particles(player, dt);
         game_renderer.render_grass();
         if (this.game.looking_at != null && player.on_water_block()) {
            GL11.glDisable(3008);
            game_renderer.method_363(this.game.looking_at, 0, player.inventory.selected_item());
            game_renderer.method_364(this.game.looking_at, 0);
            GL11.glEnable(3008);
         }

         GL11.glBlendFunc(770, 771);
         this.render_fog();
         game_renderer.render_surrounding_water();
         GL11.glEnable(GL11.GL_BLEND);
         GL11.glDisable(GL11.GL_CULL_FACE);
         GL11.glColorMask(false, false, false, false);
            var28 = game_renderer.do_occlusion_queries(player, 1);

            GL11.glColorMask(true, true, true, true);
            if (this.game.options.anaglyph_3d) {
               if (render_pass == 0) {

                  GL11.glColorMask(false, true, true, false);
               } else {

                  GL11.glColorMask(true, false, false, false);
               }
            }

            if (var28 > 0) {
               game_renderer.maybe_render_ground();
            }


         GL11.glDepthMask(true);
         GL11.glEnable(GL11.GL_CULL_FACE);
         GL11.glDisable(GL11.GL_BLEND);
         if (this.game.looking_at != null && !player.on_water_block()) {
            GL11.glDisable(GL11.GL_ALPHA_TEST);
            game_renderer.method_363(this.game.looking_at, 0, player.inventory.selected_item());
            game_renderer.method_364(this.game.looking_at, 0);
            GL11.glEnable(GL11.GL_ALPHA_TEST);
         }



         GL11.glDisable(GL11.GL_FOG);

         GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);

         GL11.glLoadIdentity();
         if (this.game.options.anaglyph_3d) {
            GL11.glTranslatef((float)((render_pass << 1) - 1) * 0.1F, 0.0F, 0.0F);
         }

         // Render the view model
         GL11.glPushMatrix();
            this.hurt_camera(dt);
            if (this.game.options.view_bobbing) {
               this.head_bob(dt);
            }

            if (!this.game.options.third_person_mode) {
               this.view_model_renderer.render_view_model(dt);
            }
         GL11.glPopMatrix();


         if (!this.game.options.third_person_mode) {
            this.view_model_renderer.method_402(dt);
            this.hurt_camera(dt);
         }

         if (this.game.options.view_bobbing) {
            this.head_bob(dt);
         }

         if (!this.game.options.anaglyph_3d) {
            return;
         }
      }


      GL11.glColorMask(true, true, true, false);
   }

   // $FF: renamed from: entities () void
   public final void setup_ui_view() {
      WidthHeight wh;
      int width = (wh = new WidthHeight(this.game.width, this.game.height)).get_width();
      int height = wh.get_height();

      GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);

      GL11.glMatrixMode(GL11.GL_PROJECTION);

      GL11.glLoadIdentity();

      GL11.glOrtho(0.0D, (double)width, (double)height, 0.0D, 1000.0D, 3000.0D);

      GL11.glMatrixMode(GL11.GL_MODELVIEW);

      GL11.glLoadIdentity();

      GL11.glTranslatef(0.0F, 0.0F, -2000.0F);
   }

   // $FF: renamed from: particles (float) void
   private void configure_fog_and_render_sky(float var1) {
      World world = this.game.world;
      LocalPlayer player = this.game.player;
      float inv_rd = 1.0F / (float)(4 - this.game.options.render_distance);
      float fog_strength = 1.0F - (float)Math.pow((double)inv_rd, 0.25D);
      Vector var5 = world.sky_color(var1);
      float var6 = var5.x;
      float var7 = var5.y;
      float var13 = var5.z;
      Vector var8 = world.fog_color(var1);
      this.fog_b = var8.x;
      this.fog_g = var8.y;
      this.fog_r = var8.z;
      this.fog_b += (var6 - this.fog_b) * fog_strength;
      this.fog_g += (var7 - this.fog_g) * fog_strength;
      this.fog_r += (var13 - this.fog_r) * fog_strength;
      Block var9;
      if ((var9 = Block.blocks[world.block_at((int)player.pos_x, (int)(player.pos_y + 0.12F), (int)player.pos_z)]) != null && var9.material != Material.field_608) {
         Material var10;
         if ((var10 = var9.material) == Material.water_block) {
            this.fog_b = 0.02F;
            this.fog_g = 0.02F;
            this.fog_r = 0.2F;
         } else if (var10 == Material.lava_block) {
            this.fog_b = 0.6F;
            this.fog_g = 0.1F;
            this.fog_r = 0.0F;
         }
      }

      float var11 = this.prev_light + (this.light - this.prev_light) * var1;
      this.fog_b *= var11;
      this.fog_g *= var11;
      this.fog_r *= var11;
      if (this.game.options.anaglyph_3d) {
         var1 = (this.fog_b * 30.0F + this.fog_g * 59.0F + this.fog_r * 11.0F) / 100.0F;
         var11 = (this.fog_b * 30.0F + this.fog_g * 70.0F) / 100.0F;
         float var12 = (this.fog_b * 30.0F + this.fog_r * 70.0F) / 100.0F;
         this.fog_b = var1;
         this.fog_g = var11;
         this.fog_r = var12;
      }

      GL11.glClearColor(this.fog_b, this.fog_g, this.fog_r, 0.0F);
   }

   // $FF: renamed from: projectiles () void
   private void render_fog() {
      World world = this.game.world;
      LocalPlayer player = this.game.player;
      float var3 = 1.0F;
      this.ohter_float_buffer.clear();
      this.ohter_float_buffer.put(this.fog_r).put(this.fog_g).put(this.fog_b).put(1.0F);
      this.ohter_float_buffer.flip();
      GL11.glFog(GL11.GL_FOG_COLOR, this.ohter_float_buffer);
      GL11.glNormal3f(0.0F, -1.0F, 0.0F);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      Block block = Block.blocks[world.block_at((int)player.pos_x, (int)(player.pos_y + 0.12F), (int)player.pos_z)];
      if (block != null && block.material.method_343()) {
         Material mat = block.material;
         GL11.glFogi(GL11.GL_FOG_MODE, GL11.GL_EXP);
         if (mat == Material.water_block) {
            GL11.glFogf(GL11.GL_FOG_DENSITY, 0.1F);
         } else if (mat == Material.lava_block) {
            GL11.glFogf(GL11.GL_FOG_DENSITY, 2.0F);
         }
      } else {
         GL11.glFogi(GL11.GL_FOG_MODE, GL11.GL_LINEAR);
         GL11.glFogf(GL11.GL_FOG_START, this.rendering_block_distance / 4.0F);
         GL11.glFogf(GL11.GL_FOG_END, this.rendering_block_distance);
      }

      GL11.glEnable(GL11.GL_COLOR_MATERIAL);
      GL11.glColorMaterial(GL11.GL_FRONT, GL11.GL_AMBIENT);
   }
}
