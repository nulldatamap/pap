package net.minecraft.client.rendering.c;

import net.minecraft.game.world.block.Block;

// $FF: renamed from: net.minecraft.client.game.entities.particles
public final class class_174 extends class_173 {
   // $FF: renamed from: player float[]
   private float[] field_832 = new float[256];
   // $FF: renamed from: particles float[]
   private float[] field_833 = new float[256];
   // $FF: renamed from: player float[]
   private float[] field_834 = new float[256];
   // $FF: renamed from: h float[]
   private float[] field_835 = new float[256];
   // $FF: renamed from: i int
   private int field_836 = 0;

   public class_174() {
      super(Block.water.maybeDropCount);
   }

   // $FF: renamed from: game () void
   public final void method_487() {
      ++this.field_836;

      int var1;
      int var2;
      float var3;
      int var4;
      int var5;
      int var6;
      for(var1 = 0; var1 < 16; ++var1) {
         for(var2 = 0; var2 < 16; ++var2) {
            var3 = 0.0F;

            for(var4 = var1 - 1; var4 <= var1 + 1; ++var4) {
               var5 = var4 & 15;
               var6 = var2 & 15;
               var3 += this.field_832[var5 + (var6 << 4)];
            }

            this.field_833[var1 + (var2 << 4)] = var3 / 3.3F + this.field_834[var1 + (var2 << 4)] * 0.8F;
         }
      }

      for(var1 = 0; var1 < 16; ++var1) {
         for(var2 = 0; var2 < 16; ++var2) {
            this.field_834[var1 + (var2 << 4)] += this.field_835[var1 + (var2 << 4)] * 0.05F;
            if (this.field_834[var1 + (var2 << 4)] < 0.0F) {
               this.field_834[var1 + (var2 << 4)] = 0.0F;
            }

            this.field_835[var1 + (var2 << 4)] -= 0.1F;
            if (Math.random() < 0.05D) {
               this.field_835[var1 + (var2 << 4)] = 0.5F;
            }
         }
      }

      float[] var8 = this.field_833;
      this.field_833 = this.field_832;
      this.field_832 = var8;

      for(var2 = 0; var2 < 256; ++var2) {
         if ((var3 = this.field_832[var2]) > 1.0F) {
            var3 = 1.0F;
         }

         if (var3 < 0.0F) {
            var3 = 0.0F;
         }

         float var9 = var3 * var3;
         var5 = (int)(32.0F + var9 * 32.0F);
         var6 = (int)(50.0F + var9 * 64.0F);
         var1 = 255;
         int var10 = (int)(146.0F + var9 * 50.0F);
         if (this.field_830) {
            var1 = (var5 * 30 + var6 * 59 + 2805) / 100;
            var4 = (var5 * 30 + var6 * 70) / 100;
            int var7 = (var5 * 30 + 17850) / 100;
            var5 = var1;
            var6 = var4;
            var1 = var7;
         }

         this.field_828[var2 << 2] = (byte)var5;
         this.field_828[(var2 << 2) + 1] = (byte)var6;
         this.field_828[(var2 << 2) + 2] = (byte)var1;
         this.field_828[(var2 << 2) + 3] = (byte)var10;
      }

   }
}
