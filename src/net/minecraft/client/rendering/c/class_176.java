package net.minecraft.client.rendering.c;

import net.minecraft.game.world.block.Block;

// $FF: renamed from: net.minecraft.client.game.entities.player
public final class class_176 extends class_173 {
   // $FF: renamed from: player float[]
   private float[] field_841 = new float[320];
   // $FF: renamed from: particles float[]
   private float[] field_842 = new float[320];

   public class_176(int var1) {
      super(Block.fire.maybeDropCount + (var1 << 4));
   }

   // $FF: renamed from: game () void
   public final void method_487() {
      int var1;
      int var2;
      int var3;
      int var5;
      int var6;
      for(var1 = 0; var1 < 16; ++var1) {
         for(var2 = 0; var2 < 20; ++var2) {
            var3 = 18;
            float var4 = this.field_841[var1 + ((var2 + 1) % 20 << 4)] * 18.0F;

            for(var5 = var1 - 1; var5 <= var1 + 1; ++var5) {
               for(var6 = var2; var6 <= var2 + 1; ++var6) {
                  if (var5 >= 0 && var6 >= 0 && var5 < 16 && var6 < 20) {
                     var4 += this.field_841[var5 + (var6 << 4)];
                  }

                  ++var3;
               }
            }

            this.field_842[var1 + (var2 << 4)] = var4 / ((float)var3 * 1.06F);
            if (var2 >= 19) {
               this.field_842[var1 + (var2 << 4)] = (float)(Math.random() * Math.random() * Math.random() * 4.0D + Math.random() * 0.10000000149011612D + 0.20000000298023224D);
            }
         }
      }

      float[] var9 = this.field_842;
      this.field_842 = this.field_841;
      this.field_841 = var9;

      for(var2 = 0; var2 < 256; ++var2) {
         float var10;
         if ((var10 = this.field_841[var2] * 1.8F) > 1.0F) {
            var10 = 1.0F;
         }

         if (var10 < 0.0F) {
            var10 = 0.0F;
         }

         var5 = (int)(var10 * 155.0F + 100.0F);
         var6 = (int)(var10 * var10 * 255.0F);
         int var7 = (int)(var10 * var10 * var10 * var10 * var10 * var10 * var10 * var10 * var10 * var10 * 255.0F);
         short var8 = 255;
         if (var10 < 0.5F) {
            var8 = 0;
         }

         if (this.field_830) {
            var1 = (var5 * 30 + var6 * 59 + var7 * 11) / 100;
            var3 = (var5 * 30 + var6 * 70) / 100;
            int var11 = (var5 * 30 + var7 * 70) / 100;
            var5 = var1;
            var6 = var3;
            var7 = var11;
         }

         this.field_828[var2 << 2] = (byte)var5;
         this.field_828[(var2 << 2) + 1] = (byte)var6;
         this.field_828[(var2 << 2) + 2] = (byte)var7;
         this.field_828[(var2 << 2) + 3] = (byte)var8;
      }

   }
}
