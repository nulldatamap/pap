package net.minecraft.client.rendering.c;

import a.MathHelper;
import java.io.IOException;
import javax.imageio.ImageIO;
import net.minecraft.game.world.block.Block;

// $FF: renamed from: net.minecraft.client.game.entities.entities
public class class_177 extends class_173 {
   // $FF: renamed from: player int
   private int field_843 = 0;
   // $FF: renamed from: particles int[]
   private int[] field_844 = new int[1024];
   // $FF: renamed from: player int[]
   private int[] field_845 = new int[1024];
   // $FF: renamed from: h int
   private int field_846;

   public class_177(int var1) {
      super(Block.field_922.maybeDropCount + var1);
      this.field_846 = (var1 << 1) - 1;
      this.field_843 = 2;

      try {
         ImageIO.read(class_177.class.getResource("/misc/gear.png")).getRGB(0, 0, 32, 32, this.field_844, 0, 32);
         ImageIO.read(class_177.class.getResource("/misc/gearmiddle.png")).getRGB(0, 0, 16, 16, this.field_845, 0, 16);
      } catch (IOException var2) {
         var2.printStackTrace();
      }
   }

   // $FF: renamed from: game () void
   public final void method_487() {
      this.field_843 = this.field_843 + this.field_846 & 63;
      float var1 = MathHelper.sin((float)this.field_843 / 64.0F * 3.1415927F * 2.0F);
      float var2 = MathHelper.cos((float)this.field_843 / 64.0F * 3.1415927F * 2.0F);

      for(int var3 = 0; var3 < 16; ++var3) {
         for(int var4 = 0; var4 < 16; ++var4) {
            float var5 = ((float)var3 / 15.0F - 0.5F) * 31.0F;
            float var6 = ((float)var4 / 15.0F - 0.5F) * 31.0F;
            float var7 = var2 * var5 - var1 * var6;
            var5 = var2 * var6 + var1 * var5;
            int var11 = (int)(var7 + 16.0F);
            int var10 = (int)(var5 + 16.0F);
            int var12 = 0;
            if (var11 >= 0 && var10 >= 0 && var11 < 32 && var10 < 32) {
               var12 = this.field_844[var11 + (var10 << 5)];
               if ((var10 = this.field_845[var3 + (var4 << 4)]) >>> 24 > 128) {
                  var12 = var10;
               }
            }

            var10 = var12 >> 16 & 255;
            var11 = var12 >> 8 & 255;
            int var8 = var12 & 255;
            var12 = var12 >>> 24 > 128 ? 255 : 0;
            int var9 = var3 + (var4 << 4);
            this.field_828[var9 << 2] = (byte)var10;
            this.field_828[(var9 << 2) + 1] = (byte)var11;
            this.field_828[(var9 << 2) + 2] = (byte)var8;
            this.field_828[(var9 << 2) + 3] = (byte)var12;
         }
      }

   }
}
