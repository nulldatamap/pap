package net.minecraft.client.rendering;

import a.MathHelper;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.items.ItemStack;
import net.minecraft.client.Game;
import net.minecraft.client.Lighting;
import net.minecraft.client.rendering.models.PlayerModelRenderer;
import net.minecraft.client.rendering.models.ModelManager;
import net.minecraft.client.player.LocalPlayer;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.game.player
public final class ViewModelRenderer {
   // $FF: renamed from: game net.minecraft.client.projectiles
   private Game game;
   // $FF: renamed from: animals net.minecraft.game.animals.k
   private ItemStack item_stack = null;
   // $FF: renamed from: entities float
   private float field_712 = 0.0F;
   // $FF: renamed from: projectiles float
   private float field_713 = 0.0F;
   // $FF: renamed from: player int
   private int field_714 = 0;
   // $FF: renamed from: particles boolean
   private boolean field_715 = false;
   // $FF: renamed from: player net.minecraft.client.game.particles
   private BlockRenderer block_renderer = new BlockRenderer();

   public ViewModelRenderer(Game game) {
      this.game = game;
   }

   // $FF: renamed from: game (float) void
   public final void render_view_model(float dt) {
      float var2 = this.field_713 + (this.field_712 - this.field_713) * dt;
      LocalPlayer var3 = this.game.player;
      GL11.glPushMatrix();
      GL11.glRotatef(var3.prev_pitch + (var3.pitch - var3.prev_pitch) * dt, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(var3.prev_yaw + (var3.yaw - var3.prev_yaw) * dt, 0.0F, 1.0F, 0.0F);
      Lighting.enable_lighting();
      GL11.glPopMatrix();
      float var9;
      GL11.glColor4f(var9 = this.game.world.light_at((int)var3.pos_x, (int)var3.pos_y, (int)var3.pos_z), var9, var9, 1.0F);
      float var4;
      if (this.item_stack != null) {
         GL11.glPushMatrix();
         if (this.field_715) {
            var4 = MathHelper.sin((var9 = ((float)this.field_714 + dt) / 8.0F) * 3.1415927F);
            GL11.glTranslatef(-MathHelper.sin(MathHelper.sqrt(var9) * 3.1415927F) * 0.4F, MathHelper.sin(MathHelper.sqrt(var9) * 3.1415927F * 2.0F) * 0.2F, -var4 * 0.2F);
         }

         GL11.glTranslatef(0.56F, -0.52F - (1.0F - var2) * 0.6F, -0.71999997F);
         GL11.glRotatef(45.0F, 0.0F, 1.0F, 0.0F);
         GL11.glEnable(2977);
         float var5;
         if (this.field_715) {
            var4 = MathHelper.sin((var9 = ((float)this.field_714 + dt) / 8.0F) * var9 * 3.1415927F);
            var5 = MathHelper.sin(MathHelper.sqrt(var9) * 3.1415927F);
            GL11.glRotatef(-var4 * 20.0F, 0.0F, 1.0F, 0.0F);
            GL11.glRotatef(-var5 * 20.0F, 0.0F, 0.0F, 1.0F);
            GL11.glRotatef(-var5 * 80.0F, 1.0F, 0.0F, 0.0F);
         }

         GL11.glScalef(0.4F, 0.4F, 0.4F);
         if (this.item_stack.id < 256 && Block.blocks[this.item_stack.id].method_492() == 0) {
            GL11.glBindTexture(3553, this.game.resource_manager.load_texture("/terrain.png"));
            this.block_renderer.fully_render(Block.blocks[this.item_stack.id]);
         } else {
            if (this.item_stack.id < 256) {
               GL11.glBindTexture(3553, this.game.resource_manager.load_texture("/terrain.png"));
            } else {
               GL11.glBindTexture(3553, this.game.resource_manager.load_texture("/gui/items.png"));
            }

            GeometryDrawer var11 = GeometryDrawer.singleton;
            ItemStack var10 = this.item_stack;
            var5 = (float)(this.item_stack.item().get_item_id() % 16 << 4) / 256.0F;
            var10 = this.item_stack;
            dt = (float)((this.item_stack.item().get_item_id() % 16 << 4) + 16) / 256.0F;
            var10 = this.item_stack;
            var2 = (float)(this.item_stack.item().get_item_id() / 16 << 4) / 256.0F;
            var10 = this.item_stack;
            var9 = (float)((this.item_stack.item().get_item_id() / 16 << 4) + 16) / 256.0F;
            GL11.glEnable(2977);
            GL11.glTranslatef(0.0F, -0.3F, 0.0F);
            GL11.glScalef(1.5F, 1.5F, 1.5F);
            GL11.glRotatef(50.0F, 0.0F, 1.0F, 0.0F);
            GL11.glRotatef(335.0F, 0.0F, 0.0F, 1.0F);
            GL11.glTranslatef(-0.9375F, -0.0625F, 0.0F);
            GeometryDrawer.set_normal(0.0F, 0.0F, 1.0F);
            var11.quads_mode();
            var11.add_vertex(0.0F, 0.0F, 0.0F, dt, var9);
            var11.add_vertex(1.0F, 0.0F, 0.0F, var5, var9);
            var11.add_vertex(1.0F, 1.0F, 0.0F, var5, var2);
            var11.add_vertex(0.0F, 1.0F, 0.0F, dt, var2);
            var11.draw();
            GeometryDrawer.set_normal(0.0F, 0.0F, -1.0F);
            var11.quads_mode();
            var11.add_vertex(0.0F, 1.0F, -0.0625F, dt, var2);
            var11.add_vertex(1.0F, 1.0F, -0.0625F, var5, var2);
            var11.add_vertex(1.0F, 0.0F, -0.0625F, var5, var9);
            var11.add_vertex(0.0F, 0.0F, -0.0625F, dt, var9);
            var11.draw();
            GeometryDrawer.set_normal(-1.0F, 0.0F, 0.0F);
            var11.quads_mode();

            int var6;
            float var7;
            float var8;
            for(var6 = 0; var6 < 16; ++var6) {
               var7 = (float)var6 / 16.0F;
               var8 = dt + (var5 - dt) * var7 - 0.001953125F;
               var7 *= 1.0F;
               var11.add_vertex(var7, 0.0F, -0.0625F, var8, var9);
               var11.add_vertex(var7, 0.0F, 0.0F, var8, var9);
               var11.add_vertex(var7, 1.0F, 0.0F, var8, var2);
               var11.add_vertex(var7, 1.0F, -0.0625F, var8, var2);
            }

            var11.draw();
            GeometryDrawer.set_normal(1.0F, 0.0F, 0.0F);
            var11.quads_mode();

            for(var6 = 0; var6 < 16; ++var6) {
               var7 = (float)var6 / 16.0F;
               var8 = dt + (var5 - dt) * var7 - 0.001953125F;
               var7 = var7 * 1.0F + 0.0625F;
               var11.add_vertex(var7, 1.0F, -0.0625F, var8, var2);
               var11.add_vertex(var7, 1.0F, 0.0F, var8, var2);
               var11.add_vertex(var7, 0.0F, 0.0F, var8, var9);
               var11.add_vertex(var7, 0.0F, -0.0625F, var8, var9);
            }

            var11.draw();
            GeometryDrawer.set_normal(0.0F, 1.0F, 0.0F);
            var11.quads_mode();

            for(var6 = 0; var6 < 16; ++var6) {
               var7 = (float)var6 / 16.0F;
               var8 = var9 + (var2 - var9) * var7 - 0.001953125F;
               var7 = var7 * 1.0F + 0.0625F;
               var11.add_vertex(0.0F, var7, 0.0F, dt, var8);
               var11.add_vertex(1.0F, var7, 0.0F, var5, var8);
               var11.add_vertex(1.0F, var7, -0.0625F, var5, var8);
               var11.add_vertex(0.0F, var7, -0.0625F, dt, var8);
            }

            var11.draw();
            GeometryDrawer.set_normal(0.0F, -1.0F, 0.0F);
            var11.quads_mode();

            for(var6 = 0; var6 < 16; ++var6) {
               var7 = (float)var6 / 16.0F;
               var8 = var9 + (var2 - var9) * var7 - 0.001953125F;
               var7 *= 1.0F;
               var11.add_vertex(1.0F, var7, 0.0F, var5, var8);
               var11.add_vertex(0.0F, var7, 0.0F, dt, var8);
               var11.add_vertex(0.0F, var7, -0.0625F, dt, var8);
               var11.add_vertex(1.0F, var7, -0.0625F, var5, var8);
            }

            var11.draw();
            GL11.glDisable(2977);
         }

         GL11.glPopMatrix();
      } else {
         GL11.glPushMatrix();
         if (this.field_715) {
            var4 = MathHelper.sin((var9 = ((float)this.field_714 + dt) / 8.0F) * 3.1415927F);
            GL11.glTranslatef(-MathHelper.sin(MathHelper.sqrt(var9) * 3.1415927F) * 0.3F, MathHelper.sin(MathHelper.sqrt(var9) * 3.1415927F * 2.0F) * 0.4F, -var4 * 0.4F);
         }

         GL11.glTranslatef(0.64000005F, -0.6F - (1.0F - var2) * 0.6F, -0.71999997F);
         GL11.glRotatef(45.0F, 0.0F, 1.0F, 0.0F);
         GL11.glEnable(2977);
         if (this.field_715) {
            var4 = MathHelper.sin((var9 = ((float)this.field_714 + dt) / 8.0F) * var9 * 3.1415927F);
            GL11.glRotatef(MathHelper.sin(MathHelper.sqrt(var9) * 3.1415927F) * 70.0F, 0.0F, 1.0F, 0.0F);
            GL11.glRotatef(-var4 * 20.0F, 0.0F, 0.0F, 1.0F);
         }

         GL11.glBindTexture(3553, this.game.resource_manager.method_394(this.game.player.texture_url, this.game.player.maybe_texture_name()));
         GL11.glTranslatef(-0.2F, -0.3F, 0.1F);
         GL11.glRotatef(120.0F, 0.0F, 0.0F, 1.0F);
         GL11.glRotatef(200.0F, 1.0F, 0.0F, 0.0F);
         GL11.glRotatef(-135.0F, 0.0F, 1.0F, 0.0F);
         GL11.glScalef(0.0625F, 0.0625F, 0.0625F);
         GL11.glTranslatef(6.0F, 0.0F, 0.0F);
         ((PlayerModelRenderer) ModelManager.singleton.model_of(this.game.player)).method_110();
         GL11.glPopMatrix();
      }

      GL11.glDisable(2977);
      Lighting.disable_lighting();
   }

   // $FF: renamed from: animals (float) void
   public final void method_402(float var1) {
      GL11.glDisable(3008);
      int var2;
      GeometryDrawer var3;
      float var7;
      float var9;
      if (this.game.player.burn_time > 0) {
         var2 = this.game.resource_manager.load_texture("/terrain.png");
         GL11.glBindTexture(3553, var2);
         var3 = GeometryDrawer.singleton;
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 0.9F);
         GL11.glEnable(3042);
         GL11.glBlendFunc(770, 771);

         for(var2 = 0; var2 < 2; ++var2) {
            GL11.glPushMatrix();
            int var4;
            int var5 = ((var4 = Block.fire.maybeDropCount + (var2 << 4)) & 15) << 4;
            var4 &= 240;
            float var6 = (float)var5 / 256.0F;
            float var10 = ((float)var5 + 15.99F) / 256.0F;
            var7 = (float)var4 / 256.0F;
            var9 = ((float)var4 + 15.99F) / 256.0F;
            GL11.glTranslatef((float)(-((var2 << 1) - 1)) * 0.24F, -0.3F, 0.0F);
            GL11.glRotatef((float)((var2 << 1) - 1) * 10.0F, 0.0F, 1.0F, 0.0F);
            var3.quads_mode();
            var3.add_vertex(-0.5F, -0.5F, -0.5F, var10, var9);
            var3.add_vertex(0.5F, -0.5F, -0.5F, var6, var9);
            var3.add_vertex(0.5F, 0.5F, -0.5F, var6, var7);
            var3.add_vertex(-0.5F, 0.5F, -0.5F, var10, var7);
            var3.draw();
            GL11.glPopMatrix();
         }

         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GL11.glDisable(3042);
      }

      if (this.game.player.on_water_block()) {
         var2 = this.game.resource_manager.load_texture("/water.png");
         GL11.glBindTexture(3553, var2);
         var3 = GeometryDrawer.singleton;
         float var8;
         GL11.glColor4f(var8 = this.game.player.light_level(var1), var8, var8, 0.5F);
         GL11.glEnable(3042);
         GL11.glBlendFunc(770, 771);
         GL11.glPushMatrix();
         var7 = -this.game.player.yaw / 64.0F;
         var9 = this.game.player.pitch / 64.0F;
         var3.quads_mode();
         var3.add_vertex(-1.0F, -1.0F, -0.5F, var7 + 4.0F, var9 + 4.0F);
         var3.add_vertex(1.0F, -1.0F, -0.5F, var7 + 0.0F, var9 + 4.0F);
         var3.add_vertex(1.0F, 1.0F, -0.5F, var7 + 0.0F, var9 + 0.0F);
         var3.add_vertex(-1.0F, 1.0F, -0.5F, var7 + 4.0F, var9 + 0.0F);
         var3.draw();
         GL11.glPopMatrix();
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GL11.glDisable(3042);
      }

      GL11.glEnable(3008);
   }

   // $FF: renamed from: game () void
   public final void update() {
      this.field_713 = this.field_712;
      if (this.field_715) {
         ++this.field_714;
         if (this.field_714 == 8) {
            this.field_714 = 0;
            this.field_715 = false;
         }
      }

      LocalPlayer var1 = this.game.player;
      float var2;
      ItemStack var3;
      if ((var2 = ((var3 = this.game.player.inventory.selected_item()) == this.item_stack ? 1.0F : 0.0F) - this.field_712) < -0.4F) {
         var2 = -0.4F;
      }

      if (var2 > 0.4F) {
         var2 = 0.4F;
      }

      this.field_712 += var2;
      if (this.field_712 < 0.1F) {
         this.item_stack = var3;
      }

   }

   // $FF: renamed from: animals () void
   public final void method_404() {
      this.field_712 = 0.0F;
   }

   // $FF: renamed from: entities () void
   public final void method_405() {
      this.field_714 = -1;
      this.field_715 = true;
   }

   // $FF: renamed from: projectiles () void
   public final void method_406() {
      this.field_712 = 0.0F;
   }
}
