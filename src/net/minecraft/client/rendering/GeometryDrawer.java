package net.minecraft.client.rendering;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBVertexBufferObject;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.ARBBufferObject;

// $FF: renamed from: net.minecraft.client.game.projectiles
public final class GeometryDrawer {
   static final int BUFFER_X_OFFSET = 0;
   static final int BUFFER_Y_OFFSET = 1;
   static final int BUFFER_Z_OFFSET = 2;
   static final int BUFFER_U_OFFSET = 3;
   static final int BUFFER_V_OFFSET = 4;
   static final int BUFFER_RBGA_OFFSET = 5;
   static final int BUFFER_ELEMENT_SIZE = 8;



   // $FF: renamed from: animals boolean
   private static boolean field_674 = false;
   // $FF: renamed from: entities boolean
   private static boolean field_675 = false;
   // $FF: renamed from: projectiles java.nio.ByteBuffer
   private ByteBuffer byte_buffer = BufferUtils.createByteBuffer(0x80_0000);
   // $FF: renamed from: player int[]
   private int[] geometry_data = new int[0x20_0000];
   // $FF: renamed from: particles int
   private int draw_target_count = 0;
   // $FF: renamed from: player float
   private float tex_u;
   // $FF: renamed from: h float
   private float tex_v;
   // $FF: renamed from: i int
   private int color;
   // $FF: renamed from: j boolean
   private boolean has_color = false;
   // $FF: renamed from: k boolean
   private boolean has_texture = false;
   // $FF: renamed from: l int
   private int main_buffer_size = 0;
   // $FF: renamed from: m int
   private int draw_target_count_2 = 0;
   // $FF: renamed from: n boolean
   private boolean is_colorless = false;
   // $FF: renamed from: o int
   private int draw_mode;
   // $FF: renamed from: game net.minecraft.client.game.projectiles
   public static GeometryDrawer singleton = new GeometryDrawer();
   // $FF: renamed from: p boolean
   private boolean is_tesselating = false;
   // $FF: renamed from: q boolean
   private boolean is_buffer_bound = false;
   // $FF: renamed from: r java.nio.IntBuffer
   private IntBuffer int_buffer;
   // $FF: renamed from: s int
   private int current_index = 0;
   // $FF: renamed from: t int
   private int int_buffer_size = 10;

   private GeometryDrawer() {
      this.is_buffer_bound = false;
      if (this.is_buffer_bound) {
         this.int_buffer = BufferUtils.createIntBuffer(this.int_buffer_size);
         ARBVertexBufferObject.glGenBuffersARB(this.int_buffer);
      }

   }

   // $FF: renamed from: game () void
   public final void draw() {
      if (!this.is_tesselating) {
         throw new IllegalStateException("Not tesselating!");
      } else {
         this.is_tesselating = false;
         if (this.draw_target_count > 0) {
            IntBuffer intbuf = this.byte_buffer.asIntBuffer();
            FloatBuffer floatbuf = this.byte_buffer.asFloatBuffer();
            intbuf.clear();
            intbuf.put(this.geometry_data, 0, this.main_buffer_size);
            this.byte_buffer.position(0);
            this.byte_buffer.limit(this.main_buffer_size << 2);
            if (this.is_buffer_bound) {
               this.current_index = (this.current_index + 1) % this.int_buffer_size;
               ARBVertexBufferObject.glBindBufferARB(GL15.GL_ARRAY_BUFFER, this.int_buffer.get(this.current_index));
               ARBVertexBufferObject.glBufferDataARB(GL15.GL_ARRAY_BUFFER, this.byte_buffer, ARBBufferObject.GL_STREAM_DRAW_ARB);
            }

            if (this.has_texture) {
               if (this.is_buffer_bound) {
                  GL11.glTexCoordPointer(2, GL11.GL_FLOAT, 32, 12L);
               } else {
                  floatbuf.position(BUFFER_U_OFFSET);
                  GL11.glTexCoordPointer(2, 32, floatbuf);
               }

               GL11.glEnableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
            }

            if (this.has_color) {
               if (this.is_buffer_bound) {
                  GL11.glColorPointer(4, GL11.GL_UNSIGNED_BYTE, 32, 20L);
               } else {
                  this.byte_buffer.position(20);
                  GL11.glColorPointer(4, true, 32, this.byte_buffer);
               }

               GL11.glEnableClientState(GL11.GL_COLOR_ARRAY);
            }

            if (this.is_buffer_bound) {
               GL11.glVertexPointer(3, GL11.GL_FLOAT, 32, 0L);
            } else {
               floatbuf.position(0);
               GL11.glVertexPointer(3, 32, floatbuf);
            }

            GL11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
            GL11.glDrawArrays(this.draw_mode, 0, this.draw_target_count);
            GL11.glDisableClientState(GL11.GL_VERTEX_ARRAY);
            if (this.has_texture) {
               GL11.glDisableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
            }

            if (this.has_color) {
               GL11.glDisableClientState(GL11.GL_COLOR_ARRAY);
            }
         }

         this.reset();
      }
   }

   // $FF: renamed from: projectiles () void
   private void reset() {
      this.draw_target_count = 0;
      this.byte_buffer.clear();
      this.main_buffer_size = 0;
      this.draw_target_count_2 = 0;
   }

   // $FF: renamed from: animals () void
   public final void quads_mode() {
      this.set_draw_mode(GL11.GL_QUADS);
   }

   // $FF: renamed from: game (int) void
   public final void set_draw_mode(int var1) {
      if (this.is_tesselating) {
         throw new IllegalStateException("Already tesselating!");
      } else {
         this.is_tesselating = true;
         this.reset();
         this.draw_mode = var1;
         this.has_color = false;
         this.has_texture = false;
         this.is_colorless = false;
      }
   }

   // $FF: renamed from: game (float, float, float) void
   public final void set_color(float r, float g, float b) {
      this.set_color((int)(r * 255.0F), (int)(g * 255.0F), (int)(b * 255.0F));
   }

   // $FF: renamed from: game (float, float, float, float) void
   public final void set_color(float r, float g, float b, float a) {
      this.set_color((int)(r * 255.0F), (int)(g * 255.0F), (int)(b * 255.0F), (int)(a * 255.0F));
   }

   // $FF: renamed from: game (int, int, int) void
   private void set_color(int r, int g, int b) {
      this.set_color(r, g, b, 255);
   }

   // $FF: renamed from: game (int, int, int, int) void
   private void set_color(int r, int g, int b, int a) {
      if (!this.is_colorless) {
         if (r > 255) {
            r = 255;
         }

         if (g > 255) {
            g = 255;
         }

         if (b > 255) {
            b = 255;
         }

         if (a > 255) {
            a = 255;
         }

         if (r < 0) {
            r = 0;
         }

         if (g < 0) {
            g = 0;
         }

         if (b < 0) {
            b = 0;
         }

         if (a < 0) {
            a = 0;
         }

         this.has_color = true;
         this.color = a << 24 | b << 16 | g << 8 | r;
      }
   }

   // $FF: renamed from: game (float, float, float, float, float) void
   public final void add_vertex(float x, float y, float z, float u, float v) {
      this.has_texture = true;
      this.tex_u = u;
      this.tex_v = v;
      this.add_vertex(x, y, z);
   }

   // $FF: renamed from: animals (float, float, float) void
   public final void add_vertex(float x, float y, float z) {
      ++this.draw_target_count_2;
      if (this.has_texture) {
         this.geometry_data[this.main_buffer_size + BUFFER_U_OFFSET] = Float.floatToRawIntBits(this.tex_u);
         this.geometry_data[this.main_buffer_size + BUFFER_V_OFFSET] = Float.floatToRawIntBits(this.tex_v);
      }

      if (this.has_color) {
         this.geometry_data[this.main_buffer_size + BUFFER_RBGA_OFFSET] = this.color;
      }

      this.geometry_data[this.main_buffer_size + BUFFER_X_OFFSET] = Float.floatToRawIntBits(x);
      this.geometry_data[this.main_buffer_size + BUFFER_Y_OFFSET] = Float.floatToRawIntBits(y);
      this.geometry_data[this.main_buffer_size + BUFFER_Z_OFFSET] = Float.floatToRawIntBits(z);
      this.main_buffer_size += BUFFER_ELEMENT_SIZE;
      ++this.draw_target_count;
      if (this.draw_target_count % 4 == 0 && this.main_buffer_size >= 0x1F_FFE0) {
         this.draw();
      }

   }

   // $FF: renamed from: animals (int) void
   public final void set_color(int var1) {
      int var2 = var1 >> 16 & 255;
      int var3 = var1 >> 8 & 255;
      var1 &= 255;
      this.set_color(var2, var3, var1);
   }

   // $FF: renamed from: entities () void
   public final void colorless() {
      this.is_colorless = true;
   }

   // $FF: renamed from: entities (float, float, float) void
   public static void set_normal(float x, float y, float z) {
      GL11.glNormal3f(x, y, z);
   }
}
