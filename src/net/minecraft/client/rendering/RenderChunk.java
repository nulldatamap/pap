package net.minecraft.client.rendering;

import a.MathHelper;
import net.minecraft.game.world.World;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.entities.Mob;
import net.minecraft.game.util.AABB;
import net.minecraft.client.rendering.models.ItemDropModelRenderer;
import net.minecraft.client.rendering.b.ICuller;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.game.h
public final class RenderChunk {
   // $FF: renamed from: player net.minecraft.game.game.player
   private World world;
   // $FF: renamed from: h int
   private int display_list = -1;
   // $FF: renamed from: i net.minecraft.client.game.projectiles
   private static GeometryDrawer drawer;
   // $FF: renamed from: game int
   public static int field_727;
   // $FF: renamed from: j int
   private int x;
   // $FF: renamed from: k int
   private int y;
   // $FF: renamed from: l int
   private int z;
   // $FF: renamed from: m int
   private int width;
   // $FF: renamed from: n int
   private int height;
   // $FF: renamed from: o int
   private int depth;
   // $FF: renamed from: animals boolean
   public boolean maybe_is_visible = false;
   // $FF: renamed from: p boolean[]
   private boolean[] passes = new boolean[2];
   // $FF: renamed from: q int
   private int center_x;
   // $FF: renamed from: r int
   private int center_y;
   // $FF: renamed from: s int
   private int center_z;
   // $FF: renamed from: entities boolean
   public boolean field_739;
   // $FF: renamed from: t net.minecraft.game.projectiles.animals
   private AABB hitbox;
   // $FF: renamed from: u net.minecraft.client.game.particles
   private BlockRenderer block_renderer;
   // $FF: renamed from: projectiles boolean
   public boolean maybe_is_visible_2 = true;
   // $FF: renamed from: player boolean
   public boolean field_743;
   // $FF: renamed from: particles int
   public int occlusion_id;

   public RenderChunk(World w, int x, int y, int z, int size, int var6) {
      this.block_renderer = new BlockRenderer(w);
      this.world = w;
      this.x = x;
      this.y = y;
      this.z = z;
      this.width = this.height = this.depth = 16;
      this.center_x = x + this.width / 2;
      this.center_y = y + this.height / 2;
      this.center_z = z + this.depth / 2;
      MathHelper.sqrt((float)(this.width * this.width + this.height * this.height + this.depth * this.depth));
      this.hitbox = (new AABB((float)x, (float)y, (float)z, (float)(x + this.width), (float)(y + this.height), (float)(z + this.depth))).grow(2.0F, 2.0F, 2.0F);
      this.display_list = var6;
      this.reset_passes();
      GL11.glDisable(GL11.GL_TEXTURE_2D);
      GL11.glNewList(var6 + 2, GL11.GL_COMPILE);
      ItemDropModelRenderer.render_hitbox(this.hitbox);
      GL11.glEndList();
      GL11.glEnable(GL11.GL_TEXTURE_2D);
   }

   // $FF: renamed from: game () void
   public final void method_421() {
      if (this.field_739) {
         ++field_727;
         int var1 = this.x;
         int var2 = this.y;
         int var3 = this.z;
         int var4 = this.x + this.width;
         int var5 = this.y + this.height;
         int var6 = this.z + this.depth;

         int pass;
         for(pass = 0; pass < 2; ++pass) {
            this.passes[pass] = true;
         }

         for(pass = 0; pass < 2; ++pass) {
            boolean var8 = false;
            boolean var9 = false;
            drawer.quads_mode();
            GL11.glNewList(this.display_list + pass,GL11.GL_COMPILE);

            for(int i = var2; i < var5; ++i) {
               for(int j = var3; j < var6; ++j) {
                  int var12 = (i * this.world.depth + j) * this.world.width + var1;

                  for(int k = var1; k < var4; ++k) {
                     int var14;
                     if ((var14 = this.world.blocks[var12++] & 255) > 0) {
                        Block var15;
                        if ((var15 = Block.blocks[var14]).method_523() != pass) {
                           var8 = true;
                        } else {
                           var9 |= this.block_renderer.actually_render_block(var15, k, i, j);
                        }
                     }
                  }
               }
            }

            drawer.draw();
            GL11.glEndList();
            if (var9) {
               this.passes[pass] = false;
            }

            if (!var8) {
               break;
            }
         }

      }
   }

   // $FF: renamed from: game (net.minecraft.game.entities.player) float
   public final float distance(Mob var1) {
      float var2 = var1.pos_x - (float)this.center_x;
      float var3 = var1.pos_y - (float)this.center_y;
      float var4 = var1.pos_z - (float)this.center_z;
      return var2 * var2 + var3 * var3 + var4 * var4;
   }

   // $FF: renamed from: projectiles () void
   private void reset_passes() {
      for(int i = 0; i < 2; ++i) {
         this.passes[i] = true;
      }

   }

   // $FF: renamed from: animals () void
   public final void reset() {
      this.reset_passes();
      this.world = null;
   }

   // $FF: renamed from: game (int[], int, int) int
   public final int method_425(int[] var1, int var2, int var3) {
      if (!this.maybe_is_visible) {
         return var2;
      } else {
         if (!this.passes[var3]) {
            var1[var2++] = this.display_list + var3;
         }

         return var2;
      }
   }

   // $FF: renamed from: game (net.minecraft.client.game.animals.player) void
   public final void method_426(ICuller var1) {
      this.maybe_is_visible = var1.is_visible(this.hitbox);
   }

   // $FF: renamed from: entities () void
   public final void method_427() {
      GL11.glCallList(this.display_list + 2);
   }

   static {
      drawer = GeometryDrawer.singleton;
      field_727 = 0;
   }
}
