package net.minecraft.client.rendering;

import java.net.HttpURLConnection;
import java.net.URL;
import javax.imageio.ImageIO;

// $FF: renamed from: net.minecraft.client.game.j
final class ImageFetcher extends Thread {
   // $FF: renamed from: game java.lang.String
   // $FF: synthetic field
   private String url;
   // $FF: renamed from: animals net.minecraft.client.game.entities
   // $FF: synthetic field
   private KindofImage field_707;
   // $FF: renamed from: entities net.minecraft.client.game.i
   // $FF: synthetic field
   private FetchedImage field_708;

   ImageFetcher(FetchedImage var1, String var2, KindofImage var3) {
      super();
      this.field_708 = var1;
      this.url = var2;
      this.field_707 = var3;
   }

   public final void run() {
      HttpURLConnection var1 = null;

      try {
         (var1 = (HttpURLConnection)(new URL(this.url)).openConnection()).setDoInput(true);
         var1.setDoOutput(false);
         var1.connect();
         if (var1.getResponseCode() == 404) {
            return;
         }

         if (this.field_707 == null) {
            this.field_708.field_720 = ImageIO.read(var1.getInputStream());
         } else {
            this.field_708.field_720 = this.field_707.method_340(ImageIO.read(var1.getInputStream()));
         }
      } catch (Exception var5) {
         var5.printStackTrace();
         return;
      } finally {
         var1.disconnect();
      }

   }
}
