package net.minecraft.client.rendering;

import java.util.Comparator;
import net.minecraft.game.entities.player.Player;

// $FF: renamed from: net.minecraft.client.game.l
public final class RenderChunkDistanceComparator implements Comparator<RenderChunk> {
   // $FF: renamed from: game net.minecraft.game.entities.player.game
   private Player player;

   public RenderChunkDistanceComparator(Player var1) {
      this.player = var1;
   }

   // IMPROVISED

   public int compare(RenderChunk o1, RenderChunk o2) {
      float dist1 = o1.distance(player);
      float dist2 = o2.distance(player);
      if( dist1 < dist2 ) return -1;
      if( dist1 > dist2 ) return 1;
      return 0;
   }
}
