package net.minecraft.client.rendering.models;

import net.minecraft.client.models.Model;
import net.minecraft.game.entities.Mob;
import net.minecraft.game.entities.animals.Sheep;
import net.minecraft.game.entities.player.Player;

// $FF: renamed from: net.minecraft.client.game.game.k
public final class SheepModelRenderer extends MobModelRenderer {
   public SheepModelRenderer(Model var1, Model var2, float var3) {
      super(var1, 0.7F);
      this.method_103(var2);
   }

   protected final boolean method_105(Mob e, int i) {
      this.bind_texture("/mob/sheep_fur.png");
      return i == 0 && !((Sheep) e).sheared;
   }
}
