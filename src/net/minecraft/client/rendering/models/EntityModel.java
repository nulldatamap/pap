package net.minecraft.client.rendering.models;

import net.minecraft.game.entities.Entity;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.game.game.game
public final class EntityModel extends ModelRenderer {
   // $FF: renamed from: game (net.minecraft.game.entities.animals, float, float, float, float, float) void
   public final void render_model(Entity ent, float x, float y, float z, float yaw, float dt) {
      GL11.glPushMatrix();
      GL11.glTranslatef(x - ent.chunk_x, y - ent.chunk_y, z - ent.chunk_z);
      render_hitbox(ent.hitbox);
      GL11.glPopMatrix();
   }
}
