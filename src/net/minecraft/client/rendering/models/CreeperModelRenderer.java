package net.minecraft.client.rendering.models;

import a.MathHelper;
import net.minecraft.client.models.class_162;
import net.minecraft.game.entities.Mob;
import net.minecraft.game.entities.monsters.Creeper;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.game.game.entities
public final class CreeperModelRenderer extends MobModelRenderer {
   public CreeperModelRenderer() {
      super(new class_162(), 0.5F);
   }


   protected final void mob_specific_animation(Mob e, float f) {
      Creeper creeper = (Creeper) e;
      float fuse = creeper.fuse_procentage(f);
      f = 1.0F + MathHelper.sin(fuse * 100F) * fuse * 0.01F;
      if(fuse < 0.0F) {
         fuse = 0.0F;
      }
      if(fuse > 1.0F) {
         fuse = 1.0F;
      }
      fuse = ((fuse *= fuse) * fuse);
      float f1 = (1.0F + fuse * 0.4F) * f;
      fuse = ((1.0F + fuse * 0.1F) / f);
      GL11.glScalef(f1, fuse, f1);
   }

   protected final int get_color_modifier(Mob e, float light, float dt) {
      float f2 = ((Creeper) e).fuse_procentage(dt);
      if((int)(f2 * 10F) % 2 == 0) {
         return 0;
      }
      int c = ((int)(f2 * 0.2F * 255F));
      if(c < 0) {
         c = 0;
      }
      if(c > 255) {
         c = 255;
      }
      return c << 24 | 0xff0000 | 0xff00 | 0xff;
   }
}
