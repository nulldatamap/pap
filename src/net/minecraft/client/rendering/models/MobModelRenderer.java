package net.minecraft.client.rendering.models;

import a.MathHelper;
import net.minecraft.game.entities.Entity;
import net.minecraft.game.entities.Mob;
import net.minecraft.client.models.Model;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.game.game.j
public class MobModelRenderer extends ModelRenderer {
   // $FF: renamed from: projectiles net.minecraft.client.animals.l
   protected Model model;
   // $FF: renamed from: player net.minecraft.client.animals.l
   private Model field_229;

   public MobModelRenderer(Model var1, float var2) {
      this.model = var1;
      this.field_222 = var2;
   }

   // $FF: renamed from: game (net.minecraft.client.animals.l) void
   public final void method_103(Model var1) {
      this.field_229 = var1;
   }

   // $FF: renamed from: game (net.minecraft.game.entities.player, float, float, float, float, float) void
   public void render_mob(Mob mob, float x, float y, float z, float myaw, float dt) {
      GL11.glPushMatrix();
      GL11.glDisable(2884);

      try {
         myaw = mob.field_83 + (mob.field_82 - mob.field_83) * dt;
         float myaw_2 = mob.prev_yaw + (mob.yaw - mob.prev_yaw) * dt;
         float mpitch = mob.prev_pitch + (mob.pitch - mob.prev_pitch) * dt;
         GL11.glTranslatef(x, y, z);
         x = (float)mob.alive_tick + dt;
         GL11.glRotatef(180.0F - myaw, 0.0F, 1.0F, 0.0F);
         if (mob.death_time > 0) {
            if ((y = MathHelper.sqrt(((float)mob.death_time + dt - 1.0F) / 20.0F * 1.6F)) > 1.0F) {
               y = 1.0F;
            }

            GL11.glRotatef(y * this.method_106(mob), 0.0F, 0.0F, 1.0F);
         }

         GL11.glScalef(-0.0625F, -0.0625F, 0.0625F);
         this.mob_specific_animation(mob, dt);
         GL11.glTranslatef(0.0F, -24.0F, 0.0F);
         GL11.glEnable(2977);
         y = mob.field_98 + (mob.field_99 - mob.field_98) * dt;
         z = mob.field_100 - mob.field_99 * (1.0F - dt);
         if (y > 1.0F) {
            y = 1.0F;
         }

         this.method_96(mob.texture_url, mob.maybe_texture_name());
         GL11.glEnable(3008);
         this.model.animate_and_render(z, y, x, myaw_2 - myaw, mpitch, 1.0F);

         for(int var9 = 0; var9 < 4; ++var9) {
            if (this.method_105(mob, var9)) {
               this.field_229.animate_and_render(z, y, x, myaw_2 - myaw, mpitch, 1.0F);
               GL11.glDisable(3042);
               GL11.glEnable(3008);
            }
         }

         float var15 = mob.light_level(dt);
         int var14;
         if ((var14 = this.get_color_modifier(mob, var15, dt)) >>> 24 > 0 || mob.hurt_time > 0 || mob.death_time > 0) {
            GL11.glDisable(3553);
            GL11.glDisable(3008);
            GL11.glEnable(3042);
            GL11.glBlendFunc(770, 771);
            GL11.glDepthFunc(514);
            if (mob.hurt_time > 0 || mob.death_time > 0) {
               GL11.glColor4f(var15, 0.0F, 0.0F, 0.4F);
               this.model.animate_and_render(z, y, x, myaw_2 - myaw, mpitch, 1.0F);

               for(int var10 = 0; var10 < 4; ++var10) {
                  if (this.method_105(mob, var10)) {
                     GL11.glColor4f(var15, 0.0F, 0.0F, 0.4F);
                     this.field_229.animate_and_render(z, y, x, myaw_2 - myaw, mpitch, 1.0F);
                  }
               }
            }

            if (var14 >>> 24 > 0) {
               float var16 = (float)(var14 >> 16 & 255) / 255.0F;
               var15 = (float)(var14 >> 8 & 255) / 255.0F;
               float var11 = (float)(var14 & 255) / 255.0F;
               dt = (float)(var14 >>> 24) / 255.0F;
               GL11.glColor4f(var16, var15, var11, dt);
               this.model.animate_and_render(z, y, x, myaw_2 - myaw, mpitch, 1.0F);

               for(int var12 = 0; var12 < 4; ++var12) {
                  if (this.method_105(mob, var12)) {
                     GL11.glColor4f(var16, var15, var11, dt);
                     this.field_229.animate_and_render(z, y, x, myaw_2 - myaw, mpitch, 1.0F);
                  }
               }
            }

            GL11.glDepthFunc(515);
            GL11.glDisable(3042);
            GL11.glEnable(3008);
            GL11.glEnable(3553);
         }

         GL11.glDisable(2977);
      } catch (Exception var13) {
         var13.printStackTrace();
      }

      GL11.glEnable(2884);
      GL11.glPopMatrix();
   }

   // $FF: renamed from: game (net.minecraft.game.entities.player, int) boolean
   protected boolean method_105(Mob var1, int var2) {
      return false;
   }

   // $FF: renamed from: game (net.minecraft.game.entities.player) float
   protected float method_106(Mob var1) {
      return 90.0F;
   }

   // $FF: renamed from: game (net.minecraft.game.entities.player, float, float) int
   protected int get_color_modifier(Mob var1, float light, float dt) {
      return 0;
   }

   // $FF: renamed from: game (net.minecraft.game.entities.player, float) void
   protected void mob_specific_animation(Mob var1, float var2) {
   }

   public void render_model(Entity b, float f1, float f2, float f3, float f4, float f5) {
      this.render_mob((Mob)b, f1, f2, f3, f4, f5);
   }
}
