package net.minecraft.client.rendering.models;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import net.minecraft.game.entities.misc.FallingBlock;
import net.minecraft.game.world.World;
import net.minecraft.game.entities.Entity;
import net.minecraft.game.entities.Mob;
import net.minecraft.game.entities.monsters.Spider;
import net.minecraft.game.entities.monsters.Creeper;
import net.minecraft.game.entities.monsters.Zombie;
import net.minecraft.game.entities.animals.Pig;
import net.minecraft.game.entities.player.Player;
import net.minecraft.client.rendering.ResourceManager;
import net.minecraft.client.models.ZombieModel;
import net.minecraft.client.models.SkeletonModel;
import net.minecraft.client.models.PigModel;
import net.minecraft.client.models.class_165;
import net.minecraft.client.models.class_166;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.game.game.i
public final class ModelManager {
   // $FF: renamed from: player java.util.Map
   private Map models = new HashMap();
   // $FF: renamed from: game net.minecraft.client.game.game.i
   public static ModelManager singleton = new ModelManager();
   // $FF: renamed from: animals net.minecraft.client.game.k
   public ResourceManager maybe_resource_manager;
   // $FF: renamed from: entities net.minecraft.game.game.player
   public World world;
   // $FF: renamed from: projectiles float
   public float yaw;
   // $FF: renamed from: particles float
   private float x;
   // $FF: renamed from: player float
   private float y;
   // $FF: renamed from: h float
   private float z;

   private ModelManager() {
      this.models.put(Spider.class, new SpiderModelRenderer());
      this.models.put(Pig.class, new MobModelRenderer(new PigModel(), 0.7F));
      this.models.put(net.minecraft.game.entities.animals.Sheep.class, new SheepModelRenderer(new class_166(), new class_165(), 0.7F));
      this.models.put(Creeper.class, new CreeperModelRenderer());
      this.models.put(net.minecraft.game.entities.monsters.Skeleton.class, new MobModelRenderer(new SkeletonModel(), 0.5F));
      this.models.put(Zombie.class, new MobModelRenderer(new ZombieModel(), 0.5F));
      this.models.put(Player.class, new PlayerModelRenderer());
      this.models.put(net.minecraft.game.entities.monsters.Giant.class, new GiantModelRenderer(new ZombieModel(), 0.5F, 6.0F));
      this.models.put(Mob.class, new MobModelRenderer(new net.minecraft.client.models.MobModel(), 0.5F));
      this.models.put(Entity.class, new EntityModel());
      this.models.put(net.minecraft.game.entities.Painting.class, new PaintingModel());
      this.models.put(net.minecraft.game.entities.projectiles.Arrow.class, new ArrowModel());
      this.models.put(net.minecraft.game.entities.misc.ItemDrop.class, new ItemDropModelRenderer());
      this.models.put(net.minecraft.game.entities.misc.PrimedTnt.class, new PrimedTntModel());
      this.models.put(FallingBlock.class, new FallingBlockRenderer());
      Iterator models = this.models.values().iterator();

      while(models.hasNext()) {
         ((ModelRenderer)models.next()).intialize(this);
      }

   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals) net.minecraft.client.game.game.particles
   public final ModelRenderer model_of(Entity ent) {
      Class entClass = ent.getClass();
      ModelRenderer model = (ModelRenderer)this.models.get(entClass);
      if (model == null && entClass != net.minecraft.game.entities.Entity.class) {
         model = (ModelRenderer)this.models.get(entClass.getSuperclass());
         this.models.put(entClass, model);
      }

      return model;
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, net.minecraft.client.game.k, net.minecraft.game.entities.player.game, float) void
   public final void set_player(World w, ResourceManager rm, Player player, float dt) {
      this.world = w;
      this.maybe_resource_manager = rm;
      this.yaw = player.prev_yaw + (player.yaw - player.prev_yaw) * dt;
      this.x = player.chunk_x + (player.pos_x - player.chunk_x) * dt;
      this.y = player.chunk_y + (player.pos_y - player.chunk_y) * dt;
      this.z = player.chunk_z + (player.pos_z - player.chunk_z) * dt;
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, float) void
   public final void render_entity(Entity ent, float dt) {
      float x = ent.chunk_x + (ent.pos_x - ent.chunk_x) * dt;
      float y = ent.chunk_y + (ent.pos_y - ent.chunk_y) * dt;
      float z = ent.chunk_z + (ent.pos_z - ent.chunk_z) * dt;
      float yaw = ent.prev_yaw + (ent.yaw - ent.prev_yaw) * dt;
      float light = this.world.light_at((int)x, (int)(y + ent.half_height()), (int)z);
      GL11.glColor3f(light, light, light);
      this.fully_render_entity(ent, x, y, z, yaw, dt);
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, float, float, float, float, float) void
   public final void fully_render_entity(Entity ent, float x, float y, float z, float yaw, float dt) {
      ModelRenderer model;
      if ((model = this.model_of(ent)) != null) {
         model.render_model(ent, x, y, z, yaw, dt);
         model.render_model_effects(ent, x, y, z, dt);
      }

   }

   // $FF: renamed from: game (net.minecraft.game.game.player) void
   public final void set_world(World var1) {
      this.world = var1;
   }

   // $FF: renamed from: game (float, float, float) float
   public final float kinda_distance(float x, float y, float z) {
      x -= this.x;
      y -= this.y;
      z -= this.z;
      return x * x + y * y + z * z;
   }
}
