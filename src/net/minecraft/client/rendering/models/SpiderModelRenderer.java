package net.minecraft.client.rendering.models;

import net.minecraft.client.models.class_161;
import net.minecraft.game.entities.Mob;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.game.game.m
public final class SpiderModelRenderer extends MobModelRenderer {
   public SpiderModelRenderer() {
      super(new class_161(), 1.0F);
      this.method_103(new class_161());
   }

   protected final float method_106(Mob e1) {
      return 180F;
   }

   protected final boolean method_105(Mob e1, int i) {
      int k = i;
      if(k != 0) {
         return false;
      }
      if(k != 0) {
         return false;
      } else {
         this.bind_texture("/mob/spider_eyes.png");
         float l = (1.0F - (e1.light_level(1.0F)) * 0.5F);
         GL11.glEnable(3042);
         GL11.glDisable(3008);
         GL11.glBlendFunc(770, 771);
         GL11.glColor4f(1.0F, 1.0F, 1.0F, l);
         return true;
      }
   }
}
