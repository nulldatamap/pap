package net.minecraft.client.rendering.models;

import java.util.Random;

import a.MathHelper;
import net.minecraft.game.entities.misc.ItemDrop;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.entities.Entity;
import net.minecraft.client.rendering.GeometryDrawer;
import net.minecraft.client.rendering.ResourceManager;
import net.minecraft.client.rendering.BlockRenderer;
import net.minecraft.client.ui.TextDrawer;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.game.game.player
public final class ItemDropModelRenderer extends ModelRenderer {
   // $FF: renamed from: projectiles net.minecraft.client.game.particles
   private BlockRenderer block_renderer = new BlockRenderer();
   // $FF: renamed from: player java.util.Random
   private Random random = new Random();

   public ItemDropModelRenderer() {
      this.field_222 = 0.15F;
      this.field_223 = 0.75F;
   }

   // $FF: renamed from: game (net.minecraft.client.game.k, net.minecraft.game.animals.k, int, int) void
   public final void render_in_inventory(ResourceManager rm, ItemStack item, int x, int y) {
      if (item != null) {
         // Render block stack
         if (item.id < 256 && Block.blocks[item.id].method_492() == 0) {
            int block_id = item.id;
            ResourceManager.bind_texture(rm.load_texture("/terrain.png"));
            Block block = Block.blocks[block_id];
            GL11.glPushMatrix();
            GL11.glTranslatef((float)(x - 2), (float)(y + 3), 0.0F);
            GL11.glScalef(10.0F, 10.0F, 10.0F);
            GL11.glTranslatef(1.0F, 0.5F, 8.0F);
            GL11.glRotatef(210.0F, 1.0F, 0.0F, 0.0F);
            GL11.glRotatef(45.0F, 0.0F, 1.0F, 0.0F);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            this.block_renderer.fully_render(block);
            GL11.glPopMatrix();
         } else {
            // Render item stack
            if (item.item().get_item_id() >= 0) {
               GL11.glDisable(2896);
               if (item.id < 256) {
                  ResourceManager.bind_texture(rm.load_texture("/terrain.png"));
               } else {
                  ResourceManager.bind_texture(rm.load_texture("/gui/items.png"));
               }

               int texx = item.item().get_item_id() % 16 << 4;
               int texy = item.item().get_item_id() / 16 << 4;
               // boolean var6 = true;
               int ax = x;
               int ay = y;
               GeometryDrawer item_quad = GeometryDrawer.singleton;
               GeometryDrawer.singleton.quads_mode();
               item_quad.add_vertex((float)ax, (float)(ay + 16), 0.0F, (float)texx * 0.00390625F, (float)(texy + 16) * 0.00390625F);
               item_quad.add_vertex((float)(ax + 16), (float)(ay + 16), 0.0F, (float)(texx + 16) * 0.00390625F, (float)(texy + 16) * 0.00390625F);
               item_quad.add_vertex((float)(ax + 16), (float)ay, 0.0F, (float)(texx + 16) * 0.00390625F, (float)texy * 0.00390625F);
               item_quad.add_vertex((float)ax, (float)ay, 0.0F, (float)texx * 0.00390625F, (float)texy * 0.00390625F);
               item_quad.draw();

               GL11.glEnable(2896);
            }
         }
      }
   }

   // $FF: renamed from: game (net.minecraft.client.entities.j, net.minecraft.game.animals.k, int, int) void
   public final void render_info_in_inventory(TextDrawer text_drawer, ItemStack item, int x, int y) {
      if (item != null) {
         if (item.count > 1) {
            String count_text = "" + item.count;

            GL11.glDisable(GL11.GL_LIGHTING);

            GL11.glDisable(GL11.GL_DEPTH_TEST);

            text_drawer.draw_text(count_text, x + 19 - 2 - text_drawer.text_width(count_text), y + 6 + 3, 0xFFFFFF);

            GL11.glEnable(GL11.GL_LIGHTING);

            GL11.glEnable(GL11.GL_DEPTH_TEST);
         }

         if (item.damage > 0) {
            int var9 = 13 - item.damage * 13 / item.durability();
            int var7 = 255 - item.damage * 255 / item.durability();
            GL11.glDisable(2896);
            GL11.glDisable(2929);
            GL11.glDisable(3553);
            GeometryDrawer var8 = GeometryDrawer.singleton;
            int var6 = 255 - var7 << 16 | var7 << 8;
            var7 = (255 - var7) / 4 << 16 | 16128;
            render_square(var8, x + 2, y + 13, 13, 2, 0);
            render_square(var8, x + 2, y + 13, 12, 1, var7);
            render_square(var8, x + 2, y + 13, var9, 1, var6);
            GL11.glEnable(3553);
            GL11.glEnable(2896);
            GL11.glEnable(2929);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         }

      }
   }

   // $FF: renamed from: game (net.minecraft.client.game.projectiles, int, int, int, int, int) void
   private static void render_square(GeometryDrawer var0, int var1, int var2, int var3, int var4, int var5) {
      var0.quads_mode();
      var0.set_color(var5);
      var0.add_vertex((float)var1, (float)var2, 0.0F);
      var0.add_vertex((float)var1, (float)(var2 + var4), 0.0F);
      var0.add_vertex((float)(var1 + var3), (float)(var2 + var4), 0.0F);
      var0.add_vertex((float)(var1 + var3), (float)var2, 0.0F);
      var0.draw();
   }

   public final void render_model(Entity ent, float scale, float x, float y, float z, float dt) {
      z = y;
      y = x;
      x = scale;
      ItemDrop drop = (ItemDrop) ent;
      random.setSeed(187L);
      ItemStack stack = drop.item_representing;
      GL11.glPushMatrix();
      float f6 = MathHelper.sin(((float)drop.age + dt) / 10F + drop.field_130) * 0.1F + 0.1F;
      dt = (((float)drop.age + dt) / 20F + drop.field_130) * 57.29578F;
      byte byte0 = 1;
      if(stack.count > 1) {
         byte0 = 2;
      }
      if(stack.count > 5) {
         byte0 = 3;
      }
      if(stack.count > 20) {
         byte0 = 4;
      }
      GL11.glTranslatef(x, y + f6, z);
      GL11.glEnable(2977);
      if(stack.id < 256 && Block.blocks[stack.id].method_492() == 0) {
         GL11.glRotatef(dt, 0.0F, 1.0F, 0.0F);
         this.bind_texture("/terrain.png");
         scale = 0.25F;
         if(!Block.blocks[stack.id].method_491() && stack.id != Block.field_911.block_id) {
            scale = 0.5F;
         }
         GL11.glScalef(scale, scale, scale);
         for(x = 0; x < byte0; x++) {
            GL11.glPushMatrix();
            if(x > 0) {
               y = ((this.random.nextFloat() * 2.0F - 1.0F) * 0.2F) / scale;
               z = ((this.random.nextFloat() * 2.0F - 1.0F) * 0.2F) / scale;
               dt = ((this.random.nextFloat() * 2.0F - 1.0F) * 0.2F) / scale;
               GL11.glTranslatef(y, z, dt);
            }
            this.block_renderer.fully_render(Block.blocks[stack.id]);
            GL11.glPopMatrix();
         }

      } else {
         GL11.glScalef(0.5F, 0.5F, 0.5F);
         int id = (stack.item().get_item_id());
         if(stack.id < 256) {
            this.bind_texture("/terrain.png");
         } else {
            this.bind_texture("/gui/items.png");
         }
         GeometryDrawer drawer = GeometryDrawer.singleton;
         y = (float)(id % 16 << 4) / 256F;
         z = (float)((id % 16 << 4) + 16) / 256F;
         dt = (float)(id / 16 << 4) / 256F;
         scale = (float)((id / 16 << 4) + 16) / 256F;
         for(int l = 0; l < byte0; l++) {
            GL11.glPushMatrix();
            if(l > 0) {
               float f7 = (this.random.nextFloat() * 2.0F - 1.0F) * 0.3F;
               float f8 = (this.random.nextFloat() * 2.0F - 1.0F) * 0.3F;
               float f9 = (this.random.nextFloat() * 2.0F - 1.0F) * 0.3F;
               GL11.glTranslatef(f7, f8, f9);
            }
            GL11.glRotatef(180F - this.manager.yaw, 0.0F, 1.0F, 0.0F);
            drawer.quads_mode();
            GeometryDrawer.set_normal(0.0F, 1.0F, 0.0F);
            drawer.add_vertex(-0.5F, -0.25F, 0.0F, y, scale);
            drawer.add_vertex(0.5F, -0.25F, 0.0F, z, scale);
            drawer.add_vertex(0.5F, 0.75F, 0.0F, z, dt);
            drawer.add_vertex(-0.5F, 0.75F, 0.0F, y, dt);
            drawer.draw();
            GL11.glPopMatrix();
         }

      }
      GL11.glDisable(2977);
      GL11.glPopMatrix();
   }
}
