package net.minecraft.client.rendering.models;

import net.minecraft.client.models.Model;
import net.minecraft.game.entities.Mob;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.game.game.l
public final class GiantModelRenderer extends MobModelRenderer {
   // $FF: renamed from: player float
   private float field_230 = 6.0F;

   public GiantModelRenderer(Model var1, float var2, float var3) {
      super(var1, 3.0F);
   }

   protected final void mob_specific_animation(Mob e1, float f) {
      GL11.glScalef(field_230, field_230, field_230);
   }
}
