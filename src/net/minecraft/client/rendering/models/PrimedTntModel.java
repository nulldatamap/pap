package net.minecraft.client.rendering.models;

import net.minecraft.game.entities.Entity;
import net.minecraft.client.rendering.BlockRenderer;
import net.minecraft.game.entities.misc.PrimedTnt;
import net.minecraft.game.world.block.Block;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.game.game.projectiles
public final class PrimedTntModel extends ModelRenderer {
   // $FF: renamed from: projectiles net.minecraft.client.game.particles
   private BlockRenderer block_renderer = new BlockRenderer();

   public final void render_model(Entity b, float f1, float f2, float f3, float f4, float f5) {
      f5 = f5;
      f4 = f3;
      f3 = f2;
      f2 = f1;
      PrimedTnt tnt = (PrimedTnt) b;
      GL11.glPushMatrix();
      GL11.glTranslatef(f2, f3, f4);
      if(((float)tnt.fuse_time - f5) + 1.0F < 10F) {
         if((f2 = 1.0F - (((float)tnt.fuse_time - f5) + 1.0F) / 10F) < 0.0F) {
            f2 = 0.0F;
         }
         if(f2 > 1.0F) {
            f2 = 1.0F;
         }
         f2 = (f2 *= f2) * f2;
         GL11.glScalef(f2 = 1.0F + f2 * 0.3F, f2, f2);
      }
      f2 = (1.0F - (((float)tnt.fuse_time - f5) + 1.0F) / 100F) * 0.8F;
      this.bind_texture("/terrain.png");
      this.block_renderer.fully_render( Block.tnt );
      if((tnt.fuse_time / 5) % 2 == 0) {
         GL11.glDisable(3553);
         GL11.glDisable(2896);
         GL11.glEnable(3042);
         GL11.glBlendFunc(770, 772);
         GL11.glColor4f(1.0F, 1.0F, 1.0F, f2);
         this.block_renderer.fully_render( Block.tnt );
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GL11.glDisable(3042);
         GL11.glEnable(2896);
         GL11.glEnable(3553);
      }
      GL11.glPopMatrix();
   }

   public PrimedTntModel() {
      this.field_222 = 0.5F;
   }
}
