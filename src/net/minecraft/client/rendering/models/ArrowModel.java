package net.minecraft.client.rendering.models;

import a.MathHelper;
import net.minecraft.client.rendering.GeometryDrawer;
import net.minecraft.game.entities.Entity;
import net.minecraft.game.entities.projectiles.Arrow;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.game.game.animals
public final class ArrowModel extends ModelRenderer {
    public final void render_model(Entity b1, float f1, float f2, float f3, float f4, float f5) {
        f5 = f5;
        f4 = f3;
        f3 = f2;
        f2 = f1;
        Arrow arrow = (Arrow) b1;
        bind_texture("/item/arrows.png");
        GL11.glPushMatrix();
        GL11.glTranslatef(f2, f3, f4);
        GL11.glRotatef((arrow.prev_yaw + (arrow.yaw - arrow.prev_yaw) * f5) - 90F, 0.0F, 1.0F, 0.0F);
        GL11.glRotatef(arrow.prev_pitch + (arrow.pitch - arrow.prev_pitch) * f5, 0.0F, 0.0F, 1.0F);
        GeometryDrawer drawer = GeometryDrawer.singleton;
        GL11.glEnable(2977);
        if((f1 = (float)arrow.field_56 - f5) > 0.0F) {
            GL11.glRotatef(f1 = MathHelper.sin(f1 * 3F) * f1, 0.0F, 0.0F, 1.0F);
        }
        GL11.glRotatef(45F, 1.0F, 0.0F, 0.0F);
        GL11.glScalef(0.05625F, 0.05625F, 0.05625F);
        GL11.glTranslatef(-4F, 0.0F, 0.0F);
        GL11.glNormal3f(0.05625F, 0.0F, 0.0F);
        drawer.quads_mode();
        drawer.add_vertex(-7F, -2F, -2F, 0.0F, 0.15625F);
        drawer.add_vertex(-7F, -2F, 2.0F, 0.15625F, 0.15625F);
        drawer.add_vertex(-7F, 2.0F, 2.0F, 0.15625F, 0.3125F);
        drawer.add_vertex(-7F, 2.0F, -2F, 0.0F, 0.3125F);
        drawer.draw();
        GL11.glNormal3f(-0.05625F, 0.0F, 0.0F);
        drawer.quads_mode();
        drawer.add_vertex(-7F, 2.0F, -2F, 0.0F, 0.15625F);
        drawer.add_vertex(-7F, 2.0F, 2.0F, 0.15625F, 0.15625F);
        drawer.add_vertex(-7F, -2F, 2.0F, 0.15625F, 0.3125F);
        drawer.add_vertex(-7F, -2F, -2F, 0.0F, 0.3125F);
        drawer.draw();
        for(f1 = 0; f1 < 4; f1++) {
            GL11.glRotatef(90F, 1.0F, 0.0F, 0.0F);
            GL11.glNormal3f(0.0F, 0.0F, 0.05625F);
            drawer.quads_mode();
            drawer.add_vertex(-8F, -2F, 0.0F, 0.0F, 0.0F);
            drawer.add_vertex(8F, -2F, 0.0F, 0.5F, 0.0F);
            drawer.add_vertex(8F, 2.0F, 0.0F, 0.5F, 0.15625F);
            drawer.add_vertex(-8F, 2.0F, 0.0F, 0.0F, 0.15625F);
            drawer.draw();
        }

        GL11.glDisable(2977);
        GL11.glPopMatrix();
    }
}
