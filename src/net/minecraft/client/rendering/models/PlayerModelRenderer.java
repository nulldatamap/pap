package net.minecraft.client.rendering.models;

import net.minecraft.game.entities.Entity;
import net.minecraft.game.entities.Mob;
import net.minecraft.game.entities.player.Player;
import net.minecraft.game.items.Armor;

// $FF: renamed from: net.minecraft.client.game.game.player
public final class PlayerModelRenderer extends MobModelRenderer {
   // $FF: renamed from: player net.minecraft.client.animals.h
   private net.minecraft.client.models.MobModel field_231;
   // $FF: renamed from: particles net.minecraft.client.animals.h
   private net.minecraft.client.models.MobModel field_232;
   // $FF: renamed from: player net.minecraft.client.animals.h
   private net.minecraft.client.models.MobModel field_233;
   // $FF: renamed from: h java.lang.String[]
   private static final String[] field_234 = new String[]{"cloth", "chain", "iron", "diamond", "gold"};

   public PlayerModelRenderer() {
      super(new net.minecraft.client.models.MobModel(0.0F), 0.5F);
      this.field_231 = (net.minecraft.client.models.MobModel)this.model;
      this.field_232 = new net.minecraft.client.models.MobModel(1.0F);
      this.field_233 = new net.minecraft.client.models.MobModel(0.5F);
   }

   // $FF: renamed from: game (net.minecraft.game.entities.player.game, float, float, float, float, float) void
   private void method_109(Player var1, float var2, float var3, float var4, float var5, float var6) {
      super.render_model(var1, var2, var3 - var1.camera_height, var4, var5, var6);
   }

   // $FF: renamed from: game () void
   public final void method_110() {
      this.field_231.field_760.render(1.0F);
   }

/*
   protected final boolean method_105(Mob e1, int i) {
      int l;
      int i1;
      l = i;
      a _i = ((int) ((a)e1));
      e1 = ((Mob) (this));
      i1 = 3 - l;
      _i = ((int) (((a) (_i)).block));
      ((a) (_i)).block.block[i1];
      _i = ((int) (((a) (_i)).block.block[i1]));
      Player p = (Player) e1;
      Armor armor = (Armor) p.inventory.armor_area[i].item();
      if((_i = ((int) (((k) (_i)).a()))) instanceof MobModelRenderer) {
         _i = ((int) ((MobModelRenderer)_i));
         ((g) (e1)).a((new StringBuilder()).append("/armor/")
                                           .append(field_234[((MobModelRenderer) (_i)).au])
                                           .append("_").append(l != 2 ? 1 : 2)
                                           .append(".png")
                                           .toString());
         ((h) (_i = ((int) (l != 2 ? ((int) (((g) (e1)).f)) : ((int) (((g) (e1)).g)))))).a.e = l == 0;
         ((h) (_i)).block.e = l == 0;
         ((h) (_i)).gen.e = l == 1 || l == 2;
         ((h) (_i)).material.e = l == 1;
         ((h) (_i)).e.e = l == 1;
         ((h) (_i)).f.e = l == 2 || l == 3;
         ((h) (_i)).g.e = l == 2 || l == 3;
         ((g) (e1)).a(((net.minecraft.client.block.l) (_i)));
         return true;
      }
      return false;
   }*/


    public final void render_model(Entity e1, float f1, float f2, float f3, float f4, float f5) {
       method_109((Player)e1, f1, f2, f3, f4, f5);
    }

    /*
    public final void a(net.minecraft.a.gen.block b1, float f1, float f2, float f3, float f4, float f5) {
        a((a)b1, f1, f2, f3, f4, f5);
    }
    */
}
