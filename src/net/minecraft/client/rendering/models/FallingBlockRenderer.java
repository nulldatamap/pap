package net.minecraft.client.rendering.models;

import net.minecraft.client.rendering.BlockRenderer;
import net.minecraft.game.entities.Entity;
import net.minecraft.game.entities.misc.FallingBlock;
import net.minecraft.game.world.block.Block;
import org.lwjgl.opengl.GL11;

public class FallingBlockRenderer extends ModelRenderer {
    private BlockRenderer block_renderer = new BlockRenderer();

    public void render_model(Entity ent, float x, float y, float z, float yaw, float dt) {
        FallingBlock b = (FallingBlock)ent;

        GL11.glPushMatrix();

        GL11.glTranslatef( x, y ,z );
        this.bind_texture("/terrain.png");
        block_renderer.fully_render(Block.blocks[b.block_id]);

        GL11.glPopMatrix();
    }
}
