package net.minecraft.client.rendering.models;

import net.minecraft.game.entities.Entity;

import java.util.Random;

// $FF: renamed from: net.minecraft.client.game.game.h
public final class PaintingModel extends ModelRenderer {
   // $FF: renamed from: projectiles java.util.Random
   private Random field_224 = new Random();

   public void render_model(Entity ent, float x, float y, float z, float yaw, float dt) { }

   /*
   public final void render_model(block block, float f1, float f2, float f3, float f4, float f5) {
        f5 = f4;
        f4 = f3;
        f3 = f2;
        f2 = f1;
        f1 = ((float) ((a)block));
        block = ((block) (this));
        material.setSeed(187L);
        GL11.glPushMatrix();
        GL11.glTranslatef(f2, f3, f4);
        GL11.glRotatef(f5, 0.0F, 1.0F, 0.0F);
        GL11.glEnable(2977);
        ((h) (block)).a("/art/kz.png");
        f2 = ((float) (((a) (f1)).block));
        GL11.glScalef(0.0625F, 0.0625F, 0.0625F);
        f5 = ((float) (((material) (f2)).f));
        f4 = ((float) (((material) (f2)).e));
        f3 = ((float) (((material) (f2)).material));
        f2 = ((float) (((material) (f2)).gen));
        f1 = f1;
        block = block;
        float f6 = (float)(-f2) / 2.0F;
        float f7 = (float)(-f3) / 2.0F;
        for(int j = 0; j < f2 / 16; j++) {
            for(int k = 0; k < f3 / 16; k++) {
                float f8 = f6 + (float)(j + 1 << 4);
                float f9 = f6 + (float)(j << 4);
                float f10 = f7 + (float)(k + 1 << 4);
                float f11 = f7 + (float)(k << 4);
                float f15 = (f10 + f11) / 2.0F;
                float f14 = (f8 + f9) / 2.0F;
                float f13 = f1;
                block b1 = block;
                int l = (int)((a) (f13)).h;
                f15 = ((float) ((int)(((a) (f13)).i + f15 / 16F)));
                int i1 = (int)((a) (f13)).j;
                if(((a) (f13)).a == 0) {
                    l = (int)(((a) (f13)).h + f14 / 16F);
                }
                if(((a) (f13)).a == 1) {
                    i1 = (int)(((a) (f13)).j - f14 / 16F);
                }
                if(((a) (f13)).a == 2) {
                    l = (int)(((a) (f13)).h - f14 / 16F);
                }
                if(((a) (f13)).a == 3) {
                    i1 = (int)(((a) (f13)).j + f14 / 16F);
                }
                float f12;
                GL11.glColor3f(f12 = ((h) (b1)).a.gen.gen(l, ((int) (f15)), i1), f12, f12);
                f12 = (float)((f4 + f2) - (j << 4)) / 256F;
                f13 = (float)((f4 + f2) - (j + 1 << 4)) / 256F;
                f14 = (float)((f5 + f3) - (k << 4)) / 256F;
                f15 = (float)((f5 + f3) - (k + 1 << 4)) / 256F;
                net.minecraft.client.a.material d1;
                (d1 = net.minecraft.client.a.material.a).block();
                net.minecraft.client.a.material.gen(0.0F, 0.0F, -1F);
                d1.a(f8, f11, -0.5F, f13, f14);
                d1.a(f9, f11, -0.5F, f12, f14);
                d1.a(f9, f10, -0.5F, f12, f15);
                d1.a(f8, f10, -0.5F, f13, f15);
                net.minecraft.client.a.material.gen(0.0F, 0.0F, 1.0F);
                d1.a(f8, f10, 0.5F, 0.75F, 0.0F);
                d1.a(f9, f10, 0.5F, 0.8125F, 0.0F);
                d1.a(f9, f11, 0.5F, 0.8125F, 0.0625F);
                d1.a(f8, f11, 0.5F, 0.75F, 0.0625F);
                net.minecraft.client.a.material.gen(0.0F, -1F, 0.0F);
                d1.a(f8, f10, -0.5F, 0.75F, 0.001953125F);
                d1.a(f9, f10, -0.5F, 0.8125F, 0.001953125F);
                d1.a(f9, f10, 0.5F, 0.8125F, 0.001953125F);
                d1.a(f8, f10, 0.5F, 0.75F, 0.001953125F);
                net.minecraft.client.a.material.gen(0.0F, 1.0F, 0.0F);
                d1.a(f8, f11, 0.5F, 0.75F, 0.001953125F);
                d1.a(f9, f11, 0.5F, 0.8125F, 0.001953125F);
                d1.a(f9, f11, -0.5F, 0.8125F, 0.001953125F);
                d1.a(f8, f11, -0.5F, 0.75F, 0.001953125F);
                net.minecraft.client.a.material.gen(-1F, 0.0F, 0.0F);
                d1.a(f8, f10, 0.5F, 0.7519531F, 0.0F);
                d1.a(f8, f11, 0.5F, 0.7519531F, 0.0625F);
                d1.a(f8, f11, -0.5F, 0.7519531F, 0.0625F);
                d1.a(f8, f10, -0.5F, 0.7519531F, 0.0F);
                net.minecraft.client.a.material.gen(1.0F, 0.0F, 0.0F);
                d1.a(f9, f10, -0.5F, 0.7519531F, 0.0F);
                d1.a(f9, f11, -0.5F, 0.7519531F, 0.0625F);
                d1.a(f9, f11, 0.5F, 0.7519531F, 0.0625F);
                d1.a(f9, f10, 0.5F, 0.7519531F, 0.0F);
                d1.a();
            }

        }

        GL11.glDisable(2977);
        GL11.glPopMatrix();
    }
    */
}
