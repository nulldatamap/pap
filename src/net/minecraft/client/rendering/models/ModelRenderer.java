package net.minecraft.client.rendering.models;

import net.minecraft.game.world.World;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.entities.Entity;
import net.minecraft.game.util.AABB;
import net.minecraft.client.rendering.GeometryDrawer;
import net.minecraft.client.rendering.ResourceManager;
import net.minecraft.client.rendering.BlockRenderer;
import net.minecraft.client.models.MobModel;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.game.game.particles
public abstract class ModelRenderer {
   // $FF: renamed from: game net.minecraft.client.game.game.i
   protected ModelManager manager;
   // $FF: renamed from: animals float
   protected float field_222;
   // $FF: renamed from: entities float
   protected float field_223;

   public ModelRenderer() {
      new MobModel();
      new BlockRenderer();
      this.field_222 = 0.0F;
      this.field_223 = 1.0F;
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, float, float, float, float, float) void
   public abstract void render_model(Entity ent, float x, float y, float z, float yaw, float dt);

   // $FF: renamed from: game (java.lang.String) void
   protected final void bind_texture(String var1) {
      ResourceManager var2 = this.manager.maybe_resource_manager;
      ResourceManager.bind_texture(this.manager.maybe_resource_manager.load_texture(var1));
   }

   // $FF: renamed from: game (java.lang.String, java.lang.String) void
   protected final void method_96(String var1, String var2) {
      ResourceManager var3 = this.manager.maybe_resource_manager;
      ResourceManager.bind_texture(this.manager.maybe_resource_manager.method_394(var1, var2));
   }

   // $FF: renamed from: game (net.minecraft.game.projectiles.animals) void
   public static void render_hitbox(AABB var0) {
      GL11.glDisable(3553);
      GeometryDrawer var1 = GeometryDrawer.singleton;
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      var1.quads_mode();
      GeometryDrawer.set_normal(0.0F, 0.0F, -1.0F);
      var1.add_vertex(var0.min_x, var0.max_y, var0.min_z);
      var1.add_vertex(var0.max_x, var0.max_y, var0.min_z);
      var1.add_vertex(var0.max_x, var0.min_y, var0.min_z);
      var1.add_vertex(var0.min_x, var0.min_y, var0.min_z);
      GeometryDrawer.set_normal(0.0F, 0.0F, 1.0F);
      var1.add_vertex(var0.min_x, var0.min_y, var0.max_z);
      var1.add_vertex(var0.max_x, var0.min_y, var0.max_z);
      var1.add_vertex(var0.max_x, var0.max_y, var0.max_z);
      var1.add_vertex(var0.min_x, var0.max_y, var0.max_z);
      GeometryDrawer.set_normal(0.0F, -1.0F, 0.0F);
      var1.add_vertex(var0.min_x, var0.min_y, var0.min_z);
      var1.add_vertex(var0.max_x, var0.min_y, var0.min_z);
      var1.add_vertex(var0.max_x, var0.min_y, var0.max_z);
      var1.add_vertex(var0.min_x, var0.min_y, var0.max_z);
      GeometryDrawer.set_normal(0.0F, 1.0F, 0.0F);
      var1.add_vertex(var0.min_x, var0.max_y, var0.max_z);
      var1.add_vertex(var0.max_x, var0.max_y, var0.max_z);
      var1.add_vertex(var0.max_x, var0.max_y, var0.min_z);
      var1.add_vertex(var0.min_x, var0.max_y, var0.min_z);
      GeometryDrawer.set_normal(-1.0F, 0.0F, 0.0F);
      var1.add_vertex(var0.min_x, var0.min_y, var0.max_z);
      var1.add_vertex(var0.min_x, var0.max_y, var0.max_z);
      var1.add_vertex(var0.min_x, var0.max_y, var0.min_z);
      var1.add_vertex(var0.min_x, var0.min_y, var0.min_z);
      GeometryDrawer.set_normal(1.0F, 0.0F, 0.0F);
      var1.add_vertex(var0.max_x, var0.min_y, var0.min_z);
      var1.add_vertex(var0.max_x, var0.max_y, var0.min_z);
      var1.add_vertex(var0.max_x, var0.max_y, var0.max_z);
      var1.add_vertex(var0.max_x, var0.min_y, var0.max_z);
      var1.draw();
      GL11.glEnable(3553);
   }

   // $FF: renamed from: game (net.minecraft.client.game.game.i) void
   public final void intialize(ModelManager var1) {
      this.manager = var1;
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals, float, float, float, float) void
   public final void render_model_effects(Entity ent, float x, float y, float z, float _dt) {
      float var12;
      float var18;
      float var19;
      int var29;
      float var34;
      float var36;
      if (this.field_222 > 0.0F) {
         float kinda_speed = this.manager.kinda_distance(x, y, z);
         kinda_speed = (1.0F - kinda_speed / 256.0F) * this.field_223;
         if (kinda_speed > 0.0F) {
            float s = kinda_speed;
            float mx = z;
            float my = y;
            float mz = x;
            ModelRenderer model = this;
            GL11.glEnable(3042);
            ResourceManager var10 = this.manager.maybe_resource_manager;
            ResourceManager.bind_texture(this.manager.maybe_resource_manager.load_texture("%%/shadow.png"));
            World var11 = this.manager.world;
            GL11.glDepthMask(false);
            var12 = this.field_222;

            for(var29 = (int)(x - var12); var29 <= (int)(mz + var12); ++var29) {
               for(int var13 = (int)(my - 2.0F); var13 <= (int)my; ++var13) {
                  for(int var14 = (int)(mx - var12); var14 <= (int)(mx + var12); ++var14) {
                     int var15;
                     if ((var15 = var11.block_at(var29, var13 - 1, var14)) > 0 && var11.method_195(var29, var13, var14) > 3) {
                        Block var16 = Block.blocks[var15];
                        GeometryDrawer var25 = GeometryDrawer.singleton;
                        if ((var34 = (s - (my - (float)var13) / 2.0F) * 0.5F * model.manager.world.light_at(var29, var13, var14)) >= 0.0F) {
                           GL11.glColor4f(1.0F, 1.0F, 1.0F, var34);
                           var25.quads_mode();
                           var34 = (float)var29 + var16.field_934;
                           var18 = (float)var29 + var16.field_937;
                           float var20 = (float)var13 + var16.field_935;
                           float var21 = (float)var14 + var16.field_936;
                           var36 = (float)var14 + var16.field_939;
                           float var22 = (mz - var34) / 2.0F / var12 + 0.5F;
                           float var17 = (mz - var18) / 2.0F / var12 + 0.5F;
                           float var23 = (mx - var21) / 2.0F / var12 + 0.5F;
                           var19 = (mx - var36) / 2.0F / var12 + 0.5F;
                           var25.add_vertex(var34, var20, var21, var22, var23);
                           var25.add_vertex(var34, var20, var36, var22, var19);
                           var25.add_vertex(var18, var20, var36, var17, var19);
                           var25.add_vertex(var18, var20, var21, var17, var23);
                           var25.draw();
                        }
                     }
                  }
               }
            }

            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            GL11.glDisable(3042);
            GL11.glDepthMask(true);
         }
      }

      if (ent.burn_time > 0) {
         GL11.glDisable(2896);
         int var26 = Block.fire.maybeDropCount;
         var29 = (Block.fire.maybeDropCount & 15) << 4;
         int var30 = var26 & 240;
         var12 = (float)var29 / 256.0F;
         float var31 = ((float)var29 + 15.99F) / 256.0F;
         float var32 = (float)var30 / 256.0F;
         float var33 = ((float)var30 + 15.99F) / 256.0F;
         GL11.glPushMatrix();
         GL11.glTranslatef(x, y, z);
         GL11.glScalef(var34 = ent.width * 1.4F, var34, var34);
         this.bind_texture("/terrain.png");
         GeometryDrawer var35 = GeometryDrawer.singleton;
         var36 = 1.0F;
         var18 = 0.0F;
         var19 = ent.height / ent.width;
         GL11.glRotatef(-this.manager.yaw, 0.0F, 1.0F, 0.0F);
         GL11.glTranslatef(0.0F, 0.0F, 0.4F + (float)((int)var19) * 0.02F);
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         var35.quads_mode();

         while(var19 > 0.0F) {
            var35.add_vertex(var36 - 0.5F, 0.0F - var18, 0.0F, var31, var33);
            var35.add_vertex(-0.5F, 0.0F - var18, 0.0F, var12, var33);
            var35.add_vertex(-0.5F, 1.4F - var18, 0.0F, var12, var32);
            var35.add_vertex(var36 - 0.5F, 1.4F - var18, 0.0F, var31, var32);
            --var19;
            --var18;
            var36 *= 0.9F;
            GL11.glTranslatef(0.0F, 0.0F, -0.04F);
         }

         var35.draw();
         GL11.glPopMatrix();
         GL11.glEnable(2896);
      }

   }
}
