package net.minecraft.client.rendering;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.awt.image.ImageObserver;

// $FF: renamed from: net.minecraft.client.game.entities
public class KindofImage {
   // $FF: renamed from: game int[]
   private int[] field_605;
   // $FF: renamed from: animals int
   private int field_606;
   // $FF: renamed from: entities int
   private int field_607;

   // $FF: renamed from: game (java.awt.image.BufferedImage) java.awt.image.BufferedImage
   public BufferedImage method_340(BufferedImage var1) {
      this.field_606 = 64;
      this.field_607 = 32;
      BufferedImage var2;
      Graphics var3;
      (var3 = (var2 = new BufferedImage(this.field_606, this.field_607, 2)).getGraphics()).drawImage(var1, 0, 0, (ImageObserver)null);
      var3.dispose();
      this.field_605 = ((DataBufferInt)var2.getRaster().getDataBuffer()).getData();
      this.method_342(0, 0, 32, 16);
      this.method_341(32, 0, 64, 32);
      this.method_342(0, 16, 64, 32);
      return var2;
   }

   // $FF: renamed from: game (int, int, int, int) void
   private void method_341(int var1, int var2, int var3, int var4) {
      byte var5 = 32;
      byte var11 = 64;
      byte var10 = 0;
      byte var9 = 32;
      KindofImage var8 = this;
      var2 = var9;

      boolean var10000;
      label43:
      while(true) {
         if (var2 >= var11) {
            var10000 = false;
            break;
         }

         for(int var6 = var10; var6 < var5; ++var6) {
            if (var8.field_605[var2 + var6 * var8.field_606] >>> 24 < 128) {
               var10000 = true;
               break label43;
            }
         }

         ++var2;
      }

      if (!var10000) {
         for(var1 = 32; var1 < 64; ++var1) {
            for(var2 = 0; var2 < 32; ++var2) {
               this.field_605[var1 + var2 * this.field_606] &= 16777215;
            }
         }

      }
   }

   // $FF: renamed from: animals (int, int, int, int) void
   private void method_342(int var1, int var2, int var3, int var4) {
      for(var1 = 0; var1 < var3; ++var1) {
         for(int var5 = var2; var5 < var4; ++var5) {
            this.field_605[var1 + var5 * this.field_606] |= -16777216;
         }
      }

   }
}
