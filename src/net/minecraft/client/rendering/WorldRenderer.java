package net.minecraft.client.rendering;

import a.MathHelper;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import net.minecraft.game.world.World;
import net.minecraft.game.world.IRenderer;
import net.minecraft.game.world.HitboxTree;
import net.minecraft.game.world.block.Block;
import net.minecraft.game.items.ItemStack;
import net.minecraft.game.entities.Entity;
import net.minecraft.game.entities.player.Player;
import net.minecraft.game.util.RaycastResult;
import net.minecraft.game.util.AABB;
import net.minecraft.game.util.Vector;
import net.minecraft.client.Game;
import net.minecraft.client.rendering.models.ModelManager;
import net.minecraft.client.rendering.b.ICuller;
import net.minecraft.client.particles.class_10;
import net.minecraft.client.particles.class_12;
import net.minecraft.client.particles.class_13;
import net.minecraft.client.particles.class_16;
import net.minecraft.client.particles.class_8;
import net.minecraft.client.particles.class_9;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBOcclusionQuery;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.opengl.NVOcclusionQuery;

// $FF: renamed from: net.minecraft.client.game.animals
public final class WorldRenderer implements IRenderer {
   // $FF: renamed from: animals net.minecraft.game.game.player
   private World world;
   // $FF: renamed from: entities net.minecraft.client.game.k
   private ResourceManager resource_manager;
   // $FF: renamed from: projectiles int
   private int some_render_list;
   // $FF: renamed from: player java.nio.IntBuffer
   private IntBuffer some_buffer = BufferUtils.createIntBuffer(65536);
   // $FF: renamed from: particles java.util.List
   private List field_628 = new ArrayList();
   // $FF: renamed from: player net.minecraft.client.game.pos_x[]
   private RenderChunk[] render_chunks_b;
   // $FF: renamed from: h net.minecraft.client.game.pos_x[]
   private RenderChunk[] render_chunks_a;
   // $FF: renamed from: i int
   private int chunks_x;
   // $FF: renamed from: j int
   private int chunks_y;
   // $FF: renamed from: k int
   private int chunks_z;
   // $FF: renamed from: l int
   private int display_list;
   // $FF: renamed from: m net.minecraft.client.projectiles
   private Game game;
   // $FF: renamed from: n net.minecraft.client.game.particles
   private BlockRenderer block_renderer;
   // $FF: renamed from: o java.nio.IntBuffer
   private IntBuffer occlusion_query_ids;
   // $FF: renamed from: p boolean
   private boolean do_occlusion = false;
   // $FF: renamed from: q int
   private int field_639 = 0;
   // $FF: renamed from: r int
   private int field_640;
   // $FF: renamed from: s int
   private int total_entities_count;
   // $FF: renamed from: t int
   private int rendered_entities_count;
   // $FF: renamed from: u int
   private int removed_entities;
   // $FF: renamed from: v int[]
   private int[] field_644 = new int['썐'];
   // $FF: renamed from: w java.nio.IntBuffer
   private IntBuffer occlusion_buffer = BufferUtils.createIntBuffer(64);
   // $FF: renamed from: x int
   private int total_chunks;
   // $FF: renamed from: y int
   private int total_loaded_chunks_all_passes;
   // $FF: renamed from: z int
   private int occluded_count_all_passes;
   // $FF: renamed from: A int
   private int chunks_rendered;
   // $FF: renamed from: B float
   private float field_650 = -9999.0F;
   // $FF: renamed from: C float
   private float field_651 = -9999.0F;
   // $FF: renamed from: D float
   private float field_652 = -9999.0F;
   // $FF: renamed from: game float
   public float field_653;

   public WorldRenderer(Game game, ResourceManager rm) {
      this.game = game;
      this.resource_manager = rm;
      this.some_render_list = GL11.glGenLists(2);
      this.display_list = GL11.glGenLists(786432);
      this.do_occlusion = GLContext.getCapabilities().GL_ARB_occlusion_query;
      if (this.do_occlusion) {
         this.occlusion_buffer.clear();
         GL11.glGetInteger(ARBOcclusionQuery.GL_QUERY_COUNTER_BITS_ARB, this.occlusion_buffer);
         if (this.occlusion_buffer.get(0) == 0) {
            this.do_occlusion = false;
         } else {
            this.occlusion_query_ids = BufferUtils.createIntBuffer(262144);
            this.occlusion_query_ids.clear();
            this.occlusion_query_ids.position(0);
            this.occlusion_query_ids.limit(262144);
            ARBOcclusionQuery.glGenQueriesARB(this.occlusion_query_ids);
         }
      }

      this.field_640 = GL11.glGenLists(1);
      GL11.glNewList(this.field_640, 4864);
      Random var5 = new Random(10842L);

      for(int var6 = 0; var6 < 500; ++var6) {
         GL11.glRotatef(var5.nextFloat() * 360.0F, 1.0F, 0.0F, 0.0F);
         GL11.glRotatef(var5.nextFloat() * 360.0F, 0.0F, 1.0F, 0.0F);
         GL11.glRotatef(var5.nextFloat() * 360.0F, 0.0F, 0.0F, 1.0F);
         GeometryDrawer var3 = GeometryDrawer.singleton;
         float var4 = 0.25F + var5.nextFloat() * 0.25F;
         var3.quads_mode();
         var3.add_vertex(-var4, -100.0F, var4, 1.0F, 1.0F);
         var3.add_vertex(var4, -100.0F, var4, 0.0F, 1.0F);
         var3.add_vertex(var4, -100.0F, -var4, 0.0F, 0.0F);
         var3.add_vertex(-var4, -100.0F, -var4, 1.0F, 0.0F);
         var3.draw();
      }

      GL11.glEndList();
   }

   // $FF: renamed from: game (net.minecraft.game.game.player) void
   public final void set_world(World world) {
      if (this.world != null) {
         this.world.remove_renderer(this);
      }

      this.field_650 = -9999.0F;
      this.field_651 = -9999.0F;
      this.field_652 = -9999.0F;
      ModelManager.singleton.set_world(world);
      this.world = world;
      this.block_renderer = new BlockRenderer(world);
      if (world != null) {
         world.add_renderer(this);
         this.update_render_chunks();
      }

   }

   // $FF: renamed from: game () void
   public final void update_render_chunks() {
      int i;
      if (this.render_chunks_a != null) {
         for(i = 0; i < this.render_chunks_a.length; ++i) {
            this.render_chunks_a[i].reset();
         }
      }

      this.chunks_x = this.world.width / 16;
      this.chunks_y = this.world.height / 16;
      this.chunks_z = this.world.depth / 16;
      this.render_chunks_a = new RenderChunk[this.chunks_x * this.chunks_y * this.chunks_z];
      this.render_chunks_b = new RenderChunk[this.chunks_x * this.chunks_y * this.chunks_z];
      i = 0;
      int chunk_id = 0;

      int x;
      for(x = 0; x < this.chunks_x; ++x) {
         for(int y = 0; y < this.chunks_y; ++y) {
            for(int z = 0; z < this.chunks_z; ++z) {
               this.render_chunks_a[(z * this.chunks_y + y) * this.chunks_x + x] = new RenderChunk(this.world, x << 4, y << 4, z << 4, 16, this.display_list + i);
               if (this.do_occlusion) {
                  this.render_chunks_a[(z * this.chunks_y + y) * this.chunks_x + x].occlusion_id = this.occlusion_query_ids.get(chunk_id);
               }

               ++chunk_id;
               this.render_chunks_b[(z * this.chunks_y + y) * this.chunks_x + x] = this.render_chunks_a[(z * this.chunks_y + y) * this.chunks_x + x];
               i += 3;
            }
         }
      }

      for(x = 0; x < this.field_628.size(); ++x) {
         ((RenderChunk)this.field_628.get(x)).field_739 = false;
      }

      this.field_628.clear();
      GL11.glNewList(this.some_render_list, 4864);
      this.method_359();
      GL11.glEndList();
      GL11.glNewList(this.some_render_list + 1, 4864);
      this.method_361();
      GL11.glEndList();
      this.method_365(0, 0, 0, this.world.width, this.world.height, this.world.depth);
   }

   // $FF: renamed from: game (net.minecraft.game.projectiles.game, net.minecraft.client.game.animals.player, float) void
   public final void render_entities(Vector vec, ICuller allowance, float dt) {
      HitboxTree hittree = this.world.hitbox_tree;
      ModelManager.singleton.set_player(this.world, this.resource_manager, this.game.player, dt);
      this.total_entities_count = 0;
      this.rendered_entities_count = 0;
      this.removed_entities = 0;

      for(int i = 0; i < hittree.width; ++i) {
         for(int j = 0; j < hittree.height; ++j) {
            for(int k = 0; k < hittree.depth; ++k) {
               List members = hittree.entity_chunks[(k * hittree.height + j) * hittree.width + i];
               if (members.size() != 0) {
                  int cx = (i << 3) + 4;
                  int cy = (j << 3) + 4;
                  int cz = (k << 3) + 4;
                  this.total_entities_count += members.size();
                  float cxf = (float)cx;
                  float cyf = (float)cy;
                  float czf = (float)cz;
                  float cyf2 = cyf;
                  float cxf2 = cxf;
                  boolean should_render;
                  if (cxf2 >= 0.0F
                          && cyf2 >= 0.0F
                          && czf >= 0.0F
                          && cxf2 < (float)this.world.width
                          && cyf2 < (float)this.world.height
                          && czf < (float)this.world.depth) {

                     int rx = (int)(cxf2 / 16.0F);
                     int ry = (int)(cyf2 / 16.0F);
                     int rz = (int)(czf / 16.0F);
                     should_render = this.render_chunks_a[(rz * this.chunks_y + ry) * this.chunks_x + rx].maybe_is_visible
                             && this.render_chunks_a[(rz * this.chunks_y + ry) * this.chunks_x + rx].maybe_is_visible_2;
                  } else {
                     should_render = true;
                  }

                  if (!should_render) {
                     this.removed_entities += members.size();
                  } else {
                     for(int m = 0; m < members.size(); ++m) {
                        Entity ent1;
                        Entity ent2;
                        cyf2 = (ent2 = ent1 = (Entity)members.get(m)).pos_x - vec.x;
                        czf = ent2.pos_y - vec.y;
                        cxf2 = ent2.pos_z - vec.z;
                        cyf2 = cyf2 * cyf2 + czf * czf + cxf2 * cxf2;
                        float var17 = cyf2;
                        AABB var19;
                        cyf2 = (var19 = ent2.hitbox).max_x - var19.min_x;
                        czf = var19.max_y - var19.min_y;
                        cxf2 = var19.max_z - var19.min_z;
                        cxf2 = (cyf2 + czf + cxf2) / 3.0F * 64.0F;
                        if (var17 < cxf2 * cxf2 && allowance.is_visible(ent1.hitbox) && (ent1 != this.world.player || this.game.options.third_person_mode)) {
                           ++this.rendered_entities_count;
                           ModelManager.singleton.render_entity(ent1, dt);
                        }
                     }
                  }
               }
            }
         }
      }

   }

   // $FF: renamed from: animals () java.lang.String
   public final String chunk_rendering_debug_info() {
      return "C: " + this.chunks_rendered + "/" + this.total_chunks
              + ". F: " + this.total_loaded_chunks_all_passes
              + ", O: " + this.occluded_count_all_passes;
   }

   // $FF: renamed from: entities () java.lang.String
   public final String entity_rendering_debug_info() {
      return "E: " + this.rendered_entities_count + "/" + this.total_entities_count
              + ". B: " + this.removed_entities
              + ", I: " + (this.total_entities_count - this.removed_entities - this.rendered_entities_count);
   }

   // $FF: renamed from: game (net.minecraft.game.entities.player.game, int) int
   public final int do_occlusion_queries(Player player, int phase) {
      if (phase == 0) {
         this.total_chunks = 0;
         this.total_loaded_chunks_all_passes = 0;
         this.occluded_count_all_passes = 0;
         this.chunks_rendered = 0;
      }

      float var3 = player.pos_x - this.field_650;
      float var4 = player.pos_y - this.field_651;
      float var5 = player.pos_z - this.field_652;
      if (var3 * var3 + var4 * var4 + var5 * var5 > 16.0F) {
         this.field_650 = player.pos_x;
         this.field_651 = player.pos_y;
         this.field_652 = player.pos_z;
         Arrays.sort(this.render_chunks_b, new RenderChunkDistanceComparator(player));
      }

      int var8;
      if (this.do_occlusion && phase == 0) {
         int var10 = 8;
         this.method_353(0, 8);

         int var6;
         for(var6 = 0; var6 < 8; ++var6) {
            this.render_chunks_b[var6].maybe_is_visible_2 = true;
         }

         var8 = 0 + this.method_354(0, 8, phase);
         do {
            int var9 = var10;
            if ((var10 <<= 1) > this.render_chunks_b.length) {
               var10 = this.render_chunks_b.length;
            }

            GL11.glDisable(3553);
            GL11.glDisable(2896);
            GL11.glDisable(3008);
            GL11.glColorMask(false, false, false, false);
            GL11.glDepthMask(false);
            this.method_353(var9, var10);
            for(var6 = var9; var6 < var10; ++var6) {
               if (!this.render_chunks_b[var6].maybe_is_visible) {
                  this.render_chunks_b[var6].maybe_is_visible_2 = true;
               }

               if (this.render_chunks_b[var6].maybe_is_visible && !this.render_chunks_b[var6].field_743) {
                  float var7 = MathHelper.sqrt(this.render_chunks_b[var6].distance(player));
                  int var11 = (int)(1.0F + var7 / 64.0F);
                  if (this.field_639 % var11 == var6 % var11) {
                     ARBOcclusionQuery.glBeginQueryARB(ARBOcclusionQuery.GL_SAMPLES_PASSED_ARB, this.render_chunks_b[var6].occlusion_id);
                     this.render_chunks_b[var6].method_427();
                     ARBOcclusionQuery.glEndQueryARB(ARBOcclusionQuery.GL_SAMPLES_PASSED_ARB);
                     this.render_chunks_b[var6].field_743 = true;
                  }
               }
            }

            GL11.glColorMask(true, true, true, true);
            GL11.glDepthMask(true);
            GL11.glEnable(3553);
            GL11.glEnable(3008);
            var8 += this.method_354(var9, var10, phase);
         } while(var10 < this.render_chunks_b.length);
      } else {
         var8 = 0 + this.method_354(0, this.render_chunks_b.length, phase);
      }

      return var8;
   }

   // $FF: renamed from: game (int, int) void
   private void method_353(int var1, int var2) {
      for(var1 = var1; var1 < var2; ++var1) {
         if (this.render_chunks_b[var1].field_743) {
            this.occlusion_buffer.clear();
            ARBOcclusionQuery.glGetQueryObjectuARB(this.render_chunks_b[var1].occlusion_id, NVOcclusionQuery.GL_PIXEL_COUNT_AVAILABLE_NV, this.occlusion_buffer);
            if (this.occlusion_buffer.get(0) != 0) {
               this.render_chunks_b[var1].field_743 = false;
               this.occlusion_buffer.clear();
               ARBOcclusionQuery.glGetQueryObjectuARB(this.render_chunks_b[var1].occlusion_id, NVOcclusionQuery.GL_PIXEL_COUNT_NV, this.occlusion_buffer);
               this.render_chunks_b[var1].maybe_is_visible_2 = this.occlusion_buffer.get(0) != 0;
            }
         }
      }

   }

   // $FF: renamed from: animals (int, int, int) int
   private int method_354(int var1, int var2, int var3) {
      int var4 = 0;

      for(var1 = var1; var1 < var2; ++var1) {
         if (var3 == 0) {
            ++this.total_chunks;
            if (!this.render_chunks_b[var1].maybe_is_visible) {
               ++this.total_loaded_chunks_all_passes;
            }

            if (this.render_chunks_b[var1].maybe_is_visible && !this.render_chunks_b[var1].maybe_is_visible_2) {
               ++this.occluded_count_all_passes;
            }

            if (this.render_chunks_b[var1].maybe_is_visible && this.render_chunks_b[var1].maybe_is_visible_2) {
               ++this.chunks_rendered;
            }
         }

         if (this.render_chunks_b[var1].maybe_is_visible && this.render_chunks_b[var1].maybe_is_visible_2) {
            var4 = this.render_chunks_b[var1].method_425(this.field_644, var4, var3);
         }
      }

      this.some_buffer.clear();
      this.some_buffer.put(this.field_644, 0, var4);
      this.some_buffer.flip();
      if (this.some_buffer.remaining() > 0) {
         GL11.glCallLists(this.some_buffer);
      }

      return this.some_buffer.remaining();
   }

   // $FF: renamed from: projectiles () void
   public final void maybe_render_ground() {
      GL11.glBindTexture(3553, this.resource_manager.load_texture("/terrain.png"));
      GL11.glCallLists(this.some_buffer);
   }

   // $FF: renamed from: player () void
   public final void method_356() {
      ++this.field_639;
   }

   // $FF: renamed from: game (float) void
   public final void method_357(float var1) {
      GL11.glDisable(3553);
      Vector var2;
      float var3 = (var2 = this.world.sky_color(var1)).x;
      float var4 = var2.y;
      float var9 = var2.z;
      if (this.game.options.anaglyph_3d) {
         float var5 = (var3 * 30.0F + var4 * 59.0F + var9 * 11.0F) / 100.0F;
         var4 = (var3 * 30.0F + var4 * 70.0F) / 100.0F;
         var9 = (var3 * 30.0F + var9 * 70.0F) / 100.0F;
         var3 = var5;
         var4 = var4;
         var9 = var9;
      }

      GL11.glDepthMask(false);
      GeometryDrawer var12 = GeometryDrawer.singleton;
      GeometryDrawer.singleton.quads_mode();
      var12.set_color(var3, var4, var9);
      var9 = (float)(this.world.height + 10);

      int var10;
      for(var10 = -2048; var10 < this.world.width + 2048; var10 += 512) {
         for(int var13 = -2048; var13 < this.world.depth + 2048; var13 += 512) {
            var12.add_vertex((float)var10, var9, (float)var13);
            var12.add_vertex((float)(var10 + 512), var9, (float)var13);
            var12.add_vertex((float)(var10 + 512), var9, (float)(var13 + 512));
            var12.add_vertex((float)var10, var9, (float)(var13 + 512));
         }
      }

      var12.draw();
      GL11.glEnable(3553);
      GL11.glDisable(2912);
      GL11.glDisable(3008);
      GL11.glEnable(3042);
      GL11.glBlendFunc(1, 1);
      GL11.glPushMatrix();
      var3 = this.world.player.chunk_x + (this.world.player.pos_x - this.world.player.chunk_x) * var1;
      var4 = this.world.player.chunk_y + (this.world.player.pos_y - this.world.player.chunk_y) * var1;
      float var6 = this.world.player.chunk_z + (this.world.player.pos_z - this.world.player.chunk_z) * var1;
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GL11.glTranslatef(var3, var4, var6);
      GL11.glRotatef(0.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(this.world.method_175(var1) * 360.0F, 1.0F, 0.0F, 0.0F);
      GL11.glBindTexture(3553, this.resource_manager.load_texture("/terrain/sun.png"));
      var12.quads_mode();
      var12.add_vertex(-30.0F, 100.0F, -30.0F, 0.0F, 0.0F);
      var12.add_vertex(30.0F, 100.0F, -30.0F, 1.0F, 0.0F);
      var12.add_vertex(30.0F, 100.0F, 30.0F, 1.0F, 1.0F);
      var12.add_vertex(-30.0F, 100.0F, 30.0F, 0.0F, 1.0F);
      var12.draw();
      GL11.glBindTexture(3553, this.resource_manager.load_texture("/terrain/moon.png"));
      var12.quads_mode();
      var12.add_vertex(-20.0F, -100.0F, 20.0F, 1.0F, 1.0F);
      var12.add_vertex(20.0F, -100.0F, 20.0F, 0.0F, 1.0F);
      var12.add_vertex(20.0F, -100.0F, -20.0F, 0.0F, 0.0F);
      var12.add_vertex(-20.0F, -100.0F, -20.0F, 1.0F, 0.0F);
      var12.draw();
      GL11.glDisable(3553);
      GL11.glColor4f(var9 = this.world.method_173(var1), var9, var9, var9);
      GL11.glCallList(this.field_640);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GL11.glEnable(3553);
      GL11.glDisable(3042);
      GL11.glEnable(3008);
      GL11.glEnable(2912);
      GL11.glPopMatrix();
      GL11.glDepthMask(true);
      GL11.glBindTexture(3553, this.resource_manager.load_texture("/clouds.png"));
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      Vector var11;
      var4 = (var11 = this.world.cloud_color(var1)).x;
      var6 = var11.y;
      var3 = var11.z;
      float var7;
      if (this.game.options.anaglyph_3d) {
         var9 = (var4 * 30.0F + var6 * 59.0F + var3 * 11.0F) / 100.0F;
         var7 = (var4 * 30.0F + var6 * 70.0F) / 100.0F;
         var3 = (var4 * 30.0F + var3 * 70.0F) / 100.0F;
         var4 = var9;
         var6 = var7;
         var3 = var3;
      }

      var9 = (float)this.world.cloud_height;
      var7 = ((float)this.field_639 + var1) * 4.8828125E-4F * 0.03F;
      var12.quads_mode();
      var12.set_color(var4, var6, var3);

      for(int var8 = -2048; var8 < this.world.width + 2048; var8 += 512) {
         for(var10 = -2048; var10 < this.world.depth + 2048; var10 += 512) {
            var12.add_vertex((float)var8, var9, (float)(var10 + 512), (float)var8 * 4.8828125E-4F + var7, (float)(var10 + 512) * 4.8828125E-4F);
            var12.add_vertex((float)(var8 + 512), var9, (float)(var10 + 512), (float)(var8 + 512) * 4.8828125E-4F + var7, (float)(var10 + 512) * 4.8828125E-4F);
            var12.add_vertex((float)(var8 + 512), var9, (float)var10, (float)(var8 + 512) * 4.8828125E-4F + var7, (float)var10 * 4.8828125E-4F);
            var12.add_vertex((float)var8, var9, (float)var10, (float)var8 * 4.8828125E-4F + var7, (float)var10 * 4.8828125E-4F);
            var12.add_vertex((float)var8, var9, (float)var10, (float)var8 * 4.8828125E-4F + var7, (float)var10 * 4.8828125E-4F);
            var12.add_vertex((float)(var8 + 512), var9, (float)var10, (float)(var8 + 512) * 4.8828125E-4F + var7, (float)var10 * 4.8828125E-4F);
            var12.add_vertex((float)(var8 + 512), var9, (float)(var10 + 512), (float)(var8 + 512) * 4.8828125E-4F + var7, (float)(var10 + 512) * 4.8828125E-4F);
            var12.add_vertex((float)var8, var9, (float)(var10 + 512), (float)var8 * 4.8828125E-4F + var7, (float)(var10 + 512) * 4.8828125E-4F);
         }
      }

      var12.draw();
   }

   // $FF: renamed from: particles () void
   public final void render_grass() {
      float var1 = this.world.light_at(0, this.world.method_181(), 0);
      GL11.glBindTexture(3553, this.resource_manager.load_texture("/dirt.png"));
      if (this.world.method_181() > this.world.water_height() && this.world.surrounding_water_type == Block.water.block_id) {
         GL11.glBindTexture(3553, this.resource_manager.load_texture("/grass.png"));
      }

      GL11.glColor4f(var1, var1, var1, 1.0F);
      GL11.glEnable(3553);
      GL11.glCallList(this.some_render_list);
   }

   // $FF: renamed from: i () void
   private void method_359() {
      GeometryDrawer var1 = GeometryDrawer.singleton;
      float var2 = (float)this.world.method_181();
      int var3 = 128;
      if (128 > this.world.width) {
         var3 = this.world.width;
      }

      if (var3 > this.world.depth) {
         var3 = this.world.depth;
      }

      int var4 = 2048 / var3;
      var1.quads_mode();

      for(int var5 = -var3 * var4; var5 < this.world.width + var3 * var4; var5 += var3) {
         for(int var6 = -var3 * var4; var6 < this.world.depth + var3 * var4; var6 += var3) {
            if (var2 < 0.0F || var5 < 0 || var6 < 0 || var5 >= this.world.width || var6 >= this.world.depth) {
               var1.add_vertex((float)var5, var2, (float)(var6 + var3), 0.0F, (float)var3);
               var1.add_vertex((float)(var5 + var3), var2, (float)(var6 + var3), (float)var3, (float)var3);
               var1.add_vertex((float)(var5 + var3), var2, (float)var6, (float)var3, 0.0F);
               var1.add_vertex((float)var5, var2, (float)var6, 0.0F, 0.0F);
            }
         }
      }

      var1.draw();
   }

   // $FF: renamed from: player () void
   public final void render_surrounding_water() {
      GL11.glEnable(GL11.GL_TEXTURE_2D);
      GL11.glEnable(GL11.GL_BLEND);
      GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.resource_manager.load_texture("/water.png"));
      float var1 = this.world.light_at(0, this.world.water_height(), 0);
      GL11.glColor4f(var1, var1, var1, 1.0F);
      GL11.glCallList(this.some_render_list + 1);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GL11.glDisable(GL11.GL_BLEND);
   }

   // $FF: renamed from: j () void
   private void method_361() {
      float var1 = (float)this.world.water_height();
      GL11.glBlendFunc(770, 771);
      GeometryDrawer var2 = GeometryDrawer.singleton;
      int var3 = 128;
      if (128 > this.world.width) {
         var3 = this.world.width;
      }

      if (var3 > this.world.depth) {
         var3 = this.world.depth;
      }

      int var4 = 2048 / var3;
      var2.quads_mode();
      float var5 = Block.water.field_934;
      float var6 = Block.water.field_936;

      for(int var7 = -var3 * var4; var7 < this.world.width + var3 * var4; var7 += var3) {
         for(int var8 = -var3 * var4; var8 < this.world.depth + var3 * var4; var8 += var3) {
            float var9 = var1 + Block.water.field_935;
            if (var1 < 0.0F || var7 < 0 || var8 < 0 || var7 >= this.world.width || var8 >= this.world.depth) {
               var2.add_vertex((float)var7 + var5, var9, (float)(var8 + var3) + var6, 0.0F, (float)var3);
               var2.add_vertex((float)(var7 + var3) + var5, var9, (float)(var8 + var3) + var6, (float)var3, (float)var3);
               var2.add_vertex((float)(var7 + var3) + var5, var9, (float)var8 + var6, (float)var3, 0.0F);
               var2.add_vertex((float)var7 + var5, var9, (float)var8 + var6, 0.0F, 0.0F);
               var2.add_vertex((float)var7 + var5, var9, (float)var8 + var6, 0.0F, 0.0F);
               var2.add_vertex((float)(var7 + var3) + var5, var9, (float)var8 + var6, (float)var3, 0.0F);
               var2.add_vertex((float)(var7 + var3) + var5, var9, (float)(var8 + var3) + var6, (float)var3, (float)var3);
               var2.add_vertex((float)var7 + var5, var9, (float)(var8 + var3) + var6, 0.0F, (float)var3);
            }
         }
      }

      var2.draw();
      GL11.glDisable(3042);
   }

   // $FF: renamed from: game (net.minecraft.game.entities.player.game) void
   public final void method_362(Player player) {
      Collections.sort(this.field_628, new SomethingComparer(player));
      int var2 = this.field_628.size() - 1;
      int var3 = this.field_628.size();

      for(int i = 0; i < var3; ++i) {
         RenderChunk var5 = (RenderChunk)this.field_628.get(var2 - i);
         if (var5.distance(player) > 2500.0F && i > 4) {
            return;
         }

         this.field_628.remove(var5);
         var5.method_421();
         var5.field_739 = false;
      }

   }

   // $FF: renamed from: game (net.minecraft.game.projectiles.entities, int, net.minecraft.game.animals.k) void
   public final void method_363(RaycastResult hit, int var2, ItemStack var3) {
      GeometryDrawer draver = GeometryDrawer.singleton;
      GL11.glEnable(3042);
      GL11.glEnable(3008);
      GL11.glBlendFunc(770, 1);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, (MathHelper.sin((float)System.currentTimeMillis() / 100.0F) * 0.2F + 0.4F) * 0.5F);
      if (this.field_653 > 0.0F) {
         GL11.glBlendFunc(774, 768);
         int var5 = this.resource_manager.load_texture("/terrain.png");
         GL11.glBindTexture(3553, var5);
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 0.5F);
         GL11.glPushMatrix();
         int block_id = this.world.block_at(hit.x, hit.y, hit.z );
         Block block = block_id > 0 ? Block.blocks[block_id] : null;
         GL11.glDisable(3008);
         draver.quads_mode();
         draver.colorless();
         if (block == null) {
            block = Block.stone;
         }

         this.block_renderer.method_407(block, hit.x, hit.y, hit.z, 240 + (int)(this.field_653 * 10.0F));
         draver.draw();
         GL11.glEnable(3008);
         GL11.glDepthMask(true);
         GL11.glPopMatrix();
      }

      GL11.glDisable(3042);
      GL11.glDisable(3008);
   }

   // $FF: renamed from: game (net.minecraft.game.projectiles.entities, int) void
   public final void method_364(RaycastResult var1, int var2) {
      GL11.glEnable(3042);
      GL11.glBlendFunc(770, 771);
      GL11.glColor4f(0.0F, 0.0F, 0.0F, 0.4F);
      GL11.glLineWidth(2.0F);
      GL11.glDisable(3553);
      GL11.glDepthMask(false);
      if ((var2 = this.world.block_at(var1.x, var1.y, var1.z)) > 0) {
         AABB var3 = Block.blocks[var2].method_501(var1.x, var1.y, var1.z).grow(0.002F, 0.002F, 0.002F);
         GeometryDrawer var4 = GeometryDrawer.singleton;
         GeometryDrawer.singleton.set_draw_mode(3);
         var4.add_vertex(var3.min_x, var3.min_y, var3.min_z);
         var4.add_vertex(var3.max_x, var3.min_y, var3.min_z);
         var4.add_vertex(var3.max_x, var3.min_y, var3.max_z);
         var4.add_vertex(var3.min_x, var3.min_y, var3.max_z);
         var4.add_vertex(var3.min_x, var3.min_y, var3.min_z);
         var4.draw();
         var4.set_draw_mode(3);
         var4.add_vertex(var3.min_x, var3.max_y, var3.min_z);
         var4.add_vertex(var3.max_x, var3.max_y, var3.min_z);
         var4.add_vertex(var3.max_x, var3.max_y, var3.max_z);
         var4.add_vertex(var3.min_x, var3.max_y, var3.max_z);
         var4.add_vertex(var3.min_x, var3.max_y, var3.min_z);
         var4.draw();
         var4.set_draw_mode(1);
         var4.add_vertex(var3.min_x, var3.min_y, var3.min_z);
         var4.add_vertex(var3.min_x, var3.max_y, var3.min_z);
         var4.add_vertex(var3.max_x, var3.min_y, var3.min_z);
         var4.add_vertex(var3.max_x, var3.max_y, var3.min_z);
         var4.add_vertex(var3.max_x, var3.min_y, var3.max_z);
         var4.add_vertex(var3.max_x, var3.max_y, var3.max_z);
         var4.add_vertex(var3.min_x, var3.min_y, var3.max_z);
         var4.add_vertex(var3.min_x, var3.max_y, var3.max_z);
         var4.draw();
      }

      GL11.glDepthMask(true);
      GL11.glEnable(3553);
      GL11.glDisable(3042);
   }

   // $FF: renamed from: animals (int, int, int, int, int, int) void
   private void method_365(int var1, int var2, int var3, int var4, int var5, int var6) {
      var1 /= 16;
      var2 /= 16;
      var3 /= 16;
      var4 /= 16;
      var5 /= 16;
      var6 /= 16;
      if (var1 < 0) {
         var1 = 0;
      }

      if (var2 < 0) {
         var2 = 0;
      }

      if (var3 < 0) {
         var3 = 0;
      }

      if (var4 > this.chunks_x - 1) {
         var4 = this.chunks_x - 1;
      }

      if (var5 > this.chunks_y - 1) {
         var5 = this.chunks_y - 1;
      }

      if (var6 > this.chunks_z - 1) {
         var6 = this.chunks_z - 1;
      }

      for(var1 = var1; var1 <= var4; ++var1) {
         for(int var7 = var2; var7 <= var5; ++var7) {
            for(int var8 = var3; var8 <= var6; ++var8) {
               RenderChunk var9 = this.render_chunks_a[(var8 * this.chunks_y + var7) * this.chunks_x + var1];
               if (!var9.field_739) {
                  var9.field_739 = true;
                  this.field_628.add(this.render_chunks_a[(var8 * this.chunks_y + var7) * this.chunks_x + var1]);
               }
            }
         }
      }

   }

   // $FF: renamed from: game (int, int, int) void
   public final void method_3(int var1, int var2, int var3) {
      this.method_365(var1 - 1, var2 - 1, var3 - 1, var1 + 1, var2 + 1, var3 + 1);
   }

   // $FF: renamed from: game (int, int, int, int, int, int) void
   public final void method_4(int var1, int var2, int var3, int var4, int var5, int var6) {
      this.method_365(var1 - 1, var2 - 1, var3 - 1, var4 + 1, var5 + 1, var6 + 1);
   }

   // $FF: renamed from: game (net.minecraft.client.game.animals.player) void
   public final void method_366(ICuller var1) {
      for(int var2 = 0; var2 < this.render_chunks_a.length; ++var2) {
         this.render_chunks_a[var2].method_426(var1);
      }

   }

   // $FF: renamed from: game (java.lang.String, float, float, float, float, float) void
   public final void method_6(String var1, float var2, float var3, float var4, float var5, float var6) {
      this.game.sound_manager.play_sound(var1, var2, var3, var4, var5, var6);
   }

   // $FF: renamed from: game (java.lang.String, float, float, float, float, float, float) void
   public final void method_7(String var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      float var8 = this.world.player.pos_x - var2;
      float var9 = this.world.player.pos_y - var3;
      float var10 = this.world.player.pos_z - var4;
      if (var8 * var8 + var9 * var9 + var10 * var10 <= 256.0F) {
         if (var1 == "bubble") {
            this.game.particle_manager.spawn_particle(new class_10(this.world, var2, var3, var4, var5, var6, var7));
         } else if (var1 == "smoke") {
            this.game.particle_manager.spawn_particle(new class_8(this.world, var2, var3, var4));
         } else if (var1 == "explode") {
            this.game.particle_manager.spawn_particle(new class_13(this.world, var2, var3, var4, var5, var6, var7));
         } else if (var1 == "flame") {
            this.game.particle_manager.spawn_particle(new class_9(this.world, var2, var3, var4));
         } else if (var1 == "lava") {
            this.game.particle_manager.spawn_particle(new class_12(this.world, var2, var3, var4));
         } else if (var1 == "splash") {
            this.game.particle_manager.spawn_particle(new class_16(this.world, var2, var3, var4));
         } else {
            if (var1 == "largesmoke") {
               this.game.particle_manager.spawn_particle(new class_8(this.world, var2, var3, var4, 2.5F));
            }

         }
      }
   }

   // $FF: renamed from: game (java.lang.String, float, float, float, float) void
   public final void method_8(String var1, float var2, float var3, float var4, float var5) {
      this.game.sound_manager.play_music(var2, var3, var4);
   }

   // $FF: renamed from: game (net.minecraft.game.entities.animals) void
   public final void method_9(Entity var1) {
      if (var1.texture_url != null) {
         this.resource_manager.method_395(var1.texture_url, new KindofImage());
      }

   }

   // $FF: renamed from: animals (net.minecraft.game.entities.animals) void
   public final void method_10(Entity var1) {
      if (var1.texture_url != null) {
         this.resource_manager.method_396(var1.texture_url);
      }

   }

   // $FF: renamed from: h () void
   public final void method_11() {
      GL11.glNewList(this.some_render_list, 4864);
      this.method_359();
      GL11.glEndList();
      GL11.glNewList(this.some_render_list + 1, 4864);
      this.method_361();
      GL11.glEndList();
   }
}
