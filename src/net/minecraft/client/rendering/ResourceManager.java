package net.minecraft.client.rendering;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;
import net.minecraft.client.GameOptions;
import net.minecraft.client.rendering.c.class_173;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.game.k
public class ResourceManager {
   // $FF: renamed from: game java.util.HashMap
   private HashMap field_698 = new HashMap();
   // $FF: renamed from: animals java.util.HashMap
   private HashMap field_699 = new HashMap();
   // $FF: renamed from: entities java.nio.IntBuffer
   private IntBuffer field_700 = BufferUtils.createIntBuffer(1);
   // $FF: renamed from: projectiles java.nio.ByteBuffer
   private ByteBuffer field_701 = BufferUtils.createByteBuffer(262144);
   // $FF: renamed from: player java.util.List
   private List field_702 = new ArrayList();
   // $FF: renamed from: particles java.util.Map
   private Map field_703 = new HashMap();
   // $FF: renamed from: player net.minecraft.client.q
   private GameOptions field_704;
   // $FF: renamed from: h boolean
   private boolean field_705 = false;

   public ResourceManager(GameOptions var1) {
      this.field_704 = var1;
   }

   // $FF: renamed from: game (java.lang.String) int
   public final int load_texture(String var1) {
      Integer var2;
      if ((var2 = (Integer)this.field_698.get(var1)) != null) {
         return var2.intValue();
      } else {
         try {
            this.field_700.clear();
            GL11.glGenTextures(this.field_700);
            int var4 = this.field_700.get(0);
            if (var1.startsWith("##")) {
               this.method_393(method_392(ImageIO.read(ResourceManager.class.getResourceAsStream(var1.substring(2)))), var4);
            } else if (var1.startsWith("%%")) {
               this.field_705 = true;
               this.method_393(ImageIO.read(ResourceManager.class.getResourceAsStream(var1.substring(2))), var4);
               this.field_705 = false;
            } else {
               this.method_393(ImageIO.read(ResourceManager.class.getResourceAsStream(var1)), var4);
            }

            this.field_698.put(var1, var4);
            return var4;
         } catch (IOException var3) {
            throw new RuntimeException("!!");
         }
      }
   }

   // $FF: renamed from: game (java.awt.image.BufferedImage) java.awt.image.BufferedImage
   private static BufferedImage method_392(BufferedImage var0) {
      int var1 = var0.getWidth() / 16;
      BufferedImage var2;
      Graphics var3 = (var2 = new BufferedImage(16, var0.getHeight() * var1, 2)).getGraphics();

      for(int var4 = 0; var4 < var1; ++var4) {
         var3.drawImage(var0, -var4 << 4, var4 * var0.getHeight(), (ImageObserver)null);
      }

      var3.dispose();
      return var2;
   }

   // $FF: renamed from: game (java.awt.image.BufferedImage, int) void
   private void method_393(BufferedImage var1, int var2) {
      GL11.glBindTexture(3553, var2);
      GL11.glTexParameteri(3553, 10241, 9728);
      GL11.glTexParameteri(3553, 10240, 9728);
      if (this.field_705) {
         GL11.glTexParameteri(3553, 10242, 10496);
         GL11.glTexParameteri(3553, 10243, 10496);
      } else {
         GL11.glTexParameteri(3553, 10242, 10497);
         GL11.glTexParameteri(3553, 10243, 10497);
      }

      var2 = var1.getWidth();
      int var3 = var1.getHeight();
      int[] var4 = new int[var2 * var3];
      byte[] var5 = new byte[var2 * var3 << 2];
      var1.getRGB(0, 0, var2, var3, var4, 0, var2);

      for(int var11 = 0; var11 < var4.length; ++var11) {
         int var6 = var4[var11] >>> 24;
         int var7 = var4[var11] >> 16 & 255;
         int var8 = var4[var11] >> 8 & 255;
         int var9 = var4[var11] & 255;
         if (this.field_704 != null && this.field_704.anaglyph_3d) {
            int var10 = (var7 * 30 + var8 * 59 + var9 * 11) / 100;
            var8 = (var7 * 30 + var8 * 70) / 100;
            var9 = (var7 * 30 + var9 * 70) / 100;
            var7 = var10;
            var8 = var8;
            var9 = var9;
         }

         var5[var11 << 2] = (byte)var7;
         var5[(var11 << 2) + 1] = (byte)var8;
         var5[(var11 << 2) + 2] = (byte)var9;
         var5[(var11 << 2) + 3] = (byte)var6;
      }

      this.field_701.clear();
      this.field_701.put(var5);
      this.field_701.position(0).limit(var5.length);
      GL11.glTexImage2D(3553, 0, 6408, var2, var3, 0, 6408, 5121, this.field_701);
   }

   // $FF: renamed from: game (java.lang.String, java.lang.String) int
   public final int method_394(String var1, String var2) {
      FetchedImage var6;
      if ((var6 = (FetchedImage)this.field_703.get(var1)) != null && var6.field_720 != null && !var6.field_723) {
         if (var6.field_722 < 0) {
            BufferedImage var4 = var6.field_720;
            this.field_700.clear();
            GL11.glGenTextures(this.field_700);
            int var5 = this.field_700.get(0);
            this.method_393(var4, var5);
            this.field_699.put(var5, var4);
            var6.field_722 = var5;
         } else {
            this.method_393(var6.field_720, var6.field_722);
         }

         var6.field_723 = true;
      }

      return var6 != null && var6.field_722 >= 0 ? var6.field_722 : this.load_texture(var2);
   }

   // $FF: renamed from: game (java.lang.String, net.minecraft.client.game.entities) net.minecraft.client.game.i
   public final FetchedImage method_395(String var1, KindofImage var2) {
      FetchedImage var3;
      if ((var3 = (FetchedImage)this.field_703.get(var1)) == null) {
         this.field_703.put(var1, new FetchedImage(var1, var2));
      } else {
         ++var3.field_721;
      }

      return var3;
   }

   // $FF: renamed from: animals (java.lang.String) void
   public final void method_396(String var1) {
      FetchedImage var2;
      if ((var2 = (FetchedImage)this.field_703.get(var1)) != null) {
         --var2.field_721;
         if (var2.field_721 == 0) {
            if (var2.field_722 >= 0) {
               int var3 = var2.field_722;
               this.field_699.remove(var3);
               this.field_700.clear();
               this.field_700.put(var3);
               this.field_700.flip();
               GL11.glDeleteTextures(this.field_700);
            }

            this.field_703.remove(var1);
         }
      }

   }

   // $FF: renamed from: game (net.minecraft.client.game.entities.animals) void
   public final void method_397(class_173 var1) {
      this.field_702.add(var1);
      var1.method_487();
   }

   // $FF: renamed from: game () void
   public final void method_398() {
      int var1;
      class_173 var2;
      for(var1 = 0; var1 < this.field_702.size(); ++var1) {
         (var2 = (class_173)this.field_702.get(var1)).field_830 = this.field_704.anaglyph_3d;
         var2.method_487();
         this.field_701.clear();
         this.field_701.put(var2.field_828);
         this.field_701.position(0).limit(var2.field_828.length);
         GL11.glTexSubImage2D(3553, 0, var2.field_829 % 16 << 4, var2.field_829 / 16 << 4, 16, 16, 6408, 5121, this.field_701);
      }

      for(var1 = 0; var1 < this.field_702.size(); ++var1) {
         if ((var2 = (class_173)this.field_702.get(var1)).field_831 > 0) {
            this.field_701.clear();
            this.field_701.put(var2.field_828);
            this.field_701.position(0).limit(var2.field_828.length);
            GL11.glBindTexture(3553, var2.field_831);
            GL11.glTexSubImage2D(3553, 0, 0, 0, 16, 16, 6408, 5121, this.field_701);
         }
      }

   }

   // $FF: renamed from: animals () void
   public final void method_399() {
      Iterator var1 = this.field_699.keySet().iterator();

      int var2;
      BufferedImage var3;
      while(var1.hasNext()) {
         var2 = ((Integer)var1.next()).intValue();
         var3 = (BufferedImage)this.field_699.get(var2);
         this.method_393(var3, var2);
      }

      for(var1 = this.field_703.values().iterator(); var1.hasNext(); ((FetchedImage)var1.next()).field_723 = false) {
         ;
      }

      var1 = this.field_698.keySet().iterator();

      while(var1.hasNext()) {
         String var5 = (String)var1.next();

         try {
            if (var5.startsWith("##")) {
               var3 = method_392(ImageIO.read(ResourceManager.class.getResourceAsStream(var5.substring(2))));
            } else if (var5.startsWith("%%")) {
               this.field_705 = true;
               var3 = ImageIO.read(ResourceManager.class.getResourceAsStream(var5.substring(2)));
               this.field_705 = false;
            } else {
               var3 = ImageIO.read(ResourceManager.class.getResourceAsStream(var5));
            }

            var2 = ((Integer)this.field_698.get(var5)).intValue();
            this.method_393(var3, var2);
         } catch (IOException var4) {
            var4.printStackTrace();
         }
      }

   }

   // $FF: renamed from: game (int) void
   public static void bind_texture(int var0) {
      if (var0 >= 0) {
         GL11.glBindTexture(3553, var0);
      }
   }
}
