package net.minecraft.client.rendering.b;

import a.MathHelper;
import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.game.animals.game
public final class SomeKindOfCamera extends SomeKindOfBaseCamera {
   // $FF: renamed from: player net.minecraft.client.game.animals.game
   private static SomeKindOfCamera singleton = new SomeKindOfCamera();
   // $FF: renamed from: particles java.nio.FloatBuffer
   private FloatBuffer projection = BufferUtils.createFloatBuffer(16);
   // $FF: renamed from: player java.nio.FloatBuffer
   private FloatBuffer modelview = BufferUtils.createFloatBuffer(16);
   // $FF: renamed from: h java.nio.FloatBuffer
   private FloatBuffer field_1002 = BufferUtils.createFloatBuffer(16);

   // $FF: renamed from: game () net.minecraft.client.game.animals.animals
   public static SomeKindOfBaseCamera update_camera() {
      SomeKindOfCamera var0 = singleton;
      singleton.projection.clear();
      var0.modelview.clear();
      var0.field_1002.clear();
      GL11.glGetFloat(GL11.GL_PROJECTION_MATRIX, var0.projection);
      GL11.glGetFloat(GL11.GL_MODELVIEW_MATRIX, var0.modelview);
      var0.projection.flip().limit(16);
      var0.projection.get(var0.field_996);
      var0.modelview.flip().limit(16);
      var0.modelview.get(var0.field_997);
      var0.field_998[0] = var0.field_997[0] * var0.field_996[0] + var0.field_997[1] * var0.field_996[4] + var0.field_997[2] * var0.field_996[8] + var0.field_997[3] * var0.field_996[12];
      var0.field_998[1] = var0.field_997[0] * var0.field_996[1] + var0.field_997[1] * var0.field_996[5] + var0.field_997[2] * var0.field_996[9] + var0.field_997[3] * var0.field_996[13];
      var0.field_998[2] = var0.field_997[0] * var0.field_996[2] + var0.field_997[1] * var0.field_996[6] + var0.field_997[2] * var0.field_996[10] + var0.field_997[3] * var0.field_996[14];
      var0.field_998[3] = var0.field_997[0] * var0.field_996[3] + var0.field_997[1] * var0.field_996[7] + var0.field_997[2] * var0.field_996[11] + var0.field_997[3] * var0.field_996[15];
      var0.field_998[4] = var0.field_997[4] * var0.field_996[0] + var0.field_997[5] * var0.field_996[4] + var0.field_997[6] * var0.field_996[8] + var0.field_997[7] * var0.field_996[12];
      var0.field_998[5] = var0.field_997[4] * var0.field_996[1] + var0.field_997[5] * var0.field_996[5] + var0.field_997[6] * var0.field_996[9] + var0.field_997[7] * var0.field_996[13];
      var0.field_998[6] = var0.field_997[4] * var0.field_996[2] + var0.field_997[5] * var0.field_996[6] + var0.field_997[6] * var0.field_996[10] + var0.field_997[7] * var0.field_996[14];
      var0.field_998[7] = var0.field_997[4] * var0.field_996[3] + var0.field_997[5] * var0.field_996[7] + var0.field_997[6] * var0.field_996[11] + var0.field_997[7] * var0.field_996[15];
      var0.field_998[8] = var0.field_997[8] * var0.field_996[0] + var0.field_997[9] * var0.field_996[4] + var0.field_997[10] * var0.field_996[8] + var0.field_997[11] * var0.field_996[12];
      var0.field_998[9] = var0.field_997[8] * var0.field_996[1] + var0.field_997[9] * var0.field_996[5] + var0.field_997[10] * var0.field_996[9] + var0.field_997[11] * var0.field_996[13];
      var0.field_998[10] = var0.field_997[8] * var0.field_996[2] + var0.field_997[9] * var0.field_996[6] + var0.field_997[10] * var0.field_996[10] + var0.field_997[11] * var0.field_996[14];
      var0.field_998[11] = var0.field_997[8] * var0.field_996[3] + var0.field_997[9] * var0.field_996[7] + var0.field_997[10] * var0.field_996[11] + var0.field_997[11] * var0.field_996[15];
      var0.field_998[12] = var0.field_997[12] * var0.field_996[0] + var0.field_997[13] * var0.field_996[4] + var0.field_997[14] * var0.field_996[8] + var0.field_997[15] * var0.field_996[12];
      var0.field_998[13] = var0.field_997[12] * var0.field_996[1] + var0.field_997[13] * var0.field_996[5] + var0.field_997[14] * var0.field_996[9] + var0.field_997[15] * var0.field_996[13];
      var0.field_998[14] = var0.field_997[12] * var0.field_996[2] + var0.field_997[13] * var0.field_996[6] + var0.field_997[14] * var0.field_996[10] + var0.field_997[15] * var0.field_996[14];
      var0.field_998[15] = var0.field_997[12] * var0.field_996[3] + var0.field_997[13] * var0.field_996[7] + var0.field_997[14] * var0.field_996[11] + var0.field_997[15] * var0.field_996[15];
      var0.field_995[0][0] = var0.field_998[3] - var0.field_998[0];
      var0.field_995[0][1] = var0.field_998[7] - var0.field_998[4];
      var0.field_995[0][2] = var0.field_998[11] - var0.field_998[8];
      var0.field_995[0][3] = var0.field_998[15] - var0.field_998[12];
      normalize(var0.field_995, 0);
      var0.field_995[1][0] = var0.field_998[3] + var0.field_998[0];
      var0.field_995[1][1] = var0.field_998[7] + var0.field_998[4];
      var0.field_995[1][2] = var0.field_998[11] + var0.field_998[8];
      var0.field_995[1][3] = var0.field_998[15] + var0.field_998[12];
      normalize(var0.field_995, 1);
      var0.field_995[2][0] = var0.field_998[3] + var0.field_998[1];
      var0.field_995[2][1] = var0.field_998[7] + var0.field_998[5];
      var0.field_995[2][2] = var0.field_998[11] + var0.field_998[9];
      var0.field_995[2][3] = var0.field_998[15] + var0.field_998[13];
      normalize(var0.field_995, 2);
      var0.field_995[3][0] = var0.field_998[3] - var0.field_998[1];
      var0.field_995[3][1] = var0.field_998[7] - var0.field_998[5];
      var0.field_995[3][2] = var0.field_998[11] - var0.field_998[9];
      var0.field_995[3][3] = var0.field_998[15] - var0.field_998[13];
      normalize(var0.field_995, 3);
      var0.field_995[4][0] = var0.field_998[3] - var0.field_998[2];
      var0.field_995[4][1] = var0.field_998[7] - var0.field_998[6];
      var0.field_995[4][2] = var0.field_998[11] - var0.field_998[10];
      var0.field_995[4][3] = var0.field_998[15] - var0.field_998[14];
      normalize(var0.field_995, 4);
      var0.field_995[5][0] = var0.field_998[3] + var0.field_998[2];
      var0.field_995[5][1] = var0.field_998[7] + var0.field_998[6];
      var0.field_995[5][2] = var0.field_998[11] + var0.field_998[10];
      var0.field_995[5][3] = var0.field_998[15] + var0.field_998[14];
      normalize(var0.field_995, 5);
      return singleton;
   }

   // $FF: renamed from: game (float[][], int) void
   private static void normalize(float[][] var0, int var1) {
      float var2 = MathHelper.sqrt(var0[var1][0] * var0[var1][0] + var0[var1][1] * var0[var1][1] + var0[var1][2] * var0[var1][2]);
      var0[var1][0] /= var2;
      var0[var1][1] /= var2;
      var0[var1][2] /= var2;
      var0[var1][3] /= var2;
   }
}
