package net.minecraft.client.rendering.b;

import net.minecraft.game.util.AABB;

// $FF: renamed from: net.minecraft.client.game.animals.entities
public final class FustrumCuller implements ICuller {
   // $FF: renamed from: game net.minecraft.client.game.animals.animals
   private SomeKindOfBaseCamera camera = SomeKindOfCamera.update_camera();

   // $FF: renamed from: game (net.minecraft.game.projectiles.animals) boolean
   public final boolean is_visible(AABB hit) {
      SomeKindOfBaseCamera var10001 = this.camera;
      float max_z = hit.max_z;
      float max_y = hit.max_y;
      float max_x = hit.max_x;
      float min_z = hit.min_z;
      float min_y = hit.min_y;
      float min_x = hit.min_x;
      SomeKindOfBaseCamera cam = this.camera;

      for(int i = 0; i < 6; ++i) {
         if (cam.field_995[i][0] * min_x +
                 cam.field_995[i][1] * min_y +
                 cam.field_995[i][2] * min_z +
                 cam.field_995[i][3] <= 0.0F

                 && cam.field_995[i][0] * max_x +
                 cam.field_995[i][1] * min_y +
                 cam.field_995[i][2] * min_z +
                 cam.field_995[i][3] <= 0.0F &&

                 cam.field_995[i][0] * min_x +
                 cam.field_995[i][1] * max_y +
                 cam.field_995[i][2] * min_z +
                 cam.field_995[i][3] <= 0.0F &&

                 cam.field_995[i][0] * max_x +
                 cam.field_995[i][1] * max_y +
                 cam.field_995[i][2] * min_z +
                 cam.field_995[i][3] <= 0.0F &&

                 cam.field_995[i][0] * min_x +
                 cam.field_995[i][1] * min_y +
                 cam.field_995[i][2] * max_z +
                 cam.field_995[i][3] <= 0.0F &&

                 cam.field_995[i][0] * max_x +
                 cam.field_995[i][1] * min_y +
                 cam.field_995[i][2] * max_z +
                 cam.field_995[i][3] <= 0.0F &&

                 cam.field_995[i][0] * min_x +
                 cam.field_995[i][1] * max_y +
                 cam.field_995[i][2] * max_z +
                 cam.field_995[i][3] <= 0.0F &&

                 cam.field_995[i][0] * max_x +
                 cam.field_995[i][1] * max_y +
                 cam.field_995[i][2] * max_z +
                 cam.field_995[i][3] <= 0.0F) {
            return false;
         }
      }

      return true;
   }
}
