package net.minecraft.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import org.lwjgl.input.Keyboard;

// $FF: renamed from: net.minecraft.client.q
public final class GameOptions {
   // $FF: renamed from: w java.lang.String[]
   private static final String[] render_distances = new String[]{"FAR", "NORMAL", "SHORT", "TINY"};
   // $FF: renamed from: x java.lang.String[]
   private static final String[] difficulties = new String[]{"Peaceful", "Easy", "Normal", "Hard"};
   // $FF: renamed from: game boolean
   public boolean music_enabled = true;
   // $FF: renamed from: animals boolean
   public boolean sound_enabled = true;
   // $FF: renamed from: entities boolean
   public boolean mouse_inverted = false;
   // $FF: renamed from: projectiles boolean
   public boolean show_fps = false;
   // $FF: renamed from: player int
   public int render_distance = 0;
   // $FF: renamed from: particles boolean
   public boolean view_bobbing = true;
   // $FF: renamed from: player boolean
   public boolean anaglyph_3d = false;
   // $FF: renamed from: h boolean
   public boolean limit_framerate = false;
   // $FF: renamed from: i net.minecraft.client.h
   public Key forward_key = new Key("Forward", 17);
   // $FF: renamed from: j net.minecraft.client.h
   public Key left_key = new Key("Left", 30);
   // $FF: renamed from: k net.minecraft.client.h
   public Key back_key = new Key("Back", 31);
   // $FF: renamed from: l net.minecraft.client.h
   public Key right_key = new Key("Right", 32);
   // $FF: renamed from: m net.minecraft.client.h
   public Key jump_key = new Key("Jump", 57);
   // $FF: renamed from: n net.minecraft.client.h
   public Key inventory_key = new Key("Inventory", 23);
   // $FF: renamed from: o net.minecraft.client.h
   public Key drop_key = new Key("Drop", 16);
   // $FF: renamed from: y net.minecraft.client.h
   private Key chat_key = new Key("Chat", 20);
   // $FF: renamed from: p net.minecraft.client.h
   public Key toggle_fog_key = new Key("Toggle fog", 33);
   // $FF: renamed from: q net.minecraft.client.h
   public Key save_loc_key = new Key("Save location", 28);
   // $FF: renamed from: r net.minecraft.client.h
   public Key load_loc_key = new Key("Load location", 19);
   // $FF: renamed from: s net.minecraft.client.h[]
   public Key[] keys;
   // $FF: renamed from: z net.minecraft.client.projectiles
   private Game game;
   // $FF: renamed from: A java.io.File
   private File options_file;
   // $FF: renamed from: t int
   public int option_count;
   // $FF: renamed from: u int
   public int difficulty;
   // $FF: renamed from: v boolean
   public boolean third_person_mode;

   public GameOptions(Game var1, File var2) {
      this.keys = new Key[]{this.forward_key, this.left_key, this.back_key, this.right_key, this.jump_key, this.drop_key, this.inventory_key, this.chat_key, this.toggle_fog_key, this.save_loc_key, this.load_loc_key};
      this.option_count = 9;
      this.difficulty = 2;
      this.third_person_mode = false;
      this.game = var1;
      this.options_file = new File(var2, "options.txt");
      this.method_279();
   }

   // $FF: renamed from: game (int) java.lang.String
   public final String GetKeyString(int var1) {
      return this.keys[var1].key_name + ": " + Keyboard.getKeyName(this.keys[var1].key_code);
   }

   // $FF: renamed from: game (int, int) void
   public final void method_276(int var1, int var2) {
      this.keys[var1].key_code = var2;
      this.save_options();
   }

   // $FF: renamed from: animals (int, int) void
   public final void method_277(int var1, int var2) {
      if (var1 == 0) {
         this.music_enabled = !this.music_enabled;
         this.game.sound_manager.stop_music();
      }

      if (var1 == 1) {
         this.sound_enabled = !this.sound_enabled;
         this.game.sound_manager.stop_music();
      }

      if (var1 == 2) {
         this.mouse_inverted = !this.mouse_inverted;
      }

      if (var1 == 3) {
         this.show_fps = !this.show_fps;
      }

      if (var1 == 4) {
         this.render_distance = this.render_distance + var2 & 3;
      }

      if (var1 == 5) {
         this.view_bobbing = !this.view_bobbing;
      }

      if (var1 == 6) {
         this.anaglyph_3d = !this.anaglyph_3d;
         this.game.resource_manager.method_399();
      }

      if (var1 == 7) {
         this.limit_framerate = !this.limit_framerate;
      }

      if (var1 == 8) {
         this.difficulty = this.difficulty + var2 & 3;
      }

      this.save_options();
   }

   // $FF: renamed from: animals (int) java.lang.String
   public final String method_278(int var1) {
      if (var1 == 0) {
         return "Music: " + (this.music_enabled ? "ON" : "OFF");
      } else if (var1 == 1) {
         return "Sound: " + (this.sound_enabled ? "ON" : "OFF");
      } else if (var1 == 2) {
         return "Invert mouse: " + (this.mouse_inverted ? "ON" : "OFF");
      } else if (var1 == 3) {
         return "Show FPS: " + (this.show_fps ? "ON" : "OFF");
      } else if (var1 == 4) {
         return "Render distance: " + render_distances[this.render_distance];
      } else if (var1 == 5) {
         return "View bobbing: " + (this.view_bobbing ? "ON" : "OFF");
      } else if (var1 == 6) {
         return "3d anaglyph: " + (this.anaglyph_3d ? "ON" : "OFF");
      } else if (var1 == 7) {
         return "Limit framerate: " + (this.limit_framerate ? "ON" : "OFF");
      } else {
         return var1 == 8 ? "Difficulty: " + difficulties[this.difficulty] : "";
      }
   }

   // $FF: renamed from: animals () void
   private void method_279() {
      try {
         if (this.options_file.exists()) {
            BufferedReader var1 = new BufferedReader(new FileReader(this.options_file));

            String var2;
            while((var2 = var1.readLine()) != null) {
               String[] var5;
               if ((var5 = var2.split(":"))[0].equals("music")) {
                  this.music_enabled = var5[1].equals("true");
               }

               if (var5[0].equals("sound")) {
                  this.sound_enabled = var5[1].equals("true");
               }

               if (var5[0].equals("invertYMouse")) {
                  this.mouse_inverted = var5[1].equals("true");
               }

               if (var5[0].equals("showFrameRate")) {
                  this.show_fps = var5[1].equals("true");
               }

               if (var5[0].equals("viewDistance")) {
                  this.render_distance = Integer.parseInt(var5[1]);
               }

               if (var5[0].equals("bobView")) {
                  this.view_bobbing = var5[1].equals("true");
               }

               if (var5[0].equals("anaglyph3d")) {
                  this.anaglyph_3d = var5[1].equals("true");
               }

               if (var5[0].equals("limitFramerate")) {
                  this.limit_framerate = var5[1].equals("true");
               }

               if (var5[0].equals("difficulty")) {
                  this.difficulty = Integer.parseInt(var5[1]);
               }

               for(int var3 = 0; var3 < this.keys.length; ++var3) {
                  if (var5[0].equals("key_" + this.keys[var3].key_name)) {
                     this.keys[var3].key_code = Integer.parseInt(var5[1]);
                  }
               }
            }

            var1.close();
         }
      } catch (Exception var4) {
         System.out.println("Failed to load options");
         var4.printStackTrace();
      }
   }

   // $FF: renamed from: game () void
   public final void save_options() {
      try {
         PrintWriter var1;
         (var1 = new PrintWriter(new FileWriter(this.options_file))).println("music:" + this.music_enabled);
         var1.println("sound:" + this.sound_enabled);
         var1.println("invertYMouse:" + this.mouse_inverted);
         var1.println("showFrameRate:" + this.show_fps);
         var1.println("viewDistance:" + this.render_distance);
         var1.println("bobView:" + this.view_bobbing);
         var1.println("anaglyph3d:" + this.anaglyph_3d);
         var1.println("limitFramerate:" + this.limit_framerate);
         var1.println("difficulty:" + this.difficulty);

         for(int var2 = 0; var2 < this.keys.length; ++var2) {
            var1.println("key_" + this.keys[var2].key_name + ":" + this.keys[var2].key_code);
         }

         var1.close();
      } catch (Exception var3) {
         System.out.println("Failed to save options");
         var3.printStackTrace();
      }
   }
}
