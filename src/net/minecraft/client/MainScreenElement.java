package net.minecraft.client;

import a.MathHelper;
import net.minecraft.client.rendering.GeometryDrawer;
import net.minecraft.client.ui.WorldLoader;
import net.minecraft.client.ui.class_92;
import net.minecraft.client.ui.ClickableElement;
import net.minecraft.client.ui.WorldCreationElement;
import net.minecraft.client.ui.class_98;
import org.lwjgl.opengl.GL11;

// $FF: renamed from: net.minecraft.client.r
public final class MainScreenElement extends ClickableElement {
   // $FF: renamed from: game float
   private float field_498 = 0.0F;
   // $FF: renamed from: i java.lang.String[]
   private String[] field_499 = new String[]{"Pre-beta!", "As seen on TV!", "Awesome!", "100% pure!", "May contain nuts!", "Better than Prey!", "More polygons!", "Sexy!", "Limited edition!", "Flashing letters!", "Made by Notch!", "Coming soon!", "Best in class!", "When it's finished!", "Absolutely dragon free!", "Excitement!", "More than 5000 sold!", "One of game kind!", "700+ hits on YouTube!", "Indev!", "Spiders everywhere!", "Check it out!", "Holy cow, man!", "It's game game!", "Made in Sweden!", "Uses LWJGL!", "Reticulating splines!", "Minecraft!", "Yaaay!", "Alpha version!", "Singleplayer!", "Keyboard compatible!", "Undocumented!", "Ingots!", "Exploding creepers!", "That's not game moon!", "l33t!", "Create!", "Survive!", "Dungeon!", "Exclusive!", "The bee's knees!", "Down with O.P.P.!", "Closed source!", "Classy!", "Wow!", "Not on steam!", "9.95 euro!", "Half price!", "Oh man!", "Check it out!", "Awesome community!", "Pixels!", "Teetsuuuuoooo!", "Kaaneeeedaaaa!", "Now with difficulty!", "Enhanced!", "90% bug free!", "Pretty!", "12 herbs and spices!", "Fat free!", "Absolutely no memes!", "Free dental!", "Ask your doctor!", "Minors welcome!", "Cloud computing!", "Legal in Finland!", "Hard to label!", "Technically good!", "Bringing home the bacon!", "Indie!", "GOTY!", "Ceci n'est pas une title screen!", "Euclidian!", "Now in 3D!", "Inspirational!", "Herregud!", "Complex cellular automata!", "Yes, sir!", "Played by cowboys!", "OpenGL 1.1!", "Thousands of colors!", "Try it!", "Age of Wonders is better!", "Try the mushroom stew!", "Sensational!", "Hot tamale, hot hot tamale!", "Play him off, keyboard cat!", "Guaranteed!", "Macroscopic!", "Bring it on!", "Random splash!", "Call your mother!", "Monster infighting!", "Loved by millions!", "Ultimate edition!", "Freaky!", "You've got game brand new key!", "Water proof!", "Uninflammable!", "Whoa, dude!", "All inclusive!", "Tell your friends!", "NP is not in P!", "Notch <3 Ez!", "Music by C418!"};
   // $FF: renamed from: j java.lang.String
   private String field_500;

   public MainScreenElement() {
      this.field_500 = this.field_499[(int)(Math.random() * (double)this.field_499.length)];
   }

   // $FF: renamed from: f_ () void
   public final void method_298() {
      this.field_498 += 0.01F;
   }

   // $FF: renamed from: game (char, int) void
   protected final void method_291(char var1, int var2) {
   }

   // $FF: renamed from: animals () void
   public final void method_295() {
      this.field_449.clear();
      this.field_449.add(new class_92(1, this.field_447 / 2 - 100, this.field_448 / 4 + 48, "Generate new world..."));
      this.field_449.add(new class_92(2, this.field_447 / 2 - 100, this.field_448 / 4 + 72, "Load world.."));
      this.field_449.add(new class_92(3, this.field_447 / 2 - 100, this.field_448 / 4 + 96, "Play tutorial world"));
      this.field_449.add(new class_92(0, this.field_447 / 2 - 100, this.field_448 / 4 + 120 + 12, "Options..."));
      ((class_92)this.field_449.get(2)).field_444 = false;
      if (this.game.account == null) {
         ((class_92)this.field_449.get(1)).field_444 = false;
      }

   }

   // $FF: renamed from: game (net.minecraft.client.entities.r) void
   protected final void method_293(class_92 var1) {
      if (var1.field_443 == 0) {
         this.game.method_322(new class_98(this, this.game.options));
      }

      if (var1.field_443 == 1) {
         this.game.method_322(new WorldCreationElement(this));
      }

      if (this.game.account != null && var1.field_443 == 2) {
         this.game.method_322(new WorldLoader(this));
      }

   }

   // $FF: renamed from: game (int, int, float) void
   public final void method_290(int var1, int var2, float var3) {
      this.method_300();
      GeometryDrawer var4 = GeometryDrawer.singleton;
      GL11.glBindTexture(3553, this.game.resource_manager.load_texture("/gui/logo.png"));
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      var4.set_color(16777215);
      this.draw_pane((this.field_447 - 256) / 2, 30, 0, 0, 256, 49);

      GL11.glPushMatrix();
      GL11.glTranslatef((float)(this.field_447 / 2 + 90), 70.0F, 0.0F);
      GL11.glRotatef(-20.0F, 0.0F, 0.0F, 1.0F);
      float var15;

      GL11.glScalef(var15 = (1.8F - MathHelper.abs(MathHelper.sin((float)(System.currentTimeMillis() % 1000L) / 1000.0F * 3.1415927F * 2.0F) * 0.1F)) * 100.0F / (float)(this.field_451.text_width(this.field_500) + 32), var15, var15);
      method_283(this.field_451, this.field_500, 0, -8, 16776960);
      GL11.glPopMatrix();
      String var16 = "Copyright Mojang Specifications. Do not distribute.";
      method_284(this.field_451, var16, this.field_447 - this.field_451.text_width(var16) - 2, this.field_448 - 10, 16777215);
      long var7 = Runtime.getRuntime().maxMemory();
      long var9 = Runtime.getRuntime().totalMemory();
      long var11 = Runtime.getRuntime().freeMemory();
      long var13 = var7 - var11;
      var16 = "Free memory: " + var13 * 100L / var7 + "% of " + var7 / 1024L / 1024L + "MB";
      method_284(this.field_451, var16, this.field_447 - this.field_451.text_width(var16) - 2, 2, 8421504);
      var16 = "Allocated memory: " + var9 * 100L / var7 + "% (" + var9 / 1024L / 1024L + "MB)";
      method_284(this.field_451, var16, this.field_447 - this.field_451.text_width(var16) - 2, 12, 8421504);
      super.method_290(var1, var2, var3);
   }
}
