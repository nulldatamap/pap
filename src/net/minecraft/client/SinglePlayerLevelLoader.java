package net.minecraft.client;

import a.class_0;
import net.minecraft.game.world.LevelSaverLoader;
import net.minecraft.game.world.World;
import net.minecraft.game.entities.Entity;
import net.minecraft.client.player.LocalPlayer;

// $FF: renamed from: net.minecraft.client.particles
public final class SinglePlayerLevelLoader extends LevelSaverLoader {
   // $FF: renamed from: game net.minecraft.client.projectiles
   private Game world;

   public SinglePlayerLevelLoader(Game var1, class_0 var2) {
      super(var2);
      this.world = var1;
   }

   // $FF: renamed from: game (net.minecraft.game.game.player, java.lang.String) net.minecraft.game.entities.animals
   protected final Entity load_entity(World var1, String var2) {
      if( var2.equals("LocalPlayer" ) ) {
         return new LocalPlayer(this.world, var1, this.world.account);
      }
      return super.load_entity(var1, var2);
   }
}
