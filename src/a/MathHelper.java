package a;

// $FF: renamed from: game.game
public final class MathHelper {
   // $FF: renamed from: game float[]
   private static float[] precalc_sin = new float[65536];

   // $FF: renamed from: game (float) float
   public static final float sin(float x) {
      return precalc_sin[(int)(x * 10430.378F) & '\uffff'];
   }

   // $FF: renamed from: animals (float) float
   public static final float cos(float x) {
      return precalc_sin[(int)(x * 10430.378F + 16384.0F) & '\uffff'];
   }

   // $FF: renamed from: entities (float) float
   public static final float sqrt(float x) {
      return (float)Math.sqrt((double)x);
   }

   // $FF: renamed from: projectiles (float) int
   public static int floorf(float x) {
      int xi = (int)x;
      return x < (float)xi ? xi - 1 : xi;
   }

   // $FF: renamed from: game (double) int
   public static int floord(double x) {
      int xi = (int)x;
      return x < (double)xi ? xi - 1 : xi;
   }

   // $FF: renamed from: player (float) float
   public static float abs(float x) {
      return x >= 0.0F ? x : -x;
   }

   static {
      for(int i = 0; i < 65536; ++i) {
         precalc_sin[i] = (float)Math.sin((double)i * 3.141592653589793D * 2.0D / 65536.0D);
      }

   }
}
