package com.jcraft.jorbis;

import a.MathHelper;
import com.jcraft.jogg.Buffer;

class Floor0 extends FuncFloor {
   float[] lsp = null;

   void pack(Object var1, Buffer var2) {
      Floor0$InfoFloor0 var4 = (Floor0$InfoFloor0)var1;
      var2.write(var4.order, 8);
      var2.write(var4.rate, 16);
      var2.write(var4.barkmap, 16);
      var2.write(var4.ampbits, 6);
      var2.write(var4.ampdB, 8);
      var2.write(var4.numbooks - 1, 4);

      for(int var3 = 0; var3 < var4.numbooks; ++var3) {
         var2.write(var4.books[var3], 8);
      }

   }

   Object unpack(Info var1, Buffer var2) {
      Floor0$InfoFloor0 var3;
      (var3 = new Floor0$InfoFloor0(this)).order = var2.read(8);
      var3.rate = var2.read(16);
      var3.barkmap = var2.read(16);
      var3.ampbits = var2.read(6);
      var3.ampdB = var2.read(8);
      var3.numbooks = var2.read(4) + 1;
      if (var3.order > 0 && var3.rate > 0 && var3.barkmap > 0 && var3.numbooks > 0) {
         for(int var4 = 0; var4 < var3.numbooks; ++var4) {
            var3.books[var4] = var2.read(8);
            if (var3.books[var4] < 0 || var3.books[var4] >= var1.books) {
               return null;
            }
         }

         return var3;
      } else {
         return null;
      }
   }

   Object look(DspState var1, InfoMode var2, Object var3) {
      Info var6 = var1.field_267;
      Floor0$InfoFloor0 var9 = (Floor0$InfoFloor0)var3;
      Floor0$LookFloor0 var4;
      (var4 = new Floor0$LookFloor0(this)).field_809 = var9.order;
      var4.field_807 = var6.blocksizes[var2.blockflag] / 2;
      var4.field_808 = var9.barkmap;
      var4.field_810 = var9;
      var4.lpclook.init(var4.field_808, var4.field_809);
      float var7 = (float)var4.field_808 / toBARK((float)((double)var9.rate / 2.0D));
      var4.linearmap = new int[var4.field_807];

      for(int var8 = 0; var8 < var4.field_807; ++var8) {
         int var5;
         if ((var5 = MathHelper.floorf(toBARK((float)((double)var9.rate / 2.0D / (double)var4.field_807 * (double)var8)) * var7)) >= var4.field_808) {
            var5 = var4.field_808;
         }

         var4.linearmap[var8] = var5;
      }

      return var4;
   }

   static float toBARK(float var0) {
      return (float)(13.1D * Math.atan(7.4E-4D * (double)var0) + 2.24D * Math.atan((double)(var0 * var0) * 1.85E-8D) + 1.0E-4D * (double)var0);
   }

   Object state(Object var1) {
      Floor0$EchstateFloor0 var2 = new Floor0$EchstateFloor0(this);
      Floor0$InfoFloor0 var3 = (Floor0$InfoFloor0)var1;
      var2.codewords = new int[var3.order];
      var2.curve = new float[var3.barkmap];
      var2.frameno = -1L;
      return var2;
   }

   void free_info(Object var1) {
   }

   void free_look(Object var1) {
   }

   void free_state(Object var1) {
   }

   int forward(Block var1, Object var2, float[] var3, float[] var4, Object var5) {
      return 0;
   }

   int inverse(Block var1, Object var2, float[] var3) {
      Floor0$LookFloor0 var12;
      Floor0$InfoFloor0 var4 = (var12 = (Floor0$LookFloor0)var2).field_810;
      int var5;
      if ((var5 = var1.opb.read(var4.ampbits)) > 0) {
         int var6 = (1 << var4.ampbits) - 1;
         float var13 = (float)var5 / (float)var6 * (float)var4.ampdB;
         if ((var6 = var1.opb.read(Util.ilog(var4.numbooks))) != -1 && var6 < var4.numbooks) {
            synchronized(this) {
               if (this.lsp != null && this.lsp.length >= var12.field_809) {
                  for(int var8 = 0; var8 < var12.field_809; ++var8) {
                     this.lsp[var8] = 0.0F;
                  }
               } else {
                  this.lsp = new float[var12.field_809];
               }

               CodeBook var15 = var1.field_252.fullbooks[var4.books[var6]];
               float var14 = 0.0F;

               int var9;
               for(var9 = 0; var9 < var12.field_809; ++var9) {
                  var3[var9] = 0.0F;
               }

               int var11;
               for(var9 = 0; var9 < var12.field_809; var9 += var15.dim) {
                  if (var15.decodevs(this.lsp, var9, var1.opb, 1, -1) == -1) {
                     for(var11 = 0; var11 < var12.field_807; ++var11) {
                        var3[var11] = 0.0F;
                     }

                     return 0;
                  }
               }

               for(var9 = 0; var9 < var12.field_809; var14 = this.lsp[var9 - 1]) {
                  for(var11 = 0; var11 < var15.dim; ++var9) {
                     this.lsp[var9] += var14;
                     ++var11;
                  }
               }

               Lsp.lsp_to_curve(var3, var12.linearmap, var12.field_807, var12.field_808, this.lsp, var12.field_809, var13, (float)var4.ampdB);
               return 1;
            }
         }
      }

      return 0;
   }

   Object inverse1(Block var1, Object var2, Object var3) {
      Floor0$LookFloor0 var9;
      Floor0$InfoFloor0 var4 = (var9 = (Floor0$LookFloor0)var2).field_810;
      float[] var5 = null;
      if (var3 instanceof float[]) {
         var5 = (float[])var3;
      }

      int var10;
      if ((var10 = var1.opb.read(var4.ampbits)) > 0) {
         int var6 = (1 << var4.ampbits) - 1;
         float var11 = (float)var10 / (float)var6 * (float)var4.ampdB;
         if ((var6 = var1.opb.read(Util.ilog(var4.numbooks))) != -1 && var6 < var4.numbooks) {
            CodeBook var12 = var1.field_252.fullbooks[var4.books[var6]];
            float var13 = 0.0F;
            int var7;
            if (var5 != null && var5.length >= var9.field_809 + 1) {
               for(var7 = 0; var7 < var5.length; ++var7) {
                  var5[var7] = 0.0F;
               }
            } else {
               var5 = new float[var9.field_809 + 1];
            }

            for(var7 = 0; var7 < var9.field_809; var7 += var12.dim) {
               if (var12.decodev_set(var5, var7, var1.opb, var12.dim) == -1) {
                  return null;
               }
            }

            for(var7 = 0; var7 < var9.field_809; var13 = var5[var7 - 1]) {
               for(int var8 = 0; var8 < var12.dim; ++var7) {
                  var5[var7] += var13;
                  ++var8;
               }
            }

            var5[var9.field_809] = var11;
            return var5;
         }
      }

      return null;
   }

   int inverse2(Block var1, Object var2, Object var3, float[] var4) {
      Floor0$LookFloor0 var6;
      Floor0$InfoFloor0 var7 = (var6 = (Floor0$LookFloor0)var2).field_810;
      if (var3 != null) {
         float[] var9;
         float var5 = (var9 = (float[])var3)[var6.field_809];
         Lsp.lsp_to_curve(var4, var6.linearmap, var6.field_807, var6.field_808, var9, var6.field_809, var5, (float)var7.ampdB);
         return 1;
      } else {
         for(int var8 = 0; var8 < var6.field_807; ++var8) {
            var4[var8] = 0.0F;
         }

         return 0;
      }
   }

   static float fromdB(float var0) {
      return (float)Math.exp((double)var0 * 0.11512925D);
   }

   static void lsp_to_lpc(float[] var0, float[] var1, int var2) {
      int var4;
      float[] var5 = new float[var4 = var2 / 2];
      float[] var6 = new float[var4];
      float[] var8 = new float[var4 + 1];
      float[] var9 = new float[var4 + 1];
      float[] var11 = new float[var4];
      float[] var12 = new float[var4];

      int var3;
      for(var3 = 0; var3 < var4; ++var3) {
         var5[var3] = (float)(-2.0D * Math.cos((double)var0[var3 << 1]));
         var6[var3] = (float)(-2.0D * Math.cos((double)var0[(var3 << 1) + 1]));
      }

      int var14;
      for(var14 = 0; var14 < var4; ++var14) {
         var8[var14] = 0.0F;
         var9[var14] = 1.0F;
         var11[var14] = 0.0F;
         var12[var14] = 1.0F;
      }

      var9[var14] = 1.0F;
      var8[var14] = 1.0F;

      for(var3 = 1; var3 < var2 + 1; ++var3) {
         float var10 = 0.0F;
         float var7 = 0.0F;

         for(var14 = 0; var14 < var4; ++var14) {
            float var13 = var5[var14] * var9[var14] + var8[var14];
            var8[var14] = var9[var14];
            var9[var14] = var7;
            var7 += var13;
            var13 = var6[var14] * var12[var14] + var11[var14];
            var11[var14] = var12[var14];
            var12[var14] = var10;
            var10 += var13;
         }

         var1[var3 - 1] = (var7 + var9[var14] + var10 - var8[var14]) / 2.0F;
         var9[var14] = var7;
         var8[var14] = var10;
      }

   }

   static void lpc_to_curve(float[] var0, float[] var1, float var2, Floor0$LookFloor0 var3, String var4, int var5) {
      float[] var7 = new float[Math.max(var3.field_808 << 1, (var3.field_809 << 1) + 2)];
      int var6;
      if (var2 == 0.0F) {
         for(var6 = 0; var6 < var3.field_807; ++var6) {
            var0[var6] = 0.0F;
         }

      } else {
         var3.lpclook.lpc_to_curve(var7, var1, var2);

         for(var6 = 0; var6 < var3.field_807; ++var6) {
            var0[var6] = var7[var3.linearmap[var6]];
         }

      }
   }
}
