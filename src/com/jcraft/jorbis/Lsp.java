package com.jcraft.jorbis;

class Lsp {
   static final float M_PI = 3.1415927F;

   static void lsp_to_curve(float[] var0, int[] var1, int var2, int var3, float[] var4, int var5, float var6, float var7) {
      float var8 = 3.1415927F / (float)var3;

      for(var3 = 0; var3 < var5; ++var3) {
         var4[var3] = Lookup.coslook(var4[var3]);
      }

      int var9 = var5 / 2 << 1;
      var3 = 0;

      while(var3 < var2) {
         int var10 = var1[var3];
         float var11 = 0.70710677F;
         float var12 = 0.70710677F;
         float var13 = Lookup.coslook(var8 * (float)var10);

         int var14;
         for(var14 = 0; var14 < var9; var14 += 2) {
            var12 *= var4[var14] - var13;
            var11 *= var4[var14 + 1] - var13;
         }

         if ((var5 & 1) != 0) {
            var12 = (var12 *= var4[var5 - 1] - var13) * var12;
            var11 *= var11 * (1.0F - var13 * var13);
         } else {
            var12 *= var12 * (var13 + 1.0F);
            var11 *= var11 * (1.0F - var13);
         }

         var14 = Float.floatToIntBits(var12 += var11);
         int var15 = Integer.MAX_VALUE & var14;
         int var16 = 0;
         if (var15 < 2139095040 && var15 != 0) {
            if (var15 < 8388608) {
               var14 = Float.floatToIntBits((float)((double)var12 * 3.3554432E7D));
               var15 = Integer.MAX_VALUE & var14;
               var16 = -25;
            }

            var16 += (var15 >>> 23) - 126;
            var12 = Float.intBitsToFloat(var14 & -2139095041 | 1056964608);
         }

         var12 = Lookup.fromdBlook(var6 * Lookup.invsqlook(var12) * Lookup.invsq2explook(var16 + var5) - var7);

         while(true) {
            int var10001 = var3++;
            var0[var10001] *= var12;
            if (var3 >= var2 || var1[var3] != var10) {
               break;
            }
         }
      }

   }
}
