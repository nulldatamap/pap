package com.jcraft.jorbis;

import com.jcraft.jogg.Buffer;

class Mapping0 extends FuncMapping {
   static int seq = 0;
   float[][] pcmbundle = null;
   int[] zerobundle = null;
   int[] nonzero = null;
   Object[] floormemo = null;

   void free_info(Object var1) {
   }

   void free_look(Object var1) {
   }

   Object look(DspState var1, InfoMode var2, Object var3) {
      Info var4 = var1.field_267;
      Mapping0$LookMapping0 var5;
      Mapping0$InfoMapping0 var10 = (var5 = new Mapping0$LookMapping0(this)).map = (Mapping0$InfoMapping0)var3;
      var5.mode = var2;
      var5.time_look = new Object[var10.submaps];
      var5.floor_look = new Object[var10.submaps];
      var5.residue_look = new Object[var10.submaps];
      var5.time_func = new FuncTime[var10.submaps];
      var5.floor_func = new FuncFloor[var10.submaps];
      var5.residue_func = new FuncResidue[var10.submaps];

      for(int var6 = 0; var6 < var10.submaps; ++var6) {
         int var7 = var10.timesubmap[var6];
         int var8 = var10.floorsubmap[var6];
         int var9 = var10.residuesubmap[var6];
         var5.time_func[var6] = FuncTime.time_P[var4.time_type[var7]];
         var5.time_look[var6] = var5.time_func[var6].look(var1, var2, var4.time_param[var7]);
         var5.floor_func[var6] = FuncFloor.floor_P[var4.floor_type[var8]];
         var5.floor_look[var6] = var5.floor_func[var6].look(var1, var2, var4.floor_param[var8]);
         var5.residue_func[var6] = FuncResidue.residue_P[var4.residue_type[var9]];
         var5.residue_look[var6] = var5.residue_func[var6].look(var1, var2, var4.residue_param[var9]);
      }

      var5.field_404 = var4.channels;
      return var5;
   }

   void pack(Info var1, Object var2, Buffer var3) {
      Mapping0$InfoMapping0 var5;
      if ((var5 = (Mapping0$InfoMapping0)var2).submaps > 1) {
         var3.write(1, 1);
         var3.write(var5.submaps - 1, 4);
      } else {
         var3.write(0, 1);
      }

      int var4;
      if (var5.coupling_steps > 0) {
         var3.write(1, 1);
         var3.write(var5.coupling_steps - 1, 8);

         for(var4 = 0; var4 < var5.coupling_steps; ++var4) {
            var3.write(var5.coupling_mag[var4], Util.ilog2(var1.channels));
            var3.write(var5.coupling_ang[var4], Util.ilog2(var1.channels));
         }
      } else {
         var3.write(0, 1);
      }

      var3.write(0, 2);
      if (var5.submaps > 1) {
         for(var4 = 0; var4 < var1.channels; ++var4) {
            var3.write(var5.chmuxlist[var4], 4);
         }
      }

      for(var4 = 0; var4 < var5.submaps; ++var4) {
         var3.write(var5.timesubmap[var4], 8);
         var3.write(var5.floorsubmap[var4], 8);
         var3.write(var5.residuesubmap[var4], 8);
      }

   }

   Object unpack(Info var1, Buffer var2) {
      Mapping0$InfoMapping0 var3 = new Mapping0$InfoMapping0(this);
      if (var2.read(1) != 0) {
         var3.submaps = var2.read(4) + 1;
      } else {
         var3.submaps = 1;
      }

      int var4;
      if (var2.read(1) != 0) {
         var3.coupling_steps = var2.read(8) + 1;

         for(var4 = 0; var4 < var3.coupling_steps; ++var4) {
            int var5 = var3.coupling_mag[var4] = var2.read(Util.ilog2(var1.channels));
            int var6 = var3.coupling_ang[var4] = var2.read(Util.ilog2(var1.channels));
            if (var5 < 0 || var6 < 0 || var5 == var6 || var5 >= var1.channels || var6 >= var1.channels) {
               var3.free();
               return null;
            }
         }
      }

      if (var2.read(2) > 0) {
         var3.free();
         return null;
      } else {
         if (var3.submaps > 1) {
            for(var4 = 0; var4 < var1.channels; ++var4) {
               var3.chmuxlist[var4] = var2.read(4);
               if (var3.chmuxlist[var4] >= var3.submaps) {
                  var3.free();
                  return null;
               }
            }
         }

         for(var4 = 0; var4 < var3.submaps; ++var4) {
            var3.timesubmap[var4] = var2.read(8);
            if (var3.timesubmap[var4] >= var1.times) {
               var3.free();
               return null;
            }

            var3.floorsubmap[var4] = var2.read(8);
            if (var3.floorsubmap[var4] >= var1.floors) {
               var3.free();
               return null;
            }

            var3.residuesubmap[var4] = var2.read(8);
            if (var3.residuesubmap[var4] >= var1.residues) {
               var3.free();
               return null;
            }
         }

         return var3;
      }
   }

   synchronized int inverse(Block var1, Object var2) {
      DspState var3 = var1.field_252;
      Info var4 = var1.field_252.field_267;
      Mapping0$LookMapping0 var14;
      Mapping0$InfoMapping0 var5 = (var14 = (Mapping0$LookMapping0)var2).map;
      InfoMode var6 = var14.mode;
      int var7 = var1.pcmend = var4.blocksizes[var1.field_250];
      float[] var15 = var3.window[var1.field_250][var1.field_249][var1.field_251][var6.windowtype];
      if (this.pcmbundle == null || this.pcmbundle.length < var4.channels) {
         this.pcmbundle = new float[var4.channels][];
         this.nonzero = new int[var4.channels];
         this.zerobundle = new int[var4.channels];
         this.floormemo = new Object[var4.channels];
      }

      int var8;
      float[] var9;
      int var10;
      int var11;
      for(var8 = 0; var8 < var4.channels; ++var8) {
         var9 = var1.pcm[var8];
         var10 = var5.chmuxlist[var8];
         this.floormemo[var8] = var14.floor_func[var10].inverse1(var1, var14.floor_look[var10], this.floormemo[var8]);
         if (this.floormemo[var8] != null) {
            this.nonzero[var8] = 1;
         } else {
            this.nonzero[var8] = 0;
         }

         for(var11 = 0; var11 < var7 / 2; ++var11) {
            var9[var11] = 0.0F;
         }
      }

      for(var8 = 0; var8 < var5.coupling_steps; ++var8) {
         if (this.nonzero[var5.coupling_mag[var8]] != 0 || this.nonzero[var5.coupling_ang[var8]] != 0) {
            this.nonzero[var5.coupling_mag[var8]] = 1;
            this.nonzero[var5.coupling_ang[var8]] = 1;
         }
      }

      for(var8 = 0; var8 < var5.submaps; ++var8) {
         int var16 = 0;

         for(var10 = 0; var10 < var4.channels; ++var10) {
            if (var5.chmuxlist[var10] == var8) {
               if (this.nonzero[var10] != 0) {
                  this.zerobundle[var16] = 1;
               } else {
                  this.zerobundle[var16] = 0;
               }

               this.pcmbundle[var16++] = var1.pcm[var10];
            }
         }

         var14.residue_func[var8].inverse(var1, var14.residue_look[var8], this.pcmbundle, this.zerobundle, var16);
      }

      for(var8 = var5.coupling_steps - 1; var8 >= 0; --var8) {
         var9 = var1.pcm[var5.coupling_mag[var8]];
         float[] var17 = var1.pcm[var5.coupling_ang[var8]];

         for(var11 = 0; var11 < var7 / 2; ++var11) {
            float var12 = var9[var11];
            float var13 = var17[var11];
            if (var12 > 0.0F) {
               if (var13 > 0.0F) {
                  var9[var11] = var12;
                  var17[var11] = var12 - var13;
               } else {
                  var17[var11] = var12;
                  var9[var11] = var12 + var13;
               }
            } else if (var13 > 0.0F) {
               var9[var11] = var12;
               var17[var11] = var12 + var13;
            } else {
               var17[var11] = var12;
               var9[var11] = var12 - var13;
            }
         }
      }

      for(var8 = 0; var8 < var4.channels; ++var8) {
         var9 = var1.pcm[var8];
         var10 = var5.chmuxlist[var8];
         var14.floor_func[var10].inverse2(var1, var14.floor_look[var10], this.floormemo[var8], var9);
      }

      for(var8 = 0; var8 < var4.channels; ++var8) {
         var9 = var1.pcm[var8];
         ((Mdct)var3.transform[var1.field_250][0]).backward(var9, var9);
      }

      for(var8 = 0; var8 < var4.channels; ++var8) {
         var9 = var1.pcm[var8];
         if (this.nonzero[var8] != 0) {
            for(var10 = 0; var10 < var7; ++var10) {
               var9[var10] *= var15[var10];
            }
         } else {
            for(var10 = 0; var10 < var7; ++var10) {
               var9[var10] = 0.0F;
            }
         }
      }

      return 0;
   }
}
