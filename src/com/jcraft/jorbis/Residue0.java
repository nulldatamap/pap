package com.jcraft.jorbis;

import com.jcraft.jogg.Buffer;

class Residue0 extends FuncResidue {
   private static int[][][] _01inverse_partword = new int[2][][];
   static int[][] _2inverse_partword = null;

   void pack(Object var1, Buffer var2) {
      Residue0$InfoResidue0 var6 = (Residue0$InfoResidue0)var1;
      int var3 = 0;
      var2.write(var6.begin, 24);
      var2.write(var6.end, 24);
      var2.write(var6.grouping - 1, 24);
      var2.write(var6.partitions - 1, 6);
      var2.write(var6.groupbook, 8);

      int var4;
      for(var4 = 0; var4 < var6.partitions; ++var4) {
         int var5;
         if (Util.ilog(var5 = var6.secondstages[var4]) > 3) {
            var2.write(var5, 3);
            var2.write(1, 1);
            var2.write(var5 >>> 3, 5);
         } else {
            var2.write(var5, 4);
         }

         var3 += Util.icount(var5);
      }

      for(var4 = 0; var4 < var3; ++var4) {
         var2.write(var6.booklist[var4], 8);
      }

   }

   Object unpack(Info var1, Buffer var2) {
      int var3 = 0;
      Residue0$InfoResidue0 var4;
      (var4 = new Residue0$InfoResidue0(this)).begin = var2.read(24);
      var4.end = var2.read(24);
      var4.grouping = var2.read(24) + 1;
      var4.partitions = var2.read(6) + 1;
      var4.groupbook = var2.read(8);

      int var5;
      for(var5 = 0; var5 < var4.partitions; ++var5) {
         int var6 = var2.read(3);
         if (var2.read(1) != 0) {
            var6 |= var2.read(5) << 3;
         }

         var4.secondstages[var5] = var6;
         var3 += Util.icount(var6);
      }

      for(var5 = 0; var5 < var3; ++var5) {
         var4.booklist[var5] = var2.read(8);
      }

      if (var4.groupbook >= var1.books) {
         this.free_info(var4);
         return null;
      } else {
         for(var5 = 0; var5 < var3; ++var5) {
            if (var4.booklist[var5] >= var1.books) {
               this.free_info(var4);
               return null;
            }
         }

         return var4;
      }
   }

   Object look(DspState var1, InfoMode var2, Object var3) {
      Residue0$InfoResidue0 var12 = (Residue0$InfoResidue0)var3;
      Residue0$LookResidue0 var4 = new Residue0$LookResidue0(this);
      int var5 = 0;
      int var6 = 0;
      var4.info = var12;
      var4.map = var2.mapping;
      var4.parts = var12.partitions;
      var4.fullbooks = var1.fullbooks;
      var4.phrasebook = var1.fullbooks[var12.groupbook];
      int var10 = var4.phrasebook.dim;
      var4.partbooks = new int[var4.parts][];

      int var7;
      int var8;
      int var9;
      int var11;
      for(var11 = 0; var11 < var4.parts; ++var11) {
         if ((var8 = Util.ilog(var7 = var12.secondstages[var11])) != 0) {
            if (var8 > var6) {
               var6 = var8;
            }

            var4.partbooks[var11] = new int[var8];

            for(var9 = 0; var9 < var8; ++var9) {
               if ((var7 & 1 << var9) != 0) {
                  var4.partbooks[var11][var9] = var12.booklist[var5++];
               }
            }
         }
      }

      var4.partvals = (int)Math.rint(Math.pow((double)var4.parts, (double)var10));
      var4.stages = var6;
      var4.decodemap = new int[var4.partvals][];

      for(var11 = 0; var11 < var4.partvals; ++var11) {
         var7 = var11;
         var8 = var4.partvals / var4.parts;
         var4.decodemap[var11] = new int[var10];

         for(var9 = 0; var9 < var10; ++var9) {
            int var13 = var7 / var8;
            var7 -= var13 * var8;
            var8 /= var4.parts;
            var4.decodemap[var11][var9] = var13;
         }
      }

      return var4;
   }

   void free_info(Object var1) {
   }

   void free_look(Object var1) {
   }

   static synchronized int _01inverse(Block var0, Object var1, float[][] var2, int var3, int var4) {
      Residue0$LookResidue0 var9;
      Residue0$InfoResidue0 var10;
      int var11 = (var10 = (var9 = (Residue0$LookResidue0)var1).info).grouping;
      int var12 = var9.phrasebook.dim;
      int var13;
      int var16 = ((var13 = (var10.end - var10.begin) / var11) + var12 - 1) / var12;
      if (_01inverse_partword.length < var3) {
         _01inverse_partword = new int[var3][][];
      }

      int var5;
      for(var5 = 0; var5 < var3; ++var5) {
         if (_01inverse_partword[var5] == null || _01inverse_partword[var5].length < var16) {
            _01inverse_partword[var5] = new int[var16][];
         }
      }

      for(int var8 = 0; var8 < var9.stages; ++var8) {
         var16 = 0;

         for(int var7 = 0; var16 < var13; ++var7) {
            int var14;
            if (var8 == 0) {
               for(var5 = 0; var5 < var3; ++var5) {
                  if ((var14 = var9.phrasebook.decode(var0.opb)) == -1) {
                     return 0;
                  }

                  _01inverse_partword[var5][var7] = var9.decodemap[var14];
                  if (_01inverse_partword[var5][var7] == null) {
                     return 0;
                  }
               }
            }

            for(int var6 = 0; var6 < var12 && var16 < var13; ++var16) {
               for(var5 = 0; var5 < var3; ++var5) {
                  var14 = var10.begin + var16 * var11;
                  int var15 = _01inverse_partword[var5][var7][var6];
                  CodeBook var17;
                  if ((var10.secondstages[var15] & 1 << var8) != 0 && (var17 = var9.fullbooks[var9.partbooks[var15][var8]]) != null) {
                     if (var4 == 0) {
                        if (var17.decodevs_add(var2[var5], var14, var0.opb, var11) == -1) {
                           return 0;
                        }
                     } else if (var4 == 1 && var17.decodev_add(var2[var5], var14, var0.opb, var11) == -1) {
                        return 0;
                     }
                  }
               }

               ++var6;
            }
         }
      }

      return 0;
   }

   static synchronized int _2inverse(Block var0, Object var1, float[][] var2, int var3) {
      Residue0$LookResidue0 var7;
      Residue0$InfoResidue0 var8;
      int var9 = (var8 = (var7 = (Residue0$LookResidue0)var1).info).grouping;
      int var10 = var7.phrasebook.dim;
      int var11;
      int var14 = ((var11 = (var8.end - var8.begin) / var9) + var10 - 1) / var10;
      if (_2inverse_partword == null || _2inverse_partword.length < var14) {
         _2inverse_partword = new int[var14][];
      }

      for(int var6 = 0; var6 < var7.stages; ++var6) {
         var14 = 0;

         for(int var5 = 0; var14 < var11; ++var5) {
            int var12;
            if (var6 == 0) {
               if ((var12 = var7.phrasebook.decode(var0.opb)) == -1) {
                  return 0;
               }

               _2inverse_partword[var5] = var7.decodemap[var12];
               if (_2inverse_partword[var5] == null) {
                  return 0;
               }
            }

            for(int var4 = 0; var4 < var10 && var14 < var11; ++var14) {
               var12 = var8.begin + var14 * var9;
               int var13 = _2inverse_partword[var5][var4];
               CodeBook var15;
               if ((var8.secondstages[var13] & 1 << var6) != 0 && (var15 = var7.fullbooks[var7.partbooks[var13][var6]]) != null && var15.decodevv_add(var2, var12, var3, var0.opb, var9) == -1) {
                  return 0;
               }

               ++var4;
            }
         }
      }

      return 0;
   }

   int inverse(Block var1, Object var2, float[][] var3, int[] var4, int var5) {
      int var6 = 0;

      for(int var7 = 0; var7 < var5; ++var7) {
         if (var4[var7] != 0) {
            var3[var6++] = var3[var7];
         }
      }

      if (var6 != 0) {
         return _01inverse(var1, var2, var3, var6, 0);
      } else {
         return 0;
      }
   }
}
