package com.jcraft.jorbis;

class Mdct {
   // $FF: renamed from: n int
   int field_383;
   int log2n;
   float[] trig;
   int[] bitrev;
   float scale;
   // $FF: renamed from: _x float[]
   float[] field_384 = new float[1024];
   // $FF: renamed from: _w float[]
   float[] field_385 = new float[1024];

   void init(int var1) {
      this.bitrev = new int[var1 / 4];
      this.trig = new float[var1 + var1 / 4];
      this.log2n = (int)Math.rint(Math.log((double)var1) / Math.log(2.0D));
      this.field_383 = var1;
      int var2;
      int var3 = (var2 = 0 + var1 / 2) + 1;
      int var4;
      int var5 = (var4 = var2 + var1 / 2) + 1;

      int var6;
      for(var6 = 0; var6 < var1 / 4; ++var6) {
         this.trig[0 + (var6 << 1)] = (float)Math.cos(3.141592653589793D / (double)var1 * (double)(var6 * 4));
         this.trig[1 + (var6 << 1)] = (float)(-Math.sin(3.141592653589793D / (double)var1 * (double)(var6 * 4)));
         this.trig[var2 + (var6 << 1)] = (float)Math.cos(3.141592653589793D / (double)(var1 * 2) * (double)(var6 * 2 + 1));
         this.trig[var3 + (var6 << 1)] = (float)Math.sin(3.141592653589793D / (double)(var1 * 2) * (double)(var6 * 2 + 1));
      }

      for(var6 = 0; var6 < var1 / 8; ++var6) {
         this.trig[var4 + (var6 << 1)] = (float)Math.cos(3.141592653589793D / (double)var1 * (double)(var6 * 4 + 2));
         this.trig[var5 + (var6 << 1)] = (float)(-Math.sin(3.141592653589793D / (double)var1 * (double)(var6 * 4 + 2)));
      }

      var6 = (1 << this.log2n - 1) - 1;
      var2 = 1 << this.log2n - 2;

      for(var3 = 0; var3 < var1 / 8; ++var3) {
         var4 = 0;

         for(var5 = 0; var2 >>> var5 != 0; ++var5) {
            if ((var2 >>> var5 & var3) != 0) {
               var4 |= 1 << var5;
            }
         }

         this.bitrev[var3 << 1] = ~var4 & var6;
         this.bitrev[(var3 << 1) + 1] = var4;
      }

      this.scale = 4.0F / (float)var1;
   }

   void clear() {
   }

   void forward(float[] var1, float[] var2) {
   }

   synchronized void backward(float[] var1, float[] var2) {
      if (this.field_384.length < this.field_383 / 2) {
         this.field_384 = new float[this.field_383 / 2];
      }

      if (this.field_385.length < this.field_383 / 2) {
         this.field_385 = new float[this.field_383 / 2];
      }

      float[] var3 = this.field_384;
      float[] var4 = this.field_385;
      int var5 = this.field_383 >>> 1;
      int var6 = this.field_383 >>> 2;
      int var7 = this.field_383 >>> 3;
      int var8 = 1;
      int var9 = 0;
      int var10 = var5;

      int var11;
      for(var11 = 0; var11 < var7; ++var11) {
         var10 -= 2;
         var3[var9++] = -var1[var8 + 2] * this.trig[var10 + 1] - var1[var8] * this.trig[var10];
         var3[var9++] = var1[var8] * this.trig[var10 + 1] - var1[var8 + 2] * this.trig[var10];
         var8 += 4;
      }

      var8 = var5 - 4;

      for(var11 = 0; var11 < var7; ++var11) {
         var10 -= 2;
         var3[var9++] = var1[var8] * this.trig[var10 + 1] + var1[var8 + 2] * this.trig[var10];
         var3[var9++] = var1[var8] * this.trig[var10] - var1[var8 + 2] * this.trig[var10 + 1];
         var8 -= 4;
      }

      float[] var17 = this.mdct_kernel(var3, var4, this.field_383, var5, var6, var7);
      var9 = 0;
      var10 = var5;
      var11 = var6;
      int var13 = var6 - 1;
      int var14;
      int var15 = (var14 = var6 + var5) - 1;

      for(var5 = 0; var5 < var6; ++var5) {
         float var16 = var17[var9] * this.trig[var10 + 1] - var17[var9 + 1] * this.trig[var10];
         float var12 = -(var17[var9] * this.trig[var10] + var17[var9 + 1] * this.trig[var10 + 1]);
         var2[var11] = -var16;
         var2[var13] = var16;
         var2[var14] = var12;
         var2[var15] = var12;
         ++var11;
         --var13;
         ++var14;
         --var15;
         var9 += 2;
         var10 += 2;
      }

   }

   private float[] mdct_kernel(float[] var1, float[] var2, int var3, int var4, int var5, int var6) {
      int var7 = var5;
      int var8 = 0;
      int var9 = var4;

      int var10;
      for(var10 = 0; var10 < var5; ++var10) {
         float var11 = var1[var7] - var1[var8];
         var2[var5 + var10] = var1[var7++] + var1[var8++];
         float var12 = var1[var7] - var1[var8];
         var9 -= 4;
         var2[var10++] = var11 * this.trig[var9] + var12 * this.trig[var9 + 1];
         var2[var10] = var12 * this.trig[var9] - var11 * this.trig[var9 + 1];
         var2[var5 + var10] = var1[var7++] + var1[var8++];
      }

      int var13;
      float var14;
      float var15;
      float var16;
      float var17;
      int var23;
      int var24;
      for(var10 = 0; var10 < this.log2n - 3; ++var10) {
         var23 = var3 >>> var10 + 2;
         var24 = 1 << var10 + 3;
         var7 = var4 - 2;
         var9 = 0;

         for(var8 = 0; var8 < var23 >>> 2; ++var8) {
            var13 = var7;
            var5 = var7 - (var23 >> 1);
            var14 = this.trig[var9];
            var16 = this.trig[var9 + 1];
            var7 -= 2;
            ++var23;

            for(int var18 = 0; var18 < 2 << var10; ++var18) {
               var17 = var2[var13] - var2[var5];
               var1[var13] = var2[var13] + var2[var5];
               ++var13;
               float var10000 = var2[var13];
               ++var5;
               var15 = var10000 - var2[var5];
               var1[var13] = var2[var13] + var2[var5];
               var1[var5] = var15 * var14 - var17 * var16;
               var1[var5 - 1] = var17 * var14 + var15 * var16;
               var13 -= var23;
               var5 -= var23;
            }

            --var23;
            var9 += var24;
         }

         float[] var21 = var2;
         var2 = var1;
         var1 = var21;
      }

      var10 = var3;
      var23 = 0;
      var24 = 0;
      var7 = var4 - 1;

      for(var5 = 0; var5 < var6; ++var5) {
         var8 = this.bitrev[var23++];
         var13 = this.bitrev[var23++];
         var14 = var2[var8] - var2[var13 + 1];
         var15 = var2[var8 - 1] + var2[var13];
         var16 = var2[var8] + var2[var13 + 1];
         var17 = var2[var8 - 1] - var2[var13];
         float var25 = var14 * this.trig[var10];
         float var19 = var15 * this.trig[var10++];
         float var20 = var14 * this.trig[var10];
         float var22 = var15 * this.trig[var10++];
         var1[var24++] = (var16 + var20 + var19) * 0.5F;
         var1[var7--] = (-var17 + var22 - var25) * 0.5F;
         var1[var24++] = (var17 + var22 - var25) * 0.5F;
         var1[var7--] = (var16 - var20 - var19) * 0.5F;
      }

      return var1;
   }
}
