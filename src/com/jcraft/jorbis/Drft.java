package com.jcraft.jorbis;

class Drft {
   // $FF: renamed from: n int
   int field_1049;
   float[] trigcache;
   int[] splitcache;
   static int[] ntryh = new int[]{4, 2, 3, 5};
   static float tpi = 6.2831855F;
   static float hsqt2 = 0.70710677F;
   static float taui = 0.8660254F;
   static float taur = -0.5F;
   static float sqrt2 = 1.4142135F;

   void backward(float[] var1) {
      if (this.field_1049 != 1) {
         drftb1(this.field_1049, var1, this.trigcache, this.trigcache, this.field_1049, this.splitcache);
      }
   }

   void init(int var1) {
      this.field_1049 = var1;
      this.trigcache = new float[var1 * 3];
      this.splitcache = new int[32];
      fdrffti(var1, this.trigcache, this.splitcache);
   }

   void clear() {
      if (this.trigcache != null) {
         this.trigcache = null;
      }

      if (this.splitcache != null) {
         this.splitcache = null;
      }

   }

   static void drfti1(int var0, float[] var1, int var2, int[] var3) {
      int var4 = 0;
      int var9 = -1;
      int var7 = var0;
      int var10 = 0;
      byte var5 = 101;

      while(true) {
         while(true) {
            int var8;
            switch(var5) {
            case 101:
               ++var9;
               if (var9 < 4) {
                  var4 = ntryh[var9];
               } else {
                  var4 += 2;
               }
            case 104:
               int var19 = var7 / var4;
               if (var7 - var4 * var19 != 0) {
                  var5 = 101;
                  break;
               } else {
                  ++var10;
                  var3[var10 + 1] = var4;
                  var7 = var19;
                  if (var4 != 2) {
                     var5 = 107;
                     break;
                  } else if (var10 == 1) {
                     var5 = 107;
                     break;
                  } else {
                     for(var8 = 1; var8 < var10; ++var8) {
                        var19 = var10 - var8 + 1;
                        var3[var19 + 1] = var3[var19];
                     }

                     var3[2] = 2;
                  }
               }
            case 107:
               if (var7 == 1) {
                  var3[0] = var0;
                  var3[1] = var10;
                  float var21 = tpi / (float)var0;
                  int var15 = 0;
                  int var18 = var10 - 1;
                  int var11 = 1;
                  if (var18 == 0) {
                     return;
                  }

                  for(var10 = 0; var10 < var18; ++var10) {
                     var4 = var3[var10 + 2];
                     int var13 = 0;
                     int var12 = var11 * var4;
                     int var16 = var0 / var12;
                     int var17 = var4 - 1;

                     for(var9 = 0; var9 < var17; ++var9) {
                        var13 += var11;
                        var8 = var15;
                        float var6 = (float)var13 * var21;
                        float var22 = 0.0F;

                        for(int var14 = 2; var14 < var16; var14 += 2) {
                           float var20 = ++var22 * var6;
                           var1[var2 + var8++] = (float)Math.cos((double)var20);
                           var1[var2 + var8++] = (float)Math.sin((double)var20);
                        }

                        var15 += var16;
                     }

                     var11 = var12;
                  }

                  return;
               }

               var5 = 104;
            }
         }
      }
   }

   static void fdrffti(int var0, float[] var1, int[] var2) {
      if (var0 != 1) {
         drfti1(var0, var1, var0, var2);
      }
   }

   static void dradf2(int var0, int var1, float[] var2, float[] var3, float[] var4, int var5) {
      int var11 = 0;
      int var12;
      int var10 = var12 = var1 * var0;
      int var13 = var0 << 1;

      int var7;
      for(var7 = 0; var7 < var1; ++var7) {
         var3[var11 << 1] = var2[var11] + var2[var12];
         var3[(var11 << 1) + var13 - 1] = var2[var11] - var2[var12];
         var11 += var0;
         var12 += var0;
      }

      if (var0 >= 2) {
         if (var0 != 2) {
            var11 = 0;
            var12 = var10;

            for(var7 = 0; var7 < var1; ++var7) {
               var13 = var12;
               int var14 = (var11 << 1) + (var0 << 1);
               int var15 = var11;
               int var16 = var11 + var11;

               for(int var6 = 2; var6 < var0; var6 += 2) {
                  var13 += 2;
                  var14 -= 2;
                  var15 += 2;
                  var16 += 2;
                  float var9 = var4[var5 + var6 - 2] * var2[var13 - 1] + var4[var5 + var6 - 1] * var2[var13];
                  float var8 = var4[var5 + var6 - 2] * var2[var13] - var4[var5 + var6 - 1] * var2[var13 - 1];
                  var3[var16] = var2[var15] + var8;
                  var3[var14] = var8 - var2[var15];
                  var3[var16 - 1] = var2[var15 - 1] + var9;
                  var3[var14 - 1] = var2[var15 - 1] - var9;
               }

               var11 += var0;
               var12 += var0;
            }

            if (var0 % 2 == 1) {
               return;
            }
         }

         var11 = var0;
         var13 = var12 = var0 - 1;
         var12 += var10;

         for(var7 = 0; var7 < var1; ++var7) {
            var3[var11] = -var2[var12];
            var3[var11 - 1] = var2[var13];
            var11 += var0 << 1;
            var12 += var0;
            var13 += var0;
         }

      }
   }

   static void dradf4(int var0, int var1, float[] var2, float[] var3, float[] var4, int var5, float[] var6, int var7, float[] var8, int var9) {
      int var12;
      int var13 = var12 = var1 * var0;
      int var16 = var12 << 1;
      int var14 = var12 + var16;
      int var15 = 0;

      int var11;
      int var17;
      float var20;
      float var24;
      for(var11 = 0; var11 < var1; ++var11) {
         var24 = var2[var13] + var2[var14];
         var20 = var2[var15] + var2[var16];
         var3[var17 = var15 << 2] = var24 + var20;
         var3[(var0 << 2) + var17 - 1] = var20 - var24;
         var3[(var17 += var0 << 1) - 1] = var2[var15] - var2[var16];
         var3[var17] = var2[var14] - var2[var13];
         var13 += var0;
         var14 += var0;
         var15 += var0;
         var16 += var0;
      }

      if (var0 >= 2) {
         int var18;
         float var21;
         if (var0 != 2) {
            var13 = 0;

            for(var11 = 0; var11 < var1; ++var11) {
               var14 = var13;
               var16 = var13 << 2;
               var17 = (var18 = var0 << 1) + var16;

               for(int var10 = 2; var10 < var0; var10 += 2) {
                  var14 += 2;
                  var16 += 2;
                  var17 -= 2;
                  var15 = var14 + var12;
                  var21 = var4[var5 + var10 - 2] * var2[var15 - 1] + var4[var5 + var10 - 1] * var2[var15];
                  float var19 = var4[var5 + var10 - 2] * var2[var15] - var4[var5 + var10 - 1] * var2[var15 - 1];
                  var15 += var12;
                  float var22 = var6[var7 + var10 - 2] * var2[var15 - 1] + var6[var7 + var10 - 1] * var2[var15];
                  var20 = var6[var7 + var10 - 2] * var2[var15] - var6[var7 + var10 - 1] * var2[var15 - 1];
                  var15 += var12;
                  float var23 = var8[var9 + var10 - 2] * var2[var15 - 1] + var8[var9 + var10 - 1] * var2[var15];
                  float var26 = var8[var9 + var10 - 2] * var2[var15] - var8[var9 + var10 - 1] * var2[var15 - 1];
                  var24 = var21 + var23;
                  float var25 = var23 - var21;
                  var21 = var19 + var26;
                  var23 = var19 - var26;
                  var26 = var2[var14] + var20;
                  var19 = var2[var14] - var20;
                  var20 = var2[var14 - 1] + var22;
                  var22 = var2[var14 - 1] - var22;
                  var3[var16 - 1] = var24 + var20;
                  var3[var16] = var21 + var26;
                  var3[var17 - 1] = var22 - var23;
                  var3[var17] = var25 - var19;
                  var3[var16 + var18 - 1] = var23 + var22;
                  var3[var16 + var18] = var25 + var19;
                  var3[var17 + var18 - 1] = var20 - var24;
                  var3[var17 + var18] = var21 - var26;
               }

               var13 += var0;
            }

            if ((var0 & 1) != 0) {
               return;
            }
         }

         var14 = (var13 = var12 + var0 - 1) + (var12 << 1);
         var15 = var0 << 2;
         var16 = var0;
         var17 = var0 << 1;
         var18 = var0;

         for(var11 = 0; var11 < var1; ++var11) {
            var21 = -hsqt2 * (var2[var13] + var2[var14]);
            var24 = hsqt2 * (var2[var13] - var2[var14]);
            var3[var16 - 1] = var24 + var2[var18 - 1];
            var3[var16 + var17 - 1] = var2[var18 - 1] - var24;
            var3[var16] = var21 - var2[var13 + var12];
            var3[var16 + var17] = var21 + var2[var13 + var12];
            var13 += var0;
            var14 += var0;
            var16 += var15;
            var18 += var0;
         }

      }
   }

   static void dradfg(int var0, int var1, int var2, int var3, float[] var4, float[] var5, float[] var6, float[] var7, float[] var8, float[] var9, int var10) {
      float var11;
      Math.cos((double)(var11 = tpi / (float)var1));
      Math.sin((double)var11);
   }

   static void drftf1(int var0, float[] var1, float[] var2, float[] var3, int[] var4) {
      int var9 = var4[1];
      int var8 = 1;
      int var7 = var0;
      int var11 = var0;

      for(int var5 = 0; var5 < var9; ++var5) {
         int var6 = var9 - var5;
         int var10 = var4[var6 + 1];
         var6 = var7 / var10;
         int var12 = (var7 = var0 / var7) * var6;
         var11 -= (var10 - 1) * var7;
         var8 = 1 - var8;
         byte var13 = 100;

         label62:
         while(true) {
            switch(var13) {
            case 100:
               if (var10 != 4) {
                  var13 = 102;
               } else {
                  int var16;
                  int var14 = (var16 = var11 + var7) + var7;
                  if (var8 != 0) {
                     dradf4(var7, var6, var2, var1, var3, var11 - 1, var3, var16 - 1, var3, var14 - 1);
                  } else {
                     dradf4(var7, var6, var1, var2, var3, var11 - 1, var3, var16 - 1, var3, var14 - 1);
                  }

                  var13 = 110;
               }
            case 101:
            case 105:
            case 106:
            case 107:
            case 108:
            default:
               break;
            case 102:
               if (var10 != 2) {
                  var13 = 104;
               } else if (var8 != 0) {
                  var13 = 103;
               } else {
                  dradf2(var7, var6, var1, var2, var3, var11 - 1);
                  var13 = 110;
               }
               break;
            case 103:
               dradf2(var7, var6, var2, var1, var3, var11 - 1);
            case 104:
               if (var7 == 1) {
                  var8 = 1 - var8;
               }

               if (var8 != 0) {
                  var13 = 109;
               } else {
                  dradfg(var7, var10, var6, var12, var1, var1, var1, var2, var2, var3, var11 - 1);
                  var8 = 1;
                  var13 = 110;
               }
               break;
            case 109:
               dradfg(var7, var10, var6, var12, var2, var2, var2, var1, var1, var3, var11 - 1);
               var8 = 0;
            case 110:
               break label62;
            }
         }

         var7 = var6;
      }

      if (var8 != 1) {
         for(int var15 = 0; var15 < var0; ++var15) {
            var1[var15] = var2[var15];
         }

      }
   }

   static void dradb2(int var0, int var1, float[] var2, float[] var3, float[] var4, int var5) {
      int var8 = var1 * var0;
      int var9 = 0;
      int var6 = 0;
      int var10 = (var0 << 1) - 1;

      int var7;
      for(var7 = 0; var7 < var1; ++var7) {
         var3[var9] = var2[var6] + var2[var10 + var6];
         var3[var9 + var8] = var2[var6] - var2[var10 + var6];
         var6 = (var9 += var0) << 1;
      }

      if (var0 >= 2) {
         if (var0 != 2) {
            var9 = 0;
            var6 = 0;

            for(var7 = 0; var7 < var1; ++var7) {
               var10 = var9;
               int var11 = var6;
               int var12 = var6 + (var0 << 1);
               int var13 = var8 + var9;

               for(var6 = 2; var6 < var0; var6 += 2) {
                  var10 += 2;
                  var11 += 2;
                  var12 -= 2;
                  var13 += 2;
                  var3[var10 - 1] = var2[var11 - 1] + var2[var12 - 1];
                  float var15 = var2[var11 - 1] - var2[var12 - 1];
                  var3[var10] = var2[var11] - var2[var12];
                  float var14 = var2[var11] + var2[var12];
                  var3[var13 - 1] = var4[var5 + var6 - 2] * var15 - var4[var5 + var6 - 1] * var14;
                  var3[var13] = var4[var5 + var6 - 2] * var14 + var4[var5 + var6 - 1] * var15;
               }

               var6 = (var9 += var0) << 1;
            }

            if (var0 % 2 == 1) {
               return;
            }
         }

         var6 = var9 = var0 - 1;

         for(var7 = 0; var7 < var1; ++var7) {
            var3[var9] = var2[var6] + var2[var6];
            var3[var9 + var8] = -(var2[var6 + 1] + var2[var6 + 1]);
            var9 += var0;
            var6 += var0 << 1;
         }

      }
   }

   static void dradb3(int var0, int var1, float[] var2, float[] var3, float[] var4, int var5, float[] var6, int var7) {
      int var10 = var1 * var0;
      int var11 = 0;
      int var8 = var10 << 1;
      int var12 = var0 << 1;
      int var13 = var0 + var12;
      int var14 = 0;

      int var9;
      float var19;
      float var20;
      float var21;
      for(var9 = 0; var9 < var1; ++var9) {
         var19 = var2[var12 - 1] + var2[var12 - 1];
         var21 = var2[var14] + taur * var19;
         var3[var11] = var2[var14] + var19;
         var20 = taui * (var2[var12] + var2[var12]);
         var3[var11 + var10] = var21 - var20;
         var3[var11 + var8] = var21 + var20;
         var11 += var0;
         var12 += var13;
         var14 += var13;
      }

      if (var0 != 1) {
         var11 = 0;
         var12 = var0 << 1;

         for(var9 = 0; var9 < var1; ++var9) {
            int var15;
            var13 = var14 = (var15 = var11 + (var11 << 1)) + var12;
            int var16 = var11;
            int var17;
            int var18 = (var17 = var11 + var10) + var10;

            for(var8 = 2; var8 < var0; var8 += 2) {
               var14 += 2;
               var13 -= 2;
               var15 += 2;
               var16 += 2;
               var17 += 2;
               var18 += 2;
               var19 = var2[var14 - 1] + var2[var13 - 1];
               var21 = var2[var15 - 1] + taur * var19;
               var3[var16 - 1] = var2[var15 - 1] + var19;
               var20 = var2[var14] - var2[var13];
               var19 = var2[var15] + taur * var20;
               var3[var16] = var2[var15] + var20;
               float var22 = taui * (var2[var14 - 1] - var2[var13 - 1]);
               var20 = taui * (var2[var14] + var2[var13]);
               float var23 = var21 - var20;
               var21 += var20;
               var20 = var19 + var22;
               var19 -= var22;
               var3[var17 - 1] = var4[var5 + var8 - 2] * var23 - var4[var5 + var8 - 1] * var20;
               var3[var17] = var4[var5 + var8 - 2] * var20 + var4[var5 + var8 - 1] * var23;
               var3[var18 - 1] = var6[var7 + var8 - 2] * var21 - var6[var7 + var8 - 1] * var19;
               var3[var18] = var6[var7 + var8 - 2] * var19 + var6[var7 + var8 - 1] * var21;
            }

            var11 += var0;
         }

      }
   }

   static void dradb4(int var0, int var1, float[] var2, float[] var3, float[] var4, int var5, float[] var6, int var7, float[] var8, int var9) {
      int var12 = var1 * var0;
      int var13 = 0;
      int var14 = var0 << 2;
      int var15 = 0;
      int var18 = var0 << 1;

      int var11;
      int var16;
      int var17;
      float var24;
      float var25;
      float var26;
      float var27;
      for(var11 = 0; var11 < var1; ++var11) {
         var16 = var15 + var18;
         var25 = var2[var16 - 1] + var2[var16 - 1];
         var27 = var2[var16] + var2[var16];
         var26 = var2[var15] - var2[(var16 += var18) - 1];
         var24 = var2[var15] + var2[var16 - 1];
         var3[var13] = var24 + var25;
         var3[var17 = var13 + var12] = var26 - var27;
         var3[var17 += var12] = var24 - var25;
         var3[var17 + var12] = var26 + var27;
         var13 += var0;
         var15 += var14;
      }

      if (var0 >= 2) {
         float var20;
         float var21;
         if (var0 != 2) {
            var13 = 0;

            for(var11 = 0; var11 < var1; ++var11) {
               var17 = (var16 = var15 = (var14 = var13 << 2) + var18) + var18;
               int var19 = var13;

               for(int var10 = 2; var10 < var0; var10 += 2) {
                  var14 += 2;
                  var15 += 2;
                  var16 -= 2;
                  var17 -= 2;
                  var19 += 2;
                  var20 = var2[var14] + var2[var17];
                  var21 = var2[var14] - var2[var17];
                  float var22 = var2[var15] - var2[var16];
                  var27 = var2[var15] + var2[var16];
                  var26 = var2[var14 - 1] - var2[var17 - 1];
                  var24 = var2[var14 - 1] + var2[var17 - 1];
                  float var23 = var2[var15 - 1] - var2[var16 - 1];
                  var25 = var2[var15 - 1] + var2[var16 - 1];
                  var3[var19 - 1] = var24 + var25;
                  var25 = var24 - var25;
                  var3[var19] = var21 + var22;
                  var22 = var21 - var22;
                  var24 = var26 - var27;
                  var26 += var27;
                  var21 = var20 + var23;
                  var23 = var20 - var23;
                  int var28;
                  var3[(var28 = var19 + var12) - 1] = var4[var5 + var10 - 2] * var24 - var4[var5 + var10 - 1] * var21;
                  var3[var28] = var4[var5 + var10 - 2] * var21 + var4[var5 + var10 - 1] * var24;
                  var3[(var28 += var12) - 1] = var6[var7 + var10 - 2] * var25 - var6[var7 + var10 - 1] * var22;
                  var3[var28] = var6[var7 + var10 - 2] * var22 + var6[var7 + var10 - 1] * var25;
                  var3[(var28 += var12) - 1] = var8[var9 + var10 - 2] * var26 - var8[var9 + var10 - 1] * var23;
                  var3[var28] = var8[var9 + var10 - 2] * var23 + var8[var9 + var10 - 1] * var26;
               }

               var13 += var0;
            }

            if (var0 % 2 == 1) {
               return;
            }
         }

         var13 = var0;
         var14 = var0 << 2;
         var15 = var0 - 1;
         var16 = var0 + var18;

         for(var11 = 0; var11 < var1; ++var11) {
            var20 = var2[var13] + var2[var16];
            var21 = var2[var16] - var2[var13];
            var26 = var2[var13 - 1] - var2[var16 - 1];
            var24 = var2[var13 - 1] + var2[var16 - 1];
            var3[var15] = var24 + var24;
            var3[var17 = var15 + var12] = sqrt2 * (var26 - var20);
            var3[var17 += var12] = var21 + var21;
            var3[var17 + var12] = -sqrt2 * (var26 + var20);
            var15 += var0;
            var13 += var14;
            var16 += var14;
         }

      }
   }

   static void dradbg(int var0, int var1, int var2, int var3, float[] var4, float[] var5, float[] var6, float[] var7, float[] var8, float[] var9, int var10) {
      int var11 = 0;
      int var15 = 0;
      int var25 = 0;
      int var33 = 0;
      float var34 = 0.0F;
      float var35 = 0.0F;
      int var36 = 0;
      short var12 = 100;

      while(true) {
         int var13;
         int var14;
         int var16;
         int var17;
         int var18;
         int var19;
         int var20;
         int var21;
         int var22;
         int var23;
         int var24;
         int var26;
         int var27;
         int var37;
         int var39;
         switch(var12) {
         case 100:
            var25 = var1 * var0;
            var15 = var2 * var0;
            float var38;
            var34 = (float)Math.cos((double)(var38 = tpi / (float)var1));
            var35 = (float)Math.sin((double)var38);
            var33 = var0 - 1 >>> 1;
            var36 = var1;
            var11 = var1 + 1 >>> 1;
            if (var0 < var2) {
               var12 = 103;
               break;
            }

            var16 = 0;
            var17 = 0;

            for(var14 = 0; var14 < var2; ++var14) {
               var18 = var16;
               var19 = var17;

               for(var39 = 0; var39 < var0; ++var39) {
                  var7[var18] = var4[var19];
                  ++var18;
                  ++var19;
               }

               var16 += var0;
               var17 += var25;
            }

            var12 = 106;
            break;
         case 103:
            var16 = 0;

            for(var39 = 0; var39 < var0; ++var39) {
               var17 = var16;
               var18 = var16;

               for(var14 = 0; var14 < var2; ++var14) {
                  var7[var17] = var4[var18];
                  var17 += var0;
                  var18 += var25;
               }

               ++var16;
            }
         case 106:
            var16 = 0;
            var17 = var36 * var15;
            var22 = var20 = var0 << 1;

            for(var13 = 1; var13 < var11; ++var13) {
               var16 += var15;
               var17 -= var15;
               var18 = var16;
               var19 = var17;
               var21 = var20;

               for(var14 = 0; var14 < var2; ++var14) {
                  var7[var18] = var4[var21 - 1] + var4[var21 - 1];
                  var7[var19] = var4[var21] + var4[var21];
                  var18 += var0;
                  var19 += var0;
                  var21 += var25;
               }

               var20 += var22;
            }

            if (var0 == 1) {
               var12 = 116;
            } else {
               if (var33 < var2) {
                  var12 = 112;
                  break;
               }

               var16 = 0;
               var17 = var36 * var15;
               var22 = 0;

               for(var13 = 1; var13 < var11; ++var13) {
                  var16 += var15;
                  var17 -= var15;
                  var18 = var16;
                  var19 = var17;
                  var23 = var22 += var0 << 1;

                  for(var14 = 0; var14 < var2; ++var14) {
                     var20 = var18;
                     var21 = var19;
                     var24 = var23;
                     var26 = var23;

                     for(var39 = 2; var39 < var0; var39 += 2) {
                        var20 += 2;
                        var21 += 2;
                        var24 += 2;
                        var26 -= 2;
                        var7[var20 - 1] = var4[var24 - 1] + var4[var26 - 1];
                        var7[var21 - 1] = var4[var24 - 1] - var4[var26 - 1];
                        var7[var20] = var4[var24] - var4[var26];
                        var7[var21] = var4[var24] + var4[var26];
                     }

                     var18 += var0;
                     var19 += var0;
                     var23 += var25;
                  }
               }

               var12 = 116;
            }
            break;
         case 112:
            var16 = 0;
            var17 = var36 * var15;
            var22 = 0;

            for(var13 = 1; var13 < var11; ++var13) {
               var16 += var15;
               var17 -= var15;
               var18 = var16;
               var19 = var17;
               var23 = var22 += var0 << 1;
               var24 = var22;

               for(var39 = 2; var39 < var0; var39 += 2) {
                  var18 += 2;
                  var19 += 2;
                  var23 += 2;
                  var24 -= 2;
                  var20 = var18;
                  var21 = var19;
                  var26 = var23;
                  var27 = var24;

                  for(var14 = 0; var14 < var2; ++var14) {
                     var7[var20 - 1] = var4[var26 - 1] + var4[var27 - 1];
                     var7[var21 - 1] = var4[var26 - 1] - var4[var27 - 1];
                     var7[var20] = var4[var26] - var4[var27];
                     var7[var21] = var4[var26] + var4[var27];
                     var20 += var0;
                     var21 += var0;
                     var26 += var25;
                     var27 += var25;
                  }
               }
            }
         case 116:
            float var30 = 1.0F;
            float var28 = 0.0F;
            var16 = 0;
            var24 = var17 = var36 * var3;
            var18 = (var1 - 1) * var3;

            for(var39 = 1; var39 < var11; ++var39) {
               var16 += var3;
               var17 -= var3;
               float var40 = var34 * var30 - var35 * var28;
               var28 = var34 * var28 + var35 * var30;
               var30 = var40;
               var19 = var16;
               var20 = var17;
               var21 = 0;
               var22 = var3;
               var23 = var18;

               for(var14 = 0; var14 < var3; ++var14) {
                  var6[var19++] = var8[var21++] + var30 * var8[var22++];
                  var6[var20++] = var28 * var8[var23++];
               }

               float var42 = var30;
               float var32 = var28;
               float var31 = var30;
               float var29 = var28;
               var21 = var3;
               var22 = var24 - var3;

               for(var13 = 2; var13 < var11; ++var13) {
                  var21 += var3;
                  var22 -= var3;
                  float var41 = var42 * var31 - var32 * var29;
                  var29 = var42 * var29 + var32 * var31;
                  var31 = var41;
                  var19 = var16;
                  var20 = var17;
                  var26 = var21;
                  var27 = var22;

                  for(var14 = 0; var14 < var3; ++var14) {
                     int var10001 = var19++;
                     var6[var10001] += var31 * var8[var26++];
                     var10001 = var20++;
                     var6[var10001] += var29 * var8[var27++];
                  }
               }
            }

            var16 = 0;

            for(var13 = 1; var13 < var11; ++var13) {
               var17 = var16 += var3;

               for(var14 = 0; var14 < var3; ++var14) {
                  var8[var14] += var8[var17++];
               }
            }

            var16 = 0;
            var17 = var36 * var15;

            for(var13 = 1; var13 < var11; ++var13) {
               var16 += var15;
               var17 -= var15;
               var18 = var16;
               var19 = var17;

               for(var14 = 0; var14 < var2; ++var14) {
                  var7[var18] = var5[var18] - var5[var19];
                  var7[var19] = var5[var18] + var5[var19];
                  var18 += var0;
                  var19 += var0;
               }
            }

            if (var0 == 1) {
               var12 = 132;
            } else {
               if (var33 < var2) {
                  var12 = 128;
                  break;
               }

               var16 = 0;
               var17 = var36 * var15;

               for(var13 = 1; var13 < var11; ++var13) {
                  var16 += var15;
                  var17 -= var15;
                  var18 = var16;
                  var19 = var17;

                  for(var14 = 0; var14 < var2; ++var14) {
                     var20 = var18;
                     var21 = var19;

                     for(var39 = 2; var39 < var0; var39 += 2) {
                        var20 += 2;
                        var21 += 2;
                        var7[var20 - 1] = var5[var20 - 1] - var5[var21];
                        var7[var21 - 1] = var5[var20 - 1] + var5[var21];
                        var7[var20] = var5[var20] + var5[var21 - 1];
                        var7[var21] = var5[var20] - var5[var21 - 1];
                     }

                     var18 += var0;
                     var19 += var0;
                  }
               }

               var12 = 132;
            }
            break;
         case 128:
            var16 = 0;
            var17 = var36 * var15;

            for(var13 = 1; var13 < var11; ++var13) {
               var16 += var15;
               var17 -= var15;
               var18 = var16;
               var19 = var17;

               for(var39 = 2; var39 < var0; var39 += 2) {
                  var18 += 2;
                  var19 += 2;
                  var20 = var18;
                  var21 = var19;

                  for(var14 = 0; var14 < var2; ++var14) {
                     var7[var20 - 1] = var5[var20 - 1] - var5[var21];
                     var7[var21 - 1] = var5[var20 - 1] + var5[var21];
                     var7[var20] = var5[var20] + var5[var21 - 1];
                     var7[var21] = var5[var20] - var5[var21 - 1];
                     var20 += var0;
                     var21 += var0;
                  }
               }
            }
         case 132:
            if (var0 == 1) {
               return;
            }

            for(var14 = 0; var14 < var3; ++var14) {
               var6[var14] = var8[var14];
            }

            var16 = 0;

            for(var13 = 1; var13 < var1; ++var13) {
               var17 = var16 += var15;

               for(var14 = 0; var14 < var2; ++var14) {
                  var5[var17] = var7[var17];
                  var17 += var0;
               }
            }

            if (var33 <= var2) {
               var37 = -var0 - 1;
               var16 = 0;

               for(var13 = 1; var13 < var1; ++var13) {
                  var37 += var0;
                  var16 += var15;
                  var3 = var37;
                  var17 = var16;

                  for(var39 = 2; var39 < var0; var39 += 2) {
                     var17 += 2;
                     var3 += 2;
                     var18 = var17;

                     for(var14 = 0; var14 < var2; ++var14) {
                        var5[var18 - 1] = var9[var10 + var3 - 1] * var7[var18 - 1] - var9[var10 + var3] * var7[var18];
                        var5[var18] = var9[var10 + var3 - 1] * var7[var18] + var9[var10 + var3] * var7[var18 - 1];
                        var18 += var0;
                     }
                  }
               }

               return;
            }

            var12 = 139;
            break;
         case 139:
            var37 = -var0 - 1;
            var16 = 0;

            for(var13 = 1; var13 < var1; ++var13) {
               var37 += var0;
               var17 = var16 += var15;

               for(var14 = 0; var14 < var2; ++var14) {
                  var3 = var37;
                  var18 = var17;

                  for(var39 = 2; var39 < var0; var39 += 2) {
                     var3 += 2;
                     var18 += 2;
                     var5[var18 - 1] = var9[var10 + var3 - 1] * var7[var18 - 1] - var9[var10 + var3] * var7[var18];
                     var5[var18] = var9[var10 + var3 - 1] * var7[var18] + var9[var10 + var3] * var7[var18 - 1];
                  }

                  var17 += var0;
               }
            }

            return;
         }
      }
   }

   static void drftb1(int var0, float[] var1, float[] var2, float[] var3, int var4, int[] var5) {
      int var8 = 0;
      int var11 = 0;
      int var15 = 0;
      int var16 = 0;
      int var10 = var5[1];
      int var9 = 0;
      int var7 = 1;
      int var12 = 1;

      for(int var6 = 0; var6 < var10; ++var6) {
         byte var13 = 100;

         label71:
         while(true) {
            int var18;
            switch(var13) {
            case 100:
               var8 = (var11 = var5[var6 + 2]) * var7;
               var16 = (var15 = var0 / var8) * var7;
               if (var11 != 4) {
                  var13 = 103;
               } else {
                  int var14 = (var18 = var12 + var15) + var15;
                  if (var9 != 0) {
                     dradb4(var15, var7, var2, var1, var3, var4 + var12 - 1, var3, var4 + var18 - 1, var3, var4 + var14 - 1);
                  } else {
                     dradb4(var15, var7, var1, var2, var3, var4 + var12 - 1, var3, var4 + var18 - 1, var3, var4 + var14 - 1);
                  }

                  var9 = 1 - var9;
                  var13 = 115;
               }
               break;
            case 103:
               if (var11 != 2) {
                  var13 = 106;
               } else {
                  if (var9 != 0) {
                     dradb2(var15, var7, var2, var1, var3, var4 + var12 - 1);
                  } else {
                     dradb2(var15, var7, var1, var2, var3, var4 + var12 - 1);
                  }

                  var9 = 1 - var9;
                  var13 = 115;
               }
               break;
            case 106:
               if (var11 != 3) {
                  var13 = 109;
               } else {
                  var18 = var12 + var15;
                  if (var9 != 0) {
                     dradb3(var15, var7, var2, var1, var3, var4 + var12 - 1, var3, var4 + var18 - 1);
                  } else {
                     dradb3(var15, var7, var1, var2, var3, var4 + var12 - 1, var3, var4 + var18 - 1);
                  }

                  var9 = 1 - var9;
                  var13 = 115;
               }
               break;
            case 109:
               if (var9 != 0) {
                  dradbg(var15, var11, var7, var16, var2, var2, var2, var1, var1, var3, var4 + var12 - 1);
               } else {
                  dradbg(var15, var11, var7, var16, var1, var1, var1, var2, var2, var3, var4 + var12 - 1);
               }

               if (var15 == 1) {
                  var9 = 1 - var9;
               }
            case 115:
               break label71;
            }
         }

         var7 = var8;
         var12 += (var11 - 1) * var15;
      }

      if (var9 != 0) {
         for(int var17 = 0; var17 < var0; ++var17) {
            var1[var17] = var2[var17];
         }

      }
   }
}
