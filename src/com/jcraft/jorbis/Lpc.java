package com.jcraft.jorbis;

class Lpc {
   Drft fft = new Drft();
   // $FF: renamed from: ln int
   int field_219;
   // $FF: renamed from: m int
   int field_220;

   static float lpc_from_data(float[] var0, float[] var1, int var2, int var3) {
      float[] var4 = new float[var3 + 1];

      int var5;
      int var6;
      float var7;
      for(var6 = var3 + 1; var6-- != 0; var4[var6] = var7) {
         var7 = 0.0F;

         for(var5 = var6; var5 < var2; ++var5) {
            var7 += var0[var5] * var0[var5 - var6];
         }
      }

      float var8 = var4[0];

      for(var5 = 0; var5 < var3; ++var5) {
         var7 = -var4[var5 + 1];
         if (var8 == 0.0F) {
            for(var2 = 0; var2 < var3; ++var2) {
               var1[var2] = 0.0F;
            }

            return 0.0F;
         }

         for(var6 = 0; var6 < var5; ++var6) {
            var7 -= var1[var6] * var4[var5 - var6];
         }

         var7 /= var8;
         var1[var5] = var7;

         for(var6 = 0; var6 < var5 / 2; ++var6) {
            float var9 = var1[var6];
            var1[var6] += var7 * var1[var5 - 1 - var6];
            var1[var5 - 1 - var6] += var7 * var9;
         }

         if (var5 % 2 != 0) {
            var1[var6] += var1[var6] * var7;
         }

         var8 = (float)((double)var8 * (1.0D - (double)(var7 * var7)));
      }

      return var8;
   }

   float lpc_from_curve(float[] var1, float[] var2) {
      int var3 = this.field_219;
      float[] var4 = new float[this.field_219 + var3];
      float var5 = (float)(0.5D / (double)var3);

      int var6;
      for(var6 = 0; var6 < var3; ++var6) {
         var4[var6 << 1] = var1[var6] * var5;
         var4[(var6 << 1) + 1] = 0.0F;
      }

      var4[(var3 << 1) - 1] = var1[var3 - 1] * var5;
      var3 <<= 1;
      this.fft.backward(var4);
      var6 = 0;

      for(int var7 = var3 / 2; var6 < var3 / 2; var4[var7++] = var5) {
         var5 = var4[var6];
         var4[var6++] = var4[var7];
      }

      return lpc_from_data(var4, var2, var3, this.field_220);
   }

   void init(int var1, int var2) {
      this.field_219 = var1;
      this.field_220 = var2;
      this.fft.init(var1 << 1);
   }

   void clear() {
      this.fft.clear();
   }

   static float FAST_HYPOT(float var0, float var1) {
      return (float)Math.sqrt((double)(var0 * var0 + var1 * var1));
   }

   void lpc_to_curve(float[] var1, float[] var2, float var3) {
      int var4;
      for(var4 = 0; var4 < this.field_219 << 1; ++var4) {
         var1[var4] = 0.0F;
      }

      if (var3 != 0.0F) {
         for(var4 = 0; var4 < this.field_220; ++var4) {
            var1[(var4 << 1) + 1] = var2[var4] / (4.0F * var3);
            var1[(var4 << 1) + 2] = -var2[var4] / (4.0F * var3);
         }

         this.fft.backward(var1);
         var4 = this.field_219 << 1;
         float var7 = (float)(1.0D / (double)var3);
         var1[0] = (float)(1.0D / (double)(var1[0] * 2.0F + var7));

         for(int var8 = 1; var8 < this.field_219; ++var8) {
            float var5 = var1[var8] + var1[var4 - var8];
            float var6 = var1[var8] - var1[var4 - var8];
            var5 += var7;
            var1[var8] = (float)(1.0D / (double)FAST_HYPOT(var5, var6));
         }

      }
   }
}
