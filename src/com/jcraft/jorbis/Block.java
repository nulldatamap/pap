package com.jcraft.jorbis;

import com.jcraft.jogg.Buffer;
import com.jcraft.jogg.Packet;

public class Block {
   float[][] pcm = new float[0][];
   Buffer opb = new Buffer();
   // $FF: renamed from: lW int
   int field_249;
   // $FF: renamed from: W int
   int field_250;
   // $FF: renamed from: nW int
   int field_251;
   int pcmend;
   int mode;
   int eofflag;
   long granulepos;
   long sequence;
   // $FF: renamed from: vd com.jcraft.jorbis.DspState
   DspState field_252;
   int glue_bits;
   int time_bits;
   int floor_bits;
   int res_bits;

   public Block(DspState var1) {
      this.field_252 = var1;
      if (var1.analysisp != 0) {
         this.opb.writeinit();
      }

   }

   public void init(DspState var1) {
      this.field_252 = var1;
   }

   public int clear() {
      if (this.field_252 != null && this.field_252.analysisp != 0) {
         this.opb.writeclear();
      }

      return 0;
   }

   public int synthesis(Packet var1) {
      Info var2 = this.field_252.field_267;
      this.opb.readinit(var1.packet_base, var1.packet, var1.bytes);
      if (this.opb.read(1) != 0) {
         return -1;
      } else {
         int var3;
         if ((var3 = this.opb.read(this.field_252.modebits)) == -1) {
            return -1;
         } else {
            this.mode = var3;
            this.field_250 = var2.mode_param[this.mode].blockflag;
            if (this.field_250 != 0) {
               this.field_249 = this.opb.read(1);
               this.field_251 = this.opb.read(1);
               if (this.field_251 == -1) {
                  return -1;
               }
            } else {
               this.field_249 = 0;
               this.field_251 = 0;
            }

            this.granulepos = var1.granulepos;
            this.sequence = var1.packetno - 3L;
            this.eofflag = var1.e_o_s;
            this.pcmend = var2.blocksizes[this.field_250];
            if (this.pcm.length < var2.channels) {
               this.pcm = new float[var2.channels][];
            }

            int var4;
            for(var4 = 0; var4 < var2.channels; ++var4) {
               if (this.pcm[var4] != null && this.pcm[var4].length >= this.pcmend) {
                  for(var3 = 0; var3 < this.pcmend; ++var3) {
                     this.pcm[var4][var3] = 0.0F;
                  }
               } else {
                  this.pcm[var4] = new float[this.pcmend];
               }
            }

            var4 = var2.map_type[var2.mode_param[this.mode].mapping];
            return FuncMapping.mapping_P[var4].inverse(this, this.field_252.mode[this.mode]);
         }
      }
   }
}
