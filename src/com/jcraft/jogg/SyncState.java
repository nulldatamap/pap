package com.jcraft.jogg;

public class SyncState {
   public byte[] data;
   int storage;
   int fill;
   int returned;
   int unsynced;
   int headerbytes;
   int bodybytes;
   private Page pageseek = new Page();
   private byte[] chksum = new byte[4];

   public int clear() {
      this.data = null;
      return 0;
   }

   public int buffer(int var1) {
      if (this.returned != 0) {
         this.fill -= this.returned;
         if (this.fill > 0) {
            System.arraycopy(this.data, this.returned, this.data, 0, this.fill);
         }

         this.returned = 0;
      }

      if (var1 > this.storage - this.fill) {
         var1 = var1 + this.fill + 4096;
         if (this.data != null) {
            byte[] var2 = new byte[var1];
            System.arraycopy(this.data, 0, var2, 0, this.data.length);
            this.data = var2;
         } else {
            this.data = new byte[var1];
         }

         this.storage = var1;
      }

      return this.fill;
   }

   public int wrote(int var1) {
      if (this.fill + var1 > this.storage) {
         return -1;
      } else {
         this.fill += var1;
         return 0;
      }
   }

   public int pageseek(Page var1) {
      int var2 = this.returned;
      int var3 = this.fill - this.returned;
      int var5;
      int var7;
      if (this.headerbytes == 0) {
         if (var3 < 27) {
            return 0;
         }

         if (this.data[var2] != 79 || this.data[var2 + 1] != 103 || this.data[var2 + 2] != 103 || this.data[var2 + 3] != 83) {
            this.headerbytes = 0;
            this.bodybytes = 0;
            var7 = 0;

            for(var5 = 0; var5 < var3 - 1; ++var5) {
               if (this.data[var2 + 1 + var5] == 79) {
                  var7 = var2 + 1 + var5;
                  break;
               }
            }

            if (var7 == 0) {
               var7 = this.fill;
            }

            this.returned = var7;
            return -(var7 - var2);
         }

         int var4 = (this.data[var2 + 26] & 255) + 27;
         if (var3 < var4) {
            return 0;
         }

         for(var5 = 0; var5 < (this.data[var2 + 26] & 255); ++var5) {
            this.bodybytes += this.data[var2 + 27 + var5] & 255;
         }

         this.headerbytes = var4;
      }

      if (this.bodybytes + this.headerbytes > var3) {
         return 0;
      } else {
         byte[] var8 = this.chksum;
         synchronized(this.chksum) {
            System.arraycopy(this.data, var2 + 22, this.chksum, 0, 4);
            this.data[var2 + 22] = 0;
            this.data[var2 + 23] = 0;
            this.data[var2 + 24] = 0;
            this.data[var2 + 25] = 0;
            Page var9 = this.pageseek;
            this.pageseek.header_base = this.data;
            var9.header = var2;
            var9.header_len = this.headerbytes;
            var9.body_base = this.data;
            var9.body = var2 + this.headerbytes;
            var9.body_len = this.bodybytes;
            var9.checksum();
            if (this.chksum[0] != this.data[var2 + 22] || this.chksum[1] != this.data[var2 + 23] || this.chksum[2] != this.data[var2 + 24] || this.chksum[3] != this.data[var2 + 25]) {
               System.arraycopy(this.chksum, 0, this.data, var2 + 22, 4);
               this.headerbytes = 0;
               this.bodybytes = 0;
               var7 = 0;

               for(var5 = 0; var5 < var3 - 1; ++var5) {
                  if (this.data[var2 + 1 + var5] == 79) {
                     var7 = var2 + 1 + var5;
                     break;
                  }
               }

               if (var7 == 0) {
                  var7 = this.fill;
               }

               this.returned = var7;
               return -(var7 - var2);
            }
         }

         var2 = this.returned;
         if (var1 != null) {
            var1.header_base = this.data;
            var1.header = var2;
            var1.header_len = this.headerbytes;
            var1.body_base = this.data;
            var1.body = var2 + this.headerbytes;
            var1.body_len = this.bodybytes;
         }

         this.unsynced = 0;
         this.returned += var3 = this.headerbytes + this.bodybytes;
         this.headerbytes = 0;
         this.bodybytes = 0;
         return var3;
      }
   }

   public int pageout(Page var1) {
      do {
         int var2;
         if ((var2 = this.pageseek(var1)) > 0) {
            return 1;
         }

         if (var2 == 0) {
            return 0;
         }
      } while(this.unsynced != 0);

      this.unsynced = 1;
      return -1;
   }

   public int reset() {
      this.fill = 0;
      this.returned = 0;
      this.unsynced = 0;
      this.headerbytes = 0;
      this.bodybytes = 0;
      return 0;
   }

   public void init() {
   }

   public int getDataOffset() {
      return this.returned;
   }

   public int getBufferOffset() {
      return this.fill;
   }
}
