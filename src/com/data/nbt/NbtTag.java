package com.data.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

// $FF: renamed from: com.game.game.j
public abstract class NbtTag {
   // $FF: renamed from: game java.lang.String
   private String field_745 = null;

   // $FF: renamed from: game (java.io.DataOutput) void
   abstract void writeTag(DataOutput var1) throws IOException;

   // $FF: renamed from: game (java.io.DataInput) void
   abstract void readTag(DataInput var1) throws IOException;

   // $FF: renamed from: game () byte
   public abstract byte tagId();

   // $FF: renamed from: entities () java.lang.String
   public final String tagName() {
      return this.field_745 == null ? "" : this.field_745;
   }

   // $FF: renamed from: l (java.lang.String) com.game.game.j
   public final NbtTag setName(String var1) {
      this.field_745 = var1;
      return this;
   }

   // $FF: renamed from: animals (java.io.DataInput) com.game.game.j
   public static NbtTag method_433(DataInput var0) throws IOException {
      byte var1;
      if ((var1 = var0.readByte()) == 0) {
         return new EndTag();
      } else {
         NbtTag var3 = fromId(var1);
         byte[] var2 = new byte[var0.readShort()];
         var0.readFully(var2);
         var3.field_745 = new String(var2, "UTF-8");
         var3.readTag(var0);
         return var3;
      }
   }

   // $FF: renamed from: game (com.game.game.j, java.io.DataOutput) void
   public static void method_434(NbtTag var0, DataOutput var1) throws IOException {
      var1.writeByte(var0.tagId());
      if (var0.tagId() != 0) {
         byte[] var2 = var0.tagName().getBytes("UTF-8");
         var1.writeShort(var2.length);
         var1.write(var2);
         var0.writeTag(var1);
      }
   }

   // $FF: renamed from: game (byte) com.game.game.j
   public static NbtTag fromId(byte var0) {
      switch(var0) {
      case 0:
         return new EndTag();
      case 1:
         return new ByteTag();
      case 2:
         return new ShortTag();
      case 3:
         return new IntTag();
      case 4:
         return new LongTag();
      case 5:
         return new FloatTag();
      case 6:
         return new DoubleTag();
      case 7:
         return new ByteArrayTag();
      case 8:
         return new StringTag();
      case 9:
         return new ListTag();
      case 10:
         return new CompoundTag();
      default:
         return null;
      }
   }
}
