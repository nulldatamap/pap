package com.data.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

// $FF: renamed from: com.game.game.k
public final class StringTag extends NbtTag {
   // $FF: renamed from: game java.lang.String
   public String field_756;

   public StringTag() {
   }

   public StringTag(String var1) {
      this.field_756 = var1;
   }

   // $FF: renamed from: game (java.io.DataOutput) void
   final void writeTag(DataOutput var1) throws IOException {
      byte[] var2 = this.field_756.getBytes("UTF-8");
      var1.writeShort(var2.length);
      var1.write(var2);
   }

   // $FF: renamed from: game (java.io.DataInput) void
   final void readTag(DataInput var1) throws IOException {
      byte[] var2 = new byte[var1.readShort()];
      var1.readFully(var2);
      this.field_756 = new String(var2, "UTF-8");
   }

   // $FF: renamed from: game () byte
   public final byte tagId() {
      return 8;
   }

   public final String toString() {
      return "" + this.field_756;
   }
}
