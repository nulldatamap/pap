package com.data.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

// $FF: renamed from: com.game.game.projectiles
public final class IntTag extends NbtTag {
   // $FF: renamed from: game int
   public int field_754;

   public IntTag() {
   }

   public IntTag(int var1) {
      this.field_754 = var1;
   }

   // $FF: renamed from: game (java.io.DataOutput) void
   final void writeTag(DataOutput var1) throws IOException {
      var1.writeInt(this.field_754);
   }

   // $FF: renamed from: game (java.io.DataInput) void
   final void readTag(DataInput var1) throws IOException {
      this.field_754 = var1.readInt();
   }

   // $FF: renamed from: game () byte
   public final byte tagId() {
      return 3;
   }

   public final String toString() {
      return "" + this.field_754;
   }
}
