package com.data.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

// $FF: renamed from: com.game.game.animals
public final class FloatTag extends NbtTag {
   // $FF: renamed from: game float
   public float value;

   public FloatTag() {
   }

   public FloatTag(float var1) {
      this.value = var1;
   }

   // $FF: renamed from: game (java.io.DataOutput) void
   final void writeTag(DataOutput var1) throws IOException {
      var1.writeFloat(this.value);
   }

   // $FF: renamed from: game (java.io.DataInput) void
   final void readTag(DataInput var1) throws IOException {
      this.value = var1.readFloat();
   }

   // $FF: renamed from: game () byte
   public final byte tagId() {
      return 5;
   }

   public final String toString() {
      return "" + this.value;
   }
}
