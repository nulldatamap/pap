package com.data.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

// $FF: renamed from: com.game.game.h
public final class ShortTag extends NbtTag {
   // $FF: renamed from: game short
   public short field_750;

   public ShortTag() {
   }

   public ShortTag(short var1) {
      this.field_750 = var1;
   }

   // $FF: renamed from: game (java.io.DataOutput) void
   final void writeTag(DataOutput var1) throws IOException {
      var1.writeShort(this.field_750);
   }

   // $FF: renamed from: game (java.io.DataInput) void
   final void readTag(DataInput var1) throws IOException {
      this.field_750 = var1.readShort();
   }

   // $FF: renamed from: game () byte
   public final byte tagId() {
      return 2;
   }

   public final String toString() {
      return "" + this.field_750;
   }
}
