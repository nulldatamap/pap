package com.data.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

// $FF: renamed from: com.game.game.particles
public final class ListTag extends NbtTag {
   // $FF: renamed from: game java.util.List
   private List field_747 = new ArrayList();
   // $FF: renamed from: animals byte
   private byte field_748;

   // $FF: renamed from: game (java.io.DataOutput) void
   final void writeTag(DataOutput var1) throws IOException {
      if (this.field_747.size() > 0) {
         this.field_748 = ((NbtTag)this.field_747.get(0)).tagId();
      } else {
         this.field_748 = 1;
      }

      var1.writeByte(this.field_748);
      var1.writeInt(this.field_747.size());

      for(int var2 = 0; var2 < this.field_747.size(); ++var2) {
         ((NbtTag)this.field_747.get(var2)).writeTag(var1);
      }

   }

   // $FF: renamed from: game (java.io.DataInput) void
   final void readTag(DataInput var1) throws IOException {
      this.field_748 = var1.readByte();
      int var2 = var1.readInt();
      this.field_747 = new ArrayList();

      for(int var3 = 0; var3 < var2; ++var3) {
         NbtTag var4;
         (var4 = fromId(this.field_748)).readTag(var1);
         this.field_747.add(var4);
      }

   }

   // $FF: renamed from: game () byte
   public final byte tagId() {
      return 9;
   }

   public final String toString() {
      StringBuilder var10000 = (new StringBuilder()).append("").append(this.field_747.size()).append(" entries of type ");
      byte var1 = this.field_748;
      String var10001;
      switch(this.field_748) {
      case 0:
         var10001 = "TAG_End";
         break;
      case 1:
         var10001 = "TAG_Byte";
         break;
      case 2:
         var10001 = "TAG_Short";
         break;
      case 3:
         var10001 = "TAG_Int";
         break;
      case 4:
         var10001 = "TAG_Long";
         break;
      case 5:
         var10001 = "TAG_Float";
         break;
      case 6:
         var10001 = "TAG_Double";
         break;
      case 7:
         var10001 = "TAG_Byte_Array";
         break;
      case 8:
         var10001 = "TAG_String";
         break;
      case 9:
         var10001 = "TAG_List";
         break;
      case 10:
         var10001 = "TAG_Compound";
         break;
      default:
         var10001 = "UNKNOWN";
      }

      return var10000.append(var10001).toString();
   }

   // $FF: renamed from: game (com.game.game.j) void
   public final void method_436(NbtTag var1) {
      this.field_748 = var1.tagId();
      this.field_747.add(var1);
   }

   // $FF: renamed from: game (int) com.game.game.j
   public final NbtTag get(int var1) {
      return (NbtTag)this.field_747.get(var1);
   }

   // $FF: renamed from: animals () int
   public final int method_438() {
      return this.field_747.size();
   }
}
