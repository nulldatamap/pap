package com.data.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

// $FF: renamed from: com.game.game.player
public final class DoubleTag extends NbtTag {
   // $FF: renamed from: game double
   private double field_746;

   // $FF: renamed from: game (java.io.DataOutput) void
   final void writeTag(DataOutput var1) throws IOException {
      var1.writeDouble(this.field_746);
   }

   // $FF: renamed from: game (java.io.DataInput) void
   final void readTag(DataInput var1) throws IOException {
      this.field_746 = var1.readDouble();
   }

   // $FF: renamed from: game () byte
   public final byte tagId() {
      return 6;
   }

   public final String toString() {
      return "" + this.field_746;
   }
}
