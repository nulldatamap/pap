package com.data.nbt;

import java.io.DataInput;
import java.io.DataOutput;

// $FF: renamed from: com.game.game.l
public final class EndTag extends NbtTag {
   // $FF: renamed from: game (java.io.DataInput) void
   final void readTag(DataInput var1) {
   }

   // $FF: renamed from: game (java.io.DataOutput) void
   final void writeTag(DataOutput var1) {
   }

   // $FF: renamed from: game () byte
   public final byte tagId() {
      return 0;
   }

   public final String toString() {
      return "END";
   }
}
