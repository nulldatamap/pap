package com.data.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

// $FF: renamed from: com.game.game.i
public final class ByteArrayTag extends NbtTag {
   // $FF: renamed from: game byte[]
   public byte[] field_749;

   public ByteArrayTag() {
   }

   public ByteArrayTag(byte[] var1) {
      this.field_749 = var1;
   }

   // $FF: renamed from: game (java.io.DataOutput) void
   final void writeTag(DataOutput var1) throws IOException {
      var1.writeInt(this.field_749.length);
      var1.write(this.field_749);
   }

   // $FF: renamed from: game (java.io.DataInput) void
   final void readTag(DataInput var1) throws IOException {
      int var2 = var1.readInt();
      this.field_749 = new byte[var2];
      var1.readFully(this.field_749);
   }

   // $FF: renamed from: game () byte
   public final byte tagId() {
      return 7;
   }

   public final String toString() {
      return "[" + this.field_749.length + " bytes]";
   }
}
