package com.data.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

// $FF: renamed from: com.game.game.game
public final class LongTag extends NbtTag {
   // $FF: renamed from: game long
   public long field_755;

   public LongTag() {
   }

   public LongTag(long var1) {
      this.field_755 = var1;
   }

   // $FF: renamed from: game (java.io.DataOutput) void
   final void writeTag(DataOutput var1) throws IOException {
      var1.writeLong(this.field_755);
   }

   // $FF: renamed from: game (java.io.DataInput) void
   final void readTag(DataInput var1) throws IOException {
      this.field_755 = var1.readLong();
   }

   // $FF: renamed from: game () byte
   public final byte tagId() {
      return 4;
   }

   public final String toString() {
      return "" + this.field_755;
   }
}
