package com.data.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

// $FF: renamed from: com.game.game.entities
public final class CompoundTag extends NbtTag {
   // $FF: renamed from: game java.util.Map
   private Map field_751 = new HashMap();

   // $FF: renamed from: game (java.io.DataOutput) void
   final void writeTag(DataOutput var1) throws IOException {
      Iterator var2 = this.field_751.values().iterator();

      while(var2.hasNext()) {
         NbtTag.method_434((NbtTag)var2.next(), var1);
      }

      var1.writeByte(0);
   }

   // $FF: renamed from: game (java.io.DataInput) void
   final void readTag(DataInput var1) throws IOException {
      this.field_751.clear();

      NbtTag var2;
      while((var2 = NbtTag.method_433(var1)).tagId() != 0) {
         this.field_751.put(var2.tagName(), var2);
      }

   }

   // $FF: renamed from: game () byte
   public final byte tagId() {
      return 10;
   }

   // $FF: renamed from: game (java.lang.String, com.game.game.j) void
   public final void write_list(String var1, NbtTag var2) {
      this.field_751.put(var1, var2.setName(var1));
   }

   // $FF: renamed from: game (java.lang.String, byte) void
   public final void write_byte(String var1, byte var2) {
      this.field_751.put(var1, (new ByteTag(var2)).setName(var1));
   }

   // $FF: renamed from: game (java.lang.String, short) void
   public final void write_short(String var1, short var2) {
      this.field_751.put(var1, (new ShortTag(var2)).setName(var1));
   }

   // $FF: renamed from: game (java.lang.String, int) void
   public final void write_int(String var1, int var2) {
      this.field_751.put(var1, (new IntTag(var2)).setName(var1));
   }

   // $FF: renamed from: game (java.lang.String, long) void
   public final void write_long(String var1, long var2) {
      this.field_751.put(var1, (new LongTag(var2)).setName(var1));
   }

   // $FF: renamed from: game (java.lang.String, float) void
   public final void write_float(String var1, float var2) {
      this.field_751.put(var1, (new FloatTag(var2)).setName(var1));
   }

   // $FF: renamed from: game (java.lang.String, java.lang.String) void
   public final void write_string(String var1, String var2) {
      this.field_751.put(var1, (new StringTag(var2)).setName(var1));
   }

   // $FF: renamed from: game (java.lang.String, byte[]) void
   public final void method_446(String var1, byte[] var2) {
      this.field_751.put(var1, (new ByteArrayTag(var2)).setName(var1));
   }

   // $FF: renamed from: game (java.lang.String, com.game.game.entities) void
   public final void method_447(String var1, CompoundTag var2) {
      this.field_751.put(var1, var2.setName(var1));
   }

   // $FF: renamed from: game (java.lang.String, boolean) void
   public final void method_448(String var1, boolean var2) {
      this.write_byte(var1, (byte)(var2 ? 1 : 0));
   }

   // $FF: renamed from: game (java.lang.String) boolean
   public final boolean method_449(String var1) {
      return this.field_751.containsKey(var1);
   }

   // $FF: renamed from: animals (java.lang.String) byte
   public final byte read_byte(String var1) {
      return !this.field_751.containsKey(var1) ? 0 : ((ByteTag)this.field_751.get(var1)).field_753;
   }

   // $FF: renamed from: entities (java.lang.String) short
   public final short read_short(String var1) {
      return !this.field_751.containsKey(var1) ? 0 : ((ShortTag)this.field_751.get(var1)).field_750;
   }

   // $FF: renamed from: projectiles (java.lang.String) int
   public final int method_452(String var1) {
      return !this.field_751.containsKey(var1) ? 0 : ((IntTag)this.field_751.get(var1)).field_754;
   }

   // $FF: renamed from: player (java.lang.String) long
   public final long method_453(String var1) {
      return !this.field_751.containsKey(var1) ? 0L : ((LongTag)this.field_751.get(var1)).field_755;
   }

   // $FF: renamed from: particles (java.lang.String) float
   public final float method_454(String var1) {
      return !this.field_751.containsKey(var1) ? 0.0F : ((FloatTag)this.field_751.get(var1)).value;
   }

   // $FF: renamed from: player (java.lang.String) java.lang.String
   public final String method_455(String var1) {
      return !this.field_751.containsKey(var1) ? "" : ((StringTag)this.field_751.get(var1)).field_756;
   }

   // $FF: renamed from: h (java.lang.String) byte[]
   public final byte[] method_456(String var1) {
      return !this.field_751.containsKey(var1) ? new byte[0] : ((ByteArrayTag)this.field_751.get(var1)).field_749;
   }

   // $FF: renamed from: i (java.lang.String) com.game.game.entities
   public final CompoundTag method_457(String var1) {
      return !this.field_751.containsKey(var1) ? new CompoundTag() : (CompoundTag)this.field_751.get(var1);
   }

   // $FF: renamed from: j (java.lang.String) com.game.game.particles
   public final ListTag read_list(String var1) {
      return !this.field_751.containsKey(var1) ? new ListTag() : (ListTag)this.field_751.get(var1);
   }

   // $FF: renamed from: k (java.lang.String) boolean
   public final boolean method_459(String var1) {
      return this.read_byte(var1) != 0;
   }

   public final String toString() {
      return "" + this.field_751.size() + " entries";
   }

   // $FF: renamed from: animals () boolean
   public final boolean method_460() {
      return this.field_751.isEmpty();
   }
}
