package com.data.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

// $FF: renamed from: com.game.game.player
public final class ByteTag extends NbtTag {
   // $FF: renamed from: game byte
   public byte field_753;

   public ByteTag() {
   }

   public ByteTag(byte var1) {
      this.field_753 = var1;
   }

   // $FF: renamed from: game (java.io.DataOutput) void
   final void writeTag(DataOutput var1) throws IOException {
      var1.writeByte(this.field_753);
   }

   // $FF: renamed from: game (java.io.DataInput) void
   final void readTag(DataInput var1) throws IOException {
      this.field_753 = var1.readByte();
   }

   // $FF: renamed from: game () byte
   public final byte tagId() {
      return 1;
   }

   public final String toString() {
      return "" + this.field_753;
   }
}
